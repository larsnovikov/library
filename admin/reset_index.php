<?php


session_start();
header("HTTP/1.1 200");
header("Status: 200 OK");
header('Content-Type: text/html;  charset=utf-8');

require_once '../config/constants.php';
require_once COREPATH . 'classes/skAutoloader.php';

skAutoloader::registerPath(COREPATH,BUILDPATH);

skLogger::init(ROOTPATH.'log/access.log');

iconv_set_encoding("input_encoding", 'UTF-8');
iconv_set_encoding("internal_encoding", 'UTF-8');
iconv_set_encoding("output_encoding", 'UTF-8');

mb_internal_encoding("UTF-8");

skConfig::getFile('../config/config.php');
skConfig::getOverlayFile('../config/config.db.php');
skConfig::getOverlayFile('../config/config.local.php');

$aDBConf = skConfig::get('db');
$odb = new skDB($aDBConf['host'],
    $aDBConf['user'],
    $aDBConf['pass'],
    $aDBConf['name']);

$odb->query("SET SESSION character_set_server='utf8'");
$odb->query("set character_set_client='utf8'");
$odb->query("set character_set_results='utf8'");
$odb->query("set collation_connection='utf8_general_ci'");

skLinker::init(COREPATH);

skTwig::Load( array(), skConfig::get('cache.rootPath').'Twig/', skConfig::get('debug.parser'));

Auth::init();

skProcessor::init('Cms');
skRequest::init();

if ( CurrentAdmin::isLoggedIn() ){

    if( Search::resetIndexTable() ) echo "Обновление поискового индекса выполнено";
}

