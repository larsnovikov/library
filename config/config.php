<?php
/**
 * $Id: config.php 1277 2012-11-22 12:17:57Z armit $
 */

/*main*/

$aConfig['path']['root'] = ROOTPATH;
$aConfig['path']['core'] = COREPATH;
$aConfig['url']['root']  = WEBROOTPATH;

/*RDBMS*/

/** @config string db.host Хост пользователя подключаемой базы данных  */
$aConfig['db']['host'] = '';

/** @config string db.user Имя пользователя подключаемой базы данных  */
$aConfig['db']['user'] = '';

/** @config string db.pass Пароль подключаемой базы данных  */
$aConfig['db']['pass'] = '';

/** @config string db.name Имя подключаемой базы данных  */
$aConfig['db']['name'] = '';

/* Пользователи */
$aConfig['users'][] = array(
    'login' => '',
    'pass'  => '',
);

/*cache*/

/** @config string cache.rootPath Путь к корневой директории хранения файлового кеша  */
$aConfig['cache']['rootPath'] = ROOTPATH.'cache/';

/** @config string cache.css Путь к корневой директории хранения скомпилированных файлов стилей  */
$aConfig['cache']['css'] = ROOTPATH.'cache/css/';

/*Security*/

/** @config string security.vector Вектор шифрования для Blowfish. Используется в генерации и проверке подписей
 *  Gateway */
$aConfig['security']['vector'] = 'x03nMwK34x&ciSUH0I1got';

/*debug mode*/

/** @config boolean debug.wsdl Флаг отладки Web сервиса
 * (WSDL документ не кешируется и пересоздается каждый раз по запросу)
 * @deprecated Проверить использование
 */
$aConfig['debug']['wsdl'] = false;

/** @config boolean debug.parser Флаг отладки для парсера
 * (Шаблоны компилируются постоянно)
 */
$aConfig['debug']['parser'] = false;

/** @config boolean debug.config Флаг отладки реестра
 * (Файлы конфигураций с модулей собираются постоянно)
 */
$aConfig['debug']['config'] = false;

/** @config boolean debug.css Флаг очистки директории хранения кеша css-файлов
 * (Скомпилированные файлы стилей в debug режиме собираются постоянно)
 */
$aConfig['debug']['css'] = false;

/* Настройки логирования */

/** @config boolean log.enable Флаг включения логирования
 */
$aConfig['log']['enable']  = true;

/** @config boolean log.users Флаг включения логирования действий пользователей
 */
$aConfig['log']['users']  = true;

/** @config boolean log.cron Флаг включения логирования планировщика заданий
 */
$aConfig['log']['cron']   = true;

/** @config boolean log.system Флаг включения логирования системного журнала
 */
$aConfig['log']['system'] = true;

/** @config boolean log.debug Флаг включения логирования журнала отладки
 */
$aConfig['log']['debug']  = true;

/* allow browsers */
/* Наименьшая допустимая версия браузера, поддерживаемая системой */
$aConfig['browser']['Internet Explorer'] = 7.0;

$aConfig['browser']['Opera'] = 9.0;

$aConfig['browser']['Firefox'] = 3.5;

$aConfig['browser']['Chrome'] = 10;

/*sections*/

/** @config integer section.main Id раздела, загружаемого по-умолчанию */
$aConfig['section']['main'] = 78;

/** @config integer section.root Id корневого раздела */
$aConfig['section']['root'] = 0;

/** @config integer section.sectionRoot Id корневого раздела для админки */
$aConfig['section']['sectionRoot'] = 3;

/** @config integer section.404 Id раздела 404 ошибки */
$aConfig['section']['404'] = 137;

/** @config integer section.auth Id раздела авторизации */
$aConfig['section']['auth'] = 137;

/** @config integer section.templates Id корневого раздела с шаблонами */
$aConfig['section']['templates'] = 2;

/** @config integer section.library Id корневого раздела с библиотеками */
$aConfig['section']['library'] = 1;

/*session*/

/** @config string Session.tickets.key Ключ сессии для хранения тикетов класса skSessionTicket */
$aConfig['session']['tickets']['key'] = '_tickets';

/** @config string Session.process.key Ключ сессии для хранения дерева процессов класса skProcessSession */
$aConfig['session']['process']['key'] = '_processSession';
$aConfig['session']['process']['designKey'] = '_designProcessSession';

$aConfig['notifications']['noreplay_email'] = 'no-reply@'.str_replace('/','',WEBROOTPATH);

/* JS inclusions */

/**
 *@example sample conditions '/core/0001/libs/jquery/jquery.js' => array('condition' => 'IE'),
 */
$aConfig['inclusion']['js'] = array(
    '/skewer_build/libs/jquery/jquery.js',
    '/skewer_build/libs/fancyBox/jquery.mousewheel-3.0.6.pack.js',
    '/skewer_build/libs/fancyBox/jquery.fancybox.pack.js',
    '/skewer_build/libs/datepicker/jquery.datepicker.js',
    '/skewer_build/libs/datepicker/jquery.datepicker-ru.js',
);

/* CSS inclusions */
/**
 *@example sample conditions '/core/0001/libs/jquery/jquery.js' => array('condition' => 'IE'),
 */
$aConfig['inclusion']['css'] = array(
    '/skewer_build/libs/fancyBox/jquery.fancybox.css',
    '/skewer_build/libs/datepicker/jquery.datepicker.css',
);

/** Parser */

$aConfig['parser']['default']['paths'] = array( BUILDPATH.'common/templates/' );

/* Upload options */

/** @config array upload.maxsize Максимально допустимый размер для загрузки изображений в байтах */
$aConfig['upload']['maxsize'] = 100 * 1024 * 1024;

/** @config array upload.images.maxWidth Максимально допустимый размер для загрузки изображений px по ширине */
$aConfig['upload']['images']['maxWidth'] = 4000;

/** @config array upload.images.maxHeight Максимально допустимый размер для загрузки изображений px по высоте */
$aConfig['upload']['images']['maxHeight'] = 4000;

/** @config array upload.allow.images Список разрешенных для загрузки расширений файлов изображений */
$aConfig['upload']['allow']['images'] = array('jpg', 'jpeg', 'gif', 'png', 'ico');

/** @config array upload.allow.images Список разрешенных для загрузки расширений flash-файлов */
$aConfig['upload']['allow']['flash'] = array('swf', 'flv');

/** @config array upload.allow.images Список разрешенных для загрузки расширений media-файлов */
$aConfig['upload']['allow']['media'] = array('aiff', 'asf', 'avi', 'bmp', 'fla', 'flv', 'gif', 'jpeg', 'jpg', 'mid',
                                             'mov', 'mp3', 'mp4', 'mpc', 'mpeg', 'mpg', 'png', 'qt', 'ram', 'rm',
                                             'rmi', 'rmvb', 'swf', 'tif', 'tiff', 'wav', 'wma', 'wmv');

/** @config array upload.allow.images Список разрешенных для загрузки расширений файлов */
$aConfig['upload']['allow']['files'] = array('7z', 'aiff', 'asf', 'avi', 'bmp', 'csv', 'doc', 'docx', 'fla', 'flv', 'gif',
                                             'gz', 'gzip', 'jpeg', 'jpg', 'mid', 'mov', 'mp3', 'mp4', 'mpc', 'mpeg',
                                             'mpg', 'ods', 'odt', 'pdf', 'png', 'ppt', 'pxd', 'qt', 'ram', 'rar',
                                             'rm', 'rmi', 'rmvb', 'rtf', 'sdc', 'sitd', 'swf', 'sxc', 'sxw', 'tar',
                                             'tgz', 'tif', 'tiff', 'txt', 'vsd', 'wav', 'wma', 'wmv', 'xls', 'xml',
                                             'zip', 'ico');



return $aConfig;
