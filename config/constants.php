<?php
/**
 * Константы окружения
 *
 * @version $Revision: 1277 $
 * @author ArmiT $Author: armit $
 * @project Skewer
 * @package Kernel
 * @date $Date: 2012-11-22 16:17:57 +0400 (Чт., 22 нояб. 2012) $
 */
/**
 *  О составе путей:
 *
 *  Площадка в зависимости от назначения может иметь два типа путей до сборки, на которой она работает:
 *  1. Площадка для разработки со своей сборкой (USECLUSTERBUILD = false):
 *
 *  /var/www/<sitename>/skewer/build/
 *                             core/
 *                             patches/
 * |_____ROOTPATH______|            - Путь к корневой директории площадки
 * |_______RELEASEPATH_______|      - Путь к корневой директории релиза(сборки, ядра и патчей)
 * |______BUILDPATH, COREPATH______|- Пути к корневым директориям сборки и ядра
 *
 * 2. Площадка в кластере, использующая кластерные сборки (USECLUSTERBUILD = true):
 *
 * /var/skewerCluster/<build Name>/<build Number>/build/
 *                                                core/
 *                                                patches/
 *                   |_BUILDNAME__|                      - Имя сборки
 *                                |_BUILDNUMBER__|       - Номер сборки
 *                   |________BUILDVERSION_______|       - Версия сборки (Имя.номер)
 * |___CLUSTERPATH___|                                   - Путь к корневой директории кластера
 * |_______________RELEASEPATH___________________|       - Путь к корневой директории релиза в кластере (сборки, ядра и патчей)
 * |_____________BUILDPATH, COREPATH____________________|- Пути к корневым директориям сборки и ядра в кластере
 *
 *
 *
 */

/* Подключается файл, генерируемый автоматически системой обновления площадок */
include_once(dirname(__FILE__).DIRECTORY_SEPARATOR.'constants.generated.php');

/* Группа реагирования на автоматические настройки */

/**
 * @const BUILDNAME string имя сборки
 */
defined('BUILDNAME') OR define('BUILDNAME', 'canape');

/**
 * @const BUILDNAME string номер сборки
 */
defined('BUILDNUMBER') OR define('BUILDNUMBER', '0014');

/**
 * @const BUILDVERSION string версия сборки
 */
defined('BUILDVERSION') OR define('BUILDVERSION', BUILDNAME.BUILDNUMBER);



/**
 * @const USECLUSTERBUILD bool Указатель на то, что используется сборка, находящаяся в кластере
 */
defined('USECLUSTERBUILD') OR define('USECLUSTERBUILD', false);

/**
 * @const CLUSTERGATEWAY string Адрес кластерного шлюза
 */
defined('CLUSTERGATEWAY') OR define('CLUSTERGATEWAY', 'http://sms.twinslab.ru/gateway/index.php');

/* Константы путей */

/**
 * @const CLUSTERPATH string Путь до корневой директории кластера
 */
defined('CLUSTERPATH') OR define('CLUSTERPATH', '');

/**
 * @const ROOTPATH string Путь до корневой директории площадки
 */
defined('ROOTPATH') OR define('ROOTPATH', dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR);

/**
 * @const CLUSTERROOTPATH string Путь до корневой директории кластера
 */
defined('RELEASEPATH') OR define('RELEASEPATH', ROOTPATH.'skewer/');

/**
 * @const BUILDPATH string Путь до корневой директории сборки
 */
defined('BUILDPATH') OR define('BUILDPATH', RELEASEPATH.'build/');

/**
* @const COREPATH string Путь до корневой директории ядра
*/
defined('COREPATH') OR define('COREPATH', RELEASEPATH.'core/');

/**
 * @const PATCHPATH string Путь до корневой директории патчей
 */
defined('PATCHPATH') OR define('PATCHPATH', ROOTPATH.'update/');

/* конец группы реагирования на автоматические настройки */

/**
 * @const FILEPATH string Путь до корневой директории загрузки публичных файлов
 */
define('FILEPATH', ROOTPATH.'files/');

/**
 * @const PRIVATE_FILEPATH string Путь до корневой директории загрузки закрытых файлов
 */
define('PRIVATE_FILEPATH', ROOTPATH.'private_files/');

/**
 * @const WEBROOTPATH string Путь до директории домена (условное определние)
 */
if(!isSet($_SERVER['HTTP_HOST'])) $_SERVER['HTTP_HOST'] = '';

define('WEBROOTPATH', $_SERVER['HTTP_HOST'].'/');



/**
 * @const WebCorePath URL до корня директории ядра
 * @deprecated Проверить использование
 */
define('WebCorePath', '/skewer/core/');

/**
 * @const psNew integer Статусы процесса - новый процесс
 */
define('psNew', 0);

/**
 * @const psComplete integer Статусы процесса - процесс отработал в штатном режиме
 */
define('psComplete', 1);

/**
 * @const psWait integer Статусы процесса - процесс ожидает отработки другого процесса
 */
define('psWait', 2);

/**
 * @const psNotFound integer Статусы процесса - процесс на который ссылаются не найден
 */
define('psNotFound', 3);

/**
 * @const psExit integer Статусы процесса - процесс закончил работу с критической ошибкой (404 auth)
 */
define('psExit', 4);

/**
 * @const psRendered integer Статусы процесса - процесс закончил рендеринг данных в шаблон
 */
define('psRendered', 5);

/**
 * @const psRendered integer Статусы процесса - процесс закончил работу с ошибкой
 */
define('psError', 6);

/**
 * @const psAll integer Статусы процесса - любой процесс
 */
define('psAll', 7);

/**
 * @const psReset integer Статусы процесса - сбросить корневой процесс
 */
define('psReset', 8);


/*Типы шаблонизаторов*/

/**
 * @const parserTwig integer Шаблонизаторы - процесс выбирает шаблонизатор skTwig для обработки данных в шаблоне
 */
define('parserTwig', 1);

/**
 * @const parserContenter integer Шаблонизаторы - процесс выбирает шаблонизатор Contenter для обработки данных в шаблоне
 */
define('parserContenter', 2);

/**
 * @const parserJSON integer Шаблонизаторы - процесс отправляет данные в менеджер для последующей конвертации в JSON формат
 */
define('parserJSON', 3);

/*Типы вызова*/

/**
 * @const ctPage integer Типы вызова - процесс вызывается как модуль
 * @deprecated Возможно, что уже не используется
 */
define('ctPage', 11);

/**
 * @const ctModule integer Типы вызова - процесс вызывается как страница
 */
define('ctModule', 12);

/*router predefined pages*/

/**
 * @const page404 integer Предустановленные разделы - раздел 404
 */
define('page404', 21);

/**
 * @const pageAuth integer Предустановленные разделы - раздел авторизации
 */
define('pageAuth', 22);


/* Очередь заданий - статусы задач */
/** @deprecated Проверить использование */
/**
 * @const taskStatusNew integer Статусы задачи - новая задача
 */
define('taskStatusNew', 31);

/**
 * @const taskStatusRunning integer Статусы задачи - выполняется
 */
define('taskStatusRunning', 32);

/**
 * @const taskStatusComplete integer Статусы задачи - выполнена
 */
define('taskStatusComplete', 33);

/**
 * @const taskStatusNotComplete integer Статусы задачи - не выполнена
 */
define('taskStatusNotComplete', 34);

/**
 * @const taskStatusTimeout integer Статусы задачи - завершено но таймауту
 */
define('taskStatusTimeout', 35);

/**
 * @const taskStatusTimeout integer Статусы задачи - повторить
 */
define('taskStatusRepeat', 36);

/* Очередь заданий - приоритеты задач */
/** @deprecated Проверить использование */
/**
 * @const taskPriorityLow integer Приоритеты задач - низкий
 */
define('taskPriorityLow', 41);

/**
 * @const taskPriorityNormal integer Приоритеты задач - нормальный
 */
define('taskPriorityNormal', 42);

/**
 * @const taskPriorityHigh integer Приоритеты задач - высокий
 */
define('taskPriorityHigh', 43);

/**
 * @const taskPriorityCritical integer Приоритеты задач - критический
 */
define('taskPriorityCritical', 44);

/* Очередь заданий - ресурсоемкость */
/** @deprecated Проверить использование */
/**
 * @const taskWeightLow integer Ресурсоемкость задач - низкая. Фоновая служба
 */
define('taskWeightLow', 51);

/**
 * @const taskWeightNormal integer Ресурсоемкость задач - обычная
 */
define('taskWeightNormal', 52);

/**
 * @const taskWeightHigh integer Ресурсоемкость задач - высокая
 */
define('taskWeightHigh', 53);

/**
 * @const taskWeightHigh integer Ресурсоемкость задач - критическая
 */
define('taskWeightCritical', 54);

/* Очередь заданий - область применения */
/** @deprecated Проверить использование */
/**
 * @const taskTargetSite integer Область применения задачи - локальная (на уровне сайта)
 */
define('taskTargetSite', 61);

/**
 * @const taskTargetServer integer Область применения задачи - уровня сервера
 */
define('taskTargetServer', 62);

/**
 * @const taskTargetServer integer Область применения задачи - общая на кластер
 */
define('taskTargetCluster', 63);

/* Logger Log Types */
/** @todo вынести в модуль логгирования  -  вынес - удалить после проверки*/
/**
 * @const logUsers integer Тип журнала событий - журнал действий пользователей
 *
define('logUsers', 71);

/**
 * @const logCron integer Тип журнала событий - журнал планировщика заданий
 *
define('logCron', 72);

/**
 * @const logSystem integer Тип журнала событий - системный журнал
 *
define('logSystem', 73);

/**
 * @const logDebug integer Тип журнала событий - журнал отладки
 *
define('logDebug', 74);  */

/* Типы выравнивания watermark`ов на изображениях */
/** @todo вынести в модуль галлереи */
/**
 * @const alignWatermarkTopLeft integer Типы выравнивания watermark - верхний левый угол
 */
define('alignWatermarkTopLeft', 81);

/**
 * @const alignWatermarkTopRight integer Типы выравнивания watermark - верхний правый угол
 */
define('alignWatermarkTopRight', 82);

/**
 * @const alignWatermarkBottomLeft integer Типы выравнивания watermark - нижний левый угол
 */
define('alignWatermarkBottomLeft', 83);

/**
 * @const alignWatermarkBottomRight integer Типы выравнивания watermark - нижний правый угол
 */
define('alignWatermarkBottomRight', 84);

/**
 * @const alignWatermarkCenter integer Типы выравнивания watermark - по центру
 */
define('alignWatermarkCenter', 85);

