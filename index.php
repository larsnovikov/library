<?php
/**
 *
 * $Id: index.php 691 2012-08-24 13:53:07Z sys $ 
 */
session_start();
ini_set('display_errors', 0);
error_reporting(E_ALL | E_DEPRECATED);
require_once 'config/constants.php';
require_once COREPATH . 'classes/skAutoloader.php';

skAutoloader::registerPath(COREPATH,BUILDPATH);
skLanguage::init(BUILDPATH, 'i18n/ru.php');
skLogger::init(ROOTPATH.'log/access.log');

iconv_set_encoding("input_encoding", 'UTF-8');
iconv_set_encoding("internal_encoding", 'UTF-8');
iconv_set_encoding("output_encoding", 'UTF-8');

mb_internal_encoding("UTF-8");

skConfig::getFile('config/config.php');
skConfig::getOverlayFile('config/config.db.php');
skConfig::getOverlayFile('config/config.local.php');

$aDBConf = skConfig::get('db');
$odb = new skDB($aDBConf['host'],
              $aDBConf['user'], 
              $aDBConf['pass'], 
              $aDBConf['name']);
$t = array();

$odb->query("SET SESSION character_set_server='utf8'", $t);
$odb->query("set character_set_client='utf8'");
$odb->query("set character_set_results='utf8'");
$odb->query("set collation_connection='utf8_general_ci'");

skLinker::init(COREPATH);

skTwig::Load( array(), skConfig::get('cache.rootPath').'Twig/', skConfig::get('debug.parser'));

Auth::init();

skProcessor::init('Page');



skRequest::init();
skResponse::init();

echo skProcessor::build();
//skLogger::display('Загружено: '.count(skAutoloader::getLoadedClasses()),skAutoloader::getLoadedClasses());
