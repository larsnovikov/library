<?php
/**
 * Шлюз - используется для работы с площадкой через Security Soap Channel
 * $Id: index.php 284 2012-06-13 14:21:04Z armit $
 */
session_start();
ini_set('display_errors', 1);
error_reporting(E_ALL | E_DEPRECATED);

require_once '../config/constants.php';
require_once COREPATH.'classes/skAutoloader.php';

skAutoloader::registerPath(COREPATH,BUILDPATH);

skLanguage::init(BUILDPATH, 'i18n/ru.php');
skLogger::init(ROOTPATH.'log/access.log');


iconv_set_encoding("input_encoding", 'UTF-8');
iconv_set_encoding("internal_encoding", 'UTF-8');
iconv_set_encoding("output_encoding", 'UTF-8');

mb_internal_encoding("UTF-8");

skConfig::getFile(ROOTPATH.'config/config.php');
skConfig::getOverlayFile(ROOTPATH.'config/config.db.php');
skConfig::getOverlayFile(ROOTPATH.'config/config.local.php');

$aDBConf = skConfig::get('db');
$odb = new skDB($aDBConf['host'],
    $aDBConf['user'],
    $aDBConf['pass'],
    $aDBConf['name']);
$t = array();

$odb->query("SET SESSION character_set_server='utf8'", $t);
$odb->query("set character_set_client='utf8'");
$odb->query("set character_set_results='utf8'");
$odb->query("set collation_connection='utf8_general_ci'");

skLinker::init(COREPATH);

skTwig::Load( array(), skConfig::get('cache.rootPath').'Twig/', skConfig::get('debug.parser'));

Auth::init();
skError::init();

skProcessor::init('Cron');

skRequest::init();
skResponse::init();

echo skProcessor::build();
