<?php
session_start();
header("HTTP/1.1 200");
header("Status: 200 OK");
header('Content-Type: image/gif;  charset=utf-8');

require_once '../config/constants.php';
require_once COREPATH . 'classes/skAutoloader.php';

skAutoloader::registerPath(COREPATH,BUILDPATH);

iconv_set_encoding("input_encoding", 'UTF-8');
iconv_set_encoding("internal_encoding", 'UTF-8');
iconv_set_encoding("output_encoding", 'UTF-8');

mb_internal_encoding("UTF-8");

skConfig::getFile('../config/config.php');
skConfig::getOverlayFile('../config/config.db.php');
skConfig::getOverlayFile('../config/config.local.php');

$oCaptcha = Captcha::getInstance();
$oCaptcha->fontFile = BUILDPATH.'common/fonts/palab.ttf';
$oCaptcha->width       = 90;
$oCaptcha->height      = 40;
$oCaptcha->fontSize    = 18;
$oCaptcha->textColor = '#000';
$oCaptcha->get_captcha();