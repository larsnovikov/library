<?php
/**
 * Ajax слой
 *
 * @author ArmiT, $Author: acat $
 * @version $Revision: 1258 $
 * @date $Date: 2012-11-21 14:39:54 +0400 (Ср., 21 нояб. 2012) $
 *
 */

session_start();
header("HTTP/1.1 200");
header("Status: 200 OK");
header('Content-Type: text/html;  charset=utf-8');

require_once '../config/constants.php';
require_once COREPATH . 'classes/skAutoloader.php';

skAutoloader::registerPath(COREPATH,BUILDPATH);

skLanguage::init(BUILDPATH, 'i18n/ru.php');
skLogger::init(ROOTPATH.'log/access.log');

iconv_set_encoding("input_encoding", 'UTF-8');
iconv_set_encoding("internal_encoding", 'UTF-8');
iconv_set_encoding("output_encoding", 'UTF-8');

mb_internal_encoding("UTF-8");

skConfig::getFile('../config/config.php');
skConfig::getOverlayFile('../config/config.db.php');
skConfig::getOverlayFile('../config/config.local.php');

$aDBConf = skConfig::get('db');
$odb = new skDB($aDBConf['host'],
    $aDBConf['user'],
    $aDBConf['pass'],
    $aDBConf['name']);

$odb->query("SET SESSION character_set_server='utf8'");
$odb->query("set character_set_client='utf8'");
$odb->query("set character_set_results='utf8'");
$odb->query("set collation_connection='utf8_general_ci'");

skLinker::init(COREPATH);

skTwig::Load( array(), skConfig::get('cache.rootPath').'Twig/', skConfig::get('debug.parser'));

Auth::init();

skProcessor::init('Ajax');
skRequest::init();

echo skProcessor::build();
