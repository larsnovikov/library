<?php
/**
 * Прототип Vendor`a правил маршрутизации для модулей
 * @class skRoutingInterface
 *
 * @author ArmiT, $Author: sapozhkov $
 * @version $Revision: 6 $
 * @date $Date: 2012-05-18 10:52:53 +0400 (Пт., 18 мая 2012) $
 * @project Skewer
 * @package kernel
 */

interface skRoutingInterface {
    /**
     * Возвращает паттерны разбора URL
     * @static
     * @return bool | array
     */
    public static function getRoutePatterns();
}// interface
