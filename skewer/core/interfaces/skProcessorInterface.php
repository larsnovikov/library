<?php
/**
 * Интерфейс построения менеджеров процессов
 * @class skProcessorInterface
 * @project Skewer
 * @package kernel
 *
 * @author ArmiT, $Author: sapozhkov $
 * @version $Revision: 6 $
 * @date $Date: 2012-05-18 10:52:53 +0400 (Пт., 18 мая 2012) $
 *
 */
interface skProcessorInterface {

    /**
     * Прототип отработки процессов
     * @abstract
     */
    public function build();

    /**
     * Прототип получения процесса по пути и статусу
     * @abstract
     * @param string $sPath путь по меткам вызова до процесса
     * @param integer $iStatus Статус отработки процесса
     */
    public function getProcess($sPath, $iStatus = psComplete);

    /**
     * Прототип выполнения дерева процессов
     * @abstract
     */
    public function executeProcessList();

    /**
     * Прототип добавления нового процесса
     * @abstract
     * @param skContext $oContext Контекст нового процесса
     */
    public function addProcess(skContext $oContext);
}// interface
