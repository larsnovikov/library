<?php
/**
 * @todo Доки
 * @class skHookInterface
 *
 * @author ArmiT, $Author: sapozhkov $
 * @version $Revision: 6 $
 * @date $Date: 2012-05-18 10:52:53 +0400 (Пт., 18 мая 2012) $
 * @project Skewer
 * @package kernel
 */
interface skHookInterface {


    public function registerHook();

    public function addBeforeHook($sEventName, $sClassName, $sHandlerMethod);

    public function addAfterHook($sEventName, $sClassName, $sHandlerMethod);

}// interface
