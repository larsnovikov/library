<?php
/**
 * Class (singleton)-configurator for manipulated configuration files
 *
 * @class skConfig
 * @project Skewer
 * @package kernel
 * @Author ArmiT, $Author: sapozhkov $
 * @version $Revision: 6 $
 * @date $Date: 2012-05-18 10:52:53 +0400 (Пт., 18 мая 2012) $
 *
 * @singleton
 */

class skConfig {
    /**
     * Массив параметров конфигурации
     * @var array
     */
	static private $aLinkStorage = array();

    /**
     * Массив хранилищ веток конфигурации
     * @var array
     */
    static private $aStorages = array();

	/**
     * Экземпляр Config класса
     * @var null|skConfig
     */
    static private $instance = NULL;
	/**
     * Разделитель уровней конфигурации
     * @var string
     */
    static private $pathDelim = '.';

    /**
     * Закрываем возможность вызвать __construct т.к. Singleton
     */
	private function __construct(){}
	
    /**
     * Возвращает экземпляр класса конфигуратора
     * @static
     * @return skConfig
     */
	public static function init() {
		
		if(isset(self::$instance) and (self::$instance instanceof self)){
			return self::$instance;
		}else{
	
			self::$instance= new self();	
			return self::$instance;
		}
	}// func

    /**
     * Загружает настройки из файла
     * @static
     * @param string $sConfigFile Путь к файлу конфигурации
     * @return bool
     */
    public static function getFile($sConfigFile = '') {
		if(!file_exists($sConfigFile)) return false;

        /** @noinspection PhpIncludeInspection */
        self::$aLinkStorage = include($sConfigFile);
		return true;
	}// func

    /**
     * Загружает файл перекрытия настроек
     * @public
     * @static
     * @param string $sConfigFile Путь к файлу конфигурации
     * @return bool
     */
    public static function getOverlayFile($sConfigFile = '') {
        if(!file_exists($sConfigFile)) return false;

        /** @noinspection PhpIncludeInspection */
        $aOverlayConf = include($sConfigFile);

        if(!sizeof($aOverlayConf)) return false;
        self::$aLinkStorage = self::getOverlayArray($aOverlayConf, self::$aLinkStorage);
        return true;
    }// func

    /**
     * Перекрывает один массив другим
     * @static
     * @protected
     * @param array $aOverlayData
     * @param array  $aExistingData
     * @return array|bool
     */
    protected static function getOverlayArray($aOverlayData, $aExistingData) {

        if(!count($aExistingData)) return false;
        if(!count($aOverlayData))  return false;

        foreach($aOverlayData as $mKey => $mItem)
            if(isset($aExistingData[$mKey]) && is_array($aExistingData[$mKey]))
                $aExistingData[$mKey] =  self::getOverlayArray($aOverlayData[$mKey], $aExistingData[$mKey]);
            else {
                $aExistingData[$mKey] = ($aOverlayData[$mKey])? $aOverlayData[$mKey]: $aExistingData[$mKey];
            }

        return $aExistingData;
    }// func

	/**
     * Возвращает значение ключа конфигурации по введенному пути
     * @static
     * @param string $key Путь до параметра конфигурации
     * @return mixed
     */
    public static function get($key = '') {
		
        $pConfig = self::$aLinkStorage;
		if(empty($key)) return $pConfig;

		$key = (strpos($key, self::$pathDelim) === FALSE)? $key: explode(self::$pathDelim,$key);

        if(!is_array($key))
            if(isset($pConfig[$key])) return $pConfig[$key];

        if(is_array($key))
            foreach($key as $item)
              if(isset($pConfig[$item])) $pConfig = $pConfig[$item];
              else return false;

        if(is_array($key)) {
            if(!is_array($pConfig))
                stripslashes($pConfig);
            return $pConfig;
        }

        return false;
	}// func

    /**
     * @static Возвращает параметр конфигурации модуля по ключу
     * @param string $key
     * @param string $sLayer
     * @return mixed
     */
    public static function getModuleParam($key = '', $sLayer = '') {

        $aPaths = array(
            'buildConfig',
            $sLayer,
            'modules'
        );

        $sModuleRootPath = implode(self::$pathDelim, $aPaths);

        return self::get($sModuleRootPath.self::$pathDelim.$key);
    }// func

    /**
     * Устанавливает значение ключа по пути (с возможностью добавить свой (int,str,array,bool))
     * @static
     * @param string $key Путь до параметра конфигурации
     * @param string $value Значение параметра конфигурации
     * @param bool $bForceWrite Принудительно заменять значение ключа.
     * @return bool Возвращает true в случае добавления значения либо false в случае неудачи.
     */
    public static function set($key = '', $value = '', $bForceWrite = true) {

		if(empty($key)) return false;
		
		$key = (strpos($key, self::$pathDelim) === FALSE)? $key: explode(self::$pathDelim,$key);

        /** @noinspection PhpUnusedLocalVariableInspection */
        $value = (is_string($value)) ? addslashes($value): $value;

        if(!is_array($key)){
            $aConfigLink = &self::$aLinkStorage[$key];
        } else {
            $aConfigLink = &self::$aLinkStorage;
            foreach($key as $item)
                $aConfigLink = &$aConfigLink[$item];
        }

        /* жесткая / мягкая вставка */
        if($bForceWrite){
            $aConfigLink = $value;
        } else {
            if(is_array($aConfigLink))
                array_push($aConfigLink, $value);
            else
                $aConfigLink = ($aConfigLink)? array($aConfigLink, $value): $value;
        }
        return true;
        
	}// func

    /**
     * Удаляет ключ конфигурации $key
     * @static
     * @param string $sKey Удаляемый ключ
     * @return bool
     */
    public static function remove($sKey) {
        if(empty($sKey)) return false;

        $sKey = (strpos($sKey, self::$pathDelim) === FALSE)? $sKey: explode(self::$pathDelim,$sKey);

        if(!is_array($sKey)){
            unSet(self::$aLinkStorage[$sKey]);
        } else {
            $aConfigLink = &self::$aLinkStorage;
            foreach($sKey as $item) {
                if($item == end($sKey)) {
                    unSet($aConfigLink[$item]);
                    break;
                }
                $aConfigLink = &$aConfigLink[$item];
            }
        }
        return true;
    }// func

    /**
     * К существующей ветке конфигурации добавляет параметр
     * @static
     * @param string $key Путь по меткам конфигурации
     * @param string $value Значение параметра конфигурации
     * @return bool Возвращает true в случае добавления значения либо false в случае неудачи.
     */
    public static function add($key = '', $value = ''){
        return self::set($key, $value, false);
    }// func



	/**
     * Осуществляет проверку на существование ветки либо ключа конфигурации
     * @static
     * @param string $key Путь до параметра конфигурации
     * @return bool
     */
    public static function isExists($key = '') {
	   
       $pConfig = self::$aLinkStorage;
	   
       if(empty($key))           return false;
       if(isset($pConfig[$key])) return true;	
        
       $key = (strpos($key, self::$pathDelim) === FALSE)? false: explode(self::$pathDelim,$key);
		
		if($key)
		  foreach($key as $item)
		      if(isset($pConfig[$item])) $pConfig = $pConfig[$item];
		      else return false;
        
        return true;   
	}// func

    /**
     * Добавляет новое хранилище с именем $sStorageName и данными $aStorageData
     * @static
     * @param string $sStorageName Название хранилища
     * @param array $aStorageData Данные, добавляемые в хранилище
     * @return bool
     */
    public static function setStorage($sStorageName, $aStorageData = array()) {

        self::$aStorages[$sStorageName] = $aStorageData;
        return self::relinkStorages();

    }// func

    /**
     * Возвращает массив данных хранилища $sStorageName
     * @static
     * @param string $sStorageName Название хранилища
     * @return bool|array Возвращает массив данных либо false
     */
    public static function getStorage($sStorageName) {

        return (isSet(self::$aStorages[$sStorageName]))? self::$aStorages[$sStorageName]: false;

    }// func

    /**
     * Уладяет существующее хранилище $sStorageName из массива конфигурации
     * @static
     * @param string $sStorageName Название хранилища
     * @return bool
     */
    public static function unsetStorage($sStorageName) {

        if(isSet(self::$aStorages[$sStorageName]))
            unSet(self::$aStorages[$sStorageName]);

        return self::relinkStorages();

    }// func

    /**
     * Добавляет в хранилище $sStorageName в ключ $sKey данные $mData
     * @static
     * @param string $sStorageName Имясуществуюего в конфигурации хранилища
     * @param string $sKey Ключ добавляемых данных
     * @param  mixed $mData Добавляемые данные
     * @return bool
     */
    public static function addStorageData($sStorageName, $sKey, $mData){

        if(!isSet(self::$aStorages[$sStorageName])) return false;

        if(!isSet(self::$aStorages[$sStorageName][$sKey])) return false;

        self::$aStorages[$sStorageName][$sKey] = $mData;

        return self::relinkStorages();
    }// func

    /**
     * Пересобирает массив ссылок на основе имеющихся хранилищ
     * @private
     * @return bool
     */
    private static function relinkStorages() {

        if(!count(self::$aStorages)) return false;
        foreach(self::$aStorages as $sStorageName => $aStorageData)
            if($aStorageData)
                foreach($aStorageData as $sKey=>$mValue)
                    self::$aLinkStorage[$sKey] = &self::$aStorages[$sStorageName][$sKey];

        return true;

    }// func

    /**
     * Очищает массив хранилищ и ссылки на них
     * @return bool
     */
    public static function clearStoragesList() {

        self::$aLinkStorage = array();
        self::$aStorages    = array();

        return true;
    }// func

    /**
     * Возвращает используемый в навигаторе разделитель путей
     * @static
     * @return string
     */
    public static function getDelimiter() {

        return self::$pathDelim;
    }// func
}// class
