<?php
/**
 * Logger класс
 * Журналирование
 *
 * @version 1.0001.28.01.11 13:52
 * @date 28.01.11 13:52
 * @author ArmiT(artem.rar@gmail.com)
 */
class skLogger {

    private static $oInstance  = NULL;
    private static $sFileName  = '';

    const logUsers  = 71;
    const logCron   = 72;
    const logSystem = 73;
    const logDebug  = 74;

	private function __construct(){}

    public static function init($filePath) {

        if(isset(self::$oInstance) and (self::$oInstance instanceof self)){

            self::$sFileName = $filePath;
            return self::$oInstance;
        }
        else{

            self::$oInstance= new self();
            self::$sFileName = $filePath;
            return self::$oInstance;
        }

    }// func

    /**
     * @static Вывод информации в файл
     * @return bool
     */
    /*public static function dump($buffer = '') {
        
        $f = fopen(self::$sFileName,"a+");

        if(flock($f, LOCK_EX)) {
            fputs($f, "[".date('r')."]: $buffer\r\n");
            flock($f, LOCK_UN);
            fclose($f);
        }// func
    }*/

    public static function dump() {

        if(!func_num_args()) return false;

        $aArgs = func_get_args();
        return static::write(static::$sFileName, $aArgs);

    }// func

    public static function error() {

        if(!func_num_args()) return false;

        $aArgs = func_get_args();
        return static::write(static::$sFileName, $aArgs);
    }

    protected static function write($sFileName, $aData) {
        $f = fopen($sFileName,"a+");

        if(flock($f, LOCK_EX)) {

            foreach($aData as $mItem) {
                switch(gettype($mItem)) {
                    case 'boolean':
                        fputs($f, "[".date('r')."](bool): $mItem\r\n");
                        break;
                    case 'integer':
                        fputs($f, "[".date('r')."](int): $mItem\r\n");
                        break;
                    case 'double':
                        fputs($f, "[".date('r')."](double): $mItem\r\n");
                        break;
                    case 'string':
                        fputs($f, "[".date('r')."](str): $mItem\r\n");
                        break;
                    case 'array':
                        fputs($f, "[".date('r')."](array): ".print_r($mItem, true)."\r\n");
                        break;
                    case 'object':
                        fputs($f, "[".date('r')."](obj): ".print_r($mItem, true)."\r\n");
                        break;
                    case 'resource':
                        fputs($f, "[".date('r')."](res): ".print_r($mItem, true)."\r\n");
                        break;
                    case 'NULL':
                        fputs($f, "[".date('r')."](null): $mItem\r\n");
                        break;
                    default:
                        fputs($f, "[".date('r')."](unknown): ".print_r($mItem, true)."\r\n");
                        break;
                }// switch
            }// each param
            flock($f, LOCK_UN);
            fclose($f);
        }// if
        return true;
    }//

    /**
     * @static Выводит данные в консоль
     * TODO Вынести в дебаггер
     */
    public static function display() {

        $mData = func_get_args();
        $uid = rand(0,1000);
        $code = 'iVBORw0KGgoAAAANSUhEUgAAADgAAAA4CAYAAAHfgQuIAAAACXBIWXMAAAsTAAALEwEAmpwYAAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADqYAAAOpgAABdvkl/FRgAAB7BJREFUeNpUjTEOwjAQBGct8wTISSl8//8SKS4VprNEaHMUIUjUOzOrZbmnmwCInsw3cSki+k51K0ASPWlWEBB9B6CehptAsPYECZ9EjedhAMTjMNwK452U+cpfyr/geCXlPP8NOkA3UaPvSKJNYmzJ2JJmAsQHAAD//3SQMQqAMBAEZw/foIJF8v83WcRK0opgZWKRhCBiPRxzs7qONX+u6/f2B9ws7O1TzTKEGFpT8aWeFTPmJ4EKXEYhFY3KQpmwJ9wshAj7DYCbwLYIvoGY+0InPAAAAP//fJKxDoQwDENfqvIJJySW+/+/grEbSDDnhsSFSuiyWnYcx/2UmHCq0Yc0Mg9QAxgJiveNoK/2ZIMQiQLsl3OcvIrtl1PNjOUDUyl/7JZhu5lRv/PjhubgnpYilC7WuLFZsT42jATYMk08y6lw1kZvj9TVolEs+5jYDwAA//+clEsOxCAMQ+2IO9Adc/8rjSq1O05BuqBAShGDZm05mJcP9/2rwcuQYA/FfsUtmzpN/jF9NsnGh0DWsZ4VdJVaEbwlmkw9CxAQQGt1G++MKe8lCRIP0xETnN5dCxuXIJWEDtqOyyokkpAqsO3c7KVXO87XS3ZHE7RBR/AsR3IWr81qHvB8muQnCHKg3eeqSD3ykq3v4RETZGoCQOhQuwAAAP//lFZBbsMwDBOD/iE7tf9/U5E/pFh6G8YdLMmy7LhZTkVTlrZEisK2PXlfl+BAOZ03+cnevYLDz7Exf5l93ABcfT3RHAebONcATTMA7+q/cBIIZ6XLOeBkqEnTlEDfd7jofgBCck4UnoL7rbgPRE4YSSx3HqsMe+QD3zVfSSCUu20Agxv7umIndO2jJKV7VigUXWNySVNaDtUaSg6o9Rsx2MmVMUZYL4agViupQrMmAAhL2JQPsJOjqO51hEvOiMKt9zdlP068pIPgVrtVfjyT95nR9zdl/57gVtE8ZrQFr9siTRQAnTcrrhXgLaaHWOcaeceetvsMtFxMKi/LUlY6dXo3YhHNx2XUiG5AsBt5S9RbOKTtT9S4UOM+vpD+pYK7SWR2Cvt0d4oB7o8Ta8mOEAiBFNMewSz0/rfKvDfu4iVCFtoRaOyP63kMUJRV0Hi/vyXxAQMi6CqSN71A6xyLRBgHUEpMtM4oICBCR7IBazpJxYsJ6u9smQf9U4SmFygke+0TyZ1h0KxzHHfDUcBoU25NRGD9sAeOoZndzJqfBOmu+1jsNsyhZHDXUzPOFsoft0Q2OztFYjuvWxMazjro0ItuK8gSBFfWxqy3XRRLga4K8w4kWk/PgltxQkQJhuFCqYeN2VkUMpr6NRazKEQTg5YvhE5viCVixlATA3NoAEeHAlDiw/+cvxTH/5CmqsM5J2WhwyzXWVmTpv7DZJtKhvNiwvGAQjZIQbXtz5PFkUJ8pyDbLsUmXlOQ6PWD3IKROBBvErkqDAShVBApOvPbvB4bVyt0VcYw1hEREZqYBt1ixOEbRQ64RUmAPgGP3OKB6hNdAq5lsb5g5eeIp2wE3G7a0uJ8W8B+h9KjIO4SzrZVK/Lz82tJk4P/xSC4F7yA62TJi0iwBbLH3gTdJdO/ZWsqkiliabfQf5rCoMa7C2qzxv0j5pNk2dK6132A/mi1duxIQhioanMDf4L1/U9lJ3Y22fitna4cDGKEECC6eyey571B6ENJVYD39zdGjr+gAaqx3zIhCreu0UdP7+gqBBSzZ2tN/Z9kMkDjiA+ZEeEp4ljcqYA9CyOqDae6TNgl1+dlbLQ2nW7v9RkWZnxaNDsXM0Mvj6IwUNC5Y47d7DnNd/XAdz8aixc/RwLpgU86NYLZMR3BesTlrnZ3tELS5lfHktodOWPogQDyTK9I3fWH6esvE0+CZkEEyAyoQv1OW5HIflyI+BB4GEMFT8wGsxhbBljHgV32/D58V/PFmIzkI6MSwV7Dbdom/Avikswh3nCVsZVTff3OQjCr6woGGia6u4/xffs+44N/VRLUJoZgJVecsjCbiFohuob7uEFdirVEfB+dvZEibUQvj32FfeiYWjvB6B5yNSPOCaEDrTXpBhW5Aybm+z2Tjrx6YWdcS7UogpLFJHD/kMc2jji3r72AiJJp0PEK+Ufc2ReKFqSi2ahdZTS1asrd4aPtZfWMXb+Zrj/tOW/YprQJjGQ8oKWbqg98XKir4OyZPFbOtGUQHj3MNyPbGApNP5OLQdC6czV5RKixS3th7l/L6jzqFKRpeXC9oMjN2J2xya/VxpmIPi/zAAJt7m59WrGJGYRoaRtKNltjE5hEEl0pnTpTD8yElDZU7SXNYlqESxPZOLGN5ZvlTPcY+6gsc5+u24u+8HWVm2N8LG2BXqb2MrqKG42Nrb26Ek5mE3ukCFrKmI/Cnn5UHDyesT/Pa9JRfzathci4XIuulpQax0w/md9SIEyzLJvw5Nxq8ICcYFbsRTvUqXc1gKQx3OrvuOiSlrYgmDFWaFi2qP62D6lEznx92oaI6zlaPdXuOacXusGvFo8oRFZdKcIO3nkd7XCZTR/gwDbC9ppnM3ahkQI9ci6qsaB65+Mo3qH67w/5CfDPUPLEo/+girEw9gL3K7Rszl6S5n8y4tRPhE5QnqfSB8IlWNkDpmLV7wC+apngQpxlRQAAAABJRU5ErkJggg==';
        echo '<div style=" cursor: pointer; position: fixed; z-index: 1000; top: 5px; left: 90%; background-color: #333; color: white; padding: 3px; border-radius: 5px;" onclick="$(\'#'.$uid.'_-console\').toggle();"><b>Console [X]</b></div>';
        echo '<div id="'.$uid.'_-console" style=" display: none; border-bottom: 1px solid black; position: fixed; z-index: 100; top: 0; left: 0; width: 100%; height: 100%; overflow: auto; background: url(\'data:image/png;base64,'.$code.'\') white repeat;">';
        echo '</ hr> dump start </ hr>';
        foreach($mData as $mItem)
            var_dump($mItem);
        echo '</ hr> dump end </ hr>';
        echo '</div>';

    }// func

    public static function getLog(){

    }

    /**
     * @static Метод добавления записи определенного типа в лог
     * @param $sTitle
     * @param $sDescription
     * @param $sModule
     * @param $iEventType
     * @param $iLogType
     * @return bool|int
     */
    public static function addToLog( $sTitle, $sDescription, $sModule, $iEventType, $iLogType ){

        if ( !skConfig::get('log.enable') ) return true;

        switch( $iLogType ){

            case self::logUsers:

                if ( !skConfig::get('log.users') ) return true;
            break;

            case self::logSystem:

                if ( !skConfig::get('log.system') ) return true;
            break;

            case self::logCron:

                if ( !skConfig::get('log.cron') ) return true;
            break;

            case self::logDebug:

                if ( !skConfig::get('log.debug') ) return true;
            break;
        }

        $aData = array();
        $aData['event_type'] = $iEventType;
        $aData['log_type'] = $iLogType;
        $aData['title'] = $sTitle;
        $aData['module'] = $sModule;
        $aData['initiator'] = CurrentAdmin::getId();
        $aData['ip'] = $_SERVER['REMOTE_ADDR'];
        $aData['proxy_ip'] = ( isset($_SERVER['HTTP_X_FORWARDED_FOR']) )? $_SERVER['HTTP_X_FORWARDED_FOR']: '';
        $aData['external_id'] = '';
        $aData['description'] = $sDescription;

        return skLoggerMapper::saveItem( $aData );
    }

    /**
     * @static Метод добавления отчета о критическом действии
     * @param $sTitle Наименование события
     * @param $sDescription Полное описание события
     * @param $iLogType Тип журнала событий
     * @param $sCalledClass Класс, вызвавший исключение
     * @param $sTitleCalled Название модуля, инициировавшего добавление записи в лог
     * @return bool|int
     */
    public static function addCriticalReport( $sTitle, $sDescription, $iLogType, $sCalledClass, $sTitleCalled = '' ){

        if ( $sTitleCalled )
            $sTotalTitle = implode(':', array($sCalledClass, $sTitleCalled));
        else $sTotalTitle = $sCalledClass;

        return self::addToLog($sTitle, $sDescription, $sTotalTitle, 1, $iLogType);
    }

    /**
     * @static Метод добавления предупреждения
     * @param $sTitle Наименование события
     * @param $sDescription Полное описание события
     * @param $iLogType Тип журнала событий
     * @param $sCalledClass Класс, вызвавший исключение
     * @param $sTitleCalled Название модуля, инициировавшего добавление записи в лог
     * @return bool|int
     */
    public static function addWarningReport( $sTitle, $sDescription, $iLogType, $sCalledClass, $sTitleCalled = '' ){

        if ( $sTitleCalled )
            $sTotalTitle = implode(':', array($sCalledClass, $sTitleCalled));
        else $sTotalTitle = $sCalledClass;

        return self::addToLog($sTitle, $sDescription, $sTotalTitle, 2, $iLogType);
    }

    /**
     * @static Метод добавления отчета об ошибке
     * @param $sTitle Наименование события
     * @param $sDescription Полное описание события
     * @param $iLogType Тип журнала событий
     * @param $sCalledClass Класс, вызвавший исключение
     * @param $sTitleCalled Название модуля, инициировавшего добавление записи в лог
     * @return bool|int
     */
    public static function addErrorReport( $sTitle, $sDescription, $iLogType, $sCalledClass, $sTitleCalled = '' ){

        if ( $sTitleCalled )
            $sTotalTitle = implode(':', array($sCalledClass, $sTitleCalled));
        else $sTotalTitle = $sCalledClass;

        return self::addToLog($sTitle, $sDescription, $sTotalTitle, 3, $iLogType);
    }

    /**
     * Метод добавления уведомления
     * @static
     * @param string $sTitle Наименование события
     * @param string|array $mDescription Полное описание события
     * @param int $iLogType Тип журнала событий
     * @param string $sCalledClass Класс, вызвавший добавление
     * @param string $sTitleCalled Название модуля, инициировавшего добавление записи в лог
     * @return bool|int
     */
    public static function addNoticeReport( $sTitle, $mDescription, $iLogType, $sCalledClass, $sTitleCalled = '' ){

        if ( $sTitleCalled )
            $sTotalTitle = implode(':', array($sCalledClass, $sTitleCalled));
        else $sTotalTitle = $sCalledClass;

        // приведение к нужному виду
        $mDescription = self::buildDescription($mDescription);

        return self::addToLog($sTitle, $mDescription, $sTotalTitle, 4, $iLogType);
    }

    /**
     * Форматирование данных в строку
     * @static
     * @param mixed $mDescArray
     * @return string
     */
    public static function buildDescription($mDescArray){

        // приведение к типу описания
        if ( is_array($mDescArray) ) {
            $mDescription = json_encode($mDescArray);
        } else {
            $mDescription = (string)$mDescArray;
        }

        return $mDescription;

    }
} // class
