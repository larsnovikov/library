<?php
/**
 * Организует отправку данных клиенту
 * @class skResponse
 *
 * @author ArmiT, $Author: acat $
 * @version $Revision: 1614 $
 * @date $Date: 2013-01-28 18:36:46 +0400 (Пн, 28 янв 2013) $
 * @project JetBrains PhpStorm
 * @package kernel
 */
class skResponse {

    /**
     * Экземпляр skResponse класса
     * @var null|\skResponse
     */
    static private $oInstance = NULL;

    private function __construct() {}// func


    public static function init() {

        if(isset(self::$oInstance) and (self::$oInstance instanceof self)){

            return self::$oInstance;
        }else{

            self::$oInstance= new self();

            return self::$oInstance;
        }
    }// func

    public static function sendAuthHeaders(){

        header("HTTP/1.1 200");
        header("Status: 200 OK");
        header('Content-Type: text/html;  charset=utf-8');

    }// func

    public static function send404Headers(){
        header("HTTP/1.1 404");
        header("Status: 404 Not Found");
        header('Content-Type: text/html;  charset=utf-8');

    }// func

    public static function sendRedirectHeaders($sLocation = '/'){
        header("HTTP/1.1 301");
        header("Status: 301 Moved Permanently");
        header("Location: $sLocation");

    }// func

    public static function sendFoundHeaders($sLocation = '/'){
        header("HTTP/1.1 302");
        header("Status: 302 Found");
        header("Location: $sLocation");

    }// func

    public static function sendHeaders(){
        header("HTTP/1.1 200");
        header("Status: 200 OK");
        header('Content-Type: text/html;  charset=utf-8');

    }// func
    
    public static function sendDiagnosticsHeaders() {
        header("HTTP/1.1 503 Service Unavailable");
        header("Retry-After: 3600");
    }// func    
}// class
