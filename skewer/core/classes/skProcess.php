<?php
/**
 * Прототип процесса
 * @class skProcess
 * @author ArmiT, $Author: acat $
 * @version $Revision: 1172 $
 * @project Skewer
 * @date $Date: 2012-11-12 15:17:31 +0400 (Пн., 12 нояб. 2012) $
 * @package kernel
 */

class skProcess {
     /**
     * Ссылка на контекст вызова процесса
     * @var skContext
     */
    protected $oContext = NULL;

    /**
     * Экземпляр запускаемого модуля
     * @var null|\skModule
     */
    protected $oObject = NULL;
    /**
     * Статус процесса
     * @var int
     */
    protected $iStatus = 0;
    /**
     * Флаг готовности дерева процессов
     * @var bool
     */
    protected $bProcessTreeComplete = false;
    /**
     * Счетчик количества выполнений
     * @var int
     */
    protected $iExecCount = 0;
    /**
     * Вывод после парсинга
     * @var string
     */
    protected $sOut = '';
    /**
     * Локальный путь к шаблону
     * @var string
     */
    public $template = '';
    /**
     * Массив дочерних процессов
     * @var array
     */
    public $processes = array();
    /**
     * Экземпляр Реврайтера для текущего процесса
     * @var null|skRouter
     */
    public $oRouter = NULL;
    /**
     * Разделитель пути в дереве процессов
     * @var string
     */
    private $sDelimiter = '.';
    
    /**
     * Конструктор нового процесса
     * @param skContext $oContext
     * @return skProcess
     */
    public function __construct(skContext $oContext) {

        $this->oContext  = $oContext;
        $this->oContext->oProcess = &$this;

        skProcessor::registerProcessPath($this->oContext->sLabelPath, $this);

    }// func

    /**
     * Запускает выполнение модуля
     * @return int Возвращает статус отработки процесса
     */
    public function execute() {

        if(!($this->oObject instanceof $this->oContext->sClassName)) {
            //$this->oContext->
            $this->oObject =  new $this->oContext->sClassName($this->oContext);

            $this->oRouter = new skRouter($this->oContext->sURL, $this->oContext->aGet);

            //@todo Добавить кеширование паттернов
            $aDecodedRules = $this->oRouter->getRulesByClassName($this->oContext->sClassName);
            if(!$this->oRouter->getParams($aDecodedRules)) {
                skProcessor::setPage(page404);

                return psExit;
            }

            $this->oContext->sURL = $this->oRouter->getURLTail();
            $this->oContext->aGet = $this->oRouter->getURLParams();

        }

        if(!$this->oObject->allowExecute()) return psError;

        $this->oObject->init();
        $this->iStatus = $this->oObject->execute();
        $this->iStatus = ($this->iStatus)? $this->iStatus: psError;
        $this->oObject->shutdown();

        return $this->iStatus;
    }// func
    
    /**
     * Запускает на выполнение дерево процессов
     * @deprecated Удалить Если не используется
     * @return bool|int Возвращает статус отработки процесса либо false в случае ошибки
     */
    public function executeProcessList() {
        $aQueue = array();
            
        $aQueue[] = &$this;

        $bComplete = false;
        $this->bProcessTreeComplete = false;


        while (!$bComplete) {

            $bComplete = true;
            $iProsessCounter = count($aQueue);

            foreach ($aQueue as $oProcess) {
                /* @var $oProcess skProcess */
                switch ($oProcess->getStatus()) {

                    case psNew:
                    case psWait:

                        $iStatus = $oProcess->execute();

                        switch ($iStatus) {

                            case psComplete:

                                if(count($oProcess->processes)){
                                    foreach ($oProcess->processes as &$oChildProcess)
                                        /* @var $oChildProcess skProcess */
                                        if($oChildProcess->getStatus() == psNew)
                                            $aQueue[] = $oChildProcess;

                                    $bComplete = false; // появились новые процессы
                                }
                                break;

                            case psError:

                                return false;
                                break;

                            case psExit:

                                return false;
                                break;

                            case psWait:
                                $bComplete = false;// Текущий процесс не отработал
                                break;

                        }// switch states after execution
                }// switch states before execution
            }// each process

            if ($iProsessCounter == count($aQueue)) $this->bProcessTreeComplete = true;
        }// process loop

        return psComplete;
        
    }// func
    
    /**
     * Производит парсинг данных в шаблон
     * @return bool|string
     */
    public function render() {

        if(count($this->processes))
            foreach ($this->processes as $sLabel => &$oChildProcess) {
                
                /* @var $oChildProcess skProcess */

               // если процесс отработал и парсер не JSON
               // заводим метку вывода в массиве данных
               if($oChildProcess->getStatus() == psComplete and ($oChildProcess->oContext->getParser() != parserJSON ) )
                   $this->oContext->setData($sLabel, $oChildProcess->render());
               else $oChildProcess->render();
            }
        
        if($this->getStatus() == psComplete){
            $this->sOut = skParser::render($this->oContext);
            $this->setStatus(psRendered);
            $this->oContext->clearData();
        }
        return $this->sOut;
    }// func
    
    /**
     * Не используется
     * @deprecated Проверить использование
     */
    public function wasCalled() {

    }// func
    
    /**
     * Не используется
     * @deprecated Проверить использование
     * @param string $sPath
     */
    public function reCall($sPath) {

    }// func

    /**
     * Добавялет дочерний процесс на выполнение
     * @param skContext $oContext Контекст Добавляемого процесса
     * @return \skProcess|bool Возвращет Экземпляр созданного процесса либо false в случае ошибки
     */
    public function addChildProcess(skContext $oContext) {

        // если место под процесс занято - удаляем процесс с его детьми
        if (isset($this->processes[$oContext->sLabel]))
            $this->removeChildProcess($oContext->sLabel);

        $oContext->sURL         = $this->oContext->sURL;
        $oContext->sLabelPath   = $this->oContext->sLabelPath.$this->sDelimiter.$oContext->sLabel;
        skProcessor::resetProcessTreeComplete();
        $oContext->setParentProcess($this);
        return $this->processes[$oContext->sLabel] = new self($oContext);
        
    }// func
    
    /**
     * Получить массив данных из модуля
     * @return array Возвращает массив данных отработанного процесса
     */
    public function getData($sLabel = '') {
        
        return $this->oContext->getData($sLabel);
    }// func
    
    /**
     * Добавить данные для парсинга в метку
     * @depricate Проверить использование и удалить если не нужен
     * @param $sLabel
     * @param $mData
     * @return void
     */
    public function setData($sLabel, $mData) {
        $this->oContext->setData($sLabel, $mData);
    }// func

    /**
     * Возвращает статус процесса
     * @return integer
     */
    public function getStatus() {
        return $this->iStatus;
    }// func

    /**
     * Устанавливает статус процессу
     * @param integer $iStatus Константа статуса
     * @return integer
     */
    public function setStatus($iStatus = psComplete) {
        return $this->iStatus = $iStatus;
    }// func

    /**
     * Возвращает отрендеренные данные процесса
     * @return string
     */
    public function getOut() {
        return $this->sOut;
    }// func

    /**
     * Возвращает имя класса модуля
     * @return string
     */
    public function getModuleClass() {
        return $this->oContext->sClassName;
    }// func

    /**
     * Возвращает путь по меткам вызова до текущего процесса
     * @return string
     */
    public function getLabelPath() {
        return $this->oContext->sLabelPath;
    }// func

    /**
     * Возвращает метку вызова текущего процесса
     * @return string
     */
    public function getLabel() {
        return $this->oContext->sLabel;
    }// func

    /**
     * Выполняет метод $sMethodName модуля с аргументами $aArguments
     * @param string $sMethodName Имя метода класса модуля
     * @param array $aArguments Аргументы, передаваемые модулю
     * @return int Возвращает статус выполнения процесса.
     */
    public function executeModuleMethod($sMethodName, $aArguments) {

        return $this->setStatus($this->oObject->$sMethodName($aArguments));

    }// func

    /**
     * Установить значение параметру модуля
     * @param string $sParamName Название параметра
     * @param mixed $mValue Значение параметра
     * @return mixed Возвращает установленное значение.
     * @todo Добавить проверку на существование параметра класса
     */
    public function setParam($sParamName, $mValue) {

        return $this->oObject->$sParamName = $mValue;

    }// func

    /**
     * Обновляет значения параметров модуля
     * @param array $aParams
     * @return bool
     */
    public function updateParams($aParams) {

        $this->oContext->setParams($aParams);
        $this->oObject->overlayParams($aParams);

        return true;
    }// func

    /**
     * Жесткая вставка.
     * Установить во входящие параметры новое значение. Значение $mValue устанавливается в
     * параметр $sParamName. Изменению подвергается POST, GET, REQUEST
     * @param string $sParamName Название параметра
     * @param mixed $mValue Значение параметра
     * @return bool Возвращает true, если значение установлено либо false в противном случае
     */
    public function setRequest($sParamName, $mValue) {

        if(skRequest::set($this->oContext->sLabelPath, $sParamName, $mValue, true))
            return true;

        // проверка на существование роутера
        if($this->oRouter)
            if($this->oRouter->set($sParamName, $mValue, true))
                return true;

        return false;
    }// func

    /**
     * Мягкая вставка.
     * Добавить во входящие параметры новое значение. Значение $mValue устанавливается в
     * параметр $sParamName. Изменению подвергается POST, GET, REQUEST в том случае, если
     * параметр отсутствовал.
     * @param string $sParamName Название параметра
     * @param  mixed$mValue Значение параметра
     * @return bool Возвращает true, если значение установлено либо false в противном случае
     */
    public function addRequest($sParamName, $mValue) {

        if(skRequest::set($this->oContext->sLabelPath, $sParamName, $mValue, false))
            return true;


        if($this->oRouter) // проверка на существование роутера (роутер существует только в page процессоре)
            if($this->oRouter->set($sParamName, $mValue, false))
                return true;

        return false;
    }// func

    /**
     * Возвращает ссылку на класс родительского процесса
     * @return null|skProcess
     */
    public function getParentProcess() {

        return $this->oContext->getParentProcess();

    }// func

    /**
     * Удалить Дочерний процесс в метке вызова $sLabel
     * @param string $sLabel Метка вызова для дочернего процесса.
     * @return bool
     */
    public function removeChildProcess($sLabel) {

        if($oProcess = $this->processes[$sLabel]){
            /**@var $oProcess skProcess */

            foreach($oProcess->processes as $sChildLabel=>$oChildProcess)
                $this->removeChildProcess($sChildLabel);

            unSet($oProcess);
            skProcessor::unregisterProcessPath($this->getLabelPath().$this->sDelimiter.$sLabel);
            skProcessor::resetProcessTreeComplete();
            unSet($this->processes[$sLabel]);

            return true;
        }
        return false;
    }// func

    /**
     * Отдает ссылку на модуль
     * @return skModule
     */
    public function getModule() {
        return $this->oObject;
    }

}// class
