<?php
/**
 *
 * @class skTasks
 *
 * @author ArmiT, $Author: sapozhkov $
 * @version $Revision: 6 $
 * @date $Date: 2012-05-18 10:52:53 +0400 (Пт., 18 мая 2012) $
 * @project Skewer
 * @package kernel
 *
 * @example
 * skTasks::add('Добавление чего-то', 'cmd cmd', );
 */
class skTasks {

    private static $oInstance    = null;
    private static $iTargetArea  = 0;
    private static $oTasksMapper = null;

    private function __construct($iTargetArea) {

        self::$iTargetArea = $iTargetArea;
        self::$oTasksMapper = new skTasksMapper();
    }// constructor

    public static function init($iTargetArea = taskTargetSite) {

        if(isset(self::$oInstance) and (self::$oInstance instanceof self)) {

            return self::$oInstance;
        } else {

            self::$oInstance= new self($iTargetArea);
            return self::$oInstance;
        }
    }// func

    public static function add($sCmd, $sTaskTitle, $iPriority = taskPriorityLow, $iResourceUse = taskWeightLow) {



    }// func

    protected function getMutex() {
        return self::$oTasksMapper->getMutex();
    }// func
}// class
