<?php
/**
 * Прототип Менеджера процессов
 * @class skProcessor
 * @project Skewer
 * @package kernel
 * @author ArmiT, $Author: acat $
 * @version $Revision: 1671 $
 * @date $Date: 2013-02-06 15:38:54 +0400 (Ср, 06 фев 2013) $
 *
 */ 
class skProcessorPrototype implements skProcessorInterface {

    /**
     * Массив процессов, которые надо обработать
     * @var array
     */
    protected $aProcessList = array();

    /**
     * Результат работы - код страницы после обработки шаблонизатором
     * @var string
     */
    protected $sOut = '';

    /**
     * Массив путей по меткам к процессам
     * @var array
     */
    public $aProcessesPaths = NULL;

    /**
     * Флаг готовности дерева процессов
     * @var bool
     */
    private $bProcessTreeComplete = false;

    /**
     * Экземпляр роутера. Требуется для получения id раздела из URL
     * @var null | skRouter
     */
    public $oRouter = null;

    /**
     * Запрашиваемый URL
     * @var string
     */
    protected $sRequestURL = '';

    /**
     * Остаток URL после получения Id раздела
     * @var string
     */
    protected $sURL = '';

    /**
     * массив GET-параметров
     * @var array
     */
    protected $aGet = array();

    /**
     * Id запрашеваемого раздела
     * @var int
     */
    protected $iPageId = 0;

    /**
     * Содержит массив ответов, полученных от модулей
     * @var array
     */
    protected $aJSONResponse = array();

    /**
     * Содержит массив заголовков JSON-запроса
     * @var array
     */
    protected $aJSONHeaders = array();

    /**
     * Переменные окружения
     * @var array
     */
    protected $aEnvParams = array();

    /**
     * @var array
     */
    protected $aLayers = array('Page','Cms','adm','Design');

    /**
     * Массив зависимых слоев. Нужно для межслойного вызова хуков, например в совместной работы Cms и adm.
     * @var array
     */
    protected $aDependLayers = false;

    /**
     * Хранит в себе экземпляр объекта доступа к настройкам дизайна для парсера
     * @var null
     */
    //protected $oDesign = NULL;

    /**
     * Запускает на выполнение корневой процесс, выводит результат работы дерева процессов
     * @static
     * @return bool|string
     */
    public function build() { }// func

    /**
     * @return array
     */
    public function getLayers(){

        return $this->aLayers;
    }

    /**
     * Возвращает процесс по пути из меток от корневого процесса page
     * @param string $sPath  Путь от корневого процесса до искомого
     * @param integer $iStatus Статус искомого процесса
     * @return bool|skProcess
     */
    public function getProcess($sPath, $iStatus = psComplete) {

        if(isSet($this->aProcessesPaths[$sPath])) {

            if($iStatus == psAll)
                return $this->aProcessesPaths[$sPath];

        /** @noinspection PhpUndefinedMethodInspection */
            if(($iStatus == $this->aProcessesPaths[$sPath]->getStatus()) !== false)
                return $this->aProcessesPaths[$sPath];

        }
        return ($this->bProcessTreeComplete)? psNotFound: psWait;
    }// func

    /**
     * Управление потоком выполнения процессов
     * @param integer $iPage Константа страницы редиректа
     * @return bool
     */
    public function setPage($iPage = page404) { }// func

    /**
     * Восстанавливает список путей к процессам
     * @param array $aProcessList Список процессов
     */
    public function recoverProcessPaths(&$aProcessList){
            foreach ($aProcessList as $oChildProcess){
                // восстанавливаем массив с путями

                /** @var $oChildProcess skProcess */
                $this->aProcessesPaths[$oChildProcess->getLabelPath()] = $oChildProcess;

                if(count($oChildProcess->processes))
                    $this->recoverProcessPaths($oChildProcess->processes);
            }
            $this->bProcessTreeComplete = true;
    }

    /**
     * Запускает на выполнение дерево процессов
     * @return bool|int
     */
    public function executeProcessList() {

        $bComplete = false;

        if($RequestEvents = skRequest::getRequestEventsList()){
            foreach($RequestEvents as $sEventName=>$aEventArguments)
                skProcessor::fireEvent($sEventName, $aEventArguments);

            skRequest::removeRequestEvents();
        }

        while (!$bComplete) {
            $bComplete = true;
            $this->bProcessTreeComplete = true;

            foreach ($this->aProcessesPaths as $oProcess) {

                /* @var $oProcess skProcess */
                switch ($oProcess->getStatus()) {

                    case psNew:
                    case psWait:

                            $iStatus = $oProcess->execute();

                        switch ($iStatus) {

                            case psComplete:

                                if(count($oProcess->processes)){

                                    /** @var skProcess $oChildProcess */
                                    foreach ($oProcess->processes as $oChildProcess)
                                        if($oChildProcess->getStatus() == psNew){
                                            $bComplete = false; // появились новые процессы
                                        }


                                }
                                break;

                            case psError:
                            case psExit:
                            case psReset:

                                return $iStatus;
                                break;

                            case psWait:
                                $bComplete = false;// Текущий процесс не отработал
                                break;

                        }// switch states after execution
                }// switch states before execution
            }// each process

            if($this->bProcessTreeComplete)
                if($bComplete)
                    $bComplete = !skProcessor::ExecuteEvents();



        }// while process loop
        return psComplete;

    }// func

    /**
     * Добавляет новый процесс в очередь выполнения.
     * @param skContext $oContext Контекст создаваемого процесса
     * @return array|bool
     */
    public function addProcess(skContext $oContext) {
        // Место занято - выходим
        if (isset($this->aProcessList[$oContext->sLabel])) return false;

        $oContext->sURL         = $this->sURL;
        $oContext->sLabelPath   = $oContext->sLabel;
        $oContext->aGet         = $this->aGet; // @todo попытка открыть доступ к GET параметрам в модулях

        /** @noinspection PhpUndefinedMethodInspection */
        skProcessor::resetProcessTreeComplete();

        return $this->aProcessList[$oContext->sLabel] = new skProcess($oContext);

    }// func

    /**
     * Добавляет JSON ответ от модуля во внутреннее хранилище для дальнейшей упаковки
     * @param skContext $oContext Контекст отработанного модуля
     * @return bool
     */
    public function addJSONResponse(skContext $oContext) {

        $aModuleResponse = $oContext->getJSONHeadersList();

        $aModuleResponse['path']       = $oContext->sLabelPath;
        $aModuleResponse['params']     = $oContext->getData();
        $aModuleResponse['className']  = $oContext->sClassName;
        $aModuleResponse['moduleName'] = $oContext->getModuleName();
        $aModuleResponse['moduleDir']  = $oContext->getModuleWebDir();
        $aModuleResponse['moduleLayer']= $oContext->getModuleLayer();

        $aModuleResponse['cmd'] = (isset($aModuleResponse['params']['cmd']) and $aModuleResponse['params']['cmd'])? $aModuleResponse['params']['cmd']: false;

        $this->aJSONResponse['data'][] = $aModuleResponse;

        return true;

    }// func

    /**
     * Регистрирует процесс в списке процессов
     * @param string $sLabelPath Путь по меткам вызова до регистрируемого процесса
     * @param skProcess $oProcess Ссылка на процесс
     * @return bool
     */
    public function registerProcessPath($sLabelPath, skProcess $oProcess) {
        $this->aProcessesPaths[$sLabelPath] = $oProcess;
        return true;
    }


    public function unregisterProcessPath($sLabelPath) {

        unSet($this->aProcessesPaths[$sLabelPath]);
        return true;
    }// func

    /**
     * Добавляет Идентификатор сессии в JSON ответ
     * @param string $sSessionId хеш-ключ сессии
     * @return bool
     */
    public function addSessionId($sSessionId) {
        $this->aJSONResponse['sessionId'] = $sSessionId;
        return true;
    }// func


    /**
     * Добавяляет статус и сообщение отработки JSON пакета
     * @param string $sMessage Сообщение отработки пакета
     * @param bool $bError Отработано с ошибкой либо нет
     * @return bool
     */
    public function addResponseStatus($sMessage, $bError = false) {
        $this->aJSONResponse['message'] = $sMessage;
        $this->aJSONResponse['success'] = $bError;
        return true;
    }// func

    public function resetProcessTreeComplete() {
        $this->bProcessTreeComplete = false;
        return true;
    }// func

    public function removeProcess($sLabelPath){

        if($oProcess = $this->getProcess($sLabelPath)) {

            /** @noinspection PhpUndefinedMethodInspection */
            skProcessor::resetProcessTreeComplete();

            // удаляем детей
            if ( ($oProcess instanceof skProcess) )
                if(($oParentProcess = $oProcess->getParentProcess()) instanceof skProcess) {
                    /** @var $oParentProcess skProcess */
                    $oParentProcess->removeChildProcess($oProcess->getLabel());
                    return true;
                }
                else {
                    foreach($oProcess->processes as $sChildLabel=>$oChildProcess)
                        $oProcess->removeChildProcess($sChildLabel);

                    skProcessor::unregisterProcessPath($sLabelPath);
                    skProcessor::resetProcessTreeComplete();
                    unSet($oProcess);
                }

            $this->unregisterProcessPath($sLabelPath);
            $this->aProcessList[$sLabelPath] = null;
            unSet($this->aProcessList[$sLabelPath]);

            return true;
        }

        return false;

    }// func

    public function setEnvParam($sName, $mValue) {
        return $this->aEnvParams[$sName] = $mValue;
    }// func

    public function getEnvParam($sName, $mDefault = false) {
        return (isSet($this->aEnvParams[$sName]))? $this->aEnvParams[$sName]: $mDefault;
    }// func

    public function getEnvParamsList() {
        return $this->aEnvParams;
    }// func
    
    /**
     * Проверяет режим работы процессора
     */
    public function isAllowedStart(){
        return SysVar::get('ProcessorEnable');
    
    }// func    

    /**
     * Занимается инициализацией окружения перед выполнением
     */
    public function init() {
        
        /* Проверяем режим работы - если процессоры выключены, говорим клиенту об этом и завершаем работу */
        if($this->isAllowedStart()==='0') {
            skResponse::sendDiagnosticsHeaders();
            echo skParser::parseTwig(skConfig::get('page.503'), array());
            exit;
        }        
        

        skFiles::init(FILEPATH, PRIVATE_FILEPATH);

        /* @todo после переделывания авторизации добавить проверку на debug режим по администратору */

        if(!$this->aDependLayers) $this->aDependLayers[] = skProcessor::getLayerName();

        /* Авторизованы в режиме дизайнера или под админом - включаем режим отладки */
        if(isSet($_SESSION['auth']['admin']['userData']) and skProcessor::getLayerName()=='Page') {

            skConfig::set('debug.config', true);
            skConfig::set('debug.parser', true);
            skConfig::set('debug.css'   , true);
        }

        /* Режим отладки дляшаблонизатора */
        (skConfig::get('debug.parser'))? skTwig::enableDebug(): skTwig::disableDebug();

        /* Режим отладки для системы */
        if(skConfig::get('debug.config')) {

            /*
             * debug:
             * 1. Читаем реестр
             * 2. Обходим модули
             * 3. Читаем их конфиги
             * 4. Добавляем в конфигуратор
             * 5. Сторим
             */

            if(! $aBuildConfig = skRegistry::getStorage('build_'.BUILDVERSION)) {
                $aBuildConfig['buildConfig'] = '';
            }

            skConfig::setStorage('buildConfig', $aBuildConfig);

//            var_dump(skConfig::get());

            if(! $aSiteConfig = skRegistry::getStorage('siteConfig')){
                skRegistry::saveStorage('siteConfig', '');
            }

            skConfig::setStorage('siteConfig','');


            $aBuildConfig = skConfig::get('buildConfig');

            skConfig::remove('buildConfig.funcPolicy');

            if($aBuildConfig)
                foreach($aBuildConfig as $sLayer => $aParams) {

                    skConfig::remove('buildConfig.'.$sLayer.'.css');
                    skConfig::remove('buildConfig.'.$sLayer.'.js');
                    skConfig::remove('buildConfig.'.$sLayer.'.hooks');

                    if(isSet($aParams['modules']))
                        foreach($aParams['modules'] as $sModuleName => $aModuleConfig) {

                            $oModuleInstaller = new skInstaller($sModuleName);
                            $oModuleInstaller->unregisterModuleConfig();
                            $oModuleInstaller->registerModuleConfig();

                        }// each module

                }// each layer

              /*
               * 1. Обходим css templates
               * 2. AnalyzeCSSFIles
               * 3. Add to css_data_groups & css_data_params
               * 4. Get data from css_data_groups & css_data_params
               * 5. ParseCSSFIles с учетом weight данными из базы
               * 6. Сборка и запись файла в cache
               * 7. Добавление в реестр по пути .compiledCSS.<layer>.<condition>.array of filePaths (empty conditions must be written as "default")
               * 8. Сохраняем в реестре
               */

            foreach( $this->aLayers as $sLayer ){

                if(!skConfig::isExists('buildConfig.'.$sLayer)) continue;

                $oCSSParser = new CSSParser();

                // Получить массив css-файлов
                $aCSSFiles = $oCSSParser->rebuildCSSArray(skConfig::get('buildConfig.'.$sLayer.'.css'));

                // Собрать параметры из файлов
                $oCSSParser->analyzeCSSFiles($aCSSFiles, 1);

                // Очистить каталог кэша css-файлов
                // @todo Очистка Директории кеш-css
                //$oCSSParser->clearCSSCache(ROOTPATH.'cache/css/');

                // Устанавлием в конфигуратор ссылки на распарсенные файлы css

                skConfig::set('buildConfig.'.$sLayer.'.css', $oCSSParser->parseCSSFiles($aCSSFiles));
            }

            skRegistry::saveStorage('build_'.BUILDVERSION, skConfig::getStorage('buildConfig'));

        } else {

            /*
             * production:
             * 1. Читаем реестр
             * 2. загружаем к конфиги
             */
            skConfig::setStorage('buildConfig', skRegistry::getStorage('build_'.BUILDVERSION));
            skConfig::setStorage('SiteConfig', skRegistry::getStorage('siteConfig'));
        }

        // запуск метода иниц. хуков
        $this->initHooks($this->aDependLayers);

        /*Линкуем css и js*/

        skLinker::addJSFile(skConfig::get('inclusion.js'));
        skLinker::addCSSFile(skConfig::get('inclusion.css'));

        $aJSFIles  = skConfig::get('buildConfig.'.skProcessor::getLayerName().'.js');

        /*
         * TODO для линкера есть дублирующая обработка js файлов модулей в PageModule с учетом фильтра,
         *  нужно решить какое из вхождений верное и удалить дубль
         */
        
        if($aJSFIles)
            foreach($aJSFIles as $aFiles)
                skLinker::addJSFile($aFiles);

        //$this->oDesign = new Design();

    }// func


    protected function initHooks($mLayer = false) {

        if(!$mLayer) return false;

        if(!is_array($mLayer)) $mLayer = array($mLayer);

        foreach($mLayer as $sLayer) {

            /* Инитим хуки модулей из конфига */
            $aBeforeHooks = skConfig::get('buildConfig.'.$sLayer.'.hooks.before');

            if($aBeforeHooks)
                foreach($aBeforeHooks as $sEvent => $aHandlers)
                    foreach($aHandlers as $aHandler)
                        if(count($aHandler))
                            skProcessor::addHookBeforeEvent($sEvent,$aHandler['class'], $aHandler['method']);

            $aAfterHooks = skConfig::get('buildConfig.'.$sLayer.'.hooks.after');

            if($aAfterHooks)
                foreach($aAfterHooks as $sEvent => $aHandlers)
                    foreach($aHandlers as $aHandler)
                        if(count($aHandler))
                            skProcessor::addHookAfterEvent($sEvent,$aHandler['class'], $aHandler['method']);
        }// each layer

        return true;
    }// func

    /**
     * Устанавливает заголовки JSON запроса
     * @param array $aJSONHeaders массив заголовков
     * @return array
     */
    public function setJSONHeaders($aJSONHeaders) {
        return $this->aJSONHeaders = $aJSONHeaders;
    }// func

    /**
     * Возвращает массив заголовков JSON запроса
     * @return array
     */
    public function getJSONHeaders() {
        return $this->aJSONHeaders;
    }// func

}// class
