<?php
/**
 * Враппер для Yaml (part of Symfony project)
 * @class skYaml
 * @extends Yaml
 *
 * @author ArmiT, $Author: sapozhkov $
 * @version $Revision: 6 $
 * @date $Date: 2012-05-18 10:52:53 +0400 (Пт., 18 мая 2012) $
 * @project Skewer
 * @package Kernel
 *
 * @link https://github.com/symfony/Yaml\
 * @use namespaces
 */

require_once COREPATH.'libs/Yaml/Dumper.php';
require_once COREPATH.'libs/Yaml/Inline.php';
require_once COREPATH.'libs/Yaml/Escaper.php';
require_once COREPATH.'libs/Yaml/Parser.php';
require_once COREPATH.'libs/Yaml/Unescaper.php';
require_once COREPATH.'libs/Yaml/Yaml.php';

use Symfony\Component\Yaml AS YamlLib;

class skYaml extends YamlLib\Yaml {

}// class
