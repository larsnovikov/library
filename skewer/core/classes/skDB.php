<?php
/**
 * Провайдер работы с БД
 * @class skDB
 * @project Skewer
 * @package kernel
 * @extends mysqli
 * @author ArmiT, $Author: acat $
 * @version $Revision: 1194 $
 * @date $Date: 2012-11-14 17:52:24 +0400 (Ср., 14 нояб. 2012) $
 *
 */
class skDB extends mysqli {

    /**
     * Создает новое подключение к СУБД
     * @param string $host
     * @param string $user
     * @param string $pass
     * @param string $db
     */
    public function __construct( $host, $user, $pass, $db ) {
        parent::init();
        //parent::__construct( $host, $user, $pass, $db );
        parent::real_connect( $host, $user, $pass, $db );

        if ( mysqli_connect_error() ) {
            die('Connect Error (' . mysqli_connect_errno() . ') '
                    . mysqli_connect_error());
        }
    }//constructor

    /**
     * Осуществляет разбор запроса с целью выявления placeholders
     * @protected
     * @param string $sQuery SQL-запрос
     * @param array $aData Массив данных для замены placeholders
     * @return string Возвращает собранный запрос
     */
    protected function parseQuery( $sQuery, $aData = array() ) {

        if ( !sizeof($aData) ) return $sQuery;

        $sPattern = '/\[([-_0-9a-z]+)(:([a-z]+)?)?\]/Ui';

        $oSelf = $this;
        $sQuery = preg_replace_callback ( $sPattern, function($matches) use ($aData, $oSelf) {

            /** @var skDB $oSelf  */
            switch ( isset($matches[3])? $matches[3]: '' ) {
                case 'i':
                    return isset($aData[$matches[1]])? (int)$aData[$matches[1]]: '';
                    break;
                case 's':
                    return isset($aData[$matches[1]])? '\''.$oSelf->real_escape_string($aData[$matches[1]]).'\'': '';
                    break;
                case 'q':
                    return isset($aData[$matches[1]])? $oSelf->real_escape_string($aData[$matches[1]]): '';
                    break;
                default:
                    return isset($aData[$matches[1]])? '\''.$oSelf->real_escape_string($aData[$matches[1]]).'\'': '';
                    break;
            }
        }, $sQuery );

        return $sQuery;

    }//func

    /**
     * Выполняет запрос $sQuery к СУБД
     * @param string $sQuery SQL-запрос
     * @param array $aData Массив данных для замены placeholders
     * @throws Exception
     * @return bool|mixed|\mysqli_result
     */
    public function query( $sQuery, $aData = array() ) {

        $mResult = false;

        try {
            $sQuery = $this->parseQuery($sQuery, $aData);

            // @todo Здесь должен быть логгер
            $mResult = parent::query( $sQuery );
            if($this->errno) throw new Exception($this->error);

        } catch (Exception $oEx) {
            echo 'DB Exception ['.$sQuery.'] - '.$oEx->getMessage().': '.$oEx->getFile().' - '.$oEx->getLine();
        }
        return $mResult;
        
    }//func
    
    /**
     * Осуществляет запрос на получение строки результата в виде одномерного ассоциативного массива
     * @param string $sQuery SQL-запрос
     * @param array $aData Массив данных для замены placeholders
     * @return bool|array
     */
    public function getRow( $sQuery, $aData = array() ){

        $rResult = $this->query($sQuery, $aData);
        
        if ( !$rResult->num_rows ) return false;
        
        return $rResult->fetch_array(MYSQLI_ASSOC);

    }//func

    /**
     * Осуществляет запрос $sQuery на получение значения поля $sFieldName
     * @param string $sQuery  $sQuery SQL-запрос
     * @param string $sFieldName Название запрашиваемого поля
     * @param array $aData Массив данных для замены placeholders
     * @return bool|string
     */
    public function getValue( $sQuery, $sFieldName, $aData = array() ){

        $rResult = $this->query($sQuery, $aData);

        if ( !$rResult->num_rows ) return false;

        $aRow = $rResult->fetch_array(MYSQLI_ASSOC);

        return (isSet($aRow[$sFieldName]))? $aRow[$sFieldName]: false;

    }//func

    /**
     * Выводит запрос $sQuery после замены placeholders на данные массива $aData
     * @param string $sQuery SQL-запрос
     * @param array $aData Массив данных для замены placeholders
     * @param bool $bExec Указывает на необходимость выполнение отображаемого запроса.
     * @return bool|\mysqli_result
     */
    public function debug( $sQuery, $aData = array(), $bExec = false ) {

        $res = $this->parseQuery($sQuery, $aData);
        echo sprintf('<fieldset style="border: 1px solid #ff8482"><legend style="color: #ff4e42">DB dump</legend>%s</fieldset>',$res);
        if($bExec)
            return $this->query($sQuery, $aData);

    }//func
    
}//class
