<?php
/**
 * Класс, реализующий Soap сервер с поддержкой шифрования трафика
 * @class skSoapServer
 * @extends SoapServer
 *
 * @author ArmiT, $Author: armit $
 * @version $Revision: 284 $
 * @date $Date: 2012-06-13 18:21:04 +0400 (Ср., 13 июня 2012) $
 * @project Skewer
 * @package Kernel
 *
 * @uses skBlowfish
 */
class skSoapServer extends SoapServer {

    /**
     * Класс, реализующий шифрование
     * @var null|skBlowfish
     */
    private $oCryptProxy = null;

    /**
     * Флаг, указывающий на необходимость шифрования/дешифровки трафика
     * @var bool
     */
    private $bCryptMode = false;

    /**
     * Ключ шифрования (рекомендуемая длина ключа не менее 32 бит)
     * @var string
     */
    private $sCryptKey = '';

    /**
     * Теги статических заголовков
     * @var array
     */
    protected $aStaticHeaderTags = array('<staticHeader>','</staticHeader>');

    /**
     * Массив заголовков
     * @var array
     */
    protected $aHeaders = array();

    /**
     * @todo Дописать
     * @var array
     */
    protected $aHeadersCallback = array();

    /**
     * Создает экземпляр Soap сервера на основе WSDL документа $wsdl и опций $options.
     * Если в массиве настроек присутствуют ключи crypt.key (ключ для шифрования трафика) и
     * crypt.vector(вектор инициализации для CBC режима шифрования), то сообщения будут отправлятся
     * после предварительного шифрования объектом oCryptProxy с помощью ключа crypt.key.
     * Все входящие пакеты так же будут подвергаться процессу дешифровки. Для того, чтобы
     * соединение было установлено в Security режиме нужно идентичным образом и с одинаковыми ключами и
     * векторами сконфигурировать серверную и клиентскую части.
     * @param string $wsdl Ссылка на валидный WSDL документ WEB сервиса.
     * @param array $options Массив настроек сервера.
     */
    public function __construct($wsdl, $options = array()) {

        /* Если требуется шифрование трафика - создаем и настраиваем прокси */
        if(isSet($options['crypt']) AND $options['crypt'])
            if(isSet($options['crypt']['key']) AND $options['crypt']['vector']){

                $this->oCryptProxy  = new skBlowfish();
                $this->sCryptKey    = $options['crypt']['key'];
                $this->oCryptProxy->setIv($options['crypt']['vector']);
                $this->bCryptMode   = true;
            }
        parent::__construct($wsdl, $options);
    }// constructor

    /**
     * Метод, принимающий запросы от Soap клиента.
     * @param null|string $request Строка запроса либо null
     * @return bool|string|void
     */
    public function handle($request = null) {

        /* пытаемся получить RAW data из стандартного ввода */
        if (!isset($GLOBALS['HTTP_RAW_POST_DATA']))
            $GLOBALS['HTTP_RAW_POST_DATA'] = file_get_contents('php://input');

        /* Если запрос не передали вручную, то применяем то, что пришло */
        if (is_null($request))
            $request = $GLOBALS['HTTP_RAW_POST_DATA'];

        /* Работаем с заголовками */
        $request = $this->parseStaticHeaders($request, $this->aHeaders);

        if(is_callable($this->aHeadersCallback)) {

            $oClass  = $this->aHeadersCallback[0];
            $sMethod = $this->aHeadersCallback[1];

            if(method_exists($oClass, $sMethod))
                $oClass->$sMethod($this->aHeaders);
            //call_user_func_array($this->aHeadersCallback, array($this->aHeaders));
        }

        /* Дешифруем если надо */
        if($this->bCryptMode)
            $request = $this->oCryptProxy->decrypt($request, $this->sCryptKey);

        /* Захватываем ответ */
        ob_start(array($this, 'encrypt'));

        /* Вызываем метод стандартного обработчика */
        parent::handle($request);

        ob_end_flush();

    }// func

    /**
     * Вызывается по завершении захвата ответа от стандартного обработчика Soap сервера.
     * Используется для шифрования ответа.
     * @param string $sOutput Ответ сервера
     * @return string
     */
    public function encrypt($sOutput) {

        if($this->bCryptMode)
            $sOutput = $this->oCryptProxy->encrypt($sOutput, $this->sCryptKey);

        return $sOutput;
    }// func

    /**
     * Разбирает тело запроса на заголовки и шифрованную часть.
     * @param string $sRequest тескт запроса
     * @param array $aHeaders Переменная, в которую по ссылке передается массив найденных параметров
     * @return bool|string Возвращает шифрованная часть запроса либо false в случае ошибки
     */
    protected function parseStaticHeaders($sRequest, &$aHeaders) {

        if(empty($sRequest)) return false;

        $sHeaders = substr($sRequest, 0, stripos($sRequest, $this->aStaticHeaderTags[1])+strlen($this->aStaticHeaderTags[1]));
        $sHeaders = substr($sHeaders, strlen($this->aStaticHeaderTags[0]));
        $sHeaders = substr($sHeaders, 0, strlen($sHeaders)-strlen($this->aStaticHeaderTags[1]));

        parse_str($sHeaders, $aHeaders);
        $sRequest = substr($sRequest,strlen($this->aStaticHeaderTags[0].$sHeaders.$this->aStaticHeaderTags[1]));

        return $sRequest;
    }// func

    /**
     * Устанавилвает метод, который будет вызван после разбора заголовков но до выполнения тела запроса
     * @param $oClass
     * @param $sMethod
     * @see call_user_func_array
     * @return void
     */
    public function onLoadHeaders($oClass, $sMethod) {

        $this->aHeadersCallback = array($oClass, $sMethod);

    }// func

}// class
