<?php
/**
 * Класс для работы с тикетами
 * @class skTicket
 *
 * @author AndyMitrich<chul@twinscom.ru>, ArmiT<armit@twinscom.ru>, $Author: sapozhkov $
 * @version $Revision: 6 $
 * @date $Date: 2012-05-18 10:52:53 +0400 (Пт., 18 мая 2012) $
 * @project Skewer
 * @package kernel
 * @extends skTicket
 *
 * @todo Добавить очистку по количеству
 * @todo В данной версии boolean false хранится как строка - сделать по-человечески
 *
 */
class skTicket {

    /**
     * Массив данных (cData) для создаваемого тикета
     * @var array
     */
    protected $aStorage     = array();

    /**
     * Секретное слово, которое участвует в генерации хеша для тикета
     * @var string
     */
    protected $sSecretWord  = 'cf7776510479c47d91ffd98414540c49';

    /**
     * Содержит время жизни тикета в днях
     * @var int
     */
    protected $iDays        = 0;

    /**
     * Содержит время жизни тикета в часах
     * @var int
     */
    protected $iHours       = 0;

    /**
     * Internal method for generate hash-words
     * @return string
     */
    private function generateTicket() {

        return md5($this->sSecretWord.md5(date('Y-m-d H:i:s'))).dechex(microtime(true));
    }// func

    /**
     * Добавляет данные в создаваемый тикет
     * @param string $sKey Ключ данных
     * @param mixed $sVal Данные
     * @return bool
     */
    public function addKey($sKey, $sVal = '') {

        if(!$sKey) return false;

        $this->aStorage[$sKey] = (is_array($sVal) || is_object($sVal))? 'crypted_'.base64_encode(serialize($sVal)): $sVal;

        return true;
    }// func

    /**
     * Устанавливает время жизни тикета в днях
     * @param integer $iDays Количество дней
     * @return bool
     */
    public function perDays($iDays) {
        $this->iDays = (int)$iDays;
        return true;
    }// func

    /**
     * Устанавливает время жизни тикета в часах
     * @param integer $iHours Количество часов
     * @return bool
     */
    public function perHours($iHours) {
        $this->iHours = (int)$iHours;
        return true;
    }// func

    /**
     * Добавляет ранее наполненный тикет в хранилище
     * @param string $sTicket хеш-ключ тикета. Указывается, если требуется обновить тикет
     * @return bool
     */
    public function addTicket($sTicket = '') {

        $aData['hash']   = (empty($sTicket))? $this->generateTicket(): $sTicket; // Если сохраняемый тикет не указан в качестве параметра - создаем новый.
        $this->iHours   += $this->iDays * 24;
        $aData['expire'] = date('Y-m-d H:i:s',strtotime("+".$this->iHours."  hours", strtotime(date('Y-m-d H:i:s'))));
        $aData['cdata']  = json_encode($this->aStorage);

        $iRes = $this->addTicketExecutor($aData);

        $this->aStorage = array();
        $this->iDays = 0;
        $this->iHours = 0;

        return ($iRes)? $aData['hash']: false;

    }// func

    /**
     * Удаляет тикет хеш-ключу
     * @param string $sTicket Хеш-ключ тикета
     * @return bool|mysqli_result
     */
    public function delTicket($sTicket) {

        if(!$sTicket) return false;

        return $this->delTicketExecutor($sTicket);
    }// func

    /**
     * Осуществляет проверку тикета на валидность по expire
     * @param string $sTicket хеш-ключ тикета
     * @return bool
     */
    public function checkTicket($sTicket) {

        if(!$sTicket) return false;

        $aData = $this->checkTicketExecutor($sTicket);
        
        if(!$aData) return false;
        
        if(date('Y-m-d H:i:s') <= $aData['expire']) return true; // тикет свежий
        
        $this->delTicket($sTicket); // удаляем протухший тикет
        
        return false;
    }// func

    /**
     * Осуществляет удаление всех просроченных по exipre тикетов
     * @return bool|mysqli_result
     */
    public function delExpire() {

        return $this->delExpireExecutor();

    }// func

    /**
     * Обновляет времея жизни для тикета $sTicket
     * @param string $sTicket хеш-ключ тикета
     * @param integer $iHours Количество дополнительных часов жизни тикета
     * @return bool|mysqli_result
     */
    public function updExpire($sTicket, $iHours) {
        
        $iHours = (int)$iHours;
        
        if(!$sTicket)  return false;
        if(!$iHours) return false;
        
        $aData['hash']   = $sTicket;
        $aData['expire'] = date('Y-m-d H:i:s',strtotime("+$iHours  hours", strtotime(date('Y-m-d H:i:s'))));
        

        return $this->updExpireExecutor($aData);

    }// func

    /**
     * Возвращает данные тикета $sTicket
     * @param  string $sTicket хеш-ключ тикета
     * @return bool
     */
    public function getTicketData($sTicket) {

        $aData = $this->getTicketExecutor($sTicket);
        if(!$aData) return false;
        $this->aStorage = json_decode($aData['cdata'], true);

        return $aData['expire'];
    }// func

    /**
     * Возвращает значения ключа $sKey ранее полученного тикета
     * @param string $sKey Ключ данных
     * @return bool|mixed
     */
    public function getKey($sKey) {

        if(!count($this->aStorage))         return false;
        if(!isSet($this->aStorage[$sKey]))  return false;
        if((stripos($this->aStorage[$sKey], 'crypted_')) === false) return $this->aStorage[$sKey];

        return unserialize(base64_decode(substr($this->aStorage[$sKey],8,strlen($this->aStorage[$sKey]))));

    }// func

    /**
     * Возвращает массив ключей ранее полученного тикета
     * @return array|bool
     */
    public function getKeysArray() {

        if(!count($this->aStorage)) return false;

        $aOut = array();

        foreach ($this->aStorage as $sKey => $sVal) {
            if((stripos($this->aStorage[$sKey], 'crypted_')) === false)
                $aOut[$sKey] = $this->aStorage[$sKey];
            else
                $aOut[$sKey] = unserialize(base64_decode(substr($this->aStorage[$sKey],8,strlen($this->aStorage[$sKey]))));
        }// each

        return $aOut;
    }// func

    /**
     * Очищает данные создаваемого тикета (без записи в базу)
     * @return bool
     */
    public function clearKeys() {

        $this->aStorage = array();
        return true;
    }// func

    /**
     * Очуществляет проверку на существование тикета $sTicket
     * @param string $sTicket хеш-ключ тикета
     * @return bool Возвращает true? если тикет существует в хранилище либо false в случае его отсутствия
     */
    public function ticketIsExists($sTicket) {

        $aData = $this->checkTicketExecutor($sTicket);

        return (!$aData)? false: true;

    }// func

    /* Definition storage type (current: skDB) */

    /**
     * Добавляет данные тикета в хранилище
     * @usedby Переопределяется в потомках класса. Не используется напрямую.
     * @param array $aData Тело создаваемого тикета
     * @return bool
     */
    protected function addTicketExecutor($aData) {

        global $odb;
        /** todo Проверить правильность работы изменненного запроса  */
        $sQuery = "
            INSERT INTO
                `tickets`
            SET
                hash=[hash:s],
                expire=[expire:s],
                cdata=[cdata:s]
            ON DUPLICATE KEY UPDATE
                expire=[expire:s],
                cdata=[cdata:s];";

        return ($odb->query($sQuery, $aData))? true: false;

    }// func

    /**
     * Удаляет тикет $sTicket из хранилища.
     * @usedby Переопределяется в потомках класса. Не используется напрямую.
     * @param string $sTicket хеш-ключ тикета
     * @return bool|mysqli_result
     */
    protected function delTicketExecutor($sTicket) {

        global $odb;

        $aData = array('hash' => $sTicket);

        $sQuery = "
            DELETE FROM
                `tickets`
            WHERE
                hash=[hash:s];";

        return $odb->query($sQuery, $aData);
    }// func

    /**
     * Возвращает тело тикета $sTicket из хранилища
     * @usedby Переопределяется в потомках класса. Не используется напрямую.
     * @param string $sTicket хеш-ключ тикета
     * @return bool
     */
    protected function getTicketExecutor($sTicket) {

        global $odb;

        $aData = array('hash' => $sTicket);

        $sQuery = "
            SELECT
                expire,
                cdata
            FROM
                `tickets`
            WHERE
                hash=[hash:s]
            LIMIT 0, 1;";

        return $odb->getRow($sQuery, $aData);
    }// func

    /**
     * Осуществляет проверку тикета $sTicket на валидность по expire
     * @usedby Переопределяется в потомках класса. Не используется напрямую.
     * @param string $sTicket хеш-ключ тикета
     * @return bool
     */
    protected function checkTicketExecutor($sTicket) {

        global $odb;

        $aData = array('hash' => $sTicket);

        $sQuery = "
            SELECT
                expire
            FROM
                `tickets`
            WHERE
                hash=[hash:s]
            LIMIT 0, 1;";

        return $odb->getRow($sQuery, $aData);
    }// func

    /**
     * Обновляет expire тикета
     * @usedby Переопределяется в потомках класса. Не используется напрямую.
     * @param array $aData Данные тикета
     * @return bool|mysqli_result
     */
    protected function updExpireExecutor($aData) {

        global $odb;

        $sQuery = "
            UPDATE
                `tickets`
            SET
                expire=[expire:s]
            WHERE
                hash=[hash:s];";

        return $odb->query($sQuery, $aData);
    }// func

    /**
     * Удаляет устаревшие по expire тикеты
     * @usedby Переопределяется в потомках класса. Не используется напрямую.
     * @return bool|mysqli_result
     */
    protected function delExpireExecutor() {

        global $odb;

        $sQuery = "
            DELETE FROM
                `tickets`
            WHERE
                expire<NOW();";

        return $odb->query($sQuery);

    }// func
}// class
