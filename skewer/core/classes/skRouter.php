<?php
/**
 * Класс для управление адресной маршрутизацией
 * @class skRouter
 *
 * @author ArmiT, $Author: sapozhkov $
 * @version $Revision: 6 $
 * @date $Date: 2012-05-18 10:52:53 +0400 (Пт., 18 мая 2012) $
 * @project Skewer
 * @package kernel
 *
 */ 
class skRouter {

    /**
     * Остаток прешедшего в роутер адреса
     * @var string
     */
    protected $sCurrentURL;

    /**
     * Разобранные GET параметры
     * @var array
     */
    protected $aGet;

    /**
     * Создает экземпяр skRouter
     * @param $sCurrentURL string
     * @param $aGet array
     * @return \skRouter
     */

    public function __construct(&$sCurrentURL, &$aGet) {

        $this->aGet        = &$aGet;
        $this->sCurrentURL = &$sCurrentURL;

        return true;
    }// constructor

    /**
     * Возвращает id текущего раздела
     * @param $sBaseURL string - Базовый URL
     * @param $iDefaultSection integer - Id раздел, загружаемого по-умолчанию
     * @return bool
     */
    public function getSection($sBaseURL, $iDefaultSection) {

        $sRAWUrl = $this->sCurrentURL;
        $bSpecialViewMode = false;
        /* Установка режима отображения и флага замены в url */
        if(strpos($this->sCurrentURL,'://m.')){
            skProcessor::setEnvParam('_viewMode','pda');
            skProcessor::setEnvParam('_viewModeURLReplace', false);
        } // if

        $sRequestURI = str_ireplace(array('http://','https://',$sBaseURL), '', $this->sCurrentURL);
        $oTree = new Tree();

        if(strpos($sRequestURI, '?')=== 0){
            $this->sCurrentURL = '/'.$sRequestURI;
            return $iDefaultSection;
        }

        if(strpos($sRequestURI, 'index.php')=== 0){
            skResponse::sendRedirectHeaders('/');
            exit;
        }

        /* Установка режима отображения и флага замены в url */
        if(!$bSpecialViewMode and strpos($sRequestURI,'pda/') === 0 ){

            if($pos = strpos($this->sCurrentURL, '/pda/')){
                $this->sCurrentURL = substr($this->sCurrentURL,0,$pos).substr($this->sCurrentURL,$pos+4);
            }

            $sRequestURI = substr($sRequestURI,4);

            skProcessor::setEnvParam('_viewMode','pda');
            skProcessor::setEnvParam('_viewModeURLReplace',true);
        } // if

        /* Убираем все что после ? т.к. оно уже есть в $_GET */
        if(($iPos = strpos($this->sCurrentURL, '?')) !== false)
            $this->sCurrentURL = substr($this->sCurrentURL, 0, $iPos);


        // делаем переадресацию если адрес введен без заключительного слеша
        if($this->sCurrentURL and substr($this->sCurrentURL,-1,1)!=='/'){

            $sRAWUrl = str_replace($this->sCurrentURL, $this->sCurrentURL.'/',$sRAWUrl);
            skResponse::sendRedirectHeaders($sRAWUrl);
            exit;
        }

        $iSectionId = (empty($sRequestURI))? $iDefaultSection: $oTree->getIdByPath('/'.$sRequestURI, $this->sCurrentURL);

        /* Убираем все что после ? т.к. оно уже есть в $_GET */
        if(($iPos = strpos($this->sCurrentURL, '?')) !== false)
            $this->sCurrentURL = substr($this->sCurrentURL, 0, $iPos);
        
        return $iSectionId;
    }// func

    /**
     * Разбирает GET параметры по правилам роутинга
     * @param $aDecodedRules array Массив разобранных правил роутинга
     * @return bool
     */
    public function getParams($aDecodedRules) {
        // @todo нужно дописать (с редиректом не понятно)
        $aSelectedRule = array();

        if ($aDecodedRules AND !empty($this->sCurrentURL)){

            // выбираем наилучшее правило
            foreach($aDecodedRules as $sCurrentRule) {


                if (preg_match($sCurrentRule['_reg_exp'], $this->sCurrentURL, $aEntry)) {
                    
                    $i = (isSet($aEntry[0]))? strlen($aEntry[0]) * 1000: 0;

                    foreach ($sCurrentRule as $key=>$val) {
                        if($key=='_reg_exp') continue;

                        if($val['type']=='const') $i+=101;
                        else $i+=100;

                    } // foreach

                    $aSelectedRule[$i] = $sCurrentRule;

                } // if preg

            } // foreach

            // если есть подходящие правила - дешифруем параметры
            if (count($aSelectedRule)) {

                krsort($aSelectedRule);
                reset($aSelectedRule);
                $sCurrentRule = current($aSelectedRule);
                $i=1;

                preg_match($sCurrentRule['_reg_exp'], $this->sCurrentURL, $aEntry);
                $this->sCurrentURL = substr($this->sCurrentURL , strlen($aEntry[0]));

                foreach ($sCurrentRule as $key => $val) {

                    switch ($val['type']) {

                        case 'set':
                            $this->aGet[$key] = $aEntry[$i];
                            $i++;
                            break;

                        case 'flag':
                            $this->aGet[substr($key, 1)] = 1;
                            $i++;
                            break;

                        case 'const':
                            $i++;
                            break;

                        case 'int':
                            $this->aGet[$val['value']] = (int) $aEntry[$i];
                            $i++;
                            break;

                        case 'str':
                            $this->aGet[$val['value']] = (string) $aEntry[$i];
                            $i++;
                            break;
                    }// switch
                } // foreach
            } // if
            
        }
        return true;
    }// func

    /**
     * Возвращает неразобранный остаток URL
     * @return string
     */
    public function getURLTail() {
        return $this->sCurrentURL;
    }// func

    /**
     * Возвращает разобранные GET параметры
     * @return array
     */
    public function getURLParams() {
        return $this->aGet;
    }// func

    /**
     * Разбирает строковое представление правил роутинга
     * @param $aRules array Массив правил маршрутизации
     * @return array
     */
    public function decodeRules($aRules) {

        if(!count($aRules)) return array();

        $aOut = array();

        foreach ($aRules as $sRule) {

            $aItem       = array();
            $sRegexp     = '';
            $aRule   = explode('/', $sRule);

            foreach($aRule as $sItem)
                if(!empty($sItem)) {

                    if(strpos($sItem, '=') > 0) {// параметр - набор

                        $mRuleValue = explode('|', substr($sItem, strpos($sItem,'=')+1));
                        $sRegexp   .= '('.substr($sItem, strpos($sItem,'=')+1).')\/';
                        $sItem      = substr($sItem,0,strpos($sItem,'='));
                        $sRuleType  = 'set';

                    } elseif(strpos($sItem,'#') === 0) { // константа - флаг

                        $sRegexp   .= '('.substr($sItem, 1).')\/';
                        $mRuleValue = substr($sItem,1);
                        $sRuleType  = 'flag';

                    } elseif(strpos($sItem,'*') === 0) { // константа - заглушка

                        $sRegexp   .= '('.substr($sItem, 1).')\/';
                        $mRuleValue = substr($sItem,1);
                        $sRuleType  = 'const';

                    } elseif($sItem != 0 || $sItem === 0 || strpos($sItem,'(int)') > 0) { // параметр - число

                        $mRuleValue = $sItem;
                        if(strpos($sItem,'(int)') > 0) $mRuleValue = substr($sItem, 0, strpos($sItem,'(int)'));

                        $sItem     = substr($sItem,0,strpos($sItem,'(int)'));
                        $sRuleType = 'int';
                        $sRegexp  .= '(\d+)\/';

                    } else { // параметр - строка

                        $mRuleValue = str_replace('(str)','',$sItem);
                        $sRuleType  = 'str';
                        $sRegexp  .= '([-a-zA-Z_0-9%+.]+)\/';
                    }

                    $aItem[$sItem] = array('type'=>$sRuleType,'value'=>$mRuleValue);
                }// rule d`t empty

            $aItem['_reg_exp'] = '/^'.$sRegexp.'/';
            $aOut[] = $aItem;
        }// each patterns

        return $aOut;
    }// func

    /*public function URL() {

        if(!func_num_args()) return false;
        $mParams = func_get_args();
        $iSectionId = $mParams[0]; // первый параметр всегда id раздела либо его alias

        unSet($mParams[0]);
        if(count($mParams)) {// остались дополнительные параметры
            foreach($mParams as $sParam) {
                list($sModuleName, $sParams) = explode('?',$sParam);
                mb_parse_str($sParam, $aP);
            }// each
        }// if

        //return $sparams;
    }// func*/

    /**
     * Возвращает базовый URL
     * @return string
     */
    public function getBaseURL() {
        return 'http://'.skConfig::get('url.root');
    }// func

    /**
     * Возвращает правила роутинга для указанного модуля
     * @param $sClassName string Название класса модуля
     * @return array|bool Массив правил роутинга
     */
    public function getRulesByClassName($sClassName) {
        
        if(empty($sClassName)) return false;
        if(!class_exists($sClassName)) return false;
        
        $oModuleRouting = substr($sClassName,0,-6).'Routing';
        if(!class_exists($oModuleRouting)) return false;
        /**@var $oModuleRouting skRoutingInterface*/
        return skRouter::decodeRules($oModuleRouting::getRoutePatterns()); // Отрезаем от названия класса "skModule" и добавляем "Routing"
    }// func

    /**
     * Фильтр. Применяется к сформированному html. Производит замену адресных конструкций на URL валидные адреса
     * @param $sInput Собранная html страница
     * @return string html страница с замененными ссылками.
     */
    public static function rewriteURLs($sInput) {

        $sp = '/
        (?<linkType>href|rel|action){1}=
        ([\'"]){1}
            (?<link>\[(?:[^"\']+)\])+
        \2/xui';

       $sOut = preg_replace_callback($sp, function($aEntry){

            $sOut = $aEntry['linkType'].'="'.skRouter::rewriteURL($aEntry['link']).'"';

            return $sOut;

        },$sInput);

        return $sOut;
    }// func

    /**
     * Собирает ссылку по правилам роутинга согласно адресной конструкции
     * @static
     * @param $sLink string Адресная конструкция
     * @return string Собранная Ссылка
     */
    public static function rewriteURL($sLink) {

        // @todo Подключить кеш
        $sLink = substr($sLink,1,-1);
        $aEntry = explode('][',$sLink);
        $oTree  = new Tree();
        $sOut = '';
        $aLostParams = array();

        foreach($aEntry as $mLink){
            if(strpos($mLink, '?')){ // is module

                list($sClassName, $sParams) = explode('?', $mLink);
                $aDecodedRules = skRouter::getRulesByClassName($sClassName);
                mb_parse_str($sParams, $aParams);
                
                // поиск подходящего правила
                $aSelectedRule = array();
                $iSelectedRuleWeight = 0;

                if($aDecodedRules) {
                    foreach ($aDecodedRules as $aItem) {

                        $iCurrentRuleWeight = 0;

                        foreach ($aItem as $sLexemeName => $aLexemeItem) {

                            if (isset($aLexemeItem['type']))
                                switch ($aLexemeItem['type']) {
                                    case 'const':

                                        break;
                                    case 'set':
                                        if (isset($aParams[$sLexemeName])) {
                                            $iCurrentRuleWeight++;
                                        } else {
                                            $iCurrentRuleWeight = -10000;
                                        }
                                        break;
                                    case 'int':
                                        if (isset($aParams[$sLexemeName])) {
                                            $iCurrentRuleWeight++;
                                        } else {
                                            $iCurrentRuleWeight = -10000;
                                        }
                                        break;
                                    case 'str':
                                        if (isset($aParams[$sLexemeName])) {
                                            $iCurrentRuleWeight++;
                                        } else {
                                            $iCurrentRuleWeight = -10000;
                                        }
                                        break;
                                }
                        }
                        if ($iCurrentRuleWeight > $iSelectedRuleWeight) {
                            $iSelectedRuleWeight = $iCurrentRuleWeight;
                            $aSelectedRule = $aItem;
                        }
                    }// each

                    // создание префикса

                    if ($iSelectedRuleWeight > 0)
                        foreach ($aSelectedRule as $sLexemeName => $aLexemeItem) {
                            if (isset($aLexemeItem['type'])) {
                                switch ($aLexemeItem['type']) {
                                    case 'const':
                                        $sOut .= $aLexemeItem['value'].'/';
                                        break;
                                    case 'set':
                                        $sOut .= $aParams[$sLexemeName].'/';
                                        unset($aParams[$sLexemeName]);
                                        break;
                                    case 'int':
                                        $sOut .= $aParams[$sLexemeName].'/';
                                        unset($aParams[$sLexemeName]);
                                        break;
                                    case 'str':
                                        $sOut .= $aParams[$sLexemeName].'/';
                                        unset($aParams[$sLexemeName]);
                                        break;
                                }
                            }
                        }// each
                }// if decode rules

                /*Аккумулируем необработанные параметры */
                $aLostParams = $aLostParams + $aParams;

            } elseif(strpos($mLink, '#')){ // is data uid

                list($sModule, $iId) = explode('#', $mLink);
                
            } elseif(($mLink)) { // is section
                $sSectionPath = $oTree->getAliasPathById((int)$mLink);

                if(skProcessor::getEnvParam('_viewModeURLReplace'))
                    $sSectionPath = DIRECTORY_SEPARATOR.skProcessor::getEnvParam('_viewMode').$sSectionPath;

                $sOut .= ($sSectionPath) ? $sSectionPath : '/';
            }

        }// each

        /* После преобразования остались незадействованные параметры - дописываем в конец */
        if(count($aLostParams)) {

            $aGet  = array();
            foreach($aLostParams as $sKey=>$sValue)
                $aGet[] = $sKey.'='.urlencode($sValue);
            
            $sOut .= (count($aGet))? '?'.implode('&',$aGet): '';

        }// if count params

        return skConfig::get('url.path').$sOut;
    }// func

    /**
     *  Ищет разобранный GET целочисленный параметр и возвращает его по ссылке и true результатом выполнения функции и false в противном случае.
     * @param string $sName Имя запрашиваемого параметра
     * @param integer $iValue Значение, возвращаемое в случае отсутствия параметра
     * @return bool
     */
    public function getInt($sName, &$iValue) {
        if(isset($this->aGet[$sName])) {
            $iValue = (int)$this->aGet[$sName];
            return true;
        }
        return false;
    }// func

    /**
     * Ищет разобранный GET строковый параметр и возвращает его по ссылке и true результатом выполнения функции и false в противном случае.
     * @param string $sName Имя запрашиваемого параметра
     * @param string $sValue Значение, возвращаемое в случае отсутствия параметра
     * @return bool
     */
    public function getStr($sName, &$sValue) {
        if(isset($this->aGet[$sName])) {
            $sValue = $this->aGet[$sName];
            /** @deprecated get_magic_quotes_gpc нельзя использовать */
            //if (get_magic_quotes_gpc()) $sValue = stripslashes($sValue);
            return true;
        }
        return false;
    }// func

    public function set($sName, $mValue, $bOverlay = true) {

        if($bOverlay OR !isset($this->aGet[$sName])) {
            $this->aGet[$sName] = $mValue;
            return true;
        }

        return false;
    }// func

    /**
     * Возвращает режим отображения
     * @return string
     */
    public function getViewMode() {

        return $this->sViewMode;

    }// func

}// class
