<?php

/**
 * @class skLanguage
 * @extends skModule
 * @project Skewer
 * @package kernel
 *
 * @author andymitrich, $Author: sapozhkov $
 * @version $Revision: 6 $
 * @date 27.01.12 12:23 $
 *
 */

class skLanguage {

    private static $aWords = array();
	private static $oInstance  = NULL;
    private static $sBuildPath = '';

	private function __construct($sBuildPath, $sLangFile){

        self::$sBuildPath = $sBuildPath;
        self::loadFile($sLangFile);
    }

	public static function init($sBuildPath, $sLangFile = '') {

        if ( empty($sBuildPath) ) return false;

		if( isset(self::$oInstance) and (self::$oInstance instanceof self) ){

			return self::$oInstance;
		}else{

			self::$oInstance= new self($sBuildPath, $sLangFile);
			return self::$oInstance;
		}
	}// func

    public static function loadFile($sLangFile = '') {

        if ( empty($sLangFile) ) return false;

		if ( !file_exists(self::$sBuildPath.$sLangFile) ) return false;

		self::$aWords = include(self::$sBuildPath.$sLangFile);
		return true;
	}// func

//	public static function get($key = '') {
//
//        $key = strtolower($key);
//
//        return ( isset($aLang[$key]) )? $aLang[$key]: false;
//	}// func

    public static function get() {

        if(!func_num_args())  return false;

        $sKey = func_get_arg(0);
        
        $sText = (isset(self::$aWords[$sKey]))? self::$aWords[$sKey]: false;

        if(func_num_args()<2) return $sText;

        $sVals = func_get_args();
        unSet($sVals[0]);

        return vsprintf($sText, $sVals);
    }// func

}//class