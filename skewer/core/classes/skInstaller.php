<?php
/**
 * Обеспечивает установку, удаление либо апгрейд модулей.
 * @class skInstaller
 *
 * @author ArmiT, $Author: sapozhkov $
 * @version $Revision: 1346 $
 * @date $Date: 2012-12-03 11:31:54 +0400 (Пн., 03 дек. 2012) $
 * @project Skewer
 * @package kernel
 *
 */
class skInstaller {

    /**
     * Экземпляр класса инсталляции для модуля
     * @var null|skModuleInstall
     */
    private $oModuleInstall = null;

    /**
     * Имя модуля
     * @var string
     */
    private $sModuleName = '';

    /**
     * Создает экземпляр инсталлера. Собирает пути, проверяет наличие файла инсталляции модуля
     * @param string $sModuleName Название модуля
     * @throws skException
     */
    public function __construct($sModuleName) {

        $this->sModuleName = $sModuleName;
        $sModuleInstall = $sModuleName.'Install';
        if ( !class_exists($sModuleInstall) )
            throw new skException('Файл установки для модуля ['.$sModuleName.'] не найден!');
        $this->oModuleInstall = new $sModuleInstall();

        if(!($this->oModuleInstall instanceof skModuleInstall))
            throw new skException('Файл установки для модуля '.$sModuleName.' не найден!');

        $this->sModuleDirPath = dirname(skAutoloader::getClassPath($sModuleInstall));
        $this->sModuleConfigPath = $this->sModuleDirPath.'/'.$sModuleName.'Config.php';

        return true;
    }

    /**
     * Регистрирует модуль в реестре а затем выполняет установку в системе
     *
     * @throws skException
     * @return bool
     */
    final public function registerModule() {

        if(! $aBuildSettings = skRegistry::getStorage('build_'.BUILDVERSION)) {
            $aBuildSettings['buildConfig'] = '';
        }

        skConfig::setStorage('buildConfig', $aBuildSettings);

        $this->registerModuleConfig();

        if(!$this->oModuleInstall->init()) throw new skException('Произошла ошибка инициализации модуля ['.$this->sModuleName.']!');

        if(!$this->oModuleInstall->install())  throw new skException('Произошла ошибка установки модуля ['.$this->sModuleName.']!');

        skRegistry::saveStorage('build_'.BUILDVERSION, skConfig::getStorage('buildConfig'));

        return true;
    }// func

    /**
     * Удаляет модуль из реестра а затем выполняет деинсталляцию в системе
     * @throws skException
     * @return bool
     */
    final public function unregisterModule() {
        if(! $aBuildSettings = skRegistry::getStorage('build_'.BUILDVERSION)) {
            $aBuildSettings['buildConfig'] = '';
        }

        skConfig::setStorage('buildConfig', $aBuildSettings);

        if(!$this->oModuleInstall->init()) throw new skException('Произошла ошибка инициализации модуля ['.$this->sModuleName.']!');

        if(!$this->oModuleInstall->deinstall())  throw new skException('Произошла ошибка удаления модуля ['.$this->sModuleName.']!');

        $this->unregisterModuleConfig();

        unSet($aModuleSettings);

        skRegistry::saveStorage('build_'.BUILDVERSION, skConfig::getStorage('buildConfig'));

        return true;

    }// func

    /**
     * Регистрирует конфигурацию модуля в реестре. Устанавливаются clientside файлы, хуки
     * @throws Exception
     */
    final public function registerModuleConfig() {

        if(!file_exists($this->sModuleConfigPath)) throw new Exception('У модуля нет файла настроек!');

        /** @noinspection PhpIncludeInspection */
        $aModuleSettings = include($this->sModuleConfigPath);

        if(!count($aModuleSettings)) throw new Exception('Файл настроек модуля пуст!');

        $sLayer = $aModuleSettings['layer'];
        $aPath = array(
            'buildConfig',
            $sLayer,
            'modules',
            $aModuleSettings['name'],
        );
        skConfig::set(implode('.', $aPath), $aModuleSettings);

        if(isSet($aModuleSettings['hooks'])) {
            if(isSet($aModuleSettings['hooks']['before']))
                foreach($aModuleSettings['hooks']['before'] as $sEventName => $aHandlers){
                    $aPath = array(
                        'buildConfig',
                        $sLayer,
                        'hooks',
                        'before',
                        $sEventName,
                        $aModuleSettings['name'],
                    );
                    skConfig::add(implode('.', $aPath), $aHandlers);
                }// each before hook
            if(isSet($aModuleSettings['hooks']['after']))
                foreach($aModuleSettings['hooks']['after'] as $sEventName => $aHandlers){
                    $aPath = array(
                        'buildConfig',
                        $sLayer,
                        'hooks',
                        'after',
                        $sEventName,
                        $aModuleSettings['name'],
                    );
                    skConfig::add(implode('.', $aPath), $aHandlers);
                }// each after hook
        }// hooks

        if(isSet($aModuleSettings['css']))
            if(count($aModuleSettings['css'])) {

                    $aPath = array(
                        'buildConfig',
                        $sLayer,
                        'css',
                        $aModuleSettings['name'],
                    );
                    skConfig::add(implode('.', $aPath), $aModuleSettings['css']);
                }// register css files

        if(isSet($aModuleSettings['js']))
            if(count($aModuleSettings['js'])) {
                    $aPath = array(
                        'buildConfig',
                        $sLayer,
                        'js',
                        $aModuleSettings['name'],
                    );
                    skConfig::add(implode('.', $aPath), $aModuleSettings['js']);
                }// register js files

        /* Добавление функциональных политик доступа */

        if(isSet($aModuleSettings['policy']))
            if(count($aModuleSettings['policy'])) {

                $aPath = array(
                    'buildConfig',
                    'funcPolicy',
                    $aModuleSettings['name'],
                );

                $aModuleData = array();

                $aModuleData['items'] = $aModuleSettings['policy'];

                /* Если у модуля есть название  */
                if(isSet($aModuleSettings['title']))
                    if(!empty($aModuleSettings['title']))
                        $aModuleData['moduleTitle'] = $aModuleSettings['title'];

                skConfig::add(implode('.', $aPath), $aModuleData);
            }


    }// func

    /**
     * Удаляет конфигурацию модуля из реестра. Удаляются хуки и clientside
     * @throws Exception
     */
    final public function unregisterModuleConfig() {


        if(!file_exists($this->sModuleConfigPath)) throw new Exception('У модуля нет файла настроек!');

        /** @noinspection PhpIncludeInspection */
        $aModuleSettings = include($this->sModuleConfigPath);

        if(!count($aModuleSettings)) throw new Exception('Файл настроек модуля пуст!');

        /* Удаление настроек модуля */
        $sLayer = $aModuleSettings['layer'];
        $sModuleName = $aModuleSettings['name'];

        return self::removeRegistryInfo( $sModuleName, $sLayer );

    }

    /**
     * Удаляет информацию о модуле из реестра
     * @param $sModuleName
     * @param $sLayer
     * @return bool
     */
    public static function removeRegistryInfo( $sModuleName, $sLayer ) {

        $aPath = array(
            'buildConfig',
            $sLayer,
            'modules',
            $sModuleName,
        );
        skConfig::remove( implode( '.', $aPath ) );

        /*Удаление before хуков */
        $aHooks = skConfig::get( 'buildConfig.' . $sLayer . '.hooks.before' );

        if ( $aHooks )
            foreach ( $aHooks as $sEventName => $aHandler ) {

                $aPath = array(
                    'buildConfig',
                    $sLayer,
                    'hooks',
                    'before',
                    $sEventName,
                    $sModuleName,
                );
                skConfig::remove( implode( '.', $aPath ) );
            }
        // each before hook

        /* Удаление after хуков */
        $aHooks = skConfig::get( 'buildConfig.' . $sLayer . '.hooks.after' );

        if ( $aHooks ) {
            foreach ( $aHooks as $sEventName => $aHandler ) {

                $aPath = array(
                    'buildConfig',
                    $sLayer,
                    'hooks',
                    'after',
                    $sEventName,
                    $sModuleName,
                );
                skConfig::remove( implode( '.', $aPath ) );
            }
            // each before hook
        }

        /* Удаление зарегистрированных CSS файлов */
        $aPath = array(
            'buildConfig',
            $sLayer,
            'css',
            $sModuleName,
        );
        skConfig::remove( implode( '.', $aPath ) );

        /* Удаление зарегистрированных JS файлов */
        $aPath = array(
            'buildConfig',
            $sLayer,
            'js',
            $sModuleName,
        );
        skConfig::remove( implode( '.', $aPath ) );

        /* Удаление функциональных политик доступа модуля */
        $aPath = array(
            'buildConfig',
            'funcPolicy',
            $sModuleName,
        );

        skConfig::remove( implode( '.', $aPath ) );

        return true;
    }
    // func

//    public static function removeRegistryInfo( $sModuleName, $sLayer ) {
//
//    }

//    public function getRegisterModules() {
//
//    }// func

}// class
