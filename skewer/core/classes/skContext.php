<?php
/**
 * Передача контекста вызова
 *
 * @class skContext
 * @project Canape 3.0 (skewer)
 * @package kernel
 * @author ArmiT, $Author: acat $
 * @version $Revision: 1172 $
 * @date $Date: 2012-11-12 15:17:31 +0400 (Пн., 12 нояб. 2012) $
 *
 */

class skContext {
    /**
     * Языковая версия вызова модуля
     * @var string
     */
    protected $sLanguage = '';
    /**
     * POST массив
     * @var array
     */
    protected $aPost = array();
    /**
     * Входные параметры вызова модуля
     * @var array
     */
    protected $aParams = array();
    /**
     * Идентификатор объекта
     * @var string
     */
    protected $sObjectId = '';
    /**
     * Тип объекта (module, page, cron)
     * @var string
     * @old
     * @todo: проверить, используется или нет.
     */
    protected $sObjectType = '';
    /**
     * Идентификатор объекта, вызвавшего текущий процесс
     * @var string
     * @old
     * @todo: Проверить использование
     */
    protected $sParentObjectId = '';
    /**
     * Тип объекта, вызвавшего текущий процесс
     * @var string
     * @old
     * @todo: Проверить использование
     */
    protected $sParentObjectType = '';
    /**
     * Тип шаблонизатора
     * @var int
     */
    protected $iParserType = parserTwig;
    /**
     * Массив выходных данных
     * @var array
     */
    protected $aData = array();
    /**
     * Локальный (относительно корня модуля) путь к шаблону
     * @var string
     */
    protected $sTemplate = '';
    /**
     * Отрендеренный результат работы процесса
     * @var string
     */
    protected $sOut = '';
    /**
     * Путь к файлу модуля от корня
     * @var string
     */
    protected $sModulePath = '';
    /**
     * Путь к директории модуля
     * @var string
     */
    protected $sModuleDir  = '';
    /**
     * Имя модуля
     * @var string
     */
    protected $sModuleName  = '';

    /**
     * Имя метки для вставки данных
     * @var string
     */
    public $sLabel = '';
    /**
     * Путь по меткам от корневого процесса до текущего включительно
     * @var string
     */
    public $sLabelPath = '';

    /**
     * Ссылка на процесс (запустивший модуль на выполнение)
     * @var null|skProcess
     */
    public $oProcess = NULL;

    /**
     * Ссылка на родительский процесс (запустивший модуль на выполнение)
     * @var null|skProcess
     */
    private $oParentProcess = NULL;

    /**
     * Имя класса запускаемого в процессе модуля
     * @var string
     */
    public $sClassName = '';
    /**
     * Неразобранная роутером часть URL
     * @var string
     */
    public $sURL = '';
    /**
     * Тип вызова процесса (page | skModule)
     * @var int
     */
    public $iCallType = '';
    /**
     * GET массив
     * @var array
     */
    public $aGet = array();

    /**
     * Путь до директории модуля от корня web-сервера с учетом alias
     * @var string
     */
    private $sModuleWebDir = '';

    /**
     * Массив дополнительных JSON headers? возвращаемых из модуля
     * @var array
     */
    protected $aJSONHeaders = array();

    /**
     * Директория с шаблонами модуля от корня директории модуля
     * @var string
     */
    private $sTemplateDir = '';

    /**
     * Имя слоя
     * @var string
     */
    protected $sModuleLayer = '';

    /**
     * Создает экземпляр контекста
     * @param string $sLabel Метка вызова
     * @param string $sClassName Имя класса вызываемого модуля
     * @param integer $iCallType Тип вызова
     * @param array $aParams Параметры вызова модуля
     * @return skContext
     */
    public function __construct($sLabel, $sClassName, $iCallType, $aParams = array()) {
        
        $this->sLabel     = $sLabel;
        $this->aParams    = $aParams;
        $this->sClassName = $sClassName;
        $this->iCallType  = $iCallType;
    }// func

    /**
     * Отдает метку вызова
     * @return string
     */
    public function getLabel() {
        return $this->sLabel;
    }
    
    /**
     * Сохраняет данные в метку
     * @param string $sLabel Метка установки данных
     * @param mixed $mData Данные
     * @return string
     */
    public function setData($sLabel, $mData) {
        return $this->aData[$sLabel] = $mData;
    }// func
    
    /**
     * Устанавливает шаблон вывода
     * @param string $sTemplate
     * @return bool
     */
    public function setTemplate($sTemplate) {
        $this->sTemplate = $sTemplate;
        return true;
    }// func
    
    /**
     * Устанавливает ответ, обработанный шаблонизатором
     * @param string $sOut
     * @return bool
     */
    public function setOut($sOut) {
        $this->sOut = $sOut;
        return true;
    }// func

    /**
     * Устанавливает тип шаблонизатора
     * @param integer $iParserType
     * @return bool
     */
    public function setParser($iParserType) {
        $this->iParserType = $iParserType;
        return true;
    }// func

    /**
     * Устанавливает путь к модулю
     * @param string $sModulePath
     * @return bool
     */
    public function setModulePath($sModulePath) {
        $this->sModulePath = $sModulePath;
        return true;
    }// func

    /**
     * Устанавливает директорию модуля
     * @param string $sModuleDir
     * @return bool
     */
    public function setModuleDir($sModuleDir) {
        $this->sModuleDir = $sModuleDir.'/';
        return true;
    }// func


    /**
     * Устанавливает имя модуля
     * @param string $sModuleName Имя модуля
     * @return bool
     */
    public function setModuleName($sModuleName) {
        $this->sModuleName = $sModuleName;
        return true;
    }// func

    /**
     * Возвращает результаты (массив) работы модуля
     * @param string $sLabel метка данных
     * @return array
     */
    public function getData($sLabel = '') {
        return (isSet($this->aData[$sLabel]))? $this->aData[$sLabel]: $this->aData;
    }// func

    /**
     * Возвращает относительный путь к шаблону рендеринга
     * @return string
     */
    public function getTemplate() {
        return $this->sTemplate;
    }// func
    
    /**
     * Возвращает тип шаблонизатора для модуля
     * @return int
     */
    public function getParser() {
        return $this->iParserType;
    }// func

    /**
     * Возвращает путь к файлу модуля
     * @return string
     */
    public function getModulePath() {
        return $this->sModulePath;
    }// func

    /**
     * Возвращает путь к директории модуля
     * @return string
     */
    public function getModuleDir() {
        return $this->sModuleDir;
    }// func

    /**
     * Возвращает параметры вызова
     * @return array
     */
    public function getParams() {
        return $this->aParams;
    }// func

    /**
     * Возвращает имя модуля
     * @return string
     */
    public function getModuleName() {
        return $this->sModuleName;
    }// func

    /**
     * Очищает массив данных, возвращенных модулем
     * @return array
     */
    public function clearData() {
        return $this->aData = array();
    }// func

    /**
     * Возвращает ссылку на родительский процесс
     * @return null|skProcess
     */
    public function getParentProcess() {
        return $this->oParentProcess;
    }// func

    /**
     * Устанавливает родительский процесс
     * @param skProcess $oParentProcess
     * @return skProcess
     */
    public function setParentProcess(skProcess &$oParentProcess) {
        return $this->oParentProcess = $oParentProcess;
    }// func

    /**
     *Устанавливает имя слоя модуля
     * @param $sModuleLayer
     */
    public function setModuleLayer($sModuleLayer){
        $this->sModuleLayer = $sModuleLayer;
    }

    /**
     * Возвращает имя слоя модуля
     * @return string
     */
    public function getModuleLayer(){
        return $this->sModuleLayer;
    }

    /**
     * Возвращает корневую директрию модуля от корня web-сервера с учетом alias
     * @return string
     */
    public function getModuleWebDir() {
        return $this->sModuleWebDir;
    }

    /**
     * Устанавливает корневую директрию модуля от корня web-сервера с учетом alias
     * @param string $sVal Путь до директории модуля
     * @return mixed
     */
    public function setModuleWebDir( $sVal ) {
        return $this->sModuleWebDir = $sVal;
    }

    /**
     * Возвращает JSON Header по ключу $sKey
     * @param string $sKey Название ключа заголовка
     * @return null
     */
    public function getJSONHeader( $sKey ) {
        return isset($this->aJSONHeaders[$sKey]) ? $this->aJSONHeaders[$sKey] : null;
    }

    /**
     * Устанавливает дополнительный JSON заголовок $sKey со значением $mValue
     * @param string $sKey Заголовок
     * @param $mValue Значение заголовка
     * @return array Возвращает значение зоголовка
     */
    public function setJSONHeader( $sKey, $mValue ) {
        return $this->aJSONHeaders[$sKey] = $mValue;
    }

    /**
     * Убирает дополнительный JSON заголовок $sKey со значением $mValue
     * @param string $sKey Заголовок
     * @return bool - true - был и удален / false - не было или не удален
     */
    public function unsetJSONHeader( $sKey ) {
        if ( isset($this->aJSONHeaders[$sKey]) ) {
            unset($this->aJSONHeaders[$sKey]);
            return true;
        } else return false;
    }

    /**
     * Возвращает список добавленных JSON Headers
     * @return array
     */
    public function getJSONHeadersList() {
        return $this->aJSONHeaders;
    }

    /**
     * Возвращает путь к директории хранения шаблонов модуля
     * @return string
     */
    public function getTplDirectory(){

        return $this->sTemplateDir;

    }// func

    /**
     * Изменяет путь к директории хранения шаблонов модуля
     * @param $sDir Директория расположения CSS файлов модуля
     * @return string
     */
    public function setTplDirectory($sDir){

        return $this->sTemplateDir = $sDir;

    }// func

    public function setParams($aParams){

        $this->aParams = $aParams;

    }// func

    /**
     * Отдает имя класса
     * @return string
     */
    public function getClassName() {
        return $this->sClassName;
    }

}// class
