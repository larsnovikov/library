<?php
/**
 * Wrapper для CodeGenerator
 * @class skCodeGenerator
 * @extends CodeGenerator
 *
 * @author ArmiT, $Author: sapozhkov $
 * @version $Revision: 6 $
 * @date $Date: 2012-05-18 10:52:53 +0400 (Пт., 18 мая 2012) $
 * @project JetBrains PhpStorm
 * @package kernel
 */
require_once(COREPATH.'libs/CodeGenerator/classes/CodeGenerator.php');
require_once(COREPATH.'libs/CodeGenerator/interfaces/codeTplInterface.php');
require_once(COREPATH.'libs/CodeGenerator/exceptions/CodeTplException.php');
require_once(COREPATH.'libs/CodeGenerator/classes/codeTplPrototype.php');

require_once(COREPATH.'libs/CodeGenerator/templates/ConstantsTpl.php');
require_once(COREPATH.'libs/CodeGenerator/templates/HtaccessTpl.php');

class skCodeGenerator extends CodeGenerator {

}// class
