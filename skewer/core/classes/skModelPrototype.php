<?php
/**
 * Прототип модели описания данных
 *
 * @class: skModelPrototype
 *
 * @Author: User, $Author: sapozhkov $
 * @version: $Revision: 6 $
 * @date: $Date: 2012-05-18 10:52:53 +0400 (Пт., 18 мая 2012) $
 *
 */

abstract class skModelPrototype {

    /**
     * @var Список полей таблицы
     */
    protected static $aParametersList = array();

    /**
     * @static Метод возвращает список полей рабочей таблицы
     * @return array
     */
    public static function getParametersList() {
        return static::$aParametersList;
    }

    /**
     * Возвращает набор имен полей
     * @static
     * @return array
     */
    public static function getParamNames(){
        return array_keys( static::$aParametersList );
    }

    /** Отдает одно поле с описанием
     * @static
     * @param $sKey
     * @return array
     */
    public static function getParameter($sKey) {

        // проверить на существование
        if ( isset(static::$aParametersList[$sKey]) ) {

            return static::getParamArrayByString( static::$aParametersList[$sKey], $sKey );

        } else return array();

    }

    /**
     * Разбирает строку на массив описания поля
     * @static
     * @param string $sInput - входная строка
     * @param string $sName - имя поля
     * @return array
     */
    protected  static function getParamArrayByString( $sInput, $sName ) {

        // разобрать
        $aValues = explode(':',$sInput);

        // выходной массив
        $aOut = array();

        $aOut['name'] = $sName;
        $aOut['type']    = $aValues[0];
        $aOut['view']    = (isSet($aValues[1]))? $aValues[1]: '';
        $aOut['title']   = (isSet($aValues[2]))? $aValues[2]: '';
        $aOut['default'] = (isSet($aValues[3]))? $aValues[3]: '';

        return $aOut;

    }

    /**
     * Отдает набор полей с описаниями
     * @param array $aFieldFilter - набор полей
     * @return array
     */
    public static function getParamDefList($aFieldFilter) {

        $aOut = array();

        foreach ( $aFieldFilter as $sFieldName ) {

            if ( $aRow = static::getParameter($sFieldName) ) {
                $aOut[$sFieldName] = $aRow;
            }

        }

        return $aOut;

    }

    /**
     * Отдает дополнительный набор параметров для конфигурации полей
     * @return array
     */
    protected static function getAddParamList(){
        return array();
    }

    /**
     * Отдает набор полей с расширенным описанием
     * @static
     * @param array $aFieldFilter - массив с набором колонок
     * @param array $aAddParamList - массив расширения парметров
     * @return array
     */
    public static function getFullParamDefList( $aFieldFilter, $aAddParamList=null ) {

        // запросить стандартный набор
        $aParamList = static::getParamDefList( $aFieldFilter );

        // если дополнительный набор не задан, взять стандартный
        if ( $aAddParamList === null )
            $aAddParamList = static::getAddParamList();

        // выходная переменная
        $aOut = array();

        // расширить дополнительными параметрами
        foreach ( $aFieldFilter AS $sKey ) {

            // основа для параметра
            if ( isset( $aParamList[$sKey] ) )
                $aNewParam = $aParamList[$sKey];
            else
                $aNewParam = static::getParamArrayByString( "s:str:$sKey", $sKey );

            // запись дополнительных параметров для поля
            if ( isset($aAddParamList[$sKey]) ) {

                // слить параметры
                foreach ( $aAddParamList[$sKey] as $sValName => $mValVal ) {

                    if ( isset($aNewParam[$sValName]) )
                        $mOldVal = $aNewParam[$sValName];
                    else $mOldVal = '';

                    if ( is_array($mValVal) ) {
                        $aNewParam[$sValName] = array_merge( $mOldVal ? $mOldVal : array(), $mValVal );
                    } else {
                        $aNewParam[$sValName] = $mValVal;
                    }

                }

            }

            // добавить в вывод, если параметр есть
            if ( $aNewParam )
                $aOut[$sKey] = $aNewParam;

        }

        // отдать
        return $aOut;

    }// func

    /**
     * Возвращает массив имен полей таблицы маппера
     * @static
     * @return array
     */
    public static function getModelFields() {

        return array_keys (static::getParametersList());
    }// func

    /**
     * Возвращает массив полей модели с значениями по-умолчанию.
     * Если указан $aFieldsFilter, то будут возвращены только поля, указанные в нем.
     * @static
     * @param array $aFieldsFilter список параметров модели, требуемых на выходе.
     * @return array|bool
     */
    public static function getBlankModel($aFieldsFilter = array()) {

        $aFields = (count($aFieldsFilter))? $aFieldsFilter: static::getModelFields();

        if(!count($aFields)) return false;

        $aOut = array();

        foreach($aFields as $aField) {

            $aParam = static::getParameter($aField);
            $aOut[$aField] = $aParam['default'];

        }// each field

        return $aOut;
    }// func

}
