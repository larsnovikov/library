<?php
/**
 * Агрегатор clientside файлов (css/js)
 * @class ClientFiles
 *
 * @author ArmiT, $Author: acat $
 * @version $Revision: 1671 $
 * @date $Date: 2013-02-06 15:38:54 +0400 (Ср, 06 фев 2013) $
 * @project Skewer
 * @package kernel
 * @singleton
 */ 
class skLinker {

    /**
     * Экземпляр класса
     * @var null
     */
    static private $oInstance = NULL;

    /**
     * Список подключенных JS файлов
     * @var array
     */
    static private $aJSFiles = array();

    /**
     * Список подключенных CSS файлов
     * @var array
     */
    static private $aCSSFiles = array();

    /**
     * Путь до корневой build - директории
     * @var string
     */
    static private $sCorePath = '';

    /**
     * Путь до корневой overlay_build - директории
     * @var string
     */
    static private $sCoreOverlayPath = '';

    /**
     * Закрытый конструктор
     * @param string $sCorePath Путь до корневой build - директории
     * @param string $sOverlayCorePath Путь до корневой overlay_build - директории
     */
    private function __construct($sCorePath, $sOverlayCorePath = '') {

        self::$sCorePath = $sCorePath;
        self::$sCoreOverlayPath = $sOverlayCorePath;

    }// func

    /**
     * Инициализирует сборщик файлов (Linker)
     * @static
     * @param string $sCorePath Путь до корневой build - директории
     * @param string $sOverlayCorePath Путь до корневой overlay_build - директории
     * @return null
     */
    public static function init($sCorePath, $sOverlayCorePath = '') {

        if(isset(self::$oInstance) and (self::$oInstance instanceof self)){

            return self::$oInstance;
        }else{

            self::$oInstance= new self($sCorePath, $sOverlayCorePath);

            return self::$oInstance;
        }
    }// func

    /**
     * Добавляет в Linker JS-Файл $mFilePath. Если $mFilePath - массив, то возможна установка
     * нескольких значений и учитываются фильтры, устанавливаемые вложенным массивом.
     * @static
     * @param array|string $mFilePath Путь либо массив путей до файлов
     * @param bool|string $sCondition Условие вывода файла в браузере
     * @return bool
     *
     * @example:
     * Ниже пример массива файлов с условиями:
     * array(
     * 'skewer_build/modules/Page/js/init.js',
     * 'skewer_build/modules/Page/js/init.ie.js' => array('condition' => 'IE 7'));
     */
    static public function addJSFile($mFilePath, $sCondition = false) {
        
        if(!$mFilePath) return false;
        
        $aOverlayJs = skConfig::get('overlay.js');
        // рекурсивно спускаемся по массиву с заменой JS локальными файлами
        $fRecurs = function($aTemp) use (&$fRecurs,$aOverlayJs){
            if (is_array($aTemp)){
                $aRecusTemp = array();
        
                foreach($aTemp as $aItem){
                    $aRecusTemp[] = $fRecurs($aItem);
                }
                return $aRecusTemp;
            } else {
                
                // готовим проверку на перекрытие JS
                $sLocalPath =  str_replace('/skewer_build/', 'build/', $aTemp);
        
                if (array_search($sLocalPath,$aOverlayJs)!==false){
                    $aTemp = "/skewer/$sLocalPath";
                    return $aTemp;
                }
                return $aTemp;
            }
        };
        
        if ($aOverlayJs)
            $mFilePath = $fRecurs($mFilePath);   

        if(is_array($mFilePath))
            if(count($mFilePath)) {
                foreach($mFilePath as $sKey=>$aParams){
                    if(is_array($aParams))
                        self::$aJSFiles[$sKey] = $aParams;
                    else
                        self::$aJSFiles[$aParams] = false;
                }
                return true;
            }

        self::$aJSFiles[$mFilePath] = ($sCondition)? array('condition' => $sCondition): false;

        return true;
    }// func

    /**
     * Добавляет в Linker CSS-Файл $mFilePath. Если $mFilePath - массив, то возможна установка
     * нескольких значений и учитываются фильтры, устанавливаемые вложенным массивом.
     * @static
     * @param array|string $mFilePath Путь либо массив путей до файлов
     * @param bool|string $sCondition Условие вывода файла в браузере
     * @return bool
     *
     * @example:
     * Ниже пример массива файлов с условиями:
     * array(
     * 'skewer_build/modules/Page/css/main.css',
     * 'skewer_build/modules/Page/css/main.ie.css' => array('condition' => 'IE 7'));
     */
    static public function addCSSFile($mFilePath, $sCondition = false) {

        if(!$mFilePath) return false;

        if(is_array($mFilePath))
            if(count($mFilePath)) {

                foreach($mFilePath as $sKey=>$aParams){
                    if(is_array($aParams))
                    {
                        self::$aCSSFiles[$sKey] = $aParams;
                    } else {
                        self::$aCSSFiles[$aParams] = false;
                    }
                }

                return true;
            }

        self::$aCSSFiles[$mFilePath] = ($sCondition)? array('condition' => $sCondition): false;
        return true;
    }// func

    /**
     * Возвращает список собранных классом Linker JS файлов
     * @static
     * @return array
     */
    static public function getJSFiles() {

        $aOut = array();

        reset(self::$aJSFiles);
        while (list($sFilePath, $aParams) = each(self::$aJSFiles)) {
            $aOut[] = array(
                'condition' => ($aParams)? $aParams['condition']: false,
                'filePath'  => $sFilePath,
            );
        }// each file
        
        return $aOut;

    }// func

    /**
     * Возвращает список собранных классом Linker CSS файлов
     * @static
     * @return array
     */
    static public function getCSSFiles() {

        $aOut = array();

        reset(self::$aCSSFiles);
        while (list($sFilePath, $aParams) = each(self::$aCSSFiles)) {
            $aOut[] = array(
                'condition' => ( isset($aParams['condition']) )? $aParams['condition']: false,
                'filePath'  => $sFilePath,
            );
        }// each file

        return $aOut;
    }// func

    /**
     * Очищает набор установленных css файлов
     * @static
     */
    static public function clearCSSFiles(){
        self::$aCSSFiles = array();
    }

    /**
     * Очищает набор установленных js файлов
     * @static
     */
    static public function clearJSFiles(){
        self::$aJSFiles = array();
    }

}// class
