<?php
/**
 *
 * @class skError
 *
 * @author ArmiT, $Author$
 * @version $Revision$
 * @date $Date$
 * @project JetBrains PhpStorm
 * @package kernel
 */
class skError {

    public static function init() {

        set_error_handler('skError::my_error_handler');

        set_exception_handler('skError::my_exception_handler');

        register_shutdown_function('skError::my_fatal_handler');

    }// func

    public static function my_error_handler($no, $str, $file, $line) {
        skLogger::dump('[error: '.$no.', '.$str.', '.$file.', '.$line.']');
    }// func

    public static function my_exception_handler(Exception $e) {
        skLogger::dump('[exception:'.$e->getMessage().']');
    }// func

    public static function my_fatal_handler() {
        if(is_null($e = error_get_last())) return true;
        skLogger::dump('[fatal:'.$e['message'].', '.$e['type'].', '.$e['file'].', '.$e['line'].']');

        return true;
    }// func


}// class Errors
