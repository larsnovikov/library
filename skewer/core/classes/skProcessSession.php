<?php
/**
 * Хранилище дерева процессов
 * @class skProcessSession
 *
 * @author ArmiT, $Author: sapozhkov $
 * @version $Revision: 6 $
 * @date $Date: 2012-05-18 10:52:53 +0400 (Пт., 18 мая 2012) $
 * @project Skewer
 * @package kernel
 */ 
class skProcessSession {

    /**
     * Экземпляр класса skTicket
     * @var null|\skSessionTicket
     */
    private $oTicket = NULL;

    /**
     * Создает экземпляр провайдера к хранилищу дерева процессов
     * @param string $sKeyName - имя ключа
     */
    public function __construct( $sKeyName = 'key' ) {

        $this->oTicket = new skSessionTicket(skConfig::get('session.process.'.$sKeyName));

    }// constructor

    /**
     * Загружает дерево процессов по идентификатору сессии
     * @param string $sTicket Хеш-ключ идентификатора сессии
     * @return bool|mixed
     */
    public function load($sTicket) {

        $sExpire = $this->oTicket->getTicketData($sTicket);
        if(!$sExpire) return false;
        $oProcess = $this->oTicket->getKey('process');

        return (($oProcess instanceof skProcess))? $oProcess: false;

    }// func

    /**
     * Сохраняет процесс / дерево процессов в сессионное хранилище
     * @param skProcess $oProcess Корневой сохраняемый процесс
     * @param bool $sSessionId Указатель на ключ хранилища сессии. Используется если требуется обновить текущий тикет.
     * @return bool
     */
    public function save(skProcess $oProcess, $sSessionId = false){

        $this->oTicket->clearKeys();
        $this->oTicket->addKey('process', $oProcess);
        return  $this->oTicket->addTicket($sSessionId);

    }// func

    /**
     * Осуществляет проверку наличия дерева процессов по тикету $sTicket
     * @param string $sTicket Хеш-ключ идентификатора сессии
     * @return bool
     */
    public function isExists($sTicket) {

        return ($this->oTicket->ticketIsExists($sTicket))? true: false;

    }// func

    /**
     * Создает пустую сессию
     * @return bool В случае успеха возвращает хеш-ключ созданной сессии
     */
    public function createSession(){

        $this->oTicket->clearKeys();
        return  $this->oTicket->addTicket();

    }// func

    /**
     * Очищает текущее хранилище
     * @static
     */
    public static function flushStorage() {

        $oProcess = new self();
        $oProcess->oTicket->clearSession();

    }

}// class
