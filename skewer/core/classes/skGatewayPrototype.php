<?php
/**
 *
 * @class skGatewayPrototype
 *
 * @author ArmiT, $Author: sapozhkov $
 * @version $Revision: 16 $
 * @date $Date: 2012-05-21 14:42:32 +0400 (Пн., 21 мая 2012) $
 * @project Skewer
 * @package Kernel
 */
class skGatewayPrototype {

    /**
     * @const int Только нешфрованные запросы
     */
    const StreamTypeNEncrypt = 0x00;

    /**
     * @const int Только шифрованные запросы
     */
    const StreamTypeEncrypt = 0x01;

    /**
     * @const int Автоопределение
     */
    const StreamTypeAuto = 0x02;

    /**
     * Режим работы с потоком
     * @var int
     */
    protected $iStreamType = 0x00;

    /**
     * Ключ для подписи и шифрования пакетов
     * @var string
     */
    protected $sKey = '';

    /**
     * Массив с параметрами заголовка отправляемого вместе с запросом
     * @var array
     */
    protected $aHeader = array();

    /**
     * Список методов запрашиваемых на выполнение в рамках одного запроса
     * @var array
     */
    protected $aActions = array();

    /**
     * Спиcок записей на отправку файлов одним запросом на сервер
     * @var array
     */
    protected $aFiles = array();

    /**
     * Метод, метод вызываемый для шифрования пакета. Принимает два параметра: ключ для шифрования и текст пакета
     * @var callback
     */
    protected $aEncryptCallback;

    /**
     * Метод, метод вызываемый для дешифровки пакета. Принимает два параметра: ключ для шифрования и текст пакета
     * @var callback
     */
    protected $aDecryptCallback;

    /**
     * Указывает ключ шифрования для режима с внутренним шифрованием
     * @param $sKey
     */
    public function setKey($sKey) {

        $this->sKey = $sKey;

    }// func

    /**
     * Позволяет указать метод, который будет производить шифрование пакета запроса. Метод принимает два параметра:
     * 1. Ключ для шифрования
     * 2. Текст пакета
     * 3. В качестве результата должен возвращать зашифрованный текст запроса
     * @param callback|callable $aCalledMethod Вызываемый метод
     * @throws GatewayException
     */
    public function onEncrypt($aCalledMethod) {

        if(!is_callable($aCalledMethod)) throw new GatewayException('doEncrypt error: is not callable method');

        $this->aEncryptCallback = $aCalledMethod;
    }// func

    /**
     * Позволяет указать метод, который будет производить дешифровку пакета ответа. Метод принимает два параметра:
     * 1. Ключ для расшифовки
     * 2. Текст пакета ответа
     * 3. В качестве результата должен возвращать зашифрованный текст ответа
     * @param callback|callable $aCalledMethod Вызываемый метод
     * @throws GatewayException
     */
    public function onDecrypt($aCalledMethod) {

        if(!is_callable($aCalledMethod)) throw new GatewayException('doDecrypt error: is not callable method');

        $this->aDecryptCallback = $aCalledMethod;
    }// func

    /**
     * Запускает пользовательскую функцию шифрования
     * @param string $sData текст ответа
     * @return string|bool Возвращает зашифрованный текст ответа либо false
     */
    protected function doEncrypt($sData) {

        return call_user_func_array($this->aEncryptCallback, array($sData, $this->sKey));
    }// func

    /**
     * Запускает пользовательскую функцию расшифровки
     * @param string $sData Зашифрованный текст запроса
     * @return string|bool Возвращает расшифрованный текст запроса либо false
     */
    protected function doDecrypt($sData) {

        return call_user_func_array($this->aDecryptCallback, array($sData, $this->sKey));
    }// func

    /**
     * Генерируем подпись для запроса
     * @param string $sClientHost Домен, с которого пришел запрос
     * @param string $sBody ело запроса
     * @return string
     */
    protected function makeCertificate($sClientHost, $sBody) {

        return md5($this->sKey.$sClientHost.$sBody);

    }// func

    /**
     * Шифрует пакет данных в зависимости от настроек
     * @param string $sData Строка данных
     * @param bool $bIsCrypted Переменная в которой взводится либо опускается флаг шифрования
     * @throws GatewayException
     * @return bool|string Возвращает зашифрованный согласно режиму пакет данных либо
     */
    protected function encryptData($sData, &$bIsCrypted = false) {

        switch($this->iStreamType) {

            /* Не шифруем пакет */
            case self::StreamTypeNEncrypt:

                $bIsCrypted = false;
                break;

            /* Шифруем пакет */
            case self::StreamTypeEncrypt:

                if(!($this->sKey)) throw new GatewayException('Encrypt error: Key not found!');

                $sData = $this->doEncrypt($sData);

                if(!$sData) throw new GatewayException('Encrypt error: Callback function is not defined or not valid!');
                $sData = base64_encode($sData);
                $bIsCrypted = true;

                break;
        }// stream mode

        return $sData;
    }//func

    /**
     * Расшифровывает пакет согласно установкам сервера.
     * @param string $sData зашифрованный текст пакета
     * @param bool $bIsCrypted Заголовок пакета указатель на шифрование
     * @return array|null Возвращает расшифрованный массив данных либо null
     * @throws GatewayException В случае ошибки генерирует исключение типа GatewayException
     */
    protected function decryptData($sData, $bIsCrypted = false) {

        $aData = null;

        switch($this->iStreamType) {

            case self::StreamTypeEncrypt:

                if(!$bIsCrypted)  throw new GatewayException('Decrypt error: Wrong settings! Do not obtained Crypt flag!');

                $sData = base64_decode($sData);
                if(empty($sData)) throw new GatewayException('Decrypt error: Request has invalid format');

                if(!($this->sKey)) throw new GatewayException('Decrypt error: Key not found!');

                $sData = $this->doDecrypt($sData);
                if(empty($sData)) throw new GatewayException('Decrypt error: Callback function is not defined or not valid!');

                $aData = json_decode($sData, true);
                break;

            case self::StreamTypeNEncrypt:

                // нет данных на входе либо не доходит
                if($bIsCrypted)  throw new GatewayException('Decrypt error: Wrong settings! Obtained Crypt flag!');
                $aData = json_decode($sData, true);
                break;

        }// Stream mode

        return $aData;
    }// func

}// class
