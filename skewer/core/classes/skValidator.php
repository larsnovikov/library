<?php
/**
 * Класс с методами проверки основных типов данных
 * @class skValidator
 *
 * @author ArmiT, $Author: acat $
 * @version $Revision: 1623 $
 * @date $Date: 2013-01-30 13:13:55 +0400 (Ср, 30 янв 2013) $
 * @project Skewer
 * @package Kernel
 */
class skValidator {

    /** максимальная длина имени площадки */
    const maxHostNameLen = 64;

    /**
     * Паттерн, используемый для проверки URL на правильность.
     * Содержит placeholder {schemes}? который может быть заменен на конкретный признак протокола
     * перед обработкой регулярным выражением.
     * @var string
     * @see validSchemes
     */
    protected static $sURLPattern='/^{schemes}:\/\/(([A-Z0-9][A-Z0-9-]*)(\.[A-Z0-9][A-Z0-9-]*)+)/i';

    /**
     * Паттерн, используемый для проверки логинов на правильность.
     * В текущем виде доступен ввод только латиницы, цыфр и знаков дифиса и подчеркивания
     * @var string
     */
    protected static $sLoginPattern = '/^[-_a-z0-9]+$/i';

    /**
     * Список разрешенных в URL схем.
     * @var array
     **/
    protected static $aValidSchemes=array('http','https');

    /**
     * Схема, применяемая по-умолчанию
     * @var string
     */
    protected static $sDefaultScheme = 'http';

    /**
     * Максимально допустимый размер логина в символах
     * @var int
     */
    protected static $iMaxLoginSize = 255;

    /**
     * Минимально допустимый размер логина в символах
     * @var int
     */
    protected static $iMinLoginSize = 3;

    /* Methods */

    /**
     * Проверяет URL $sUrl на соответствие стандарту
     * @static
     * @param string $sUrl
     * @return bool|string
     */
    public static function isUrl($sUrl) {

        if(!is_string($sUrl) OR strlen($sUrl)>2000)// защита от DOS атак
            return false;

        if(static::$sDefaultScheme !== null AND strpos($sUrl,'://') === false)
            $sUrl = static::$sDefaultScheme.'://'.$sUrl;

        $sURLPattern = str_replace(
            '{schemes}',
            '('.implode('|',static::$aValidSchemes).')',
            static::$sURLPattern
        );

        if(preg_match($sURLPattern, $sUrl)) return $sUrl;

        return false;
    }

    /**
     * Проверяет логин $sLogin на недопустимые символы.
     * @static
     * @param string $sLogin Проверяемый логин
     * @return bool|string Возвращает текст логина в случае его корректности либо false в случае ошибки.
     */
    public static function isLogin($sLogin) {

        // ограничения по длине и типу
        $iStrLen = strlen( $sLogin );
        if(!is_string($sLogin) OR $iStrLen >static::$iMaxLoginSize) return false;
        if($iStrLen<static::$iMinLoginSize) return false;

        if(preg_match(static::$sLoginPattern, $sLogin)) return $sLogin;

        return false;
    }

    /**
     * Проверяет имя на корректность в качестве имени площадки
     * @param $sName
     * @return bool
     */
    public static function isValidHostName( $sName ) {
        return (bool)preg_match( '/^[a-z][-a-z0-9]{0,'.(self::maxHostNameLen-1).'}$/', $sName );
    }

    /**
     * Разбирает Строку IP фильтра на IP-адрес и маску
     * Принимает параметры следующего вида:
     * xxx.xxx.xxx.xxx[/x]
     * @example
     * '*' - разрешает все адреса
     * '192.168.1.1' - разрешает конкретный адрес
     * '192.168.0.0/16' - разрешает все IP адреса в 192.168 с маской 255.255.0.0
     *
     * @param string $sFilter Валидная строка фильтра
     * @return array Возвращает массив, состоящий из 2-х элементов. ip-адрес и mask-маска подсети
     * Если маска не указана, то в соответствующем ключе будет null
     * @throws ErrorException
     */
    protected static function parseIPFilter($sFilter) {

        $aFilter = array(
            'ip' => '*',
            'mask' => null
        );

        if($sFilter == '*') return $aFilter;

        preg_match_all('
		/^
			(?<ipaddr>
				\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}  # сам IPv4 адрес
			)
			(?:[\\/] 				# Эта группа нам в результате не нужна
				(?<mask>[0-9]{1,2}) # маска, если есть (возможные значения 1 - 32)
			 )?
		$/xUi', $sFilter, $aEntry,PREG_SET_ORDER);

        if(!count($aEntry)) throw new ErrorException('Error: Wrong filter format!');

        $aEntry = $aEntry[0];

        $aFilter['ip'] = $aEntry['ipaddr'];

        if ( isSet($aEntry['mask']) and !empty($aEntry['mask']) )
            $aFilter['mask']= (int)$aEntry['mask'];
        else
            $aFilter['mask'] = null;

        return $aFilter;
    }

    /**
     * Проверяет $IP  на вхождение в диапазон $sFilter. Возвращает true,
     * если $IP является валидным в диапазоне,
     * указанном в $sFilter либо false в противном случае.
     * @param string  $IP Валидная запись ip-адреса
     * @param string  $sFilter Валидная запись фильтра
     * @return bool
     * @throws ErrorException
     */
    public static function checkIP($IP, $sFilter) {

        $aFilter = self::parseIPFilter($sFilter);

        $Mask 	   = (int)$aFilter['mask'];
        $FilterIP  = $aFilter['ip'];
        /* Любой IP */
        if($FilterIP == '*') return true;

        $IP	= ip2long($IP);
        if ( !$IP )
            throw new ErrorException('Error: Wrong ip!');

        $FilterIP	= ip2long($FilterIP);
        if ( !$FilterIP )
            throw new ErrorException('Error: Wrong filter ip!');

        /* Конкретный IP */
        if( !$Mask )
            return $IP === $FilterIP;

        /* Подсеть */
        if($Mask < 1 OR $Mask > 32) throw new ErrorException('Error: Wrong submask value!');

        $subMask 	= (int)(pow(2,32) - pow(2,$Mask));

        return (($FilterIP & $subMask) == ($IP & $subMask))? true: false;

    }

}
