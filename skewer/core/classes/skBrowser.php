<?php

/**
 * Обёртка для библиотеки определения браузера
 * @class skBrowser
 * @extends Browser
 * @project Skewer
 * @package kernel
 *
 * @author Andy Mitrich, $Author: sapozhkov $
 * @version $Revision: 6 $
 * @date 16.03.12 10:24 $
 *
 */

require_once COREPATH.'libs/Browser/Browser.php';

class skBrowser extends Browser {

}//class