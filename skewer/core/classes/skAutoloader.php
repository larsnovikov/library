<?php
/**
 * Загрузка ресурсов по имени класса
 *
 * @class skAutoloader
 *
 * @author ArmiT, $Author: sapozhkov $
 * @version $Revision: 1346 $
 * @date $Date: 2012-12-03 11:31:54 +0400 (Пн., 03 дек. 2012) $
 *
 * @project Skewer
 * @package kernel
 *
 * @description
 * Паттерн: [name[2|4 additional]?][layer]?[type]?
 *
 * @example
 * Примеры загружаемых классов:
 * <br> $t = new skProcess();
 * <br> $t = new ProcessModule();
 * <br> $t = new ProcessApi();
 * <br> $t = new Process2GalleryModule();
 * <br> $t = new Process4GalleryApi();
 * <br> $t = new ProcessAdmModule();
 * <br> $t = new ProcessAdmApi();
 * <br> $t = new Process2GalleryAdmModule();
 * <br> $t = new Process4GalleryAdmApi();
 */

class skAutoloader {
    /**
     * Абсолютный путь до корня ядра
     * @var string
     */
    static protected $sCorePath = '';
    /**
     * Абсолютный путь до корня сборки
     * @var string
     */
    static protected $sBuildPath = '';
    /**
     * Типы допустимых функциональных расширений
     * @var array
     */
    static protected $sTypes = array(
        'Module',
        'Api',
        'Exception',
        'Interface',
        'Routing',
        'Hooks',
        'Install',
        'Mapper',
        'Model',
        'Ajax',
        'Service',
        'Prototype',
    );

    /**
     * Типы допустимых функциональных слоев
     * @var array
     */
    static protected $sLayers = array(
        'Adm',
        'Cms',
        'Tool',
        'Design',
    );
    /**
     * Список ранее загруженных классов
     * @var array
     */
    static protected $aLoadedClasses = array();
    /**
     * Закрываем возможность вызова напряумю
     */
    private function __construct() {

    }// constructor

    /**
     * Регистрация загрузчика согласно пути
     * @static
     * @param string $sCorePath путь директории ядра
     * @param string $sBuildPath путь директории сборки
     * @return bool
     */
    public static function registerPath($sCorePath,$sBuildPath) {

        self::$sCorePath = $sCorePath;
        self::$sBuildPath = $sBuildPath;
        spl_autoload_register(array(new self, 'autoload'));
        return true;
    }// func

     /**
     * Разбор имени файла, подгрузка файла класса.
     * @static
     * @param string $sClassName Имя класса
     * @return bool
     */
    static public function autoload($sClassName) {

        /**
         * [moduleName] - название модуля
         * [mode] - слой
         * [type] - тип файла (Api/skModule/other)
         */
        preg_match_all('/^(sk)?+(?<moduleName>[-_a-z0-9]+)((2|4)[-_a-z0-9]+)?(?<mode>'.implode('|',self::$sLayers).')?(?<type>'.implode('|',self::$sTypes).'){1}?$/Ui', $sClassName, $aEntry, PREG_SET_ORDER);

        // флаг класса ядра
        $isCore = strpos($sClassName,'sk')===0;

        if($aEntry) {

            $aEntry = $aEntry[0];

            // определение параменных
            $sModuleName = $aEntry['moduleName'];
            $sMode = $aEntry['mode'];
            $sType = $aEntry['type'];

            // префикс слоя
            //  имена классов - CamelCase а директории в нижнем регистре
            $sModePath = !empty($sMode) ? strtolower($sMode).'/' : '';

            // если найдено составное имя
            if ( $isCore ) {

                // выбор имени файла по типу
                switch ($sType) {

                    case 'Mapper':
                        $sFilePath = self::$sCorePath.'mappers/'.$sClassName.'.php';
                        break;

                    case 'Interface':
                        $sFilePath = self::$sCorePath.'interfaces/'.$sClassName.'.php';
                        break;

                    default:
                        $sFilePath = self::$sCorePath.'classes/'.$sClassName.'.php';
                        break;

                }

            } else {

                // выбор имени файла по типу
                switch ($sType) {

                    default:
                        $sFilePath = self::$sBuildPath.$sModePath.'modules/'.$sModuleName.'/'.$sClassName.'.php';
                        break;
                    case 'Exception':
                        $sFilePath = self::$sBuildPath.'exceptions/'.$sClassName.'.php';
                        break;
                    case 'Interface':
                        $sFilePath = self::$sBuildPath.'interfaces/'.$sClassName.'.php';
                        break;
                    case 'Prototype':
                        $sFilePath = self::$sBuildPath.'prototypes/'.$sClassName.'.php';
                        break;
                    case 'Mapper':
                        $sFilePath = self::$sBuildPath.$sModePath.'modules/'.$sModuleName.'/'.$sClassName.'.php';
                        if ( !file_exists($sFilePath) )
                            $sFilePath = self::$sBuildPath.'mappers/'.$sClassName.'.php';
                        break;

                }

            }

        } else {

            if ( $isCore )
                $sFilePath = self::$sCorePath.'classes/'.$sClassName.'.php';
            else
                $sFilePath = self::$sBuildPath.'classes/'.$sClassName.'.php';

        }

        if(!$sFilePath or !file_exists($sFilePath)) return false;

        self::$aLoadedClasses[$sClassName] = $sFilePath;

        /** @noinspection PhpIncludeInspection */
        require($sFilePath);

        return true;

    }// func

    /**
     * Получить путь к файлу класса по его названию
     * @static
     * @param string $sClassName Имя класса
     * @return bool|string
     */
    static public function getClassPath($sClassName) {
        return (isSet(self::$aLoadedClasses[$sClassName]))? self::$aLoadedClasses[$sClassName]: false;
    }// func

    /**
     * Возвращает список ранее загруженных файлов
     * @static
     * @return array Список ранее загруженных классов
     */
    static public function getLoadedClasses() {
        return self::$aLoadedClasses;
    }// func
}// class
