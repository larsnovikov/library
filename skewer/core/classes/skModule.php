<?php
/**
 * Прототип модуля
 *
 * @class skModule
 * @project Canape 3.0 (skewer)
 * @package kernel
 * @implements skModuleInterface
 * @author ArmiT $Author: acat $
 * @version $Revision: 1438 $
 * @date $Date: 2012-12-19 14:43:58 +0400 (Ср., 19 дек. 2012) $
 *
 */
abstract class skModule implements skModuleInterface {

    /**
     * Экземпляр Контекста вызова
     * @var null|skContext
     */
    protected $oContext = NULL;

    /**
     * Директория с CSS файлами модуля от корня директории модуля
     * @var string
     */
    private $sCSSDir = 'css';

    /**
     * Директория с шаблонами модуля от корня директории модуля
     * @var string
     */
    private $sTemplateDir = 'templates';

    /**
     * Директория с JS файлами модуля от корня директории модуля
     * @var string
     */
    private $sJSDir = 'js';

    /**
     * Создает экземпляр модуля
     * @param skContext $oContext Передаваемый контекст
     */
    final public function __construct(skContext $oContext) {

        $sClassName  = get_class($this);
        $sModulePath = skAutoloader::getClassPath($sClassName);

        $this->oContext = $oContext;

        $this->oContext->setModulePath($sModulePath);
        /** @noinspection PhpParamsInspection */
        $this->oContext->setModuleDir(dirname($sModulePath));
        $this->oContext->setModuleWebDir( '/skewer_build'.substr(dirname($sModulePath),strlen(BUILDPATH)-1) );
        $this->oContext->setTplDirectory( $this->getTplDirectory() );

        // слой модуля
        $sModuleDir = substr(dirname($sModulePath),strlen(BUILDPATH));
        $this->oContext->setModuleLayer(substr($sModuleDir,0,strpos($sModuleDir,'/')));

        $aPathItems = explode(DIRECTORY_SEPARATOR, trim(dirname($sModulePath), DIRECTORY_SEPARATOR));
        if(count($aPathItems))
            $this->oContext->setModuleName(end($aPathItems));

        $aOverlayParams = $oContext->getParams();

        $this->overlayParams($aOverlayParams);

        return true;
    }// func

    /**
     * Перекрывает свойства классу модуля
     * @param array|null $aOverlayParams
     * @return bool
     */
    final public function overlayParams($aOverlayParams = null) {


        if(count($aOverlayParams)) {

            $oRef = new ReflectionClass(get_class($this));
            $aPropValues = $oRef->getDefaultProperties();
            $aProperties = $oRef->getProperties(ReflectionProperty::IS_PUBLIC | ReflectionProperty::IS_PROTECTED);

            if(count($aProperties))
                foreach($aProperties as $oProperty) {

                    if(!($oProperty instanceof ReflectionProperty)) continue;

                    if($oProperty->name == 'oContext') continue;
                    $sName = $oProperty->name;
                    $this->$sName = $aPropValues[$sName];
                    if(isSet($aOverlayParams[$sName]))
                        $this->$sName = $aOverlayParams[$sName];

                }// each

        }

    }// func

    /**
     * Закрытый деструктор без возможности перекрытия
     */
    final public function __destruct() {

    }// func

    /**
     * Прототип - выполняется до вызова метода Execute
     * @return bool
     */
    public function init() {

    }// func

    /**
     * Прототип - вызывается после метода Execute
     * @return bool
     */
    public function shutdown() {

    }// func

    /**
     * Разрешить выполнение модуля
     * @return bool
     */
    public function allowExecute() {
        return true;
    }// func

    /**
     * Добавляет данные для вывода в шаблонизатор
     * @final
     * @param string $sLabel Метка для вставки данных
     * @param mixed $mData Данные
     * @return bool
     */
    final public function setData($sLabel, $mData) {
        $this->oContext->setData($sLabel, $mData);
        return true;
    }// func

    /**
     * Возвращает Данные, установленные модулем в процессе выполнения
     * @param string $sLabel Если указан параметр sLabel -  будет возвращено только то значение,
     * которое к нему прикреплено.
     * @return bool
     */
    final public function getData($sLabel = '') {

        return $this->oContext->getData($sLabel);
    }// func

    /**
     * Устанавливает шаблон для вывода
     * @final
     * @param string $sTemplate Шаблон для рендеринга данных
     * @return bool
     */
    final public function setTemplate($sTemplate) {

        $this->oContext->setTemplate($sTemplate);
        return true;
    }// func

    /**
     * Устанавливает вывод, обработанный шаблонизатором
     * @final
     * @param string $sOut
     * @return bool
     */
    final public function setOut($sOut) {
        $this->oContext->setOut($sOut);
        return true;
    }// func

    /**
     * Указывает тип шаблонизатора
     * @final
     * @param int $iParserType
     * @return bool
     */
    final public function setParser($iParserType) {
        $this->oContext->setParser($iParserType);
        return true;
    }// func

    /**
     * Возвращает путь к файлу модуля
     * @final
     * @return string
     */
    final public function getModulePath() {
        return $this->oContext->getModulePath();
    }// func

    /**
     * Возвращает путь к директории модуля
     * @final
     * @return string
     */
    final public function getModuleDir() {
        return $this->oContext->getModuleDir();
    }// func

    /**
     * Возвращает web путь к директории модуля
     * @final
     * @return string
     */
    final public function getModuleWebDir() {
        return $this->oContext->getModuleWebDir();
    }// func

    /**
     * Ищет разобранный GET/POST/JSON целочисленный параметр.
     * @param string $sName Имя исходного параметра
     * @param int $mDefault Значение, подставляемое по-умолчанию
     * @return int
     */
    final public function getInt($sName, $mDefault = 0) {

        $iVal=$mDefault;
        /**
         * Ищем в POST и JSON параметрах
         * Перекрываем GET параметрами
         */
        if(skRequest::getInt($this->oContext->sLabelPath, $sName, $iVal)) return $iVal;
        if($this->oContext->oProcess->oRouter->getInt($sName,$iVal)) return $iVal;
        /** @todo Добавить проверку (Запрос через skRequest) */
        //if($this->oContext->oProcessor->oRouter->getInt($sName,$iVal)) return $iVal;

        return $iVal;
    }// func

    /**
     *  Ищет разобранный GET/POST/JSON строковый параметр.
     * @param string $sName Имя исходного параметра
     * @param string $mDefault Значение, подставляемое по-умолчанию
     * @return string
     */
    final public function getStr($sName, $mDefault = '') {

        $sVal=$mDefault;
        /**
         * Ищем в POST и JSON параметрах
         * Перекрываем GET параметрами
         */
        if(skRequest::getStr($this->oContext->sLabelPath, $sName, $sVal)) return $sVal;
        if($this->oContext->oProcess->oRouter->getStr($sName,$sVal)) return $sVal;
        /** @todo Добавить проверку (Запрос через skRequest) */
        //if($this->oContext->oProcessor->oRouter->getStr($sName,$sVal)) return $sVal;

        return $sVal;
    }// func


    /**
     *  Ищет разобранный GET/POST/JSON неопределенный параметр.
     * @param string $sName Имя исходного параметра
     * @param string $mDefault Значение, подставляемое по-умолчанию
     * @return mixed
     */
    final public function get($sName, $mDefault = '') {

        $sVal=$mDefault;
        /**
         * Ищем в POST и JSON параметрах
         * Перекрываем GET параметрами
         */
        if(skRequest::get($this->oContext->sLabelPath, $sName, $sVal)) return $sVal;
        if($this->oContext->oProcess->oRouter->getStr($sName,$sVal)) return $sVal;
        /** @todo Добавить проверку (Запрос через skRequest) */
        //if($this->oContext->oProcessor->oRouter->getStr($sName,$sVal)) return $sVal;

        return $sVal;
    }// func

    /**
     * Сохраняет во входной массив (GET/POST/JSON) значение
     * @param $sName
     * @param $mValue
     */
    final function set( $sName, $mValue ) {
        skRequest::set($this->oContext->sLabelPath, $sName, $mValue);
    }

    /**
     * Возвращает имя текущего модуля
     * @return string
     */
    final public function getModuleName() {
        return $this->oContext->getModuleName();
    }

    /**
     * отдает имя слоя
     * @return string
     */
    public function getLayerName() {
        return $this->oContext->getModuleLayer();
    }

    final public function getModuleNameAdm() {

        return $this->getModuleName().'Adm';
    }

    /**
     * Устаналивает имя текущего модуля
     * @param string $sModuleName Имя модуля
     * @return bool
     */
    final public function setModuleName($sModuleName) {
        return $this->oContext->setModuleName($sModuleName);
    }// func

    /**
     * Добавляет новый обработчик события $sEventName
     * @param string $sEventName Название события
     * @param string $sHandlerMethod Метод-обработчик в классе текущего модуля
     * @return bool
     */
    final public function addEventListener($sEventName, $sHandlerMethod){

        return skProcessor::addEventListener($sEventName, $this->oContext->sLabelPath, $sHandlerMethod);

    }// func

    /**
     * Ставит Событие в очередь исполнения
     * @param string $sEventName Название события
     * @param array $aArguments Аргументы, передаваемые обработчикам
     * @return void
     */
    final public function fireEvent($sEventName, $aArguments = array()){

        /** @noinspection PhpInconsistentReturnPointsInspection */
        return skProcessor::fireEvent($sEventName, $aArguments);

    }// func




    /**
     * Возвращает экземпляр процесса по цепочке меток до него
     * @final
     * @param string $sPath Путь от корневого процесса до искомого
     * @param integer $iStatus Статус искомого процесса. psAll - выбрать все
     * @return \skProcess|int
     */
    final public function getProcess($sPath, $iStatus = psComplete) {
        /** @noinspection PhpUndefinedMethodInspection */
        return skProcessor::getProcess($sPath, $iStatus);
    }// func

    /**
     * Adding child process in parent process
     * @param skContext $oContext
     * @return array|bool
     */
    public function addChildProcess(skContext $oContext) {
        return $this->oContext->oProcess->addChildProcess($oContext);
    }// func

    /**
     * Возвращает список ссылок на дочерние процессы.
     * @return array
     */
    final public function getChildProcesses() {
        return $this->oContext->oProcess->processes;
    }// func

    /**
     * Возвращает ссылку на дочерний процесс по метке $sLabel его вызова
     * @param string $sLabel Метка вызова дочернего процесса
     * @return bool|skProcess Возвращает ссылку на процесс либо false
     */
    final public function getChildProcess($sLabel) {
        return (isSet($this->oContext->oProcess->processes[$sLabel]))? $this->oContext->oProcess->processes[$sLabel]: false;
    }// func

    /**
     * Устанавливает статус $iStatus дочернему процессу в метке вызова $sLabel
     * @param string $sLabel Название метки вызова для дочернего процесса
     * @param integer $iStatus Константа статуса
     * @return bool Возвращает true если процесс найден и false в противном случае
     */
    final public function setChildProcessStatus($sLabel, $iStatus) {
        if(isSet($this->oContext->oProcess->processes[$sLabel])) {
            /** @noinspection PhpUndefinedMethodInspection */
            $this->oContext->oProcess->processes[$sLabel]->setStatus($iStatus);
            return true;
        }
        return false;
    }// func

    /**
     * Удаляет подчиненный процесс
     * @param $sLabel
     * @return bool
     */
    final public function removeChildProcess( $sLabel ) {
        return $this->oContext->oProcess->removeChildProcess( $sLabel );
    }

    /**
     * Удаляет все подчиненные процессы
     * @return int количество удаленных процессов
     */
    final public function removeAllChildProcess() {

        $aProcesses = $this->getChildProcesses();

        // удалить все по очереди
        /* @var $oProcess skProcess */
        foreach ( $aProcesses as $oProcess ) {
            $this->removeChildProcess( $oProcess->getLabel() );
        }

        return count($aProcesses);

    }

    /**
     * Устанавливает новый параметр окружения с именем $sName и значением $mValue
     * @param string $sName Имя параметра окружения
     * @param string $mValue Значение параметра окружения
     * @return mixed
     */
    final public function setEnvParam($sName, $mValue) {

        /** @noinspection PhpUndefinedMethodInspection */
        return skProcessor::setEnvParam($sName, $mValue);
    }// func

    /**
     * Возвращает параметр окружения $sName либо $mDefault в случае, если параметр отсутствует.
     * @param string $sName Имя параметра окружения
     * @param bool|mixed $mDefault Значение, возвращаемое, если параметр не был найден.
     * @return mixed
     */
    final public function getEnvParam($sName, $mDefault = false) {
        /** @noinspection PhpUndefinedMethodInspection */
        return skProcessor::getEnvParam($sName, $mDefault);
    }// func

    /**
     * Возвращает список параметров окружения (обших переменных в рамках дерева процессов)
     * @return mixed
     */
    final public function getEnvParamsList() {
        /** @noinspection PhpUndefinedMethodInspection */
        return skProcessor::getEnvParamsList();
    }// func

    /**
     * Добавляет в список вывода на страницу JS файл $sFileName. В условии $sCondition, если задано.
     * @param string $sFileName - Имя JS-файла в директории текущего модуля
     * @param bool $sCondition Условие (Используется совместно с <!--[if ]><![endif]-->)
     * @return bool
     */
    final public function addJSFile($sFileName, $sCondition = false){

        if ( $sFileName[0] === '/' )
            $sPath = '';
        else
            $sPath = $this->oContext->getModuleWebDir().DIRECTORY_SEPARATOR.$this->sJSDir.DIRECTORY_SEPARATOR;
        return skLinker::addJSFile($sPath.$sFileName, $sCondition);
    }// func

    /**
     * Добавляет в список вывода на страницу CSS файл $sFileName. В условии $sCondition, если задано.
     * @param string $sFileName - Имя CSS-файла в директории текущего модуля
     * @param bool $sCondition Условие (Используется совместно с <!--[if ]><![endif]-->)
     * @return bool
     */
    final public function addCSSFile($sFileName, $sCondition = false){

        $sPath = DIRECTORY_SEPARATOR.$this->sCSSDir.DIRECTORY_SEPARATOR;
        return skLinker::addCSSFile($this->oContext->getModuleWebDir().$sPath.$sFileName, $sCondition);
    }// func

    /**
     * Возвращает путь к директории хранения JS файлов модуля
     * @return string
     */
    final public function getJSDirectory(){

        return $this->sJSDir;

    }// func

    /**
     * Изменяет путь к директории хранения JS файлов модуля
     * @param $sDir Директория расположения JS файлов модуля
     * @return string
     */
    final public function setJSDirectory($sDir){

        return $this->sJSDir = $sDir;

    }// func

    /**
     * Возвращает путь к директории хранения CSS файлов модуля
     * @return string
     */
    public function getCSSDirectory(){

        return $this->sCSSDir;

    }// func

    /**
     * Изменяет путь к директории хранения CSS файлов модуля
     * @param $sDir Директория расположения CSS файлов модуля
     * @return string
     */
    public function setCSSDirectory($sDir){

        return $this->sCSSDir = $sDir;

    }// func

    /**
     * Возвращает путь к директории хранения шаблонов модуля
     * @return string
     */
    public function getTplDirectory(){

        return $this->sTemplateDir;

    }// func

    /**
     * Изменяет путь к директории хранения шаблонов модуля
     * @param $sDir Директория расположения CSS файлов модуля
     * @return string
     */
    public function setTplDirectory($sDir){

        $this->oContext->setTplDirectory( $sDir );
        return $this->sTemplateDir = $sDir;

    }// func

    /**
     * Генирирует массив для постраничного вывода.
     * @param integer $iPage Текущая страница постраничного
     * @param integer $iCount Количество элементов в списке (например, товаров в категории)
     * @param integer $iSectionId Раздел для вывода постраничного списка
     * @param array $aURLParams массив дополнительных GET параметров
     * @param array $aParams Параметры настройки внешнего вида постраничного см. \skPaginator
     * @param string $sLabel Метка вывода сгенерированного массива в шаблон.
     * @return string
     */
    final public function getPageLine($iPage, $iCount, $iSectionId, $aURLParams = array(), $aParams = array(), $sLabel = 'aPages') {

        $aURL[$this->oContext->sClassName] = (count($aURLParams))? $aURLParams: array();

        return $this->oContext->setData($sLabel, Paginator::getPageLine($iPage, $iCount, $iSectionId, $aURL, $aParams));

    }// func

    /**
     * Установить дополнительный JSON Header в response package от модуля
     * @param string $sKey Заголовок
     * @param mixed $mValue Значение заголовка
     */
    final public function setJSONHeader( $sKey, $mValue ) {
        $this->oContext->setJSONHeader( $sKey, $mValue );
    }

    /**
     * Удалить дополнительный JSON Header в response package от модуля
     * @param string $sKey Заголовок
     * @return bool
     */
    final public function unsetJSONHeader( $sKey ) {
        return $this->oContext->unsetJSONHeader( $sKey );
    }

    /**
     * Получить дополнительный JSON Header по названию $sKey
     * @param $sKey
     * @return null|mixed
     */
    final public function getJSONHeader( $sKey ) {
        return $this->oContext->getJSONHeader( $sKey );
    }

    /**
     * Отрендерить шаблон, вернуть строку с результатом
     * @param $sTemplate - шаблон
     * @param array $aData - массив для парсинга
     * @return string
     */
    protected function renderTemplate( $sTemplate, $aData=array() ) {

        return skParser::parseTwig( $sTemplate, $aData, $this->getModuleDir().$this->getTplDirectory().DIRECTORY_SEPARATOR );

    }

    final public function setPage($iPage = page404) {

        /** @noinspection PhpUndefinedMethodInspection */
        return skProcessor::setPage($iPage);

    }// func

    /**
     * Возвращает параметр конфигурации модуля по ключу $key в слое $sLayer
     * @param string $key
     * @param string $sLayer
     * @return mixed
     */
    final protected function getConfigParam($key='', $sLayer = ''){

        $key = $this->getModuleName().'.'.$key;

        if ( empty($sLayer) ) $sLayer = skProcessor::getLayerName();

        return skConfig::getModuleParam($key, $sLayer);

    }

    /**
     * Возвращает текущий режим отображения страницы
     * @return string
     */
    final public function getViewMode(){

        /** @noinspection PhpUndefinedMethodInspection */
        return ( skProcessor::getEnvParam('_viewMode') )? skProcessor::getEnvParam('_viewMode'): 'default';
    }

    public function addCriticalReport($sTitle, $sDescription, $iLogType, $sCalledClass, $sTitleCalled = ''){
        return skLogger::addCriticalReport($sTitle, $sDescription, $iLogType, $sCalledClass, $sTitleCalled);
    }

    public function addWarningReport($sTitle, $sDescription, $iLogType, $sCalledClass, $sTitleCalled = ''){
        return skLogger::addWarningReport($sTitle, $sDescription, $iLogType, $sCalledClass, $sTitleCalled);
    }

    public function addErrorReport($sTitle, $sDescription, $iLogType, $sCalledClass, $sTitleCalled = ''){
        return skLogger::addErrorReport($sTitle, $sDescription, $iLogType, $sCalledClass, $sTitleCalled);
    }

    public function addNoticeReport($sTitle, $sDescription, $iLogType, $sCalledClass, $sTitleCalled = ''){
        return skLogger::addNoticeReport($sTitle, $sDescription, $iLogType, $sCalledClass, $sTitleCalled);
    }

}// class
