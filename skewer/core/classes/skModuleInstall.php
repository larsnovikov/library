<?php
/**
 * Класс-прототип файлов установки, обновления и удаления модулей.
 * @class skModuleInstall
 * @extends skUpdateHelper
 *
 * @author ArmiT, $Author: armit $
 * @version $Revision: 249 $
 * @date $Date: 2012-06-01 12:41:33 +0400 (Пт., 01 июня 2012) $
 * @project Skewer
 * @package Kernel
 */ 
class skModuleInstall {

    /**
     * Экземпляр для работы с RDBMS
     * @var null|skDB
     */
    private $oDB = null;

    /**
     * Конструктор
     */
    public function __construct() {
        global $odb;

        $this->oDB = $odb;
    }

    /**
     * Прототип функции инициализации перед выполнением операций над модулем.
     * @return bool
     */
    public function init() {

    }// func

    /**
     * Прототип функции для инструкций установки модуля
     * @return bool
     */
    public function install() {

    }// func

    /**
     * Прототип функции для инструкций удаления модуля
     * @return bool
     */
    public function deinstall() {

    }// func

    /**
     * Провести апгрейд модуля с версии до версии
     * @param $iPreviosVersion
     * @param $iNextVersion
     */
    public function upgrade($iPreviosVersion, $iNextVersion) {

    }// func

}// class
