<?php
/**
 *
 * @class skMapperPrototype
 *
 * @author ArmiT, $Author: kolesnikov $
 * @version $Revision: 713 $
 * @date $Date: 2012-08-29 11:00:48 +0400 (Ср., 29 авг. 2012) $
 * @project JetBrains PhpStorm
 * @package kernel
 */
abstract class skMapperPrototype extends skModelPrototype {

    /**
     * @var Имя таблицы
     */
    protected static $sCurrentTable;

    /**
     * Имя ключевого поля
     * @var string
     */
    protected static $sKeyFieldName = 'id';

    /**
     * @static Метод возвращает имя рабочей таблицы
     * @return
     */
    public static function getCurrentTable() {

        return static::$sCurrentTable;
    }

    public static function getKeyFieldName() {
        return static::$sKeyFieldName;
    }


    /**
     * Метод сохранения записи
     * @static
     * @param array $aInputData
     * @return bool|int
     */
    public static function saveItem( $aInputData = array() ) {

        if ( !$aInputData ) return false;

        // имя ключевого поля - обязательное
        $sKeyFieldName = static::getKeyFieldName();

        // флаг наличия поля идентификатора
        $bFoundId = false;
        // значение поля идентификатора
        $mIdKeyVal = 'NULL';

        global $odb;
        $aData      = array();
        $aFields    = array();
        /*
         * Проходимся по массиву переданных извне параметров и, при совпадении варианта switch с параметром входного массива,
         * добавляем соответствующий элемент к массиву плейсхолдеров и к строке запроса
         */
        foreach( $aInputData as $sKey=>$sValue ){

            // запросить описание поля
            $aFieldParams = static::getParameter($sKey);

            // если поле найдено
            if ( $aFieldParams ){

                // если поле - первичный ключ
                if ( $sKeyFieldName === $sKey ) {

                    if ( $sValue ){
                        /** @fixme Что это? (real_escape_string)  */
                        $mIdKeyVal = $odb->real_escape_string($sValue);
                        $bFoundId = true;
                    }
                    else {

                        $mIdKeyVal = 'NULL';
                    }

                } else {

                    // тип данных
                    $sType = $aFieldParams['type'];

                    if ( is_null($sValue) ) {
                        $sType = 'q';
                        $sValue = 'NULL';
                    }

                    // добавить в список элементов на сохранение
                    $aFields[] = "`$sKey`=[$sKey:{$sType}]";

                    // добавить в массив для меток в запросе
                    $aData[$sKey] = $sValue;

                }

            }

        }//foreach $aNewsData

        if ( sizeof($aFields) ){

            // Если массив с элементами запроса не пуст - собираем из него строку
            $sFields = implode(',', $aFields);

            // имя паблицы
            $aData['sTableName'] = static::getCurrentTable();

            // имя и значение ключевого поля
            $aData['sKeyFieldName'] = $sKeyFieldName;
            $aData['mIdKeyVal'] = $mIdKeyVal;

            // если найден id
            if ( $bFoundId ) {

                $sQuery = "
                    UPDATE `[sTableName:q]`
                    SET $sFields
                    WHERE `[sKeyFieldName:q]`=[mIdKeyVal:q]";

            } else {

                $sQuery = "
                    INSERT INTO `[sTableName:q]`
                    SET $sFields
                    ON DUPLICATE KEY UPDATE
                    $sFields;";

            }

            //echo "<XMP>"; print_r($odb->debug($sQuery,$aData)); echo "</XMP>";
            // выполнение запроса
            $odb->query($sQuery,$aData);

            // если сохранение
            if ( $bFoundId ) {

                if ( $odb->affected_rows!=-1 )
                    return $aInputData[$sKeyFieldName];
                else
                    return false;

            } else {
                // вставка - вернуть новый id
                return $odb->insert_id;
            }

        }

        return false;
    }

    /**
     * @static Метод получения списка записей по фильтру
     *
     * Фильтр имеет вид:
     *      array = (
     *          'select_fields'     => array( <имя поля 1>, ... ),
     *          'where_condition'   => array( <имя поля> => array (
     *                                      'sign' => <операция сравнения>,
     *                                      'value' => <значение>
     *                                       )
     *                                 ),
     *          'limit'             => array(
     *              'start' => <начальный элемент>,
     *              'count' => <количество в выборке>
     *          )
     *          'order' => array(
     *              'field' => <имя поля>,
     *              'way' => <направление сортировки>
     *          )
     *
     * @param $aFilter array
     * @return array
     */
    public static function getItems( $aFilter=array() ){

        global $odb;

        // Получаем имя класса, вызывающего метод
        $sCurrentClass = get_called_class();
        $aData = array();

        $sSelectFields = '*';
        // Обрабатываем элементы фильтра, перечисляющие выбираемые поля
        if ( isset($aFilter['select_fields']) && sizeof($aFilter['select_fields']) ){

            $aSelectFields = array();
            
            foreach( $aFilter['select_fields'] as $sValue ){

                // Получаем тип поля
                /** @noinspection PhpUndefinedMethodInspection */
                $aValueParams = $sCurrentClass::getParameter($sValue);

                if ( is_array($aValueParams) && sizeof($aValueParams) ){

                    // Собираем поля в массив
                    $aSelectFields[] = $sValue;
                }

            }
            // Собираем условие по выбираемым полям
            if ( sizeof($aSelectFields) ) $sSelectFields = '`'.implode('`, `', $aSelectFields).'`';
            else return false;
        }


        // Обрабатываем элементы фильтра, формирующие условие
        $sWhereCondition = '';
        if ( isset($aFilter['where_condition']) && $aFilter['where_condition'] ){

            $aWhereCondition = array();
            
            foreach( $aFilter['where_condition'] as $sKey=>$aValue ){

                // Получаем тип поля
                /** @noinspection PhpUndefinedMethodInspection */
                $aValueParams = $sCurrentClass::getParameter($sKey);


                if ( is_array($aValueParams) && sizeof($aValueParams) ){

                    // Собираем условие WHERE для выборки элементов
                    switch( $aValue['sign'] ){

                        case 'BETWEEN':
                            $aWhereCondition[] = "`$sKey` BETWEEN [".$sKey.'_from'.":s] AND [".$sKey.'_to'.":s]";
                            $aData[$sKey.'_from'] = $aValue['value'][0];
                            $aData[$sKey.'_to'] = $aValue['value'][1];
                            break;

                        case 'IN':
                            $aWhereCondition[] = "`$sKey` {$aValue['sign']} ([$sKey:q])";
                            $aData[$sKey] = $aValue['value'];
                        break;
                        default:
                            $aWhereCondition[] = "`$sKey`{$aValue['sign']}[$sKey:{$aValueParams['type']}]";
                            $aData[$sKey] = $aValue['value'];
                        break;
                    }
                }
            }
            if ( sizeof($aWhereCondition) ) $sWhereCondition = "WHERE ".implode(' AND ', $aWhereCondition);
            else return false;
        }

        $sOrderCondition = '';

        if ( isset($aFilter['order']) ){

            $sOrderCondition = " ORDER BY {$aFilter['order']['field']} {$aFilter['order']['way']} ";
        }

        $sLimitCondition = '';

        // Обрабатываем ограничение по количеству выбираемых элементов
        if ( isset($aFilter['limit']) && $aFilter['limit'] ){

            $aData['start_limit'] = $aFilter['limit']['start'];
            $aData['count_limit'] = $aFilter['limit']['count'];
            $sLimitCondition = " LIMIT [start_limit:i], [count_limit:i]";
        }

        /** @noinspection PhpUndefinedMethodInspection */
        $sTableName = $sCurrentClass::getCurrentTable();

        // SQL_CALC_FOUND_ROWS используется для подсчета общего количества записей
        $sQuery = "
            SELECT
            SQL_CALC_FOUND_ROWS
                $sSelectFields
            FROM
                `$sTableName`
            $sWhereCondition
            $sOrderCondition
            $sLimitCondition;";

        //var_dump($odb->debug($sQuery, $aData));
        $rResult = $odb->query($sQuery, $aData);

        if ( !$rResult ) return false;

        // Собирается результирующий массив
        $aItems = array('items'=>array());
        
        while( $aRow = $rResult->fetch_array(MYSQLI_ASSOC) ){

            // приведение к нужному типу
            foreach ( $aRow AS $sFieldName => $mFieldVal ) {
                $aParam = self::getParameter($sFieldName);
                if ( $aParam and $aParam['type'] === 'i' and !is_null($mFieldVal) )
                    $aRow[$sFieldName] = (int)$mFieldVal;
            }

            // выдача записи
            $aItems['items'][] = $aRow;

        }

        // Получаем общее число записей из таблицы
        $sCountQuery = "SELECT FOUND_ROWS() as `rows`;";
        $aCount = $odb->getRow($sCountQuery);
        $aItems['count'] = $aCount['rows'];

        return $aItems;
    }

    /**
     * Возвращает требуемую по id или фильтру запись в виде одномерного массива
     * @static
     * @param int|array $mFilter - фильтр по параметрам
     * @return array
     */
    public static function getItem( $mFilter=array() ) {

        // если пришло число
        if ( is_numeric($mFilter) ) {
            // запросить по id
            $aFilter = array();
            $aFilter['where_condition'][static::getKeyFieldName()] = array(
                'sign' => '=',
                'value' => $mFilter
            );
        } elseif ( is_array($mFilter) ) {
            // если массив - использовать его как фильтр
            $aFilter = $mFilter;
        } else {
            // иначе отдать пустой массив
            return array();
        }

        // если нет ограничения по количеству
        if ( !isset($aFilter['limit']) ) {
            // добавить его
            $aFilter['limit'] = array(
                'start' => 0,
                'count' => 1
            );
        }

        // запросить элементы по фильтру
        $aItems = static::getItems( $aFilter );

        // если нашлись записи
        if ( $aItems['count'] ) {
            // отдать первую запись
            return $aItems['items'][0];
        } else {
            // наче вернуть пустой массив
            return array();
        }

    }

    /**
     * Удаление записи
     * @static
     * @param mixed $mFilter - id или массив фильтра
     * @return int|bool - false - ошибка, int - сколько удалено
     */
    public static function delItem( $mFilter ) {

        // имя ключевого поля - обязательное
        $sKeyFieldName = static::getKeyFieldName();

        if ( !$sKeyFieldName ) return false;

        if ( is_array($mFilter) ) {
            $iId = isset($mFilter[$sKeyFieldName]) ? (int)$mFilter[$sKeyFieldName] : 0;
        } else {
            $iId = (int)$mFilter;
        }

        if ( !$iId ) return false;

        $sQuery = '
            DELETE
            FROM `[table_name:q]`
            WHERE `[keyField:q]`=[id:i];
        ';

        $sTableName = static::getCurrentTable();

        global $odb;

//        var_dump($odb->debug($sQuery, array(
//            'table_name' => $sTableName,
//            'keyField' => $sKeyFieldName,
//            'id' => $iId
//        )));

        $odb->query($sQuery, array(
            'table_name' => $sTableName,
            'keyField' => $sKeyFieldName,
            'id' => $iId
        ));

        return $odb->affected_rows;

    }// function
}// class
