<?php
/**
 * Proxy Класс для предоставления доступа к менеджерам процессов
 * @class skProcessor
 *
 * @author ArmiT, $Author: acat $
 * @version $Revision: 1437 $
 * @date $Date: 2012-12-19 14:36:22 +0400 (Ср., 19 дек. 2012) $
 * @project Skewer
 * @package kernel
 * @static
 * @singleton
 */
class skProcessor {

    /**
     * Экземпляр выбранного менеджера процессов
     * @var null|\skProcessorPrototype
     */
    static private $oProcessor = null;
    /**
     * Хранит экземпляр текущего класса
     * @var null|\skProcessor
     */
    static private  $oInstance = NULL;

    /**
     * Название слоя вызова
     * @var string
     */
    static private $sLayerName = '';

    /**
     * Суффикс класса-процессора для слоя
     * @var string
     */
    static private $sClassSuffix = 'Processor';

    /**
     * Список before хуков для слоя
     * @var array
     */
    static private $aEventBeforeHooks = array();

    /**
     * Список обработчиков событий
     * @var array
     */
    static private $aEventListeners = array();

    /**
     * Список after хуков
     * @var array
     */
    static private $aEventAfterHooks = array();

    /**
     * Временный массив выполняемых событий
     * @var array
     */
    static private $aEvents = array();


    /**
     * Массив ссылок на классы-хелперы в шаблонах
     * @var array
     */
    static private $aParserHelpers = array();

    /**
     * Создает экземпляр процессора, и выбранного менеджера процессов
     * @param string $sLayerName Имя слоя процессора
     * @example Cms или Page
     * @return \skProcessor
     */
    private function __construct($sLayerName) {

        $sClassName = $sLayerName.self::$sClassSuffix;
        self::$sLayerName = $sLayerName;
        self::$oProcessor = (class_exists($sClassName))? new $sClassName() : null;

        if(!(self::$oProcessor instanceof skProcessorPrototype))return false;

        return self::$oProcessor->init();

    }// constructor

    /**
     * Закрываем возможность вызвать clone
     */
    private function __clone() {}

    /**
     * Инициализирует процессор с указанным менеджером
     * @static
     * @param $sLayerName
     * @example CmsProcessor или PageProcessor
     * @return null|skProcessor
     */
    public static function init($sLayerName) {

        if(isset(self::$oInstance) and (self::$oInstance instanceof self)){

            return self::$oInstance;
        }else{

            self::$oInstance= new self($sLayerName);

            return self::$oInstance;
        }
    }// func

    /**
     * Отрабатывает запросы к менеджерам процессов
     * @static
     * @param string $sMethodName Имя метода
     * @param array $aArguments Массив передаваемых аргументов
     * @return mixed|\skProcessorPrototype Возвращает результат выполнения вызываемой функции менеджера процессов
     */
    static public function __callStatic($sMethodName, $aArguments){

        return call_user_func_array(array(self::$oProcessor, $sMethodName), $aArguments);
    }// func

    /**
     * Возвращает название слоя для которого был вызван процессор
     * @static
     * @return string
     */
    public static function getLayerName() {
        return self::$sLayerName;
    }// func

    /* Работа с событиями */

    /**
     * Добавляет обработчик события $sEventName.
     * @static
     * @param string $sEventName Название события
     * @param string $sListenerPath Путь по меткам вызова до конечного модуля
     * @param string $sHandlerMethod Метод-обработчик события в модуле
     * @return bool
     */
    static public function addEventListener($sEventName, $sListenerPath, $sHandlerMethod) {

        self::$aEventListeners[$sEventName][$sListenerPath] = $sHandlerMethod;

        return true;
    }// func

    /**
     * Добавляет before обработчик события $sEventName.
     * @static
     * @param string $sEventName Название события
     * @param string $sClassName Класс метода обработчика
     * @param string $sHandlerMethod Метод-обработчик
     * @return bool
     */
    static public function addHookBeforeEvent($sEventName, $sClassName, $sHandlerMethod) {

        self::$aEventBeforeHooks[$sEventName][$sClassName] = $sHandlerMethod;
        return true;
    }// func

    /**
     * Добавляет after обработчик события $sEventName.
     * @static
     * @param string $sEventName Название события
     * @param string $sClassName Класс метода обработчика
     * @param string $sHandlerMethod Метод-обработчик
     * @return bool
     */
    static public function addHookAfterEvent($sEventName, $sClassName, $sHandlerMethod) {

        self::$aEventAfterHooks[$sEventName][$sClassName] = $sHandlerMethod;
        return true;
    }// func

    /**
     * Выполняет события
     * @static
     * @return bool
     */
    static public function ExecuteEvents() {

        $bListenersWasCalled = false;
        //var_dump(self::$aEventListeners);
        while(count(self::$aEvents)) {

        list($sEventName, $aArguments) = array_shift(self::$aEvents);

        if(isSet(self::$aEventBeforeHooks[$sEventName]))
            foreach(self::$aEventBeforeHooks[$sEventName] as $sClassName=>$sHandlerMethod)
                call_user_func_array(array($sClassName, $sHandlerMethod), $aArguments);

        if(isSet(self::$aEventListeners[$sEventName]))
            foreach(self::$aEventListeners[$sEventName] as $sListenerPath=>$sHandlerMethod){
                $oProcess = self::$oProcessor->getProcess($sListenerPath, psAll);
                /** @var $oProcess skProcess */
                $iStatus = $oProcess->executeModuleMethod($sHandlerMethod, $aArguments);
                if($iStatus == psNew or $iStatus == psWait) $bListenersWasCalled = true;
            }

        if(isSet(self::$aEventAfterHooks[$sEventName]))
            foreach(self::$aEventAfterHooks[$sEventName] as $sClassName=>$sHandlerMethod)
                call_user_func_array(array($sClassName, $sHandlerMethod), $aArguments);

        }// while count events
        return $bListenersWasCalled;

    }// func

    /**
     * Запускает на выполнение событие $sEventName
     * @static
     * @param string $sEventName Название события
     * @param array $aArguments Аргументы, передаваемые обработчикам события
     * @return bool
     */
    static public function fireEvent($sEventName, $aArguments = array()) {

        self::$aEvents[] = array($sEventName, $aArguments);
        return true;
    }// func

    /**
     * Возвращает список before хуков
     * @static
     * @return array|bool
     */
    static public function getBeforeHooks() {

        return (count(self::$aEventBeforeHooks))? self::$aEventBeforeHooks: false;
    }// func

    /**
     * Возвращает список after хуков
     * @static
     * @return array|bool
     */
    static public function getAfterHooks() {

        return (count(self::$aEventAfterHooks))? self::$aEventAfterHooks: false;
    }// func

    /**
     * Возвращает список Слушателей событий
     * @static
     * @return array|bool
     */
    static public function getEventListeners() {

        return (count(self::$aEventListeners))? self::$aEventListeners: false;
    }// func

    /**
     * Добавялет класс-hepler для использования в шаблонах
     * @param object $oHepler Передаваемый в шаблон класс
     * @param string $sName Имя для использования объекта
     * @internal param array $aParserHelpers
     * @return bool
     */
    public static function setParserHelper(&$oHepler,$sName='') {

        if(!is_object($oHepler)) return false;
        self::$aParserHelpers[$sName?$sName:get_class($oHepler)] = &$oHepler;

        return true;
    }// func

    /**
     * Возвращает массив ссылок на объекты и static классы хелперов, доступных в шаблонах
     * @return array
     */
    public static function getParserHelpers() {

        return self::$aParserHelpers;
    }// func


    /**
     * Возвращает true если площадка находится в кластере, либо false в противном случае.
     * @return bool
     */
    public static function inCluster(){

        return (defined('INCLUSTER') AND INCLUSTER) ? true : false;

    }


}// class
