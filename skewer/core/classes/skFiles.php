<?php
/**
 * Класс работы с файловой системой
 * @class skFile
 *
 * @author ArmiT, $Author: acat $
 * @version $Revision: 961 $
 * @date $Date: 2012-10-03 16:29:35 +0400 (Ср., 03 окт. 2012) $
 * @project Skewer
 * @package kernel
 *
 *
 */
class skFiles {
    /**
     * Экземпляр класса
     * @var null|\skFiles
     */
    private static $oInstance = NULL;

    /**
     * Путь до корневой публичной директории загрузки файлов
     * @var string
     */
    private static $sUploadPath = '';

    /**
     * Путь до корневой закрытой директории загрузки файлов
     * @var string
     */
    private static $sUploadProtectedPath = '';

    /**
     * Закрытый конструктор
     */
    /** @noinspection PhpUnusedPrivateMethodInspection */
    private function __construct() { }// constructor

    /**
     * Устанавливает начальные значения, требуемые для работы класса.
     * @static
     * @param string $sFilePath Путь до корневой публичной директории загрузки файлов
     * @param string $sProtectedFilePath Путь до корневой закрытой директории загрузки файлов
     * @return null|\skFiles
     */
    public static function init($sFilePath, $sProtectedFilePath) {

        if(isset(self::$oInstance) and (self::$oInstance instanceof self)){
            self::registerPaths($sFilePath, $sProtectedFilePath);
            return self::$oInstance;
        }else{

            self::$oInstance= new self();
            self::registerPaths($sFilePath, $sProtectedFilePath);
            return self::$oInstance;
        }

    }// func

    /**
     * Устанавливает пути до корневых директорий загруprb файлов
     * @static
     * @param string $sFilePath Путь до корневой публичной директории загрузки файлов
     * @param string $sProtectedFilePath Путь до корневой закрытой директории загрузки файлов
     */
    protected static function registerPaths($sFilePath, $sProtectedFilePath) {

        self::$sUploadPath = $sFilePath;
        self::$sUploadProtectedPath = $sProtectedFilePath;

    }// func

    /**
     * @todo Уточнить принцип использования
     * Удаляет файл $sFileName
     * @static
     * @param string $sFileName удаляемый файл
     * @return bool
     */
    public static function remove($sFileName) {
        if ( strpos($sFileName,'..')!==false ) return false;
        if(!is_file($sFileName)) return false;
        return unlink($sFileName);
    }// func

    /**
     * Возвращает расширение файла $sFileName
     * @static
     * @param $sFileName полное имя файла
     * @return bool|mixed
     */
    public static function getExtension($sFileName) {
        return ($aFile = explode('.',$sFileName))? end($aFile): false;
    }// func

    /**
     * Преобразует в URL-валидный формат строку либо имя файла $sFileName. Имя указывается без учета пути.
     * @static
     * @param $sFileName обрабатываемая строка
     * @param bool $bIsFile Строка может обрабатываться как имя файла так и как строка в зависимости от значения $bIsFile
     * @param bool $bUseSalt Подставлять хеш в конец имени файла
     * @param int $iLength максимальная длина преобразованной строки
     * @return string Возвращает преобразованную строку
     */
    public static function makeURLValidName($sFileName, $bIsFile = true, $bUseSalt = false, $iLength = 25) {

        // замена символов
        $sFileName = skTranslit::change($sFileName);
        $sFileName = strtolower(trim($sFileName));

        $sExt = '';

        if($bIsFile) {

            $sExt      = self::getExtension($sFileName);
            $sFileName = basename($sFileName, '.'.$sExt);
        }

        if($bUseSalt) {
            $sFileName = substr(md5(microtime()), 0, $iLength);
        } else {
            $sFileName = skTranslit::changeDeprecated( $sFileName );
            $sFileName = trim( skTranslit::mergeDelimiters($sFileName), skTranslit::getDelimiter() );
        }

        return substr($sFileName, 0, $iLength).(($bIsFile and !empty($sExt))? '.'.$sExt: '');

    }// func

    /**
     * Проверяет наличие директории $sPath. Если Директория не существует - пытается создать. Если указан $bProtected
     * Проверка ведется в закрытой директории
     * @static
     * @param string $sPath Путь до проверяемой директории
     * @param bool $bProtected Флаг поиска в закрытой директории
     * @return bool|string
     */
    public static function createFolderPath($sPath, $bProtected = false) {

        $sRootFolder = (!$bProtected)? self::$sUploadPath: self::$sUploadProtectedPath;

        if(is_dir($sRootFolder.$sPath)) return $sRootFolder.$sPath;

        $aFolders = explode(DIRECTORY_SEPARATOR, $sPath);
        $aFolders = array_diff($aFolders, array(''));

        if(!count($aFolders)) return false;

        $sCreatedFolder = $sRootFolder;

        foreach($aFolders as $sFolder){

            $sFolder = self::makeURLValidName($sFolder, false);

            if(empty($sFolder)) return false;

            $sCreatedFolder .= $sFolder.DIRECTORY_SEPARATOR;

            if(is_dir($sCreatedFolder)) continue;

            if(!@mkdir($sCreatedFolder)) return false;

            chmod($sCreatedFolder,0777);

        }// each folder

        return $sCreatedFolder;

    }// func

    /**
     * Проверяет наличие директории $sPath. Если Директория не существует - пытается создать.
     * @static
     * @param string $sPath путь и создаваемая директория(поиск ведется от корневой директории сайта)
     * @return bool|string
     */
    public static function makeDirectory($sPath) {

        if(! defined('ROOTPATH')) return false;

        $sNewPath = ROOTPATH.DIRECTORY_SEPARATOR.$sPath;

        if(is_dir($sNewPath)) return $sNewPath;

        $aFolders = explode(DIRECTORY_SEPARATOR, $sPath);
        $aFolders = array_diff($aFolders, array(''));

        if(!count($aFolders)) return false;

        $sCreatedFolder = ROOTPATH;

        foreach($aFolders as $sFolder) {

            $sFolder = self::makeURLValidName($sFolder, false);

            if(empty($sFolder)) return false;

            $sCreatedFolder .= $sFolder.DIRECTORY_SEPARATOR;

            if(is_dir($sCreatedFolder)) continue;

            if(!@mkdir($sCreatedFolder)) return false;

            chmod($sCreatedFolder,0777);

        }// each folder

        return $sCreatedFolder;
    }// func

    /**
     * Перемещает папку раздела из закрытой директории в открытую
     * @static
     * @param integer $iSectionId Id раздела
     * @return bool
     */
    public static function moveToPublic($iSectionId) {
        if(!is_dir(self::$sUploadProtectedPath.$iSectionId)) return false;

        $sNewPath = self::createFolderPath($iSectionId.DIRECTORY_SEPARATOR,false);

        return rename(self::$sUploadProtectedPath.$iSectionId, $sNewPath);
    }// func

    /**
     * Перемещает папку раздела из открытой директории в закрытую
     * @static
     * @param int $iSectionId Id раздела
     * @return bool
     */
    public static function moveToPrivate($iSectionId) {
        if(!is_dir(self::$sUploadPath.$iSectionId)) return false;

        $sNewPath = self::createFolderPath($iSectionId.DIRECTORY_SEPARATOR,true);

        return rename(self::$sUploadPath.$iSectionId, $sNewPath);
    }// func

    /**
     * Возвращает список файлов из директории раздела $iSectionId
     * @static
     * @param $iSectionId
     * @param string $sFolderName Если указана поддиректория - возвращается список файлов из  $iSectionId/$sFolderName
     * @param bool $bProtected Поиск производится в закрытой директории
     * @param array $aFileTypes Содержит массив расширений файлов, требуемых в результирующей выборке
     * @param string $sSortBy содержит ключ поля результирующего массива, по которому требуется сортировка. Может принимать следующие значения:
     * name, size, modifyDate
     * @param string $sDirection Содержит направление сортировки Может принимать два значения: ASC и DESC
     * @param bool $bWithDirs Указывает на необходимость выборки поддиректорий в результирующий список
     * @return array|bool Возвращает массив найденных файлов либо false если директория не найдена
     */
    public static function getFilesFromSection($iSectionId, $sFolderName = '', $bProtected = false, $aFileTypes = array(), $sSortBy = 'modifyDate', $sDirection = 'ASC', $bWithDirs = false) {

        $aOut = array();

        $sFolder = (!$bProtected)? self::$sUploadPath: self::$sUploadProtectedPath;

        $sFolder .= $iSectionId.DIRECTORY_SEPARATOR;

        if ( $sFolderName )
            $sFolder .= $sFolderName.DIRECTORY_SEPARATOR;

        if(!is_dir($sFolder)) return false;

        $oDir = dir($sFolder);

        /** @noinspection PhpUndefinedFieldInspection */
        if($oDir->handle)
            while(false !== ($sFile = $oDir->read())){

                    $aFile = array();

                    if($sFile == '.' and $sFile == '..') continue;

                    $sFilePath = $sFolder.$sFile;

                    if(!$bWithDirs AND is_dir($sFilePath)) continue;

                    $sExt = self::getExtension($sFile);

                    if(empty($sExt)) continue;
                    if(count($aFileTypes) and !in_array($sExt, $aFileTypes)) continue;

                    $aFile['name'] = $sFile;
                    $aFile['size'] = self::getFileSize($sFilePath);
                    $aFile['ext']  = $sExt;
                    $aFile['modifyDate']  = date ("d-m-Y H:i:s", filemtime($sFilePath));
                    $aFile['webPath']     = str_ireplace(ROOTPATH, 'http://'.WEBROOTPATH, $sFilePath);
                    $aFile['webPathShort']     = str_ireplace(ROOTPATH, '/', $sFilePath);
                    $aFile['serverPath']  = $sFilePath;
                    $aOut[] = $aFile;
            }// h

        $oDir->close();

        usort($aOut, function($aFirst, $aSecond) use ($sSortBy, $sDirection){
            $iOut = 1;
            switch($sSortBy){
                case 'name':
                    $iOut = strcmp($aFirst['name'], $aSecond['name']);
                    break;
                case 'size':
                    $iOut = ($aFirst['size'] > $aSecond['size'])? 1: -1;
                    break;
                case 'modifyDate':
                    $iOut = ($aFirst['modifyDate'] > $aSecond['modifyDate'])? 1: -1;
                    break;
            }

            return ($sDirection == 'DESC')? $iOut*(-1): $iOut;

        });

        return $aOut;
    }// func

    /**
     * Возвращает адаптированный размер файлаю
     * @static
     * @param $sFilePath Полный путь к файлу
     * @return bool|string Возвращает строку размера либо false если файл не найден
     */
    public static function getFileSize($sFilePath) {

        if(!file_exists($sFilePath)) return false;

        $iFileSize = filesize($sFilePath);

        $aDeg[0] = 'б';
        $aDeg[1] = 'Кб';
        $aDeg[2] = 'Мб';
        $aDeg[3] = 'Гб';
        $aDeg[4] = 'Тб';
        $i = 0;
        while(floor($iFileSize/1024) > 0) {
            ++$i;
            $iFileSize /= 1024;
        }
        $sResult =  round($iFileSize, 1).' '.(isset($aDeg[$i])? $aDeg[$i]: '??');
        return $sResult;


    }// func

    /**
     * Проверяет существование директории
     * @static
     * @param $iSectionId - раздел
     * @param string $sFolderName - директория
     * @param bool $bProtected - флаг "закрытая часть"
     * @return string - валидное имя существующей папки или ''
     */
    public static function checkFilePath( $iSectionId, $sFolderName='', $bProtected=false ) {

        $sFolder = (!$bProtected)? self::$sUploadPath: self::$sUploadProtectedPath;

        $sFolder .= $iSectionId.DIRECTORY_SEPARATOR.$sFolderName;

        if ( $sFolderName )
            $sFolder .= DIRECTORY_SEPARATOR;

        return is_dir($sFolder) ? $sFolder : '';

    }

    /**
     * Возвращает полный путь по параметрам загрузки
     * @static
     * @param $iSectionId - раздел
     * @param string $sFolderName - директория
     * @param bool $bProtected - флаг "закрытая часть"
     * @return string - валидное имя существующей папки или ''
     */
    public static function getFilePath( $iSectionId, $sFolderName='', $bProtected=false ) {

        $sFolder = (!$bProtected)? self::$sUploadPath: self::$sUploadProtectedPath;

        $sFolder .= $iSectionId.DIRECTORY_SEPARATOR.$sFolderName.'/';

        return $sFolder;

    }

    /**
     * Удаляет открытые и закрытые  директории загруженный файлов раздела $iSectionId
     * @static
     * @param int $iSectionId Id удаляемого раздела
     * @return bool
     */
    public static function delSection($iSectionId) {

        $sSectionDir = self::$sUploadPath.$iSectionId.DIRECTORY_SEPARATOR;
        $sPrivateSectionDir = self::$sUploadProtectedPath.$iSectionId.DIRECTORY_SEPARATOR;

        self::delDirectoryRec($sSectionDir);
        self::delDirectoryRec($sPrivateSectionDir);

        return true;
    }// func

    /**
     * Удаляет директорию $sDirectory, включая все ее содержимое
     * @static
     * @param $sDirectory Путь к удаляемой директории
     * @return bool
     */
    public static function delDirectoryRec($sDirectory) {

        if (!is_dir($sDirectory)) return false;

            $aFiles = scandir($sDirectory);
            foreach ($aFiles as $sFile) {
                if ($sFile == "." OR $sFile == "..") continue;

                if (filetype($sDirectory.$sFile) == 'dir')
                    self::delDirectoryRec($sDirectory.$sFile.DIRECTORY_SEPARATOR);
                else
                    unlink($sDirectory.$sFile);

            }
            reset($aFiles);
            rmdir($sDirectory);

        return true;
    }// func

    /**
     * Генерирует заведомо уникальное для директории $sDirectoryPath имя файла
     * @static
     * @param $sDirectoryPath
     * @param $sFileName
     * @param int $iCounterLimit
     * @throws Exception
     * @return bool|string Возвращает новое имя либо false
     */
    public static function generateUniqFileName($sDirectoryPath, $sFileName, $iCounterLimit = 100){
        try {
            $sFileName = skFiles::makeURLValidName($sFileName, true, true);
            $i = 0;
            if(file_exists($sDirectoryPath.$sFileName))
                while(file_exists($sDirectoryPath.$sFileName)) {
                    $sFileName = skFiles::makeURLValidName($sFileName, true, true);
                    $i++;
                    if($i > $iCounterLimit) throw new Exception('Error: Name is not generated!');
                }
        } catch (Exception $e) {
            echo $e;
            return false;
        }

        return $sDirectoryPath.$sFileName;
    }// func

    /**
     * Возвращает путь до корневой директории для загрузки файлов
     * @static
     * @param bool $bProtected Указывает на режим (защищенная директория или нет)
     * @return string
     */
    public static function getRootUploadPath($bProtected = false) {

        return ($bProtected)? PRIVATE_FILEPATH: FILEPATH;

    }// func

    /**
     * Возвращает WEB путь собранный из root
     * @static
     * @param string $sSystemPath Путь относительно web корня сервера
     * @param bool $bWithSiteAddress Указывает на необходимость замены включая домен
     * @return mixed
     */
    public static function getWebPath($sSystemPath, $bWithSiteAddress = true) {

        $sReplacment = ($bWithSiteAddress)? 'http://'.WEBROOTPATH: '/';

        return str_replace(ROOTPATH, $sReplacment, $sSystemPath);

    }// func

    /**
     * Отдает максимальный допустимый размер для пакета загрузки
     * @static
     * @return int
     */
    public static function getMaxUploadSize() {

        $mSize = trim(ini_get('post_max_size'));
        $cLast = strtolower($mSize[strlen($mSize)-1]);
        switch($cLast) {
            /** @noinspection PhpMissingBreakStatementInspection */
            case 'g':
                $mSize *= 1024;
            /** @noinspection PhpMissingBreakStatementInspection */
            case 'm':
                $mSize *= 1024;
            case 'k':
                $mSize *= 1024;
        }

        return (int)$mSize;

    }

    /**
     * Переводит размер в байтах с строку с размером
     * @static
     * @param int $iBytes
     * @return string
     */
    public static function sizeToStr( $iBytes ) {
        $unit=array('б','Кб','Мб','Гб','Тб','Пб');
        if ( !$iBytes )
            return '0 '.$unit[0];
        return round($iBytes/pow(1024,($i=(int)floor(log($iBytes,1024)))),2).' '.$unit[$i];
    }

    /**
     * Отдает строку для сортировки размеров в списке
     * @param int $iBytes
     * @return string
     */
    public static function sizeToSortStr( $iBytes ) {
        if ( !$iBytes )
            return '0000.00';
        else return sprintf(
            '%d%06.2f',
            $i=(int)floor(log($iBytes,1024)),
            round($iBytes/pow(1024,($i)),2)
        );
    }

}
