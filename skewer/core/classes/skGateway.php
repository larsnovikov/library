<?php
/**
 *
 * @class skGateway
 *
 * @author ArmiT, $Author: acat $
 * @version $Revision: 1677 $
 * @date $Date: 2013-02-06 17:37:23 +0400 (Ср, 06 фев 2013) $
 * @project JetBrains PhpStorm
 * @package kernel
 */
class skGateway {

    /**
     * Устанавливает соединение с GatewayServer $sGatewayServer площадки. В качестве параметров аутентификации используется
     * Ключ площадки $sAppKey.
     * @example
     * try {
     *
     *  $oClient = skGateway::connect('http://canape3-0a.vv90.ru/gateway/index.php', APPKEY);
     *
     *  if(!($oClient instanceof skGatewayClient)) throw new GatewayException('Error init;');
     *
     *  $oClient->addHeader('MyHeader', '123');
     *  $oClient->addMethod('TestClass', 'TestMethod', array(1,2), array(new ResTest(), 'respo'));
     *
     *  if(!$oClient->doRequest()) throw new GatewayException($oClient-getError());
     *
     * } catch(GatewayException $e) {
     *  echo $e->getMessage();
     * }
     * после корректной инициализации вернет экземпляр skGatewayClient
     * @static
     * @param string $sGatewayServer URL к GatewayServer скрипту
     * @param string $sAppKey Ключ приложения
     * @return skGatewayClient
     */
    public static function connect($sGatewayServer, $sAppKey) {

        // todo после перевода на SITE_ID убрать
        $iSiteId = defined('SITE_ID') ? SITE_ID : 0;

        $oClient = new skGatewayClient($sGatewayServer, $iSiteId, skGatewayClient::StreamTypeEncrypt);

        $oClient->setKey($sAppKey);
        if ( $_SERVER['HTTP_HOST'] )
            $oClient->setClientHost( $_SERVER['HTTP_HOST'] );

        $oCrypt = new skBlowfish();
        $oCrypt->setIv(skConfig::get('security.vector'));

        $oClient->onEncrypt(array($oCrypt, 'encrypt'));
        $oClient->onDecrypt(array($oCrypt, 'decrypt'));

        return $oClient;

    }// func
}// class
