<?php

/**
 * Обёртка для библиотеки стеммера
 * @class skStemmer
 * @project Skewer
 * @package kernel
 * @extends Lingua_Stem_Ru
 *
 * @author andymitrich, $Author: sapozhkov $
 * @version $Revision: 6 $
 * @date 20.02.12 16:33 $
 *
 */
require_once COREPATH.'libs/Stemmer/Lingua_Stem_Ru.php';

class skStemmer extends Lingua_Stem_Ru{

} //class