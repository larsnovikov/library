<?php
/**
 * Системный хелпер, предоставляющий набор методов для установки модулей и патчей
 * @class skUpdateHelper
 *
 * @author ArmiT, $Author: sapozhkov $
 * @version $Revision: 1346 $
 * @date $Date: 2012-12-03 11:31:54 +0400 (Пн., 03 дек. 2012) $
 * @project Skewer
 * @package Kernel
 *
 * @uses skDB
 * @uses Tree
 * @uses Parameters
 * @uses skConfig
 * @uses skRegistry
 * @uses skTwig
 * @filesource constants.php
 */
class skUpdateHelper {

    /* Vars */
    /**
     * Ссылка на DB Connector
     * @var null|skDB
     */
    protected $oDB = null;

    /**
     * Экземпляр класса работы с разделами
     * @var null|Tree
     */
    protected $oTree = null;

    /**
     * Экземпляр класса работы с параметрами разделов
     * @var null|Parameters
     */
    protected $oParams = null;

    /**
     * Имя реестра в хранилище
     * @var string
     */
    protected $sStorageName = '';

    /**
     * Путь до корневой директории сайта
     * @var string
     */
    protected $sRootPath = '';

    /* Methods */

    /**
     * Конструктор хелпера системы обновлений
     * @throws UpdateException
     */
    public function __construct() {
        global $odb;

            if(!defined('BUILDVERSION'))
                throw new UpdateException(skLanguage::get('updateError_NoConstrData', 'BUILDVERSION'));

            if(!defined('ROOTPATH'))
                throw new UpdateException(skLanguage::get('updateError_NoConstrData', 'ROOTPATH'));

            $this->oDB           = &$odb;
            $this->oTree         = new Tree();
            $this->oParams       = new Parameters();
            $this->sRootPath     = ROOTPATH;
            $this->sStorageName  = 'build_'.BUILDVERSION;

    }// constructor

    /* Работа с БД */

    /**
     * Выполняет SQL запросы из файла $sFile
     * @param string $sFile Путь к файлу с SQL инструкциями
     * @return bool
     * @throws UpdateException
     */
    public function executeSQLFile($sFile) {

        if(!file_exists($sFile)) throw new UpdateException(skLanguage::get('updateError_sqlfile_notfound', $sFile));

        $sSQLQueries = file_get_contents($sFile);

        if(empty($sSQLQueries)) return false;

        if(!$this->oDB->multi_query($sSQLQueries)) throw new UpdateException(skLanguage::get('updateError_wrong_query', $this->oDB->error));

        return true;
    }// func

    /**
     * Выполняет запрос $sQuery с данными $aData к БД
     * @param string $sQuery Выполняемый SQL запрос
     * @param array $aData Данные для подстановки вместо placeholders
     * @return mysqli_result Возвращает результат выполнения запроса
     * @throws UpdateException
     */
    public function executeSQLQuery($sQuery, $aData = array()) {

        if(empty($sQuery)) throw new UpdateException(skLanguage::get('updateError_wrong_query'));

        $rRes = $this->oDB->query($sQuery, $aData);

        if($this->oDB->errno) throw new UpdateException($this->oDB->error,$this->oDB->errno);

        return $rRes;
    }// func

    /**
     * Перенос полей из одной таблицы в другую
     * @param $sFromTable
     * @param $sTargetTable
     * @param $aFields
     * @param int|string $mCondition
     * @return bool
     */
    public function transferRowsForDB($sFromTable, $sTargetTable, $aFields, $mCondition = '1'){

        $sFields = '`'.implode('`,`',$aFields) .'`';

        $sQuery =
            "INSERT INTO $sTargetTable ($sFields)
              (SELECT $sFields FROM $sFromTable WHERE $mCondition)";

        $this->executeSQLQuery($sQuery);

        $sQuery =
            "DELETE FROM $sFromTable WHERE $mCondition";

        $this->executeSQLQuery($sQuery);

        return true;
    }

    /* Работа с Разделами */

    /**
     * Добавляет подраздел к $iParent с названием $sTitle
     * @param int $iParent Id родительского раздела
     * @param string $sTitle Название раздела
     * @param string $sAlias Псевдоним для раздела. Если не указан - формируется из Названия раздела
     * @param bool $bVisible Флаг вида отображения в системе
     * @param string $sLink Внешняя (redirect) ссылка с раздела.
     * @throws UpdateException
     * @return int|bool Возвращает Id созданного раздела либо false в случае ошибки.
     */
    public function addSection($iParent, $sTitle, $sAlias = '', $bVisible = true, $sLink = '') {

        if(! $iNewSection = $this->oTree->addSection($iParent, $sTitle, 0, $sAlias, $bVisible, $sLink))
            throw new UpdateException(skLanguage::get('updateError_sectionNotAdded', $sTitle));

        return $iNewSection;

    }// func

    /**
     * Добавялет подраздел к $iParent на основе раздела-шаблона $iTemplateId
     * @param int $iParent Id родительского раздела
     * @param int $iTemplateId Id Раздела-шаблона
     * @param string $sAlias Псевдоним для раздела. Если не указан - формируется из Названия раздела
     * @param string $sTitle Название раздела
     * @param bool $bVisible Флаг вида отображения в системе
     * @param string $sLink Внешняя (redirect) ссылка с раздела.
     * @return bool|int  Возвращает Id созданного раздела либо false в случае ошибки.
     * @throws UpdateException
     */
    public function addSectionByTemplate($iParent, $iTemplateId, $sAlias, $sTitle, $bVisible = true, $sLink = '') {

        if(! $iNewSection = $this->oTree->addSection($iParent, $sTitle, $iTemplateId, $sAlias, $bVisible, $sLink))
            throw new UpdateException(skLanguage::get('updateError_tplNotFound', $iTemplateId));

        return $iNewSection;

    }// func

    /**
     * Возвращает Id раздела по пути псевдонимов $sAliasPath
     * @param string $sAliasPath путь до раздела по дереву псевдонимов.
     * @return bool Возвращает Id раздела либо false, если таковой небыл найден.
     * @throws UpdateException
     */
    public function getSectionIdByAlias($sAliasPath) {

        if(! $iSectionId = $this->oTree->getIdByPath( $sAliasPath ))
            throw new UpdateException(skLanguage::get('updateError_AliasNotFound', $sAliasPath));

       return $iSectionId;

    }// func


    /**
     * @fixme Решить область обновлений
     */
    public function updateSection() {}// func

    /**
     * Удаляет раздел $iSectionId
     * @param int $iSectionId Id удаляемого раздела
     * @return bool
     * @throws UpdateException
     */
    public function removeSection($iSectionId) {

        if(!$this->oTree->deleteItem($iSectionId))
            throw new UpdateException(skLanguage::get('updateError_SectionNotDeleted', $iSectionId));

        return true;

    }// func

    /**
     * @param $iSectionId
     * @return bool
     */
    public function isSection($iSectionId){

        return (bool)$this->oTree->getById($iSectionId);
        
    }

    /**
     * @fixme Уточнить механизм по API
     */
    public function applyAccessToSection() {}// func

    /* Работа с параметрами */

    /**
     * Добавляет параметр в раздел $iParent с именем $sName и значением $mValue в группу $sGroup. Если указаны, дополнительные параметры - они
     * так же будут добавлены.
     * @param int $iParent Id родительского раздела для параметра
     * @param string $sName Имя параметра
     * @param mixed $mValue Значение параметра
     * @param string $mShowVal Расширенное значение параметра
     * @param string $sGroup Группа параметра (по-умолчанию добавялет в .)
     * @param string $sTitle Название параметра
     * @param int $iAccessLevel Тип параметра
     * @return bool|int Возвращает Id созданного параметра либо false в случае ошибки.
     * @throws UpdateException
     */
    public function addParameter($iParent, $sName, $mValue, $mShowVal = '', $sGroup = '.',  $sTitle = '', $iAccessLevel = 0) {

        if(!(int)$iParent OR empty($sName))
            throw new UpdateException(skLanguage::get('ParamsWrongData', $sName, $iParent));

        $aItem['name']          = $sName;
        $aItem['group']         = $sGroup;
        $aItem['value']         = $mValue;
        $aItem['title']         = $sTitle;
        $aItem['parent']        = $iParent;
        $aItem['show_val']      = $mShowVal;
        $aItem['access_level']  = $iAccessLevel;

        if(!$iNewParamId = $this->oParams->saveParameter($aItem))
            throw new UpdateException(skLanguage::get('ParamNotSaved', $sName, $iParent));

        return $iNewParamId;

    }// func

    /**
     * Обновляет данные параметра $sName группы $sGroup в разделе $iParent. Если Указаны дополнительные параметры, то они так же будут сохранены
     * как свойства параметра.
     * @param int $iParent Id родительского раздела для параметра
     * @param string $sName Имя параметра
     * @param string $sGroup Группа параметра (по-умолчанию добавялет в .)
     * @param null|mixed $mValue Значение параметра
     * @param null|mixed $mShowVal Расширенное значение параметра
     * @param null|string $sTitle Название параметра
     * @param null|int $iAccessLevel Тип параметра
     * @return bool|int Возвращает Id обновленного параметра либо false в случае ошибки.
     * @throws UpdateException
     */
    public function updateParameter($iParent, $sName, $sGroup, $mValue = null, $mShowVal = null,  $sTitle = null, $iAccessLevel = null) {

        if(!(int)$iParent OR empty($sName) OR empty($sGroup))
            throw new UpdateException(skLanguage::get('ParamsWrongData', $sName, $iParent));

        if(!$aExistsParam = $this->oParams->getByName( $iParent, $sGroup, $sName))
            throw new UpdateException(skLanguage::get('ParamNotFound', $sName, $iParent));


        $aItem['id'] = $aExistsParam['id'];

        if(!is_null($mValue))       $aItem['value'] = $mValue;
        if(!is_null($sTitle))       $aItem['title'] = $sTitle;
        if(!is_null($mShowVal))     $aItem['show_val'] = $mShowVal;
        if(!is_null($iAccessLevel)) $aItem['access_level'] = $iAccessLevel;

        if(!$iNewParamId = $this->oParams->saveParameter($aItem))
            throw new UpdateException(skLanguage::get('ParamNotSaved', $sName, $iParent));

        return $iNewParamId;

    }// func

    /**
     * Обновляет значение на $mValue параметра $sName в группе $sGroup из раздела $iParent
     * @param int $iParent Id Раздела
     * @param string $sName Имя параметра
     * @param string $sGroup Группа параметра
     * @param mixed $mValue Новое значение параметра
     * @return bool|int
     */
    public function updateParameterValue($iParent, $sName, $sGroup, $mValue) {

       return $this->updateParameter($iParent, $sName, $sGroup, $mValue);

    }// func

    /**
     * Обновляет расширенное значение на $mText параметра $sName в группе $sGroup из раздела $iParent
     * @param int $iParent Id Раздела
     * @param string $sName Имя параметра
     * @param string $sGroup Группа параметра
     * @param mixed $mText Новое расширенное значение параметра
     * @return bool|int
     */
    public function updateParameterText($iParent, $sName, $sGroup, $mText) {

        return $this->updateParameter($iParent, $sName, $sGroup, null, $mText);

    }// func

    /**
     * Обновляет название на $sNewTitle параметра $sName в группе $sGroup из раздела $iParent
     * @param int $iParent Id Раздела
     * @param string $sName Имя параметра
     * @param string $sGroup Группа параметра
     * @param string $sNewTitle Новое название параметра
     * @return bool|int
     */
    public function updateParameterTitle($iParent, $sName, $sGroup, $sNewTitle) {

        return $this->updateParameter($iParent, $sName, $sGroup, null, null, $sNewTitle);

    }// func

    /**
     * Обновляет тип на $iNewType параметра $sName в группе $sGroup из раздела $iParent
     * @param int $iParent Id Раздела
     * @param string $sName Имя параметра
     * @param string $sGroup Группа параметра
     * @param int $iNewType Новый тип параметра
     * @return bool|int
     */
    public function updateParameterType($iParent, $sName, $sGroup, $iNewType) {

        return $this->updateParameter($iParent, $sName, $sGroup, null, null, null, $iNewType);

    }// func

    /**
     * Удаляет параметр $sName группы $sGroup из раздела $iParent
     * @param int $iParent Id родительского раздела
     * @param string $sName Имя удаляемого параметра
     * @param string $sGroup Имя родительской группы
     * @return bool
     */
    public function removeParameter($iParent, $sName, $sGroup = '.') {

        return $this->oParams->removeByName($iParent, $sName, $sGroup);

    }// func

    /**
     * Удаляет параметр с Id = $iParamId
     * @param int $iParamId
     * @return bool|int
     * @throws UpdateException
     */
    public function removeParameterById($iParamId) {

        if(!(int)$iParamId) throw new UpdateException(skLanguage::get('ParamNotDeleted', $iParamId));

        return $this->oParams->delById($iParamId);

    }// func


    public function isSetParameter($iParent, $sName, $sGroup = '.'){

        return (bool)count( $this->oParams->getByName($iParent,$sGroup,$sName) );

    }

    /* Работа с реестром */

    /**
     * Создает хранилище $sStorageName с набором данных $aData. Если указан $bIfNotExist то
     * перед созданием происходит проверка на существование одноименного хранилища и если оно
     * существует то дубль не создается
     * @param string $sStorageName Имя создаваемого хранилища
     * @param array $aData
     * @param bool $bIfNotExist
     * @return bool
     */
    public function createRegistryStorage($sStorageName, $aData = array(), $bIfNotExist = true) {

        if(skRegistry::isExist($sStorageName) AND $bIfNotExist) return false;

        return skRegistry::saveStorage($sStorageName, $aData);

    }// func

    /**
     * Создает копию реестра с именем $sToStorageName на основе реестра с именем $sFromStorageName. Если указан $bIfNotExist
     * то новый реестр будет создан только в том случае, если его ранее не существовало. В противном случае будет произведена перезапись данных.
     * @param string $sFromStorageName Имя реестра - источника данных
     * @param string $sToStorageName Имя создаваемого реестра
     * @param bool $bIfNotExist Указатель на необходимость проверки на наличие дубликатов с таким именем.
     * @return bool Возвращает true в случае успешного клонирования реестра либо false  в случае ошибки либо в случае наличия ранее созданного реестра с
     * именем $sToStorageName при наличии флага $bIfNotExist
     */
    public function cloneRegistryStorage($sFromStorageName, $sToStorageName, $bIfNotExist = true) {

        if($this->isExistRegistryStorage($sToStorageName) AND $bIfNotExist) return false;

        if(!$this->isExistRegistryStorage($sFromStorageName)) return false;

        $aData = skRegistry::getStorage($sFromStorageName);

        skRegistry::saveStorage($sToStorageName,$aData);
        return true;
    }// func

    /**
     * Возвращает true если хранилище $sStorageName существует либо false в противном случае
     * @param string $sStorageName
     * @return bool
     */
    public function isExistRegistryStorage($sStorageName) {

        return skRegistry::isExist($sStorageName);

    }// func

    /**
     * Удаляет хранилище $sStorageName
     * @param string $sStorageName
     * @return bool
     */
    public function removeRegistryStorage($sStorageName) {

        return skRegistry::removeStorage($sStorageName);

    }// func

    /**
     * Добавляет ключ $sKey со значением $mValue в реестр. Добавление ведется в реестр настроек
     * площадки с именем, включающим в себя номер текущей версии сборки build_<build version> либо в указанный в $sStorageName
     * Если реестр с таким именем не был найден формируется исключение типа UpdateException.
     * @example build_0008
     *
     * Запись ключей пути ведется от корневого зарезервированного ключа "buildConfig"
     * @example buildConfig.funcPolicy.Page.items
     *
     * @param string $sKey Имя ключа, в который будут добавлены данные
     * @param mixed $mValue Добавляемые в ключ данные
     * @param null|string $sStorageName Имя используемого реестра
     * @return bool
     * @throws UpdateException
     */
    public function addRegistryKey($sKey, $mValue, $sStorageName = null) {

        $sStorageName = (is_null($sStorageName))? $this->sStorageName: $sStorageName;


        if(! $aBuildConfig = skRegistry::getStorage($sStorageName))
            throw new UpdateException(skLanguage::get('updateError_storageNotFound', $sStorageName));

        skConfig::setStorage($sStorageName, $aBuildConfig);

        skConfig::add($sKey, $mValue);

        skRegistry::saveStorage($sStorageName, skConfig::getStorage($sStorageName));

        return true;
    }// func

    /**
     * Возвращает данные, хранящиеся в ключе $sKey. В качестве текущего реестра выбирается реестр настроек
     * площадки с именем, включающим в себя номер текущей версии сборки build_<build version> либо в указанный в $sStorageName
     * Если реестр с таким именем не был найден формируется исключение типа UpdateException.
     * @example build_0008
     * @param string $sKey Имя иcкомого ключа
     * @param null|string $sStorageName  Имя используемого реестра
     * @return mixed|null
     * @throws UpdateException
     */
    public function getRegistryKey($sKey, $sStorageName = null) {

        $sStorageName = (is_null($sStorageName))? $this->sStorageName: $sStorageName;

        if(! $aBuildConfig = skRegistry::getStorage($sStorageName))
            throw new UpdateException(skLanguage::get('updateError_storageNotFound'), $sStorageName);

        skConfig::setStorage($sStorageName, $aBuildConfig);

        return skConfig::get($sKey);

    }// func

    /**
     * Сохраняет измененное на $mValue значение ключа $sKey. В качестве текущего реестра выбирается реестр настроек
     * площадки с именем, включающим в себя номер текущей версии сборки build_<build version> либо в указанный в $sStorageName
     * Если реестр с таким именем не был найден формируется исключение типа UpdateException.
     * @param null|string $sStorageName  Имя используемого реестра
     * @param string $sKey
     * @param mixed $mValue
     * @return bool|null
     * @throws UpdateException
     */
    public function updateRegistryKey($sKey, $mValue, $sStorageName = null) {

        $sStorageName = (is_null($sStorageName))? $this->sStorageName: $sStorageName;

        if(! $aBuildConfig = skRegistry::getStorage($sStorageName))
            throw new UpdateException(skLanguage::get('updateError_storageNotFound', $sStorageName));

        skConfig::setStorage($sStorageName, $aBuildConfig);

        skConfig::set($sKey, $mValue);

        skRegistry::saveStorage($sStorageName, skConfig::getStorage($sStorageName));

        return true;
    }// func

    /**
     * Возвращает true если ключ $sKey реестра найден либо false в противном случае. В качестве текущего
     * реестра выбирается реестр настроек площадки с именем, включающим в себя номер текущей версии сборки
     * build_<build version> либо в указанный в $sStorageName. Если реестр с таким именем не был найден
     * формируется исключение типа UpdateException.
     * @param null|string $sStorageName  Имя используемого реестра
     * @param string $sKey
     * @return bool|null
     * @throws UpdateException
     */
    public function isExistRegistryKey($sKey, $sStorageName = null) {

        $sStorageName = (is_null($sStorageName))? $this->sStorageName: $sStorageName;

        if(! $aBuildConfig = skRegistry::getStorage($sStorageName))
            throw new UpdateException(skLanguage::get('updateError_storageNotFound'), $sStorageName);

        skConfig::setStorage($sStorageName, $aBuildConfig);

        return skConfig::isExists($sKey);

    }// func

    /**
     * Удаляет из реестра ключ $sKey.  В качестве текущего реестра используется реестр настроек площадки с именем,
     * включающим в себя номер текущей версии сборки build_<build version> либо в указанный в $sStorageName.
     * Если реестр с таким именем не был найден формируется исключение типа UpdateException.
     * @param null|string $sStorageName  Имя используемого реестра
     * @param string $sKey Имя удаляемого ключа.
     * @return bool|null
     * @throws UpdateException
     */
    public function removeRegistryKey($sKey, $sStorageName = null) {

        $sStorageName = (is_null($sStorageName))? $this->sStorageName: $sStorageName;

        if(! $aBuildConfig = skRegistry::getStorage($sStorageName))
            throw new UpdateException(skLanguage::get('updateError_storageNotFound', $sStorageName));

        skConfig::setStorage($sStorageName, $aBuildConfig);

        skConfig::remove($sKey);

        skRegistry::saveStorage($sStorageName, skConfig::getStorage($sStorageName));

        return true;

    }// func

    /* Работа с файловой системой */

    /**
     * Создает директорию $sLocalPath. Поиск ведется от корневой директории сайта
     * @param string $sLocalPath путь и имя создаваемой директории
     * @return bool|string Возвращает полный путь к созданной директории либо false в случае ошибки
     * @throws UpdateException
     */
    public function makeDirectory($sLocalPath) {

        if(! $sNewPath = skFiles::makeDirectory($sLocalPath))
            throw new  UpdateException(skLanguage::get('FolderNotCreated', $sLocalPath));

        return $sNewPath;

    }// func

    /**
     * Перемещает директорию или файл $sOldPath по пути $sNewPath.
     * @param string $sOldPath перемещаемая папка или файл
     * @param $sNewPath новые путь и имя перемещаемой папки либо файла
     * @return bool Возвращает true если перемещение прошло успешно либо false в случае ошибки
     * @throws UpdateException
     */
    public function moveDirectory($sOldPath, $sNewPath) {

        if(!rename($sOldPath, $sNewPath))
            throw new UpdateException(skLanguage::get('FolderNotMoved', $sOldPath, $sNewPath));

        return true;

    }// func

    /**
     * Рекурсивно удаляет директорию $sPath
     * @param string $sPath путь к удаляемой директории, включая ROOTPATH
     * @return bool Возвращает true, если удаление прошло успешно либо false в противном случае
     * @throws UpdateException
     */
    public function removeDirectory($sPath) {

        if(!skFiles::delDirectoryRec($sPath))
            throw new UpdateException(skLanguage::get('FolderNotRemoved', $sPath));

        return true;

    }// func

    /**
     * Удаляет файл $sFilePath. Возвращает true, если удаление прошло успешно либо false в случае ошибки
     * @param string $sFilePath Полный путь к удаляемому файлу
     * @return bool
     * @throws UpdateException
     */
    public function removeFile($sFilePath) {

        if(!skFiles::remove($sFilePath))
            throw new UpdateException(skLanguage::get('FileNotRemoved', $sFilePath));

        return true;

    }// func

    /**
     * Перемещает файл $sFile в $sMovePath. Если указан $bHardSet, то при наличии по месту назначения
     * ранее созданного файла, оный будет заменен перемещаемым.
     * @param string $sFile Перемещаемый файл
     * @param string $sMovePath Место назначения
     * @param bool $bHardSet Указатель на необходимость замены файла при наличии идентичного
     * @return bool Возвращает true если перемещение прошло успешно либо false в случа ошибки.
     * @throws UpdateException
     */
    public function moveFile($sFile, $sMovePath, $bHardSet = true) {

        /* Перемещаемый файл существует */
        if(!file_exists($sFile) OR
            empty($sMovePath)) throw new UpdateException(skLanguage::get('FileNotFound', $sFile));

        /* Место назначения корректно */
        if(file_exists($sMovePath))
            if(!$bHardSet)
                return false;
            else
                if(!unlink($sMovePath))
                    throw new UpdateException(skLanguage::get('FileNotRemoved'));
        /* Перемещаем */
        return rename($sFile, $sMovePath);

    }// func

    /**
     * Копирует файл $sFile в $sMovePath. Если указан $bHardSet, то при наличии по месту назначения
     * ранее созданного файла, оный будет заменен копируемым.
     * @param string $sFile Перемещаемый файл
     * @param string $sMovePath Место назначения
     * @param bool $bHardSet Указатель на необходимость замены файла при наличии идентичного
     * @return bool Возвращает true если копирование прошло успешно либо false в случа ошибки.
     * @throws UpdateException
     */
    public function copyFile($sFile, $sMovePath, $bHardSet = true) {

        /* Перемещаемый файл существует */
        if(!file_exists($sFile) OR
            empty($sMovePath)) throw new UpdateException(skLanguage::get('FileNotFound', $sFile));

        /* Место назначения корректно */
        if(file_exists($sMovePath))
            if(!$bHardSet)
                return false;
            else
                if(!unlink($sMovePath))
                    throw new UpdateException(skLanguage::get('FileNotRemoved'));
        /* Копируем */
        return copy($sFile, $sMovePath);

    }// func

    /* Работа с .htaccess */

    /**
     * Заполняет шаблон файла .htaccess данными и перезаписывает в текущую обновляемую площадку в качестве корневого.
     * Шаблон лежит в директории "common/templates" сборки.
     * Внимание! Перезапись ведется в любом случае.
     * @param string $sTplPath Путь к директории с шаблоном
     * @param array $aData Данные, вставляемые в шаблон
     * @throws UpdateException
     * @return bool
     * @uses /skewerBuild/common/templates/htaccess.twig
     */
    public function updateHtaccess($sTplPath, $aData = array()) {

        /* Генерируем файл с константами для площадки */

        $oCodeGen = new skCodeGenerator($sTplPath, ROOTPATH);

        $oCodeGen->add(new HtaccessTpl('.htaccess', $aData));

        if(!$oCodeGen->make()) throw new UpdateException($oCodeGen->getError());

        return true;

    }// func

    /* Работа с константами системы */

    /**
     * Осуществляет синтаксичесикй разбор шаблона файла констант (по-умолчанию <buildPath>/common/templates/constants.twig)
     * и замену меток на их значения из $aData. Собранный файл записывается в директорию "config"
     * площадки и используется в качестве источника основных констант
     * @param string $sTplPath Путь к директории с шаблоном
     * @param array $aData Массив данных для замены в шаблоне
     * @throws UpdateException
     * @return bool
     */
    public function updateConstants($sTplPath, $aData) {

        /* Генерируем файл с константами для площадки */
        $oCodeGen = new skCodeGenerator($sTplPath, ROOTPATH);

        $oCodeGen->add(new ConstantsTpl('config/constants.generated.php', $aData));

        if(!$oCodeGen->make()) throw new UpdateException($oCodeGen->getError());

        return true;

    }// func

    public function closeSite () {}// func

    public function openSite () {}// func

    /* Работа с политиками доступа */

    public function addAuthPolicy() {}// func

    public function updateAuthPolicy() {}// func

    public function removeAuthPolicy() {}// func

    /**
     * Устанавливает модуль
     * @param string $sModuleName имя модуля
     * @return bool
     * @throws UpdateException
     */
    public function moduleInstall( $sModuleName ) {

        if(!$sModuleName)
            throw new UpdateException('Имя модуля не задано при установке модуля');

        // todo проверить наличие Module в конце имени

        try {
            $oInstaller = new skInstaller($sModuleName);
            $oInstaller->registerModule();
            skRegistry::saveStorage('build_'.BUILDVERSION, skConfig::getStorage('buildConfig'));
            return true;
        } catch ( skException $e ) {
            throw new UpdateException($e->getMessage(), $e->getCode(), $e);
        }

    }

    /**
     * Деинсталирует модуль
     * @param $sModuleName
     * @return bool
     * @throws UpdateException
     */
    public function moduleUninstall( $sModuleName ) {

        if(!$sModuleName)
            throw new UpdateException('Имя модуля не задано при удалении модуля');

        // todo проверить наличие Module в конце имени

        try {
            $oInstaller = new skInstaller($sModuleName);
            $oInstaller->unregisterModule();
            skRegistry::saveStorage('build_'.BUILDVERSION, skConfig::getStorage('buildConfig'));
            return true;
        } catch ( skException $e ) {
            throw new UpdateException($e->getMessage(), $e->getCode(), $e);
        }

    }

    /**
     * Переустанавливает модуль
     * @param $sModuleName
     * @return bool
     */
    public function moduleReinstall( $sModuleName ) {
        return $this->moduleUninstall($sModuleName) and $this->moduleInstall($sModuleName);
    }

    /**
     * Удаляет информацию о модуле из реестра
     * @param $sModuleName
     * @param $sLayer
     * @return bool
     */
    public function moduleRemoveRegistryInfo( $sModuleName, $sLayer ) {
        skInstaller::removeRegistryInfo( $sModuleName, $sLayer );
        skRegistry::saveStorage('build_'.BUILDVERSION, skConfig::getStorage('buildConfig'));
        return true;
    }

    /**
     * Устанавливает патч, если тот еще не стоял
     * @param $sPatchFile
     * @throws UpdateException
     * @return bool
     */
    public function installPatch( $sPatchFile ) {

        $sPatchExecFile = PATCHPATH.$sPatchFile;

        /* Получаем экземпляр инсталлятора обновлений */
        $oPatchInstaller = new skPatchInstaller($sPatchExecFile);
        /* Устанавливаем проверку на то, что патч еще не устанавливался */
        $oPatchInstaller->setChecker("PatchesToolApi::checkPatch");

        /* В ходе установки произошла ошибка - выходим (хотим, чтобы сообщение об ощибке попало на клиент, используем skGatewayExecuteException) */
        $oPatchInstaller->install();

        /* Все прошло нормально - пишем о том, что патч поставили */
        PatchesToolMapper::registerPatch(basename($sPatchFile), $sPatchFile, $oPatchInstaller->getDescription());

        return true;

    }

}// class
