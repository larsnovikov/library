<?php
/**
 * Представление данных
 * @class skParser
 * @project Canape 3.0 (skewer)
 * @package kernel
 * @Author ArmiT, $Author: acat $
 * @version $Revision: 1671 $
 * @date $Date: 2013-02-06 15:38:54 +0400 (Ср, 06 фев 2013) $
 *
 */
class skParser {
    /**
     * Производит запуск указанного в контексте шаблонизатора. Используя шаблон и данные, формирует вывод.
     * @static
     * @param skContext $oContext Контекст обрабатываемого процесса
     * @return bool|string
     */
    public static function render(skContext &$oContext) {

        $sOut       = '';
        $aData      = $oContext->getData();
        $aData['_objectId'] = $oContext->getLabel();

        $sModuleDir = $oContext->getModuleDir().$oContext->getTplDirectory().DIRECTORY_SEPARATOR;

        switch ($oContext->getParser()) {
            case parserTwig:

                // Если в данных есть настройки зон расположения - производим их обработку

                if(isset($aData['.layout']))
                    foreach($aData['.layout'] as $sZoneName=>$aZoneData) {

                        $aData['layout'][$sZoneName] = '';

                        foreach($aZoneData as $sLabel){
                            if(is_array($sLabel)){


                                // обрабатываем инклуды
                                $aData['layout'][$sZoneName] .=
                                    self::parseTwig( $sLabel['templateFile'], $aData, $sLabel['templateDir']);
                            } // if
                            else {
                                if(isset($aData[$sLabel]))
                                    $aData['layout'][$sZoneName] .= $aData[$sLabel];
                            } // else
                        } // foreach

                    } // if / foreach

                $sOut = self::parseTwig( $oContext->getTemplate(), $aData, $sModuleDir );

                break;

            case parserContenter:

                break;

            case parserJSON:

                if($oContext->oProcess->getStatus() == psComplete)
                    skProcessor::addJSONResponse($oContext);

                break;
        }// switch parser type

        return $sOut;

    }// func

    /**
     * Отрендерить шаблон, вернуть строку с результатом
     * @param $sTemplate - шаблон
     * @param array $aData - массив для парсинга
     * @param string $sTemplateDir - адрес шаблона
     * @return string
     */
    public static function parseTwig( $sTemplate, $aData, $sTemplateDir='' ) {

        // набор предустановленных в конфиге путей для парсинга
        $aConfigPaths = skConfig::get('parser.default.paths');
        if ( !is_array($aConfigPaths) ) $aConfigPaths = array();

        // готовим данные для наследования шаблонов
        $aOverlay = skConfig::get('overlay.template');
        
        if ($aOverlay){
            $sShortPath = str_replace(RELEASEPATH, '', $sTemplateDir);
            
            // прогоняем все файлы на наличие наследника
            foreach($aOverlay as $sOverlayPath){
                // проверяем на перекрытие шаблонов
                if ($sShortPath.$sTemplate == $sOverlayPath){
                    // и заменяем путь к файлу
                    $sTemplateDir = ROOTPATH.'skewer/'.str_replace($sTemplate, "", $sOverlayPath);
                    break;
                }
            } // foreach
        }
        
        $aPaths = array();

        /* Если установлен режим отображения отличный от стандартного, то добавляем путь к шаблонам с учетом режима */

        $aPaths = $sTemplateDir;

        if($sViewMode = skProcessor::getEnvParam('_viewMode'))
            if(file_exists($sTemplateDir.$sViewMode.DIRECTORY_SEPARATOR))
                $aPaths = $sTemplateDir.$sViewMode.DIRECTORY_SEPARATOR;

        $aTplPaths = array_merge($aConfigPaths,array($aPaths));
        $aTplPaths = array_diff($aTplPaths, array(''));

        /** @fixme Добавил путь до папки commons/templates */
        skTwig::setPath( $aTplPaths );
        /** @fixme Продумать механизм протягивания в шаблоны глобальных параметров */
        //skTwig::assign('skRouter',$oContext->oProcess->oRouter);

        /* Получить список хелперов для шаблонов */
        $aParserHelpers = skProcessor::getParserHelpers();
        if(count($aParserHelpers))
            foreach($aParserHelpers as $sHelperName => $oHelperObject)
                skTwig::assign($sHelperName, $oHelperObject);

        foreach ($aData as $sLabel => $mData) {
            skTwig::assign($sLabel, $mData);
        }

        return skTwig::render($sTemplate);

    }

}// class
