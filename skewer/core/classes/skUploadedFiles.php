<?php
/**
 * Класс конвейерной обработки загруженных через HTTP/POST файлов
 * @class skUploadedFiles
 * @implements Iterator
 *
 * @author ArmiT, $Author: acat $
 * @version $Revision: 1433 $
 * @date $Date: 2012-12-18 16:15:33 +0400 (Вт., 18 дек. 2012) $
 * @project Skewer
 * @package kernel
 *
 * @example
 * Логика построения фильтров:
 *
 * $aFilter['name'] =  array('1.jpg'[, '2.jpg']); // массив ожидаемых в результате файлов по имени
 * $aFilter['size'] =  12000; // in bytes // максимально допустимые размеры
 * $aFilter['format'] =  array('png'[,'jpeg'[,'gif'[,'rar']]]); // допустимые форматы файлов (см. MIME types)
 * $aFilter['type'] = array('image'[,'application'[,'text']]); // допустимые типы файлов (см. MIME types)
 * $aFilter['fieldName'] = array('doc'[, 'etc']); // ожидаемые поля формы
 * $aFilter['allowExtensions'] = array('jpg'[, 'png']); // допустимые расширения загруженных файлов
 * $aFilter['imgMaxWidth'] = <number>; // максимальная ширина для изображений
 * $aFilter['imgMaxHeight'] = <number>; // максимальная высота для изображений
 *
 *
 *
 */
class skUploadedFiles implements Iterator {

    /**
     * Указатель на текущий файл в списке
     * @var int
     */
    private static $iPos     = 0;

    /**
     * Массив загруженных файлов
     * @var array
     */
    private static $aFiles   = array();

    /**
     * Экземпляр класса
     * @var null|\skUploadedFiles
     */
    private static $instance = NULL;

    /**
     * Контейнер текста ошибки
     * @var mixed
     */
    private static $mError   = false;

    /**
     * Массив фильтров выборки загруженных файлов
     * @var array
     */
    private static $aFilter  = array();

    /**
     * Путь до корневой публичной директории загрузки файлов
     * @var string
     */
    private static $sUploadPath = '';

    /**
     * Путь до корневой закрытой директории загрузки файлов
     * @var string
     */
    private static $sUploadProtectedPath = '';

    /**
     * Закрытый конструктор
     */
    private function __construct() { }// constructor

    /**
     * Возвражает объект-итератор skUploadedFiles, отфильтрованный по фильтру $aFilter
     * @static
     * @param array $aFilter Фильтр выборки в итератор загруженный файлов
     * @param string $sFilePath Путь до публичной директории загрузки файлов
     * @param string $sProtectedFilePath Путь до закрытой директории загрузки файлов
     * @return null|\skUploadedFiles
     */
    public static function get($aFilter = array(), $sFilePath, $sProtectedFilePath) {

        self::$iPos = 0;

        if(isset(self::$instance) and (self::$instance instanceof self)){
            self::init($aFilter, $sFilePath, $sProtectedFilePath);
            return self::$instance;
        }else{

            self::$instance= new self();
            self::init($aFilter, $sFilePath, $sProtectedFilePath);
            return self::$instance;
        }
    }// func

    /**
     * Устанавливает начальные значения, собирает массив загруженных файлов из $_FILES
     * @param array $aFilter Фильтр выборки в итератор загруженный файлов
     * @param string $sFilePath Путь до публичной директории загрузки файлов
     * @param string $sProtectedFilePath Путь до закрытой директории загрузки файлов
     * @return bool
     */
    protected function init($aFilter = array(), $sFilePath, $sProtectedFilePath) {

        self::$sUploadPath = $sFilePath;
        self::$sUploadProtectedPath = $sProtectedFilePath;

        if(count($_FILES))
            foreach($_FILES as $sFieldName => $aFileFields){

                $aFiles = array();
                foreach($aFileFields as $sFileParam=>$aParam) {
                    if( is_array($aParam) && count($aParam) ) {
                        foreach($aParam as $iObjKey=>$sVal)
                            $aFiles[$iObjKey][$sFileParam] = $sVal;
                    } else {
                        $aFiles[0][$sFileParam] = $aParam;
                    }
                }

                foreach($aFiles as $aFile){
                    $aFile['fieldName'] = $sFieldName;
                    self::$aFiles[] = $aFile;
                }
            }

        if(!count(self::$aFiles)) {
            self::$mError = 'Ошибка загрузки файлов';
            return false;
        }
        // @todo Нужно ли чистить $_FILES после обхода без возможности повторного доступа??
        self::$aFilter = $aFilter;
        return true;
    }

    /**
     * Возвращает текст ошибки для текущего обрабатываемого файла
     * @return bool|mixed Текст ошибки либо false
     */
    public function getError() {
        return self::$mError;
    }

    /**
     * Сбрасывает внутренний указатель итератора на первый элемент списка файлов
     */
    public function rewind() {
        self::$iPos = 0;
    }

    /**
     * Возвращает текущий файл
     * @throws Exception
     * @return \skUploadedFiles
     */
    public function current() {

        try {

            self::$mError = false;
            $aFilter = self::$aFilter;
            $aCurrentFile = self::$aFiles[self::$iPos];
            if($aCurrentFile['error']) throw new Exception('Ошибка загрузки файла.');

            if(!is_uploaded_file($aCurrentFile['tmp_name'])) throw new Exception('Файл не был загружен через POST HTTP.');

            /*filter*/
            if(isSet($aFilter['size']))
                if((int)$aCurrentFile['size'] > (int)$aFilter['size'])  throw new Exception('Превышен максимально допустимый размер файла.');

            if(isSet($aFilter['name']) AND count($aFilter['name']))
                if(!in_array($aCurrentFile['name'], $aFilter['name'])) throw new Exception("Файл {$aCurrentFile['name']} не требуется.");


            if(isSet($aFilter['fieldName']) AND count($aFilter['fieldName']))
                if(!in_array($aCurrentFile['fieldName'], $aFilter['fieldName'])) throw new Exception("Файл {$aCurrentFile['name']} не требуется.");

            /* Если хотим проверять значения размеров то предварительно их получаем */
            if(isSet($aFilter['imgMaxWidth']) OR isSet($aFilter['imgMaxHeight'])) {
                $aImageInfo = getimagesize($aCurrentFile['tmp_name']);
                if(!count($aImageInfo)) throw new Exception('Файл не является изображением!');
                $iImageWidth  = $aImageInfo[0];
                $iImageHeight = $aImageInfo[1];

            }

            if(isSet($aFilter['imgMaxWidth']) AND is_numeric($aFilter['imgMaxWidth'])) {

                if($aFilter['imgMaxWidth'] < $iImageWidth) throw new Exception('Изображение имеет недопустимые размеры!');

            }

            if(isSet($aFilter['imgMaxHeight']) AND is_numeric($aFilter['imgMaxHeight'])) {

                if($aFilter['imgMaxHeight'] < $iImageHeight) throw new Exception('Изображение имеет недопустимые размеры!');

            }

            $sFileExtension = skFiles::getExtension($aCurrentFile['name']);

            if(isSet($aFilter['allowExtensions']) AND count($aFilter['allowExtensions']))
                if(!in_array(strtolower($sFileExtension), $aFilter['allowExtensions'])) throw new Exception("Файл {$aCurrentFile['name']} имеет неверный формат.");

            list($sType, $sFormat) = explode('/', $aCurrentFile['type']);

            if($sType and $sFormat) {

                if(isSet($aFilter['type']) AND count($aFilter['type']))
                    if(!in_array($sType, $aFilter['type'])) throw new Exception("Недопустимый тип файла.");

                if(isSet($aFilter['format']) AND count($aFilter['format']))
                    if(!in_array($sFormat, $aFilter['format'])) throw new Exception("Недопустимый формат файла.");
            }

            //$oFile = new skUploadedFile('');
            //if($oFile->initUploadedFile($aCurrentFile))
                return $aCurrentFile;

        } catch (Exception $e) {
            self::$mError = $e->getMessage();
        }

        return false;
    }

    /**
     * Возвращает текущий ключ итератора
     * @return int
     */
    public function key() {
        return self::$iPos;
    }

    /**
     * Инкрементор итератора
     */
    public function next() {
        ++self::$iPos;
    }

    /**
     * Возвращает true, если
     * @return bool
     */
    public function valid() {
        return isset(self::$aFiles[self::$iPos]);
    }

    /**
     * Возвращает количество загруженных файлов
     * @return int
     */
    public function count() {
        return count(self::$aFiles);
    }

    /**
     * Перемещает текущий, обрабатываемый файл в итераторе в директорию, указанную в $sFilePath. Перемещение файла возможно
     * только в в рамках корневой директории FILEPATH или PRIVATE_FILEPATH в зависимости от состояния флага $bProtected.
     * 1. Если не указано $sNewFileName, используется текущее имя файла. После предварительной обработки функцией makeURLValidName
     * 2. Если файл с преобразованным именем существует, то генерируется уникальный хеш, дописываемый в конец имени файла.
     * 3. Если директория назначения для перемещаемого файла не найдена - она создается.
     * 4. Если Указано $sNewFileName - используется вместо текущего имени файла, после предварительной обработки makeURLValidName
     * @param string $sFilePath Директория назначения перемещаемого файла
     * @param bool $bProtected Флаг перемещения файла в закрытую директорию
     * @param string $sNewFileName Новое имя файла
     * @throws Exception
     * @return bool|string
     */
    public function move($sFilePath, $bProtected = false, $sNewFileName = '') {
        try {

            if(!empty($sNewFileName))
                $sNewFileName = skFiles::makeURLValidName($sNewFileName);
            else
                $sNewFileName = skFiles::makeURLValidName(self::$aFiles[self::$iPos]['name']);

            if(!$sNewFilePath = skFiles::createFolderPath($sFilePath, $bProtected)) throw new Exception('Невозможно создать директорию '.$sFilePath);

            $iCounter = 0;

            if(file_exists($sNewFilePath.$sNewFileName))
                while(file_exists($sNewFilePath.$sNewFileName)) {
                    $sNewFileName = skFiles::makeURLValidName($sNewFileName, true, true);
                    $iCounter++;
                    if($iCounter > 100) throw new Exception('Ошибка перемещения файла');
                }

            $sFullFileName = $sNewFilePath.$sNewFileName;

            $bMoveRes = move_uploaded_file(self::$aFiles[self::$iPos]['tmp_name'], $sFullFileName);

            // если загрука прошла удачно
            if ( $bMoveRes ) {

                // права
                chmod($sFullFileName, 0644);

                // обработка пользовательской функции после загрузки
                if ( $this->aCallOnUpload ) {

                    // собираем параметры
                    $aParams = self::$aFiles[self::$iPos];
                    $aParams['name'] = $sFullFileName;
                    $aParams['fileDir'] = $sNewFilePath;
                    $aParams['fileName'] = $sNewFileName;

                    // вызываем
                    call_user_func( $this->aCallOnUpload, $aParams );

                }

            }

        } catch (Exception $e){
            return false;
        }
        /** @todo Было так - как оказалось, не правильно! */
        //return ($bMoveRes)? self::$sUploadPath.$sNewFilePath: false;
        return ($bMoveRes)? $sFullFileName: false;
    }// func


    /**
     * Перемещает текущий загруженный файл итератора в директорию раздела $iSectionId.
     * 1. Если указан путь в поддиректории $sFolderName - файл перемещается по этому пути.
     * 2. Если указан $sFileName - файл перемещается с новым именем
     * 3. Если $bProtected равен true - файл перемещается в закрытую  директорию.
     * 4. Пути к корневым директориям указаны в константах FILEPATH и PRIVATE_FILEPATH
     * @param integer $iSectionId Id раздела
     * @param string $sFolderName путь по директориям раздела
     * @param string $sFileName Имя файла
     * @param bool $bProtected Флаг переноса в закрытую директорию
     * @return bool|string
     */
    public function UploadToSection($iSectionId, $sFolderName = '', $sFileName = '', $bProtected = false) {

        if(!$iSectionId) return false;

        $sFilePath = $iSectionId.DIRECTORY_SEPARATOR.$sFolderName;
        if ( $sFolderName )
            $sFilePath .= DIRECTORY_SEPARATOR;
        return $this->move($sFilePath, $bProtected, $sFileName);

    }// func

    /** @var array массив с параметрами обработчика после загрузки */
    protected $aCallOnUpload = null;

    /**
     * Установка обработчика, вызяваемого после переменщения загруженных файлов
     * @param array $aCall массив с 2 строками: имя класса и имя метода
     * @return bool
     */
    public function setOnUpload( $aCall ){

        // проверка входного массива
        if ( !is_array($aCall) or count($aCall)!=2 )
            return false;

        // имена параметров
        $sClass = (string)$aCall[0];
        $sMethod = (string)$aCall[1];

        // проверка наличия элементов
        if ( !class_exists($sClass) or !method_exists($sClass,$sMethod) )
            return false;

        $this->aCallOnUpload = $aCall;
        return true;

    }


}// class
