<?php
/**
 * Wrapper for external library blowfish
 * @link https://github.com/themattharris/PHP-Blowfish
 *
 * @class skBlowfish
 * @uses Blowfish
 *
 * @author ArmiT, $Author: sapozhkov $
 * @version $Revision: 16 $
 * @date $Date: 2012-05-21 14:42:32 +0400 (Пн., 21 мая 2012) $
 * @project Skewer
 * @package Kernel
 *
 * @example
 *
 * <pre>
 *    Encode:
 *    $encodedText = Blowfish::encrypt(
 *                              'text for encrypt',             # text for encode
 *                              'This is my secret key',        # encryption key
 *                              Blowfish::BLOWFISH_MODE_CBC,    # Encryption Mode
 *                              Blowfish::BLOWFISH_PADDING_RFC, # Padding Style
 *                              'x03nMwK34x&ciSUH0I1got'        # Initi Vector - required for CBC
 *                  );
 *    Decode:
 *      $deciphered = Blowfish::decrypt(
 *                                  'encrypted text',               # text for decode
 *                                  'This is my secret key',        # encryption key
 *                                  Blowfish::BLOWFISH_MODE_CBC,    # Encryption Mode
 *                                  Blowfish::BLOWFISH_PADDING_RFC, # Padding Style
 *                                  'x03nMwK34x&ciSUH0I1got'        # Initialisation Vector - required for CBC
 *                    );
 * </pre>
 */

require_once COREPATH.'libs/Blowfish/blowfish.php';
class skBlowfish {


    /**
     * Вектор инициализации для CBC режима
     * @var string
     */
    private $sIv = '';


    /**
     * Устанавливает вектор инициализации для режима CBC
     * @param $sIv
     * @return mixed
     */
    public function setIv($sIv) {

        return $this->sIv = $sIv;

    }// func

    /**
     * Зашифровывает $text алгоритмом Blowfish используя ключ $key
     * @param string $text Исходный текст
     * @param string $key Ключ для шифрования
     * @throws Exception
     * @return bool|string Возвращает зашифрованный текст
     */
    public function encrypt($text, $key) {

        try {

            if(empty($this->sIv)) throw new Exception('Init vector not defined!');

            return Blowfish::encrypt($text, $key, Blowfish::BLOWFISH_MODE_CBC,Blowfish::BLOWFISH_PADDING_RFC, $this->sIv);

        } catch(skException $e) {

            return false;
        }
    }// func

    /**
     * Расшифровывает $text алгоритмом Blowfish используя ключ $key
     * @param string $text Зашифованный текст
     * @param string $key Ключ для расшифровки
     * @throws Exception
     * @return bool|string Возвращает расшифрованный текст
     */
    public function decrypt($text, $key) {

        try {

            if(empty($this->sIv)) throw new Exception('Init vector not defined!');

            return Blowfish::decrypt($text, $key, Blowfish::BLOWFISH_MODE_CBC, Blowfish::BLOWFISH_PADDING_RFC, $this->sIv);

        } catch(skException $e) {

            return false;
        }
    }// func

}// class
