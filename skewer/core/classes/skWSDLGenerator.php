<?php
/**
 * Класс-генератор WSDL документа на основе PHP класса
 * Основано на YII::CWsdlGenerator
 *
 * @class skWSDLGenerator
 * @author ArmiT, $Author: sapozhkov $
 * @version $Revision: 6 $
 * @date $Date: 2012-05-18 10:52:53 +0400 (Пт., 18 мая 2012) $
 * @project Skewer
 * @package Kernel
 * @copyright Copyright &copy; 2008-2011 Yii Software LLC
 * @copyright Copyright &copy; 2012 Twins LLC
 */
class skWSDLGenerator {


    /**
     * Пространство имен, используемое в wsdl документе. Если не указано, используется имя
     * класса
     * @var string
     */
    protected $sNamespace;

    /**
     * Имя генерируемого wsdl
     * Если не указано - используется "urn:{$className}wsdl".
     * @var string
     *
     */
    protected $sServiceName = '';

    /**
     * Стек операторов
     * @var array
     */
    private $aOperations = array();

    /**
     * Стек типов
     * @var array
     */
    private $aTypes = array();

    /**
     * Стек сообщений
     * @var array
     */
    private $aMessages = array();

    /**
     * Устанавливает в качестве имени сервиса $sServiceName
     * @param string $sServiceName
     * @return string
     */
    public function setServiceName($sServiceName) {

        return $this->sServiceName = $sServiceName;

    }// func

    /**
     * Устанавливает пространство имен для WSDL
     * @param string $sNamespace
     * @return mixed
     */
    public function setNamespace($sNamespace) {

        return $this->sNamespace = $sNamespace;

    }// func

    /**
     * Создает WSDL описание для класса $sClassName
     * @param string $sClassName Имя класса-обработчика web-сервиса
     * @param string $sUrl URL web-сервиса
     * @param string $sEncoding Кодировка документа
     * @return bool|string Возвращает текст документа либо false в случае ошибки
     * @throws Exception
     */
    public function createWsdl($sClassName, $sUrl, $sEncoding='UTF-8') {

        try {

            if(!class_exists($sClassName)) throw new Exception();

            if(empty($this->sServiceName)) $this->sServiceName = $sClassName;
            if(empty($this->sNamespace))   $this->sNamespace = "urn:{$sClassName}wsdl";

            $oRefClass = new ReflectionClass($sClassName);

            if($aMethods = $oRefClass->getMethods())
                foreach($aMethods as $oMethod) {
                    /** @var ReflectionMethod $oMethod */
                    if(!$oMethod->isPublic()) continue;
                    $this->addMethod($oMethod);
                }// each method of class

            return $this->makeWSDLContent($sUrl,$sEncoding)->saveXML();

        } catch(Exception $e) {

            return false;
        }

    }// func

    /**
     * Добавляет в стек описание метода $oMethod на основе объекта ReflectionMethod
     * @param ReflectionMethod $oMethod Объект метода класса
     * @return bool Возвращает true если метод был добавлен либо false в случае ошибки
     * @throws Exception
     */
    private function addMethod(ReflectionMethod $oMethod) {

        try {

            $sDoc = $oMethod->getDocComment();
            if(strpos($sDoc,'@soap') === false)
                throw new Exception('This method is not for soap!');

            $sMethodName = $oMethod->getName();
            $sDoc = preg_replace('/^\s*\**(\s*?$|\s*)/m','',$sDoc);
            $aParams = $oMethod->getParameters();
            $aMessage = array();
            $n = preg_match_all('/^@param\s+([\w\.]+(\[\s*\])?)\s*?(.*)$/im', $sDoc, $aMatches);

            if($n > count($aParams)) $n = count($aParams);

            for($i=0; $i<$n; ++$i)
                $aMessage[$aParams[$i]->getName()] = array($this->addType($aMatches[1][$i]), trim($aMatches[3][$i]));

            $this->aMessages[$sMethodName.'Request'] = $aMessage;

            if(preg_match('/^@return\s+([\w\.]+(\[\s*\])?)\s*?(.*)$/im', $sDoc, $aMatches))
                $aReturn = array($this->addType($aMatches[1]),trim($aMatches[2]));
            else
                $aReturn = null;
            $this->aMessages[$sMethodName.'Response']=array('return'=>$aReturn);

            if(preg_match('/^\/\*+\s*([^@]*?)\n@/s',$sDoc,$aMatches))
                $sDoc = trim($aMatches[1]);
            else
                $sDoc = '';
            $this->aOperations[$sMethodName] = $sDoc;

        } catch(Exception $e) {

            return false;
        }

        return true;
    }// func

    /**
     * Добавляет в стек типов php тип $sType. Преобразование происходит автоматически
     * @param string $sType Добавляемый тип в php нотации
     * @return string
     */
    private function addType($sType) {

        $aDefTypes = array(
            'str' => 'xsd:string',
            'string' => 'xsd:string',

            'int' => 'xsd:int',
            'integer' => 'xsd:integer',

            'float' => 'xsd:float',
            'double' => 'xsd:float',

            'bool' => 'xsd:boolean',
            'boolean' => 'xsd:boolean',

            'date' => 'xsd:date',
            'time' => 'xsd:time',
            'datetime' => 'xsd:dateTime',

            'array' => 'soap-enc:Array',
            'object' => 'xsd:struct',
            'mixed' => 'xsd:anyType',
        );
        if (isset($aDefTypes[$sType])) {
            return $aDefTypes[$sType];

        } else {
            if (isset($this->aTypes[$sType])) {
                return is_array($this->aTypes[$sType]) ? 'tns:'.$sType : $this->aTypes[$sType];

            } else {
                if (($pos = strpos($sType, '[]')) !== false) {
                    $sType = substr($sType, 0, $pos);
                    if (isset($aDefTypes[$sType])) {
                        $this->aTypes[$sType.'[]'] = 'xsd:'.$sType.'Array';

                    } else {
                        $this->aTypes[$sType.'[]'] = 'tns:'.$sType.'Array';
                        $this->addType($sType);
                    }
                    return $this->aTypes[$sType.'[]'];

                } else {
                    $this->aTypes[$sType] = array();
                    $class = new ReflectionClass($sType);

                    foreach ($class->getProperties() as $property) {
                        $comment = $property->getDocComment();
                        if ($property->isPublic() && strpos($comment, '@soap') !== false) {
                            if (preg_match('/@var\s+([\w\.]+(\[\s*\])?)\s*?(.*)$/mi', $comment, $matches))
                                $this->aTypes[$sType][$property->getName()] = array($this->addType($matches[1]), trim($matches[3]));
                        }
                    }
                    return 'tns:'.$sType;
                }
            }
        }
    }// func

    /**
     * Генерирует WSDL документ с адресом сервиса $sServiceUrl в кодировке $sEncoding
     * @param string $sServiceUrl
     * @param string $sEncoding
     * @return DOMDocument
     */
    private function makeWSDLContent($sServiceUrl, $sEncoding) {

        $sXml = "<?xml version=\"1.0\" encoding=\"$sEncoding\"?>
                 <definitions name=\"{$this->sServiceName}\" targetNamespace=\"{$this->sNamespace}\"
                 xmlns=\"http://schemas.xmlsoap.org/wsdl/\"
                 xmlns:tns=\"{$this->sNamespace}\"
                 xmlns:soap=\"http://schemas.xmlsoap.org/wsdl/soap/\"
                 xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"
                 xmlns:wsdl=\"http://schemas.xmlsoap.org/wsdl/\"
                 xmlns:soap-enc=\"http://schemas.xmlsoap.org/soap/encoding/\"></definitions>";

        $oDOM = new DOMDocument();
        $oDOM->loadXml($sXml);
        $this->addTypes($oDOM);
        $this->addMessages($oDOM);
        $this->addPortTypes($oDOM);
        $this->addBindings($oDOM);
        $this->addService($oDOM, $sServiceUrl);

        return $oDOM;

    }// func

    /**
     * Добавляет тип к описанию WSDL в объекте $oDOM
     * @param DOMDocument $oDOM
     * @return bool
     */
    private function addTypes(DOMDocument $oDOM) {

        if($this->aTypes === array()) return false;

        $aTypes  = $oDOM->createElement('wsdl:types');
        $schema = $oDOM->createElement('xsd:schema');
        $schema->setAttribute('targetNamespace',$this->sNamespace);
        foreach($this->aTypes as $phpType=>$xmlType) {
            if(is_string($xmlType) && strrpos($xmlType,'Array')!==strlen($xmlType)-5)
                continue;  // simple type
            $complexType = $oDOM->createElement('xsd:complexType');

            if(is_string($xmlType)) {

                if(($pos=strpos($xmlType,'tns:'))!==false)
                    $complexType->setAttribute('name',substr($xmlType,4));
                else

                    $complexType->setAttribute('name',$xmlType);
                $complexContent=$oDOM->createElement('xsd:complexContent');
                $restriction=$oDOM->createElement('xsd:restriction');
                $restriction->setAttribute('base','soap-enc:Array');
                $attribute=$oDOM->createElement('xsd:attribute');
                $attribute->setAttribute('ref','soap-enc:arrayType');
                $attribute->setAttribute('wsdl:arrayType',substr($xmlType,0,strlen($xmlType)-5).'[]');
                $restriction->appendChild($attribute);
                $complexContent->appendChild($restriction);
                $complexType->appendChild($complexContent);

            } else if(is_array($xmlType)) {

                $complexType->setAttribute('name',$phpType);
                $all=$oDOM->createElement('xsd:all');
                foreach($xmlType as $name=>$type) {

                    $element=$oDOM->createElement('xsd:element');
                    $element->setAttribute('name',$name);
                    $element->setAttribute('type',$type[0]);
                    $all->appendChild($element);
                }
                $complexType->appendChild($all);
            }
            $schema->appendChild($complexType);
            $aTypes->appendChild($schema);
        }

        $oDOM->documentElement->appendChild($aTypes);

        return true;
    }// func

    /**
     * Добавляет описания из стека к WSDL в объекте $oDOM
     * @param DOMDocument $oDOM
     */
    private function addMessages(DOMDocument $oDOM) {

        foreach($this->aMessages as $name=>$message) {

            $element = $oDOM->createElement('wsdl:message');
            $element->setAttribute('name',$name);

            foreach($this->aMessages[$name] as $partName=>$part) {
                if(is_array($part)) {

                    $partElement=$oDOM->createElement('wsdl:part');
                    $partElement->setAttribute('name',$partName);
                    $partElement->setAttribute('type',$part[0]);
                    $element->appendChild($partElement);
                }
            }
            $oDOM->documentElement->appendChild($element);
        }
    }// func

    /**
     * Add PortTypes from stack to the $oDOM
     * @param DOMDocument $oDOM
     */
    private function addPortTypes(DOMDocument $oDOM) {

        $portType = $oDOM->createElement('wsdl:portType');
        $portType->setAttribute('name',$this->sServiceName.'PortType');
        $oDOM->documentElement->appendChild($portType);

        foreach($this->aOperations as $name=>$doc)
            $portType->appendChild($this->createPortElement($oDOM,$name,$doc));
    }// func

    /**
     * @param DOMDocument $oDOM
     * @param $sName
     * @param $doc
     * @return DOMElement
     */
    private function createPortElement(DOMDocument $oDOM, $sName, $doc) {

        $operation=$oDOM->createElement('wsdl:operation');
        $operation->setAttribute('name',$sName);

        $input = $oDOM->createElement('wsdl:input');
        $input->setAttribute('message', 'tns:'.$sName.'Request');
        $output = $oDOM->createElement('wsdl:output');
        $output->setAttribute('message', 'tns:'.$sName.'Response');

        $operation->appendChild($oDOM->createElement('wsdl:documentation',$doc));
        $operation->appendChild($input);
        $operation->appendChild($output);

        return $operation;
    }// func

    /**
     * @param DOMDocument $oDOM
     */
    private function addBindings(DOMDocument $oDOM) {

        $binding = $oDOM->createElement('wsdl:binding');
        $binding->setAttribute('name',$this->sServiceName.'Binding');
        $binding->setAttribute('type','tns:'.$this->sServiceName.'PortType');

        $soapBinding = $oDOM->createElement('soap:binding');
        $soapBinding->setAttribute('style','rpc');
        $soapBinding->setAttribute('transport','http://schemas.xmlsoap.org/soap/http');
        $binding->appendChild($soapBinding);

        $oDOM->documentElement->appendChild($binding);

        foreach($this->aOperations as $name=>$doc)
            $binding->appendChild($this->createOperationElement($oDOM,$name));
    }// func

    /**
     * @param DOMDocument $oDOM
     * @param $sName
     * @return DOMElement
     */
    private function createOperationElement(DOMDocument $oDOM, $sName) {

        $operation = $oDOM->createElement('wsdl:operation');
        $operation->setAttribute('name', $sName);
        $soapOperation = $oDOM->createElement('soap:operation');
        $soapOperation->setAttribute('soapAction', $this->sNamespace.'#'.$sName);
        $soapOperation->setAttribute('style','rpc');

        $input = $oDOM->createElement('wsdl:input');
        $output = $oDOM->createElement('wsdl:output');

        $soapBody = $oDOM->createElement('soap:body');
        $soapBody->setAttribute('use', 'encoded');
        $soapBody->setAttribute('namespace', $this->sNamespace);
        $soapBody->setAttribute('encodingStyle', 'http://schemas.xmlsoap.org/soap/encoding/');
        $input->appendChild($soapBody);
        $output->appendChild(clone $soapBody);

        $operation->appendChild($soapOperation);
        $operation->appendChild($input);
        $operation->appendChild($output);

        return $operation;
    }// func

    /**
     * @param DOMDocument $oDOM
     * @param $sServiceUrl
     */
    private function addService(DOMDocument $oDOM, $sServiceUrl) {

        $service=$oDOM->createElement('wsdl:service');
        $service->setAttribute('name', $this->sServiceName.'Service');

        $port=$oDOM->createElement('wsdl:port');
        $port->setAttribute('name', $this->sServiceName.'Port');
        $port->setAttribute('binding', 'tns:'.$this->sServiceName.'Binding');

        $soapAddress=$oDOM->createElement('soap:address');
        $soapAddress->setAttribute('location',$sServiceUrl);
        $port->appendChild($soapAddress);
        $service->appendChild($port);
        $oDOM->documentElement->appendChild($service);
    }// func

}// class
