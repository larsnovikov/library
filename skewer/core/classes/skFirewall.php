<?php
/**
 *
 * @class skFirewall
 *
 * @author ArmiT, $Author$
 * @version $Revision$
 * @date $Date$
 * @project JetBrains PhpStorm
 * @package kernel
 */

require_once(COREPATH.'libs/Firewall/mappers/firewallMapper.php');
require_once(COREPATH.'libs/Firewall/Firewall.php');
require_once(COREPATH.'libs/Firewall/classes/FirewallFilter.php');
require_once(COREPATH.'libs/Firewall/classes/FirewallAction.php');
require_once(COREPATH.'libs/Firewall/classes/FirewallEntity.php');
require_once(COREPATH.'libs/Firewall/entities/FirewallFilterEntity.php');
require_once(COREPATH.'libs/Firewall/entities/FirewallActionEntity.php');


class skFirewall extends Firewall {



}// class


