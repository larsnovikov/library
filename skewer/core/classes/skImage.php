<?php
/**
 * Библиотека для обработки изображений
 * @class skImage
 *
 * @author ArmiT, $Author: acat $
 * @version $Revision: 961 $
 * @date $Date: 2012-10-03 16:29:35 +0400 (Ср., 03 окт. 2012) $
 * @project Skewer
 * @package Kernel
 */
class skImage {

    /* Properties */

    /**
     * Ограничение. максимальная площадь изображения в пикселах;
     * @var int
     */
    protected $iSrcMaxSquare = 25000000;

    /**
     * Ограничение. максимальная высота загружаемого изображения;
     * @var int
     */
    protected static $iSrcMaxHeight = 5000;

    /**
     * Ограничение. максимальная ширина загружеемого изображения;
     * @var int
     */
    protected static $iSrcMaxWidth = 5000;

    /**
     * Ограничение. максимальный размер файла ( 8Мб );
     * @var int
     */
    protected $iSrcMaxSize = 8388608;

    /**
     * Имя загруженного изображения;
     * @var string
     */
    protected $sSrcFileName = '';

    /**
     * Высота загруженного изображения;
     * @var int
     */
    protected $iSrcHeight = 0;

    /**
     * Отдает высоту оригинала
     * @return int
     */
    public function getSrcHeight() {
        return $this->iSrcHeight;
    }

    /**
     * Ширина загруженного изображения;
     * @var int
     */
    protected $iSrcWidth = 0;

    /**
     * Тип загруженного файла;
     * @var int
     */
    protected $iSrcImageType = 0;

    /**
     * Доступные форматы
     * @var array
     */
    protected static $aSrcImageTypes = array(
        2 => 'jpg',
        1 => 'gif',
        3 => 'png'
    );

    /* Текущие параметры */

    /**
     * Текущее рабочее поле;
     * @var bool|resource
     */
    protected $image = false;

    /**
     * Высота текущего изображения;
     * @var int
     */
    protected $iCurrentHeight = 0;

    /**
    * Ширина текущего изображения;
    * @var int
    */
    protected $iCurrentWidth = 0;

    /**
     * Тип изображения при генерации файла;
     * @var int
     */
    protected $iCurrentType = 0;

    /**
     * Качество создаваемого изображения. используется только для jpg;
     * @var int
     */
    protected $iCurrentQuality = 95;

    /* Буфер */

    /**
     * Рабочее поле в буфере;
     * @var bool|resource
     */
    protected $rImageBuffer = false;

    /**
     * Высота рабочего поля в буфере;
     * @var int
     */
    protected $iBufferHeight = 0;

    /**
     * Ширина рабочего поля в буфере;
     * @var int
     */
    protected $iBufferWidth = 0;


    /* Служебные */

    /**
     * имя формата обработки;
     * @deprecated
     */
    protected $format_name = '';

    /**
     * папка для сохранения изображений;
     * @deprecated
     */
    protected $trg_def_dir = 'photos/img';

    /**
     * Изменение мест параметров при вертикальном изображении
     * @var bool
     */
    protected $bRotate = false;

    /**
     * вписать изображение
     * @var bool
     */
    protected $bAccomodate = true;

    /**
     * цвет фона при вписывании
     * @var int
     */
    protected $iBackgroundColor = 16777215;

    /**
     * используемый движок ( gd / im )
     * @deprecated
     */
    protected $engine = 'gd';

    /**
     * Используемый для watermark`ов шрифт (ищет в <skewer_base_path>/build/<version>/common/fonts/)
     * @var string
     */
    protected $sFont = 'font3.ttf';

    /* Methods */

    /**
     * Создает новое рабочее пространство для обработки изображения
     * @param int $iWidth Ширина рабочего поля
     * @param int $iHeight Высота рабочего поля\
     * @return resource|bool Возвращает указатель на созданный ресурс либо false в случае возникновения ошибки
     */
    public function create($iWidth, $iHeight) {

        $this->iCurrentHeight = $iHeight;
        $this->iCurrentWidth = $iWidth;
        $this->image = imagecreatetruecolor($iWidth, $iHeight);

        return $this->image;

    }// func

    /**
     * Загружает файл $sFileName в рабочую область
     * @param string $sFileName путь к файлу изображения
     * @return bool
     */
    public function load( $sFileName ){

        // проверка ниличия файлов
        if ( !file_exists($sFileName) ) return false;

        // проверка размера файла
        if ( filesize($sFileName) > $this->iSrcMaxSize ) return false;

        // получение лин. размеров и типа изображения
        list($iWidth, $iHeight, $iType) = getimagesize( $sFileName );

        // проверка ограничений размеров и типов
        if ( $iHeight > self::$iSrcMaxHeight OR
             $iWidth > self::$iSrcMaxWidth   OR
             ($iWidth*$iHeight) > $this->iSrcMaxSquare OR
             !isset(self::$aSrcImageTypes[$iType]) ) return false;

        // занесение данных во внутренние переменные
        $this->iSrcWidth      = $iWidth;
        $this->iSrcHeight     = $iHeight;
        $this->iCurrentType   = $iType;
        $this->sSrcFileName   = $sFileName;
        $this->iCurrentWidth  = $iWidth;
        $this->iSrcImageType  = $iType;
        $this->iCurrentHeight = $iHeight;

        // накладывение изображения из файла
        switch ($iType) {

            case 1: // gif
                $this->image = imagecreatefromgif($sFileName);
                break;
            case 2: // jpg
                $this->image = imagecreatefromjpeg($sFileName);
                break;
            case 3: // png
                $this->image = imagecreatefrompng($sFileName);
                break;
            default:
                return false;

        }// switch

        return true;
    } // func

    /**
     * Возвращает тип изображения
     * @return mixed
     */
    public function getImageType() {

        return self::$aSrcImageTypes[$this->iCurrentType];
    }// func

    /**
     * Отдает набор разрешенных типоа файтов
     * @static
     * @return array
     */
    public static function getAllowImageTypes() {
        return self::$aSrcImageTypes;
    }

    /**
     * Отдает максимальный допустимый линейный размер изрбражения
     * @static
     * @return mixed
     */
    public static function getMaxLineSize() {
        return max( self::$iSrcMaxHeight, self::$iSrcMaxWidth );
    }

    /**
     * Возвращает бинарник файла на стандартный выход
     * @return int
     */
    public function getFile() {

        return (int)$this->save('');

    }// func

    /**
     * Сохраняет файл. Если $sFileName задано, то с таким именем, иначе в папку trg_def_dir.
     * Возвращает строку с именем созданного файла или false;
     * @param  string $sFileName Имя создаваемого сайта
     * @return bool|string
     */
    public function save( $sFileName ){

        if ( !$this->image ) return false;

        switch ( $this->iCurrentType ? $this->iCurrentType : $this->iSrcImageType ) {
            case 1:
                imagegif($this->image, $sFileName ? $sFileName : null);
                break;
            case 2:
                imagejpeg($this->image, $sFileName, $this->iCurrentQuality);
                break;
            case 3:
                imagepng($this->image, $sFileName ? $sFileName : null);
                break;
            default:
                return false;
        }// switch

        return $sFileName ? $sFileName : false;

    } // function save


    //
    /**
     * очистка объекта
     * Очищает текущее состояние буфера иопределителей текущего состояния
     */
    public function clear(){

        if ( $this->image ) imagedestroy($this->image);

        $this->image = false;
        $this->sSrcFileName = '';
        $this->iSrcHeight = 0;
        $this->iSrcWidth = 0;
        $this->iSrcImageType = 0;
        $this->iCurrentHeight = 0;
        $this->iCurrentWidth = 0;

        $this->clearBuffer();

    }// func

    /**
     * Cохраняет текущую рабочую область в буфер
     * @return bool
     */
    public function saveToBuffer() {

        if( !$this->image ) return false;

        $iWidth  = $this->iBufferWidth = imagesx($this->image);
        $iHeight = $this->iBufferHeight = imagesy($this->image);

        if ( $this->rImageBuffer ) imagedestroy($this->rImageBuffer);

        $this->rImageBuffer = imagecreatetruecolor($iWidth, $iHeight);
        imagecopy($this->rImageBuffer, $this->image, 0, 0, 0, 0, $iWidth, $iHeight);

        return true;

    }// func

    /**
     * Возвращает массив с размерами текущего обрабатываемого изображения либо false
     * @return array|bool array(width, height)
     */
    public function getSize() {

        if ( !$this->image ) return false;

        return array(imagesx($this->image),imagesy($this->image));
    }// func

    /**
     * Загружает рабочую область из буфера
     * @return bool
     */
    public function loadFromBuffer() {

        if ( !$this->rImageBuffer ) return false;

        $iWidth  = $this->iCurrentWidth = imagesx($this->rImageBuffer);
        $iHeight = $this->iCurrentHeight = imagesy($this->rImageBuffer);

        imagedestroy($this->image);
        $this->image = imagecreatetruecolor($iWidth, $iHeight);
        imagecopy($this->image, $this->rImageBuffer, 0, 0, 0, 0, $iWidth, $iHeight);

        return true;

    }// func

    /**
     * Очищает буфер
     * @return bool
     */
    function clearBuffer() {

        if ( $this->rImageBuffer ) imagedestroy($this->rImageBuffer);

        $this->rImageBuffer = false;
        $this->iBufferHeight = 0;
        $this->iBufferWidth = 0;

        return true;

    }// func

    /**
     * Осуществляет изменение размера исходного изображения. Если указаны параметры $iWidth и $iHeight происходит изменение размера (resize) изображения
     * до указанных. Если указаны параметры $iLeftCrop и $iTopCrop, то участок, находящийся левее и выше указанной точки будет исключен из результата (crop).
     * Если указаны парамеры $iWidthCrop $iHeightCrop, то изображение обрезается до указанной ширины и высоты. Параметр $iRoateImage указывает на необходимость
     * поворота изображения на 90 градусов, а $iAccomodateImage вписывает изображение в размеры.
     * @param int $iWidth Ширина изображения
     * @param int $iHeight Высота изображения
     * @param bool $iCropLeft Смещение по горизонтали для кропа
     * @param bool $iCropTop Смещение по вертикали для кропа
     * @param bool $iCropWidth Ширина изображения для кропа
     * @param bool $iCropHeight Высота изображения для кропа
     * @param int $iRoateImage Флаг поворота изображения
     * @param int $iAccomodateImage Флаг вписывания изображения в размеры
     * @return bool
     */
    function cropImage( $iWidth, $iHeight, $iCropLeft=false, $iCropTop=false, $iCropWidth=false, $iCropHeight=false, $iRoateImage=-1, $iAccomodateImage=-1 ) {

        if ( !$iWidth and !$iHeight and !$iCropLeft and !$iCropTop and !$iCropWidth and !$iCropHeight ) return true;

        // использование внутренних параметров
        if ( $iRoateImage === -1 )      $iRoateImage      = $this->bRotate;
        if ( $iAccomodateImage === -1 ) $iAccomodateImage = $this->bAccomodate;

        // приведение типов
        $iWidth = (int)$iWidth;
        $iHeight =(int)$iHeight;
        $iCropTop = (int)$iCropTop;
        $iCropLeft = (int)$iCropLeft;

        // обработка нулевых значений
        if ( $iCropWidth<1 )  $iCropWidth  = $this->iCurrentWidth;
        if ( $iCropHeight<1 ) $iCropHeight = $this->iCurrentHeight;

        // --- Обработка параметра "поворот"
        if ( $iRoateImage AND ( $this->iCurrentWidth < $this->iCurrentHeight ) ) {
            $i       = $iWidth;
            $iWidth  = $iHeight;
            $iHeight = $i;
        } // if roate

        // --- Проверка вхождения кропа в пределы изображения
        // вхождение начальных координат по нижней границе
        if( $iCropTop<0 )  $iCropTop  = 0;
        if( $iCropLeft<0 ) $iCropLeft = 0;

        // вхождение начальных координат по верхней границе
        if( $iCropTop > $this->iCurrentHeight ) $iCropTop  = $this->iCurrentHeight;
        if( $iCropLeft > $this->iCurrentWidth ) $iCropLeft = $this->iCurrentWidth;

        // вхождение длин
        if( $iCropWidth > $this->iCurrentWidth )    $iCropWidth  = $this->iCurrentWidth;
        if( $iCropHeight > $this->iCurrentHeight )  $iCropHeight = $this->iCurrentHeight;

        // вхождение сумм отступ+длина
        if( $iCropLeft + $iCropWidth > $this->iCurrentWidth ) $iCropLeft = $this->iCurrentWidth - $iCropWidth;
        if( $iCropTop + $iCropHeight > $this->iCurrentHeight ) $iCropTop = $this->iCurrentHeight - $iCropHeight;

        // вхождение начальных координат по нижней границе (после предыдущей проверки)
        if( $iCropTop <0 )  $iCropTop =0;
        if( $iCropLeft <0 ) $iCropLeft=0;

        // параметры для вставки
        $iDstWidth = $iCropWidth;
        $iDstHeight = $iCropHeight;

        // --- Анализ входых параметров без вписывания
        if ( !$iAccomodateImage ) {
            // если заданы оба параметра
            if ( $iWidth and $iHeight ){

                // отношения сторон вырезки и оригинала
                $kW = $iWidth/$iCropWidth;  // соотношение ширин
                $kH = $iHeight/$iCropHeight;// высот
                $kF = $iCropWidth/$iCropHeight;     // сторон в выходном форматеы

                // вычисляется по какой стороне больше не влезает
                if ( $kH < $kW ) {
                    $iDstWidth = $iWidth;
                    if ( $iDstWidth > $this->iCurrentWidth )
                        $iDstWidth = $this->iCurrentWidth;
                    $iDstHeight = (int)$iWidth/$kF;
                } else {
                    $iDstHeight = $iHeight;
                    if ( $iDstHeight > $this->iCurrentHeight )
                        $iDstHeight = $this->iCurrentHeight;
                    $iDstWidth = (int)$iDstHeight*$kF;
                }

            } else {

                // если ни один размер не задан, взять оригинальный размер
                if ( !$iHeight and !$iWidth ) {

                    $iDstWidth  = $iWidth  = $iCropWidth;
                    $iDstHeight = $iHeight = $iCropHeight;
                } else{ // не задан только один из размеров

                    if ( $iWidth )
                        $iWidth  = min($iWidth , $iCropWidth);
                    else
                        $iHeight = min($iHeight, $iCropHeight);

                    $iAccomodateImage = 1;
                }
            }
        } // if not enter

        // Анализ входых данных с параметром "вписать"
        if ( $iAccomodateImage ) {

            // обработка отсутствия финальных размеров изображения
            if ( !$iWidth and !$iHeight ) {

                $iWidth  = $iCropWidth;
                $iHeight = $iCropHeight;

            } elseif ( $iWidth xor $iHeight ) {

                if ( $iWidth )
                    $iHeight = round($iCropHeight*min($iWidth ,$iCropWidth)/$iCropWidth);
                else
                    $iWidth  = round($iCropWidth*min($iHeight,$iCropHeight)/$iCropHeight);
            }

            // вырезка
            if ( $iWidth < $iCropWidth or $iHeight < $iCropHeight ) {

                $iRate = min($iWidth/$iCropWidth, $iHeight/$iCropHeight);
                $iDstWidth = round($iCropWidth*$iRate);
                $iDstHeight = round($iCropHeight*$iRate);
            } else {
                $iDstWidth = $iCropWidth;
                $iDstHeight = $iCropHeight;
            }

        } // if enter

        // сдвиг в центр
        $iDstTop  = round( ($iHeight - $iDstHeight)/2 );
        $iDstLeft = round( ($iWidth - $iDstWidth)/2 );

        // --- Изменение изображения в рабочей области
        $rTempImage = $this->image;
        $this->create($iWidth, $iHeight);
        imagefill($this->image, 0, 0, $this->iBackgroundColor);

        imagecopyresampled(
            $this->image, $rTempImage,
            $iDstLeft, $iDstTop,
            $iCropLeft, $iCropTop,
            $iDstWidth, $iDstHeight,
            $iCropWidth, $iCropHeight
        );
        imagedestroy($rTempImage);

        return true;

    }// func

    /**
     * Изменяет размер изображения
     * @param int $iWidth Ширина результата
     * @param int $iHeight Высота результата
     * @param int $iRotateImage Флаг поворота
     * @param int $iAccomodateImage Флаг вписывания изображения
     * @return bool
     */
    public function resize($iWidth, $iHeight, $iRotateImage = -1, $iAccomodateImage = -1 ){

        if ( !(int)$iWidth and !(int)$iHeight ) return false;

        return $this->cropImage( $iWidth, $iHeight, false, false, false, false, $iRotateImage, $iAccomodateImage );

    }// func

    /**
     * Добавляет watermark(водяной знак к изображению)
     * possible watermark align types:
     * alignWatermarkTopLeft
     * alignWatermarkTopRight
     * alignWatermarkBottomLeft
     * alignWatermarkBottomRight
     * alignWatermarkCenter
     * @param string $sWatermark Путь к изображению водяного знака (png)
     * @param int $iAlign Тип выравнивания
     * @return bool
     */
    function applyWatermark($sWatermark, $iAlign = alignWatermarkBottomRight) {

        $sPossibleFileName = ROOTPATH.$sWatermark;
        $iAlphaLevel = 70; // прозрачность
        $iMargin = 10;     // отступы от краев

        if (file_exists($sPossibleFileName)) {

            list($iWMWidth, $iWMHeight, $iImageType) = getimagesize($sPossibleFileName);

            // файл точно является картинкой, причем png
            // у jpg нет прозрачности, а у gif - полупрозрачности (края получаются рваными)
            if ($iImageType == 3) {

                $rWatermarkImage = imagecreatefrompng($sPossibleFileName);

                switch($iAlign) {

                    case alignWatermarkTopLeft:

                        $iX = $iMargin;
                        $iY = $iMargin;

                        break;
                    case alignWatermarkTopRight:

                        $iX = $this->iCurrentWidth - $iWMWidth - $iMargin;
                        $iY = $iMargin;

                        break;
                    case alignWatermarkBottomLeft:

                        $iX = $iMargin;
                        $iY = $this->iCurrentHeight - $iWMHeight - $iMargin;

                        break;

                    default:
                    case alignWatermarkBottomRight:

                        $iX = $this->iCurrentWidth - $iWMWidth - $iMargin;
                        $iY = $this->iCurrentHeight - $iWMHeight - $iMargin;

                        break;
                    case alignWatermarkCenter:

                        $iX = ( $this->iCurrentWidth - $iWMWidth ) / 2;
                        $iY = ( $this->iCurrentHeight - $iWMHeight ) / 2;

                        break;

                }

                imagecopy($this->image, $rWatermarkImage, (int)$iX, (int)$iY, 0, 0, $iWMWidth, $iWMHeight);
                return true;
            }

        } else {

            $sFontFile = BUILDPATH . 'common/fonts/' . $this->sFont;
            $iR = 255;
            $iG = 255;
            $iB = 255;

            $iWMWidth  = imagesx($this->image);
            $iWMHeight = imagesy($this->image);
            //$iAngle  = -rad2deg(atan2((-$iHeight), ($iWidth)));
            $iAngle  = 0;
            $iTextColor = imagecolorallocatealpha($this->image, $iR, $iG, $iB, $iAlphaLevel);

            //$fSize = (($iWidth + $iHeight) / 2) * 2 / strlen($sWatermark);
            $fSize = 18;
            $aBox = imagettfbbox($fSize, $iAngle, $sFontFile, $sWatermark);

            /*
            * 0	lower left corner, X position
            * 1	lower left corner, Y position
            * 2	lower right corner, X position
            * 3	lower right corner, Y position
            * 4	upper right corner, X position
            * 5	upper right corner, Y position
            * 6	upper left corner, X position
            * 7	upper left corner, Y position
            */
            switch($iAlign) {

                case alignWatermarkTopLeft:

                    $iX = $iMargin;
                    $iY = $fSize + $iMargin;

                    break;
                case alignWatermarkTopRight:

                    $iX = $iWMWidth - abs($aBox[4] - $aBox[0]) -  $iMargin;
                    $iY = $fSize + $iMargin;

                    break;
                case alignWatermarkBottomLeft:

                    $iX = $iMargin;
                    $iY = $iWMHeight - $fSize;

                    break;
                case alignWatermarkBottomRight:

                    $iX = $iWMWidth - abs($aBox[4] - $aBox[0]) -  $iMargin;
                    $iY = $iWMHeight - $fSize;

                    break;
                case alignWatermarkCenter:

                    $iX = $iWMWidth / 2 - abs($aBox[4] - $aBox[0]) / 2;
                    $iY = $iWMHeight / 2 + abs($aBox[5] - $aBox[1]) / 2;

                    break;

                default:

                    $iX = 0;
                    $iY = $fSize + $iMargin;

                    break;

            }// type of align

            imagettftext($this->image, $fSize, $iAngle, $iX, $iY, $iTextColor, $sFontFile, $sWatermark);

            return true;
        }

        return false;

    }// func

    /**
     * Указывает на необходимость поворота изображения
     * @param boolean $bRotate
     */
    public function isRotate($bRotate) {
        $this->bRotate = $bRotate;
    }
}// class
