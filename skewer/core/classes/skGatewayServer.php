<?php
/**
 *
 * @class skGatewayServer
 *
 * @author ArmiT, $Author: sapozhkov $
 * @version $Revision: 16 $
 * @date $Date: 2012-05-21 14:42:32 +0400 (Пн., 21 мая 2012) $
 * @project Skewer
 * @package Kernel
 */
class skGatewayServer extends skGatewayPrototype {

    /**
     * @const int Тип валидации на выполнение - по принадлежности классу
     */
    const ValidateTypeConcreteClass = 0x03;

    /**
     * @const int Тип валидации на выполнение - по родительскому классу
     */
    const ValidateTypeParentClass = 0x04;

    /**
     * @const int Тип валидации на выполнение - по соответствию интерфейсу
     */
    const ValidateTypeInterface = 0x05;

    /**
     * Текущий тип валидации метода класса на выполнение
     * @var int
     */
    private $iValidateType = 0x04;

    /**
     * Значение валидации класса в зависимости от настроек
     * @var string
     */
    protected $sValidateValue = '';

    /**
     * Обработчик пришедших заголовков
     * @var null|callable|callback
     */
    protected $mHeaderHandler = null;

    /**
     * Массив ответов по результатам выполнения запросов имеет следующий вид:
     * ['response'] = <результат работы метода>
     * ['error']    = экземпляр, сгенерированного исключения
     * @var array
     */
    protected $aResponse = array();

    /**
     * @param int $iStreamMode тип пакетов
     */
    public function __construct($iStreamMode = 0x00) {

        $this->iStreamType = (int)$iStreamMode;
        return true;
    }// func

    /**
     * Указывает класс, методы потомков которого можно выполнять средствами протокола
     * @param string $sClassName
     */
    public function addParentClass($sClassName) {

        $this->sValidateValue = $sClassName;
        $this->iValidateType  = self::ValidateTypeParentClass;

    }// func

    /**
     * Указывает класс, методы которого можно выполнять средствами протокола
     * @param string $sClassName
     */
    public function addClass($sClassName) {

        $this->sValidateValue = $sClassName;
        $this->iValidateType  = self::ValidateTypeConcreteClass;

    }// func

    /**
     * Указывает интерфейс к которому должны пренадлежать классы, методы которых можно выполнять
     * @param string $sInterfaceName
     */
    public function addInterface($sInterfaceName) {

        $this->sValidateValue = $sInterfaceName;
        $this->iValidateType  = self::ValidateTypeInterface;

    }// func

    /**
     * Указыввает функцию обратного вызова для обработки пришедшего заголовка
     * @param callback|callable $mCalledMethod
     * @return bool
     */
    public function onLoadHeaderHandler($mCalledMethod) {

        if(!is_callable($mCalledMethod)) return false;

        $this->mHeaderHandler = $mCalledMethod;

        return true;
    }// func

    /**
     * Проверяет наличие строки запроса в массиве POST и возвращает ее либо генерирует исключение типа GatewayException
     * @return string
     * @throws GatewayException
     */
    protected function getRequest() {

        if(!count($_POST)) throw new GatewayException('Request error: Empty Request!');

        if(!isSet($_POST['_gateway_request']) OR empty($_POST['_gateway_request']))
            throw new GatewayException('Request error: Wrong Request!');

        return $_POST['_gateway_request'];
    }// func

    protected function parseRequest($sRequest) {

        if(!function_exists('json_decode'))  throw new GatewayException('Request error: JSON library not found!');

        $aPackage = json_decode(stripslashes($sRequest), true);

        if(!count($aPackage) OR
            !isSet($aPackage['Header']) OR
            !count($aPackage['Header']) OR
            !isSet($aPackage['Data']))
            throw new GatewayException('Request error: Package has wrong format!');

        $this->aHeader = $aPackage['Header'];

        if(!isSet($this->aHeader['Client']) OR
            !$this->aHeader['Client'] OR
            !isSet($this->aHeader['Certificate']) OR
            !$this->aHeader['Certificate'])
            throw new GatewayException('Request error: Package has wrong Header (Not found required fields)!');

        call_user_func_array($this->mHeaderHandler, array(&$this, $this->aHeader));

        $aPackage['Data'] = $this->decryptData($aPackage['Data'], $aPackage['Header']['crypted']);

        /** @todo Добавить проверку сертификата */

        //if(isSet($aPackage['Data']['Actions']))
        $this->aActions = (isSet($aPackage['Data']['Actions']) && count($aPackage['Data']['Actions']))? $aPackage['Data']['Actions']: null;

        $this->aFiles   = (isSet($aPackage['Data']['Files']) && count($aPackage['Data']['Files']))? $aPackage['Data']['Files']: null;

        if(!$this->aActions AND !$this->aFiles) throw new GatewayException('Request error: No actions & Files!');

    }// func

    /**
     * Проверяет правильность указания класса и метода для выполнения Согласно настройкам сервера. Их наличие и доступность.
     * Если метод является валидным с точки зрения настроек сервера, то происходит его выполнение.
     * @param array|callable|callback $aAction Вызываемые методы
     * @throws skGatewayExecuteException
     * @return bool Возвращает true если метод разрешен для выполнения либо false в случае ошибки
     */
    protected function executeAction($aAction) {

        /* Есть ли в массиве не менее двух параметров */
        if(!is_array($aAction) OR count($aAction) < 2) return false;

        /* Запись о классе есть и она не пустая */
        if(!isSet($aAction['Class']) OR empty($aAction['Class']))
            throw new skGatewayExecuteException('Error checking: Class ['.$aAction['Class'].'] not received!');

        $sClass = $aAction['Class'];

        /* Запись о методе есть и она не пустая */
        if(!isSet($aAction['Method']) OR empty($aAction['Method']))
            throw new skGatewayExecuteException('Error checking: Method ['.$aAction['Method'].'] not received!');

        $sMethod = $aAction['Method'];


        $aParameters = array();

        /* Запись о параметрах есть и она не пустая */
        if(isSet($aAction['Parameters']) OR count($aAction['Parameters']))
            $aParameters = $aAction['Parameters'];

        /* В зависимости от типа валидации */
        switch($this->iValidateType) {

            /* Заглушка */
            case self::ValidateTypeConcreteClass:

                throw new skGatewayExecuteException('Error checking: Mode [concrete class] is not implemented!');
                break;

            /* По родительскому классу */
            case self::ValidateTypeParentClass:

                /* Пытаемся получить описание класса */
                $oCalledClass = new ReflectionClass($sClass);

                if( !($oCalledClass instanceof ReflectionClass) ) throw new skGatewayExecuteException('Error checking: Class ['.$sClass.'] not found!');

                /* Запрашиваем родителя класса и проверяем на соответствие условию */
                if( $oCalledClass->getParentClass()->name != $this->sValidateValue)
                    throw new skGatewayExecuteException('Security error: Class ['.$sClass.'] not accessible!');

                break;

            /* Заглушка */
            case self::ValidateTypeInterface:

                throw new skGatewayExecuteException('Error checking: Mode [Interface] is not implemented!');
                break;

        }// case of validate type

        /* Проверяем наличие метода в классе */

        /* Пытаемся получить описание метода */
        $oCalledMethod = new ReflectionMethod($sClass, $sMethod);

        /* Метод найден */
        if( !($oCalledMethod instanceof ReflectionMethod) ) throw new skGatewayExecuteException('Error checking: Method ['.$sMethod.'] in class ['.$sClass.'] not found!');

        /* И он публичный */
        if(!$oCalledMethod->isPublic()) throw new skGatewayExecuteException('Error checking: Method ['.$sMethod.'] in class ['.$sClass.'] not accessible!');

        /* Пытаемся выполнить */
        $mResponse = $oCalledMethod->invokeArgs(new $sClass(), $aParameters);

        return $mResponse;
    }// func

    /**
     * Последовательно выполнить все методы по запросу
     * @throws GatewayException
     * @return bool
     */
    protected function doActions() {

        if(!count($this->aActions)) throw new GatewayException('Execute error: Queue of actions is empty!');

        foreach($this->aActions as $aAction) {
            try {

                $aResponse = array(
                    'response' => $this->executeAction($aAction),
                    'error'    => null,
                );
                $this->aResponse[] = $aResponse;

            } catch(skGatewayExecuteException $e) { /* До выполнения дело не дошло, отвалились по недопустимости вызова */

                $this->aResponse[] = array(
                    'response' => null,
                    'error'=> $e->getMessage(),
                );

            } catch(ReflectionException $e) { /* Пытались выполнить - что-то пошло не так */

                $this->aResponse[] = array(
                    'response' => null,
                    'error'=> $e->getMessage(),
                );
            }
        }// each method

        return true;
    }// func

    /**
     * @todo Реализовать обработку
     * Должен обрабатывать действия после загрузки файлов
     * @return bool
     */
    protected function doFiles() {


        if(!count($this->aFiles)) return false;

        return true;
    }// func

    /**
     * Собирает ответ с результатами обработки запроса. Шифорвание происходит в зависимости от режима
     * @param array $aHeader Массив заголовков, отправляемых в ответе
     * @param array $aResponse Массив ответов отработки запроса
     * @return string Возвращает строку ответа либо генерирует исключение типа GatewayException в случае ошибки
     * @throws GatewayException
     */
    protected function doResponse($aHeader, $aResponse) {

        $aPackage['Header'] = $aHeader;

        $aResponse = json_encode($aResponse);
        $aPackage['Header']['Certificate'] = $this->makeCertificate($this->aHeader['Client'], $aResponse);

        $aResponse = $this->encryptData($aResponse, $aPackage['Header']['crypted']);

        if(empty($aResponse)) throw new GatewayException('Response error: Assembled response is empty!');

        $aPackage['Data'] = $aResponse;

        return json_encode($aPackage);
    }// func

    /**
     *  Слушаем на предмет запросов, выполняем, собираем ответ, отдаем
     * @throws GatewayException
     * @return string
     */
    public function handler() {

        try {

            $sRequest = $this->getRequest();
            $this->parseRequest($sRequest);

            $this->doFiles();
            $this->doActions();

            $aHeader = array(
                'Status'  => 200,
                'Version' => $this->aHeader['Version'],
            );

            echo $this->doResponse($aHeader, $this->aResponse);

        } catch(GatewayException $e) {

            $aHeader = array(
                'Status'  => 500,
                'Version' => $this->aHeader['Version'],
            );

            $aErrorResponse = array('error' => $e->getMessage());
            echo $t = $this->doResponse($aHeader, $aErrorResponse);
        }


    }// func

}// class
