<?php
/**
 * Класс работы с тикетами с переопределенным хранилищем
 * @class skSessionTicket
 * @extends skTicket
 *
 * @author ArmiT, $Author: sapozhkov $
 * @version $Revision: 6 $
 * @date $Date: 2012-05-18 10:52:53 +0400 (Пт., 18 мая 2012) $
 * @project Skewer
 * @package kernel
 *
 * @todo Добавить очистку по количеству
 */

class skSessionTicket extends skTicket {

    /**
     * корневой ключ в сессии для хранения тикетов
     * @var string
     */
    private $sSessionKey = '_tickets';


    /**
     * Создает экземпляр тикета с хранилищем в сессии с возможностью указания ключа сессии для хранения данных
     * @param string $sKey Корневой ключ в сессии для хранения тикетов (Необяз.)
     * @return \skSessionTicket
     */
    public function __construct($sKey = '') {

        $sSettingsKey = skConfig::get('session.tickets.key');
        if(!empty($sSettingsKey))
            $this->sSessionKey = $sSettingsKey;


        if(!empty($sKey))
            $this->sSessionKey =  $sKey;

        return true;
    }// constructor

    /**
     * Добавляет данные тикета в хранилище
     * @param array $aData Тело создаваемого тикета
     * @return bool
     */
    protected function addTicketExecutor($aData) {

        $_SESSION[$this->sSessionKey][$aData['hash']] = $aData;
        return true;

    }// func

    /**
     * Очищает сессионные данные
     */
    public function clearSession() {
        unset( $_SESSION[$this->sSessionKey] );
    }

    /**
     * Удаляет тикет $sTicket из хранилища.
     * @param string $sTicket хеш-ключ тикета
     * @return bool|mysqli_result
     */
    protected function delTicketExecutor($sTicket){

        unSet($_SESSION[$this->sSessionKey][$sTicket]);
        return true;
    }// func

    /**
     * Возвращает тело тикета $sTicket из хранилища
     * @param string $sTicket хеш-ключ тикета
     * @return bool
     */
    protected function getTicketExecutor($sTicket){

        return (isSet($_SESSION[$this->sSessionKey][$sTicket]))? $_SESSION[$this->sSessionKey][$sTicket]: false;

    }// func

    /**
     * Осуществляет проверку тикета $sTicket на валидность по expire
     * @param string $sTicket хеш-ключ тикета
     * @return bool
     */
    protected function checkTicketExecutor($sTicket){

        return (isSet($_SESSION[$this->sSessionKey][$sTicket]))? $_SESSION[$this->sSessionKey][$sTicket]['expire']: false;

    }// func

    /**
     * Обновляет expire тикета
     * @param array $aData Данные тикета
     * @return bool|mysqli_result
     */
    protected function updExpireExecutor($aData){

        if(!isSet($_SESSION[$this->sSessionKey][$aData['hash']])) return false;

        $_SESSION[$this->sSessionKey][$aData['hash']] = $aData;

        return true;

    }// func

}// class
