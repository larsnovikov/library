<?php
require_once COREPATH.'libs/Twig/Autoloader.php';
/**
 * Класс-шаблонизатор Обертка для skTwig
 * @class skTwig
 * @project Skewer
 * @package kernel
 *
 * @Author ArmiT, $Author: sokol $
 * @version $Revision: 532 $
 * @date $Date: 2012-08-01 10:51:46 +0400 (Ср., 01 авг. 2012) $
 *
 * @source http://twig.kron0s.com/
 *
 * @dependig Twig_Autoloader
 *
 */

class skTwig {
    
    /**
     * Экземпляр шаблонизатора
     * @var null|\skTwig $instance
     */
    private static $instance    = NULL;
    /**
     * Массив путей в директориям шаблонов
     * @var array
     */
    private static $aTplPath    = array();
    /**
     * Путь к директории хранения кеша шаблонизатора
     * @var string
     */
    private static $sCache      = '';
    /**
     * Экземпляр skTwig File system class
     * @var null|\Twig_Loader_Filesystem
     */
    private static $oLoader     = NULL;
    /**
     * Экземпляр skTwig Environment class
     * @var null|\Twig_Environment
     */
    private static $oEnv        = NULL;
    /**
     * Хранилище данных, вставляемых в шаблон
     * @var array
     */
    private static $aStack      = array();
    /**
     * Флаг режима отладки
     * @var bool
     */
    private static $bDebugMode  = false;

    /**
     * Инициализирует шаблонизатор, устанавливает начальные настройки.
     */
    private function __construct() {

        Twig_Autoloader::register();

        //if(!count(self::$aTplPath)) return false;
 
        self::$oLoader = new Twig_Loader_Filesystem(self::$aTplPath);
        self::$oEnv = new Twig_Environment(self::$oLoader, array(
          'cache' => self::$sCache,
          'debug' => self::$bDebugMode,
          'autoescape' => false, 
        ));
        
    }// constructor

    /**
     * Запрещаем вызов извне.
     */
    private function __clone() {

    }// clone

    /**
     * Создает / возвращает экземпляр шаблонизатора
     * @static
     * @param array $aTplPath Массив путей к директориям шаблонов
     * @param string $sCache Путь к директроии хранения кеша
     * @param bool $bDebugMode Флаг режима отладки
     * @return null|\skTwig /skTwig
     */
    public static function Load($aTplPath, $sCache = '', $bDebugMode = false) {

        if(isset(self::$instance) and (self::$instance instanceof self)) {

			return self::$instance;
		} else {
            self::$aTplPath     = $aTplPath;
            self::$sCache       = (String)$sCache;
            self::$bDebugMode   = (bool)$bDebugMode;
            self::$instance     = new self();
			return self::$instance;
		}
    }// func

    /**
     * Включает режим отладки для шаблонизатора. В случае, если вызов происходит без предварительной инициализации,
     * то метод возвращает false либо true в случае успешной установки режима
     * @static
     * @return bool
     */
    public static function enableDebug() {

        if(!(self::$oEnv instanceof Twig_Environment)) return false;
        self::$bDebugMode = true;
        self::$oEnv->enableDebug();
        self::$oEnv->enableAutoReload();

        return false;
    }// func

    /**
     * Выключает режим отладки для шаблонизатора. В случае, если вызов происходит без предварительной инициализации,
     * то метод возвращает false либо true в случае успешной установки режима
     * @static
     * @return bool
     */
    public static function disableDebug() {

        if(!(self::$oEnv instanceof Twig_Environment)) return false;
        self::$bDebugMode = false;
        self::$oEnv->disableDebug();
        self::$oEnv->disableAutoReload();

        return false;
    }// func

    /**
     * Возвращает режим работы шаблонизатора. Debug/Production
     * @static
     * @return bool
     */
    public static function isDebugMode() {

        if(!(self::$oEnv instanceof Twig_Environment)) return false;
        return self::$oEnv->isDebug();

    }// func

    /**
     * Присваивает имя данным, по которому они будут доступны в шаблоне
     * @static
     * @param string $sName
     * @param mixed $uValue
     * @return bool
     */
    public static function assign($sName, $uValue) {

        if(empty($sName)) return false;
        self::$aStack[$sName] = $uValue;

        return true;
    }// func

    /**
     * Устанавливает путь к директории шаблонов
     * @static
     * @param array $aPaths
     * @param bool $bForceWrite
     * @return bool
     */
    public static function setPath($aPaths = array(), $bForceWrite=true) {
        
        if(!(self::$oLoader instanceof Twig_Loader_Filesystem)) return false;
        self::$oLoader->setPaths($aPaths);
        
        return true;
    }// func

    /**
     * Возвращает код шаблона после обработки шаблонизатором
     * @static
     * @param string $sTplName Ссылка на шаблон в рамках ранее указанной среды
     * @return bool|string
     */
    public static function render($sTplName) {

        $oTpl = self::$oEnv->loadTemplate($sTplName);
        $sOut = $oTpl->render(self::$aStack);
        self::$aStack = array();
        return $sOut;
    }// func
}// class
