<?php
/**
 * Класс работы с реестром (в БД)
 * @class skRegistry
 *
 * @author ArmiT, $Author: armit $
 * @version $Revision: 249 $
 * @date $Date: 2012-06-01 12:41:33 +0400 (Пт., 01 июня 2012) $
 * @project Skewer
 * @package kernel
 */ 
class skRegistry {

    /**
     * Возвращает массив данных из хранилища $sStorageName
     * @static
     * @param string $sStorageName Имя хранилища данных
     * @return bool|array
     */
    public static function getStorage($sStorageName) {
        global $odb;

        $sQuery = "
            SELECT
                `data`
            FROM
                `registry_storage`
            WHERE
                `name`=[name:s];";

        $mData = $odb->getValue($sQuery,'data', array('name' => $sStorageName));

        if(!$mData) return false;

        return json_decode($mData, true);
        //if(!$oResult->num_rows) return false;
        //$aData = $oResult->fetch_array(MYSQL_ASSOC);
    }// func

    /**
     * Сохраняет массив данных $aStorageData в хранилище $sStorageName
     * @static
     * @param string $sStorageName Имя хранилища
     * @param array $aStorageData Данные, сохраняемые в хранилище
     * @return bool|mixed
     */
    public static function saveStorage($sStorageName, $aStorageData = array()) {
        global $odb;

        $sQuery = "
            INSERT INTO
                `registry_storage`
            SET
                `name`=[name:s], `data`=[data:s]
            ON DUPLICATE KEY UPDATE
                `data`=[data:s];";

        $aData['name'] = $sStorageName;
        $aData['data'] = json_encode($aStorageData);

        $odb->query($sQuery, $aData);

        return $odb->insert_id;

    }// func

    /**
     * Удялет хранилище $sStorageName
     * @static
     * @param string $sStorageName Имя удаляемого хранилища
     * @return bool
     */
    public static function removeStorage($sStorageName) {
        global $odb;

        try {

            $sQuery = "
                DELETE FROM
                    `registry_storage`
                WHERE
                    `name`=[name:s];";

            $aData['name'] = $sStorageName;

            return ($odb->query($sQuery, $aData))? true: false;

        } catch(Exception $e) {

            return false;
        }
    }// func

    /**
     * Возвращает true, если хранилище с именем $sStorageName существует либо false в
     * противном случае.
     * @static
     * @param string $sStorageName имя хранилища
     * @return bool
     */
    public static function isExist($sStorageName) {

        global $odb;

        try {

            $sQuery = "
                SELECT
                  `name`
                FROM
                    `registry_storage`
                WHERE
                    `name`=[name:s]
                LIMIT 0, 1;";

            $aData['name'] = $sStorageName;

            $rRes = $odb->query($sQuery, $aData);

            return ($rRes->num_rows)? true: false;

        } catch(Exception $e) {

            return false;
        }

    }// func

}// class
