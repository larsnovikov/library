<?php
/**
 * Класс для транслитерации строк
 *
 * @class: skTranslit
 *
 * @Author: User, $Author: acat $
 * @version: $Revision: 1623 $
 * @date: $Date: 2013-01-30 13:13:55 +0400 (Ср, 30 янв 2013) $
 *
 */

class skTranslit {

    /**
     * Массив соответствия символов
     * @var array
     */
    protected static $aConvert = array(
        "'"=>"",
        "`"=>"",
        "а"=>"a","А"=>"A",
        "б"=>"b","Б"=>"B",
        "в"=>"v","В"=>"V",
        "г"=>"g","Г"=>"G",
        "д"=>"d","Д"=>"D",
        "е"=>"e","Е"=>"E",
        "ё"=>"e","Ё"=>"E",
        "ж"=>"zh","Ж"=>"Zh",
        "з"=>"z","З"=>"Z",
        "и"=>"i","И"=>"I",
        "й"=>"j","Й"=>"J",
        "к"=>"k","К"=>"K",
        "л"=>"l","Л"=>"L",
        "м"=>"m","М"=>"M",
        "н"=>"n","Н"=>"N",
        "о"=>"o","О"=>"O",
        "п"=>"p","П"=>"P",
        "р"=>"r","Р"=>"R",
        "с"=>"s","С"=>"S",
        "т"=>"t","Т"=>"T",
        "у"=>"u","У"=>"U",
        "ф"=>"f","Ф"=>"F",
        "х"=>"h","Х"=>"H",
        "ц"=>"c","Ц"=>"C",
        "ч"=>"ch","Ч"=>"Ch",
        "ш"=>"sh","Ш"=>"Sh",
        "щ"=>"sch","Щ"=>"Sch",
        "ъ"=>"","Ъ"=>"",
        "ы"=>"y","Ы"=>"Y",
        "ь"=>"","Ь"=>"",
        "э"=>"e","Э"=>"E",
        "ю"=>"yu","Ю"=>"Yu",
        "я"=>"ya","Я"=>"Ya",
    );

    /**
     * Шаблон для поиска запрещенных символов
     * @var string
     */
    protected static $sDeprecatedPattern = '/(\s)+|[^-_a-z0-9]+/Uis';

    /**
     * Разделитель
     * @var string
     */
    protected static $sDelimiter = '-';


    /**
     * Заменяет все неугодные символы
     * @static
     * @param string $sIn - входная строка
     * @param bool $bToLower
     * @return string
     */
    public static function change( $sIn, $bToLower=true ) {

        $sOut = iconv( "UTF-8", "UTF-8//IGNORE", strtr( $sIn, static::$aConvert ) );
        return $bToLower ? strtolower($sOut) : $sOut;

    }

    /**
     * Заменяет запрещенные символы на разделители
     * @static
     * @param string|array $mIn
     * @return string|array
     */
    public static function changeDeprecated( $mIn ) {

        return preg_replace(static::$sDeprecatedPattern, static::$sDelimiter, $mIn);

    }

    /**
     * Сливает несколько подряд идущих разделителей в один
     * @static
     * @param string|array $mIn
     * @return string|array
     */
    public static function mergeDelimiters( $mIn ) {

        return preg_replace('/['.static::$sDelimiter.']{2,}/s', static::$sDelimiter, $mIn);

    }

    /**
     * Возвращает текущее значение разделителя
     * @static
     * @return string
     */
    public static function getDelimiter() {
        return static::$sDelimiter;
    }
	
	/**
     * Устаналвивает $sDelimiter в качестве основного разделителя
     * @static
     * @param string $sDelimiter
     * @return mixed
     */
    public static function setDelimiter($sDelimiter) {
        return static::$sDelimiter = $sDelimiter;
    }
	
}
