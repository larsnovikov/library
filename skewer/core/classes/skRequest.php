<?php
/**
 * Менеджер POST(JSON) параметров.
 *
 * @class RequestParams
 * @project Skewer
 * @package kernel
 * @Author ArmiT, $Author: sapozhkov $
 * @version $Revision: 6 $
 * @date $Date: 2012-05-18 10:52:53 +0400 (Пт., 18 мая 2012) $
 * @static
 * @singleton
 */
class skRequest {

    /**
     * Набор POST параметров
     * @var array
     */
    static private $aParams = array();

    /**
     * Массив запрашиваемых на выполнение events
     * @var array
     */
    static private $aEvents = array();

    /**
     * true, если запрос пришел в виде JSON пакета
     * @var bool
     */
    static private $iIsJSONRequest = false;

    /**
     * Идентификатор сессии
     * @var string
     */
    static private $sSessionId = '';

    /**
     * Экземпляр текущего класса
     * @var null
     */
    static private  $oInstance = NULL;

    /**
     * Закрываем возможность вызвать clone
     */
    private function __clone() {}

    /**
     * Инициализация менеджера входящих параметров
     * @static
     * @return null
     */
    static public function init() {

        if(isset(self::$oInstance) and (self::$oInstance instanceof self)){

            return self::$oInstance;
        }else{

            self::$oInstance= new self();

            return self::$oInstance;
        }
    }// func

    /**
     * Создает экземпляр менеджера параметров. Обрабатывает POST параметры.
     * В случае обнаружения JSON Package - разбирает его.
     */
    private function __construct() {

        /* Обычные post параметры */

        if(count($_POST))
            foreach($_POST as $sKey=>$sData)
                self::$aParams['post'][$sKey] = $sData;


        if(count($_GET))
            foreach($_GET as $sKey=>$sData)
                self::$aParams['get'][$sKey] = $sData;


        $iHandler = fopen('php://input','r');
        $sJsonInput = fgets($iHandler);

        $aRequest = json_decode($sJsonInput, true);

        fclose($iHandler);

        if(!empty($aRequest)) {

            self::$iIsJSONRequest = true;

            if($aRequest['sessionId'] && !empty($aRequest['sessionId'])) self::$sSessionId = $aRequest['sessionId'];

            if(isSet($aRequest['data']) && count($aRequest['data']))
                foreach($aRequest['data'] as $aParam) {
                    self::$aParams['json'][$aParam['path']] = $aParam;
                }// each param

            /*Смотрим есть ли запросы на обработку событий*/
            if(isSet($aRequest['events']) && count($aRequest['events']))
                foreach($aRequest['events'] as $sEvent=>$aParams) {
                    self::$aEvents[$sEvent] = $aParams;
                }// each param

            if(isSet($aRequest['data'])) unSet($aRequest['data']);

            skProcessor::setJSONHeaders($aRequest);

        } else {

            /*Отработка посылки в POST*/
            $bHasSessionIdInPost = isSet(self::$aParams['post']['sessionId']) && self::$aParams['post']['sessionId'] && !empty(self::$aParams['post']['sessionId']);
            if($bHasSessionIdInPost) self::$sSessionId = self::$aParams['post']['sessionId'];

            $bHasPathInPost = isSet(self::$aParams['post']['path']) && !empty(self::$aParams['post']['path']);
            if($bHasPathInPost)self::$aParams['json'][self::$aParams['post']['path']] = self::$aParams['post'];

            // если есть обязательные параметры - взвести флаг
            if ( $bHasPathInPost and $bHasSessionIdInPost )
                self::$iIsJSONRequest = true;

        }

        return true;
    }// constructor

    /**
     * Возвращает значение целочисленного параметра по имени и пути меток до него
     * @param string $sLabelPath Путь меток вызова до него
     * @param string $sName Имя параметра
     * @param int $mDefault Значение, возвращаемое по-умолчанию
     * @return bool
     */
    static public function getInt($sLabelPath, $sName, &$mDefault = 0) {

        if(isset(self::$aParams['json'][$sLabelPath]['params'][$sName])) {
            $mDefault = (int)self::$aParams['json'][$sLabelPath]['params'][$sName];
            return true;
        }

        if(isset(self::$aParams['post'][$sName])) {
            $mDefault = (int)self::$aParams['post'][$sName];
            return true;
        }

        if(isset(self::$aParams['get'][$sName])) {
            $mDefault = (int)self::$aParams['get'][$sName];
            return true;
        }

        return false;
    }// func

    /**
     * Возвращает значение строкового параметра по имени и пути меток до него
     * @param $sLabelPath Путь меток вызова до него
     * @param $sName Имя параметра
     * @param string $mDefault Значение, возвращаемое по-умолчанию
     * @return bool
     */
    static public function getStr($sLabelPath, $sName, &$mDefault = '') {

        if(isset(self::$aParams['json'][$sLabelPath]['params'][$sName])) {
            $mDefault = (String)self::$aParams['json'][$sLabelPath]['params'][$sName];
            return true;
        }
        if(isset(self::$aParams['post'][$sName])) {
            $mDefault = self::$aParams['post'][$sName];
            return true;
        }

        if(isset(self::$aParams['get'][$sName])) {
            $mDefault = self::$aParams['get'][$sName];
            return true;
        }

        return false;
    }// func

    /**
     * Возвращает значение неопределенного параметра по имени и пути меток до него
     * @param $sLabelPath Путь меток вызова до него
     * @param $sName Имя параметра
     * @param string $mDefault Значение, возвращаемое по-умолчанию
     * @return bool
     */
    static public function get($sLabelPath, $sName, &$mDefault = '') {

        if(isset(self::$aParams['json'][$sLabelPath]['params'][$sName])) {
            $mDefault = self::$aParams['json'][$sLabelPath]['params'][$sName];
            return true;
        }

        if(isset(self::$aParams['post'][$sName])) {
            $mDefault = self::$aParams['post'][$sName];
            return true;
        }

        if(isset(self::$aParams['get'][$sName])) {
            $mDefault = self::$aParams['get'][$sName];
            return true;
        }

        return false;

    }// func

    /**
     * Установить в POST ключу $sLabel значение $mData
     * @static
     * @param $sLabel
     * @param $mData
     * @return array
     */
    static public function setPOSTParam($sLabel, $mData) {

        return self::$aParams['post'][$sLabel] = $mData;

    }// func

    /**
     * Возвращает true, если был получен JSON Package и false в противном случае.
     * @static
     * @return bool
     */
    static public function isJSONRequest() {

        return (bool)self::$iIsJSONRequest;
    }// func

    /**
     * Возвращает идентификатор сессии дерева процессов.
     * @static
     * @return string
     */
    static public function getSessionId() {

        return self::$sSessionId;
    }// func

    /**
     * Возвращает разобранный JSON Package в случае его нахождения либо false.
     * @static
     * @return bool|array
     */
    static public function getJSONPackage() {

        return (isSet(self::$aParams['json']))? self::$aParams['json']: false;
    }// func

    /**
     * Устанавливает новое значение параметра в JSON Package
     * @static
     * @param string $sLabelPath Путь по меткам вызова
     * @param string $sName Название параметра
     * @param mixed $mValue Значение параметра
     * @param bool $bOverlay Флаг жесткой либо мягкой вставки (перекрывать или нет существующиее значение)
     * @return bool
     */
    static public function set($sLabelPath, $sName, $mValue, $bOverlay = true) {

        if($bOverlay OR !isset(self::$aParams['json'][$sLabelPath]['params'][$sName])) {
            self::$aParams['json'][$sLabelPath]['params'][$sName] = $mValue;
            return true;
        }

        return false;
    }// func

    /**
     * Возвращает список событий из JSON Пакета
     * @static
     * @return bool|array
     */
    static public function getRequestEventsList() {
       return (count(self::$aEvents))? self::$aEvents: false;
    }// func

    /**
     * Возвращает событие из JSON пакета с именем $sEventName
     * @static
     * @param string $sEventName Имя события
     * @return bool|array
     */
    static public function getRequestEvent($sEventName) {
        return (isSet(self::$aEvents[$sEventName]))? self::$aEvents[$sEventName]: false;
    }// func

    /**
     * Очищает список событий из JSON пакета
     * @static
     * @return bool
     */
    static public function removeRequestEvents() {
        self::$aEvents = array();
        return true;
    }// func
}// class
