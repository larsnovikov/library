<?php
/**
 * Класс CRUD операций с моделями firewall_filters, firewall_actions, firewall_policy, firewall_reactions
 * @class skFirewallMapper
 *
 * @author ArmiT, $Author$
 * @version $Revision$
 * @date $Date$
 * @project Skewer
 * @package kernel
 */
class FirewallMapper extends skMapperPrototype {

    /**
     * Добавляет/обновляет фильтр
     * @param $aData массив данных согласно модели. Наличие ключа id говорит о том, что будет происходить обновление
     * @return bool|mixed
     */
    public function saveFilter($aData) {
        global $odb;

        if(!count($aData)) return false;

        $sQuery = "
            INSERT INTO `firewall_filters`
            SET
              ".(isSet($aData['filter_id']) && $aData['filter_id']? '`filter_id`=[filter_id:i],':'')."
              ".(isSet($aData['filter'])? '`filter`=[filter:s],': '')."
              `active`=[active:i]
            ON DUPLICATE KEY UPDATE
              ".(isSet($aData['filter'])? '`filter`=[filter:s],': '')."
              `active`=[active:i];";

        $odb->query($sQuery,$aData);

        return (isSet($aData['filter_id']) && (int)$aData['filter_id'])? $aData['filter_id']: $odb->insert_id;
    }// func

    /**
     * Удаляет фильтр по ID
     * @param $iFilterId
     * @return bool
     */
    public function removeFilter($iFilterId) {
        global $odb;

        $sQuery = "DELETE FROM `firewall_filters`  WHERE `filter_id`=[filter_id:id];";

        $odb->query($sQuery,array('filter_id' => (int)$iFilterId));

        return ($odb->errno)? false: true;
    }// func

    /**
     * Добавляет/обновляет действие
     * @param $aData массив данных согласно модели. Наличие ключа id говорит о том, что будет происходить обновление
     * @return bool|mixed
     */
    public function saveAction($aData) {
        global $odb;

        if(!count($aData)) return false;

        $sQuery = "
            INSERT INTO `firewall_actions`
            SET
              ".(isSet($aData['action_id']) && $aData['action_id'] ? '`action_id`=[action_id:i],': '')."
              `action`=[action:s],
              `title`=[title:s]
            ON DUPLICATE KEY UPDATE
              `action`=[action:s],
              `title`=[title:s];";

        $odb->query($sQuery, $aData);

        return (isSet($aData['action_id']) && (int)$aData['action_id'])? $aData['action_id']: $odb->insert_id;
    }// func

    /**
     * Удаляет действие по Id
     * @param $iActionId
     * @return bool
     */
    public function removeAction($iActionId) {
        global $odb;

        $sQuery = "DELETE FROM `firewall_actions`  WHERE `action_id`=[action_id:id];";

        $odb->query($sQuery,array('action_id' => (int)$iActionId));

        return ($odb->errno)? false: true;
    }// func

    /**
     * Сохраняет запись реакции
     * @param $aData набор данных согласно модели
     * @return bool
     */
    public function saveReaction($aData) {
        global $odb;

        if(!count($aData)) return false;
        if(!isSet($aData['filter_id']) OR !$iFilterId = $aData['filter_id']) return false;
        if(!isSet($aData['action_id']) OR !$aData['action_id']) return false;

        $sQuery = "
            SELECT
                MAX(priority) AS min_priority
            FROM
                `firewall_reactions`
            WHERE
                `filter_id`=[filter_id:i];";

        $iMinPriority = $odb->getValue($sQuery,'min_priority',array('filter_id' => $iFilterId));

        $aData['priority'] = ($iMinPriority)? ++$iMinPriority: 1;

        $sQuery = "
            INSERT INTO `firewall_reactions`
            SET
              `filter_id`=[filter_id:i],
              `action_id`=[action_id:i],
              `active`=[active:i],
              `priority`=[priority:i]
            ON DUPLICATE KEY UPDATE
              `active`=[active:i],
              `priority`=[priority:i];";

        $odb->query($sQuery,$aData);

        return ($odb->errno)? false: true;
    }// func

    /**
     * Удаляет реакцию для фильтра $iFilterId с действием $iActionId
     * @param $iFilterId
     * @param $iActionId
     * @return bool
     */
    public function removeReaction($iFilterId, $iActionId) {
        global $odb;

        $sQuery = "DELETE FROM `firewall_reactions`  WHERE `action_id`=[action_id:id] AND `filter_id`=[filter_id:i];";

        $odb->query($sQuery,array('action_id' => (int)$iActionId, 'filter_id' => (int)$iFilterId));

        return ($odb->errno)? false: true;
    }// func

    /**
     * Добавляет правило
     * @param $aData согласно модели
     * @return bool
     */
    public function savePolicyCondition($aData) {
        global $odb;

        if(!count($aData)) return false;
        if(!isSet($aData['filter_id']) OR !$aData['filter_id']) return false;
        if(!isSet($aData['policy_id']) OR !$iPolicyId = $aData['policy_id']) return false;

        $sExpire = (!isSet($aData['expire']) OR !is_null($aData['expire']))? '': ', `expire`=[expire:s]';

        $sQuery = "
            SELECT
                MAX(priority) AS min_priority
            FROM
                `firewall_policy`
            WHERE
                `policy_id`=[policy_id:i];";

        $iMinPriority = $odb->getValue($sQuery,'min_priority',array('policy_id' => $iPolicyId));

        $aData['priority'] = ($iMinPriority)? ++$iMinPriority: 1;

        $sQuery = "
            INSERT INTO `firewall_policy`
            SET
              `filter_id`=[filter_id:i],
              `policy_id`=[policy_id:i],
              `direction`=[direction:i],
              `priority`=[priority:i],
              `active`=[active:i]
              $sExpire
            ON DUPLICATE KEY UPDATE
              `direction`=[direction:i],
              `priority`=[priority:i],
              `active`=[active:i]
              $sExpire;";

        $odb->query($sQuery,$aData);

        return ($odb->errno)? false: true;

    }// func

    /**
     * Возвращает массив фильтров, привязанных к политике $iPolicyId
     * @param $iPolicyId
     * @return array|bool
     */
    public function getPolicyFilters($iPolicyId) {
        global $odb;

        $sQuery = "
            SELECT
                fp.*,
                ff.filter as `filter`
            FROM
                `firewall_policy` AS fp
            INNER JOIN `firewall_filters` AS ff ON ff.filter_id=fp.filter_id
            WHERE
                fp.`policy_id`=[policy_id:i] AND fp.`active`=1
            ORDER BY
                fp.`priority`
            ASC;";

        $rRes = $odb->query($sQuery, array('policy_id' => $iPolicyId));

        if($odb->errno OR !$rRes->num_rows) return false;

        $aItems = array();
        while($aItem = $rRes->fetch_assoc())
            $aItems[] = $aItem;

        return $aItems;
    }// func

    /**
     * Возвращает массив действий по $iFilterId
     * @param $iFilterId
     * @return array|bool
     */
    public function getActionsByFilter($iFilterId) {
        global $odb;

        $sQuery = "
                SELECT
                    fr.*,
                    fa.`action` as `action`,
                    fa.`title` as `title`
                FROM
                    `firewall_reactions` as fr
                INNER JOIN `firewall_actions` as fa ON fa.action_id = fr.action_id
                WHERE
                    fr.`filter_id`=[filter_id:i] AND
                    fr.`active`=1
                ORDER BY
                    fr.`priority`
                ASC;";

        $rRes = $odb->query($sQuery, array('filter_id' => $iFilterId));
        if($odb->errno OR !$rRes->num_rows) return false;

        $aItems = array();
        while($aItem = $rRes->fetch_assoc())
            $aItems[] = $aItem;

        return $aItems;

    }// func

}// class
