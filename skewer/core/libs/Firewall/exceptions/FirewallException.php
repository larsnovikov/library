<?php
/**
 * Заглушка исключений файервола
 * @class FirewallException
 *
 * @author ArmiT, $Author$
 * @version $Revision$
 * @date $Date$
 * @project JetBrains PhpStorm
 * @package kernel
 */
class FirewallException extends skException {

}// class
