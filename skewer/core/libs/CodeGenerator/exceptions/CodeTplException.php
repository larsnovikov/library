<?php
/**
 *
 * @class CodeTplException
 * @extends Exception
 *
 * @author ArmiT, $Author: armit $
 * @version $Revision: 290 $
 * @date $Date: 2012-06-14 14:24:09 +0400 (Чт., 14 июня 2012) $
 * @project JetBrains PhpStorm
 * @package kernel
 */
class CodeTplException extends Exception {
}// class
