<?php
/**
 *
 * @class HtaccessTpl
 *
 * @author ArmiT, $Author: armit $
 * @version $Revision: 290 $
 * @date $Date: 2012-06-14 14:24:09 +0400 (Чт., 14 июня 2012) $
 * @project Skewer
 * @package kernel
 */

class HtaccessTpl extends codeTplPrototype implements codeTplInterface {

    protected $sFilePath = '';

    protected $aData = array();

    protected $sTemplate = 'htaccess.twig';

    public function __construct($sFilePath, $aData, $sTemplate = false) {

        $this->sFilePath = $sFilePath;
        $this->aData     = $aData;
        $this->sTemplate = (!$sTemplate)? $this->sTemplate: $sTemplate;

    }// constructor

    public function make() {

        return $this->createFileByTpl($this->getSiteRootPath().$this->sFilePath, $this->sTemplate, $this->aData, false);

    }

    public function remove() {}


}
