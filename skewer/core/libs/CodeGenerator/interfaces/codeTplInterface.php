<?php
/**
 *
 * @class codeTplInterface
 *
 * @author ArmiT, $Author: armit $
 * @version $Revision: 290 $
 * @date $Date: 2012-06-14 14:24:09 +0400 (Чт., 14 июня 2012) $
 * @project Skewer
 * @package Libs
 */

interface codeTplInterface {

    public function make();

    public function remove();

}// iface
