<?php
/**
 *
 * @class codeTplPrototype
 *
 * @author ArmiT, $Author: armit $
 * @version $Revision: 290 $
 * @date $Date: 2012-06-14 14:24:09 +0400 (Чт., 14 июня 2012) $
 * @project JetBrains PhpStorm
 * @package kernel
 */

abstract class codeTplPrototype implements codeTplInterface {

    /**
     * Путь к корневой директории целевого хоста
     * @var string
     */
    protected $sSiteRootPath = '';

    /**
     * Путь к корневой директории с шаблонами файлов
     * @var string
     */
    protected $sTplRootPath = '';

    /**
     * Создает файл $sFileName путем генерации на основе шаблона $sTplName и данных $aData.
     * Если Указано $bIfNotExists то файл создается только в том случае, если он не был создан ранее.
     * В противном случае происходит перезапись.
     * @param string $sFileName Путь и имя создаваемого файла
     * @param $sTplName Имя файла шаблона. по-умолчанию шаблоны лежат и ищутся в директории
     *  <buildPath>/common/templates/
     * @param array $aData Массив данных для вставки в шаблон
     * @param bool $bIfNotExists Флаг, указывающий на необходимость создания файла только в том
     * случае, если он не существует.
     * @return bool Возвращает true, если создание прошло успешно и false в противном случае.
     * @throws CodeTplException
     */
    public function createFileByTpl($sFileName, $sTplName, $aData, $bIfNotExists = true) {

            if(!file_exists($this->sTplRootPath.$sTplName)) throw new CodeTplException('CodeTpl error: Template file ['.$this->sTplRootPath.$sTplName.'] not found!');

            /* Если файл уже существует и не нужна перезапись - выходим */
            if($bIfNotExists AND file_exists($sFileName)) throw new CodeTplException('CodeTpl error: Target file ['.$sFileName.'] not found!');

            skTwig::setPath(array($this->sTplRootPath));

            if(count($aData))
                foreach($aData as $sKey => $mValue)
                        skTwig::assign($sKey, $mValue);

            $sContent = skTwig::render($sTplName);

            if(!$sContent) throw new CodeTplException();

            $rF = fopen($sFileName, 'w+');
            if(!$rF) throw new CodeTplException();
            fwrite($rF, $sContent);
            fclose ($rF);

        return true;

    }// func

    public function createFile($sFileName, $sContent, $bIfNotExists = true) {}// func

    public function createDirectory($sDirectoryPath, $bIfNotExists = true) {}// func

    public function removeFile($sFileName) {}// func

    public function removeDirectory($sDirectoryName) {}// func

    public function add(codeTplPrototype $oChild) {

        $oChild->make();

    }// func



    /**
     * Устанавливает $sSiteRootPath в качестве пути к корневой директории целевого хоста
     * @param $sSiteRootPath Абсолютный путь
     */
    public function setSiteRootPath($sSiteRootPath) {
        $this->sSiteRootPath = $sSiteRootPath;
    }// func

    /**
     * Возвращает путь к корневой директории целевого хоста
     * @return string
     */
    public function getSiteRootPath() {
        return $this->sSiteRootPath;
    }

    /**
     * Устанавливает $sTplRootPath в качестве пути к корневой директориии с шаблонами генератора
     * @param string $sTplRootPath
     */
    public function setTplRootPath($sTplRootPath) {
        $this->sTplRootPath = $sTplRootPath;
    }

    /**
     * Возвращает путь к корневой директории с шаблонами для генератора
     * @return string
     */
    public function getTplRootPath() {
        return $this->sTplRootPath;
    }
    // func

}// class
