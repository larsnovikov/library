<?php
/**
 *
 * @class skAuthMapper
 *
 * @author ArmiT, $Author: sapozhkov $
 * @version $Revision: 6 $
 * @date $Date: 2012-05-18 10:52:53 +0400 (Пт., 18 мая 2012) $
 * @project JetBrains PhpStorm
 * @package kernel
 */
class skAuthMapper extends skMapperPrototype {



    



    /**
     * @var array Конфигурация полей таблицы Personal_policy_func
     */
    protected static $aPersonalPolicyFuncParameters = array(
        'user_id' => 'i:hide:Идентификатор пользователя',
        'module_name' => 's:str:Имя модуля',
        'param_name' => 's:str:Имя параметра модуля',
        'value' => 's:str:Значение параметра'
    );


    /**
     * @var array Конфигурация полей таблицы Personal_policy_data
     */
    protected static $aPersonalPolicyDataParameters = array(
        'user_id' => 'i:hide:Идентификатор пользователя',
        'start_section' => 'i:hide:Стартовый раздел после авторизации',
        'read_disable' => 's:str:Список разделов, начиная с которых вглубь дерева разделов запрещается доступ',
        'read_enable' => 's:str:Список разделов, начиная с которых вглубь дерева разрешается доступ только на чтение',
        'write_enable' => 's:str:Список разделов, начиная с которых вглубь дерева разрешается полный доступ',
        'cache_read' => 's:check:Список всех разделов доступных на чтение',
        'cache_write' => 's:str:Список всех разделов с полным доступом',
        'cache_func' => 's:str:Кеш функционального уровня'
    );

    public static function saveUser( $aInputData ){

        self::setCurrentTable(self::$sUsersTable);
        self::setParametersList(self::$aUsersParameters);

        return self::saveItem($aInputData);

    }

    public static function getUser($aFilter){

        self::setCurrentTable(self::$sUsersTable);
        self::setParametersList(self::$aUsersParameters);

        $aUser = self::getItems($aFilter);

        return ( sizeof($aUser) )? $aUser['items'][0]: false;
    }

    public static function getGroupPolicy($aFilter){

        self::setCurrentTable(self::$sUsersTable);
        self::setParametersList(self::$aUsersParameters);

        $aUser = self::getItems($aFilter);

        return ( sizeof($aUser) )? $aUser['items'][0]: false;
    }

    public static function getGroupPolicyFunc(){}

    public static function getPersonalPolicyFunc(){}

    public static function getGroupPolicyData(){}

    public static function getPersonalPolicyData(){}
    
}// class
