<?php

/**
 * @class skLoggerMapper
 * @extends skModule
 * @project Skewer
 * @package kernel
 *
 * @author Andy Mitrich, $Author: acat $
 * @version $Revision: 1672 $
 * @date 27.01.12 11:24 $
 *
 */

class skLoggerMapper extends skMapperPrototype {

    /* Common Part */

    /**
     * Имя таблицы, с которой работает маппер
     * @var string
     */
    protected static $sCurrentTable = 'log';

    /**
     * @static Метод возвращает имя рабочей таблицы
     * @return string
     */
    public static function getCurrentTable() {

        return self::$sCurrentTable;
    }

    /**
     * @static Метод возвращает список полей рабочей таблицы
     * @return array
     */
    public static function getParametersList() {

        return self::$aParametersList;
    }

    /**
     * Конфигурация полей таблицы
     * @var array
     */
    protected static $aParametersList = array(
        'id' => 'i:show:Идентификатор записи',
        'event_time' => 's:show:Дата и время',
        'event_type' => 'i:show:Уровень события',
        'event_title' => 's:show:Уровень события',
        'log_type' => 'i:show:Журнал событий',
        'log_title' => 's:show:Журнал событий',
        'title' => 's:show:Название события',
        'module' => 's:show:Название модуля',
        'module_title' => 's:show:Название модуля',
        'initiator' => 'i:show:ID пользователя, 0 - если система',
        'ip' => 's:show:IP пользователя',
        'proxy_ip' => 's:show:IP пользователя (proxy)',
        'external_id' => 's:show:ID записи в общем хранилище + маркер была ли запись экспортирована',
        'description' => 's:show:Подробное описание события'
    );

    protected static function getAddParamList(){

        return array(
            'id' => array(
                'view' => 'hide',
                'listColumns' => array(
                    'hidden' => true
                )
            ),
            'title' => array(
                'listColumns' => array( 'flex' => 2 )
            ),
            'log_title' => array(
                'listColumns' => array( 'width' => 150 )
            ),
            'module_title' => array(
                'listColumns' => array( 'width' => 150 )
            ),
            'description' => array(
                'labelAlign' => 'top'
            )
        );
    }// function getAddParamList()

    public static function clearLog($aKeys = array()){

        global $odb;

        if ( !sizeof($aKeys) ){
            $sQuery = "
                TRUNCATE TABLE `".self::$sCurrentTable."`";
        }
        else
            $sQuery = "
                DELETE
                FROM
                    `log`;";

        return $odb->query($sQuery);
    }


    public static function getItemsList($aInputData){

        global $odb;

        $sTableName = self::getCurrentTable();

        $aInputData['table'] = $sTableName;

        $aFilter = array();


        if ( isset($aInputData['limit']) and $aInputData['limit'] ) $aFilter['limit'] = $aInputData['limit'];
        if ( isset($aInputData['order']) and $aInputData['order'] ) $aFilter['order'] = $aInputData['order'];


        $aWhereCondition = array();

        if ( isset($aInputData['login']) ){
            if( !$aInputData['login'] )
                $aWhereCondition[] = '!`initiator`';
            else
                $aWhereCondition[] = '`login` = [login:s]';
        }

        if ( isset($aInputData['module']) )
            $aWhereCondition[] = '`module` LIKE [module:s]';//$aFilter['where_condition']['module'] = array('sign'=>'LIKE', 'value'=>$aInputData['module']);

        if ( isset($aInputData['event_type']) )
            $aWhereCondition[] = '`event_type` = [event_type:s]';//$aFilter['where_condition']['event_type'] = array('sign'=>'=', 'value'=>$aInputData['level']);

        if ( isset($aInputData['log_type']) )
            $aWhereCondition[] = '`log_type` = [log_type:s]';//$aFilter['where_condition']['log_type'] = array('sign'=>'=', 'value'=>$aInputData['log']);

        if ( isset($aInputData['event_time']) and $aInputData['event_time'] )
            if( $aInputData['event_time']['sign'] == 'BETWEEN' ){
                $aWhereCondition[] = '`event_time` BETWEEN [event_time_from'.':s] AND [event_time_to'.':s]';// $aFilter['where_condition']['event_time'] = $aInputData['event_time'];
                $aInputData['event_time_from'] = $aInputData['event_time']['value'][0];
                $aInputData['event_time_to'] = $aInputData['event_time']['value'][1];
            }else{
                $aWhereCondition[] = '`event_time` '.$aInputData['event_time']['sign'].' [event_time_:s]';// $aFilter['where_condition']['event_time'] = $aInputData['event_time'];
                $aInputData['event_time_'] = $aInputData['event_time']['value'];
            }
            
        if (!CurrentAdmin::isSystemMode()){
            $aWhereCondition[] = "`group_policy`.alias != 'sysadmin'";
        }

        $sWhereCondition = '';
        if ( sizeof($aWhereCondition) ) $sWhereCondition = "WHERE ".implode(' AND ', $aWhereCondition);
        //return self::getItems($aFilter);

        $sOrderCondition = '';

        if ( isset($aInputData['order']) ){

            $sOrderCondition = " ORDER BY {$aInputData['order']['field']} {$aInputData['order']['way']} ";
        }

        $sLimitCondition = '';

        // Обрабатываем ограничение по количеству выбираемых элементов
        if ( isset($aInputData['limit']) && $aInputData['limit'] ){

            $aInputData['start_limit'] = $aInputData['limit']['start'];
            $aInputData['count_limit'] = $aInputData['limit']['count'];
            $sLimitCondition = " LIMIT [start_limit:i], [count_limit:i]";
        }

        $sQuery = "
            SELECT
            SQL_CALC_FOUND_ROWS
            `log`.*,
            `users`.login as login,
            `group_policy`.id as 'gP'
            FROM
            `[table:q]`
            LEFT JOIN
            `users` ON `users`.id=`[table:q]`.initiator
            LEFT JOIN
            `group_policy` ON `group_policy`.id=`users`.group_policy_id
            $sWhereCondition
            $sOrderCondition
            $sLimitCondition;";        
        $rResult = $odb->query($sQuery, $aInputData);

        if ( !$rResult ) return false;

        // Собирается результирующий массив
        $aItems = array('items'=>array());

        while( $aRow = $rResult->fetch_array(MYSQLI_ASSOC) ){
            $aItems['items'][] = $aRow;
        }

        // Получаем общее число записей из таблицы
        $sCountQuery = "SELECT FOUND_ROWS() as `rows`;";
        $aCount = $odb->getRow($sCountQuery);
        $aItems['count'] = $aCount['rows'];

        return $aItems;


    }

} //class