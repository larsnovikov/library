<?php
/**
 *
 * @class skTasksMapper
 *
 * @author ArmiT, $Author: sapozhkov $
 * @version $Revision: 6 $
 * @date $Date: 2012-05-18 10:52:53 +0400 (Пт., 18 мая 2012) $
 * @project Skewer
 * @package kernel
 */
class skTasksMapper extends skMapperPrototype {

    /**
     * @var string Имя таблицы, с которой работает маппер
     */
    protected  static $sCurrentTable = 'task_queue';

    /**
     * @var array Конфигурация полей таблицы
     */
    protected static $aParametersList = array(
        'id' => 'i',
        'title' => 's',
        'command' => 's',
        'priority' => 'i',
        'resource_use' => 'i',
        'target_area' => 'i',
        'start_time' => 's',
        'mutex' => 'i',
        'status' => 'i',
        'exec_counter' => 'i',
    );


    public function getMutex() {

        $sQuery = '
            INSERT INTO
              `task_mutex_counter`
            SET
              `mutex`=null;';
        $this->odb->query($sQuery);
        return $this->odb->insert_id;
    }// func


    public function addTask($aData) {


        self::setCurrentTable(self::$sCurrentTable);
        self::setParametersList(self::$aParametersList);

        foreach( $aData as $sFieldName => $mValue )
            if($aField = self::getParameter($sFieldName))
                $aFields[] = "`$sFieldName`=[{$aField['name']}:{$aField['type']}]";

        $sFields = implode(',', $aFields);

        $sQuery = '
            INSERT INTO
              `task_queue`
            SET
              id=[id:i],
              title=[title:s],
              command=[command:s],
              priority=[priority:i],
              target_area=[target_area:i],
              mutex=[mutex:i],
              status=[status:i],
              status=[status:i],
              exec_counter=0
            ON DUPLICATE KEY UPDATE
              $sFields
            ;';

        //$this->odb->query($sQuery,);

    }// func
}// class
