<?php
/**
 * класс для внутренних логических ошибок выполнения модуля (нет id, ...)
 */
class ModuleAdminErrorException extends Exception{}
