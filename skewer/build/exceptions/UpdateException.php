<?php
/**
 * Обработчик исключений для модулей обновления и установки
 * @class UpdateExeption
 * @extends skException
 * @implements skExeptionInterface
 *
 * @author ArmiT, $Author: sys $
 * @version $Revision: 691 $
 * @date $Date: 2012-08-24 17:53:07 +0400 (Пт., 24 авг. 2012) $
 * @project Skewer
 * @package Build
 */
class UpdateException extends skException implements skExeptionInterface {

    public function __construct ($message = "", $code = 0, Exception $previous = null) {

        // залогить сразу
        skLogger::dump( sprintf('UpdateException: %s (%s:%d)',
            $this->getMessage(),
            $this->getFile(),
            $this->getLine()
        ) );

        parent::__construct($message, $code, $previous);

    }

}// class
