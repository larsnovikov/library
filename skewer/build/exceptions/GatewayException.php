<?php
/**
 * Обработчик исключений для Web-сервисов площадок с отправкой ошибок на клиент
 * @class GatewayException
 * @extends skException
 * @implements skExeptionInterface
 *
 * @author ArmiT, $Author$
 * @version $Revision$
 * @date $Date$
 * @project Skewer
 * @package Kernel
 */
class GatewayException extends skException implements skExeptionInterface {

}// class