<?php
/* main */
$aConfig['name']     = 'MainBannerAdm';
$aConfig['title']    = 'Баннер в шапке';
$aConfig['version']  = '1.000a';
$aConfig['description']  = 'Админ-интерфейс управления баннером в шапке';
$aConfig['revision'] = '0001';
$aConfig['layer']     = 'adm';

return $aConfig;