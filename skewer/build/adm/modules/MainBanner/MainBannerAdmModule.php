<?php
/**
 * @class MainBannerAdmModule
 * @extends AdminToolTabModulePrototype
 * @project Skewer
 * @package kernel
 *
 * @author kolesnikov, $Author: $
 * @version $Revision: $
 * @date $Date: $
 */
class MainBannerAdmModule extends AdminToolTabModulePrototype {

    /** @var string - Имя модуля */
    protected $sTabName = 'Баннер в шапке';

    protected $iCurrentBanner = 0;


    protected function preExecute() {

        CurrentAdmin::testControlPanelAccess();

        //$this->iCurrentBanner = $this->getInt('banner_id');
    }

    /**
     * Первичное состояние
     */
    protected function actionInit() {

        // вывод списка
        $this->actionBannerList();

    }

    //---- BANNER -----------------------------------------------------------

    /**
     * Список баннеров
     */
    protected function actionBannerList() {

        $this->iCurrentBanner = 0;

        $oList = new ExtList();

        // формирование модели данных
        $aModel = MainBanner2BannersAdmApi::getListModel(MainBanner2BannersAdmApi::getListFields());

        $oList->setFields( $aModel );

        // набор редактируемых в списке полей
        $oList->setEditableFields( MainBanner2BannersAdmApi::getEditableListFields(), 'saveBanner' );

        // добавление набора данных
        $aItems = MainBanner2BannersAdmApi::getListItems( $this->getModuleWebDir() );
        $oList->setValues( $aItems['items'] );
        $oList->setTotal( $aItems['count'] );

        // кнопки в списке
        $oList->addRowBtnUpdate('editBannerForm');
        $oList->addRowBtnDelete('delBanner');

        // кнопки в панели
        $oList->addDockedItem(array(
            'text' => 'Добавить баннер',
            'state' => 'init',
            'action' => 'editBannerForm',
            'iconCls' => 'icon-add',
        ));
        $oList->addDockedItem(array(
            'text' => 'Настройки показа',
            'state' => 'init',
            'action' => 'toolsForm',
            'iconCls' => 'icon-edit',
        ));

        // вывод данных в интерфейс
        $this->setExtInterface( $oList );
    }

    /**
     * Форма редактирования баннера
     */
    protected function actionEditBannerForm() {

        $aData = $this->get('data');
        $iItemId = isSet($aData['id']) ? $aData['id'] : false;

        $oForm = new ExtForm();

        /* установить набор элементов формы */
        $aModel = MainBanner2BannersAdmApi::getBannerForm();

        $aItems = array();
        if( $iItemId )
            $aItems = MainBanner2BannersAdmMapper::getItem($iItemId);
        else {
            foreach($aModel as $sKey=>$aVal)
                if( isSet($aVal['default']) )
                    $aItems[$sKey] = $aVal['default'];
        }

        $oForm->setFields($aModel);
        $oForm->setValues($aItems);

        $oForm->addBntSave( 'saveBanner' );
        if( $iItemId )
            $oForm->addDockedItem(array(
                'text' => 'Редактировать слайды',
                'state' => 'init',
                'action' => 'slideList',
                'iconCls' => 'icon-edit',
            ));
        $oForm->addBntCancel('bannerList');

        $this->iCurrentBanner = $iItemId;

        $this->setExtInterface($oForm);

        return psComplete;

    }

    /**
     * Добавление/обновление баннера
     */
    protected function actionSaveBanner() {

        $aData = $this->get('data');

        try{

            MainBanner2BannersAdmMapper::saveItem($aData);

        } catch ( Exception $e ) {
            $this->addError($e->getMessage());
        }

        $this->actionBannerList();
    }

    /**
     * Удаление баннера
     */
    protected function actionDelBanner() {

        $aData = $this->get('data');

        $iItemId = iSset($aData['id']) ? $aData['id'] : false;

        if( $iItemId )
            MainBanner2BannersAdmMapper::delItem($iItemId);

        $this->actionBannerList();

    }

    //---- SLIDE -----------------------------------------------------------

    /**
     * Список слайдов для баннера
     */
    protected function actionSlideList() {

        if( !$this->iCurrentBanner )
            throw new Exception('Ошибка! Баннер не найден');

        $oList = new ExtList();

        $oList->enableDragAndDrop( 'sortSlideList' );

        // формирование модели данных
        $aModel = MainBanner2SlidesAdmApi::getListModel(MainBanner2SlidesAdmApi::getListFields());

        $oList->setFields( $aModel );

        // набор редактируемых в списке полей
        $oList->setEditableFields( MainBanner2SlidesAdmApi::getEditableListFields(), 'saveSlide' );

        // добавление набора данных
        $aItems = MainBanner2SlidesAdmApi::getListItems($this->iCurrentBanner);
        $oList->setValues( $aItems['items'] );
        $oList->setTotal( $aItems['count'] );

        // кнопки в списке
        $oList->addRowBtnUpdate('editSlideForm');
        $oList->addRowBtnDelete('delSlide');

        // кнопки в панели
        $oList->addDockedItem(array(
            'text' => 'Добавить слайд',
            'state' => 'init',
            'action' => 'editSlideForm',
            'iconCls' => 'icon-add',
        ));
        $oList->addBntCancel('bannerList');


        // вывод данных в интерфейс
        $this->setExtInterface( $oList );
    }

    protected function actionSortSlideList() {

        $aData = $this->get( 'data' );
        $aDropData = $this->get( 'dropData' );
        $sPosition = $this->get( 'position' );

        if( !isSet($aData['id']) || !$aData['id'] ||
            !isSet($aDropData['id']) || !$aDropData['id'] || !$sPosition )
            $this->addError('Ошибка! Неверно заданы параметры сортировки');

        $aData = MainBanner2SlidesAdmMapper::getItem($aData['id']);
        $aDropData = MainBanner2SlidesAdmMapper::getItem($aDropData['id']);

        if( !MainBanner2SlidesAdmApi::sortSlides($aData, $aDropData, $sPosition) )
            $this->addError('Ошибка! Неверно заданы параметры сортировки');
    }

    /**
     * Форма редактиорвания слайда
     * @return int
     */
    protected function actionEditSlideForm() {

        $aData = $this->get('data');
        $iItemId = isSet($aData['id']) ? $aData['id'] : false;
        $sNoImage = $this->getModuleWebDir().'/img/noimg.png';
        $sUploadImage = isSet($aData['upload_image']) ? $aData['upload_image'] : (isSet($aData['img']) ? $aData['img'] : $sNoImage);

        $oForm = new ExtForm();

        $this->addJSFile( '/skewer_build/libs/jquery/jquery.js' );
        $this->addJSFile( '/skewer_build/libs/jquery/jquery-ui-1.9.1.custom.min.js' );
        //$this->addJSFile( '/skewer_build/libs/CKEditor/ckeditor.js' );

        $aItems = array();

        // получаем модель
        $aModel = MainBanner2SlidesAdmApi::getBannerForm();

        // получаем список значений
        if( $iItemId )
            $aItems = MainBanner2SlidesAdmMapper::getItem($iItemId);
        else {
            foreach($aModel as $sKey=>$aVal)
                if( isSet($aVal['default']) )
                    $aItems[$sKey] = $aVal['default'];
        }

        // добавляем пробел, если поле пустое для возможности редактированть
        for( $i=1; $i<5; $i++ )
            if( !isset($aItems['text'.$i]) || !$aItems['text'.$i] )
                $aItems['text'.$i] = "&nbsp;";

        if( $sUploadImage )
            $aItems['img'] = $sUploadImage;

        /* формируем модель формы */
        $aImgTab = array(
            'name'  => 'canv',
            'title' => 'Настройка слайда',
            'value' => $aItems
        );

        $this->addCssFile('SlideShower.css');

        $aCanvas['canv'] = $oForm->getSpecificItemInitArray('SlideShower', $aImgTab );

        $aModel = array_merge($aCanvas,$aModel);

        $oForm->setFields($aModel);

        /* формируем контент формы */
        $oForm->setValues($aItems);

        /* элементы управления */
        $this->addLibClass( 'SlideLoadImage' );

        $oForm->addBntSave( 'saveSlide' );
        $oForm->addButton( ExtDockedByUserFile::create('Загрузить фон','SlideLoadImage')
                ->setIconCls( ExtDocked::iconAdd )
                ->setAddParam('slideId',$iItemId)
        );
        $oForm->addBntCancel('slideList');

        /* save */
        $this->setExtInterface($oForm);

        return psComplete;
    }

    /**
     * Обработка загруженного изображения для фона слайда
     * @return mixed
     * @throws Exception
     */
    protected function actionUploadImage() {

        $iItemId = (int) $this->get( 'slideId' );

        $sSourceFN = MainBanner2SlidesAdmApi::uploadFile();

        $this->set('data',array('id'=>$iItemId,'upload_image'=>$sSourceFN));
        $this->actionEditSlideForm();
    }

    protected function actionSaveSlide() {

        $aData = $this->get('data');

        try{

            if( !$this->iCurrentBanner )
                throw new Exception('Ошибка! Баннер не найден');

            $aData['banner_id'] = $this->iCurrentBanner;

            if( isSet($aData['id']) && isset($aData['position']) && $aData['position'] ) {
                unset($aData['position']); // удаляем из полей на добавление, тк может быть недействительным
            } else  {  // если новый элемент или позиция почемуто не задана
                $aData['position'] = MainBanner2SlidesAdmMapper::getMaxOrder($aData['banner_id'],$aData['id'])+1;
            }

            for( $i = 1; $i < 5; $i++ ) {
                if ( !$aData['text'.$i] ) $aData['text'.$i] = '';
                if ( !$aData['text'.$i.'_v'] ) $aData['text'.$i.'_v'] = 0;
                if ( !$aData['text'.$i.'_h'] ) $aData['text'.$i.'_h'] = 0;
            }

            MainBanner2SlidesAdmMapper::saveItem($aData);

        } catch ( Exception $e ) {
            $this->addError($e->getMessage());
        }

        $this->actionSlideList();
    }

    protected function actionDelSlide() {

        $aData = $this->get('data');

        $iItemId = iSset($aData['id']) ? $aData['id'] : false;

        if( $iItemId )
            MainBanner2SlidesAdmMapper::delItem($iItemId);

        $this->actionSlideList();
    }

    //---- TOOLS -----------------------------------------------------------

    protected function actionToolsForm() {

        $oForm = new ExtForm();

        /* формируем модель формы */
        $aModel = MainBanner2BannersAdmApi::getToolModel();

        $oForm->setFields($aModel);

        /* формируем контент формы */
        $aItems = MainBanner2ToolsAdmMapper::getToolsList();

        $oForm->setValues($aItems);


        /* кнопки управления */
        $oForm->addBntSave( 'saveTools' );
        $oForm->addBntCancel('bannerList');

        /* save */
        $this->setExtInterface($oForm);

        return psComplete;

    }

    protected function actionSaveTools() {

        $aData = $this->get('data');

        MainBanner2ToolsAdmMapper::saveTools($aData);

        $this->actionToolsForm();
    }



}
