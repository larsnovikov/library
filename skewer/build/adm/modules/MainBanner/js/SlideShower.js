/**
 * Библиотека для отображения слайда для баннера
 */
Ext.define('Ext.adm.SlideShower',{

    extend: 'Ext.form.field.Base',

    border: 0,
    padding: 5,
    minWidth: 1000,
    width: 1280,

    fieldSubTpl:  [
        '<div class="b-banner">' +
        '<div class="banner__item">' +
            '<div id="ban_dd_lab_1" class="banner__text1" style="' +
                '<tpl if="text1_h != 0">left: {text1_h}px;</tpl>' +
                '<tpl if="text1_v != 0">top: {text1_v}px;</tpl>' +
            '"><div id="ban_edit_lab_1" contenteditable="true" class="builder-show-field">{text1}</div><span class="js_sdd ban-dd">text1</span></div>' +
            '<div id="ban_dd_lab_2" class="banner__text2" style="' +
                '<tpl if="text2_h != 0">left: {text2_h}px;</tpl>' +
                '<tpl if="text2_v != 0">top: {text2_v}px;</tpl>' +
            '"><div id="ban_edit_lab_2" contenteditable="true" class="builder-show-field">{text2}</div><span class="js_sdd ban-dd">text2</span></div>' +
            '<div id="ban_dd_lab_3" class="banner__text3" style="' +
                '<tpl if="text3_h != 0">right: {text3_h}px;</tpl>' +
                '<tpl if="text3_v != 0">top: {text3_v}px;</tpl>' +
            '"><div id="ban_edit_lab_3" contenteditable="true" class="builder-show-field">{text3}</div><span class="js_sdd ban-dd">text3</span></div>' +
            '<div id="ban_dd_lab_4" class="banner__text4" style="' +
                '<tpl if="text4_h != 0">right: {text4_h}px;</tpl>' +
                '<tpl if="text4_v != 0">top: {text4_v}px;</tpl>' +
            '"><div id="ban_edit_lab_4" contenteditable="true" class="builder-show-field">{text4}</div><span class="js_sdd ban-dd">text4</span></div>' +
        '</div>'+
        '<img src="{img}" alt="{title}" {addAttr}></div>'
    ],
    listeners: {
        // кроп интерфейс вызывается только при необходимости
        afterrender: function( me ) {

            $("[id^=ban_dd_lab_]").draggable({
                containment:".b-banner",
                handle: ".js_sdd",
                stop: function(){
                    var cur_id = this.id.substr(11);
                    sk.removeCKEditorOnPlace( 'ban_edit_lab_' + cur_id );
                    sk.initCKEditorOnPlace( 'ban_edit_lab_' + cur_id );
                }
            });

            for( var i=1; i<5; i++ )
                sk.initCKEditorOnPlace( 'ban_edit_lab_' + i );

        },
        beforedestroy: function() {

            for( var i=1; i<5; i++ )
                sk.removeCKEditorOnPlace( 'ban_edit_lab_' + i );

            $("[id^=ban_dd_lab_]").remove();

        }
    },

    execute: function() {
        if( $(".b-banner").children("img").size() )
            $(this.bodyEl.dom).width($(".b-banner").children("img").width() ? $(".b-banner").children("img").width() : 1000);
    },


    initComponent: function() {

        var me = this;

        // если есть спец параметр - заменить стандартный
        if ( me.value )
            me.subTplData = me.value;

        me.callParent();

    },

//    beforeDestroy: function(){
//
//
//
//        //CKEDITOR.inline( 'ban_edit_lab_1' );
//
//    },

    getSubmitData: function() {

        var data = {};
        var image_w = $(".b-banner img").width();

        for( var i=1; i<5; i++ ) {
            data['text' + i] = $('#ban_edit_lab_' + i).html();
            data['text' + i + '_v'] = parseInt($('#ban_dd_lab_' + i).css('top'));
            data['text' + i + '_h'] = parseInt($('#ban_dd_lab_' + i).css('left'));

            if( (i > 2) && image_w )
                data['text' + i + '_h'] = image_w - $('#ban_dd_lab_' + i).width() - data['text' + i + '_h'];

        }

        return data;
    }


});