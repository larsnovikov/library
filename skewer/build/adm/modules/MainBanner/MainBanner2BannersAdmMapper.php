<?php
/**
 * Модель для сущности - баннер
 * @class MainBanner2BannersAdmMapper
 *
 * @author kolesnikov, $Author: $
 * @version $Revision: $
 * @date $Date: $
 * @project Skewer
 */
class MainBanner2BannersAdmMapper extends skMapperPrototype {

    /**
     * Таблица модели
     * @var string
     */
    protected  static $sCurrentTable = 'banners_main';

    /**
     * Модель данных
     * @var array
     */
    protected static $aParametersList = array(

        'id'        => 'i:hide:Идентификатор баннера',
        'title'     => 's:str:Название баннера:Новый баннер',
        'section'   => 'i:select:Разделы для показа',
        'active'    => 'i:check:Активность:1',
        'on_include'=> 'i:check:Показывать на внутренних страницах:0',
        'bullet'    => 'i:check:Показывать буллет:0',
        'scroll'    => 'i:check:Показывать прокрутку вперед/назад:0',

    );

    /**
     * Расширенные параметры для формы
     * @return array
     */
    protected static function getAddParamList(){

        return array(
            'section' => ExtForm::getDesc4SelectFromArray(self::getSectionTitle())
        );
    }

    /**
     * Получение дерева разделов в виде списка
     * @return array
     */
    public static function getSectionTitle(){

        $oTree = new Tree();
        $aSections = $oTree->getSectionsInLine(skConfig::get('section.sectionRoot'));
        unset($oTree);
        $aResult = array();

        foreach( $aSections as $aSection )
            $aResult[$aSection['id']] = $aSection['title'];

        return $aResult;
    }


    /**
     * Получение списка баннеров, доступных в разделе $iSectionId
     * @param $iSectionId
     * @return array
     */
    public static function getBannersForSection( $iSectionId ) {

        global $odb;

        $oTree = new Tree();

        $aTreeSection = $oTree->getSectionParents( $iSectionId );

        $aData = array(
            'table_name' => static::getCurrentTable(),
            'section' => $iSectionId,
            'section_path' => $aTreeSection ? implode(", ",$aTreeSection) : ''
        );

        $sQuery = '
            SELECT `id`, `title`, `section`,`bullet`,`scroll`
            FROM `[table_name:q]`
            WHERE `active` AND (
                `section`=[section:i] OR
                (`on_include` AND (`section` IN ([section_path:q])))
            );
        ';

        $rResult = $odb->query($sQuery, $aData);

        $aItems = array();

        while( $aRow = $rResult->fetch_assoc() ){

            $aItems[] = $aRow;
        }

        return $aItems;
    }

    /**
     * Получение списка баннеров с доп. полями
     * @param string $sDir
     * @return array
     */
    public static function getBannerList( $sDir ) {

        global $odb;

        $aData = array(
            'tab_banner' => static::getCurrentTable(),
            'tab_slide' => MainBanner2SlidesAdmMapper::getCurrentTable(),
        );

        $sQuery = '
            SELECT `[tab_banner:q]`.*,
              (SELECT `[tab_slide:q]`.`img`
                  FROM `[tab_slide:q]`
                  WHERE  `[tab_slide:q]`.`banner_id` = `[tab_banner:q]`.`id` AND `[tab_slide:q]`.`active`
                  ORDER BY `position` ASC
                  LIMIT 1 ) AS preview_img
            FROM `[tab_banner:q]` ;
        ';

        $rResult = $odb->query($sQuery, $aData);

        $aItems = array();

        while( $aRow = $rResult->fetch_assoc() ){
            $aRow['active'] = ($aRow['active'] == 1);

            if( !$aRow['preview_img'] )
                 $aRow['preview_img'] = $sDir.'/img/noimg.png';

            $aItems[] = $aRow;
        }

        $out = array(
            'count' => count($aItems),
            'items' => $aItems
        );
        return $out;

    }

}
