<?php
/**
 * API для работы с баннерами
 * @class MainBanner2BannersAdmApi
 *
 * @author kolesnikov, $Author: $
 * @version $Revision: $
 * @date $Date: $
 */
class MainBanner2BannersAdmApi {

    /**
     * Список полей для списка
     */
    public static function getListFields() {

        return array(
//            'id',
            'title',
            'active',
        );
    }

    /**
     * Набор полей, редактируемых в списке
     * @static
     * @return array
     */
    public static function getEditableListFields() {
        return array(
            'active'
        );
    }

    /**
     * Модель для списка банеров
     * @param $aFields
     * @return array
     */
    public static function getListModel($aFields){
        $aModel = MainBanner2BannersAdmMapper::getFullParamDefList($aFields);

        $aPreview = array(
            'name' => 'preview_img',
            'view' => 'addImg',
            'title' => 'Слайд',
        );

        $aNewModel = array();
        foreach($aModel as $sKey=>$aVal){

            if( $sKey == 'active' )
                $aNewModel['preview_img'] = $aPreview;

            $aNewModel[$sKey] = $aVal;
        }

        return $aNewModel;
    }

    /**
     * Получить список баннеров
     * @param string $sDir
     * @param int $iPage
     * @param int $iOnPage
     * @return array
     */
    public static function getListItems($sDir, $iPage = 0, $iOnPage = 1000) {

        $aBannerList = MainBanner2BannersAdmMapper::getBannerList($sDir);

        return $aBannerList;
    }

    /**
     * Модель для формы редактирования баннера
     * @return array
     */
    public static function getBannerForm() {

        $aModel = MainBanner2BannersAdmMapper::getFullParamDefList(MainBanner2BannersAdmMapper::getModelFields());

        return $aModel;
    }

    /**
     * Получение значение параметра
     * @param $sParamName
     * @return bool
     */
    public static function getToolsParam( $sParamName ) {

        $aItem = MainBanner2ToolsAdmMapper::getItem($sParamName);

        if( !count($aItem) )
            return false;

        return isSet($aItem['bt_value']) ? $aItem['bt_value'] : false;

    }


    public static function setToolsParam( $sParamName, $sParamValue ) {

        $aItem = array(
            'bt_key'=>$sParamName,
            'bt_value'=>$sParamValue
        );

        $mResult = MainBanner2ToolsAdmMapper::saveItem($aItem);

        return $mResult;
    }

    public static function getAllTools() {

        $aItems = MainBanner2ToolsAdmMapper::getItems();

        $out = array();

        if( isSet($aItems['items']) && count($aItems['items']) )
        foreach($aItems['items'] as $aItemValue) {

            $out[$aItemValue['bt_key']] = $aItemValue['bt_value'];

        }

        return $out;
    }

    //----------------------------- public -------------------------------------------
    public static function getBannersListForSection( $iSectionId ) {

        $aBanners = MainBanner2BannersAdmMapper::getBannersForSection( $iSectionId );

        // ищем баннеры заданные для раздела
        $bUseParentBanners = true;
        foreach($aBanners as $aBanner)
            if( $aBanner['section'] == $iSectionId )
                $bUseParentBanners = false;

        // если нашли - удаляем баннеры родителей
        if( !$bUseParentBanners ) {

            foreach($aBanners as $iKey=>$aBanner)
                if( $aBanner['section'] != $iSectionId )
                    unset($aBanners[$iKey]);

            $aBanners = array_values($aBanners);
        }

        return $aBanners;
    }

    /**
     * Получение модели баннера со слайдами
     * @param $iBannerId
     * @return array|bool
     */
    public static function getBannerForView( $iBannerId ) {

        $aBanner = MainBanner2BannersAdmMapper::getBannerWithSlides( $iBannerId );

        return count($aBanner) ? $aBanner : false;
    }


    //----------------------- tools -----------------------------------------
    public static function getToolModel(){

        $aAnimType = array('slide'=>'slide','fade'=>'fade');
        //
        $aModel = array(

            'animtype' => array_merge(
                array( 'name' => 'animtype', 'title' => 'Тип анимации' ),
                ExtForm::getDesc4SelectFromArray($aAnimType)
            ),

            'animduration' => array(
                'name' => 'animduration',
                'type' => 'i',
                'view' => 'str',
                'title' => 'Время анимации смены слайда, мс',
                'default' => '450',
            ),
            'animspeed' => array(
                'name' => 'animspeed',
                'type' => 'i',
                'view' => 'str',
                'title' => 'Время задержки между слайдами, мс',
                'default' => '4000',
            ),
            'hoverpause' => array(
                'name' => 'hoverpause',
                'type' => 'i',
                'view' => 'check',
                'title' => 'Останавливать автоматическую смену слайдов при наведении курсора',
                'default' => 'true',
            ),
            'banner_w' => array(
                'name' => 'banner_w',
                'type' => 'i',
                'view' => 'str',
                'title' => 'Ширина баннера',
                'default' => '980',
            ),
            'banner_h' => array(
                'name' => 'banner_h',
                'type' => 'i',
                'view' => 'str',
                'title' => 'Высота баннера',
                'default' => '272',
            ),
        );

        return $aModel;
    }


}
