<?php
/**
 * Модель для таблицы настроек сайта
 * @class MainBanner2ToolsAdmMapper
 *
 * @author kolesnikov, $Author: $
 * @version $Revision: $
 * @date $Date: $
 * @project Skewer
 */
class MainBanner2ToolsAdmMapper extends skMapperPrototype {

    /**
     * Первичный ключ
     * @var string
     */
    protected static $sKeyFieldName = 'bt_key';

    /**
     * Таблица модели
     * @var string
     */
    protected  static $sCurrentTable = 'banners_tools';

    /**
     * Модель данных
     * @var array
     */
    protected static $aParametersList = array(

        'bt_key'    => 's:str:Название',
        'bt_value'  => 's:str:Значение',

    );


    /**
     * Получить список настроек
     * @return array
     */
    public static function getToolsList(){

        $aItems = self::getItems();

        $aTools = array();

        if( $aItems['count'] && $aItems['items'] )
        foreach( $aItems['items'] as $aItem) {

            $aTools[ $aItem['bt_key'] ] = $aItem['bt_value'];
        }

        return $aTools;
    }


    /**
     * Сохранить список настроек
     * @param array $aItems
     * @return bool
     */
    public static function saveTools($aItems = array()) {

        global $odb;

        $sQuery = "
            UPDATE `[table:q]`
            SET `bt_value`=[bt_value:s]
            WHERE
                `bt_key` = [bt_key:s]";

        foreach( $aItems as $sKey=>$sValue ){

            $aTemp = array(
                'table'     => self::getCurrentTable(),
                'bt_key'    => $sKey,
                'bt_value'  => $sValue
            );

            $odb->query($sQuery, $aTemp);

        }

        return true;
    }


}