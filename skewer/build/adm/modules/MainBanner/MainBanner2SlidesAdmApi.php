<?php
/**
 * API для работы со слайдами баннера
 * @class MainBanner2SlidesAdmApi
 *
 * @author kolesnikov, $Author: $
 * @version $Revision: $
 * @date $Date: $
 */
class MainBanner2SlidesAdmApi {

    /**
     * Список полей для списка
     */
    public static function getListFields() {

        return array(
//            'id',
            'active',
        );
    }

    /**
     * Набор полей, редактируемых в списке
     * @static
     * @return array
     */
    public static function getEditableListFields() {
        return array(
            'active'
        );
    }

    /**
     * Модель для списка банеров
     * @param $aFields
     * @return array
     */
    public static function getListModel($aFields){

        $aModel = MainBanner2SlidesAdmMapper::getFullParamDefList($aFields);

        $aPreview = array(
            'name' => 'preview_img',
            'view' => 'addImg',
            'title' => 'Слайд',
        );

        $aNewModel = array();
        foreach($aModel as $sKey=>$aVal){

            if( $sKey == 'active' )
                $aNewModel['preview_img'] = $aPreview;

            $aNewModel[$sKey] = $aVal;
        }

        return $aNewModel;
    }

    /**
     * Получить список баннеров
     * @param $iBannerId
     * @param int $iPage
     * @param int $iOnPage
     * @return array
     */
    public static function getListItems($iBannerId, $iPage = 0, $iOnPage = 1000) {

        $aFilter = array(
            'where_condition' => array(
                'banner_id' => array('sign'=>'=','value'=>$iBannerId)
            ),
            'limit'=>array(
                'start'=>$iPage*$iOnPage,
                'count'=>$iOnPage
            ),
            'order' => array(
                'field' => 'position',
                'way' => 'ASC'
            )
        );

        $aItems = MainBanner2SlidesAdmMapper::getItems($aFilter);
        $aNewItems = array('items'=>array());
        if(isSet($aItems['items']) && count($aItems['items']))
        foreach($aItems['items'] as $aItem){

            $aItem['preview_img'] = $aItem['img'];

            $aNewItems['items'][] = $aItem;
        }

        $aNewItems['count'] = count($aNewItems['items']);

        return $aNewItems;
    }

    /**
     * Модель для формы редактирования баннера
     * @return array
     */
    public static function getBannerForm() {

        $aModel = MainBanner2SlidesAdmMapper::getFullParamDefList(array('id','img','active','position'));

        return $aModel;
    }


    /**
     * Загружает изображения и перемещает в целевую директорию
     * @static
     * @return bool|string
     * @throws Exception
     */
    public static function uploadFile() {

        // параметры загружаемого файла
        $aFilter = array();
        $aFilter['size']            = skConfig::get('upload.maxsize');
        $aFilter['allowExtensions'] = skConfig::get('upload.allow.images');
        $aFilter['imgMaxWidth']     = skConfig::get('upload.images.maxWidth');
        $aFilter['imgMaxHeight']    = skConfig::get('upload.images.maxHeight');

        // загрузка
        $oFiles = skUploadedFiles::get($aFilter, FILEPATH, PRIVATE_FILEPATH);

        // если ни один файл не загружен - выйти
        if(!$oFiles->count())
            throw new Exception ($oFiles->getError());

        /*
         * пока берется только один файл, но делался задел на загрузку
         * большего количества
         */

        // имя файла исходника
        $sSourceFN = false;

        /** @noinspection PhpUnusedLocalVariableInspection */
        foreach($oFiles  as  $file) {

            // проверка ошибок
            if($sError = $oFiles->getError())
                throw new Exception ($sError);

            /* @todo Должна быть добавлена проверка на защиту директории (protected: true/false) */
            $iSectionId = 78; // todo указать нужный раздел
            $sSourceFN = $oFiles->UploadToSection($iSectionId, 'banner/', false);

        }

        // проверка наличия имени файла
        if(!$sSourceFN)
            throw new Exception("Изображение не было загружено!");

        // отрезать корневую папку от пути
        $sSourceFN = substr( $sSourceFN, strlen(ROOTPATH)-1 );

        return $sSourceFN;

    }

    /**
     * Сортировка полей в таблице
     * @param $aItem
     * @param $aTarget
     * @param string $sOrderType
     * @return bool
     */
    public static function sortSlides($aItem, $aTarget, $sOrderType='before') {

        $sSortField = 'position';

        // должны быть в одной форме
        if( $aItem['banner_id'] != $aTarget['banner_id'] )
            return false;

        // выбираем напрвление сдвига
        if( $aItem[$sSortField] > $aTarget[$sSortField] ){

            $iStartPos = $aTarget[$sSortField];
            if( $sOrderType=='before' ) $iStartPos--;
            $iEndPos = $aItem[$sSortField];
            $iNewPos = $sOrderType=='before' ? $aTarget[$sSortField] : $aTarget[$sSortField] + 1;
            MainBanner2SlidesAdmMapper::shiftParamPosition($aItem['banner_id'], $iStartPos, $iEndPos, '+');
            MainBanner2SlidesAdmMapper::changeParamPosition($aItem['id'],$iNewPos);

        } else {

            $iStartPos = $aItem[$sSortField];
            $iEndPos = $aTarget[$sSortField];
            if( $sOrderType=='after' ) $iEndPos++;
            $iNewPos = $sOrderType=='after' ? $aTarget[$sSortField] : $aTarget[$sSortField] - 1;
            MainBanner2SlidesAdmMapper::shiftParamPosition($aItem['banner_id'], $iStartPos, $iEndPos, '-');
            MainBanner2SlidesAdmMapper::changeParamPosition($aItem['id'],$iNewPos);

        }

        return true;

    }

}
