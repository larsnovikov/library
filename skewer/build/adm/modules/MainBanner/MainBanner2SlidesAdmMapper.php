<?php
/**
 * Модель для сущности - Слайд
 * @class MainBanner2SlidesAdmMapper
 *
 * @author kolesnikov, $Author: $
 * @version $Revision: $
 * @date $Date: $
 * @project Skewer
 */
class MainBanner2SlidesAdmMapper extends skMapperPrototype {

    /**
     * Первичный ключ
     * @var string
     */
    //protected static $sKeyFieldName = 'banner_id';

    /**
     * Таблица модели
     * @var string
     */
    protected  static $sCurrentTable = 'banners_slides';

    /**
     * Модель данных
     * @var array
     */
    protected static $aParametersList = array(

        'id'        => 'i:hide:Идентификатор слайда',
        'banner_id' => 'i:hide:Баннер',
        'active'    => 'i:check:Активность:1',
        'position'  => 'i:hide:Порядковый номер',
        'img'       => 's:hide:Фоновое изображение',
        'text1'     => 's:hide:Текстовый блок 1',
        'text1_h'   => 'i:hide:1',
        'text1_v'   => 'i:hide:1',
        'text2'      => 's:hide:Текстовый блок 2',
        'text2_h'   => 'i:hide:1',
        'text2_v'   => 'i:hide:1',
        'text3'      => 's:hide:Текстовый блок 3',
        'text3_h'   => 'i:hide:1',
        'text3_v'   => 'i:hide:1',
        'text4'      => 's:hide:Текстовый блок 4',
        'text4_h'   => 'i:hide:1',
        'text4_v'   => 'i:hide:1',

    );


    /**
     * Возвращает набор слайдов для баннера
     * @param $iBannerId
     * @return array|bool
     */
    public static function getSlidesForBanner( $iBannerId ) {

        $aFilter = array(
            'where_condition' => array(
                'banner_id' => array('sign'=>'=','value'=>$iBannerId),
                'active' => array('sign'=>'=','value'=>1),
            ),
            'order' => array(
                'field' => 'position',
                'way' => 'ASC'
            )
        );

        $aItems = self::getItems($aFilter);

        return (isSet($aItems['items']) && count($aItems['items'])) ? $aItems['items'] : false;
    }


    /**
     * Выполняет сдвиг положений параметров
     * @param $iFormId
     * @param $iStartPos
     * @param $iEndPos
     * @param string $sSign
     * @return int
     */
    public static function shiftParamPosition($iFormId,$iStartPos,$iEndPos,$sSign = '+'){

        global $odb;

        $sQuery = "
            UPDATE `[table:q]`
            SET `position`=`position` [sign:q] 1
            WHERE
                `banner_id` = [form:i] AND
                `position` > [start:i] AND
                `position` < [end:i]";

        $odb->query($sQuery, array(
            'table' => self::$sCurrentTable,
            'form' => $iFormId,
            'sign' => $sSign,
            'start' => $iStartPos,
            'end' => $iEndPos
        ));

        return $odb->affected_rows;
    }

    /**
     * меняет значение для поля сортировки параметра на заданное
     * @param $iParamId
     * @param $iPos
     * @return int
     */
    public static function changeParamPosition($iParamId, $iPos){

        global $odb;

        $sQuery = "
            UPDATE `[table:q]`
            SET `position`=[pos:i]
            WHERE
                `id` = [param_id:i]";

        $odb->query($sQuery, array(
            'table' => self::$sCurrentTable,
            'param_id' => $iParamId,
            'pos' => $iPos
        ));

        return $odb->affected_rows;
    }


    public static function getMaxOrder($iFormId, $iCurItemId = 0){

        global $odb;

        $iCurItemId = (int)$iCurItemId;

        $aData = array( 'banner_id' => $iFormId,
            'add_where' => $iCurItemId ? "`id`<>$iCurItemId AND" : '',
            'table' => self::$sCurrentTable
        );

        $sQuery = "
            SELECT
                MAX(`position`) as position
            FROM
                `[table:q]`
            WHERE [add_where:q]
                `banner_id` = [banner_id:i];";

        $aValue = $odb->getRow($sQuery, $aData);

        return $aValue['position'];
    }// function getMaxOrder()


}

