<?php
/**
 * API для админской части новостной системы
 */

class DocumentsAdmApi {

    /**
     * @static Возвращает набор полей для отображения в списке
     * @return array
     */
    public static function getListFields(){
        return
            array(
                'id',
                'title',
                'publication_date',
                'active',
                'on_main',
                'archive'
            );
    }

    public static function getDetailFields(){
        return
            array(
                'id',
                'title',
                'publication_date',
                'publication_time',
                'announce',
                'full_text',
                'active',
                'on_main',
                'archive',
                'hyperlink',
                'news-alias'
            );
    }

    public static function getModel($aFields){

        return DocumentsMapper::getFullParamDefList($aFields);
    }

    /**
     * Метод по выборке из базы конкретной новости по ID и родителю
     * @param $iNewsId - id новости
     * @param $iSectionId - id раздела
     * @return array
     */
    public static function getNewsById($iNewsId,$iSectionId){

        // Собираем массив фильтра
        $aFilter = array(
            'where_condition' => array(
                'id' => array(
                    'sign' => '=',
                    'value' => $iNewsId
                ),
                'parent_section' => array(
                    'sign' => '=',
                    'value' => $iSectionId
                )
            )
        );

        // запрос данных
        $aItem = DocumentsMapper::getItem($aFilter);

        // преобразование даты
        if ( $aItem ){

            $tDate = strtotime($aItem['publication_date']);
            $aItem['publication_date'] = date('d.m.Y', $tDate);
            $aItem['publication_time'] = date('H:i', $tDate);
            $aItem['full_text'] = ImageResize::restoreTags($aItem['full_text']);
            $aItem['announce'] = ImageResize::restoreTags($aItem['announce']);
        }

        return $aItem;
    }

    /**
     * Метод по выборке списка новостей из базы
     * @static
     * @param $aFilter
     * @internal param $iSectionId - id раздела
     * @internal param int $iPage - страница
     * @internal param int $iOnPage - число элементов на страницу
     * @return array
     */
    public static function getNewsList( $aFilter ){

        // запросить значения
        $aResponse = DocumentsMapper::getItems($aFilter);

        // пересобрать даты
        if ( $aResponse['count'] ) {
            foreach ( $aResponse['items'] as $iKey => $aRow ) {
                $aResponse['items'][$iKey]['publication_date'] = date('d.m.Y H:i',strtotime($aRow['publication_date']));
            }
        }

        return $aResponse;
    }

    /**
     * @static Сохраняет данные
     * @param $aData - массив на сохранение
     * @param $iSectionId - id родительского раздела
     * @param \AdminModulePrototype $oCaller ссылка на вызвавший модуль
     * @return int
     */
    public static function updNews( $aData, $iSectionId, AdminModulePrototype $oCaller ) {

        if ( $aData ) {
            $aData['parent_section'] = $iSectionId;

            if ( !isset($aData['publication_time']) )
                $aData['publication_time'] = '12:00:00';

            $aData['publication_date'] = date('Y-m-d H:i:s',strtotime($aData['publication_date'].$aData['publication_time']));
            unset($aData['publication_time']);

            $aData['last_modified_date'] = date("Y-m-d H:i:s", time());

            if ( !isset($aData['news-alias']) || empty($aData['news-alias']) )
                $aData['news-alias'] = skTranslit::change($aData['title']);
            else
                $aData['news-alias'] = skTranslit::change($aData['news-alias']);

            if ( preg_match("/^[0-9]+$/i", $aData['news-alias']) ){
                $aData['news-alias'] = 'news-'.$aData['news-alias'];
            }
            $aData['news-alias'] = skTranslit::changeDeprecated($aData['news-alias']);
            $aData['news-alias'] = skTranslit::mergeDelimiters($aData['news-alias']);
            $aData['news-alias'] = self::checkAlias($aData['id'],$aData['news-alias']);

            $aData['full_text'] = ImageResize::wrapTags($aData['full_text'],$aData['parent_section']);
            $aData['announce'] = ImageResize::wrapTags($aData['announce'],$aData['parent_section']);

            if( $aData['hyperlink'] && (strpos($aData['hyperlink'],'http') === false) && ($aData['hyperlink'][0] !== '/') )
                $aData['hyperlink'] = 'http://'.$aData['hyperlink'];

            $iNewsId = DocumentsMapper::saveItem($aData);

            if ( $iNewsId ){

                try {
                    RSS::buildRssFile();
                } catch ( Exception $e ) {
                    $oCaller->addError($e->getMessage());
                }

                if ( !$aData['id'] ) $aData['id'] = $iNewsId;

                self::addToIndex($aData);
            }

            return $iNewsId;

        }

        return 0;
    }

    /**
     * Отдает шаблонный набор значений для добавления новой записи
     * @static
     * @return array
     */
    public static function getBlankValues() {
        return array(
            'title' => 'Документ',
            'publication_date' => date('d.m.Y',time()),
            'publication_time' => date('H:i',time()),
            'active' => 1
        );
    }

    /**
     * @static
     * @param array $aData
     * @return bool|int
     */
    public static function addToIndex($aData = array()){

        if ( !$aData ) return false;

        $aData['full_text'] = strip_tags($aData['full_text']);
        $sHref = (!empty($aData['news-alias']))? skRouter::rewriteURL('['.$aData['parent_section'].'][DocumentsModule?news-alias='.$aData['news-alias'].']'): skRouter::rewriteURL('['.$aData['parent_section'].'][DocumentsModule?news_id='.$aData['id'].']');
        $sHref = $_SERVER['HTTP_HOST'].$sHref;

        return Search::addToIndex($aData['title'], $aData['full_text'], $aData['id'], 'News', $aData['parent_section'], $sHref);
    }

    /**
     * @static
     * @param $iItemId
     * @return bool|mysqli_result
     */
    public static function removeFromIndex($iItemId){

        if ( !$iItemId ) return false;

        return Search::removeFromIndex($iItemId, 'News');
    }

    /**
     * @static
     * @return array
     */
    public static function getIndexData(){

        $aItems = array();
        $aFilter = array(
            'select_fields' => array(
                'id',
                'news-alias',
                'parent_section',
                'title',
                'full_text'
            )
        );

        $aTempItems = DocumentsMapper::getItems($aFilter);

        $sDomain = DomainsToolApi::getMainDomain();

        if ( $aTempItems['count'] ){

            foreach($aTempItems['items'] as $aTempItem){

                $aTempItem['full_text'] = strip_tags($aTempItem['full_text']);

                $sLink = (!empty($aTempItem['news-alias']))? skRouter::rewriteURL('['.$aTempItem['parent_section'].'][DocumentsModule?news-alias='.$aTempItem['news-alias'].']'): skRouter::rewriteURL('['.$aTempItem['parent_section'].'][DocumentsModule?news_id='.$aTempItem['id'].']');

                $aItems[] = array(
                    'search_title' => $aTempItem['title'],
                    'search_text' => $aTempItem['full_text'],
                    'id' => $aTempItem['id'],
                    'class_name' => 'Documents',
                    'parent_section' => $aTempItem['parent_section'],
                    'href' => $sDomain.$sLink
                );

            }
        }

        return $aItems;
    }

    /**
     * @static
     * @param $iId
     * @param $sAlias
     * @return string
     */
    public static function checkAlias($iId, $sAlias){

        if ( !($sAlias) ) $sAlias = date('d.m.Y H:i');

        $aItems = DocumentsMapper::checkAlias($sAlias);
        $iSize = sizeof($aItems);

        if ( !$iSize || ($iSize==1 && array_key_exists($iId, $aItems)) ) return $sAlias;

        if ( $iSize>1 || ($iSize==1 && !array_key_exists($iId, $aItems)) ) {

            return self::checkAlias($iId, $sAlias.++$iSize);
        }

        return false;
    }

    /**
     * @static
     * @param $iItemId
     * @param \AdminModulePrototype $oCaller ссылка на вызвавший модуль
     */
    public static function delNews($iItemId, AdminModulePrototype $oCaller){

        if ( DocumentsMapper::delItem($iItemId) ){

            try {
                RSS::buildRssFile();
            } catch ( Exception $e ) {
                $oCaller->addError($e->getMessage());
            }

            self::removeFromIndex($iItemId);
        }
    }

}
