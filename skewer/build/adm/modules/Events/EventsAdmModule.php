<?php

/**
 * @class EventsAdmModule
 * @extends skModule
 * @project Skewer
 * @package kernel
 *
 * @author Andy Mitrich, $Author: acat $
 * @version $Revision: 1172 $
 * @date 28.11.11 17:04 $
 *
 */

class EventsAdmModule extends AdminSectionTabModulePrototype {

    // id текущего раздела
    protected $iSectionId = 0;

    // число элементов на страницу
    protected $iOnPage = 20;

    // текущий номер страницы ( с 0, а приходит с 1 )
    protected $iPage = 0;

    /**
     * @var string - Имя модуля
     */
    protected $sTabName = 'Мероприятия';

    /**
     * Метод, выполняемый перед action меодом
     * @throws ModuleAdminErrorException
     * @return bool
     */
    protected function preExecute() {

        // номер страницы
        $this->iPage = $this->getInt('page');

        // id текущего раздела
        $this->iSectionId = $this->getInt('sectionId');

        // проверить права доступа
        if ( !CurrentAdmin::canRead($this->iSectionId) )
            throw new ModuleAdminErrorException( 'accessDenied' );

    }

    /**
     * Первичное состояние
     */
    protected function actionInit() {

        // вывод списка
        $this->actionList();

    }

    /**
     * Список новостей
     */
    protected function actionList() {

        // объект для построения списка
        $oList = new ExtList();

        /**
         * Модель данных
         */

        $aModel = EventsAdmApi::getModel(EventsAdmApi::getListFields());

        // задать модель данных для вывода
        $oList->setFields( $aModel );

        // число записей на страницу
        $oList->setPageNum( $this->iPage );
        $oList->setOnPage( $this->iOnPage );

        // добавление набора данных
        $aItems = EventsAdmApi::getListItems($this->iSectionId, $this->iPage, $this->iOnPage);

        $oList->setValues( $aItems['items'] );
        $oList->setTotal( $aItems['count'] );

        // добавление кнопок
        $oList->addRowBtnUpdate();
        $oList->addRowBtnDelete();

        // кнопка добавления
        $oList->addBntAdd('show');

        // вывод данных в интерфейс
        $this->setExtInterface( $oList );

    }

    /**
     * Отображение формы
     */
    protected function actionShow() {

        // подключить автоматический генератор форм
        $oForm = new ExtForm();

        // номер новости
        $aData = $this->get('data');
        $iItemId = (is_array($aData) && isset($aData['id'])) ? (int)$aData['id'] : 0;

        // запись новости
        $aItem = $iItemId ? EventsAdmApi::getEventById( $iItemId, $this->iSectionId ) : EventsAdmApi::getEventBlankValues();

        // установить набор элементов формы
        $oForm->setFields( EventsAdmApi::getModel(EventsAdmApi::getDetailFields()) );

        // установить значения для элементов
        $oForm->setValues( $aItem );

        // добавление кнопок
        $oForm->addBntSave();
        $oForm->addBntCancel();
        if ( $iItemId ) {
            $oForm->addBntSeparator('->');
            $oForm->addBntDelete();
        }

        // добавление SEO блока полей
        SEOData::appendExtForm( $oForm, 'events', $iItemId );

        // вывод данных в интерфейс
        $this->setExtInterface( $oForm );

    }

    /**
     * Сохранение новости
     */
    protected function actionSave() {

        // запросить данные
        $aData = $this->get( 'data' );

        // есть данные - сохранить
        if ( $aData ) {

            EventsAdmApi::updEvent( $aData, $this->iSectionId );

            if ( isset($aData['id']) && $aData['id'] )
                $this->addModuleNoticeReport("Редактирование мероприятия",$aData);
            else {
                unset($aData['id']);
                $this->addModuleNoticeReport("Создание мероприятия",$aData);
            }
        }

        $this->fireEvent('EventsAdmModule:save');

        // вывод списка
        $this->actionList();
    }

    /**
     * Удаляет запись
     */
    protected function actionDelete() {

        // запросить данные
        $aData = $this->get( 'data' );

        // id записи
        $iItemId = ( is_array($aData) and isset($aData['id']) ) ? (int)$aData['id'] : 0;

        // удаление
        EventsAdmApi::delEvent( $iItemId );

        $this->addModuleNoticeReport("Удаление мероприятия",$aData);

        $this->fireEvent('EventsAdmModule:delete');

        // вывод списка
        $this->actionList();
    }

    /**
     * Установка служебных данных
     * @param \ExtPrototype $oExtIface
     */
    protected function setServiceData( ExtPrototype $oExtIface ) {

        // установить заголовок модуля
        $oExtIface->setTitle( $this->sTabName );

        // установить данные для передачи интерфейсу
        $oExtIface->setServiceData( array(
            'sectionId' => $this->iSectionId,
            'page' => $this->iPage
        ) );

    }

}//class
