<?php

/**
 * @class EventsAdmInstall
 * @extends skModule
 * @project Skewer
 * @package adm
 *
 * @author Andy Mitrich, $Author: sapozhkov $
 * @version $Revision: 6 $
 * @date 07.02.12 17:39 $
 *
 */

class EventsAdmInstall extends skModuleInstall{

    public function init() {
        return true;
    }// func

    public function install() {
        return true;
    }// func

    public function deinstall() {
        return true;
    }// func

}//class