<?php

/* main */
$aConfig['name']     = 'EventsAdm';
$aConfig['title']    = 'Мероприятия (админ)';
$aConfig['version']  = '1.000a';
$aConfig['description']  = 'Админ-интерфейс управления мероприятиями';
$aConfig['revision'] = '0001';
$aConfig['layer']     = 'adm';

//$aConfig['hooks']['after']['removeSection']  = array(
//
//    'class' => 'EventsApi',
//    'method' => 'removeFromSection',
//);


return $aConfig;