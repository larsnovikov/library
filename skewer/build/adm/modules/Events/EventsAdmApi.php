<?php

/**
 * @class EventsAdmApi
 * @extends skModule
 * @project Skewer
 * @package kernel
 *
 * @author Andy Mitrich, $Author: sapozhkov $
 * @version $Revision: 1346 $
 * @date 28.11.11 17:04 $
 *
 */

class EventsAdmApi {

    /**
     * @static Возвращает набор полей для отображения в списке
     * @return array
     */
    public static function getListFields(){

        return array(
            'id',
            'title',
            'publication_date',
            'date_start',
            'date_end',
            'active',
            'on_main',
            'archive'
        );
    }

    public static function getDetailFields(){

        return array(
            'id',
            'title',
            'publication_date',
            'publication_time',
            'date_start',
            'date_end',
            'announce',
            'full_text',
            'active',
            'on_main',
            'archive',
            'hyperlink',
            'event-alias'
        );
    }

    public static function getModel($aFields){

        return EventsMapper::getFullParamDefList($aFields);
    }

    public static function getListItems($iSectionId, $iPage, $iOnPage){

        // Собираем массив фильтра
        $aFilter = array(
            'select_fields' => self::getListFields(),
            'where_condition' => array(
                'parent_section' => array(
                    'sign' => '=',
                    'value' => $iSectionId
                )
            )
        );

        // постраничный
        if ( $iOnPage ) {
            $aFilter['limit'] = array(
                'start' => $iPage*$iOnPage,
                'count' => $iOnPage
            );
        }

        $aItems = EventsMapper::getItems($aFilter);

        if ( sizeof($aItems) )
            foreach( $aItems['items'] as &$aItem ){

                $aItem['publication_date'] = date('d.m.Y',strtotime($aItem['publication_date']));
                $aItem['date_start'] = date('d.m.Y',strtotime($aItem['date_start']));
                $aItem['date_end'] = date('d.m.Y',strtotime($aItem['date_end']));
            }

        return $aItems;
    }

    public static function getEventById($iItemId){

        $aItem = EventsMapper::getItem($iItemId);

        if ( $aItem ){
            $tDate = strtotime($aItem['publication_date']);
            $aItem['publication_date'] = date('d.m.Y',$tDate);
            $aItem['publication_time'] = date('H:i',$tDate);
            $aItem['date_start'] = date('d.m.Y',strtotime($aItem['date_start']));
            $aItem['date_end'] = date('d.m.Y',strtotime($aItem['date_end']));
            $aItem['full_text'] = ImageResize::restoreTags($aItem['full_text']);
            $aItem['announce'] = ImageResize::restoreTags($aItem['announce']);
        }

        return $aItem;
    }

    public static function updEvent( $aData, $iSectionId ){

        if ( !isset($aData['publication_time']) )
            $aData['publication_time'] = '12:00:00';

        $aData['publication_date'] = date('Y-m-d H:i:s',strtotime($aData['publication_date'].$aData['publication_time']));
        unset($aData['publication_time']);

        $aData['date_start'] = date('Y-m-d',strtotime($aData['date_start'])).' 12:00:00';
        $aData['date_end'] = date('Y-m-d',strtotime($aData['date_end'])).' 12:00:00';
        $aData['parent_section'] = $iSectionId;
        $aData['last_modified_date'] = date("Y-m-d H:i:s", time());

        if ( !isset($aData['event-alias']) || empty($aData['event-alias']) )
            $aData['event-alias'] = skTranslit::change($aData['title']);
        else
            $aData['event-alias'] = skTranslit::change($aData['event-alias']);

        if ( preg_match("/^[0-9]+$/i", $aData['event-alias']) ){
            $aData['event-alias'] = 'event-'.$aData['event-alias'];
        }

        $aData['event-alias'] = skTranslit::changeDeprecated($aData['event-alias']);
        $aData['event-alias'] = skTranslit::mergeDelimiters($aData['event-alias']);
        $aData['event-alias'] = self::checkAlias($aData['id'],$aData['event-alias']);

        $aData['full_text'] = ImageResize::wrapTags($aData['full_text'],$aData['parent_section']);
        $aData['announce'] = ImageResize::wrapTags($aData['announce'],$aData['parent_section']);

        if( $aData['hyperlink'] && (strpos($aData['hyperlink'],'http') === false) && ($aData['hyperlink'][0] !== '/') )
            $aData['hyperlink'] = 'http://'.$aData['hyperlink'];

        $iEventId = EventsMapper::saveItem($aData);

        if ( $iEventId ){

            try {
                RSS::buildRssFile();
            } catch ( Exception $e ) {
                return false;
            }

            if ( !$aData['id'] ) $aData['id'] = $iEventId;

            self::addToIndex($aData);
        }
        return true;
    }

    public static function checkAlias($iId, $sAlias){

        if ( !($sAlias) ) $sAlias = date('d.m.Y H:i');

        $aItems = EventsMapper::checkAlias($sAlias);
        $iSize = sizeof($aItems);

        if ( !$iSize || ($iSize==1 && array_key_exists($iId, $aItems)) ) return $sAlias;

        if ( $iSize>1 || ($iSize==1 && !array_key_exists($iId, $aItems)) ) {

            return self::checkAlias($iId, $sAlias.++$iSize);
        }

        return false;
    }

    public static function delEvent($iItemId){

        if ( EventsMapper::delItem($iItemId) ){

            try {
                RSS::buildRssFile();
            } catch ( Exception $e ) {
                return false;
            }

            EventsAdmApi::removeFromIndex($iItemId);
        }

        return true;

    }

    /**
     * Отдает шаблонный набор значений для добавления новой записи
     * @static
     * @return array
     */
    public static function getEventBlankValues() {

        return array(
            'title' => 'Мероприятие',
            'publication_date' => date('d.m.Y',time()),
            'publication_time' => date('H:i',time()),
            'date_start' => date('d.m.Y',time()),
            'date_end' => date('d.m.Y',time()),
            'active' => 1
        );
    }

    /**
     * @static
     * @param array $aData
     * @return bool|int
     */
    public static function addToIndex($aData = array()){

        if ( !$aData ) return false;

        $aData['full_text'] = strip_tags($aData['full_text']);

        $sHref = (!empty($aData['event-alias']))? skRouter::rewriteURL('['.$aData['parent_section'].'][EventsModule?event-alias='.$aData['event-alias'].']'): skRouter::rewriteURL('['.$aData['parent_section'].'][EventsModule?event_id='.$aData['id'].']');
        $sHref = $_SERVER['HTTP_HOST'].$sHref;
        return Search::addToIndex($aData['title'], $aData['full_text'], $aData['id'], 'Events', $aData['parent_section'], $sHref);
    }

    /**
     * @static
     * @param $iItemId
     * @return bool|mysqli_result
     */
    public static function removeFromIndex($iItemId){

        if ( !$iItemId ) return false;

        return Search::removeFromIndex($iItemId, 'Events');
    }

    public static function getIndexData(){

        $aItems = array();
        $aFilter = array(
            'select_fields' => array(
                'id',
                'event-alias',
                'parent_section',
                'title',
                'full_text'
            )
        );

        $aTempItems = EventsMapper::getItems($aFilter);

        $sDomain = DomainsToolApi::getMainDomain();

        if ( $aTempItems['count'] ){

            foreach($aTempItems['items'] as $aTempItem){

                $aTempItem['full_text'] = strip_tags($aTempItem['full_text']);

                $sLink = (!empty($aTempItem['event-alias']))? skRouter::rewriteURL('['.$aTempItem['parent_section'].'][EventsModule?event-alias='.$aTempItem['event-alias'].']'): skRouter::rewriteURL('['.$aTempItem['parent_section'].'][EventsModule?event_id='.$aTempItem['id'].']');

                $aItems[] = array(
                    'search_title' => $aTempItem['title'],
                    'search_text' => $aTempItem['full_text'],
                    'id' => $aTempItem['id'],
                    'class_name' => 'Events',
                    'parent_section' => $aTempItem['parent_section'],
                    'href' => $sDomain.$sLink
                );
            }
        }

        return $aItems;
    }

}
