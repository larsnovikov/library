<?php
/**
 * Панель отображения набора файлов для панели выбора файлов
 *
 * @class: Files4FileBrowserAdmModule
 *
 * @Author: User, $Author: sapozhkov $
 * @version: $Revision: 1334 $
 * @date: $Date: 2012-11-30 11:53:56 +0400 (Пт., 30 нояб. 2012) $
 *
 */

class Files4FileBrowserAdmModule extends FilesAdmModule {

    // возможность выбирать файлы
    protected $bCanSelect = true;

    protected $sListBuilderClass = 'Files2ExtListAdmModule';

    public function init(){

        // вызвать инициализацию, прописанной в родительском классе
        parent::init();

        // установить имя для используемого модуля
        $this->addLibClass('FileBrowserFiles');

    }

}
