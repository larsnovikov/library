<?php
/**
 * Панель отображения набора файлов для панели выбора файлов дизайнерского режима
 */
class Files4DesignFileBrowserAdmModule extends Files4FileBrowserAdmModule {

    /**
     * Отдает id директории для записи
     * @return int
     */
    protected function getSectionId() {
        return 'design';
    }


}
