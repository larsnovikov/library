/**
 * Расширение стандартного класса автогенериремых списков
 * под нужны выбор файлов во всплывающем окне
 */
Ext.define('Ext.adm.FileBrowserFiles',{

    extend: 'Ext.Builder.List',

    multiSelect: false,

    execute: function( data, cmd ){

        switch ( cmd ) {

            case 'selectFile':

                // взять выбранную строку
                var selection = this.getView().getSelectionModel().getSelection();

                // если не выбрано - выдать ошибку
                if ( !selection.length ) {
                    sk.error(buildLang.fileBrowserNoSelection);
                    break;
                }

                // выполнить переданные директивы
                var value = selection[0].get('webPathShort');
                processManager.fireEvent( 'select_file_set', value );

                break;

            default:
                this.callParent(arguments);
                break;

        }

    },

    /**
     * Событие по двойному клику на строке - выбрать запись
     */
    onDblClick: function() {

        this.execute( {}, 'selectFile' );

    }

});
