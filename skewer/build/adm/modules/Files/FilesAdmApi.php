<?php
/**
 * Api для работы с браузером загруженных фалов
 *
 * @class: FilesAdmApi
 *
 * @Author: User, $Author: sapozhkov $
 * @version: $Revision: 1316 $
 * @date: $Date: 2012-11-27 18:31:02 +0400 (Вт., 27 нояб. 2012) $
 *
 */

class FilesAdmApi extends skModelPrototype {

    /** @var string имя директории миниатюр */
    public static $thumbDirName = '_thumb';

    /**
     * @var array Конфигурация полей
     */
    protected static $aParametersList = array(
        'name' => 'i:str:Имя файла',
        'size' => 'i:str:Размер',
        'ext' => 'i:str:Тип файла',
        'modifyDate' => 'i:str:Дата модификации',
        'webPath' => 'i:str:Web адрес',
        'webPathShort' => 'i:str:Web адрес без хоста',
        'serverPath' => 'i:str:Адрес на сервере'
    );

    /**
     * Отдает дополнительный набор параметров для конфигурации полей
     * @return array
     */
    protected static function getAddParamList() {
        return array(
            'name' => array(
                'listColumns' => array(
                    'flex' => 1
                )
            ),
            'modifyDate' => array(
                'listColumns' => array(
                    'width' => 150
                )
            )

        );
    }


    /**
     * Возвращщает набор полей для списка
     * @static
     * @return array
     */
    public static function getListFields() {
        return explode(',','name,size,ext,modifyDate');
    }

    /**
     * Запрос набора файлов раздела (без серверного пути)
     * @param int $iSectionId id раздела
     * @return array
     */
    public static function getFiles( $iSectionId ) {

        if ( !$iSectionId )
            return array();

        // запросить файлы
        $aFiles = skFiles::getFilesFromSection( $iSectionId );

        if ( !$aFiles )
            return array();

        // удрать поле серверного имени
        foreach ( $aFiles as $iKey => $aFile ) {
            unset( $aFiles[$iKey]['serverPath'] );
        }

        return $aFiles;

    }

    /**
     * Удаляет файл
     * @static
     * @param int $iSectionId id раздела
     * @param string $sFileName имя файла
     * @return bool
     */
    public static function deleteFile( $iSectionId, $sFileName ) {

        // взять раздел
        $sDir = skFiles::checkFilePath( $iSectionId );

        // раздел есть и файл удален
        $bRes = (bool)($sDir and skFiles::remove( $sDir.$sFileName ));

        if ( $bRes ) {
            FilesAdmApi::deleteThumbnail( $sDir.$sFileName );
        }

        return $bRes;

    }

    /**
     * Загружает переданный набор файлов в прикрепленнцю к разделу директорию
     * @static
     * @param int $iSectionId id раздела
     * @throws Exception
     * @return array ( total, loaded )
     */
    public static function uploadFiles( $iSectionId ) {

        $iTotal = 0;
        $iLoaded = 0;
        $aFiles = array();

        // инициализация загрузчика
        $aFilter['allowExtensions'] = skConfig::get('upload.allow.files');
        $oUplFiles = skUploadedFiles::get($aFilter, FILEPATH, PRIVATE_FILEPATH);
        $oUplFiles->setOnUpload( array('FilesAdmApi','onFileUpload') );

        if($oUplFiles->count()) {

            /** @noinspection PhpUnusedLocalVariableInspection */
            foreach($oUplFiles as $file) {
                try {
                    $iTotal++;
                    if($sError = $oUplFiles->getError()) throw new Exception ($sError);
                    if ( $sFilePath = $oUplFiles->UploadToSection($iSectionId) ) {
                        $iLoaded++;
                        $aFiles[] = basename($sFilePath);
                    }
                } catch (Exception $e) {
                }
            }

        }

        return array(
            'files' => $aFiles,
            'total' => $iTotal,
            'loaded' => $iLoaded
        );

    }

    /**
     * Обработчик после загрузки файла
     * @static
     * @param $aItem
     * @return bool
     */
    public static function onFileUpload( $aItem ){

        // инициализация переменных
        $sBaseDir = $aItem['fileDir'];
        $sName = $aItem['fileName'];
        $sThumbDir = $sBaseDir.self::$thumbDirName.'/';
        $sSrcFileName = $aItem['name'];
        $sThumbFileName = $sThumbDir.$sName;
        $iWidth = 85;
        $iHeight =  64;

        // проверка на картинку: нет - выйти
        $aImgTypes = skConfig::get('upload.allow.images');
        if ( !$aImgTypes ) return false;
        if ( !preg_match('/('.implode('|',$aImgTypes).')$/',$sName) )
            return false;

        // проверка наличий директории
        if ( !is_dir( $sThumbDir ) )
            mkdir($sThumbDir,0777);

        $oImage = new skImage();

        if(!$oImage->load($sSrcFileName))
            return false;

        $oImage->resize($iWidth, $iHeight);

        return (bool)$oImage->save($sThumbFileName);

    }

    /**
     * Возвращает имя миниатюры для файла
     * (самой миниатюры может и не быть)
     * @static
     * @param $sFileName
     * @return mixed
     */
    public static function getThumbName( $sFileName ) {

        return preg_replace(
            sprintf('/\%s([^\%1$s]+)$/',DIRECTORY_SEPARATOR),
            sprintf('/%s/$1',self::$thumbDirName),
            $sFileName
        );

    }

    /**
     * Удаление миниатюры по имени фалйа
     * @static
     * @param $sFileName
     */
    public static function deleteThumbnail( $sFileName ) {

        $sThumbName = self::getThumbName( $sFileName );

        if ( file_exists($sThumbName) ) {
            unlink( $sThumbName );
        }

    }

}
