<?php
/**
 * Класс для работы с набором файлов раздела
 *
 * @class: FilesAdmModule
 *
 * @Author: sapozhkov, $Author: sapozhkov $
 * @version: $Revision: 1316 $
 * @date: $Date: 2012-11-27 18:31:02 +0400 (Вт., 27 нояб. 2012) $
 *
 */

class FilesAdmModule extends AdminSectionTabModulePrototype {

    // id текущего раздела
    protected $iSectionId = 0;

    // возможность выбирать файлы
    protected $bCanSelect = false;

    // набор сообщений
    protected $aSysMessages = array();

    /**
     * @var \ExtList
     */
    protected $sListBuilderClass = 'ExtList';

    /**
     * Метод, выполняемый перед action меодом
     * @throws ModuleAdminErrorException
     * @return bool
     */
    protected function preExecute() {

        // id текущего раздела
        $this->iSectionId = $this->getInt('sectionId');

        // проверить права доступа
        if ( $this->iSectionId and !CurrentAdmin::canRead($this->iSectionId) )
            throw new ModuleAdminErrorException( 'accessDenied' );

        // составление набора допустимых расширений для изображений
        $this->aImgExt = skConfig::get('upload.allow.images');

        $this->aSysMessages = array();

    }

    /**
     * @var string - Имя модуля
     */
    protected $sTabName = 'Файлы';

    /**
     * Отдает id директории для записи
     * @return int
     */
    protected function getSectionId() {
        return $this->iSectionId;
    }

    /**
     * Первичная загрузка
     */
    protected function actionInit() {

        $this->actionList();

    }

    /**
     * Загрузка списка
     */
    protected function actionList() {

        $this->setPanelName( 'Список файлов' );

        // запрос файлов раздела
        $aItems = FilesAdmApi::getFiles($this->getSectionId());

        if ( $this->hasImages($aItems) ) {
            $this->actionPreviewList( $aItems );
        } else {
            $this->actionSimpleList( $aItems );
        }

        // обработка сообщений
        if ( isset( $this->aSysMessages['loadResult'] ) ) {
            $iTotal = $this->aSysMessages['loadResult']['total'];
            $iLoaded = $this->aSysMessages['loadResult']['loaded'];

            if ( !$iTotal ) {
                $this->addError( 'Ошибка загрузки данных' );
            } else {

                if ( $iLoaded )
                    $this->addMessage( sprintf('Загружено %d из %d', $iLoaded, $iTotal) );
                else
                    $this->addError( 'Ни одного файла не загружено!' );

            }

        }

    }

    /**
     * Отображение обычного списка фалов
     * @param array $aItems набор файлов
     */
    private function actionSimpleList( $aItems ) {

        /**
         * объект для построения списка (может быть перекрыт)
         * @var \ExtList $oList
         */
        $oList = new $this->sListBuilderClass();

        /**
         * Модель данных
         */

        // фильтр по полям
        $aFieldFilter = FilesAdmApi::getListFields();

        // описания полей
        $aListModel = FilesAdmApi::getFullParamDefList( $aFieldFilter );

        // задать модель данных для вывода
        $oList->setFields( $aListModel );

        /**
         * Данные
         */

        // добавление набора данных
        $oList->setValues($aItems);

        /**
         * Интерфейс
         */

        // сортировка
        $oList->addSorter('ext');
        $oList->addSorter('name');

        // группировка
        $oList->setGroupField('ext');

        // добавление кнопок
        $this->addListButtons( $oList );

        // кнопка в строке удаления
        $oList->addRowBtnDelete();

        // вывод данных в интерфейс
        $this->setExtInterface( $oList );

    }

    /**
     * Добавление кнопок к интерфейсу "список"
     * @param ExtPrototype $oList
     */
    private function addListButtons( $oList ) {

        // кнопка выбора файла, если доступна
        if ( $this->bCanSelect ) {
            $oList->addDockedItem(array(
                'text' => 'Выбрать',
                'iconCls' => 'icon-commit',
                'state' => 'selectFile'
            ));
            $oList->addBntSeparator();

        }

        // кнопка добавления
        $oList->addDockedItem(array(
            'text' => 'Загрузить',
            'iconCls' => 'icon-add',
            'action' => 'addForm',
        ));

    }

    /**
     * Отображение списка фалов с миниатюрами
     * @param array $aItems набор файлов
     */
    private function actionPreviewList( $aItems ) {

        // объект для построения списка
        $oIface = new ExtUserFile( 'FileBrowserImages' );

        // Добавление библиотек для работы
        $this->addLibClass( 'FileImageListView' );

        // добавление миниатюр в спиок файллов
        $aItems = $this->makePreviewListArray( $aItems );

        // сортировка файлов по имени
        usort( $aItems, array( $this, 'sortFIlesByName' ) );

        // добавление кнопок
        $this->addListButtons( $oIface );

        // добавление кнопки удаления
        $oIface->addBntSeparator('->');
        $oIface->addDockedItem(array(
            'text' => '_del',
            'iconCls' => 'icon-delete',
            'state' => 'delete',
            'action' => ''
        ));

        // добавляем css файл для
        $this->addCssFile('files.css');

        // задать команду для обработки
        $this->setCmd('load_list');

        // задать список файлов
        $this->setData('files',$aItems);

        // вывод данных в интерфейс
        $this->setExtInterface( $oIface );

    }

    /**
     * Подготавливает список файлов для в виде миниатюр
     * @param array $aItems входной список фалйов
     * @return array
     */
    private function makePreviewListArray($aItems){

        $aOut = array();

        // перебор файлов
        foreach ( $aItems as $aItem ) {

            // флаг наличия миниатюры
            $bThumb = false;

            // если изображение
            if ( $this->isImage($aItem) ) {

                // и есть миниатюра
                $sThumbName = FilesAdmApi::getThumbName( $aItem['webPathShort'] );
                if ( file_exists(ROOTPATH.$sThumbName) ) {
                    $aItem['preview'] = $sThumbName;
                    $aItem['thumb'] = 1;
                    $bThumb = true;
                }
            }

            // если нет миниатюры
            if ( !$bThumb ) {
                $aItem['preview'] = $this->getModuleWebDir().'/img/file.png';
                $aItem['thumb'] = 0;
            }
            $aOut[] = $aItem;
        }

        return $aOut;

    }

    /**
     * сортировка файлов по имени
     * @param array $a1 первый элемент
     * @param array $a2 второй элемент
     * @return int
     */
    protected function sortFIlesByName( $a1, $a2 ) {

        // заначения
        $s1 = $a1['name'];
        $s2 = $a2['name'];

        // сравнение
        if ($s1 == $s2)
            return 0;
        return ($s1 < $s2) ? -1 : 1;

    }

    /**
     * Удаляет файл
     * @throws ModuleAdminErrorException
     */
    protected function actionDelete() {

        // запросить данные
        $aData = $this->get( 'data' );

        // удаление одного файла
        if ( $aData ) {

            // проверка наличия данных
            if ( !is_array($aData) or !isset($aData['name']) or !$aData['name'] )
                throw new ModuleAdminErrorException('badData');

            // удалить файл
            $bRes = FilesAdmApi::deleteFile( $this->getSectionId(), $aData['name'] );

            if ( $bRes ) {
                skLogger::addNoticeReport('Удаление файла', skLogger::buildDescription($aData), skLogger::logUsers, $this->getModuleName());
                $this->addMessage( 'Файл удален.' );
            } else {
                $this->addError( 'Файл не удален!' );
            }


        } else {

            $aData = $this->get( 'delItems' );

            // проверка наличия данных
            if ( !is_array($aData) or !$aData )
                throw new ModuleAdminErrorException('badData');

            // счетчики
            $iTotal = count($aData);
            $iCnt = 0;

            // удаление файлов
            foreach ( $aData as $sFileName ) {
                // удалить файл
                $iCnt += (int)(bool)FilesAdmApi::deleteFile( $this->getSectionId(), $sFileName );
            }

            // сообщения
            if ( $iCnt ) {
                $this->addMessage( sprintf('Удалено файлов: %d из %d', $iCnt, $iTotal) );
                skLogger::addNoticeReport('Удаление файлов', array('section'=>$this->getSectionId(),'files'=>$aData), skLogger::logUsers, $this->getModuleName());
            } else {
                $this->addError( 'Ни одного файла не удалено!' );
            }

        }

        // показать список
        $this->actionList();

    }

    /**
     * Отображает форму добавления
     */
    protected function actionAddForm() {

        // объект для построения списка
        $oIface = new ExtUserFile( 'FileAddForm' );

        $this->setPanelName( 'Загрузка файлов' );

        // добавить кнопки
        // загрузка
        $oIface->addDockedItem(array(
            'text' => 'Загрузить',
            'iconCls' => 'icon-commit',
            'state' => 'upload'
        ));
//        $oIface->addBntSeparator();
//        // еще один файл
//        $oIface->addDockedItem(array(
//            'text' => 'Добавить поле',
//            'iconCls' => 'icon-add',
//            'state' => 'addField'
//        ));
        $oIface->addBntSeparator();
        // отмена
        $oIface->addBntCancel();

        // вывод данных в интерфейс
        $this->setExtInterface( $oIface );

    }

    /**
     * Установка служебных данных
     * @param \ExtPrototype $oExtIface
     */
    protected function setServiceData( ExtPrototype $oExtIface ) {

        // установить заголовок модуля
        $oExtIface->setTitle( $this->sTabName );

        // установить данные для передачи интерфейсу
        $oExtIface->setServiceData( array(
            'sectionId' => $this->getSectionId()
        ) );

    }

    protected function actionUpload() {

        // отдать правильный заголовок
        header('Content-Type: text/html');

        // загрузить файлы
        $aRes = FilesAdmApi::uploadFiles( $this->getSectionId() );

        skLogger::addNoticeReport('Загрузка файла',skLogger::buildDescription($aRes),skLogger::logUsers,$this->getModuleName());

        $this->aSysMessages['loadResult'] = $aRes;

        $this->setData('loadedFiles',$aRes['files']);

        // вызвать состояние "список"
        $this->actionList();

    }

    /** @var array список расширений, считающихся картинками */
    private $aImgExt = array();

    /**
     * Определяет, относится ли расширение к картинкам
     * @param string|array $mExt расширение файла/описание файла
     * @return bool
     */
    private function isImage( $mExt ){
        return in_array(is_array($mExt)?$mExt['ext']:$mExt, $this->aImgExt);
    }

    /**
     * Вычисляет, есть ли среди списка файлов картинки
     * @param $aItems
     * @return bool
     */
    private function hasImages( $aItems ) {

        // перебор записей
        foreach ( $aItems as $aItem ) {
            // есть найдена картинка
            if ( $this->isImage($aItem) )
                return true;
        }

        // нет картинок
        return false;

    }

}
