<?php
/**
 *
 *
 * @class: Files2ExtListModule
 *
 * @Author: User, $Author: sapozhkov $
 * @version: $Revision: 1316 $
 * @date: $Date: 2012-11-27 18:31:02 +0400 (Вт., 27 нояб. 2012) $
 *
 */

class Files2ExtListAdmModule extends ExtList {

    /**
     * Возвращает имя компонента
     * @return string
     */
    public function getComponentName() {
        return '';
    }

    /**
     * Собирает интерфейсный массив для выдачи в JS
     * @return array
     */
    public function getInterfaceArray() {

        // забпосить результат работы родительской функции
        $aValues = parent::getInterfaceArray();

        // убрать имя стандартного компонента
        unset( $aValues['extComponent'] );

        // добавить имя специфического компонента
        $aValues['componentName'] = 'FileBrowserFiles';

        // добавить стандартный компонент как подключенный модуль,
        //  поскольку он будет использоваться
        $this->addComponent('List');

        // вернуть результирующий массив
        return $aValues;

    }

}
