<?php
/**
 *
 * @class EditorAdmModule
 *
 * @author ArmiT, $Author: acat $
 * @version $Revision: 1466 $
 * @date $Date: 2012-12-20 19:13:34 +0400 (Чт., 20 дек. 2012) $
 *
 */
class EditorAdmModule extends AdminSectionTabModulePrototype {

    /** @var string Имя модуля */
    protected $sTabName = 'Редактор';

    /** id текущего раздела */
    protected $iSectionId = 0;

    /**
     * Метод, выполняемый перед action меодом
     * @throws ModuleAdminErrorException
     * @return bool
     */
    protected function preExecute() {

        // id текущего раздела
        $this->iSectionId = $this->getInt('sectionId');

        // проверить права доступа
        if ( !CurrentAdmin::canRead($this->iSectionId) )
            throw new ModuleAdminErrorException( 'accessDenied' );

    }

    /**
     * Установка служебных данных
     * @param \ExtPrototype $oExtIface
     */
    protected function setServiceData( ExtPrototype $oExtIface ) {

        // установить заголовок модуля
        $oExtIface->setTitle( $this->sTabName );

        // установить данные для передачи интерфейсу
        $oExtIface->setServiceData( array(
            'sectionId' => $this->iSectionId
        ) );

    }

    /**
     * Инициализация
     * @return bool
     */
    public function actionInit() {

        return $this->actionLoadItems();

    }

    /**
     * Запрос доступных параметров
     * @param string $sMode
     * @return array
     */
    protected function getAvailItems( $sMode = 'full' ){

        // набор полей
        $aFields = $sMode === 'short' ?
            array('id','access_level') :
            array('id','title','value','show_val','access_level')
        ;

        // запросить все значимые параметры раздела без наследования
        $aFilter = array(
            'fields' => $aFields,
            'with_access_level' => true
        );

        return Parameters::getAllAsList( $this->iSectionId, $aFilter, false );

    }

    /**
     * Сохранение данных
     * @return bool
     */
    public function actionSave(){

        // запросить набор редакторов для раздела
        $aNowItems = $this->getAvailItems( 'short' );

        $aSaveItems = $this->get('data',array());

        // данные по типам
        $aTypes = Parameters::getParamTypes();

        // результат операции
        $bAllRes = false;

        // сохранить в каждый параметр то, что пришло
        foreach ( $aNowItems as $aItem  ) {

            // если есть такой тип
            $iAbsAccessLevel = abs($aItem['access_level']);
            if ( isset($aTypes[$iAbsAccessLevel]) ) {

                // тип
                $aType = $aTypes[$iAbsAccessLevel];

                // собрать имя пришедшего параметра
                $sName = 'field_'.$aItem['id'];

                // если есть такое поле в массиве на сохранение
                if ( isset( $aSaveItems[$sName] ) ) {

                    // для wyswyg сделать оборачивание картинок с размерами
                    if ( $aItem['access_level'] == Parameters::paramWyswyg ) {
                        $aSaveItems[$sName] = ImageResize::wrapTags($aSaveItems[$sName], $this->iSectionId );
                    }

                    // сохранить
                    $bOneRes = Parameters::saveParameter( array(
                        'id' => $aItem['id'],
                        $aType['val'] => $aSaveItems[$sName]
                    ) );

                    // что-то, да сохранено
                    if ( $bOneRes) $bAllRes = true;

                }

            }

        }

        if ( $bAllRes ) Tree::updModifyDate( $this->iSectionId );

        SEOData::saveJSData( 'section', $this->iSectionId, $aSaveItems );

        $this->fireEvent('EditorAdmModule:save');

        // добавить в лог сообщение о редактировании
        skLogger::addNoticeReport("Редактирование текста раздела",skLogger::buildDescription(array('Результат сохранения' => $bAllRes)),skLogger::logUsers,$this->getModuleName());





        // запросить контент страницы
        $aContent = Parameters::getByName( $this->iSectionId, '.', 'staticContent');
        $sTitle = Tree::getTitleById( $this->iSectionId );

        // добавить в поисковый индекс, если есть данные
        if ( isset($aContent['show_val']) )
            self::addToIndex($sTitle, $aContent['show_val'], $this->iSectionId );

        // положить в ответ результат сохранения
        $this->setData('saveResult',$bAllRes);

        // отдать текущее состояние всех элементов
        return $this->actionLoadItems();

    }

    /**
     * Загрузка списка
     * @return bool
     */
    public function actionLoadItems() {

        ft::init();

        // набор полей текущего раздела
        $aItems = $this->getAvailItems( 'full' );

        // сортировка полей
        $aItems = $this->sortItems( $aItems );

        // данные по типам
        $aTypes = Parameters::getParamTypes();

        // создание модели
        $aFields = array();

        // собрать список
        foreach ( $aItems as $aItem  ) {
            // если есть такой тип
            $iAbsAccessLevel = abs($aItem['access_level']);
            if ( isset($aTypes[$iAbsAccessLevel]) ) {

                // тип данных
                $aType = $aTypes[$iAbsAccessLevel];

                // имя поля
                $sFieldName = 'field_' . $aItem['id'];

                $oField = new ExtField();

                $oField->setName($sFieldName);
                $oField->setView($aType['type']);
                $oField->setTitle($aItem['title']);
                $oField->setValue( $aItem[ $aType['val'] ] );

                $this->modifyField( $oField, $aItem );

                $aFields[] = $oField;

            }
        }

        // группировка полей
        $aFields = $this->groupItems( $aFields, $aItems );

        $aFields = $this->addAdditionalComponents( $aFields );

        $oForm = new ExtForm();

        $oForm->setFields( $aFields );
        $oForm->useSpecSectionForImages( $this->iSectionId );

        $oForm->addBntSave();
        $oForm->addBntCancel();

        // вывод данных в интерфейс
        $this->setExtInterface( $oForm );

    }

    /**
     * Добавление дополнительных элементов
     * @param $aOut
     * @return array
     */
    protected function addAdditionalComponents( $aOut ) {

        // если есть дополнительные компоненты
        if ( $this->hasAddComponenst( $this->iSectionId ) ) {

            // добавить сслку
            $oHrefFIeld = $this->getHrefField();
            if ( $oHrefFIeld )
                array_unshift( $aOut, $oHrefFIeld );

            // добавление блока SEO данных
            $aOut[] = SEOData::getJSFields( 'section', $this->iSectionId );
            $this->addLibClass( 'SEOFields', 'adm', 'SEO' );

        }

        return $aOut;

    }

    /**
     * Собирает набор элементов для редактирования
     * @param $aItems
     * @return array
     */
    protected function sortItems($aItems) {

        $iSectionId = $this->getInt('sectionId');

        $aOldItems = $aItems;

        /* Сортируем поля в редакторе */
        $aOrderConditions = Parameters::getByName( $iSectionId, '.', 'field_order' );

        if(count($aOrderConditions)){

            $aOrderConditions = preg_replace('/\x0a+|\x0d+/Uimse', '', $aOrderConditions['show_val']);
            $aOrderConditions = explode(';', trim($aOrderConditions));
            $aOrderConditions = array_diff($aOrderConditions, array(''));

            $aItems = array();

            if(count($aOrderConditions))
                foreach($aOrderConditions as $sCondition){
                    list($sGroup, $sParamName) = explode(':',$sCondition);
                    foreach($aOldItems as $iKey=>$aItem) {

                        if($aItem['group'] == $sGroup && $aItem['name'] == $sParamName) {
                            $aItems[] = $aItem;
                            unSet($aOldItems[$iKey]);
                        }
                    }
                }
            $aItems = array_merge( $aItems, $aOldItems);
        }

        return $aItems;

    }

    /**
     * Группирует поля
     * @param $aFields
     * @param $aItems
     * @return mixed
     */
    protected function groupItems( $aFields, $aItems ) {

        // собрать наименования групп
        $aGroups = array();
        foreach ( $aItems as $iKey => $aItem ) {
            $aGroups[ $aItem['group'] ][] = $iKey;
        }

        // запросить имена для групп
        $aGroupTitleList = Parameters::getAllAsList( $this->iSectionId, array('name'=>'.groupTitle') );
        foreach ( $aGroupTitleList as $aGroupTitle ) {

            // проверяем наличие полей
            $sGroupName = $aGroupTitle['group'];
            if ( !array_key_exists( $sGroupName, $aGroups ) )
                continue;

            // первый элемент заменяем
            $iFirstKey = $aGroups[$sGroupName][0];

            // собираем набор полей для группы
            $aGroupItems = array();
            foreach( $aGroups[$sGroupName] as $iOldKey ) {
                /** @var ExtField $oField  */
                $oField = $aFields[$iOldKey];
                // удаляем все кроме первой
                if ( $iOldKey !== $iFirstKey )
                    unset($aFields[$iOldKey]);
                $aGroupItems[] = ExtForm::getFieldDesc( $oField );
            }

            // создаем поле-группу на месте первого вхождения
            $aFields[$iFirstKey] = array(
                'name' => 'group_'.$sGroupName,
                'collapsed' => false,
                'type' => 'specific',
                'view' => 'specific',
                'extendLibName' => 'FieldGroup',
                'layerName' => 'sk',
                'title' => $aGroupTitle['value'],
                'group_name' => $sGroupName,
                'row_id' => '1',
                'data' => self::get($sGroupName, 1),
                'fieldConfig' => $aGroupItems
            );

        }

        return $aFields;

    }

    /**
     * Отдает массив для поля - ссылки на страницу
     * @return \ExtField|null
     */
    protected function getHrefField() {

        // формирование поля
        $oField = new ExtFieldLink();
        $oField->setName('page_href');
        $oField->setTitle('Ссылка');

        // запросить запись раздела
        $aSection = Tree::getById( $this->iSectionId );
        if ( !$aSection )
            return null;

        // если скрыт из пути
        if ( $aSection[Tree::VISIBLE] == Tree::visibleHideFromPath )
            return null;

        if ( $aSection[Tree::LINK] ) {
            // если есть ссылка - взять её
            $sHref = $aSection[Tree::LINK];
        } else {
            // нет - просто сформировать ссылку
            $sHref = skRouter::rewriteURL( "[$this->iSectionId]" );
        }

        // составить текст ссылки
        $sText = $sHref==='/' ? 'главная' : $sHref;

        // добавить параметры
        $oField->setLink($sText,$sHref);

        return $oField;

    }

    /**
     * Возвращает флаг наличия дополнительных элементов
     * @param int $iSectionId
     * @return bool
     */
    protected function hasAddComponenst( $iSectionId ) {

        // данные раздела
        $aSection = Tree::getById( $iSectionId );
        if ( !$aSection )
            return false;

        // для исключенных из пути не выводим
        if ( $aSection[Tree::VISIBLE] == Tree::visibleHideFromPath )
            return false;

        // для разделов со ссылками не выводим
        if ( $aSection[Tree::LINK] )
            return false;

        // id родительского раздела
        $iParentSectionId = $aSection[Tree::PARENT];

        // если не наследники подчиненных/корневого раздела
        $bIsSysSection = in_array( $iParentSectionId, array(
            skConfig::get( 'section.root' ),
            skConfig::get( 'section.templates' ),
            skConfig::get( 'section.library' )
        ) );

        return !$bIsSysSection;

    }

    public static function addToIndex($sTitle, $sText, $iObjectId ){

        $sText = strip_tags($sText);
        $sHref = $_SERVER['HTTP_HOST'].skRouter::rewriteURL('['.$iObjectId.']');
        return Search::addToIndex($sTitle, $sText, $iObjectId, 'Page', $iObjectId, $sHref);
    }

    public static function removeFromIndex($iItemId){

        if ( !$iItemId ) return false;

        return Search::removeFromIndex($iItemId, 'Page');
    }

    /**
     * Дополнение поля параметрами, если требуется
     * @param ExtField $oField
     * @param array $aItem
     */
    private function modifyField( ExtField $oField, $aItem ) {

        switch ( abs($aItem['access_level']) ) {

            // обработка wyswyg
            case Parameters::paramWyswyg:

                // отменить оборачивание картинок с размерами
                $oField->setValue( ImageResize::restoreTags($oField->getValue()) );

                // стилевой класс для body в wysiwyg
                $addCssClassName = 'b-editor b-wyswyg';

                // извлекаем дополнительные параметры висивига из поля value
                $aWysiwygParams = Parameters::extractWysiwygParams($aItem['value']);

                if($aWysiwygParams['height']) {
                    $iHeight = $aWysiwygParams['height'];
                    $oField->setDescVal('height', $iHeight );
                } else $iHeight = 300;
                if($aWysiwygParams['cssClass'])
                    $addCssClassName = $aWysiwygParams['cssClass'];

                $addCssFileName = skConfig::get('buildConfig.Page.css.default.default');

                // спец стилевик
                $oField->setDescVal('addConfig', array(
                    'height' => $iHeight-100,
                    'contentsCss' => array(
                        $addCssFileName,
                        '/skewer_build/cms/modules/Frame/css/wyswyg.css'
                    ),
                    'bodyClass' => $addCssClassName
                ));

                break;

            // обработка полей типа "текст"
            case Parameters::paramText:

                $iVal = (int)$aItem['value'];
                if ( $iVal )
                    $oField->setDescVal('height',$iVal);

                break;
        }

    }


}
