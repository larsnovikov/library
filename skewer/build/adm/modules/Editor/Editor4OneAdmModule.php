<?php
/**
 * Редактор для единственного параметра
 */
class Editor4OneAdmModule extends EditorAdmModule {

    private $sEditorId = '';

    /**
     * Метод, выполняемый перед action меодом
     * @return bool
     */
    protected function preExecute() {
        parent::preExecute();
        $this->sEditorId = $this->getStr('editorId');
    }

    /**
     * Установка служебных данных
     * @param \ExtPrototype $oExtIface
     */
    protected function setServiceData( ExtPrototype $oExtIface ) {

        parent::setServiceData( $oExtIface );

        // расширение массива сервисных данных
        $aData = $oExtIface->getServiceData();
        $aData['editorId'] = $this->sEditorId;
        $oExtIface->setServiceData( $aData );

    }

    /**
     * Запрос доступных параметров
     * @param string $sMode
     * @throws ModuleAdminErrorException
     * @return array
     */
    protected function getAvailItems( $sMode = 'full' ){

        // восстанавливаем id параметра по его имени "группа/имя"
        $aParamData = explode('/', $this->sEditorId);

        if(count($aParamData)>1){
            $oParam = Parameters::getByName( $this->iSectionId, $aParamData[0], $aParamData[1], true );
            if(isset($oParam[Tree::ID])) {

                $this->iSectionId = $oParam[Tree::PARENT];

                if(!CurrentAdmin::canRead($oParam[Tree::PARENT]))
                    throw new ModuleAdminErrorException('Доступ запрещен');

                return array( $oParam );

            }
        }
        return array();
    }

    /**
     * Сохранение данных
     * @return bool
     */
    public function actionSave(){
        $this->fireJSEvent('reload_display_form');
        parent::actionSave();
    }

    /**
     * Собирает набор элементов для редактирования
     * @param $aItems
     * @return array
     */
    protected function sortItems($aItems) {
        return $aItems;
    }

    /**
     * Добавление дополнительных элементов
     * @param $aOut
     * @return array
     */
    protected function addAdditionalComponents( $aOut ) {
        return $aOut;
    }

}
