<?php
/**
 * @class GuestBookAdmModule
 * @extends AdminModulePrototype
 * @project Skewer
 * @package modules
 *
 * @author kolesnikov, $Author: $
 * @version $Revision: $
 * @date $Date: $
 *
 */
class GuestBookAdmModule extends AdminSectionTabModulePrototype {


    // id текущего раздела
    protected $iSectionId = 0;

    /**
     * @var string - Имя модуля
     */
    protected $sTabName = 'Отзывы';

    // число элементов на страницу
    public  $onAdmPage = 20;

    // текущий номер страницы ( с 0, а приходит с 1 )
    protected $iPage = 0;

    protected $iStatusFilter = GuestBookMapper::statusNew;

    /**
     * Метод, выполняемый перед action меодом
     * @throws ModuleAdminErrorException
     * @return bool
     */
    protected function preExecute() {

        // номер страницы
        $this->iPage = $this->getInt('page');

        // id текущего раздела
        $this->iSectionId = $this->getInt('sectionId');

        $this->iStatusFilter = $this->get('fStatus', GuestBookMapper::statusNew);

        // проверить права доступа
        if ( !CurrentAdmin::canRead($this->iSectionId) )
            throw new ModuleAdminErrorException( 'accessDenied' );

    }

    /**
     * Установка служебных данных
     * @param \ExtPrototype $oExtIface
     */
    protected function setServiceData( ExtPrototype $oExtIface ) {

        // установить заголовок модуля
        $oExtIface->setTitle( $this->sTabName );

        // установить данные для передачи интерфейсу
        $oExtIface->setServiceData( array(
            'sectionId' => $this->iSectionId,
            'page' => $this->iPage,
            'fStatus' => $this->iStatusFilter
        ) );

    }


    public function actionInit(){

        // объект для построения списка
        $oList = new ExtList();

        $aModel = GuestBookAdmApi::getModel( GuestBookAdmApi::getListFields() );

        // добавляем фильтр по пользователям
        $oList->addFilterSelect( 'fStatus', GuestBookAdmApi::getStatusList(), $this->iStatusFilter, 'Статус' );

        // задать модель данных для вывода
        $oList->setFields( $aModel );

        // число записей на страницу
        $oList->setOnPage( $this->onAdmPage );
        $oList->setPageNum($this->iPage);

        // добавление набора данных
        $aItems = GuestBookAdmApi::getItems( $this->iSectionId, $this->iStatusFilter, $this->iPage, $this->onAdmPage );

        $oList->setValues( $aItems['items'] );
        $oList->setTotal( $aItems['count'] );

        $oList->addRowBtnUpdate();
        $oList->addRowBtnDelete();

        // кнопка добавления
        $oList->addBntAdd('show');

        // вывод данных в интерфейс
        $this->setExtInterface( $oList );
    }

    /**
     * Отображение формы
     */
    protected function actionShow() {

        // подключить автоматический генератор форм
        $oForm = new ExtForm();

        // номер новости
        $aData = $this->get('data');
        $iItemId = (is_array($aData) && isset($aData['id'])) ? (int)$aData['id'] : 0;

        // запись новости
        $aItem = $iItemId ? GuestBookAdmApi::getItemById( $iItemId ) : array();

        $aModel = GuestBookAdmApi::getModel(GuestBookAdmApi::getDetailFields());

        // установить набор элементов формы
        $oForm->setFields( $aModel );

        // установить значения для элементов
        $oForm->setValues( $aItem );

        // добавление кнопок
        $oForm->addBntSave();
        $oForm->addBntCancel();

        // если существующая запись
        if ( $aItem ) {

            // кнопки модерации
            if ( $aItem['status'] == GuestBookMapper::statusNew ) {

                $oForm->addBntSeparator('-');
                $oForm->addDockedItem(array(
                    'text' => 'Одобрить',
                    'iconCls' => 'icon-commit',
                    'action' => 'Save',
                    'addParams' => array( 'status' => GuestBookMapper::statusApproved ),
                ));
                $oForm->addDockedItem(array(
                    'text' => 'Отклонить',
                    'iconCls' => 'icon-stop',
                    'action' => 'Save',
                    'addParams' => array( 'status' => GuestBookMapper::statusRejected ),
                ));

            }

            // кнопка удаления
            $oForm->addBntSeparator('->');
            $oForm->addBntDelete();

        }

        // вывод данных в интерфейс
        $this->setExtInterface( $oForm );

    }

    /**
     * Сохранение новости
     */
    protected function actionSave() {

        // запросить данные
        $aData = $this->get( 'data' );

        // перекрытие статуса
        $iStatus = $this->get( 'status', null );
        if ( !is_null( $iStatus ) )
            $aData['status'] = $iStatus;

        if( !$aData['date_time'] )
            $aData['date_time'] = date('Y-m-d H:i:s');

        if( !$aData['parent'] )
            $aData['parent'] = $this->iSectionId;

        // есть данные - сохранить
        if ( $aData ){

            // сохранение данных
            GuestBookMapper::saveItem( $aData);

        }

        // вывод списка
        $this->actionInit();

    }

    /**
     * Удаляет запись
     */
    protected function actionDelete() {

        // запросить данные
        $aData = $this->get( 'data' );

        // id записи
        $iItemId = ( is_array($aData) and isset($aData['id']) ) ? (int)$aData['id'] : 0;

        // удаление
        GuestBookAdmApi::delItem( $iItemId );

        //$this->addNoticeReport("Удаление новости",$aData);

        // вывод списка
        $this->actionInit();

    }

}
