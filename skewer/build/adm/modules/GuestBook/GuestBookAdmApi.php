<?php
/**
 * Created by JetBrains PhpStorm.
 * User: user
 * Date: 27.07.12
 * Time: 16:35
 * To change this template use File | Settings | File Templates.
 */
class GuestBookAdmApi {

    /**
     * Отдает набор доступных статусов
     * @return array
     */
    public static function getStatusList() {
        return array(
            GuestBookMapper::statusNew => 'новый',
            GuestBookMapper::statusApproved => 'одобрен',
            GuestBookMapper::statusRejected => 'отклонен'
        );
    }

    /**
     * @static Возвращает набор полей для отображения в списке
     * @return array
     */
    public static function getListFields(){
        return array(
            'id',
            'date_time',
            'name',
            'content',
            'status'
        );
    }

    public static function getDetailFields(){
        return array(
            'id',
            'parent',
            'date_time',
            'name',
            'email',
            'status',
            'city',
            'content',
        );
    }

    public static function getModel($aFields){

        $aModel = GuestBookMapper::getFullParamDefList($aFields, self::getAddModelParams() );

        return $aModel;
    }

    /**
     * Отдает дополнительный расширяющий массив
     * @return array
     */
    protected static function getAddModelParams() {

        $aModel = array(
            'id' => array(
                'listColumns' => array('hidden' => true)
            ),
            'date_time' => array(
                'listColumns' => array('width' => 120)
            ),
            'name' => array(
                'listColumns' => array('width' => 150)
            ),
            'content' => array(
                'grow' => true,
                'growMax' => 500,
                'listColumns' => array('flex' => 1)
            ),
            'status' => array(
                'listColumns' => array('width' => 60),
            )
        );

        $aModel['status'] = array_merge(
            $aModel['status'],
            ExtForm::getDesc4SelectFromArray( self::getStatusList() )
        );

        return $aModel;

    }


    public static function delItem($iItemId){

        GuestBookMapper::delItem($iItemId);

        return true;

    }

    /**
     * Отдает набор записей
     * @param int $iSection id раздела
     * @param bool|int $mStatus фильтр-статус
     * @param int $iPage номер страницы
     * @param int $iOnPage количество записей на страницу
     * @return array
     */
    public static function getItems($iSection, $mStatus, $iPage, $iOnPage){

        // добавление набора данных
        $aFilter = array(
            'select_fields' => GuestBookAdmApi::getListFields(),
            'limit' => array (
                'start' => $iPage*$iOnPage,
                'count' => $iOnPage
            ),
            'order' => array(
                'field' => 'date_time',
                'way' => 'DESC'
            )
        );

        // фильтр по разделу
        if($iSection){
            $aFilter['where_condition'] = array(
                'parent' => array(
                    'sign' => '=',
                    'value' => $iSection
                )
            );
        }

        // фильтр по статусы
        if ( is_numeric($mStatus) ) {
            $aFilter['where_condition'] = array(
                'status' => array(
                    'sign' => '=',
                    'value' => $mStatus
                )
            );
        }

        // запрос данных
        $aItems = GuestBookMapper::getItems($aFilter);

        // расстановка статусов
        $aStatus = self::getStatusList();
        if($aItems['count']) {
            foreach($aItems['items'] as $iKey=>$aItem){
                $aItems['items'][$iKey]['status'] = isset( $aStatus[$aItem['status']] ) ?
                    $aStatus[$aItem['status']] : '---';
            }
        }

        return $aItems;
    }


    public static function getItemById($iItemId){

        $aData = GuestBookMapper::getItem($iItemId);

        return $aData;
    }

}
