<?php
/**
 * API работы с админской частью галереи
 */
class GalleryAdmApi {

    /**
     * id формата для копа
     */
    const sCropFormatName = 'crop_min';

    /**
     * Высота элемента для кропа
     */
    const cropHeight = 400;

    /**
     * Загружает изображения и перемещает в целевую директорию
     * @static
     * @param $iSectionId
     * @return bool|string
     * @throws Exception
     */
    public static function uploadFile( $iSectionId ) {

        // параметры загружаемого файла
        $aFilter = array();
        $aFilter['size']            = skConfig::get('upload.maxsize');
        $aFilter['allowExtensions'] = skConfig::get('upload.allow.images');
        $aFilter['imgMaxWidth']     = skConfig::get('upload.images.maxWidth');
        $aFilter['imgMaxHeight']    = skConfig::get('upload.images.maxHeight');

        // загрузка
        $oFiles = skUploadedFiles::get($aFilter, FILEPATH, PRIVATE_FILEPATH);

        // если ни один файл не загружен - выйти
        if(!$oFiles->count())
            throw new Exception ($oFiles->getError());

        /*
         * пока берется только один файл, но делался задел на загрузку
         * большего количества
         */

        // имя файла исходника
        $sSourceFN = array();

        /** @noinspection PhpUnusedLocalVariableInspection */
        foreach($oFiles  as  $file) {

            // проверка ошибок
            if($sError = $oFiles->getError())
                throw new Exception ($sError);

            /* @todo Должна быть добавлена проверка на защиту директории (protected: true/false) */
            $sNewFile = $oFiles->UploadToSection($iSectionId, 'gallery/sources/', false);

            // отрезать корневую папку от пути
            $sNewFile = substr( $sNewFile, strlen(ROOTPATH)-1 );

            $sSourceFN[] = $sNewFile;

        }

        // проверка наличия имени файла
        if( !count($sSourceFN) )
            throw new Exception("Изображение не было загружено!");


        return ( count($sSourceFN) == 1 ) ? $sSourceFN[0] : $sSourceFN;
    }

    /**
     * Создает миниатюру для режима обрезки
     * @static
     * @param string $sSourceFN имя исходного файла
     * @param int $iSectionId if раздела
     * @throws Exception
     * @return array
     */
    public static function createCropMin( $sSourceFN, $iSectionId ) {

        $bProtected = false;
        $sSourceFN = ROOTPATH.$sSourceFN;

        /* Путь к корневой директории галереи в текущем разделе */
        $sImagePath = skFiles::getFilePath( $iSectionId, Gallery2ImagesAdmApi::getModuleDirName() );

        /* Загрузка исходного изображения для дальнейшей обработки */
        $oImage = new skImage();

        // загрузить изображение
        if(!$oImage->load($sSourceFN))
            throw new Exception('Crop. Image processing error: Image not loaded!');

        // привести высоту к приемлемому размеру
        $iHeight = min( $oImage->getSrcHeight(), self::cropHeight );

        // изменить размер
        $oImage->resize(0, $iHeight);

        $sSavedFile = skFiles::generateUniqFileName($sImagePath.self::sCropFormatName.DIRECTORY_SEPARATOR, basename($sSourceFN));

        $sSavedFile = str_replace(skFiles::getRootUploadPath($bProtected), '', $sSavedFile);

        $sDir = skFiles::createFolderPath(dirname($sSavedFile), $bProtected);

        if(!$sDir) throw new Exception('Crop. Image processing error: Directory is not created!');

        $sNewFilePath = $sDir.basename($sSavedFile);

        /* Сохранить измененное изображение */
        if(!$oImage->save($sNewFilePath)) throw new Exception('Crop. Image processing error: Image do not saved!');

        list($iWidth, $iHeight) = $oImage->getSize();
        return array(
            'file'   => skFiles::getWebPath($sNewFilePath, false),
            'width'  => $iWidth,
            'height' => $iHeight,
            'koef'   => $oImage->getSrcHeight()/self::cropHeight
        );

    }

    /**
     * Валидирует набор id для альбома
     * @static
     * @param $aIdList
     * @param $iAlbumId
     * @return array
     */
    public static function validateIdList( $aIdList, $iAlbumId ) {

        // набор приведенных к типу id на удаление
        $aIntIdList = array();
        foreach ( $aIdList as $mId ) {
            $iId = (int)$mId;
            if ( $iId )
                $aIntIdList[] = $iId;
        }

        // набор точно присутствующих в альбоме id
        $aValidIdList = array();

        // изображения в альбоме
        $aAlbumItems = Gallery2ImagesApi::getFromAlbum( $iAlbumId, true);
        foreach ( $aAlbumItems as $aItem ) {
            $iId = $aItem['image_id'];
            if ( in_array( $iId, $aIntIdList ) )
                $aValidIdList[] = $iId;
        }

        return $aValidIdList;

    }
    /**
     * Валидирует набор id альбомов для раздела
     * @static
     * @param array $aIdList
     * @param int $iSectionId
     * @return array
     */
    public static function validateIdAlbumsList( $aIdList, $iSectionId ) {

        // набор приведенных к типу id на удаление
        $aIntIdList = array();
        foreach ( $aIdList as $mId ) {
            $iId = (int)$mId;
            if ( $iId )
                $aIntIdList[] = $iId;
        }

        // набор точно присутствующих в разделе id альбомов
        $aValidIdList = array();

        // альбомы в разделе
        $aAlbumItems = Gallery2AlbumsApi::getBySection($iSectionId);
        $aAlbumItems = (count($aAlbumItems['items']))? $aAlbumItems['items'] : false;

        if(is_array($aAlbumItems))
        foreach ( $aAlbumItems as $aItem ) {
            $iId = $aItem['album_id'];
            if ( in_array( $iId, $aIntIdList ) )
                $aValidIdList[] = $iId;
        }

        return $aValidIdList;

    }


}
