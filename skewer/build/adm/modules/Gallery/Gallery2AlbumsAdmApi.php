<?php
/**
 * API для работы с альбомами
 * @class Gallery2AlbumsApi
 *
 * @author ArmiT, $Author: acat $
 * @version $Revision: 1343 $
 * @date $Date: 2012-11-30 17:14:11 +0400 (Пт., 30 нояб. 2012) $
 * @project Skewer
 * @package Modules
 */
class Gallery2AlbumsAdmApi {

    /**
     * Добавляет либо обновляет данные альбома. Если указан $iAlbumId,
     * то происходит обновление записи.
     * @param  array $aData Массив данных
     * @param bool|int $iAlbumId Id обновляемого альбома
     * @return bool|int Возвращет Id созданной записи, true, если запись была обновлена
     * либо false в случае ошибки.
     */
    public static function setAlbum($aData, $iAlbumId = false) {

        if($iAlbumId) $aData['album_id'] = (int)$iAlbumId;
        return Gallery2AlbumsMapper::saveItem($aData);

    }// func

    /**
     * Добавляет новый альбом
     * @param array $aData Данные альбома
     * @return bool|int Возвращет Id созданной записи
     * либо false в случае ошибки.
     */
    public static function addAlbum($aData) {

        if(isSet($aData['album_id'])) unSet($aData['album_id']);

        return self::setAlbum($aData);

    }// func


    /**
     * Добавляет новый альбом с минимальным каличеством параметров
     * @param  string $sTitle       Название альбома
     * @param string $sDescription  Описание альбома
     * @param int $iProfileId id    Профиля настроек для загрузки изображений
     * @param bool $bVisible        Выводить или нет альбом на сайт
     * @return bool|int Возвращет Id созданной записи
     * либо false в случае ошибки.
     */
    public static function simpleAddAlbum($sTitle, $sDescription, $iProfileId, $bVisible = true) {

        $aData['title']       = $sTitle;
        $aData['visible']     = (bool)$bVisible;
        $aData['profile_id']  = (int)$iProfileId;
        $aData['description'] = $sDescription;

        return self::setAlbum($aData);

    }// func

    /**
     * Обновляет данные альбома.
     * @param $iAlbumId - Id изменяемого альбома
     * @param array $aData Данные формата
     * @return bool
     */
    public static function updateAlbum($iAlbumId, $aData) {

        return self::setAlbum($aData, $iAlbumId);

    }// func

    /**
     * Обновляет данные данные альбома с минимальным количеством параметров.
     * @param int $iAlbumId     Id изменяемого альбома
     * @param $sTitle       Название альбома
     * @param $sDescription Описание альбома
     * @param int $iProfileId   Id профиля настроек для альбома
     * @param bool $bVisible Выводить альбом на сайте
     * @return bool
     */
    public static function simpleUpdateAlbum($iAlbumId, $iProfileId, $sTitle = '', $sDescription = '', $bVisible = -1) {

        if(!(int)$iAlbumId) return false;

        $aData = array();

        if($iProfileId)           $aData['profile_id']  = (int)$iProfileId;
        if(!empty($sTitle))       $aData['title']       = $sTitle;
        if($bVisible != -1)       $aData['visible']     = (bool)$bVisible;
        if(! empty($sDescription))$aData['description'] = $sDescription;

        if(!count($aData)) return false;

        return self::setAlbum($aData, $iAlbumId);

    }// func


    /**
     * Удаляет альбом $iAlbumId. Возвращает true, если удаление прошло успешно либо false вслучае ошибки.
     * Текст ошибки сохраняется в параметр $mError
     * @param int $iAlbumId Id удаляемого альбома
     * @param bool|string $mError Содержит текст ошибки либо false
     * @throws Exception
     * @return bool
     */
    public static function removeAlbum($iAlbumId, &$mError = false) {

        try {
            /* Проверка данных на валидность */
            If(!(int)$iAlbumId) throw new Exception('Ошибка: Альбом не найден!');

            /* Выбрать альбом */
            if(!$aAlbum = Gallery2AlbumsApi::getById($iAlbumId))  throw new Exception('Ошибка: Альбом не найден!');

            /* Выбрать изображения к нему */
            if($aImages = Gallery2ImagesApi::getFromAlbum($iAlbumId))
            /* Удалить изображения */
                foreach($aImages as $aImage) {

                    $mError = false;
                    if(!Gallery2ImagesAdmApi::removeImage($aImage['image_id'], $mError)){
                        throw new Exception($mError);
                    }
                }// each image in current album

            /* Удалить запись об альбоме */
            Gallery2AlbumsMapper::delItem($iAlbumId);

            /* Пересчитать зависимости */
            /** @todo дописать после введения сортировки */

        } catch (Exception $e) {

            $mError = $e->getMessage();
            return false;
        }

        return true;
    }// func

    /**
     * Изменяет Активность альбома (флаг видимости на сайте)
     * @param int $iAlbumId Id существующего формата
     * @param bool $bActive
     * @return bool
     */
    public static function changeActive($iAlbumId, $bActive = true) {

        If(!(int)$iAlbumId) return false;

        $aData['active'] = (int)$bActive;

        return self::setAlbum($aData, $iAlbumId);

    }// func

    public static function toggleActive($iAlbumId){

        if( !$iAlbumId ) return false;

        Gallery2AlbumsMapper::toggleActive($iAlbumId);

        return true;
    }


    public function orderAlbums() {}// func

    /**
     * Устанавливает владельца альбома (модуль, хранящий изображения)
     * @param int $iAlbumId Id альбома
     * @param string $sOwner Владелец альбома (название модуля)
     * @return bool
     */
    public function setOwner($iAlbumId, $sOwner) {



    }// func

    /**
     * Удаляет альбомы и изображения из раздела $iSectionId
     * @param int $iSectionId Id раздела
     * @return bool
     */
    public function removeFromSection($iSectionId) {

    }// func


    /**
     * Отдает шаблонный набор значений для добавления новой записи
     * @static
     * @return array
     */
    public static function getAlbumBlankValues() {

        return Gallery2AlbumsMapper::getBlankModel();

    }

    /**
     * Сортирует альбомы
     * @param int $iItemId id перемещаемого объекта
     * @param int $iTargetId id объекта, относительно которого идет перемещение
     * @param string $sOrderType направление переноса
     * @return bool
     */
    public static function sortAlbums($iItemId, $iTargetId, $sOrderType='before') {

        $sSortField = 'priority';

        // перемещаемое изображение
        $aAlbum = Gallery2AlbumsApi::getById($iItemId);

        // "относительное" изображение
        $aTargetAlbum = Gallery2AlbumsApi::getById($iTargetId);

        // должны быть в одном альбоме
        if( $aAlbum['section_id'] != $aTargetAlbum['section_id'] )
            return false;

        // выбираем напрвление сдвига
        if( $aAlbum[$sSortField] > $aTargetAlbum[$sSortField] ){

            $iStartPos = $aTargetAlbum[$sSortField];
            if( $sOrderType=='after' ) $iStartPos--;
            $iEndPos = $aAlbum[$sSortField];
            $iNewPos = $sOrderType=='after' ? $aTargetAlbum[$sSortField] : $aTargetAlbum[$sSortField] + 1;
            Gallery2AlbumsMapper::shiftAlbumPosition($aAlbum['section_id'], $iStartPos, $iEndPos, '+');
            Gallery2AlbumsMapper::changeAlbumPosition($aAlbum['album_id'],$iNewPos);

        } else {

            $iStartPos = $aAlbum[$sSortField];
            $iEndPos = $aTargetAlbum[$sSortField];
            if( $sOrderType=='before' ) $iEndPos++;
            $iNewPos = $sOrderType=='before' ? $aTargetAlbum[$sSortField] : $aTargetAlbum[$sSortField] - 1;
            Gallery2AlbumsMapper::shiftAlbumPosition($aAlbum['section_id'], $iStartPos, $iEndPos, '-');
            Gallery2AlbumsMapper::changeAlbumPosition($aAlbum['album_id'],$iNewPos);

        }

        return true;
    }


}
