<?php
/**
 * Класс работы с временными файлами
 *
 * id
 * name
 * value
 * add_date
 */
class Gallery2TmpAdmModule {

    /**
     * Создает запись во временной таблице
     * @static
     * @param $sName
     * @param $sValue
     * @return int
     */
    public static function create( $sName, $sValue ) {
        $sQuery = "INSERT INTO `photogallery_tmp` SET `id`=NULL, `name`=[name:s], `value`=[value:s]";
        $aData = array(
            'name' => $sName,
            'value' => $sValue,
            'add_date' => date("Y-m-d H:i:s", time())
        );
        global $odb;
        $odb->query($sQuery, $aData);
        return $odb->insert_id;
    }

    /**
     * Отдает запись по id
     * @static
     * @param $iId
     * @return array|null
     */
    public static function getById( $iId ) {
        $sQuery = "SELECT `id`, `name`, `value` FROM `photogallery_tmp` WHERE `id`=[id:i]";
        $aData = array( 'id' => $iId );
        global $odb;
        $rResult = $odb->query($sQuery,$aData);
        if ( !$rResult ) return null;
        if( $aRow = $rResult->fetch_assoc() )
            return $aRow;
        return null;
    }

    /**
     * Удаляет запись по id
     * @static
     * @param int|array $mIdList
     * @return int
     */
    public static function delById( $mIdList ) {
        if ( !$mIdList ) return 0;
        if ( is_numeric($mIdList) )
            $mIdList = array( $mIdList );
        $sQuery = "DELETE FROM `photogallery_tmp` WHERE `id` IN ([idList:q])";
        $aData = array( 'idList' => implode( ',', $mIdList ) );
        global $odb;
        $odb->query($sQuery,$aData);
        return $odb->affected_rows;
    }

    /**
     * Отдает список записей, срок давности которых превышает заданный порог
     * @static
     */
    public static function getOldRows( $iDays=2 ) {
        $sQuery = "SELECT `id`, `name`, `value` FROM `photogallery_tmp` WHERE `add_date`<[date:s]";
        $aData = array(
            'date' => date("Y-m-d H:i:s", mktime( 0, 0, 0, date('m'), date('d')-$iDays ) ),
        );
        global $odb;
        $rResult = $odb->query($sQuery,$aData);
        if ( !$rResult ) return array();
        $aOut = array();
        while ( $aRow = $rResult->fetch_assoc() )
            $aOut[] = $aRow;
        return $aOut;
    }

    /**
     * Отдает флаг разрешения запуска сборщика мусора
     * @static
     * @return bool
     */
    public static function allowStartScavenger () {
        return (bool)(rand(0,10)===5);
    }

}
