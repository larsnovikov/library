<?php

/* main */
$aConfig['name']     = 'GalleryAdm';
$aConfig['title']     = 'Галерея (админ)';
$aConfig['version']  = '1.000a';
$aConfig['description']  = 'Система администрирования фотогаллереи';
$aConfig['revision'] = '0001';
$aConfig['layer']     = 'adm';

return $aConfig;