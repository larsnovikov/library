<?php
/**
 * API для работы с изображениями
 * @class Gallery2PhotosApi
 *
 * @author ArmiT, $Author: acat $
 * @version $Revision: 1343 $
 * @date $Date: 2012-11-30 17:14:11 +0400 (Пт., 30 нояб. 2012) $
 * @project Skewer
 * @package Modules
 */
class Gallery2ImagesAdmApi {

    /**
     * Название директории для хранения thumbnails
     * @var string
     */
    protected static $sThumbnailDirectory = 'thumbnails';
    /**
     * Ширина thumbnails для списка изображений в альбоме
     * @var int
     */
    protected static $iThumbnailWidth  = 0;

    /**
     * Высота thumbnails для списка изображений в альбоме
     * @var int
     */
    protected static $iThumbnailHeight = 150;

    /**
     * Имя директории для загрузки изображенийв разделе
     * @var string
     */
    protected static $sModuleDirName = 'Gallery';

    public static function getModuleDirName() {
        return self::$sModuleDirName;
    }

    /**
     * Добавляет либо обновляет данные изображения в зависимости от набора параметров
     * @static
     * @param array $aData Данные
     * @param bool $iImageId Id Обновляемого изображения
     * @return bool|int
     */
    protected static function setImage($aData, $iImageId = false) {

        if($iImageId) $aData['image_id'] = (int)$iImageId;
        return Gallery2ImagesMapper::saveItem($aData);

    }// func

    /**
     * Добавляет данные по изображению в альбом $iAlbumId.
     * @static
     * @param int $iAlbumId Id альбома, в который происходит добавление изображения
     * @param array $aImagesData Массив с изображениями, созданными согласно форматам профиля альбома
     * @param string $sTitle Название изображения
     * @param string $sDescription Описание изображения
     * @param string $sThumbnailPath Путь к изображению миниатюры для системы администрирования
     * @param string $sSourceImage Путь к исходному изображению
     * @param bool $bVisible Флаг указывающий на необходимость вывода изображения на сайте
     * @return bool|int Возвращает Id добавленной записи либо false в случае неудачи
     */
    public static function addImage($iAlbumId, $aImagesData, $sTitle = '', $sDescription = '', $sThumbnailPath = '', $sSourceImage = '', $bVisible = true) {
        $aData = array();

        if(!(int)$iAlbumId)      return false;
        if(!count($aImagesData)) return false;

        $aData['title']       = $sTitle;
        $aData['source']      = $sSourceImage;
        $aData['visible']     = (bool)$bVisible;
        $aData['album_id']    = $iAlbumId;
        $aData['thumbnail']   = $sThumbnailPath;
        $aData['description'] = $sDescription;
        $aData['images_data'] = json_encode($aImagesData);
        $aData['priority']    = Gallery2ImagesApi::getCountByAlbum($iAlbumId) + 1;

        return self::setImage($aData);

    }// func

    /**
     * Обновляет заголовочные данные изображения $iImageId
     * @static
     * @param int $iImageId Id обновляемой записи
     * @param string $sTitle Название
     * @param string $sDescription Описание
     * @param bool $bVisible Флаг, указывающий на необходимость вывода в альбоме
     * @return bool
     */
    public static function updateImageHeader($iImageId, $sTitle, $sDescription, $bVisible) {

        if(!(int)$iImageId) return false;

        $aData['title']       = $sTitle;
        $aData['visible']     = ($bVisible)? 1: 0;
        $aData['description'] = $sDescription;

        return self::setImage($aData, $iImageId);

    } // func

    /**
     * Удаляет изображение $iImageId из альбома
     * @static
     * @param int $iImageId Id удаляемого изображения
     * @param bool|string $mError Переменная, возвращающая сообщение об ошибке случае неудачи
     * @return bool Возвращает true в случае удачного удаление записи и файлов либо false и сообщение об
     * ошибке в параметре $sError.
     * @throws Exception
     */
    public static function removeImage($iImageId, &$mError = '') {

        try {

            if(!(int)$iImageId) throw new Exception('Ошибка: Изображение не существует!');

            /* Запросить запись */
            $aImage = Gallery2ImagesApi::getImage($iImageId);

            if(!$aImage) throw new Exception('Ошибка: Изображение не существует!');

            /* Удалить исходник */
            skFiles::remove(ROOTPATH.$aImage['source']);

            /* Удалить thumbnail */
            skFiles::remove(ROOTPATH.$aImage['thumbnail']);

            /* Удалить ресайзы */
            if(!$aImages = json_decode($aImage['images_data'], true)) throw new Exception('Ошибка: Данные имеют неверный формат!');

            /* Обходим по форматам */
            foreach($aImages as $aItems){
                skFiles::remove(ROOTPATH.$aItems['file']);
            }
            /* Удалить запись */
            Gallery2ImagesMapper::delItem($iImageId);

            /* Пересчитать зависимости */

        } catch (Exception $e) {
            $mError = $e->getMessage();
            return false;
        }

        return true;
    }// func

    /**
     * Изменение видимости изображения
     * @param $iImageId
     * @return bool
     */
    public static function changeActivePhoto($iImageId){

        if( !$iImageId ) return false;

        Gallery2ImagesMapper::changeImageActive($iImageId);

        return true;
    }

    /**
     * Обрабатывает изображение $sImagePath согласно профилю настроек $iProfileId для раздела $iSectionId.
     * @static
     * @param string $sImageFile Абсолютный путь к исходному файлу изображения
     * @param integer|array $mProfileId Id профиля настроек, согласно которому будет обработано изображение
     * @param integer $iSectionId Id раздела, в который будут сохранены файлы созданных изображений
     * @param bool $bProtected Указатель на необходимость сохранять файлы в закрытую для доступа директорию
     * @param bool $bCreateAllFormat Флаг определяет создавать ли миниатюры всех доступных форматов (false - только пришедших в $mProfileId)
     * @param bool|string $mError Переменная, в которую будет возвращен текст ошибки либо false
     * @throws Exception
     * @return bool|array Возвращает массив с описанием созданных изображений либо false
     */
    public static function processImage($sImageFile, $mProfileId, $iSectionId, $bProtected = false, $bCreateAllFormat = true, &$mError = false) {

        try {

            if ( is_array($mProfileId) ) {
                $iProfileId = $mProfileId['iProfileId'];
                $aCropData = $mProfileId['crop'];
            } else {
                $iProfileId = (int)$mProfileId;
                $aCropData = array();
            }

            /* Обработали ошибки входных данных */
            if(!(int)$iProfileId)         throw new Exception('Image processing error: Profile is not exists!');
            if(!(int)$iSectionId)         throw new Exception('Image processing error: Section is not exists!');
            if(!file_exists($sImageFile)) throw new Exception('Image processing error: Image is not exists!');

            /* Путь к корневой директории галереи в текущем разделе */
            //$sImagePath = dirname(dirname($sImageFile));
            $sImagePath = skFiles::getFilePath( $iSectionId, self::getModuleDirName(), $bProtected);

            /* Чтение настроек профиля */
            $aFormats = Gallery2FormatsApi::getByProfile($iProfileId);

            if(!count($aFormats))         throw new Exception('Image processing error: Formats is not exists!');

            /* Загрузка исходного изображения для дальнейшей обработки */
            $oImage = new skImage();

            $aOut = false;

            if(!$oImage->load($sImageFile)) throw new Exception('Image processing error: Image not loaded!');

            $oImage->saveToBuffer();


            /* Создание thumbnail для системы администрирования */
            $bCreatedThumbnail = false;
            if( isSet($aFormats) ) {
                foreach($aFormats as $aFormat)
                    if( $aFormat['name'] == 'preview' && isSet($aCropData['preview']) ) {

                        // кроп данные
                        $aCrop = $aCropData[$aFormat['name']];

                        // коэффициент масштабирования миниатюры для вырезки
                        $fKoef = 1;
                        if ( GalleryAdmApi::cropHeight && $oImage->getSrcHeight()>GalleryAdmApi::cropHeight )
                            $fKoef = $oImage->getSrcHeight()/GalleryAdmApi::cropHeight;

                        // обрезка
                        $oImage->cropImage(
                            self::$iThumbnailWidth, self::$iThumbnailHeight,
                            $aCrop['x']*$fKoef, $aCrop['y']*$fKoef,
                            $aCrop['width']*$fKoef, $aCrop['height']*$fKoef,
                            false, true
                        );

                        $bCreatedThumbnail = true;
                    }
            }



            if( $bCreateAllFormat || $bCreatedThumbnail ) {

                if( !$bCreatedThumbnail )
                    $oImage->resize(self::$iThumbnailWidth, self::$iThumbnailHeight);

                $sSavedFile = skFiles::generateUniqFileName($sImagePath.self::$sThumbnailDirectory.DIRECTORY_SEPARATOR, basename($sImageFile));

                $sSavedFile = str_replace(skFiles::getRootUploadPath($bProtected), '', $sSavedFile);

                $sDir = skFiles::createFolderPath(dirname($sSavedFile), $bProtected);

                if(!$sDir) throw new Exception('Image processing error: Thumbnail for image is not created!');

                $sThumbnailPath = $sDir.basename($sSavedFile);
                $aOut['thumbnail'] = skFiles::getWebPath($sThumbnailPath, false);

                /* Сохранить измененное thumbnail */
                $oImage->save($sThumbnailPath);

            }


            /* Обработка изображения по каждому из форматов профиля */
            foreach($aFormats as $aFormat) {

                $oImage->loadFromBuffer();

                if ( isset($aCropData[$aFormat['name']]) ) {

                    // кроп данные
                    $aCrop = $aCropData[$aFormat['name']];

                    // коэффициент масштабирования миниатюры для вырезки
                    if ( $oImage->getSrcHeight()>GalleryAdmApi::cropHeight )
                        $fKoef = $oImage->getSrcHeight()/GalleryAdmApi::cropHeight;
                    else
                        $fKoef = 1;

                    // обрезка
                    $oImage->cropImage(
                        $aFormat['width'], $aFormat['height'],
                        $aCrop['x']*$fKoef, $aCrop['y']*$fKoef,
                        $aCrop['width']*$fKoef, $aCrop['height']*$fKoef,
                        $aFormat['resize_on_larger_side'], $aFormat['scale_and_crop']
                    );

                } else {

                    if( !$bCreateAllFormat ) continue;

                    // обычное изменение размера
                    $oImage->resize($aFormat['width'], $aFormat['height'],$aFormat['resize_on_larger_side'],$aFormat['scale_and_crop']);
                }

                /** not arbeiten */
                if($aFormat['use_watermark'])
                    $oImage->applyWatermark($aFormat['watermark'], $aFormat['watermark_align']);

                $sSavedFile = skFiles::generateUniqFileName($sImagePath.$aFormat['name'].DIRECTORY_SEPARATOR, basename($sImageFile));

                $sSavedFile = str_replace(skFiles::getRootUploadPath($bProtected), '', $sSavedFile);

                $sDir = skFiles::createFolderPath(dirname($sSavedFile), $bProtected);

                if(!$sDir) throw new Exception('Image processing error: Directory is not created!');

                $sNewFilePath = $sDir.basename($sSavedFile);

                /* Сохранить измененное изображение */
                if(!$oImage->save($sNewFilePath)) throw new Exception('Image processing error: Image do not saved!');

                list($iWidth, $iHeight) = $oImage->getSize();
                $aImage = array(

                    'file'   => skFiles::getWebPath($sNewFilePath, false),
                    'name'   => $aFormat['name'],
                    'width'  => $iWidth,
                    'height' => $iHeight,

                );

                $aOut[$aFormat['name']] = $aImage;

            }// each format

            $oImage->clear();

        } catch (Exception $e) {

            $mError = $e->getMessage();
            return false;
        }

        return $aOut;

    }// func

    /**
     * Сортирует изображения
     * @param int $iItemId id перемещаемого объекта
     * @param int $iTargetId id объекта, относительно которого идет перемещение
     * @param string $sOrderType направление переноса
     * @return bool
     */
    public static function sortImages($iItemId, $iTargetId, $sOrderType='before') {

        $sSortField = 'priority';

        // перемещаемое изображение
        $aImage = Gallery2ImagesApi::getImage($iItemId);

        // "относительное" изображение
        $aTargetImage = Gallery2ImagesApi::getImage($iTargetId);

        // должны быть в одном альбоме
        if( $aImage['album_id'] != $aTargetImage['album_id'] )
            return false;

        // выбираем напрвление сдвига
        if( $aImage[$sSortField] > $aTargetImage[$sSortField] ){

            $iStartPos = $aTargetImage[$sSortField];
            if( $sOrderType=='after' ) $iStartPos--;
            $iEndPos = $aImage[$sSortField];
            $iNewPos = $sOrderType=='after' ? $aTargetImage[$sSortField] : $aTargetImage[$sSortField] + 1;
            Gallery2ImagesMapper::shiftImagePosition($aImage['album_id'], $iStartPos, $iEndPos, '+');
            Gallery2ImagesMapper::changeImagePosition($aImage['image_id'],$iNewPos);

        } else {

            $iStartPos = $aImage[$sSortField];
            $iEndPos = $aTargetImage[$sSortField];
            if( $sOrderType=='before' ) $iEndPos++;
            $iNewPos = $sOrderType=='before' ? $aTargetImage[$sSortField] : $aTargetImage[$sSortField] - 1;
            Gallery2ImagesMapper::shiftImagePosition($aImage['album_id'], $iStartPos, $iEndPos, '-');
            Gallery2ImagesMapper::changeImagePosition($aImage['image_id'],$iNewPos);

        }

        return true;
    }

    /**
     * Заменяем миниатюры в изображении $iImageId
     * @param $iImageId
     * @param $mProfile
     * @param $mThumbnailPath
     * @return bool|int
     */
    public static function replaceImageReCropFormat($iImageId, $mProfile, $mThumbnailPath = false){


        $aImage = Gallery2ImagesMapper::getItem($iImageId);

        if( !$aImage  ) return false;

        $aImagesData = json_decode($aImage['images_data'], true);
        foreach($mProfile as $sFormatName=>$aFormat) {

            // удаление старых(земененных) изображений
            skFiles::remove(ROOTPATH.$aImagesData[$sFormatName]['file']);

            // замена миниатюры на новую
            $aImagesData[$sFormatName] = $aFormat;
        }

        $aData['images_data'] = json_encode($aImagesData);
        // изменяем миниатюры для админки
        if( $mThumbnailPath )
            $aData['thumbnail']   = $mThumbnailPath;

        return self::setImage($aData, $iImageId);

    }

}// class
