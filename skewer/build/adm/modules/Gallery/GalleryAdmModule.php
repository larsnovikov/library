<?php
/**
 * Система администрирования для модуля фотогаллереи
 * @class GalleryAdmModule
 *
 * @author ArmiT, $Author: acat $
 * @version $Revision: 1672 $
 * @date $Date: 2013-02-06 15:59:05 +0400 (Ср, 06 фев 2013) $
 * @project JetBrains PhpStorm
 * @package kernel
 */
class GalleryAdmModule extends AdminSectionTabModulePrototype {

    /**
     * Id текущего раздела
     * @var int
     */
    protected $iSectionId = 0;

    /**
     * Название вкладки модуля
     * @var string
     */
    protected $sTabName = 'Галерея';

    /**
     * Количество записей на страницу
     * @var int
     */
    protected $iOnPage = 10;

    /**
     * Массив полей, выводимых колонками в списке альбомов
     * @var array
     */
    protected $aAlbumsListFields = array('album_id', 'title', 'visible');

    /**
     * Номер текущей страницы
     * @var int
     */
    protected $iPage = 0;

    /**
     * Id текущего альбома
     * @var int
     */
    protected $iCurrentAlbumId = 0;

    /**
     * Максимально допустимый размер для загружаемых изображений
     * @var int
     */
    protected $iMaxUploadSize = 0;

    /**
     * Название, присваиваемое загруженному изображению по-умолчанию
     * @var string
     */
    protected $sDefaultImageTitle = 'Изображение';

    /*
     * Массив данных для ресайза
     * заполняется при загрузке файла
     */
    protected $aUploadedData = array();

    /**
     * Префикс, добавляемый к числовым алиасам альбомов
     * @var string
     */
    protected $sAlbumAliasPrefix = 'album-';

    /* Methods */

    /**
     * Иницализация
     */
    protected function preExecute() {

        CurrentAdmin::testControlPanelAccess();

        /* текущая страница постраничного */
        $this->iPage = $this->getInt('page');

        /* Id текущего раздела */
        $this->iSectionId = $this->getInt('sectionId', $this->getEnvParam('sectionId'));

        /* Максимально допустимый размер для загружаемых изображений */
        $this->iMaxUploadSize = skConfig::get('upload.maxsize');

        /* Восстанавливаем текущий альбом */
        $this->iCurrentAlbumId = $this->getInt('currentAlbumId');
        //if ( !CurrentAdmin::canRead($this->iSectionId) ) throw new ModuleAdminErrorException( 'accessDenied' );

    }// func

    /**
     * Установка служебных данных
     * @param \ExtPrototype $oExtIface
     */
    protected function setServiceData( ExtPrototype $oExtIface ) {

        // установить заголовок модуля
        $oExtIface->setTitle( $this->sTabName );

        // установить данные для передачи интерфейсу
        $oExtIface->setServiceData( array(
            'sectionId' => $this->iSectionId,
            'page' => $this->iPage,
            'currentAlbumId' => $this->iCurrentAlbumId,
        ) );

    }// func

    /**
     * Вызывается в случае отсутствия явного обработчика
     * @return int
     */
    protected function actionInit() {

        return $this->actionGetAlbums();

    }

    /**
     * запустить сборщик мусора
     */
    protected function startScavenger() {

        // запросить
        $aRows = Gallery2TmpAdmModule::getOldRows();
        if ( !$aRows )
            return;

        // набор id для удаления
        $aIdList = array();

        // перебрать все записи
        foreach ( $aRows as $aRow ) {
            // id в список удаления
            $aIdList[] = $aRow['id'];

            // проверить наличие записей с откатом разделов
            if ( strpos($aRow['value'], '..') )
                continue;

            // удаление файла
            if( is_file( ROOTPATH.$aRow['value'] ) )
                unlink(ROOTPATH.$aRow['value']);

        }

        // удалние записей по списку
        Gallery2TmpAdmModule::delById( $aIdList );

    }

    /**
     * Возвращает список альбомов
     * @throws Exception
     * @return int
     */
    protected function actionGetAlbums() {

        /* список альбомов текущего раздела */
        $this->iCurrentAlbumId = 0;
        $this->setPanelName(' Альбомы',true);

        /* Строим список альбомов */
        $oList = new ExtUserFile( 'PhotoAlbumList' );

        if(!$this->iSectionId)
            $this->iSectionId = $this->getEnvParam('sectionId');

        /* Выбираем данные для списка */
        $aAlbums = array();
        $aItems = Gallery2AlbumsApi::getAllBySection($this->iSectionId);

        if(is_array($aItems))
            foreach($aItems as $aAlbum) {
                $sFirstImg = $aAlbum['album_img'];
                if(!$sFirstImg) $sFirstImg = $this->getModuleWebDir().'/img/no_photo.png';
                $aAlbums[] = array(
                    'url'      => $sFirstImg,
                    'name'     => $aAlbum['title'],
                    'size'     => 0,
                    'lastmod'  => $aAlbum['creation_date'],
                    'album_id' => $aAlbum['album_id'],
                    'album_count' => $aAlbum['album_count'],
                    'active'   => $aAlbum['visible'] ? 'checked' : ''
                );
            }// each picture

        /* Записываем данные на отправку */
        $this->setData('albums',$aAlbums);
        /* Добавление библиотек для работы */
        $this->addLibClass( 'PhotoSorter' );
        $this->addLibClass( 'PhotoAlbumListView' );

        $oList->addButton( ExtDockedAddBtn::create()
            ->setAction('addUpdAlbum')
        );
        $oList->addBntSeparator('->');
        $oList->addButton( ExtDockedDelBtn::create()
            ->setAction('')
            ->setState('del_selected')
        );

        /* Добавляем css файл для */
        $this->addCssFile('gallery.css');

        /* php событие при клике */
        $this->setData('clickAction','showAlbum');

        $this->setCmd('show_albums_list');

        $this->setExtInterface( $oList );

        return psComplete;
    }// func

    /**
     * Изменение видимости фотографии
     */
    public function actionAlbumActiveChange() {

        $iAlbumId = $this->get('data');

        Gallery2AlbumsAdmApi::toggleActive( $iAlbumId );

    }

    /**
     * писание альбома, список изображений в нем. Загрузка
     * @throws Exception
     * @return int
     */
    protected function actionShowAlbum() {

        // очистить контейнер загруки
        //$this->clearUploadedDataWithSrc();
        $this->clearUploadedData();

        /* Строим список изображений */
        $oList = new ExtUserFile( 'PhotoList' );

        $aData = $this->get('data');

        if($this->iCurrentAlbumId)
            $aData['album_id'] = $this->iCurrentAlbumId;

        if(!(int)$iAlbumId = $aData['album_id']) throw new Exception('Ошибка: Альбом не определен!');

        $aAlbum = Gallery2AlbumsApi::getById( $iAlbumId );
        if ( !$aAlbum )
            throw new Exception('Альбом не найден');

        $this->iCurrentAlbumId = $iAlbumId;

        /* Устанавливаем название вкладки  */
        $this->setPanelName(sprintf('Альбом "%s"',$aAlbum['title']),true);

        /* Выбираем данные для списка */
        $aItems = Gallery2ImagesApi::getFromAlbum($iAlbumId);

        $aImages = array();

        if($aItems)
            foreach($aItems as $aImage) {
                $aImages[] = array(
                    'url'      => $aImage['thumbnail'],
                    'name'     => $aImage['title'],
                    'size'     => 0,
                    'lastmod'  => $aImage['creation_date'],
                    'image_id' => $aImage['image_id'],
                    'album_id' => $aImage['album_id'],
                    'active'   => $aImage['visible'] ? 'checked' : ''
                );
            }// each picture

        /* Записываем данные на отправку */
        $this->setData('images',$aImages);

        /* Добавление библиотек для работы */
        $this->addLibClass( 'PhotoSorter' );
        $this->addLibClass( 'PhotoAddField' );
        $this->addLibClass( 'PhotoListView' );

        // дополнительный текст для списка
        $this->setData('addText',
            sprintf('Вы можете загрузить фото не превышающее %d Мб по объему, %d пикселей '.
                    'по большей из сторон и только следующих форматов: %s.',
                skFiles::getMaxUploadSize()/1024/1024,
                skImage::getMaxLineSize(),
                implode(', ',skImage::getAllowImageTypes())
            )
        );

        $oList->addDockedItem(array(
            'text' => 'Редактировать',
            'iconCls' => 'icon-edit',
            'state' => 'init',
            'action' => 'addUpdAlbum',
        ));
        $oList->addDockedItem(array(
            'text' => 'В список',
            'iconCls' => 'icon-cancel',
            'state' => 'init',
            'action' => 'getAlbums',
        ));
        $oList->addBntSeparator('');

        // кнопка загрузки
        $oList->addButton( ExtDockedByUserFile::create('Загрузить','PhotoAddField')
            ->setIconCls( ExtDocked::iconAdd )
        );

        // кнопка удаления
        $oList->addBntSeparator('->');
        $oList->addBntDelete('', 'del_selected');

        /* Добавляем css файл для */
        $this->addCssFile('gallery.css');

        /* php событие при клике */
        $this->setData('clickAction','showImage');

        $this->setCmd('show_photos_list');

        $this->setExtInterface( $oList );

        return psComplete;
    }// func

    protected  function actionShowImage() {

        $aData = $this->get('data');

        if(!$iImageId = $aData['image_id']) throw new Exception('Ошибка: Изображение не найдено!');

        /* Получить изображение */

        $aImage = Gallery2ImagesApi::getImage($iImageId);

        if(!$aImage) throw new Exception('Ошибка: Изображение не найдено!');

        $this->iCurrentAlbumId = $aImage['album_id'];

        if(!$aImage['images_data'] = json_decode($aImage['images_data'], true))
            throw new Exception('Ошибка: Изображение повреждено!');

        /* Получить Набор форматов для изображения */
        $iProfileId = Gallery2AlbumsApi::getProfileId($this->iCurrentAlbumId);
        $aFormats   = Gallery2FormatsApi::getByProfile($iProfileId);

        if(!$aFormats) throw new Exception('Ошибка: Видимо, профиль для этого альбома был удален или не существует!');

        /* Собираем массив данных по изображению */
        $aTabs['name']  = 'formats';
        $aTabs['title'] = 'Изображения';
        $i = 0;
        foreach($aFormats as $aFormat) {

            if( isSet($aImage['images_data'][$aFormat['name']]) && $aFormat['active'] ) {
                ++$i;
                $aImageItem['src']    = $aImage['images_data'][$aFormat['name']]['file'];
                $aImageItem['name']   = $aFormat['name'];
                $aImageItem['title']  = ($aFormat['title'])? $aFormat['title']: "Размер ($i)";
                $aImageItem['width']  = ($aImage['images_data'][$aFormat['name']]['width'])?  $aImage['images_data'][$aFormat['name']]['width']: '*';
                $aImageItem['height'] = ($aImage['images_data'][$aFormat['name']]['height'])? $aImage['images_data'][$aFormat['name']]['height']: '*';

                $aTabs['value'][] = $aImageItem;
            }

        }

        // дополнительная библиотека для отображения
        $this->addLibClass( 'PhotoImg' );
        //$this->addLibClass( 'PhotoAddToFormatField' );

        /* Подключить автоматический генератор форм */
        $oForm = new ExtForm();

        /* Установить заголовок панели */
        if(!empty($aImage['title']))
            $this->setPanelName(sprintf('Редактирование изображения "%s"',$aImage['title']), true);
        else
            $this->setPanelName('Редактирование изображения', true);

        $aItem = array();

        /* Установить набор элементов формы */
        $aFieldFilter = array();

        /** @todo Добавить проверку на наличие фоток согласно формату (Не удалять )*/

        /*Добавить поле - набор изображений */
        $aFieldFilter['formats'] = $oForm->getSpecificItemInitArray('PhotoShowFormatsField', $aTabs );

        /* Добавить поле - Выводить */
        $aFieldFilter['visible'] = array(
            'name' => 'visible',
            'title' => 'Выводить в альбоме',
            'view' => 'check',
            'value' => $aImage['visible'],
        );

        /* Добавить поле - Название */
        $aFieldFilter['title'] = array(
            'name' => 'title',
            'title' => 'Название',
            'view' => 'str',
            'value' => $aImage['title'],
        );

        /* Добавить поле - Описание */
        $aFieldFilter['description'] = array(
            'name' => 'description',
            'title' => 'Описание',
            'view' => 'text',
            'value' => $aImage['description'],
        );

        /* Добавить поле - Дата создания */
        $aFieldFilter['creation_date'] = array(
            'name' => 'creation_date',
            'title' => 'Дата создания',
            'view' => 'str',
            'disabled' => '1',
            'value' => $aImage['creation_date'],
        );

        /* Добавить поле - image id */
        $aFieldFilter['image_id'] = array(
            'name' => 'image_id',
            'title' => '',
            'view' => 'hide',
            'value' => $aImage['image_id'],
        );
        skLogger::dump('[imgId]='.$aImage['image_id']);
        $oForm->setFields( $aFieldFilter );

        /* Установить значения для элементов */
        $oForm->setValues( $aItem );

        /* Кнопки боковой панели */
        $oForm->addBntSave('updateImage');
        $oForm->addBntCancel('showAlbum');
        $oForm->addBntSeparator('-');
        $oForm->addDockedItem(array(
            'text' => 'Re-crop',
            'iconCls' => 'icon-edit',
            'state' => 'init',
            'action' => 'reCropForm',
        ));

        // кнопка загрузки новой миниатюры
        $oForm->addButton( ExtDockedByUserFile::create('Изменить','PhotoAddToFormatField')
                ->setIconCls( ExtDocked::iconEdit )
                ->setAddParam('imageId',$iImageId)
        );

        $oForm->addBntSeparator('->');
        $oForm->addBntDelete('deleteImage');

        /* Построить интерфейс */
        $this->setExtInterface( $oForm );

        return psComplete;

    }// func

    /**
     * Обновляет заголовочные данные изображения
     * @return int
     * @throws Exception
     */
    protected function actionUpdateImage() {

        $aData = $this->get( 'data' );

        if(!count($aData)) throw new Exception ('Ошибка: Изображение не сохранено!');
        if(!(int)$iImageId = $aData['image_id']) throw new Exception ('Ошибка: Изображение не сохранено!');

        /* Обновляем данные изображения */
        Gallery2ImagesAdmApi::updateImageHeader($iImageId, $aData['title'], $aData['description'], $aData['visible']);

        /* вывод списка */
        return $this->actionShowAlbum();

    }// func

    protected function actionDeleteImage() {

        $aData = $this->get( 'data' );

        if(!count($aData)) throw new Exception ('Ошибка: Изображение не удалено!');
        if(!(int)$iImageId = $aData['image_id']) throw new Exception ('Ошибка: Изображение не удалено!');

        /* Обновляем данные изображения */
        
        $mError = '';
        $bRes = Gallery2ImagesAdmApi::removeImage($iImageId, $mError);

        if(!$bRes) throw new Exception ($mError);
       
        $this->addNoticeReport("Удаление фото", "id фото $iImageId", skLogger::logUsers, "GalleryAdmModule");

        return $this->actionShowAlbum();

    }// func

    /**
     * Групповое удаление изображений
     */
    protected function actionGroupDel() {

        // набор входных данных для удаления
        $aInList = $this->get( 'delItems' );

        // проверить принадлежность целевому альбому
        $aDelList = GalleryAdmApi::validateIdList( $aInList, $this->iCurrentAlbumId );

        // удалить по списку
        $iCnt = 0;
        
        foreach ( $aDelList as $iId ) {
            if( Gallery2ImagesAdmApi::removeImage( $iId ) )
                $iCnt++;
        }

        $this->addMessage( sprintf('Удалено изображений: %d из %d', $iCnt, count($aInList)) );
        
        $this->addNoticeReport("Удаление фото", "<pre>".print_r($aDelList, true)."</pre>", skLogger::logUsers, "GalleryAdmModule");

        $this->actionShowAlbum();

    }

    /**
     * Изменение видимости фотографии
     */
    public function actionPhotoActiveChange() {

        $iPhotoId = $this->get('data');

        Gallery2ImagesAdmApi::changeActivePhoto( $iPhotoId );

    }


    /**
     * Загрузка нового изображения для определенного формата
     */
    protected function actionLoadNewImageForFormat(){

        $this->setPanelName('Загрузка изображения', true);

        // Обработка входных данных
        $sFormat = $this->get( 'formatName' );
        $iImageId = $this->get( 'imageId' );

        if( !$sFormat || !$iImageId )
            throw new Exception("Ошибка входных данных!");

        if(!$iAlbumId = (int)$this->iCurrentAlbumId) throw new Exception('Альбом не определен!');

        $iProfileId = Gallery2AlbumsApi::getProfileId($iAlbumId);

        if(!$iProfileId) throw new Exception('Неверные данные!');

        // Загрузка изображений, перемещение в целевую директорию
        $sSourceFN = GalleryAdmApi::uploadFile( $this->iSectionId );

        $this->actionReCropForm($iImageId, $sFormat, $sSourceFN);
    }

    /**
     * Изменений кропинга для определенного формата изображения
     */
    protected function actionReCropForm($iImageId = 0, $sFormatName = '', $sSourceFN = '') {

        // Обработка входных данных
        $aData = $this->get( 'data' );
        $sFormat = isSet($aData['selectedFormat']) ? $aData['selectedFormat'] : $sFormatName;
        $iImageId = isSet($aData['image_id']) ? $aData['image_id'] : $iImageId;
        if( !$sFormat || !$iImageId )
            throw new Exception("Ошибка входных данных!");

        $this->setPanelName('Изменение изображения ', true);

        // получение информации о текущем формате
        $aFormat = Gallery2FormatsApi::getByName($sFormat);
        if( !$aFormat )
            throw new Exception("Ошибка: не верно задан формат изображения!");

        // получение исходного изображения
        if( !$sSourceFN ){
            $aImage = Gallery2ImagesMapper::getItem($iImageId);
            $sSourceFN = $aImage['source'];
        }

        // создать миниатюру
        $aCropMin = GalleryAdmApi::createCropMin( $sSourceFN, $this->iSectionId );

        // добавить в сессию запись о загруженном файле и о миниатюре и получить ключ
        $sCropFN = $aCropMin['file'];
        $this->aUploadedData = array(
            'crop' => $sCropFN,
            'source' => $sSourceFN,
            'crop_id' => Gallery2TmpAdmModule::create('crop', $sCropFN),
            'source_id' => Gallery2TmpAdmModule::create('source', $sSourceFN)
        );

        // данные о миниатюре для отображения кроп интерфейса
        $this->setData( 'cropData', $aCropMin );
        $this->setData( 'formatsData', $aFormat );

        /*
         * Сборка интерфейса
         */

        /* Подключить автоматический генератор форм */
        $oForm = new ExtForm();

        $aItem = array();

        /* Установить набор элементов формы */
        $aFieldFilter = array();

        /*Добавить поле - набор изображений */
        /* Собираем массив данных по изображению */
        $aTabs['name']  = 'formats';
        $aTabs['title'] = 'Изображения';
        $aTabs['value'] = array('cropData'=>$aCropMin,'formatsData'=>$aFormat);
        $aTabs['cropData'] = $aCropMin;
        $aTabs['formatsData'] = $aFormat;
        $aTabs['subtext'] = 'Вы можете выбрать размер кадрирования фотографии, потянув за рамки фотографии';

        $aFieldFilter['formats'] = $oForm->getSpecificItemInitArray('PhotoResizerList', $aTabs );

        $this->addLibClass( 'PhotoResizer' );

        // передача идентификатора изображения
        $aFieldFilter['image_id'] = array(
            'name' => 'image_id',
            'title' => '',
            'view' => 'hide',
            'value' => $iImageId
        );


        $oForm->setFields( $aFieldFilter );

        /* Установить значения для элементов */
        $oForm->setValues( $aItem );

        /* Кнопки боковой панели */
        // добавить кнопку запуска обработки
        $oForm->addBntSave('saveReCropImage');

        // и кнопку отмены (возврата к альбому)
        $oForm->addBntCancel('showAlbum');

        $this->addJSFile( 'jquery.min.js' );
        $this->addJSFile( 'jquery.Jcrop.min.js' );
        $this->addCSSFile( 'jquery.Jcrop.min.css' );

        /* Построить интерфейс */
        $this->setExtInterface( $oForm );

    }

    /**
     * Сохранение перекроппинного изображения для определенного формата
     * @throws Exception
     */
    protected function actionSaveReCropImage() {

        // id отображаемого альбома
        $iAlbumId = (int)$this->iCurrentAlbumId;
        if( !$iAlbumId )
            throw new Exception('Альбом не определен');

        // id профиля для альбома
        $iProfileId = Gallery2AlbumsApi::getProfileId($iAlbumId);
        if(!$iProfileId)
            throw new Exception('Неверные данные!');

        // получить ключ кропа
        $aCrop = array();
        $aData = $this->get('data');
        if( is_array($aData) )
            foreach( $aData as $sKey=>$aVal ) {
                if(strpos($sKey,'cropData_') !== false)
                    $aCrop[substr($sKey,9)] = $aVal;
            }

        if( !($iImageId = isSet($aData['image_id']) ? $aData['image_id'] : false) )
            throw new Exception('Неверные данные!');

        $aData = $this->aUploadedData;

        if ( !$aData or !count($aCrop) )
            throw new Exception('Отсутствуют данные для сохранения фото.');

        // запросить файл
        $sImagePath = $aData['source'];
        $sImageFullPath = ROOTPATH.$sImagePath;

        /* Обработка изображения согласно профилю настроек, перемещение созданных файлов в целевые директории */
        if(!$sImagePath) throw new Exception("Изображение не было загружено!");

        $sError = false;

        $mProfileId = array(
            'crop' => $aCrop,
            'iProfileId' => $iProfileId
        );

        $aUpdImage = Gallery2ImagesAdmApi::processImage($sImageFullPath, $mProfileId, $this->iSectionId, false, false, $sError);

        if(!$aUpdImage OR $sError) throw new Exception($sError);

        $sThumbnail   = (isSet($aUpdImage['thumbnail'])) ? $aUpdImage['thumbnail']: false;

        unSet($aUpdImage['thumbnail']);

        /* Сохранение измененных миниатюр в БД */
        $bReplaceImage = Gallery2ImagesAdmApi::replaceImageReCropFormat($iImageId, $aUpdImage, $sThumbnail);

        // очистить контейнер загруки
        $this->clearUploadedData();

        if(!$bReplaceImage) throw new Exception('Изображение не сохранено!');

        $this->set('data',array('image_id'=>$iImageId));
        
        $this->addNoticeReport("Сохранение изображения", "id фото = $iImageId", skLogger::logUsers, "GalleryAdmModule");

        $this->actionShowAlbum();

    }


    /**
     * Форма  Добавления / редактирования описания альбома
     * @return int
     * @throws Exception
     */
    protected function actionAddUpdAlbum() {

        $oForm = new ExtForm();

        $this->setPanelName('Добавление/Редактирование альбома',true);

        try {

            $iAlbumId = false;
            if($this->iCurrentAlbumId) $iAlbumId = $this->iCurrentAlbumId;

            /* Получаем данные формата или заготовку под новый формат */
            $aValues = $iAlbumId ? Gallery2AlbumsApi::getById($iAlbumId) : Gallery2AlbumsAdmApi::getAlbumBlankValues();


            /* установить набор элементов формы */
            $aItems = Gallery2AlbumsMapper::getFullParamDefList(Gallery2AlbumsMapper::getModelFields());

            /* Запрещаем изменять профиль для альбома */
            if($iAlbumId)
                $aItems['profile_id']['disabled'] = 1;

            if(!$iAlbumId)
                unSet($aItems['creation_date']);

            $oForm->setFields( $aItems );

            /* Если профиль по-умолчанию не задан - ставим первый активный */
            if(!$aValues['profile_id']) $aValues['profile_id'] = Gallery2ProfilesApi::getFirstActiveProfileId();

            /* установить значения для элементов */
            $oForm->setValues( $aValues );

            /* добавление кнопок */

            $oForm->addBntSave('saveAlbum');

            // добавление SEO блока полей
            SEOData::appendExtForm( $oForm, 'gallery', $iAlbumId );

        } catch (Exception $e) {

            $oForm->addError($e->getMessage());

        }

        /* Если Создается новый альбом, то "canсel" ведет на список альбомов а не детальную конкретного  */
        if ( $iAlbumId )
            $oForm->addBntCancel('showAlbum');
        else
            $oForm->addBntCancel('getAlbums');

        if ( $iAlbumId ) {
            $oForm->addBntSeparator('->');
            $oForm->addBntDelete('delAlbum');
        }
        // вывод данных в интерфейс
        $this->setExtInterface( $oForm );

        return psComplete;
    }// func

    /**
     * Сохраняет Описание альбома
     * @throws Exception
     */
    protected function actionSaveAlbum() {

        $aData = $this->get( 'data' );

        if(!count($aData)) throw new Exception ('Ошибка: Данные не были отправлены!');

        // добавляем алиас
        if(!isset($aData['alias']) || !$aData['alias']){
            $aData['alias'] = skFiles::makeURLValidName($aData['title'],false);
        } else {
            $aData['alias'] = skFiles::makeURLValidName($aData['alias'],false);

            /* Если передали в алиасе число - добавляем префикс */
            $mAlias = $aData['alias'];
            if((int)$mAlias)
                $aData['alias'] = $this->sAlbumAliasPrefix.$aData['alias'];
        }


        // проверка на дубли алиса
        $sAlias = $aData['alias'];
        $iNum = 0;
        do {
            $bFlag = true;
            $sCurAlias = $sAlias;
            if($iNum) $sCurAlias .= $iNum;
            if( !( $aItem = Gallery2AlbumsApi::getByAlias($sCurAlias, $this->iSectionId) ) ){
                $aData['alias'] = $sCurAlias;
                $bFlag = false;
            } else {
                if( isSet($aData['album_id']) && $aItem['album_id'] == $aData['album_id'] ) {
                    $aData['alias'] = $sCurAlias;
                    $bFlag = false;
                }
            }
            $iNum++;
            if($iNum > 300) $bFlag = false;
        } while( $bFlag );


        $iAlbumId = ($aData['album_id'])? $aData['album_id']: false;

        /* Установка дополнительных значений */
        $aData['owner'] = 'section';              // владельца
        $aData['section_id'] = $this->iSectionId; // родительского раздела

        /* Индекс сортировки */
        if(!$iAlbumId)
            $aData['priority'] = Gallery2AlbumsMapper::getMaxPriorityBySection($this->iSectionId)+1;

        /* Добавляем либо обнавляем данные альбома */
        $iNewId = Gallery2AlbumsAdmApi::setAlbum($aData, $iAlbumId);

        // сохранение SEO данных
        SEOData::saveJSData( 'gallery', $iNewId, $aData );

        /* Если все нормально, устанавливаем в кач. текущего альбома, тот, который был определен */
        if($iNewId)  $this->iCurrentAlbumId = $iNewId;
        
        $this->addNoticeReport("Сохранение альбома", "Альбом id = $iNewId", skLogger::logUsers, "GalleryAdmModule");

        /* вывод изображений альбома */
        $this->actionShowAlbum();

    }// func

    /**
     * Автоматический кроп фотографий при мультифайловой загрузки
     * @param array $aFiles
     * @throws Exception
     * @return void
     */
    protected function actionMultiUploadImages($aFiles = array()) {

        // id отображаемого альбома
        $iAlbumId = (int)$this->iCurrentAlbumId;
        if( !$iAlbumId )
            throw new Exception('Альбом не определен');

        // id профиля для альбома
        $iProfileId = Gallery2AlbumsApi::getProfileId($iAlbumId);
        if(!$iProfileId)
            throw new Exception('Неверные данные!');

        // набор форматов альбома todo ?
        $aFormats   = Gallery2FormatsApi::getByProfile($iProfileId);
        if(!$aFormats) throw new Exception('Ошибка: Видимо, профиль для этого альбома был удален или не существует!');
        foreach( $aFormats as $iKey=>$aFormat)
            if( !$aFormat['active'] ) unset($aFormats[$iKey]);
        unset( $aFormats['thumbnail'] );


        foreach($aFiles as $sSourceFN) {


            $aCrop = array();
            if( count($aFormats) )
                foreach( $aFormats as $iKey=>$aFormat ) {
                    //if(strpos($sKey,'cropData_') !== false)
                    $aCrop[$aFormat['name']] = array('x'=>0,'y'=>0,'width'=>0,'height'=>0);
                }

            $sTitle = '';
            $sDescription = '';

            if ( !count($aCrop) )
                throw new Exception('Отсутствуют данные для сохранения фото.');

            // запросить файл
            $sImagePath = $sSourceFN;
            $sImageFullPath = ROOTPATH.$sImagePath;

            /* Обработка изображения согласно профилю настроек, перемещение созданных файлов в целевые директории */
            if(!$sImagePath) throw new Exception("Изображение не было загружено!");

            $sError = false;

            $mProfileId = array(
                'crop' => $aCrop,
                'iProfileId' => $iProfileId
            );

            $aNewImage = Gallery2ImagesAdmApi::processImage($sImageFullPath, $mProfileId, $this->iSectionId, false, true, $sError);

            if(!$aNewImage OR $sError) throw new Exception($sError);

            $sThumbnail   = (isSet($aNewImage['thumbnail']))? $aNewImage['thumbnail']: '';

            unSet($aNewImage['thumbnail']);

            /* Сохранение сущности в БД */
            $iImageId = Gallery2ImagesAdmApi::addImage($iAlbumId, $aNewImage, $sTitle, $sDescription, $sThumbnail, $sImagePath);

            // очистить контейнер загруки todo
            $this->clearUploadedData();

            if(!$iImageId) throw new Exception('Изображение не сохранено!');

        }

        $this->actionShowAlbum();
    }


    /**
     * Загружает изображение на сервер и обрабатывает согласно профилю
     * @return bool|int
     * @throws Exception
     */
    protected function actionUploadImage() {

        // сборщик мусора
        if ( Gallery2TmpAdmModule::allowStartScavenger() )
            $this->startScavenger();

        if(!$iAlbumId = (int)$this->iCurrentAlbumId) throw new Exception('Альбом не определен!');

        $iProfileId = Gallery2AlbumsApi::getProfileId($iAlbumId);

        if(!$iProfileId) throw new Exception('Неверные данные!');

        // набор форматов альбома
        $aFormats   = Gallery2FormatsApi::getByProfile($iProfileId);
        if(!$aFormats) throw new Exception('Ошибка: Видимо, профиль для этого альбома был удален или не существует!');
        foreach( $aFormats as $iKey=>$aFormat)
            if( !$aFormat['active'] ) unset($aFormats[$iKey]);
        unset( $aFormats['thumbnail'] );

        $this->setPanelName('Загрузка изображения', true);

        // Загрузка изображений, перемещение в целевую директорию
        $sSourceFN = GalleryAdmApi::uploadFile( $this->iSectionId );

        // если загружено несколько файлов - делаем автоматический кроппинг
        if( count($sSourceFN) > 1 ) {
            $this->actionMultiUploadImages($sSourceFN);
            return false;
        }

        // создать миниатюру
        $aCropMin = GalleryAdmApi::createCropMin( $sSourceFN, $this->iSectionId );

        // добавить в сессию запись о загруженном файле и о миниатюре и получить ключ
        $sCropFN = $aCropMin['file'];
        $this->aUploadedData = array(
            'crop' => $sCropFN,
            'source' => $sSourceFN,
            'crop_id' => Gallery2TmpAdmModule::create('crop', $sCropFN),
            'source_id' => Gallery2TmpAdmModule::create('source', $sSourceFN)
        );

        // данные о миниатюре для отображения кроп интерфейса
        $this->setData( 'cropData', $aCropMin );
        $this->setData( 'formatsData', $aFormats );

        /*
         * Сборка интерфейса
         */

        /* Подключить автоматический генератор форм */
        $oForm = new ExtForm();

        $aItem = array();

        /* Установить набор элементов формы */
        $aFieldFilter = array();

        /*Добавить поле - набор изображений */
        /* Собираем массив данных по изображению */
        $aTabs['name']  = 'formats';
        $aTabs['title'] = 'Изображения';
        $aTabs['value'] = array('cropData'=>$aCropMin,'formatsData'=>$aFormats);
        $aTabs['cropData'] = $aCropMin;
        $aTabs['formatsData'] = $aFormats;
        $aTabs['subtext'] = 'Вы можете выбрать размер кадрирования фотографии, потянув за рамки фотографии';

        $aFieldFilter['formats'] = $oForm->getSpecificItemInitArray('PhotoResizerList', $aTabs );

        $this->addLibClass( 'PhotoResizer' );

        /* Добавить поле - Название */
        $aFieldFilter['title'] = array(
            'name' => 'title',
            'title' => 'Название',
            'view' => 'str',
            'value' => '',
        );

        /* Добавить поле - Описание */
        $aFieldFilter['description'] = array(
            'name' => 'description',
            'title' => 'Описание',
            'view' => 'text',
            'value' => '',
        );

        $oForm->setFields( $aFieldFilter );

        /* Установить значения для элементов */
        $oForm->setValues( $aItem );

        /* Кнопки боковой панели */
        // добавить кнопку запуска обработки
        $oForm->addBntSave('resizePhoto');
        // и кнопку отмены (возврата к альбому)
        $oForm->addBntCancel('showAlbum');

        $this->addJSFile( 'jquery.min.js' );
        $this->addJSFile( 'jquery.Jcrop.min.js' );
        $this->addCSSFile( 'jquery.Jcrop.min.css' );

        /* Построить интерфейс */
        $this->setExtInterface( $oForm );

    }// func

    /**
     * Выполнение ресайза уже загруженного фото
     * @throws Exception
     */
    protected function actionResizePhoto() {

        // id отображаемого альбома
        $iAlbumId = (int)$this->iCurrentAlbumId;
        if( !$iAlbumId )
            throw new Exception('Альбом не определен');

        // id профиля для альбома
        $iProfileId = Gallery2AlbumsApi::getProfileId($iAlbumId);
        if(!$iProfileId)
            throw new Exception('Неверные данные!');

        // получить ключ кропа
        $aCrop = array();
        $aData = $this->get('data');
        if( is_array($aData) )
        foreach( $aData as $sKey=>$aVal ) {
            if(strpos($sKey,'cropData_') !== false)
                $aCrop[substr($sKey,9)] = $aVal;
        }

        $sTitle = isset($aData['title']) ? $aData['title'] : '';
        $sDescription = isset($aData['description']) ? $aData['description'] : '';
        $aData = $this->aUploadedData;

        if ( !$aData or !count($aCrop) )
            throw new Exception('Отсутствуют данные для сохранения фото.');

        // запросить файл
        $sImagePath = $aData['source'];
        $sImageFullPath = ROOTPATH.$sImagePath;

        /* Обработка изображения согласно профилю настроек, перемещение созданных файлов в целевые директории */
        if(!$sImagePath) throw new Exception("Изображение не было загружено!");

        $sError = false;

        $mProfileId = array(
            'crop' => $aCrop,
            'iProfileId' => $iProfileId
        );

        $aNewImage = Gallery2ImagesAdmApi::processImage($sImageFullPath, $mProfileId, $this->iSectionId, false, true, $sError);

        if(!$aNewImage OR $sError) throw new Exception($sError);

        $sThumbnail   = (isSet($aNewImage['thumbnail']))? $aNewImage['thumbnail']: '';

        unSet($aNewImage['thumbnail']);

        /* Сохранение сущности в БД */
        $iImageId = Gallery2ImagesAdmApi::addImage($iAlbumId, $aNewImage, $sTitle, $sDescription, $sThumbnail, $sImagePath);

        // очистить контейнер загруки todo
        $this->clearUploadedData();

        if(!$iImageId) throw new Exception('Изображение не сохранено!');

        $this->set('data',array('image_id'=>$iImageId));
        $this->addNoticeReport("Сохранение изображения", "id фото = $iImageId", skLogger::logUsers, "GalleryAdmModule");

        $this->actionShowAlbum();

    }


    /**
     * Удаляет файлы и очищает контейнер загрузки
     */
    protected function clearUploadedData() {

        // выйти, если уже очищен
        if ( !$this->aUploadedData )
            return;

        // удалить миниатюру и исходник
        unlink( ROOTPATH.$this->aUploadedData['crop'] );

        Gallery2TmpAdmModule::delById( $this->aUploadedData['crop_id'] );
        Gallery2TmpAdmModule::delById( $this->aUploadedData['source_id'] );

        // очистить контейнер данных загрузки
        $this->aUploadedData = array();
    }

    /**
     * Удаляет все служебные файлы и очищает контейнер загрузки
     */
//    protected function clearUploadedDataWithSrc() {
//
//        // выйти, если уже очищен
//        if ( !$this->aUploadedData )
//            return;
//
//        // удалить миниатюру и исходник
//        //  НЕЛЬЗЯ УДАЛЯТЬ ЕСЛИ ПЕРЕКРОПИВАЕМ
//        //unlink( ROOTPATH.$this->aUploadedData['source'] );
//
//        $this->clearUploadedData();
//
//    }


    /**
     * Удаляет выбранный альбом и фотографии к нему
     * @throws Exception
     */
    protected function actionDelAlbum() {

        /* Данные по альбому */
        $aData = $this->get('data');

        if(!isSet($aData['album_id']) OR !$iAlbumId = (int)$aData['album_id']) throw new Exception('Ошибка: Альбом не указан!');

        /*Удаление альбома*/
        $mError = false;
        if(!Gallery2AlbumsAdmApi::removeAlbum($iAlbumId, $mError)) throw new Exception($mError);
        /* Сброс значения текущго альбома */
        $this->iCurrentAlbumId = false;

        /*Вывод списка альбомов для текущего раздела*/
        $this->actionGetAlbums();
        
        $this->addNoticeReport("Удаление альбома", "Альбом $aData[title] удален", skLogger::logUsers, "GalleryAdmModule");

    }

    /**
     * Сортировка картинок
     */
    protected function actionSortImages(){

        $iItemId = $this->get('itemId');
        $iTargetItemId = $this->get('targetId');
        $sOrderType = $this->get('orderType');

        if ( !$iItemId or !$iTargetItemId or !$sOrderType )
            throw new Exception('Не заданы параметры для сортировки');

        $iItemId = (int)str_replace('horizontal_sort', '', $iItemId);
        $iTargetItemId = (int)str_replace('horizontal_sort', '', $iTargetItemId);

        Gallery2ImagesAdmApi::sortImages($iItemId, $iTargetItemId, $sOrderType);

    }

    /**
     * Сортировка альбомов
     */
    protected function actionSortAlbums(){

        $iItemId = $this->get('itemId');
        $iTargetItemId = $this->get('targetId');
        $sOrderType = $this->get('orderType');

        if ( !$iItemId or !$iTargetItemId or !$sOrderType )
            throw new Exception('Не заданы параметры для сортировки');

        $iItemId = (int)str_replace('horizontal_sort', '', $iItemId);
        $iTargetItemId = (int)str_replace('horizontal_sort', '', $iTargetItemId);

        Gallery2AlbumsAdmApi::sortAlbums($iItemId, $iTargetItemId, $sOrderType);

    }


    /**
     * Групповое удаление альбомов
     */
    protected function actionGroupAlbumDel() {

        // набор входных данных для удаления
        $aInList = $this->get( 'delItems' );

        // проверить принадлежность целевому альбому
        $aDelList = GalleryAdmApi::validateIdAlbumsList( $aInList, $this->iSectionId );

        // удалить по списку
        $iCnt = 0;
        foreach ( $aDelList as $iId ) {
            if( Gallery2AlbumsAdmApi::removeAlbum( $iId ) )
                $iCnt++;
        }

        $this->addMessage( sprintf('Удалено альбомов: %d из %d', $iCnt, count($aInList)) );

        $this->addNoticeReport("Удаление альбомов", "<pre>".print_r($aDelList, true)."</pre>", skLogger::logUsers, "GalleryAdmModule");
        
        $this->actionGetAlbums();

    }

}