<?php
/**
 * Маппер для идминского интерфейса SEO интеграций
 */
class SEOAdmMapper extends SEODataMapper {

    /**
     * Отдает набор имен полей с данными
     * @static
     * @return string[]
     */
    public static function getDataFields() {
        return array(
            'frequency',
            'priority',
            'title',
            'keywords',
            'description',
        );
    }

    // дополнительный набор параметров
    protected static function getAddParamList() {
        return array(
            'keywords' => array(
                'height' => 60
            ),
            'description' => array(
                'height' => 60
            ),
            'priority' => array(
                'minValue' =>  0,
                'maxValue' => 1,
                'step' => 0.1
            ),
            'frequency' => ExtForm::getDesc4SelectFromArray( SEODataMapper::getFrequencyList() )
        );
    }

}
