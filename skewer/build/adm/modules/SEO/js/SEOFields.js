/**
 * Поле - контейнер для SEO полей
 */
Ext.define('Ext.adm.SEOFields',{

    extend: 'Ext.form.FieldSet',

    defaultType: 'textfield',
    collapsible: true,
    collapsed: false,
    padding: '0 0 0 5',

    fieldConfig: [],

    initComponent: function(){

        var me = this;

        this.items = Ext.create('Ext.sk.FieldTypes').createFields( this.fieldConfig, this.layerName );

        me.title = '<a class="cursor_hand" onclick="changeSeoBlockState( this ); return false;">'+me.title+'</a>';

        me.callParent();

    }

});

/**
 * открывает/закрывает seo блок
 */
changeSeoBlockState = function( linkTag ) {
    var element = Ext.get(linkTag).dom.parentElement.parentElement.parentElement;
    var cont = Ext.ComponentManager.get( element.id );
    cont.expand();
};
