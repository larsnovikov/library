<?php

/**
 * @class HTMLBannersAdmApi
 * @extends skModule
 * @project Skewer
 * @package kernel
 *
 * @author Andy Mitrich, $Author: acat $
 * @version $Revision: 1219 $
 * @date 19.12.11 13:47 $
 *
 */

class HTMLBannersAdmApi {

    /**
     * Конфигурационный массив для положений
     * @var array
     */
    protected static $aBannerLocations = array(
        'left' => array('name' => 'left', 'title' => 'Левая колонка', 'pos' => 1),
        'right' => array('name' => 'right', 'title' => 'Правая колонка', 'pos' => 2),
        'content_top' => array('name' => 'content_top', 'title' => 'Верх контентной области', 'pos' => 3),
        'content_bottom' => array('name' => 'content_bottom', 'title' => 'Низ контентной области', 'pos' => 4)
    );

    /**
     * @static
     * @return array
     */
    public static function getBannerLocations(){

        $aLocations = array();
        foreach(self::$aBannerLocations as $aLocation){

            $aLocations[$aLocation['name']] = $aLocation['title'];
        };

        return $aLocations;
    }

    /**
     * @static
     * @return array
     */
    public static function getSectionTitle(){

        $oTree = new Tree();
        $aSections = $oTree->getSectionsInLine(skConfig::get('section.sectionRoot'));
        $aResult = array();

        foreach( $aSections as $aSection ){
            $aResult[$aSection['id']] = $aSection['title'];
        }

        return $aResult;
    }

    /**
     * Имя области, не найденной в конфинурационном массиве
     * @var string
     */
    protected static $sOthersTitle = 'Неактивные баннеры';

    /**
     * Метод возвращает список полей, выбираемых при отображении списка баннеров
     * @return array
     */
    public static function getListFields(){
        return array(
            'id',
            'title',
            'locationTitle',
            'section',
            'sort',
            'active',
            'on_main',
            'on_allpages',
            'on_include'
        );
    }// function getListFields()

    /**
     * Метод возвращает список полей, выбираемых при отображении формы редактирования баннера
     * @return array
     */
    public static function getDetailFields(){

        return array('id','title','content','location','section','active','on_main','on_include','on_allpages', 'sort');
    }// function getDetailFields()

    /**
     * @static
     * @param $aFields
     * @return array
     */
    public static function getModel($aFields){

        return HTMLBannersMapper::getFullParamDefList($aFields);
    }

    /**
     * @static
     * @param array $aFilter
     * @return array
     */
    public static function getListItems( $aFilter=array() ){

        $aFields = HTMLBannersMapper::getItems($aFilter);

        foreach ( $aFields['items'] as &$aField ){

            // если элемент активен и есть такое положение в конфигурации
            if ( $aField['active'] and isset( self::$aBannerLocations[$aField['location']] ) ) {

                // добавить соответствующие записи
                $aField['locationTitle'] = self::$aBannerLocations[$aField['location']]['pos'].' - '.self::$aBannerLocations[$aField['location']]['title'];

            } else {

                // нет - отнести к группе остальных
                $aField['location'] = '';
                $aField['locationTitle'] = self::$sOthersTitle;

            }

            // расстановка имен разедлов
            $iSection = $aField['section'];

            if ( $iSection ) {

                if ( !isset($aSections[$iSection]) )
                    $aSections[$iSection] = Tree::getTitleById( $iSection );

                $aField['section'] = $aSections[$iSection];
            }
        }

        return $aFields;
    }

    /**
     * @static
     * @param $iBannerId
     * @return array
     */
    public static function getBannerById($iBannerId){

        $aBanner = HTMLBannersMapper::getItem($iBannerId);

        return $aBanner;
    }

    /**
     * @static
     * @param $aData
     * @return bool
     */
    public static function updBanner($aData){

        return HTMLBannersMapper::updBanner($aData);
    }

    /**
     * @static
     * @param $iBannerId
     * @return bool|int
     */
    public static function delBanner($iBannerId){

        return HTMLBannersMapper::delItem($iBannerId);
    }

    /**
     * @static
     * @return array
     */
    public static function getBlankValues(){

        return array(
            'title' => 'Баннер',
            'active' => 1,
            'location' => 'right',
            'section' => 3,
            'sort' => '',
            'on_main' => 1,
            'on_allpages' => 1,
            'on_include' => 1
        );
    }

}// class