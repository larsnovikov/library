<?php

/* main */
$aConfig['name']     = 'HTMLBannersAdm';
$aConfig['title']    = 'Баннерная система (админ)';
$aConfig['version']  = '1.000a';
$aConfig['description']  = 'Админ-интерфейс управления баннерной системой';
$aConfig['revision'] = '0001';
$aConfig['layer']     = 'adm';

return $aConfig;