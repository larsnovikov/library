<?php

/**
 * @class HTMLBannersAdmModule
 * @extends skModule
 * @project Skewer
 * @package kernel
 *
 * @author Andy Mitrich, $Author: acat $
 * @version $Revision: 1466 $
 * @date 19.12.11 13:46 $
 *
 */

class HTMLBannersAdmModule extends AdminSectionTabModulePrototype{

    var $iSectionId;
    var $iPage = 0;
    var $iOnPage = 10;

    /**
     * @var string - Имя модуля
     */
    protected $sTabName = 'Баннеры';

    protected function preExecute() {

        // номер страницы
        $this->iPage = $this->getInt('page');

        // id текущего раздела
        $this->iSectionId = $this->getEnvParam('sectionId');
    }

    protected function actionInit() {

        $this->actionList();
    }

    /**
     * Список баннеров
     */
    protected function actionList() {

        // объект для построения списка
        $oList = new ExtList();

        $aModel = HTMLBannersAdmApi::getModel(HTMLBannersAdmApi::getListFields());

        // задать модель данных для вывода
        $oList->setFields( $aModel );

        $this->setListItems($oList);

        // сортировка
        $oList->addSorter('locationTitle');

        // группировка
        $oList->setGroupField('locationTitle');

        // добавление кнопок
        $oList->addRowBtnUpdate();
        $oList->addRowBtnDelete();

        // кнопка добавления
        $oList->addBntAdd('show');

        // вывод данных в интерфейс
        $this->setExtInterface( $oList );

    }

    protected function setListItems( ExtList &$oList ) {

        // число записей на страницу
        $oList->setPageNum( $this->iPage );
        $oList->setOnPage( $this->iOnPage );

        // добавление набора данных
        $aFilter = array(
            'limit' => $this->iOnPage ? array (
                'start' => $this->iPage*$this->iOnPage,
                'count' => $this->iOnPage
            ) : false,
        );

        // добавление набора данных
        $aItems = HTMLBannersAdmApi::getListItems($aFilter);

        $oList->setValues( $aItems['items'] );
        $oList->setTotal( $aItems['count'] );

    }

    /**
     * Отображение формы
     */
    protected function actionShow() {

        // подключить автоматический генератор форм
        $oForm = new ExtForm();

        // номер баннера
        $aData = $this->get('data');
        $iItemId = (is_array($aData) && isset($aData['id'])) ? (int)$aData['id'] : 0;

        // запись баннера
        $aItem = $iItemId ? HTMLBannersAdmApi::getBannerById($iItemId): HTMLBannersAdmApi::getBlankValues();

        // установить набор элементов формы
        $oForm->setFields( HTMLBannersAdmApi::getModel(HTMLBannersAdmApi::getDetailFields()));
        $oForm->useSpecSectionForImages();

        // установить значения для элементов
        $oForm->setValues( $aItem );

        // добавление кнопок
        $oForm->addBntSave();
        $oForm->addBntCancel();
        if ( $iItemId ) {
            $oForm->addBntSeparator('->');
            $oForm->addBntDelete();
        }

        // вывод данных в интерфейс
        $this->setExtInterface( $oForm );
    }

    /**
     * Сохранение баннера
     */
    protected function actionSave() {

        // запросить данные
        $aData = $this->get( 'data' );

        // есть данные - сохранить
        if ( $aData )
            HTMLBannersAdmApi::updBanner( $aData );

        if ( isset($aData['id']) && $aData['id'] )
            skLogger::addNoticeReport("Редактирование баннера",skLogger::buildDescription($aData),skLogger::logUsers,$this->getModuleNameAdm());
        else {
            unset($aData['id']);
            skLogger::addNoticeReport("Создание баннера",skLogger::buildDescription($aData),skLogger::logUsers,$this->getModuleNameAdm());
        }

        // вывод списка
        $this->actionList();

    }

    /**
     * Удаляет запись
     */
    protected function actionDelete() {

        // запросить данные
        $aData = $this->get( 'data' );

        // id записи
        $iItemId = ( is_array($aData) and isset($aData['id']) ) ? (int)$aData['id'] : 0;

        // удаление
        HTMLBannersAdmApi::delBanner( $iItemId );

        skLogger::addNoticeReport('Удаление баннера', skLogger::buildDescription($aData), skLogger::logUsers, $this->getModuleNameAdm());

        // вывод списка
        $this->actionList();

    }

    /**
     * Установка служебных данных
     * @param \ExtPrototype $oExtIface
     */
    protected function setServiceData( ExtPrototype $oExtIface ) {

        // установить заголовок модуля
        $oExtIface->setTitle( $this->sTabName );

        // установить данные для передачи интерфейсу
        $oExtIface->setServiceData( array(
            'sectionId' => $this->iSectionId,
            'page' => $this->iPage
        ) );

    }
}// class