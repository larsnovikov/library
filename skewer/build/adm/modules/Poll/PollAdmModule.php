<?php

/**
 * @class PollAdmModule
 * @extends skModule
 * @project Skewer
 * @package kernel
 *
 * @author Andy Mitrich, $Author: acat $
 * @version $Revision: 1172 $
 * @date 29.12.11 10:46 $
 *
 */

class PollAdmModule extends AdminSectionTabModulePrototype{

    var $iSectionId;
    var $iPage = 0;
    var $iOnPage = 0;
    var $iCurrentPoll = 0;
    /**
     * @var null|\PollAdmApi
     */
    var $oPollApi = NULL;

    /**
     * @var string - Имя модуля
     */
    protected $sTabName = 'Голосование';

    protected function preExecute() {

        // номер страницы
        $this->iPage = $this->getInt('page');
        // id текущего раздела
        $this->iSectionId = $this->getEnvParam('sectionId');
        $this->iCurrentPoll = $this->getInt('poll_id', 0);
    }

    protected function actionInit() {

        $this->actionList();
    }

    /**
     * Список опросов
     */
    protected function actionList() {

        // объект для построения списка
        $oList = new ExtList();

        $aModel = PollAdmApi::getPollModel(PollAdmApi::getPollListFields());

        // задать модель данных для вывода
        $oList->setFields( $aModel );

        // число записей на страницу
        $oList->setOnPage( $this->iOnPage );

        // добавление набора данных
        $aFilter = array(
            'limit' => $this->iOnPage ? array (
                'start' => $this->iPage*$this->iOnPage,
                'count' => $this->iOnPage
            ) : false,
        );

        // добавление набора данных
        $aItems = PollAdmApi::getPollList($aFilter);

        $oList->setValues( $aItems['items'] );
        $oList->setTotal( $aItems['count'] );

        // вывод данных в интерфейс
        $this->setExtInterface( $oList );

        /**
         * Интерфейс
         */
        // сортировка
        //$oList->addSorter('sort');
        //$oList->addSorter('locationTitle');

        // группировка
        $oList->setGroupField('locationTitle');

        // добавление кнопок
        $oList->addRowBtnUpdate();
        $oList->addRowBtnDelete();

        // кнопка добавления
        $oList->addBntAdd('show');

        // вывод данных в интерфейс
        $this->setExtInterface( $oList );

    }

    /**
     * Отображение формы
     */
    protected function actionShow() {

        // подключить автоматический генератор форм
        $oForm = new ExtForm();

        // номер новости
        $aData = $this->get('data');
        $iItemId = (is_array($aData) && isset($aData['id'])) ? (int)$aData['id'] : 0;
        if ( !$iItemId ) $iItemId = $this->iCurrentPoll;

        // запись новости
        $aItem = $iItemId ? PollAdmApi::getPollById($iItemId): PollAdmApi::getPollBlankValues();

        // установить набор элементов формы
        $oForm->setFields( PollAdmApi::getPollModel(PollAdmApi::getPollDetailFields()));

        // установить значения для элементов
        $oForm->setValues( $aItem );

        // добавление кнопок
        $oForm->addBntSave();
        $oForm->addBntCancel();

        if ( $iItemId ){
            $oForm->addDockedItem(array(
                'text' => 'Ответы',
                'iconCls' => 'icon-edit',
                'state' => 'answerList',
                'action' => 'answerList'
            ));

            $this->iCurrentPoll = $iItemId;

            $oForm->addBntSeparator('->');
            $oForm->addBntDelete();
        }

        // вывод данных в интерфейс
        $this->setExtInterface( $oForm );
    }

    /**
     * Список опросов
     */
    protected function actionAnswerList() {

        // объект для построения списка
        $oList = new ExtList();

        $aModel = PollAdmApi::getAnswerModel(PollAdmApi::getAnswerListFields());

        // задать модель данных для вывода
        $oList->setFields( $aModel );

        $this->setAnswerListItems($oList);

        // вывод данных в интерфейс
        $this->setExtInterface( $oList );

        // добавление кнопок
        $oList->addRowBtn(array(
            'tooltip' =>'_upd',
            'iconCls' => 'icon-edit',
            'action' => 'showAnswerForm',
            'state' => 'edit_form'
        ));

        $oList->addRowBtn(array(
            'text' => '_delete',
            'iconCls' => 'icon-delete',
            'state' => 'answerDelete',
            'action' => 'answerDelete'
        ));

        // кнопка добавления
        $oList->addBntAdd('showAnswerForm');
        $oList->addDockedItem(array(
            'text' => 'Назад',
            'iconCls' => 'icon-cancel',
            'state' => 'edit_form',
            'action' => 'show'
        ));

        // вывод данных в интерфейс
        $this->setExtInterface( $oList );

    }

    /**
     * @param ExtList $oList
     */
    protected function setAnswerListItems( ExtList &$oList ) {

        $aData = $this->get( 'data' );

        $iItemId = ( is_array($aData) and isset($aData['id']) ) ? (int)$aData['id'] : 0;

        if ( !$iItemId )
            $iItemId = (isset($aData['parent_poll']) ) ? (int)$aData['parent_poll'] : $iItemId;

        if ( !$iItemId )
            $iItemId = $this->iCurrentPoll;

        // добавление набора данных
        $aItems = PollAdmApi::getAnswerList($iItemId);

        $oList->setValues( $aItems['items'] );
        $oList->setTotal( $aItems['count'] );

    }

    /**
     * Отображение формы
     */
    protected function actionShowAnswerForm() {

        // подключить автоматический генератор форм
        $oForm = new ExtForm();

        // номер новости
        $aData = $this->get('data');

        $iItemId = (is_array($aData) && isset($aData['answer_id'])) ? (int)$aData['answer_id'] : 0;

        // запись новости
        $aItem = $iItemId ? PollAdmApi::getAnswerById($iItemId): PollAdmApi::getAnswerBlankValues();

        // установить набор элементов формы
        $oForm->setFields( PollAdmApi::getAnswerModel(PollAdmApi::getAnswerDetailFields()), $iItemId );

        // установить значения для элементов
        $oForm->setValues( $aItem );

        $oForm->addDockedItem(array(
            'text' => '_save',
            'iconCls' => 'icon-save',
            'state' => 'answerSave',
            'action' => 'answerSave'
        ));
        $oForm->addDockedItem(array(
            'text' => '_cancel',
            'iconCls' => 'icon-cancel',
            'state' => 'answerList',
            'action' => 'answerList'
        ));

        if ( $iItemId ) {
            $oForm->addBntSeparator('->');
            $oForm->addDockedItem(array(
                'text' => 'Удалить',
                'iconCls' => 'icon-delete',
                'state' => 'answerDelete',
                'action' => 'answerDelete'
            ));
        }

        // вывод данных в интерфейс
        $this->setExtInterface( $oForm );
    }

    /**
     * Сохранение опроса
     */
    protected function actionSave() {

        // запросить данные
        $aData = $this->get( 'data' );

        // есть данные - сохранить
        if ( $aData )
            PollAdmApi::updPoll( $aData );

        if ( isset($aData['id']) && $aData['id'] )
            skLogger::addNoticeReport("Редактирование опроса",skLogger::buildDescription($aData),skLogger::logUsers,$this->getModuleNameAdm());
        else {
            unset($aData['id']);
            skLogger::addNoticeReport("Создание опроса",skLogger::buildDescription($aData),skLogger::logUsers,$this->getModuleNameAdm());
        }

        // вывод списка
        $this->actionList();

    }

    /**
     * Сохранение варианта ответа
     */
    protected function actionAnswerSave() {

        // запросить данные
        $aData = $this->get( 'data' );

        $aData['parent_poll'] = $this->iCurrentPoll;

        // есть данные - сохранить
        if ( $aData )
            PollAdmApi::updAnswer( $aData );

        if ( isset($aData['id']) && $aData['id'] )
            skLogger::addNoticeReport("Редактирование варианта ответа",skLogger::buildDescription($aData),skLogger::logUsers,$this->getModuleNameAdm());
        else {
            unset($aData['id']);
            skLogger::addNoticeReport("Добавление варианта ответа",skLogger::buildDescription($aData),skLogger::logUsers,$this->getModuleNameAdm());
        }

        // вывод списка
        $this->actionAnswerList();

    }

    /**
     * Удаляет запись
     */
    protected function actionDelete() {

        // запросить данные
        $aData = $this->get( 'data' );

        // id записи
        $iItemId = ( is_array($aData) and isset($aData['id']) ) ? (int)$aData['id'] : 0;

        // удаление
        PollAdmApi::delPoll( $iItemId );

        skLogger::addNoticeReport("Удаление опроса",skLogger::buildDescription($aData),skLogger::logUsers,$this->getModuleNameAdm());

        // вывод списка
        $this->actionList();

    }

    /**
     * Удаляет запись
     */
    protected function actionAnswerDelete() {

        // запросить данные
        $aData = $this->get( 'data' );

        $aData['parent_poll'] = $this->iCurrentPoll;

        // удаление
        PollAdmApi::delAnswer( $aData );

        skLogger::addNoticeReport("Удаление варианта ответа",skLogger::buildDescription($aData),skLogger::logUsers,$this->getModuleNameAdm());

        // вывод списка
        $this->actionAnswerList();

    }


    /**
     * Установка служебных данных
     * @param \ExtPrototype $oExtIface
     */
    protected function setServiceData( ExtPrototype $oExtIface ) {

        // установить заголовок модуля
        $oExtIface->setTitle( $this->sTabName );

        // установить данные для передачи интерфейсу
        $oExtIface->setServiceData( array(
            'sectionId' => $this->iSectionId,
            'page' => $this->iPage,
            'poll_id' => $this->iCurrentPoll
        ) );

    }

}// class