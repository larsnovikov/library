<?php

/* main */
$aConfig['name']     = 'PollAdm';
$aConfig['title']    = 'Голосование (админ)';
$aConfig['version']  = '1.000a';
$aConfig['description']  = 'Админ-интерфейс управления модулем голосования';
$aConfig['revision'] = '0001';
$aConfig['layer']     = 'adm';

return $aConfig;
