<?php

/**
 * @class PollAdmApi
 * @extends skModule
 * @project Skewer
 * @package kernel
 *
 * @author Andy Mitrich, $Author: armit $
 * @version $Revision: 249 $
 * @date 29.12.11 10:46 $
 *
 */

class PollAdmApi {

    /**
     * Конфигурационный массив для положений
     * @var array
     */
    protected static $aPollLocations = array(
        'left' => array('name' => 'left', 'title' => 'Левая колонка', 'pos' => 1),
        'right' => array('name' => 'right', 'title' => 'Правая колонка', 'pos' => 2)
    );

    /**
     * @static
     * @return array
     */
    public static function getPollLocations(){

        $aLocations = array();
        foreach(self::$aPollLocations as $aLocation){

            $aLocations[$aLocation['name']] = $aLocation['title'];
        };

        return $aLocations;
    }

    /**
     * Имя области, не найденной в конфинурационном массиве
     * @var string
     */
    protected static $sOthersTitle = 'Неактивные опросы';

    /**
     * @static
     * @return array
     */
    public static function getSectionTitle(){

        $oTree = new Tree();
        $aSections = $oTree->getSectionsInLine(skConfig::get('section.sectionRoot'));
        unset($oTree);
        $aResult = array();

        foreach( $aSections as $aSection ){
            $aResult[$aSection['id']] = $aSection['title'];
        }

        return $aResult;
    }

    /**
     * Метод возвращает список полей, выбираемых при отображении списка опросов
     * @return array
     */
    public static function getPollListFields(){
        return array(
            'id',
            'title',
            'question',
            'location',
            'locationTitle',
            'section',
            'sort',
            'active',
            'on_main',
            'on_allpages',
            'on_include'
        );
    }// function getListFields()

    /**
     * Метод возвращает список полей, выбираемых при отображении списка вариантов ответа для опроса
     * @return array
     */
    public static function getAnswerListFields(){
        return array(
            'answer_id',
            'title',
            'parent_poll',
            'value',
            'sort'
        );
    }// function getAnswersListFields()

    /**
     * Метод возвращает список полей, выбираемых при отображении формы редактирования опроса
     * @return array
     */
    public static function getPollDetailFields(){
        return array(
            'id',
            'title',
            'question',
            'location',
            'section',
            'active',
            'on_main',
            'on_include',
            'on_allpages',
            'sort'
        );
    }// function getDetailFields()

    /**
     * @return array
     */
    public static function getAnswerDetailFields(){
        return array(
            'answer_id',
            'title',
            'parent_poll',
            'value',
            'sort'
        );
    }// function getAnswersFields()

    /**
     * @static
     * @param $aFields
     * @return array
     */
    public static function getPollModel($aFields){

        return PollMapper::getFullParamDefList($aFields);
    }

    /**
     * @static
     * @param $aFields
     * @return array
     */
    public static function getAnswerModel($aFields){

        return Poll2AnswersMapper::getFullParamDefList($aFields);
    }

    /**
     * @static
     * @return array
     */
    public static function getPollList(){

        $aFields = PollMapper::getItems();

        foreach ( $aFields['items'] as &$aField ){

            // если элемент активен и есть такое положение в конфигурации
            if ( $aField['active'] and isset( self::$aPollLocations[$aField['location']] ) ) {

                // добавить соответствующие записи
                $aField['locationTitle'] = self::$aPollLocations[$aField['location']]['pos'].' - '.self::$aPollLocations[$aField['location']]['title'];

            } else {

                // нет - отнести к группе остальных
                $aField['location'] = '';
                $aField['locationTitle'] = self::$sOthersTitle;

            }

            // расстановка имен разедлов
            $iSection = $aField['section'];

            if ( $iSection ) {

                if ( !isset($aSections[$iSection]) )
                    $aSections[$iSection] = Tree::getTitleById( $iSection );

                $aField['section'] = $aSections[$iSection];
            }
        }

        return $aFields;
    }

    /**
     * @static
     * @param $iItemId
     * @return array|bool
     */
    public static function getAnswerList($iItemId){

        if ( !$iItemId ) return false;

        $aFilter['where_condition'] = array(
            'parent_poll' => array(
                'sign' => '=',
                'value' => $iItemId
            )
        );

        $aAnswers = Poll2AnswersMapper::getItems($aFilter);

        return $aAnswers;
    }

    /**
     * @static
     * @param $iPollId
     * @return array+
     */
    public static function getPollById($iPollId){

        $aFilter['where_condition'] = array(
            'id' => array(
                'sign' => '=',
                'value' => $iPollId
            )
        );

        $aPoll = PollMapper::getItem($aFilter);

        return $aPoll;
    }

    /**
     * @static
     * @param $iAnswerId
     * @return array
     */
    public static function getAnswerById($iAnswerId){

        $aFilter['where_condition'] = array(
            'answer_id' => array(
                'sign' => '=',
                'value' => $iAnswerId
            )
        );

        $aAnswer = Poll2AnswersMapper::getItem($aFilter);

        return $aAnswer;
    }

    /**
     * @static
     * @param $aData
     * @return bool
     */
    public static function updPoll($aData){

        return PollMapper::updPoll($aData);
    }

    /**
     * @static
     * @param $iPollId
     * @return bool
     */
    public static function delPoll($iPollId){

        return PollMapper::delPoll($iPollId);
    }

    /**
     * @static
     * @param $aData
     * @return bool|int
     */
    public static function updAnswer($aData){

        return Poll2AnswersMapper::updAnswer($aData);
    }

    /**
     * @static
     * @param $aInputData
     * @return bool|mysqli_result
     */
    public static function delAnswer($aInputData){

        return Poll2AnswersMapper::delAnswer($aInputData);
    }

    /**
     * @static
     * @return array
     */
    public static function getPollBlankValues(){
        return array(
            'title' => 'Опрос',
            'active' => 1,
            'location' => 'left',
            'section' => 3,
            'sort' => ''
        );
    }

    /**
     * @static
     * @return array
     */
    public static function getAnswerBlankValues(){
        return array(
            'title' => 'Вариант ответа',
            'value' => '0'
        );
    }


}// class