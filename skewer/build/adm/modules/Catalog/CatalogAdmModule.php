<?php
/**
 * Каталог
 */

class CatalogAdmModule extends AdminTabModulePrototype {

    protected $sTabName = 'Каталог';

    public function actionInit(){

        // установка заголовка
        $this->setPanelName( 'Список карточек' );

        // объект для построения списка
        $oList = new ExtFTList();

        // задать модель данных для вывода
        $oList->setFields( Catalog::getEntityModel(), 'editor' );

        $oList->setValues( array() );

        $oList->addRowBtnDelete();

        // вывод данных в интерфейс
        $this->setExtInterface( $oList );
    }

}
