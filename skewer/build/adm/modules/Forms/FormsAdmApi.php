<?php

/**
 * @class FormsAdmApi
 * @extends skModule
 * @project Skewer
 * @package kernel
 *
 * @author Andy Mitrich, $Author: sapozhkov $
 * @version $Revision: 1346 $
 * @date 16.01.12 13:16 $
 *
 */

class FormsAdmApi {

    /* Common Part */

    public function __construct(){

        $aSettings = skConfig::get('buildConfig.Page.modules.Forms.settings');

        $this->aValidationTypes = $aSettings['validation_types'];
        $this->aHandlerTypes = $aSettings['handler_types'];
        $this->aFieldTypes = $aSettings['field_types'];
    }

    private $aHandlerTypes = array();
    private $aValidationTypes = array();
    private $aFieldTypes = array();

    public function getFormModel($aFields){

        return FormsMapper::getFullParamDefList($aFields);
    }

    public function getParamModel($aFields){

        return Forms2ParamsMapper::getFullParamDefList($aFields);
    }

    public function getAnswerModel($aFields){

        return Forms2AnswerMapper::getFullParamDefList($aFields);
    }

    public function getTemplatesForForms(){

        $aTemplateForms = FormsMapper::getTemplatesForForms(false);

        //array_unshift($aTemplateForms, array('form_id'=>'', 'form_title'=>'Новая форма')); todo
        array_unshift($aTemplateForms, array('form_id'=>0, 'form_title'=>' -- Форма не задана --'));

        return $aTemplateForms;
    }

    public function getTemplatesForNewForms(){

        $aTemplateForms = FormsMapper::getTemplatesForForms();

        array_unshift($aTemplateForms, array('form_id'=>'', 'form_title'=>' -- Новая форма --'));

        return $aTemplateForms;
    }

    public function getTemplatesData($iForm = 0){

        return array(
            'new_form' => array(
                'name' => 'new_form',
                'type' => 's',
                'view' => 'select',
                'title' => 'Выбрать форму',
                'valueField' => 'form_id',
                'displayField' => 'form_title',
                'value' => $iForm,
                'store' => array(
                    'fields' => array(
                        '0' => 'form_id',
                        '1' => 'form_title',
                    ),
                    'data' => $this->getTemplatesForForms()
                )
            )
        );
    }

    public function getNewFormData(){

        return array(
            'new_form' => array(
                'name' => 'new_form',
                'type' => 's',
                'view' => 'select',
                'title' => 'Выбрать шаблон для формы',
                'valueField' => 'form_id',
                'displayField' => 'form_title',
                'store' => array(
                    'fields' => array(
                        '0' => 'form_id',
                        '1' => 'form_title',
                    ),
                    'data' => $this->getTemplatesForNewForms()
                )
            )
        );
    }

    /* Forms Execute */

    public function addFormByTemplate($iSectionId, $iTemplateId) {

        if( !$iTemplateId ) return false;

        $oForm = FormsMapper::getFormById($iTemplateId, true);
        $oForm = Forms2ParamsMapper::getParams($oForm);

        if( !($oForm instanceof FormsEntity) ) return false;

        $oForm->setId(0);
        $oForm->setSectionId($iSectionId);

        $aParams = $oForm->getParams();

        $aFormData = $oForm->getData();
        unset($aFormData['form_id']);

        $iNewId = FormsMapper::updForm($aFormData);
        foreach($aParams as $oParam)
            /** @var $oParam Forms2ParamsEntity */
            if( $oParam instanceof Forms2ParamsEntity ){

                $oParam->setId(0);
                $oParam->setFormId($iNewId);
                $aParamData = $oParam->getData();
                Forms2ParamsMapper::updParameter($aParamData, false);
            }

        // link form to section
        Forms2SectionMapper::linkFormToSection($iNewId,$iSectionId);

        return $iNewId;
    }// func

    /**
     * Метод возвращает список полей, выбираемых при отображении списка форм
     * @return array
     */
    public function getFormListFields(){
        return array(
            'form_id',
            'form_title',
            'form_section',
            'form_handler_type',
            'form_handler_value',
            'form_active'
        );
    }// function getListFields()

    /**
     * Метод возвращает список полей, выбираемых при отображении формы редактирования
     * @return array
     */
    public function getFormDetailFields(){

        return array(
            'form_id',
            'form_title',
            'form_section',
            'form_handler_type',
            'form_handler_value',
            'form_redirect',
            'form_active',
            'form_captcha',
            'form_is_template',
            'form_answer'
        );
    }// function getDetailFields()

    public function getFormById($iFormId){

        $aForm = FormsMapper::getItem($iFormId);

        return $aForm;
    }

    public function getFormList(){

        $aFields = FormsMapper::getItems();
        $aSections = array();

        foreach ( $aFields['items'] as &$aField ){

            // расстановка имен разедлов
            $iSection = $aField['form_section'];
            if ( $iSection ) {
                if ( !isset($aSections[$iSection]) )
                    $aSections[$iSection] = Tree::getTitleById( $iSection );
                $aField['form_section'] = $aSections[$iSection];
            }

            $sHandlerType = $aField['form_handler_type'];
            if ( $sHandlerType && isset($this->aHandlerTypes[$sHandlerType]) ) {

                $aField['form_handler_type'] = $this->aHandlerTypes[$sHandlerType];
            }

            // расстановка "+" вместо 1
            foreach ( array('form_active') as $sName )
                $aField[$sName] = $aField[$sName] ? '+' : '';

        }

        return $aFields;
    }

    public function getBlankValues($iSectionId, $sFormTitle = 'Форма'){

        return array(
            'form_title' => $sFormTitle,
            'form_active' => 1,
            'form_captcha' => 1,
            'form_section' => $iSectionId,
            'form_handler_type' => 'toMail',
            'form_answer' => 0
        );
    }

    public function updForm($aData){

        return FormsMapper::updForm($aData);
    }

    public function delForm($iFormId){

        FormsMapper::delForm($iFormId);
        // TODO удаление привязки к разделам !!!
        Forms2ParamsMapper::delParameterByForm($iFormId);
        return true;
    }

    public function checkForms($iSectionId){

        return Forms2SectionMapper::getFormBySectionId($iSectionId);
        //return FormsMapper::checkFormsBySectionId($iSectionId);
    }

    /* Parameters Execute */

    /**
     * @return array
     */
    public function getParamsListFields(){
        return array(
            'param_id',
            'form_id',
            'param_title',
            'param_name',
            'param_type',
            'param_required'
        );
    }// function getAnswersListFields()

    /**
     * @return array
     */
    public function getParamsDetailFields(){
        return array(
            'param_id',
            'form_id',
            'param_title',
            'param_name',
            'param_description',
            'param_type',
            'param_required',
            'param_default',
            'param_maxlength',
            'param_validation_type',
            'param_priority'
        );
    }// function getAnswersListFields()

    public function getAnswerDetailFields(){
        return array(
            'answer_id',
            'form_id',
            'answer_title',
            'answer_body'
        );
    }// function getAnswersListFields()

    public function getParamsList($iItemId){

        if ( !$iItemId ) return false;

        $aFilter['where_condition'] = array(
            'form_id' => array(
                'sign' => '=',
                'value' => $iItemId
            )
        );

        $aFilter['order'] = array('field' => 'param_priority','way' => 'ASC');

        $aParams = Forms2ParamsMapper::getItems($aFilter);

        foreach ( $aParams['items'] as &$aField ){

            $sFieldType = $aField['param_type'];
            if ( $sFieldType ) {

                $aField['param_type'] = $this->aFieldTypes[$sFieldType];
            }

            // расстановка "+" вместо 1
            foreach ( array('param_required') as $sName )
                $aField[$sName] = $aField[$sName] ? '+' : '';

        }

        return $aParams;
    }

    public function getParamById($iParamId){

        $aFilter['where_condition'] = array(
            'param_id' => array(
                'sign' => '=',
                'value' => $iParamId
            )
        );

        $aParam = Forms2ParamsMapper::getItem($aFilter);

        return $aParam;
    }

    public function getAnswerByFormId($iFormId){

        $aFilter['where_condition'] = array(
            'form_id' => array(
                'sign' => '=',
                'value' => $iFormId
            )
        );

        $aParam = Forms2AnswerMapper::getItem($aFilter);

        return $aParam;
    }

    public static function getParamByName($sParamName, $sForm=false){

        $aFilter['where_condition'] = array(
            'param_name' => array(
                'sign' => '=',
                'value' => $sParamName
            )
        );

        if($sForm)
            $aFilter['where_condition']['form_id'] = array(
                'sign' => '=',
                'value' => $sForm
            );

        $aParam = Forms2ParamsMapper::getItem($aFilter);

        return $aParam;
    }

    /**
     * Проверка соблюдения уникальности имени параметра в форме
     * @param $aParam
     * @param bool $iForm
     * @return bool
     */
    public static function isUniqueParamName(&$aParam, $iForm=false){

        $iNum = 0;

        $bFlag = true;

        do {

            $sCurAlias = $aParam['param_name'];

            if($iNum) $sCurAlias .= $iNum;

            $aItem = FormsAdmApi::getParamByName($sCurAlias, $iForm);

            if( empty($aItem) || (isset($aItem['param_id']) && $aItem['param_id'] == $aParam['param_id']) ){

                $aParam['param_name'] = $sCurAlias;

                $bFlag = false;

            }

            $iNum++;

            if($iNum > 3000) $bFlag = false;

        } while( $bFlag );

        return true;
    }


    public function updParam($aData){

        return Forms2ParamsMapper::updParameter($aData);
    }

    public function delParam($aInputData){

        return Forms2ParamsMapper::delParameter($aInputData);
    }

    /**
     * @return array
     */
    // TODO Необходимо генерировать служебное имя
    public function getBlankParamValues(){

        return array(
            'param_title' => 'Поле формы',
            'param_name' => 'fieldname',
            'param_type' => 1,
            'param_validation_type' => 'text',
            'param_maxlength' => 255
        );
    }

    public static function getSectionTitle () {

        $oTree = new Tree();
        $aSections = $oTree->getSectionsInLine(skConfig::get('section.sectionRoot'));
        unset($oTree);
        $aResult = array();

        foreach( $aSections as $aSection ){
            $aResult[$aSection['id']] = $aSection['title'];
        }

        return $aResult;

    }

    public function createNewForm ($iSectionId, $sSectionName = 'имя не задано') {

        $aTempates = $this->getTemplatesForForms();

        if( count($aTempates) > 1 )
            $iTemplateId = $aTempates[1]['form_id']; // todo
        else throw new Exception('Ошибка! Не найден наблон форм');

        $iNewFormId = $this->addFormByTemplate($iSectionId, $iTemplateId);

        $aData = array( 'form_id' => $iNewFormId,
                        'form_title' => $sSectionName,
                        'form_is_template' => 0);

        $this->updForm($aData);

        return $iNewFormId;
    }

    public function deleteFormBySection ( $iSectionId ) {

        FormsMapper::delFormBySection($iSectionId);

        return true;
    }

    public static function sortParams($aItem, $aTarget, $sOrderType='before') {

        $sSortField = 'param_priority';

        // должны быть в одной форме
        if( $aItem['form_id'] != $aTarget['form_id'] )
            return false;

        // выбираем напрвление сдвига
        if( $aItem[$sSortField] > $aTarget[$sSortField] ){

            $iStartPos = $aTarget[$sSortField];
            if( $sOrderType=='before' ) $iStartPos--;
            $iEndPos = $aItem[$sSortField];
            $iNewPos = $sOrderType=='before' ? $aTarget[$sSortField] : $aTarget[$sSortField] + 1;
            Forms2ParamsMapper::shiftParamPosition($aItem['form_id'], $iStartPos, $iEndPos, '+');
            Forms2ParamsMapper::changeParamPosition($aItem['param_id'],$iNewPos);

        } else {

            $iStartPos = $aItem[$sSortField];
            $iEndPos = $aTarget[$sSortField];
            if( $sOrderType=='after' ) $iEndPos++;
            $iNewPos = $sOrderType=='after' ? $aTarget[$sSortField] : $aTarget[$sSortField] - 1;
            Forms2ParamsMapper::shiftParamPosition($aItem['form_id'], $iStartPos, $iEndPos, '-');
            Forms2ParamsMapper::changeParamPosition($aItem['param_id'],$iNewPos);

        }

        return true;

    }

    public static function linkFormToSection($iFormId, $iSectionId) {

        Forms2SectionMapper::delFormBySection($iSectionId);

        if( $iFormId )
            Forms2SectionMapper::linkFormToSection($iFormId, $iSectionId);

        return true;
    }

} //class
