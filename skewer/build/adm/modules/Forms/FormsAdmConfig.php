<?php

/* main */
$aConfig['name']     = 'FormsAdm';
$aConfig['title']    = 'Конструктор форм (админ)';
$aConfig['version']  = '1.000a';
$aConfig['description']  = 'Админ-интерфейс управления конструктором форм';
$aConfig['revision'] = '0001';
$aConfig['layer']     = 'adm';

return $aConfig;
