<?php
/**
 * Модуль добавления формы для раздела
 * @class FormsAdmModule
 * @extends skModule
 * @project Skewer
 * @package kernel
 *
 * @author kolesnikov, $Author: sapozhkov $
 * @version $Revision:  $
 * @date $Date: $
 *
 */
class FormsAdmModule extends AdminSectionTabModulePrototype {

    public $iSectionId;
    public $iCurrentForm = 0;
    public $enableSettings = 0;
    /**
     * @var null|\FormsAdmApi
     */
    var $oFormApi = NULL;

    /**
     * @var string - Имя модуля
     */
    protected $sTabName = 'Конструктор форм';

    protected function preExecute() {

        CurrentAdmin::testControlPanelAccess();

        // id текущего раздела
        $this->iCurrentForm = $this->getInt('form_id');

        if ( $this->getInt('sectionId') )
            $this->iSectionId = $this->getInt('sectionId');

        if ( !$this->iSectionId )
            $this->iSectionId = $this->getEnvParam('sectionId');

        $this->oFormApi = new FormsAdmApi();
    }

    protected function actionInit() {

        $this->setPanelName('Выбор формы');

        $this->actionPreList();

    }

    // форма задания формы для раздела
    protected function actionPreList(){

        // подключить автоматический генератор форм
        $oForm = new ExtForm();

        $iForm = (int)$this->oFormApi->checkForms($this->iSectionId);

        $aTItems = $this->oFormApi->getTemplatesData($iForm);

        // установить набор элементов формы
        $oForm->setFields( $aTItems );

        $oForm->addBntSave( 'linkFormToSection' );

        $oForm->addDockedItem(array(
            'text' => 'Добавить новую форму',
            'iconCls' => 'icon-add',
            'state' => 'addNewForm',
            'action' => 'addNewForm'
        ));

        // вывод данных в интерфейс
        $this->setExtInterface( $oForm );
    }


    protected function actionLinkFormToSection() {

        $aData = $this->get('data');

        $iNewForm = ( isset($aData['new_form']) && $aData['new_form'] )? $aData['new_form']: 0;

        FormsAdmApi::linkFormToSection($iNewForm, $this->iSectionId);
        $this->addNoticeReport("Изменение формы в разделе", "id раздела: $this->iSectionId", skLogger::logUsers, "FormsAdmModule");
        
        $this->actionPreList();

    }

    // интерфейс выбора шаблона для новой формы
    protected function actionAddNewForm() {

        // подключить автоматический генератор форм
        $oForm = new ExtForm();

        //$iForm = $this->oFormApi->checkForms($this->iSectionId);

        $aTItems = $this->oFormApi->getNewFormData();

        // установить набор элементов формы
        $oForm->setFields( $aTItems );

        $oForm->addBntSave( 'saveNewForm' );

        // вывод данных в интерфейс
        $this->setExtInterface( $oForm );
    }

    // сохранение новой формы по шаблону
    protected function actionSaveNewForm() {

        $aData = $this->get('data');

        $iItemId = isset($aData['new_form']) ? $aData['new_form'] : 0;

        if( $iItemId )
            $this->actionShowByTemplate($iItemId);
        else
            $this->actionShow( array('form_title'=>Tree::getTitleById($this->iSectionId)) );
    }

    /**
     * Отображение формы
     */
    protected function actionShow($aSubData = array()) {

        // подключить автоматический генератор форм
        $oForm = new ExtForm();

        // номер опроса
        $aData = $this->get('data');
        if( is_array($aSubData) && !empty($aSubData) ) $aData = array_merge($aData, $aSubData);

        $iItemId = (is_array($aData) && isset($aData['form_id'])) ? (int)$aData['form_id'] : 0;

        if ( !$iItemId ) $iItemId = $this->iCurrentForm;

        $aItem = $iItemId ? $this->oFormApi->getFormById($iItemId): $this->oFormApi->getBlankValues($this->iSectionId,isset($aData['form_title'])?$aData['form_title']:'Форма');

        // установить набор элементов формы
        $aItems = $this->oFormApi->getFormModel($this->oFormApi->getFormDetailFields());

        // скрываем поле раздел для показа до лучших времен =) //if ( $this->enableSettings==0 )
        unset( $aItems['form_section'] );

        // изменение интерфейса для разных политик администраторов
        //$aItems['form_section']['disabled'] = true;
        if( !CurrentAdmin::isSystemMode() ){
            unset( $aItems['form_handler_type'] );
            unset( $aItems['form_is_template'] );
            //unset( $aItems['form_title'] );
        } else {
            $aItems['form_handler_value']['title'] = 'Значение обработчика';
        }

        $aItems['form_handler_value']['subtext'] = 'По умолчанию используется адрес <b>'.Parameters::getValByName(3,'.', 'email').'</b>';
        $aItems['form_redirect']['subtext'] = 'По умолчанию используется текущий раздел';

        $oForm->setFields( $aItems );

        // установить значения для элементов
        $oForm->setValues( $aItem );

        $oForm->addBntSave();
        $oForm->addBntCancel();

        // вывод данных в интерфейс
        $this->setExtInterface( $oForm );
    }

    /**
     * Сохранение опроса
     */
    protected function actionSave() {

        // запросить данные
        $aData = $this->get( 'data' );

        // todo перенести проверки в апи и сделать одним методом с FormAmdModule
        if( !isset($aData['form_handler_type']) || !$aData['form_handler_type'] )
            $aData['form_handler_type'] = 'toMail';

        // есть данные - сохранить
        if ( $aData )
            $this->iCurrentForm = $this->oFormApi->updForm( $aData );

        $this->actionShow();
    }

    protected function actionShowByTemplate($iItemId) {

        if ( !$iItemId ) return false;
        // есть данные - сохранить

        $iNewForm = $this->oFormApi->addFormByTemplate( $this->iSectionId, $iItemId );

        $this->iCurrentForm = $iNewForm;

        // вывод списка
        $this->actionShow();

        return true;
    }

    /**
     * Удаляет запись
     */
    protected function actionDelete() {

        // запросить данные
        $aData = $this->get( 'data' );

        // id записи
        $iItemId = ( is_array($aData) and isset($aData['form_id']) ) ? (int)$aData['form_id'] : 0;

        // удаление
        $this->oFormApi->delForm( $iItemId );

        $this->actionPreList();
    }


    /**
     * Установка служебных данных
     * @param \ExtPrototype $oExtIface
     */
    protected function setServiceData( ExtPrototype $oExtIface ) {

        // установить заголовок модуля
        $oExtIface->setTitle( $this->sTabName );

        // установить данные для передачи интерфейсу
        $oExtIface->setServiceData( array(
            'section' => $this->iSectionId,
            'form_id' => $this->iCurrentForm,
            'enableSettings' => $this->enableSettings
        ) );

    }

}
