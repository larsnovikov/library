<?php
/**
 * API для админской части новостной системы
 */

class NewsAdmApi {

    /**
     * @static Возвращает набор полей для отображения в списке
     * @return array
     */
    public static function getListFields(){
        return
            array(
                'id',
                'title',
                'publication_date',
                'publication_show_val',
                'active',
                'on_main'
            );
    }

    /**
     * Набор полей, редактируемых в списке
     * @static
     * @return array
     */
    public static function getEditableListFields() {
        return array(
            'active',
            'on_main'
        );
    }

    public static function getDetailFields(){
        return
            array(
                'id',
                'title',
                'publication_date',
                'publication_time',
                'active',
                'on_main',
                'announce',
                'full_text',
                'hyperlink',
                'news-alias'
            );
    }

    public static function getModel($aFields){

        return NewsMapper::getFullParamDefList($aFields);
    }

    /**
     * Метод по выборке из базы конкретной новости по ID и родителю
     * @param $iNewsId - id новости
     * @param $iSectionId - id раздела
     * @return array
     */
    public static function getNewsById($iNewsId,$iSectionId = 0){

        // Собираем массив фильтра
        $aFilter = array(
            'where_condition' => array(
                'id' => array(
                    'sign' => '=',
                    'value' => $iNewsId
                )
            )
        );

        if( $iSectionId )
            $aFilter['where_condition']['parent_section'] = array(
                'sign' => '=',
                'value' => $iSectionId
            );

        // запрос данных
        $aItem = NewsMapper::getItem($aFilter);

        // преобразование даты
        if ( $aItem ){

            $tDate = strtotime($aItem['publication_date']);
            $aItem['publication_date'] = date('d.m.Y', $tDate);
            $aItem['publication_time'] = date('H:i', $tDate);
            $aItem['full_text'] = ImageResize::restoreTags($aItem['full_text']);
            $aItem['announce'] = ImageResize::restoreTags($aItem['announce']);
        }

        return $aItem;
    }

    /**
     * Метод по выборке списка новостей из базы
     * @static
     * @param $aFilter
     * @internal param $iSectionId - id раздела
     * @internal param int $iPage - страница
     * @internal param int $iOnPage - число элементов на страницу
     * @return array
     */
    public static function getNewsList( $aFilter ){

        // запросить значения
        $aResponse = NewsMapper::getItems($aFilter);

        // пересобрать даты
        if ( $aResponse['count'] ) {
            foreach ( $aResponse['items'] as $iKey => $aRow ) {
                $aResponse['items'][$iKey]['publication_show_val'] = date('d.m.Y H:i',strtotime($aRow['publication_date']));
            }
        }

        return $aResponse;
    }

    /**
     * Удаляет пробелы и теги в пустом тексте
     * @param $sText
     * @return mixed
     */
    public static function checkEmptyText( $sText ) {

        if( !str_replace('&nbsp;', '', trim( strip_tags( $sText ) )) )
            $sText = '';

        return $sText;
    }

    /**
     * @static Сохраняет данные
     * @param $aData - массив на сохранение
     * @param $iSectionId - id родительского раздела
     * @param \AdminModulePrototype $oCaller ссылка на вызвавший модуль
     * @return int
     */
    public static function updNews( $aData, $iSectionId, AdminModulePrototype $oCaller ) {

        if ( $aData ) {
            $aData['parent_section'] = $iSectionId;

            if ( isset($aData['publication_date']) and isset($aData['publication_time']) ) {

                $aData['publication_date'] = date('Y-m-d H:i:s',strtotime($aData['publication_date'].$aData['publication_time']));

                if ( !isset($aData['publication_time']) )
                    $aData['publication_time'] = '12:00:00';

                unset($aData['publication_time']);

            }

            $aData['last_modified_date'] = date("Y-m-d H:i:s", time());

            if ( !isset($aData['news-alias']) || empty($aData['news-alias']) )
                $aData['news-alias'] = skTranslit::change($aData['title']);
            else
                $aData['news-alias'] = skTranslit::change($aData['news-alias']);

            if ( preg_match("/^[0-9]+$/i", $aData['news-alias']) ){
                $aData['news-alias'] = 'news-'.$aData['news-alias'];
            }
            $aData['news-alias'] = skTranslit::changeDeprecated($aData['news-alias']);
            $aData['news-alias'] = skTranslit::mergeDelimiters($aData['news-alias']);
            $aData['news-alias'] = self::checkAlias($aData['id'],$aData['news-alias']);

            if ( isset($aData['full_text']) and isset($iSectionId)  )
                $aData['full_text'] = ImageResize::wrapTags($aData['full_text'],$iSectionId);
            if ( isset($aData['announce']) and isset($iSectionId)  )
                $aData['announce'] = ImageResize::wrapTags($aData['announce'],$iSectionId);

            if( isset($aData['hyperlink']) and $aData['hyperlink'] and (strpos($aData['hyperlink'],'http') === false) and ($aData['hyperlink'][0] !== '/') )
                $aData['hyperlink'] = 'http://'.$aData['hyperlink'];

            //if( isSet($aData['full_text']) )
              //  $aData['full_text'] = self::checkEmptyText( $aData['full_text'] );
            //if( isSet($aData['announce']) )
              //  $aData['announce'] = self::checkEmptyText( $aData['announce'] );

            $iNewsId = NewsMapper::saveItem($aData);

            if ( $iNewsId ){

                try {
                    RSS::buildRssFile();
                } catch ( Exception $e ) {
                    $oCaller->addError($e->getMessage());
                }

                if ( !$aData['id'] ) $aData['id'] = $iNewsId;

                self::addToIndex($aData['id']);
            }

            return $iNewsId;

        }

        return 0;
    }

    /**
     * Отдает шаблонный набор значений для добавления новой записи
     * @static
     * @return array
     */
    public static function getBlankValues() {
        return array(
            'title' => 'Новость',
            'publication_date' => date('d.m.Y',time()),
            'publication_time' => date('H:i',time()),
            'active' => 1
        );
    }

    /**
     * @static
     * @param $iItemId
     * @return bool|int
     */
    public static function addToIndex( $iItemId ){

        $aData = self::getNewsById($iItemId);

        if ( !$aData ) return false;

        // нет данных - не добавлять в индекс
        if ( !isset($aData['full_text']) or !isset($aData['news-alias'])  ){
            if( $aData['id'] )
                self::removeFromIndex($aData['id']);
            return false;
        }

        $aData['full_text'] = strip_tags($aData['full_text']);
        $sHref = (!empty($aData['news-alias']))? skRouter::rewriteURL('['.$aData['parent_section'].'][NewsModule?news-alias='.$aData['news-alias'].']'): skRouter::rewriteURL('['.$aData['parent_section'].'][NewsModule?news_id='.$aData['id'].']');
        $sHref = $_SERVER['HTTP_HOST'].$sHref;

        if( $aData['active'] )
            $bRes = Search::addToIndex($aData['title'], $aData['full_text'], $aData['id'], 'News', $aData['parent_section'], $sHref);
        else
            $bRes = self::removeFromIndex($aData['id']);

        return $bRes;
    }

    /**
     * @static
     * @param $iItemId
     * @return bool|mysqli_result
     */
    public static function removeFromIndex($iItemId){

        if ( !$iItemId ) return false;

        return Search::removeFromIndex($iItemId, 'News');
    }

    /**
     * @static
     * @return array
     */
    public static function getIndexData(){

        $aItems = array();
        $aFilter = array(
            'select_fields' => array(
                'id',
                'news-alias',
                'parent_section',
                'title',
                'full_text'
            )
        );

        $aTempItems = NewsMapper::getItems($aFilter);

        $sDomain = DomainsToolApi::getMainDomain();

        if ( $aTempItems['count'] ){

            foreach($aTempItems['items'] as $aTempItem){

                $aTempItem['full_text'] = strip_tags($aTempItem['full_text']);

                $sLink = (!empty($aTempItem['news-alias']))? skRouter::rewriteURL('['.$aTempItem['parent_section'].'][NewsModule?news-alias='.$aTempItem['news-alias'].']'): skRouter::rewriteURL('['.$aTempItem['parent_section'].'][NewsModule?news_id='.$aTempItem['id'].']');

                $aItems[] = array(
                    'search_title' => $aTempItem['title'],
                    'search_text' => $aTempItem['full_text'],
                    'id' => $aTempItem['id'],
                    'class_name' => 'News',
                    'parent_section' => $aTempItem['parent_section'],
                    'href' => $sDomain.$sLink
                );

            }
        }

        return $aItems;
    }

    /**
     * @static
     * @param $iId
     * @param $sAlias
     * @return string
     */
    public static function checkAlias($iId, $sAlias){

        if ( !($sAlias) ) $sAlias = date('d.m.Y H:i');

        $aItems = NewsMapper::checkAlias($sAlias);
        $iSize = sizeof($aItems);

        if ( !$iSize || ($iSize==1 && array_key_exists($iId, $aItems)) ) return $sAlias;

        if ( $iSize>1 || ($iSize==1 && !array_key_exists($iId, $aItems)) ) {

            return self::checkAlias($iId, $sAlias.++$iSize);
        }

        return false;
    }

    /**
     * @static
     * @param $iItemId
     * @param \AdminModulePrototype $oCaller ссылка на вызвавший модуль
     */
    public static function delNews($iItemId, AdminModulePrototype $oCaller){

        if ( NewsMapper::delItem($iItemId) ){

            try {
                RSS::buildRssFile();
            } catch ( Exception $e ) {
                $oCaller->addError($e->getMessage());
            }

            self::removeFromIndex($iItemId);
        }
    }

}
