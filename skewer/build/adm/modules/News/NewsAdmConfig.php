<?php

/* main */
$aConfig['name']     = 'NewsAdm';
$aConfig['title']    = 'Новости (админ)';
$aConfig['version']  = '1.000a';
$aConfig['description']  = 'Админ-интерфейс управления новостной системой';
$aConfig['revision'] = '0001';
$aConfig['layer']     = 'adm';

//$aConfig['hooks']['after']['removeSection']  = array(
//
//    'class' => 'NewsApi',
//    'method' => 'removeFromSection',
//);

return $aConfig;
