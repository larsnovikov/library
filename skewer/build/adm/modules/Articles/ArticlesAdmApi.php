<?php
/**
 * Created by JetBrains PhpStorm.
 * User: user
 * Date: 31.10.12
 * Time: 18:13
 * To change this template use File | Settings | File Templates.
 */
class ArticlesAdmApi {

    /**
     * @static Возвращает набор полей для отображения в списке
     * @return array
     */
    public static function getListFields(){
        return
            array(
                'id',
                'title',
                'author',
                'publication_date',
                'publication_show_val',
                'active'
            );
    }

    /**
     * Набор полей, редактируемых в списке
     * @static
     * @return array
     */
    public static function getEditableListFields() {
        return array(
            'active'
        );
    }

    public static function getDetailFields(){
        return
            array(
                'id',
                'title',
                'author',
                'publication_date',
                'publication_time',
                'announce',
                'full_text',
                'active',
                'hyperlink',
                'articles-alias'
            );
    }

    public static function getModel($aFields){

        return ArticlesMapper::getFullParamDefList($aFields);
    }

    /**
     * Метод по выборке из базы конкретной новости по ID и родителю
     * @param $iArticlesId - id новости
     * @param $iSectionId - id раздела
     * @return array
     */
    public static function getArticlesById($iArticlesId,$iSectionId = 0){

        // Собираем массив фильтра
        $aFilter = array(
            'where_condition' => array(
                'id' => array(
                    'sign' => '=',
                    'value' => $iArticlesId
                )
            )
        );

        if( $iSectionId )
            $aFilter['where_condition']['parent_section'] = array(
                'sign' => '=',
                'value' => $iSectionId
            );

        // запрос данных
        $aItem = ArticlesMapper::getItem($aFilter);

        // преобразование даты
        if ( $aItem ){

            $tDate = strtotime($aItem['publication_date']);
            $aItem['publication_date'] = date('d.m.Y', $tDate);
            $aItem['publication_time'] = date('H:i', $tDate);
            $aItem['full_text'] = ImageResize::restoreTags($aItem['full_text']);
            $aItem['announce'] = ImageResize::restoreTags($aItem['announce']);
        }

        return $aItem;
    }

    /**
     * Метод по выборке списка новостей из базы
     * @static
     * @param $aFilter
     * @internal param $iSectionId - id раздела
     * @internal param int $iPage - страница
     * @internal param int $iOnPage - число элементов на страницу
     * @return array
     */
    public static function getArticlesList( $aFilter ){

        // запросить значения
        $aResponse = ArticlesMapper::getItems($aFilter);

        // пересобрать даты
        if ( $aResponse['count'] ) {
            foreach ( $aResponse['items'] as $iKey => $aRow ) {
                $aResponse['items'][$iKey]['publication_show_val'] = date('d.m.Y H:i',strtotime($aRow['publication_date']));
            }
        }

        return $aResponse;
    }

    /**
     * @static Сохраняет данные
     * @param $aData - массив на сохранение
     * @param $iSectionId - id родительского раздела
     * @param \AdminModulePrototype $oCaller ссылка на вызвавший модуль
     * @return int
     */
    public static function updArticles( $aData, $iSectionId, AdminModulePrototype $oCaller ) {

        if ( $aData ) {
            $aData['parent_section'] = $iSectionId;

            if ( isset($aData['publication_date']) and isset($aData['publication_time']) ) {

                $aData['publication_date'] = date('Y-m-d H:i:s',strtotime($aData['publication_date'].$aData['publication_time']));

                if ( !isset($aData['publication_time']) )
                    $aData['publication_time'] = '12:00:00';

                unset($aData['publication_time']);

            }

            $aData['last_modified_date'] = date("Y-m-d H:i:s", time());

            if ( !isset($aData['articles-alias']) || empty($aData['articles-alias']) )
                $aData['articles-alias'] = skTranslit::change($aData['title']);
            else
                $aData['articles-alias'] = skTranslit::change($aData['articles-alias']);

            if ( preg_match("/^[0-9]+$/i", $aData['articles-alias']) ){
                $aData['articles-alias'] = 'articles-'.$aData['articles-alias'];
            }
            $aData['articles-alias'] = skTranslit::changeDeprecated($aData['articles-alias']);
            $aData['articles-alias'] = skTranslit::mergeDelimiters($aData['articles-alias']);
            $aData['articles-alias'] = self::checkAlias($aData['id'],$aData['articles-alias']);

            if ( isset($aData['full_text']) and isset($iSectionId)  )
                $aData['full_text'] = ImageResize::wrapTags($aData['full_text'],$iSectionId);
            if ( isset($aData['announce']) and isset($iSectionId)  )
                $aData['announce'] = ImageResize::wrapTags($aData['announce'],$iSectionId);

            if( isset($aData['hyperlink']) and $aData['hyperlink'] and (strpos($aData['hyperlink'],'http') === false) and ($aData['hyperlink'][0] !== '/') )
                $aData['hyperlink'] = 'http://'.$aData['hyperlink'];

            $iArticlesId = ArticlesMapper::saveItem($aData);

            if ( $iArticlesId ){

                try {
                    RSS::buildRssFile();
                } catch ( Exception $e ) {
                    $oCaller->addError($e->getMessage());
                }

                if ( !$aData['id'] ) $aData['id'] = $iArticlesId;

                self::addToIndex($aData['id']);
            }

            return $iArticlesId;

        }

        return 0;
    }

    /**
     * Отдает шаблонный набор значений для добавления новой записи
     * @static
     * @return array
     */
    public static function getBlankValues() {
        return array(
            'title' => 'Статья',
            'publication_date' => date('d.m.Y',time()),
            'publication_time' => date('H:i',time()),
            'active' => 1
        );
    }

    /**
     * @static
     * @param $iItemId
     * @return bool|int
     */
    public static function addToIndex( $iItemId ){

        $aData = self::getArticlesById($iItemId);

        if ( !$aData ) return false;

        // нет данных - не добавлять в индекс
        if ( !isset($aData['full_text']) or !isset($aData['articles-alias'])  ){
            if( $aData['id'] )
                self::removeFromIndex($aData['id']);
            return false;
        }

        $aData['full_text'] = strip_tags($aData['full_text']);
        $sHref = (!empty($aData['articles-alias']))? skRouter::rewriteURL('['.$aData['parent_section'].'][ArticlesModule?articles-alias='.$aData['articles-alias'].']'): skRouter::rewriteURL('['.$aData['parent_section'].'][ArticlesModule?articles_id='.$aData['id'].']');
        $sHref = $_SERVER['HTTP_HOST'].$sHref;

        if( $aData['active'] )
            $bRes = Search::addToIndex($aData['title'], $aData['full_text'], $aData['id'], 'Articles', $aData['parent_section'], $sHref);
        else
            $bRes = self::removeFromIndex($aData['id']);

        return $bRes;
    }

    /**
     * @static
     * @param $iItemId
     * @return bool|mysqli_result
     */
    public static function removeFromIndex($iItemId){

        if ( !$iItemId ) return false;

        return Search::removeFromIndex($iItemId, 'Articles');
    }

    /**
     * @static
     * @return array
     */
    public static function getIndexData(){

        $aItems = array();
        $aFilter = array(
            'select_fields' => array(
                'id',
                'articles-alias',
                'parent_section',
                'title',
                'full_text'
            )
        );

        $aTempItems = ArticlesMapper::getItems($aFilter);

        $sDomain = DomainsToolApi::getMainDomain();

        if ( $aTempItems['count'] ){

            foreach($aTempItems['items'] as $aTempItem){

                $aTempItem['full_text'] = strip_tags($aTempItem['full_text']);

                $sLink = (!empty($aTempItem['articles-alias']))? skRouter::rewriteURL('['.$aTempItem['parent_section'].'][ArticlesModule?articles-alias='.$aTempItem['articles-alias'].']'): skRouter::rewriteURL('['.$aTempItem['parent_section'].'][ArticlesModule?articles_id='.$aTempItem['id'].']');

                $aItems[] = array(
                    'search_title' => $aTempItem['title'],
                    'search_text' => $aTempItem['full_text'],
                    'id' => $aTempItem['id'],
                    'class_name' => 'Articles',
                    'parent_section' => $aTempItem['parent_section'],
                    'href' => $sDomain.$sLink
                );

            }
        }

        return $aItems;
    }

    /**
     * @static
     * @param $iId
     * @param $sAlias
     * @return string
     */
    public static function checkAlias($iId, $sAlias){

        if ( !($sAlias) ) $sAlias = date('d.m.Y H:i');

        $aItems = ArticlesMapper::checkAlias($sAlias);
        $iSize = sizeof($aItems);

        if ( !$iSize || ($iSize==1 && array_key_exists($iId, $aItems)) ) return $sAlias;

        if ( $iSize>1 || ($iSize==1 && !array_key_exists($iId, $aItems)) ) {

            return self::checkAlias($iId, $sAlias.++$iSize);
        }

        return false;
    }

    /**
     * @static
     * @param $iItemId
     * @param \AdminModulePrototype $oCaller ссылка на вызвавший модуль
     */
    public static function delArticles($iItemId, AdminModulePrototype $oCaller){

        if ( ArticlesMapper::delItem($iItemId) ){

            try {
                RSS::buildRssFile();
            } catch ( Exception $e ) {
                $oCaller->addError($e->getMessage());
            }

            self::removeFromIndex($iItemId);
        }
    }
}
