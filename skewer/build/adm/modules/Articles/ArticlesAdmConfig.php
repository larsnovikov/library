<?php
/* main */
$aConfig['name']     = 'ArticlesAdm';
$aConfig['title']    = 'Статьи (админ)';
$aConfig['version']  = '1.000a';
$aConfig['description']  = 'Админ-интерфейс управления статьями';
$aConfig['revision'] = '0001';
$aConfig['layer']     = 'adm';

return $aConfig;