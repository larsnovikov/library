<?php
/**
 * @class ArticlesAdmModule
 * @extends skModule
 * @project Skewer
 * @package kernel
 *
 * @author kolesnikov, $Author: $
 * @version $Revision: $
 * @date $Date: $
 *
 */

class ArticlesAdmModule extends AdminSectionTabModulePrototype {

    // id текущего раздела
    protected $iSectionId = 0;

    // число элементов на страницу
    protected $iOnPage = 20;

    // текущий номер страницы ( с 0, а приходит с 1 )
    protected $iPage = 0;

    /**
     * @var string - Имя модуля
     */
    protected $sTabName = 'Статьи';

    /**
     * Метод, выполняемый перед action меодом
     * @throws ModuleAdminErrorException
     * @return bool
     */
    protected function preExecute() {

        // номер страницы
        $this->iPage = $this->getInt('page');

        // id текущего раздела
        $this->iSectionId = $this->getInt('sectionId');

        // проверить права доступа
        if ( !CurrentAdmin::canRead($this->iSectionId) )
            throw new ModuleAdminErrorException( 'accessDenied' );

    }

    /**
     * Первичное состояние
     */
    protected function actionInit() {

        // вывод списка
        $this->actionList();

    }

    protected function actionList() {

        // объект для построения списка
        $oList = new ExtList();

        $aModel = ArticlesAdmApi::getModel( ArticlesAdmApi::getListFields() );

        // задать модель данных для вывода
        $oList->setFields( $aModel );

        // набор редактируемых в списке полей
        $oList->setEditableFields( ArticlesAdmApi::getEditableListFields() );

        // число записей на страницу
        $oList->setOnPage( $this->iOnPage );
        $oList->setPageNum($this->iPage);

        // добавление набора данных
        $aFilter = array(
            'select_fields' => ArticlesAdmApi::getListFields(),
            'where_condition' => array(
                'parent_section' => array(
                    'sign' => '=',
                    'value' => $this->iSectionId
                )
            ),
            'limit' => array (
                'start' => $this->iPage*$this->iOnPage,
                'count' => $this->iOnPage
            ),
            'order' => array(
                'field' => 'publication_date',
                'way' => 'DESC'
            )
        );

        // добавление набора данных
        $aItems = ArticlesAdmApi::getArticlesList( $aFilter );

        $oList->setValues( $aItems['items'] );
        $oList->setTotal( $aItems['count'] );

        $oList->addRowBtnUpdate();
        $oList->addRowBtnDelete();

        // кнопка добавления
        $oList->addBntAdd('show');

        // вывод данных в интерфейс
        $this->setExtInterface( $oList );
    }

    /**
     * Отображение формы
     */
    protected function actionShow() {

        // подключить автоматический генератор форм
        $oForm = new ExtForm();

        // номер новости
        $aData = $this->get('data');
        $iItemId = (is_array($aData) && isset($aData['id'])) ? (int)$aData['id'] : 0;

        // запись новости
        $aItem = $iItemId ? ArticlesAdmApi::getArticlesById( $iItemId, $this->iSectionId ) : ArticlesAdmApi::getBlankValues();

        $aModel = ArticlesAdmApi::getModel(ArticlesAdmApi::getDetailFields());
        unset($aModel['last_modified_date']);

        // установить набор элементов формы
        $oForm->setFields( $aModel );

        // установить значения для элементов
        $oForm->setValues( $aItem );

        // добавление кнопок
        $oForm->addBntSave();
        $oForm->addBntCancel();
        if ( $iItemId ) {
            $oForm->addBntSeparator('->');
            $oForm->addBntDelete();
        }

        // добавление SEO блока полей
        SEOData::appendExtForm( $oForm, 'articles', $iItemId );

        // вывод данных в интерфейс
        $this->setExtInterface( $oForm );

    }

    /**
     * Сохранение новости
     */
    protected function actionSave() {

        // запросить данные
        $aData = $this->get( 'data' );

        // есть данные - сохранить
        if ( $aData ){

            // сохранение данных
            $iId = ArticlesAdmApi::updArticles( $aData, $this->iSectionId, $this );

            // сохранение SEO данных
            SEOData::saveJSData( 'articles', $iId, $aData );

            if ( isset($aData['id']) && $aData['id'] ){
                //$this->addNoticeReport("Редактирование статьи",$aData);
            } else {
                unset($aData['id']);
                //$this->addNoticeReport("Добавление статьи",$aData);
            }
        }

        $this->fireEvent('ArticlesAdmModule:save');

        // вывод списка
        $this->actionInit();

    }

    /**
     * Удаляет запись
     */
    protected function actionDelete() {

        // запросить данные
        $aData = $this->get( 'data' );

        // id записи
        $iItemId = ( is_array($aData) and isset($aData['id']) ) ? (int)$aData['id'] : 0;

        // удаление
        ArticlesAdmApi::delArticles( $iItemId, $this );

        // удаление SEO данных
        SEOData::del( 'articles', $iItemId );

        //$this->addNoticeReport("Удаление статьи",$aData);

        $this->fireEvent('ArticlesAdmModule:delete');

        // вывод списка
        $this->actionInit();

    }

    /**
     * Установка служебных данных
     * @param \ExtPrototype $oExtIface
     */
    protected function setServiceData( ExtPrototype $oExtIface ) {

        // установить заголовок модуля
        $oExtIface->setTitle( $this->sTabName );

        // установить данные для передачи интерфейсу
        $oExtIface->setServiceData( array(
            'sectionId' => $this->iSectionId,
            'page' => $this->iPage
        ) );

    }

}
