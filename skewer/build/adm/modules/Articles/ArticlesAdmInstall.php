<?php
/**
 * @class ArticlesAdmInstall
 * @extends skModule
 * @project Skewer
 * @package adm
 *
 * @author kolesnikov, $Author: $
 * @version $Revision: $
 * @date $Date: $
 *
 */

class ArticlesAdmInstall extends skModuleInstall{

    public function init() {
        return true;
    }// func

    public function install() {
        return true;
    }// func

    public function deinstall() {
        return true;
    }// func

}//class
