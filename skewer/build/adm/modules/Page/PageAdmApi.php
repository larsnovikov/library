<?php

/**
 * @class PageAdmApi
 * @extends skModule
 * @project Skewer
 * @package kernel
 *
 * @author Andy Mitrich, $Author: sapozhkov $
 * @version $Revision: 1346 $
 * @date 20.02.12 12:29 $
 *
 */

class PageAdmApi {

    /**
     * Метод, перестраивающий индексы для редактора
     * В выборку попадают разделы с:
     ** - поле visible = 1
     ** - которые разрешены для чтения публичному пользователю
     * @static
     * @return array
     * TODO Переписать метод с использованием маппера
     */
    public static function getIndexData(){

        global $odb;

        $aSections = Auth::getReadableSections('public');//CurrentUser::getReadableSections();

        $sDomain = DomainsToolApi::getMainDomain();

        if ( !$aSections ) return array();

        $sSections = implode(',', $aSections);

        $sQuery = "
            SELECT
                `t`.`id`,
                `t`.`title`,
                `p`.`show_val`
            FROM `parameters` as `p` INNER JOIN `tree_section` as `t` ON `t`.`id`=`p`.`parent`
            WHERE
                `p`.`name` = 'staticContent' AND
                `t`.`id` IN ($sSections) AND
                `t`.`visible` = 1;";

        $rResult = $odb->query($sQuery);

        $aItems = array();
        while( $aRow = $rResult->fetch_assoc() ){

            $aRow['show_val'] = strip_tags($aRow['show_val']);

            $aItems[] = array(

                'search_title' => $aRow['title'],
                'search_text' => $aRow['show_val'],
                'id' => $aRow['id'],
                'class_name' => 'Page',
                'parent_section' => $aRow['id'],
                'href' => $sDomain.skRouter::rewriteURL('['.$aRow['id'].']')
            );
        }

        return $aItems;
    }

} //class