<?php
/**
 * Класс для обработки ветки шаблонов
 *
 * @class: Tree4TplAdmModule
 *
 * @Author: User, $Author$
 * @version: $Revision$
 * @date: $Date$
 *
 */

class Tree4TplAdmModule extends TreeAdmModule {

    /** @var string заместитель основной JS библиотеки */
    protected $sMainJSClass = 'Tree4Tpl';

    /** @var bool Флаг наличия нескольких деревьев */
    protected $bMultiTree = true;

    /**
     * Отдает id родительского раздела
     * @return int
     */
    protected function getStartSection() {
        return (int)skConfig::get('section.templates');
    }

}
