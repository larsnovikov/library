<?php
/**
 * 
 * @class TreeAdmModule
 *
 * @author ArmiT, $Author: acat $
 * @version $Revision: 1672 $
 * @date $Date: 2013-02-06 15:59:05 +0400 (Ср, 06 фев 2013) $
 *
 */ 
class TreeAdmModule extends AdminTabListModulePrototype {

    /**
     * Отдает класс-родитель, насдедники которого могут быть добавлены в дерево процессов
     * в качестве вкладок
     * @return string
     */
    public function getAllowedChildClassForTab() {
        return 'AdminSectionTabModulePrototype';
    }

    /**
     * Отдает инициализационный массив для набора вкладок
     * @param int|string $mRowId идентификатор записи
     * @return string[]
     */
    public function getTabsInitList( $mRowId ) {

        $mType = Tree::getTypeById( $mRowId );

        // если нет раздела
        if ( $mType === false )
            return array();

        // выбор действия по типу раздела
        switch ( $mType ) {

            // обычный раздел
            case Tree::typeSection:

                // загрузить модули раздела
                return self::initSectionModules( $mRowId );

            // папка
            case Tree::typeDirectory:

                // загрузить только файловый менеджер
                return array(
                    'files' => 'FilesAdmModule'
                );

            default:
                return array();

        }

    }

    /**
     * Задает дополнительные параметры для вкладок
     * @static
     * @param $mRowId
     * @return array
     */
    public function getTabsAddParams( $mRowId ) {

        // выходной массив
        $aOut = array();

        // запрос упрощенного списка параметров
        $aParamsList = Parameters::getAllSimple( $mRowId );

        // если есть параметры
        if(count($aParamsList)) {

            // обойти все
            foreach($aParamsList as $aParams) {

                // если есть инициализация админского
                if(isSet($aParams['objectAdm']) && $aParams['objectAdm']) {

                    // имя модуля
                    $sModuleName = $aParams['objectAdm'];
                    unset( $aParams['objectAdm'] );

                    $aOut['obj'.$sModuleName] = $aParams;

                }

            }

        }

        return $aOut;

    }


    /**
     * Отдает набор объектов для раздела
     * @static
     * @param $mRowId
     * @return array
     */
    protected static function initSectionModules( $mRowId ) {

        $aOut = array();

        /**
         * админские модули
         */

        $aOut['editor'] = 'EditorAdmModule';
        if(CurrentAdmin::isSystemMode())
            $aOut['params'] = 'ParamsAdmModule';

        /**
         * обычные модули
         */

        // запрос упрощенного списка параметров
        $aParamsList = Parameters::getAllSimple( $mRowId );

        // если есть параметры
        if(count($aParamsList)) {

            // обойти все
            foreach($aParamsList as $aParams) {

                // если есть инициализация админского
                if(isSet($aParams['objectAdm']) && $aParams['objectAdm']) {

                    // имя модуля
                    $sModuleName = $aParams['objectAdm'];

                    $aOut['obj'.$sModuleName] = $sModuleName;

                }

            }

        }

        return $aOut;

    }

    /**
     * Стартовый раздел
     * @var int
     */
    protected $iStartSection = null;

    /** @var bool Флаг наличия нескольких деревьев */
    protected $bMultiTree = false;

    /** @var bool Флаг не отображения дополнительных интерфейсов */
    protected $bDropView = false;

    /** @var string заместитель основной JS библиотеки */
    protected $sMainJSClass = '';

    public function init(){

        // вызвать инициализацию, прописанной в родительском классе
        parent::init();

        // если задан заместитель основной JS библиотеки
        if ( $this->sMainJSClass ) {

            // изменение стандартного имени модуля
            $this->setModuleName( $this->sMainJSClass );

            // подцепить основной модуль как подчиненный
            $this->addLibClass( 'Tree', 'adm', 'Tree' );

        }

    }// func

    /**
     * Внутренняя функция. Отдает подразделы заданного раздела
     * @param $iId
     * @return array
     */
    protected function getSubSections( $iId ) {

        $iId = (int)$iId;

        // объект для работы с деревьями
        $oTree = new Tree();

        $aFilter['auth_list'] = CurrentAdmin::getReadableSections();
        // запрос подразделов
        $aItems = $oTree->getSubItems( $iId, $aFilter );

        // обозначить разделы без наследников
        $aItems = $oTree->markLeafs( $aItems );

        $this->setData('cmd','loadItems');

        return array_values($aItems);

    }

    /**
     * Проверка доступа к заданному разделу
     * @param $iSectionId
     * @return bool
     */
    protected function testAccess( $iSectionId ) {

        // todo сделать проверку прав доступа
        // + проверка доступа к 0 разделу на запись (для parent)
        $bRes = (bool)$iSectionId;

        // если нет доступа
        // throw new Exception('authError');

        return $bRes;

    }

    /**
     * Проверка на неудаляемый системный раздел
     * @param $iSectionId
     * @throws Exception
     * @return bool
     */
    protected function testBreakDelete( $iSectionId ) {

        if( $aParam = Parameters::getByName($iSectionId,'.','_break_delete') )
            if( $aParam['value'] )
                throw new Exception('Ошибка! Нельзя удалить системный раздел.');

        return true;
    }

    /**
     * Отдает id родительского раздела
     * @return int
     */
    protected function getStartSection() {
        return (int)skConfig::get('section.root');
    }

    /**
     * Состаяние. Выбор корневого набора разделов
     * @return bool
     */
    protected function actionInit() {

        // родительский раздел
        $iId = $this->getStartSection();

        // todo auth нужно проверять доступность корневого раздела

        // загрузка элементов
        $this->setData('items', $this->getSubSections( $iId ));

        // установка корневого раздела
        $this->addInitParam( 'rootSection',$iId );
        $this->addInitParam( 'multiTree', $this->bMultiTree );

        // блокировка некоторых параметров
        $this->addInitParam( 'showButtons', !$this->bDropView );

        // название
        $this->addInitParam( 'title', Tree::getTitleById($iId) );

        return true;

    }

    /**
     * Состояние. Выбор подразделов заданного раздела
     * @return bool
     */
    protected function actionGetSubItems() {

        // заданный раздел
        $iId = $this->getInt('node');

        $this->setData('items', $this->getSubSections( $iId ));

        return true;

    }

//    /**
//     * Изменение видимости раздела
//     * @return bool
//     */
//    protected function actionSetVisibility() {
//
//        // запрос пришедших переменных
//        $iSectionId = $this->getInt('sectionId');
//        $iVisible = $this->getInt('visible');
//
//        // проверка прав доступа
//        $this->testAccess( $iSectionId );
//
//        if ( !in_array($iVisible,array(0,1)) )
//            $iVisible = 1;
//
//        // установить видимость
//        $oTree = new Tree();
//        $bRes = (bool)$oTree->setVisible($iSectionId,$iVisible);
//
//        $this->setData('cmd','setVisible');
//        if ( $bRes ) {
//            $this->setData('sectionId',$iSectionId);
//            $this->setData('visible',$iVisible);
//        }
//
//        return true;
//
//    }

    /**
     * Запрос параметров для построения формы добавления / редактирования
     * @throws ModuleAdminErrorException
     */
    protected function actionGetForm() {

        // добавить библиотеку отображения
        $this->addLibClass('TreeForm');

        // запрос пришедших переменных
        $iSectionId = $this->getInt('selectedId');
        $aRow = $this->get('item');
        $iTemplateId = 0;
        
        // если дополнение и есть блокировка дополнительных интерфейсов
        if ( !$iSectionId and $this->bDropView )
            throw new ModuleAdminErrorException('adding is not allowed');

        // приведение к нужному типу
        if ( !is_array($aRow) ) $aRow = array();

        // объект для работы с деревьями
        $oTree = new Tree();

        if ( $iSectionId ) {

            // проверка прав доступа
            $this->testAccess( $iSectionId );

            // запросить основную строку
            $aRow = $oTree->getById($iSectionId);
            if ( !$aRow ) throw new ModuleAdminErrorException('notFound');

            // объект для работы с параметрами
            $oParam = new Parameters();

            // запросить шаблон для данного раздела
            $iTemplateId = (int)$oParam->getValByName($iSectionId,'.','template');

        }

        // устанавливаем дефолтное значение, если не задано
        if(isset($aRow[Tree::TYPE]) and $aRow[Tree::TYPE] == Tree::typeDirectory)
            $iTemplateId = Tree::tplDirId;

        // набор шаблонов
        $aRow['template_list'] = $this->getTemplateList($iTemplateId);

        // сохранение ссылки на шаблон
        $aRow['template'] = $iTemplateId;

        // собрать родительские разделы
        $oTree->setCurrentAdminFilter();
        $aRow['parent_list'] = $oTree->getSectionsInLine( $this->getStartSection() );

        // отдать параметры
        $this->setCmd( 'createForm' );
        $this->setData( 'form', $aRow );

    }
    
    /**
     * Устанавливаем список шаблонов для каждого блока разделов
     * @param int $iTemplateId 
     * @return array      
     */
    protected function getTemplateList(&$iTemplateId){
        $aResult = array();
        $aResult = TreeAdmApi::getTemplateList( $iTemplateId );
        return $aResult;
    } 
    

    /**
     * Сохранение/добавление раздела
     * @throws ModuleAdminErrorException
     * @return bool
     */
    protected function actionSaveSection() {

        // массив на сохранение
        $aData = $this->get('item');

        // id раздела
        $iSectionId = $this->getInt('sectionId');

        /** @var bool $iIsNew флаг "Новая запись" */
        $iIsNew = !$iSectionId;

        // состояние системы
        $this->setCmd('saveItem');

        // id родительского раздела
        $iParentId = isset($aData[Tree::PARENT]) ? (int)$aData[Tree::PARENT] : 0;
        $this->testAccess( $iParentId );

        // проверка прав доступа
        if ( $iSectionId ) {

            // проверить права доступа
            $this->testAccess( $iSectionId );

        } else {

            // если есть блокировка дополнительных интерфейсов
            if ( $this->bDropView )
                throw new ModuleAdminErrorException('adding is not allowed');

        }

        if ( $aData ) {

            // объект для работы с деревьями
            $oTree = new Tree();

            // если новая запись - добавить параметр сортировки
            if ( !$iSectionId ) {
                $aData[Tree::POSITION] = $oTree->getNextPosByParentId( $iParentId );
            }

            // если нет псевдонима - сформировать из имени
            if ( !isset($aData[Tree::ALIAS]) or !$aData[Tree::ALIAS] )
                $aData[Tree::ALIAS] = Tree::makeAlias( $aData[Tree::TITLE] );
            // ... есть - переформировать
            else
                $aData[Tree::ALIAS] = Tree::makeAlias( $aData[Tree::ALIAS] );

            // запрет на обновление типа раздела
            if ( isset($aData[Tree::TYPE]) )
                unset( $aData[Tree::TYPE] );

            // сохраняемы шаблон
            $iTemplateId = isset($aData['template']) ? (int)$aData['template'] : 0;
            $bIsDirectory = $iTemplateId === Tree::tplDirId;

            // если запись новая
            if ( $iIsNew and $bIsDirectory ) {
                $aData[Tree::TYPE] = Tree::typeDirectory;
            }

            // сохранить раздел
            $iResId = $oTree->saveItem( $aData );

            if ( isset($aData[Tree::ID]) && $aData[Tree::ID] )
                skLogger::addNoticeReport("Редактирование раздела",skLogger::buildDescription($aData),skLogger::logUsers,$this->getModuleName());
            else {
                unset($aData[Tree::ID]);
                skLogger::addNoticeReport("Создание раздела",skLogger::buildDescription($aData),skLogger::logUsers,$this->getModuleName());
            }

            /* Обновление кеша политик доступа */
            Policy::incPolicyVersion();
            CurrentAdmin::reloadPolicy();

            // статус сохранения
            $this->setData('saveResult',$iResId);

            // выйти, если не создан раздел
            if ( !$iResId ) return false;

            // выдать в ответ текущие данные в базе
            $aNowItem = $oTree->getById( $iResId );
            $this->setData('item',$aNowItem);

            // для нового раздела выполнить дополнительные действия
            if ( $iIsNew and !$bIsDirectory ) {

                // сохранение перекрытых даных для раздела
                TreeAdmApi::setCoverTplData( $iResId, $iTemplateId );

                // добавление формы если шаблон раздел с формой
                // todo есть предложение весь метод перевести на event, а добавление формы вынестив hook
                $sTemplateAlias = $oTree->getAliasById($iTemplateId);
                if( $sTemplateAlias == 'tpl-contact-form' ){
                    $FormsAdmApi = new FormsAdmApi();
                    $FormsAdmApi->createNewForm($iResId, $aData[Tree::TITLE]);
                }

            }

        } else {

            // при ошибке сохранения
            $this->setData('saveResult',false);

            return false;

        }

        return true;

    }

    /**
     * Удаление раздела
     */
    protected function actionDeleteSection() {

        // id раздела
        $iSectionId = $this->getInt('sectionId');

        // проверка прав доступа
        $this->testAccess( $iSectionId );

        //проверка на системный раздел
        $this->testBreakDelete( $iSectionId );

        // объект для работы с деревьями
        $oTree = new Tree();

        // удаление раздела

        $bRes = $oTree->deleteItem( $iSectionId );

        SEOData::del('section',$iSectionId);

        /* запускаем событие если удаление прошло нормально */
        //if($bRes) $this->fireEvent('removeSection', array('sectionId' => $iSectionId));

        skLogger::addNoticeReport("Удаление раздела",skLogger::buildDescription(array('ID раздела'=>$iSectionId)),skLogger::logUsers,$this->getModuleName());

        // возврат результата
        $this->setCmd('deleteSection');
        $this->setData( 'deletedId', $bRes ? $iSectionId : 0 );

    }

    /**
     * Поветочно открывает дерево до нужного раздлела
     * @param int $iFromSection - корневая вершина
     * @param int $iToSection - целевая вершина
     * @param array &$aParents - набор радителей
     * @param array $aTail - набор подчиненных разделов
     * @return array
     */
    protected function getSectionsTree( $iFromSection=0, $iToSection = 0, &$aParents, $aTail = array() ) {

        if ( !$iToSection ) $iToSection = $this->getInt('sectionId');

        // объект для работы с деревом разделов
        $oTree = new Tree();

        // родительский раздел
        $iParentId = $oTree->getParentId($iToSection);

        // если есть родитель и до конца не добрались
        if( $iParentId and $iFromSection != $iToSection ){

            // составление набора родительских элементов
            $aParents[] = $iParentId;

            // запросить набор элементов этого уровня
            $aFilter['auth_list'] = CurrentAdmin::getReadableSections();
            $aFilter['fields'] = array(
                Tree::ID,
                Tree::TITLE,
                Tree::PARENT,
                Tree::VISIBLE,
                Tree::TYPE,
                Tree::LINK
            );
            $aItems = $oTree->getSubItems($iParentId,$aFilter);

            // обобначить разделы без наследников
            $aItems = $oTree->markLeafs( $aItems );

            // рекурсивно спуститься ниже
            $aItems = array_merge($this->getSectionsTree($iFromSection, $iParentId, $aParents, $aItems),$aTail);

            return $aItems;
        } // if parent
        else {
            return $aTail;
        } // else

    } // func

    /**
     * Возвращает в поток дерево разделов, открытое до определенной вершины
     */
    protected function actionGetTree() {

        // целевой раздел
        $iToSection = $this->getInt('sectionId');

        // запросить дерево
        $aParents = array();
        $aItems = $this->getSectionsTree( 0, $iToSection, $aParents );

        // отдать в вывод, если найдено
        if ( $aItems ) {
            $this->setCmd('loadTree');
            $this->setData('sectionId',$iToSection);
            $this->setData( 'items', $aItems );
            $this->setData( 'parents', array_reverse($aParents) );
        }

    }

    /**
     * Просто возвращает данные для выбора раздела
     */
    protected function actionSelectNode(){

        // целевой раздел
        $iToSection = $this->getInt('sectionId');

        // отдать в вывод, если найдено
        $this->setCmd('selectNode');
        $this->setData('sectionId',$iToSection);

    }

    /**
     * Событие изменения положения раздела
     * @throws ModuleAdminErrorException
     */
    protected function actionChangePosition() {

        // направление
        $sDirection = $this->getStr('direction');
        // id переносимого элемента
        $iItemId = $this->getInt('itemId');
        // id элемента относительного которого идет перемещение
        $iOverId = $this->getInt('overId');

        // проверка наличия параменных
        if ( !$iItemId or !$iOverId or !$sDirection )
            throw new ModuleAdminErrorException('badData');

        // проверка прав доступа
        $this->testAccess( $iItemId );
        $this->testAccess( $iOverId );

        // объект для работы с деревом разделов
        $oTree = new Tree();

        // запросить записи элементов
        $aRow = $oTree->getById($iItemId);
        $aOverRow = $oTree->getById($iOverId);

        // наличие разделов обязательно
        if ( !$aRow or !$aOverRow )
            throw new ModuleAdminErrorException('loadSectionError');

        // родительский раздел
        $iItemParentId = (int)$aRow[Tree::PARENT];
        $iOverParentId = (int)$aOverRow[Tree::PARENT];

        // направление переноса
        switch ( $sDirection ) {

            // добавить как подчиненный
            case 'append':

                // запросить максимальную позицию наследников раздела назначения
                $iPosition = $oTree->getNextPosByParentId( $iOverId );

                skLogger::addNoticeReport("Смена положения раздела",skLogger::buildDescription(array('Тип действия'=>'Добавление подчиненного','ID переносимого элемента'=>$iItemId,'Родительский раздел'=>$iOverParentId)),skLogger::logUsers,$this->getModuleName());

                // изменить положение и родителя раздела
                $oTree->saveItem(array(
                    'id' => $iItemId,
                    'parent' => $iOverId,
                    'position' => $iPosition
                ));

                break;

            // вставить до/после элемента
            case 'before':
            case 'after':

                // в корень писать нельзя, из него забирать нельзя, но сортировать можно
                if ( $iOverParentId xor $iItemParentId )
                    throw new ModuleAdminErrorException('badData');

                // флаг "положить до"
                $bBefore = (bool)($sDirection == 'before');

                // взять позицию элемента привязки
                $iOverPositionId = (int)$aOverRow[Tree::POSITION];

                // новое положение
                $iPosition = $bBefore ? $iOverPositionId : $iOverPositionId+1;

                // выполнить сдвиг позиции
                $oTree->shiftPositionByParent( $iOverParentId, $iPosition );

                skLogger::addNoticeReport("Смена положения раздела",skLogger::buildDescription(array('Тип действия'=>'Вставить до/после','ID переносимого элемента'=>$iItemId,'Родительский раздел'=>$iOverParentId)),skLogger::logUsers,$this->getModuleName());

                // изменить положение и родителя раздела
                $oTree->saveItem(array(
                    'id' => $iItemId,
                    'parent' => $iOverParentId,
                    'position' => $iPosition
                ));

                break;

            // неподдерживаемый вариант
            default:
                throw new ModuleAdminErrorException('badData');

        }

    }


}// class
