/**
 * Форма для создания и редактирования разделов
 */
Ext.define('Ext.adm.TreeForm', {
    extend: 'Ext.window.Window',
    title: buildLang.treeFormHeaderAdd,
    isNew: true,
    tree: null,
    treeStore: null,
    form: {},
    template_list: [],
    frame: false,
    closable: true,
    width: 450,
    closeAction: 'destroy',
    modal: true,
    parameters: {
        id: 0,
        alias: '',
        title: '',
        parent: 0,
        template: 0,
        link: '',
        visible: 0
    },
    bodyStyle: 'padding: 5px;',
    items: {
        id: 'form',
        region: 'center',
        xtype: 'form',
        fieldDefaults: {
            labelAlign: 'left',
            labelWidth: 140,
            anchor: '100%',
            margin: '5px'
        },
        items: [{
            name: 'id',
            xtype: 'hiddenfield'
        }, {
            name: 'title',
            xtype: 'textfield',
            fieldLabel: buildLang.treeFormTitleTitle,
            allowBlank: false,
            value: ''
        }, {
            name: 'alias',
            xtype: 'textfield',
            fieldLabel: buildLang.treeFormTitleAlias,
            allowBlank: true,
            value: ''
        }, {
            name: 'parent',
            xtype: 'combo',
            fieldLabel: buildLang.treeFormTitleParent,
            mode: 'local',
            value: '',
            triggerAction: 'all',
            forceSelection: true,
            allowBlank: false,
            editable: false,
            displayField: 'title',
            valueField:'id',
            queryMode: 'local',
            store: Ext.create('Ext.data.Store', {
                fields: ['id','title'],
                data: []
            })
        }, {
            name: 'template',
            xtype: 'combo',
            fieldLabel: buildLang.treeFormTitleTemplate,
            mode: 'local',
            value: '',
            triggerAction: 'all',
            forceSelection: true,
            //allowBlank: false,
            editable: false,
            displayField: 'title',
            valueField:'id',
            queryMode: 'local',
            store: Ext.create('Ext.data.Store', {
                fields: ['id','title'],
                data: []
            })
        }, {
            name: 'link',
            xtype: 'textfield',
            fieldLabel: buildLang.treeFormTitleLink,
            value: ''
        }, {
           name: 'visible',
           xtype: 'combo',
           fieldLabel: buildLang.treeTitleVisible,
           mode: 'local',
           value: '',
           triggerAction: 'all',
           forceSelection: true,
           allowBlank: false,
           editable: true,
           displayField: 'title',
           valueField:'id',
           queryMode: 'local',
           store: Ext.create('Ext.data.Store', {
               fields: ['id','title'],
               data: [{
                   id: 0,
                   title: buildLang.treeTitleVisible0
               },{
                   id: 1,
                   title: buildLang.treeTitleVisible1
               },{
                   id: 2,
                   title: buildLang.treeTitleVisible2
               }]
           })
        }],
        buttons: [{
            text: buildLang.paramFormClose,
            handler: function() {
                this.up('window').close();
            }
        }, {
            text: buildLang.paramFormSaveUpd,
            handler: function() {

                // выйти, если данные не проходят валидацию
                if (!this.up('form').getForm().isValid())
                    return;

                // сохраняемый набор данных
                var row = this.up('form').getForm().getValues();

                var window = this.up('window');
                this.up('window').tree.saveRow( row, function(){

                    // закрыть окно с формой
                    //noinspection JSUnresolvedFunction
                    window.close();

                } );

            }
        }]
    },

    initComponent: function(){

        // генерация объекта
        this.callParent();

        // изменение заголовка при редктировании
        if( this.isNew ) {
            this.title = buildLang.treeFormHeaderUpd;
        }

        // формирование выпадающего списка групп
        var form = this.down('form').getForm();

        // набор шаблонов
        var templateField = form.findField('template');
        var templateList = this.form.template_list || [];
        templateField.store.loadData( templateList );
        if( this.isNew ) {
            templateField.disable();
        }

        // набор родительских разделов
        var parentField = form.findField('parent');
        var parentList = this.form['parent_list'] || [];
        parentField.store.loadData( parentList );

        // выбрать первое значение, если есть
        if ( typeof this.form.template == 'undefined' && templateList.length )
            this.form.template = templateList[0].value;

        // установка значений
        form.setValues( this.form );

        // отобразить окно с формой
        this.show();

    }

});
