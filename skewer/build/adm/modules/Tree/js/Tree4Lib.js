/**
 * Форма для создания и редактирования библиотек
 */
Ext.define('Ext.adm.Tree4Lib', {
    extend: 'Ext.adm.Tree',
    cls: 'sk-treeLib',

    addToRootNode: true,
    defaultSectionType: 1,

    viewConfig: {
        plugins: [{
            ddGroup: 'ddLib',
            ptype  : 'treeviewdragdrop'
        }],
        listeners: {
            beforedrop: function(node, data, overModel, dropPosition){
                this.up('panel').onBeforeDrop( node, data, overModel, dropPosition );
            }
        }
    }

});
