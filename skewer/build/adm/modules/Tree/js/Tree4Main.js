/**
 * Форма для создания и редактирования разделов
 */
Ext.define('Ext.adm.Tree4Main', {
    extend: 'Ext.adm.Tree',
    cls: 'sk-tree',

    viewConfig: {
        plugins: [{
            ddGroup: 'ddMain',
            ptype  : 'treeviewdragdrop'
        }],
        listeners: {
            beforedrop: function(node, data, overModel, dropPosition){
                this.up('panel').onBeforeDrop( node, data, overModel, dropPosition );
            }
        }
    },

    addDockedButtons: function(){
        this.callParent();
        if ( this.showButtons ) {
            this.tbar.push({
                text: 'Настройка сайта',
                iconCls: 'icon-edit',
                scope: this,
                handler: this.showSiteSettings
            });
        }

    },

    // вызов основных настроек сайта
    showSiteSettings: function(){

        var me = this;

        if ( this.useHistory ) {

            this.nowSectionId = me.sectionId;
            me.sectionId = me.rootSection;

            // изменить контрольную точку страницы
            processManager.fireEvent('location_change');

        } else {

            // иначе просто перейти к разделу
            this.findSection( me.rootSection );

        }

    }

});
