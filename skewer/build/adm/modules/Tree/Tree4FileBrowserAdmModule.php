<?php
/**
 * Дерево разеделов для панели выбора файлов
 *
 * @class: Tree4FileBrowserAdmModule
 *
 * @Author: User, $Author: acat $
 * @version: $Revision: 1466 $
 * @date: $Date: 2012-12-20 19:13:34 +0400 (Чт., 20 дек. 2012) $
 *
 */

class Tree4FileBrowserAdmModule extends TreeAdmModule {

    /** @var string заместитель основной JS библиотеки */
    protected $sMainJSClass = 'FileBrowserTree';

}
