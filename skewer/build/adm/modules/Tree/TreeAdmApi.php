<?php
/**
 * API для работы с деревом в админском интерфейсе
 */
class TreeAdmApi {

    /**
     * Отдает набор шаблонов.
     * Дополняет список фиктивными служебными
     * @param int $iTemplateId
     * @return array
     */
    public static function getTemplateList( &$iTemplateId ) {
        $aOut = array();

        // объект для работы с деревьями
        $oTree = new Tree();

        // запросить шаблоны
        $aTplFilter = array(
            'fields' => array('id', 'title'),
            'visibility' => 1
        );
        $aTemplates = $oTree->getSubItems( skConfig::get('section.templates'), $aTplFilter);

        // добавть шаблоны
        foreach ( $aTemplates as $aTpl ) {
            $aOut[] = array(
                'id' => (int)$aTpl['id'],
                'title' => $aTpl['title']
            );
        }

        // если нет в шаблонах
        if ( !isset( $aTemplates[$iTemplateId] ) ) {

            // есть шаблоны
            if ( !empty( $aTemplates ) ) {
                // выбрать первый
                $aFirstTpl = reset( $aTemplates );
                $iTemplateId = $aFirstTpl['id'];

            } else {

                // нет - добавить безымянный
                array_unshift($aOut, array(
                    'id' => (int)$iTemplateId,
                    'title' => (string)($iTemplateId?$iTemplateId:'---')
                ));

            }
        }

        return $aOut;

    }

    /**
     * Сохраняет перекрытые данные для раздела
     * Вызывается только при создании
     * @param int $iSectionId id раздела
     * @param int $iTemplateId id шиблона для сохранения
     */
    public static function setCoverTplData( $iSectionId, $iTemplateId ) {

        // объект для работы с деревьями
        $oTree = new Tree();

        // id шаблона
        if ( !$oTree->getById($iTemplateId) )
            $iTemplateId = 0;

        // объект для работы с параметрами
        $oParam = new Parameters();

        /**
         * При добавлении добавить все параметры шаблона с access_level>0
         */

        // при заданном шаблоне
        if ( $iTemplateId ) {

            // фильтр: нужные поля и уровень доступа > 0
            $aFilter = array();
            $aFilter['fields'] = array('id','title','value','show_val','access_level');
            $aFilter['with_access_level'] = '>0';

            // запросить данные
            $aAddParams = $oParam->getAllAsList( $iTemplateId, $aFilter, false );

            // добавить все параметры
            foreach ( $aAddParams as $aParam ) {

                $aParam['id'] = 0;
                $aParam['parent'] = $iSectionId;
                $oParam->saveParameter($aParam);

            }

        }

        /**
         * Запись о шаблоне: конец
         */

        // запросить запись
        $aTplParam = $oParam->getByName( $iSectionId, '.', 'template', false );

        // нет - создать
        if ( !$aTplParam )
            $aTplParam = array(
                'id' => 0,
                'parent' => $iSectionId,
                'group' => '.',
                'name' => 'template'
            );

        // сохранить значение параметра
        $aTplParam['value'] = $iTemplateId;

        // сохранить
        $oParam->saveParameter($aTplParam);

    }

}
