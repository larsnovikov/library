<?php
/**
 * Класс для обработки основной ветки сайта
 */

class Tree4MainAdmModule extends TreeAdmModule {

    /** @var string заместитель основной JS библиотеки */
    protected $sMainJSClass = 'Tree4Main';

    /** @var bool Флаг наличия нескольких деревьев */
    protected $bMultiTree = true;

    /**
     * Отдает id родительского раздела
     * @return int
     */
    protected function getStartSection() {
        return (int)skConfig::get('section.sectionRoot');
    }

}
