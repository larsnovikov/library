<?php
/**
 * Класс для обработки ветки библиотек
 */

class Tree4LibAdmModule extends TreeAdmModule {

    /** @var string заместитель основной JS библиотеки */
    protected $sMainJSClass = 'Tree4Lib';

    /** @var bool Флаг наличия нескольких деревьев */
    protected $bMultiTree = true;

    /**
     * Отдает id родительского раздела
     * @return int
     */
    protected function getStartSection() {
        return (int)skConfig::get('section.library');
    }
    
    /**
     * Устанавливаем список шаблонов для библиотек
     * @param int $iTemplateId
     * @return array
     */    
    protected function getTemplateList(&$iTemplateId){
        $aResult = array();
        
        // добавляем фиктивный раздел Папка
         $aResult[] = array(
                 'id' => Tree::tplDirId,
                 'title' => '"Папка"'
         );
        
        return $aResult;
    }    

}
