<?php
/**
 * 
 * @class ParamsAdmModule
 *
 * @author ArmiT, $Author: sapozhkov $
 * @version $Revision: 1316 $
 * @date $Date: 2012-11-27 18:31:02 +0400 (Вт., 27 нояб. 2012) $
 *
 */ 
class ParamsAdmModule extends AdminSectionTabModulePrototype {

    /**
     * @var string - Имя модуля
     */
    protected $sTabName = 'Параметры';

    // id текущего раздела
    protected $iSectionId = 0;

    /**
     * Метод, выполняемый перед action меодом
     * @throws ModuleAdminErrorException
     * @return bool
     */
    protected function preExecute() {

        // id текущего раздела
        $this->iSectionId = $this->getInt('sectionId');

        // проверить права доступа
        if ( !CurrentAdmin::canRead($this->iSectionId) )
            throw new ModuleAdminErrorException( 'accessDenied' );

    }

    /**
     * Установка служебных данных
     * @param \ExtPrototype $oExtIface
     */
    protected function setServiceData( ExtPrototype $oExtIface ) {

        // установить заголовок модуля
        $oExtIface->setTitle( $this->sTabName );

        // установить данные для передачи интерфейсу
        $oExtIface->setServiceData( array(
            'sectionId' => $this->iSectionId
        ) );

    }

    /**
     * Инициализация
     */
    public function actionInit() {

        // заголовок
        $this->setPanelName( 'Список параметров' );

        // объект для построения списка
        $oList = new ExtList();

        $aModel = Parameters::getFullParamDefList( Parameters::getListFields() );

        // задать модель данных для вывода
        $oList->setFields( $aModel );

        // редактируемое поле
        $oList->setEditableFields( array('value') );

        $aItems = Parameters::getAllAsList( $this->iSectionId, array('fields'=>Parameters::getListFields()) );
        $oList->setValues( $aItems );

        // группировка
        $oList->setGroupField('group');

        $oList->addSorter('name');

        $oList->addRowCustomBtn( 'ParamsEditBtn' );
        $oList->addRowCustomBtn( 'ParamsDelBtn' );

        // кнопка добавления
        $oList->addBntAdd('show');
        $oList->addBntDelete();

        // вывод данных в интерфейс
        $this->setExtInterface( $oList );


    }

    /**
     * Загружает форму для редактирования
     */
    protected function actionShow() {

        // подключить автоматический генератор форм
        $oForm = new ExtForm();

        // номер новости
        $iItemId = $this->getInDataValInt('id');

        // запись новости
        if ( $iItemId ) {

            $aItem = Parameters::getById($iItemId);

            if ( !$aItem )
                throw new Exception('Параметр не найден');

            // если нельзя читать раздел
            if ( !CurrentAdmin::canRead( $aItem['parent'] ) )
                throw new Exception('Доступ к параметру запрещен');

            // если параметр не принадлежит данному разделу - отвязать от id
            if ( (int)$aItem['parent']!==$this->iSectionId ) {
                $aItem['id'] = 0;
                $this->setPanelName( 'Клонирование родительского параметра' );
            } else {
                $this->setPanelName( 'Редактирование параметра' );
            }

        } else {

            $this->setPanelName( 'Добавление параметра' );

            $aItem = array(
                'id' => 0,
                'group' => '.',
                'parent' => $this->iSectionId,
                'access_level' => '0'
            );

        }

        $aModel = Parameters::getFullParamDefList(
            Parameters::getDetailFields(),
            Parameters::getAddParamForForm($this->iSectionId)
        );

        // установить набор элементов формы
        $oForm->setFields( $aModel );

        // установить значения для элементов
        $oForm->setValues( $aItem );

        // добавление кнопок
        $oForm->addBntSave();
        $oForm->addBntCancel();
        if ( $iItemId ) {
            $oForm->addBntSeparator('->');
            $oForm->addBntDelete();
        }

        // вывод данных в интерфейс
        $this->setExtInterface( $oForm );

    }

    /**
     * Сохранение параметров
     * @throws Exception
     * @return bool
     */
    public function actionSave() {

        // массив на сохранение
        $aData = $this->get('data');
        if ( !$aData )
            throw new Exception('Нет данных для сохранения');

        // id элемента
        $iId = $this->getInDataValInt('id');

        // если задан id
        if ( $iId ) {

            // проверить совпадение раздела
            $aRowInBase  = Parameters::getById( $iId );
            if ( $aRowInBase and (int)$aRowInBase['parent']!== $this->iSectionId ) {

                // перекрываем все данные пришедшими
                $aData = array_merge( $aRowInBase, $aData );

                // задаем параметры для создания нового параметра в этом разделе
                $aData['id'] = 0;
                $aData['parent'] = $this->iSectionId;

            }

        } else {

            // перекрть раздел
            $aData['parent'] = $this->iSectionId;

        }

        // сохранить параметр
        $iRes = Parameters::saveParameter( $aData );

        if ( $iRes ) {
            $this->addMessage('Параметр сохранен');
        } else {
            $this->addError('Возникла ошибка при сохранении');
        }

        // отдать результат сохранения
        $this->setData('saveResult',$iRes);

        // отдать назад список параметров
        $this->actionInit();

    }

    /**
     * Удалние набора параметров
     * @throws Exception
     * @return bool
     */
    public function actionDelete(){

        // список/id параметра
        $iId = $this->getInDataValInt('id');

        if ( !$iId )
            throw new Exception('Параметр для удаления не выбран');

        // составление набора id для удаления
        $aItem = Parameters::getById( $iId );
        if ( !$aItem )
            throw new Exception('Параметр не найден');

        if ( (int)$aItem['parent']!==$this->iSectionId )
            throw new Exception('Нельзя удалять параметры из родительского раздела');

        // выполнение запроса на удаление
        $iRes = Parameters::delById($iId, $this->iSectionId );

        if ( $iRes ) {
            $this->addMessage('Параметр удален');
        } else {
            $this->addError('При удалении возникла ошибка');
        }

        // отдать назад список параметров
        $this->actionInit();

    }

    /**
     * Создать копию параметра для заданного раздел
     * @throws Exception
     * @return array
     */
    public function actionClone(){

        // id параметра
        $iId = $this->getInDataValInt('id');

        // запросить дублируемое поле
        $aSrcRow = Parameters::getById( $iId );

        if ( !$aSrcRow )
            throw new Exception('Исходная строка не найдена');

        // перекрытие параметров
        $aSrcRow['id'] = 0;
        $aSrcRow['parent'] = $this->iSectionId;

        // сохранение новой записи
        $iRes = Parameters::saveParameter( $aSrcRow );

        // отдать результат только в случае ошибки
        if ( !$iRes )
            throw new Exception('Возникла ошибка при клонировании');

        // отдать назад список параметров
        $this->actionInit();

    }
}
