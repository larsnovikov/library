/**
 * Кастомное поле для редактирования типа поля
 */
Ext.define('Ext.adm.ParamsTypeField', {
    extend: 'Ext.form.field.ComboBox',
    mode: 'local',
    triggerAction: 'all',
    forceSelection: false,
    allowBlank: false,
    editable: false,
    displayField: 'title',
    valueField:'type_id',
    queryMode: 'local',
    store: null,

    initComponent: function() {
        var me = this;
        me.store = Ext.create('Ext.data.Store', {
            fields: ['type_id','title'],
            data: me.getTypesAsArray()
        });
        me.callParent();
    },

    // получить набор типов в виде массива
    getTypesAsArray: function(){
        var aOut = [];
        var typeList = Ext.create('Ext.sk.FieldTypes').getTypesAsObject();
        for ( var type in typeList ) {
            if ( typeof typeList[type].type_id === 'undefined' )
                continue;
            aOut.push( { type_id: typeList[type].type_id.toString(), title: typeList[type].title } )
        }
        // для локальных переменных
        for ( type in typeList ) {
            if ( !typeList[type].type_id )
                continue;
            if ( typeList[type].type_id )
                aOut.push( { type_id: -typeList[type].type_id, title: typeList[type].title+(buildLang.contTabLocalField) } )
        }
        return aOut;
    }

});
