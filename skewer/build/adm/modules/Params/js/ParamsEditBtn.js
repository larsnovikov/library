/**
 * Настраиваемая кнопка для строк редактировать/исправить для раздела
 */
Ext.define('Ext.adm.ParamsEditBtn', {
    extend: 'Ext.Component',
    state: 'edit_form',
    getClass: function(value, meta, rec,rowIndex,colIndex,store,grid) {
        var sectionId = processManager.getMainContainer(grid).serviceData.sectionId;
        if (rec.get('parent') == sectionId) {
            // редактировать
            this.items[0].tooltip = buildLang.upd;
            return 'icon-edit';
        } else {
            // Исправить для раздела
            this.items[0].tooltip = buildLang.paramAddForSection;
            return 'icon-add';
        }
    },
    handler: function(grid, rowIndex) {
        var mainContainer = processManager.getMainContainer(grid);
        var rec = grid.getStore().getAt(rowIndex);
        // редактировать
        processManager.setData(mainContainer.path,Ext.merge({
            cmd: 'show',
            data: {id: rec.get('id')}
        },mainContainer.serviceData));
        // отправить запрос
        processManager.postData();
    }
});
