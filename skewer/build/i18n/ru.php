<?php

$aLang = array();

/* сообщения для общесистемных API */
/* работа с параметрами */
$aLang['ParamNotSaved']         = 'Ошибка: Параметр [name: %s, section: %d] не сохранен!';
$aLang['ParamNotDeleted']       = 'Ошибка: Параметр [id: %d] не удален!';
$aLang['ParamsWrongData']       = 'Ошибка: Параметр [name: %s, section: %d] не может быть сохранен - неверные параметры!';
$aLang['ParamNotFound']         = 'Ошибка: Параметр [name: %s, section: %d] не найден!';

/*работа с файловой системой*/
$aLang['FolderNotCreated']      = 'Ошибка: Директория [path: %s] не создана!';
$aLang['FolderNotMoved']        = 'Ошибка: Директория [of: %s to: $s] не перемещена!';
$aLang['FolderNotRemoved']      = 'Ошибка: Директория [%s] не удалена!';
$aLang['FileNotRemoved']        = 'Ошибка: Файл [%s] не удален!';
$aLang['FileNotFound']          = 'Ошибка: Файл [%s] не найден!';

/*астрактные ответы*/
$aLang['WrongData']             = 'Ошибка: Неверные параметры!';

/*Ошибки парсинга*/
$aLang['ParseError']             = 'Ошибка обработки шаблона!';

/* сообщения, используемые при установке модулей и обновлении площадок  */
$aLang['updateError_wrongConstrData']       = 'Ошибка: Система обновлений не может быть запущена! Переданы неверные параметры.';
$aLang['updateError_NoConstrData']          = 'Ошибка: Система обновлений не может быть запущена! Отсутствуют обязательные параметры [%s].';
$aLang['updateError_sqlfile_notfound']      = 'Ошибка: SQL Файл [%s] не найден!';
$aLang['updateError_wrong_query']           = 'Ошибка: SQL Файл не выполнен с ошибкой: [%s]';
$aLang['updateError_tplNotFound']           = 'Ошибка: Шаблонный раздел [%d] не найден!';
$aLang['updateError_sectionNotAdded']       = 'Ошибка: Раздел [%s] не добавлен!';
$aLang['updateError_AliasNotFound']         = 'Ошибка: Раздел с псевдонимом [%s] не найден!';
$aLang['updateError_SectionNotDeleted']     = 'Ошибка: Раздел [%s] не удален!';
$aLang['updateError_storageNotFound']       = 'Ошибка: Хранилище [%s] реестра не найдено!';

/* Сообщения, используемые в файлах обновлений (патчах) */
$aLang['patchError_wrongConstrData']        = 'Отсутствуют необходимые параметры запуска обновления!';

return $aLang;