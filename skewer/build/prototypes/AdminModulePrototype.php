<?php
/**
 * Прототип админского модуля с собственными js файлами для отображения интерфейса
 * @class: AdminModulePrototype
 * @project Skewer
 * @package Build
 *
 * @Author: sapozhkov, $Author: sapozhkov $
 * @version: $Revision: 1146 $
 * @date: $Date: 2012-11-08 16:57:07 +0400 (Чт., 08 нояб. 2012) $
 *
 */
class AdminModulePrototype extends skModule {

    /**
     * Инициализация модуля
     * @return bool|void
     */
    public function init(){

        Ext::init();

        /** @noinspection PhpParamsInspection */
        $this->setParser(parserJSON);
        $this->clearMessages();
        $this->setJSONHeader('subLibs', array());

    }

    /**
     * Обработка пришедших запросов. Составляет имя внетреннего метода из
     * пришедшего параметра cmd и отдает результат выполнения функции
     *
     * @return int
     */
    public function execute() {

        // запрос комманды
        $sAction = $this->getStr('cmd');

        // есть - выполнить
        if ( $this->actionExists( $sAction ) ) {

            $iStatus = psComplete;

            try {

                // убрать сообщение об ошибке (от предыдущих ответов)
                $this->unsetJSONHeader('moduleError');

                // метод перед выполнением состояния
                $this->preExecute();

                // выполнение заданного метода
                $iStatus = (int)call_user_func(array(
                    $this,
                    $this->getActionMethodName( $sAction )
                ));

                // нет статуса - поставить "Завершен в штатном режиме"
                if ( !$iStatus )
                    $iStatus = psComplete;

                $aErrorList = $this->getErrors();
                if ( $aErrorList ) $this->setData( 'moduleErrorList', $aErrorList );
                $aMessageList = $this->getMessages();
                if ( $aMessageList ) $this->setData( 'moduleMessageList', $aMessageList );

                // js события
                $aJSEvents = $this->getJSEvents();
                $this->clearJSEvents();
                if ( $aJSEvents )
                    $this->setJSONHeader('fireEvents',$aJSEvents);
                else
                    $this->unsetJSONHeader('fireEvents');

                // прослушивание js событий
                $aJSListeners = $this->getJSListeners();
                $this->clearJSListeners();
                if ( $aJSListeners )
                    $this->setJSONHeader('listenEvents',$aJSListeners);
                else
                    $this->unsetJSONHeader('listenEvents');



            } catch ( ModuleAdminErrorException $e ) {

                // выдать системную ошибку в массив выдачи модуля
                $this->setJSONHeader('moduleError',$e->getMessage());

            } catch ( Exception $e ) {
                // вернуть ошибку
                $this->setData( 'error', $e->getMessage() );
            }

            return $iStatus;

        } else {

            $this->setJSONHeader('moduleError', 'Недопустимое состояние');

            // нет - просто отработать
            return psComplete;
        }

    }

    /**
     * Отдает true если есть доступный метод для заданного состояния
     * @param $sAction
     * @return bool
     */
    public function actionExists( $sAction ) {
        return method_exists( $this, $this->getActionMethodName($sAction) );
    }

    /**
     * Отдает имя метода по имени состояния
     * @param string $sAction
     * @return string
     */
    public function getActionMethodName( $sAction ) {
        return 'action' . ($sAction ? ucfirst( $sAction ) : $this->getBaseActionName());
    }

    /**
     * Отддает имя первичного состояния (если не задано)
     * @return string
     */
    public function getBaseActionName() {
        return 'Init';
    }

    /**
     * Разрешить выполнение модуля
     * @return bool
     */
    public function allowExecute() {
        return CurrentAdmin::isLoggedIn();
    }

    /**
     * Метод, выполняемый перед action меодом
     * @return bool
     */
    protected function preExecute() {
        return true;
    }

    /**
     * Устанавливает cmd в выходной массив
     * @param $sCmd
     */
    protected function setCmd( $sCmd ) {
        $this->setData('cmd',$sCmd);
    }

    /**
     * Добавить определение js библиотеки в вывод
     * Устанавливается путь в js и при запросе подгружается
     * @param string $sLibName имя библиотеки
     * @param string $sLayerName слой
     * @param string $sModuleName имя модуля
     */
    public function addLibClass( $sLibName, $sLayerName='', $sModuleName='' ) {

        // сторонний компонент
        $bNotOwn = ($sLayerName or $sModuleName);

        // если не задан слой
        if ( !$sLayerName )
            $sLayerName = $this->getLayerName();

        // если не задано имя модуля
        if ( !$sModuleName )
            $sModuleName = $this->getModuleName();

        // добавление в список вызова
        $aNowLibs = $this->getJSONHeader('subLibs');
        if ( !is_array($aNowLibs) ) $aNowLibs = array();
        if ( !in_array( $sLibName, $aNowLibs ) ) {
            $aLib = array(
                'name' => $sLibName,
                'layer' => $sLayerName,
                'module' => $sModuleName,
                'notOwn' => $bNotOwn,
            );
            array_push($aNowLibs, $aLib );
            $this->setJSONHeader('subLibs',$aNowLibs);
        }
    }

    /**
     * Добавляет инициализационный параметр для js слоя
     * @param $sName - имя параметра
     * @param $sValue - значение
     */
    protected function addInitParam( $sName, $sValue ){
        $aNowParams = $this->getJSONHeader('init');
        if ( !is_array($aNowParams) ) $aNowParams = array();
        $aNowParams[$sName] = $sValue;
        $this->setJSONHeader('init',$aNowParams);
    }

    /**
     * Работа с сообщениями
     * @var array
     */
    protected $aMessages = array(
        'errors' => array(),
        'messages' => array()
    );

    /**
     * Добавить сообщение
     * @param $sHeader
     * @param $sText
     */
    public function addMessage( $sHeader ,$sText='' ) {
        $this->aMessages['messages'][] = array( $sHeader ,$sText );
    }

    /**
     * Возвращает набор сообщений
     * @return array
     */
    public function getMessages() {
        return $this->aMessages['messages']?$this->aMessages['messages']:array();
    }

    /**
     * Добавить сообщение об ошибке
     * @param $sText
     */
    public function addError( $sText ) {
        $this->aMessages['errors'][] = array( $sText );
    }

    /**
     * Возвращает набор сообщений об ошибках
     * @return array
     */
    public function getErrors() {
        return $this->aMessages['errors']?$this->aMessages['errors']:array();
    }

    /**
     * Очистка списка сообщений
     */
    public function clearMessages(){
        $this->aMessages = array(
            'errors' => array(),
            'messages' => array()
        );
    }

    /**
     * Работа с JS событиями
     */

    /**
     * Набор событий, которые должны быть вызваны
     * @var array
     */
    protected $aJSEventList = array();

    /**
     * Добавить событие на выполнение в JS части
     * @param string $sEventName - имя события
     */
    public function fireJSEvent( $sEventName ){

        // собираем посылку
        $aData = array(
            $sEventName
        );

        // если входных параметров больше 2
        if ( func_num_args()>1 ) {
            for ($i=1; $i<func_num_args(); $i++) {
                $aData[] = func_get_arg($i);
            }
        }

        $this->aJSEventList[] = $aData;

    }

    /**
     * Возвращает набор событий для выполнения в JS части
     * @return array
     */
    public function getJSEvents(){
        return $this->aJSEventList;
    }

    /**
     * Очищает набор событий для выполнения в JS части
     */
    public function clearJSEvents(){
        $this->aJSEventList = array();
    }

    /**
     * Работа с JS событиями - прослушивание
     */

    /**
     * @var array набор слушаемых событий
     */
    protected $aJSListeners = array();

    /**
     * Добавляет php подписчика для js события
     * На одно событие можно повесить только одного слушателя
     * @param string $sEventName имя js события
     * @param string $sActionName название действия в php
     */
    protected function addJSListener( $sEventName, $sActionName ) {
        $this->aJSListeners[$sEventName] = $sActionName;
    }

    /**
     * Отдает набор php подписчиков для js события
     * @return array
     */
    public function getJSListeners() {
        return $this->aJSListeners;
    }

    /**
     * Очищает список подписчиков js событий
     */
    public function clearJSListeners() {
        $this->aJSListeners = array();
    }

    /**
     * Работа с данными
     */

    /**
     * Получить массив пришедших данных, с возможностью фильтрации
     * @param array $aFilter массив имен необходимых полей
     * @param bool $bExclude - флаг исключение указанных полей
     *          true - !все указанные в фильтре поля
     *          false - все что есть в ответе, кроме заданных
     * @return array
     */
    protected function getInData( $aFilter=array(), $bExclude=false ){

        // получить данные
        $aData = $this->get('data');
        if ( !is_array($aData) )
            $aData = array();

        // если есть ограничение по полям
        if ( $aFilter ) {
            $aOut = array();

            // если флаг исключения
            if ( $bExclude ) {
                // из полученных полей
                foreach ( $aData as $sName ) {
                    // убрать те, что есть в фильтре
                    if ( !in_array($sName,$aFilter) ) {
                        $aOut[$sName] = (string)$aData[$sName];
                    }
                }
            } else {
                // взять список необходимых полей
                foreach ( $aFilter as $sName ) {
                    // добавить в вывод те поля, которые есть в посылке,
                    // остальные заполнить пустышками
                    $aOut[$sName] = isset($aData[$sName]) ? (string)$aData[$sName] : '';
                }
            }

        } else {
            // не задан фильтр - просто вернуть все, что есть в посылке
            $aOut = $aData;
        }

        return $aOut;

    }

    /**
     * Возвращает значение элемента во входном массиве
     * @param string $sName Имя исходного параметра
     * @param mixed $mDefault Значение, подставляемое по-умолчанию
     * @return string
     */
    protected function getInDataVal( $sName, $mDefault = '' ) {

        $aData = $this->get('data');
        if ( !$aData or !is_array($aData) )
            return '';

        return isset($aData[$sName]) ? (string)$aData[$sName] : $mDefault;

    }

    /**
     * Отдает значение переменной из массива данных
     * @param string $sName Имя исходного параметра
     * @param mixed $mDefault Значение, подставляемое по-умолчанию
     * @return int
     */
    protected function getInDataValInt( $sName, $mDefault = 0 ) {
        return (int)$this->getInDataVal($sName,$mDefault);
    }

    /**
     * Отдает флаг фозможности создания наследников для данного модуля
     * в дереве процессов
     * @return bool
     */
    protected function canBeParent() {
        return true;
    }

    /**
     * Отдает класс-родитель, насдедники которого могут быть добавлены в дерево процессов
     * @return string
     */
    protected function getAllowedChildClass() {
        return 'AdminModulePrototype';
    }

    /**
     * Adding child process in parent process
     * @final
     * @param skContext $oContext
     * @throws Exception
     * @return array|bool
     */
    public function addChildProcess(skContext $oContext) {

        // проверка возможности добавлять подчиненные модули
        if ( !$this->canBeParent() )
            throw new Exception( sprintf('Модуль `%s` не может иметь подчиненных модулей.',$oContext->getClassName()) );

        // проверка добавляемого модуля
        $sParentClass = $this->getAllowedChildClass();
        if ( $sParentClass ) {
            $oRC = new ReflectionClass($oContext->getClassName());
            if ( !$oRC->isSubclassOf($sParentClass) ) {
                throw new Exception( sprintf('Модуль `%s` должен быть наследован от `%s`',$oContext->getClassName(),$sParentClass) );
            }
        }

        return parent::addChildProcess( $oContext );

    }

    /*
     * Блок методов, отправляющих данные в лог
     */
    public function addModuleCriticalReport($sTitle, $sDescription){
        return skLogger::addCriticalReport($sTitle, $sDescription, skLogger::logUsers, $this->getModuleNameAdm());
    }

    public function addModuleWarningReport($sTitle, $sDescription){
        return skLogger::addWarningReport($sTitle, $sDescription, skLogger::logUsers, $this->getModuleNameAdm());
    }

    public function addModuleErrorReport($sTitle, $sDescription){
        return skLogger::addErrorReport($sTitle, $sDescription, skLogger::logUsers, $this->getModuleNameAdm());
    }

    public function addModuleNoticeReport($sTitle, $sDescription){
        return skLogger::addNoticeReport($sTitle, $sDescription, skLogger::logUsers, $this->getModuleNameAdm());
    }


}
