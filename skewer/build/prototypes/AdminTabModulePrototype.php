<?php
/**
 * Протитип модулей на основе автопостроителя
 */
abstract class AdminTabModulePrototype extends AdminModulePrototype {

    /**
     * Имя модуля
     * @var string
     */
    protected $sTabName = '';

    /**
     * Отдает название модуля
     * @return string
     */
    public function getTitle() {
        return $this->sTabName;
    }

    /**
     * Имя панели
     * @var string
     */
    protected $sPanelName = '';

    /**
     * Устанавливает название панели
     * @param string $sNewName - новое имя
     * @param bool $bAddTabName - нужно ли добавить в начало имя вкладки
     */
    protected function setPanelName($sNewName, $bAddTabName = false ){
        $sPrefix = $bAddTabName && $this->getTitle() ? $this->getTitle() : '' ;
        $sDelimiter = $sPrefix && $sNewName ? ': ' : '';
        $this->sPanelName = $sPrefix.$sDelimiter.$sNewName;
    }

    /**
     * Отдает флаг использования автопостроителя
     * @return bool
     */
    public function useBuilder() {
        return true;
    }

    public function actionInitTab() {

        $oTab = new ExtLoadTab();

        // флаг инициализации вкладки
        $this->setData( 'initTabFlag', true );

        $this->setExtInterface( $oTab );

    }

    /**
     * Установка служебных данных
     * @param ExtPrototype $oExtIface
     */
    protected function setServiceData( ExtPrototype $oExtIface ) {}

    /**
     * Добавляет данные для вывода в шаблонизатор
     * @param ExtPrototype $oInterface - модуль, для которого идет вызов
     * @return bool
     */
    final public function setExtInterface( ExtPrototype $oInterface ) {

        // установка заголовков
        $oInterface->setTitle( $this->getTitle() );
        $oInterface->setPanelTitle( $this->sPanelName ? $this->sPanelName : $this->getTitle() );

        // установка служебных данных из модуля для передачи
        $this->setServiceData( $oInterface );

        // добавление интерфейсных данных в посылку
        $oInterface->setInterfaceData( $this );

        return true;

    }

    /**
     * Работа с данными
     */

    /**
     * Получить массив пришедших данных, с возможностью фильтрации
     * @param array $aFilter массив имен необходимых полей
     * @param bool $bExclude - флаг исключение указанных полей
     *          true - !все указанные в фильтре поля
     *          false - все что есть в ответе, кроме заданных
     * @return array
     */
    protected function getInData( $aFilter=array(), $bExclude=false ){

        // получить данные
        $aData = $this->get('data');
        if ( !is_array($aData) )
            $aData = array();

        // если есть ограничение по полям
        if ( $aFilter ) {
            $aOut = array();

            // если флаг исключения
            if ( $bExclude ) {
                // из полученных полей
                foreach ( $aData as $sName ) {
                    // убрать те, что есть в фильтре
                    if ( !in_array($sName,$aFilter) ) {
                        $aOut[$sName] = (string)$aData[$sName];
                    }
                }
            } else {
                // взять список необходимых полей
                foreach ( $aFilter as $sName ) {
                    // добавить в вывод те поля, которые есть в посылке,
                    // остальные заполнить пустышками
                    $aOut[$sName] = isset($aData[$sName]) ? (string)$aData[$sName] : '';
                }
            }

        } else {
            // не задан фильтр - просто вернуть все, что есть в посылке
            $aOut = $aData;
        }

        return $aOut;

    }

    /**
     * Отдает флаг фозможности создания наследников для данного модуля
     * в дереве процессов
     * @return bool
     */
    protected function canBeParent() {
        return false;
    }

}
