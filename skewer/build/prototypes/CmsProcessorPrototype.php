<?php
/**
 * 
 * Прототип менеджера процессов для системы администрирования
 * @class CmsProcessorPrototype
 * @project Skewer
 * @package build
 *
 * @extends skProcessorPrototype
 * @implements skProcessorInterface
 * @author ArmiT, $Author: sapozhkov $
 * @version $Revision: 1346 $
 * @date $Date: 2012-12-03 11:31:54 +0400 (Пн., 03 дек. 2012) $
 *
 *
 */ 
abstract class CmsProcessorPrototype extends skProcessorPrototype implements skProcessorInterface {
    const labelOut = 'out';

    /**
     * Отдает имя ключа для сессионного хранилища
     * @return string
     */
    abstract protected function getSessionKeyName();

    /**
     * Возвращает имя модуля основного слоя
     * @return string
     */
    abstract public function getLayoutModuleName();

    /**
     * Возвращает имя модуля авторизации
     * @return string
     */
    abstract public function getAuthModuleName();

    /**
     * Возвращает имя первично инициализируемого модуля
     * @return string
     */
    abstract public function getFrameModuleName();

    /**
     * Возвращает имя первично инициализируемого модуля при отсутствии авторизации
     * @return string
     */
    abstract public function getFrameAuthModuleName();

    /**
     * Отдает базовый url для сервиса
     * @return string
     */
    abstract public function getBaseUrl();

    /**
     * Установки межслойной работы процессора
     * @var array
     */
    protected $aDependLayers = array('Cms', 'adm', 'tool');

    /**
     * Запускает на выполнение корневой процесс, выводит результат работы дерева процессов
     * @static
     * @throws Exception
     * @return bool|string
     */
    public function build() {

        // очистить набор дополнительных файлов
        skLinker::clearCSSFiles();
        skLinker::clearJSFiles();

        /** @var $oRootProcess skProcess */
        $oRootProcess = null;

        // если тип запроса - JSON
        if(skRequest::isJSONRequest()) {

            try {

                $oProcessSession = new skProcessSession( $this->getSessionKeyName() );
                if(($sSessionId = skRequest::getSessionId()) != false)
                    if($oProcessSession->isExists($sSessionId)) {
                        $oRootProcess = $oProcessSession->load($sSessionId);
                        $this->addSessionId($sSessionId);
                    }

                $iLoopCnt = 0;
                $iStatus = 0;

                // выполнять процесс пока он возвращает psExit
                do {

                    if ( ++$iLoopCnt > 20 )
                        throw new Exception('loop error: infinit reset status');

                    if ( $iStatus==psReset ) {
                        /** @noinspection PhpUndefinedMethodInspection */
                        skProcessor::removeProcess( self::labelOut );
                        $oRootProcess=null;
                    }

                    // если процесс уже был создан в предыдущих запросах
                    if($oRootProcess instanceof skProcess){

                        $this->aProcessList[self::labelOut] = $oRootProcess;
                        $this->recoverProcessPaths($this->aProcessList);

                        // разбираем JSON пакет и инитим процессы на запуск
                        if($aJSONPackage = skRequest::getJSONPackage())
                            foreach($aJSONPackage as $sPath => $aPacket)
                                if(($oProc = $this->getProcess($sPath, psRendered)) instanceof skProcess)
                                    $oProc->setStatus(psNew);

                    }

                    // корневого процесса еще нет - создаем его
                    else {

                        // Проверка имеет ли пользователь сответствующие права
                        if ( CurrentAdmin::isLoggedIn() ){

                            // добавление корневого процесса
                            $oRootProcess = $this->addProcess(new skContext( self::labelOut, $this->getLayoutModuleName(), ctModule, array()));

                        } else {
                            $oRootProcess = $this->addProcess(new skContext( self::labelOut, $this->getAuthModuleName(), ctModule, array('viewMode'=>'form')));
                        }

                    }

                    // выполнить процесс
                    $iStatus = $this->executeProcessList();

                } while( $iStatus == psExit or $iStatus==psReset );

                // отрендерить результат
                $oRootProcess->render();
                $oProcessSession->save($oRootProcess, $sSessionId);

                // добавить в ответ результат реботы - success
                $this->addResponseStatus('Ok',true);

                // дополнительные файлы
                $aAddJSFiles = skLinker::getJSFiles();
                $aAddCSSFiles = skLinker::getCSSFiles();
                if ( $aAddJSFiles or $aAddCSSFiles ) {
                    $this->aJSONResponse['addFiles'] = array(
                        'js' => skLinker::getJSFiles(),
                        'css' => skLinker::getCSSFiles()
                    );
                }

            } catch ( Exception $e ) {
                var_dump($e);
                $this->addResponseStatus($e->getMessage(),false);
            }

            // собрать ответ в формате JSON
            $sOut = json_encode($this->aJSONResponse);

        }

        // не JSON - первичный вызов
        else {

            if( isSet($_GET['token']) && ($sToken = $_GET['token']) ){

                Auth::authUserByToken($sToken);

                skResponse::sendFoundHeaders( $this->getBaseUrl() );

                exit;

            }

            // если не залогинен и есть спец модуль для авторизации
            if ( !CurrentAdmin::isLoggedIn() and $this->getFrameAuthModuleName() ){
                $sFrameModuleName = $this->getFrameAuthModuleName();
            } else {
                // иначе загружаем стандартный
                $sFrameModuleName = $this->getFrameModuleName();
            }

            // загружаем основную обвязку страницы
            $oRootProcess = $this->addProcess(new skContext( self::labelOut, $sFrameModuleName, ctModule));

            // выполняем
            $this->executeProcessList();

            // рендерим
            $oRootProcess->render();

            // собираем ответ
            $sOut = $oRootProcess->getOut();

        }

        return $sOut;
    }

}
