<?php
/**
 * Класс для работы с деревьями
 * @class TreePrototype
 * @project Skewer
 * @package Build
 */

abstract class TreePrototype implements TreeInterface {

    protected $sPathTail = '';  // не разобранная часть пути
    const iLevelLimit = 10;    // ограничение вложенности
    const ID = 'id';
    const LINK = 'link';
    const VISIBLE = 'visible';
    const ALIAS = 'alias';
    const TITLE = 'title';
    const PARENT = 'parent';
    const TYPE = 'type';
    const POSITION = 'position';
    const ALIAS_PATH = 'alias_path';
    const LEVEL = 'level';

    /**
     * @func error - внутренняя функция регистрации ошибк
     * @param $text
     * @return string
     */
    protected static function error( $text ) {
        return $text;
    }

    /**
     * @static возвращает набор допустимых полей для таблицы
     * @return array
     */
    protected static function getTreeFieldList(){
        return array(
            self::ID => 'i',
            self::ALIAS => 's',
            self::TITLE => 's',
            self::PARENT => 'i',
            self::VISIBLE => 'i',
            self::TYPE => 'i',
            self::POSITION => 'i',
            self::ALIAS_PATH => 's',
            self::LINK => 's',
            self::LEVEL => 'i'
        );
    }

    /**
     * @func getById - получить описание по id записи
     * @param $id - идентификатор записи
     * @return array
     */
    public static function getById( $id ) {

        // запрос
        global $odb;
        return $odb->getRow(
            'SELECT * FROM `[table_name:q]` WHERE `id`=[id:i]',
            array(
                self::ID => $id,
                'table_name' => static::getTableName()
            )
        );

    }

    /**
     * Отдает тип раздела по id, или flase, если нет записи
     * @static
     * @param $iSectionId - id раздела
     * @return bool|int
     */
    public static function getTypeById( $iSectionId ) {

        // запрос раздела
        $aRow = static::getById( $iSectionId );

        if ( $aRow and isset($aRow[self::TYPE]) )
            return (int)$aRow[self::TYPE];
        else
            return false;

    }

    /**
     * Отдает название раздела по id
     * @static
     * @param $iSectionId - id раздела
     * @return string
     */
    public static function getTitleById( $iSectionId ) {

        // запрос раздела
        $aRow = static::getById( $iSectionId );

        if ( $aRow and isset($aRow[self::TITLE]) )
            return $aRow[self::TITLE];
        else
            return '';

    }

    /**
     * Отдает набор строк по списку id
     * @param $idList
     * @param bool $keepOrder
     * @return array|bool
     */
    public static function getByIdList( $idList , $keepOrder = true) {

        if ( is_array($idList) and count($idList)) {

            // если набор id

            foreach ( $idList as $key=>$id )
                $idList[$key] = (int)$id;
            $aWhereConditions = '`id` IN ('.implode(',',$idList).')';

            // сборка запроса
            $sQuery = 'SELECT * FROM `[table_name:q]` WHERE [where:q]';

            global $odb;
            $oResult = $odb->query(
                $sQuery,
                array(
                    'table_name' => static::getTableName(),
                    'where' => $aWhereConditions
                )
            );

            // сборка выходного массива
            $aOut = array();
            while ($aRow = $oResult->fetch_array(MYSQLI_ASSOC))
                $aOut[ $aRow[self::ID] ] = $aRow;

            if($keepOrder){
                $result = array();
                foreach($idList as $id)
                    if(isset($aOut[$id])) $result[] = $aOut[$id];
                return $result;
            } // if keep order

            else return $aOut;

        } else {
            return false;
        }
    }


    /**
     * @func getIdByPath - получить id записи по пути
     * @param string $sTotalPath путь
     * @param string $sPathTail изменяемая переменная для остатка пути
     * @return int
     */
    public static function getIdByPath( $sTotalPath, &$sPathTail='' ) {

        // первичные проверки
        if ( !$sTotalPath ) {
            //echo $this->sPathTail = '';
            return 0;
        }

        $sPath = $sTotalPath;    // начальный путь
        //if ( $sPath{0} != '/' ) $sPath = '/'.$sPath;

        global $odb;
        $id = 0;                // найденный идентификатор
        $i = 0;

        // циклически проверить путь
        do {
            // запрос
            $aRow = $odb->getRow(
                'SELECT `id` FROM `[table_name:q]` WHERE `alias_path`=[path:s]',
                array(
                    'path' => $sPath,
                    'table_name' => static::getTableName()
                )
            );

            // если нашли строку
            if ( $aRow ) {
                $id = $aRow[self::ID];
            } else {
                $sPath = substr( $sPath, 0, strrpos($sPath,'/',-2)+1 );
            }

        } while ( !$id and $sPath and $sPath!=='/' and ++$i<static::iLevelLimit );

        $sPathTail = ($sPath===$sTotalPath) ? '' : substr( $sTotalPath, strlen($sPath) );

        return $id;

    }

    /**
     * @func getSubItems - получить набор подразделов заданного раздела
     * @param $mParentId - (int/array) - 1 или несколько родительских разделов
     * @param array $aFilter
     *      visibility = 0/1/2
     *      sort = position / title / title:ASC / title:DESC
     *      auth_list = array - набор допустимых id
     *      fields = array
     * @return array
     */
    public static function getSubItems( $mParentId, $aFilter=array() ) {

        // наборы для выборки
        $aWhereConditions = array();
        $aOrderConditions = array();

        // сборка условия запроса по родительской записи
        if ( is_array($mParentId) ) {
            // если набор родительских id
            $aParentList = array();
            foreach ( $mParentId as $p_id )
                $aParentList[] = (int)$p_id;
            $aWhereConditions[] = '`parent` IN ('.implode(',',$aParentList).')';
        } else {
            $aWhereConditions[] = '`parent`='.(int)$mParentId;
        }

        // видимость
        if ( isset($aFilter['visibility']) )
            $aWhereConditions[] = ' `visible`='.(int)$aFilter['visibility'];

        // доступные id
        if ( isset($aFilter['auth_list']) and $aFilter['auth_list'] ) {
            if ( is_string($aFilter['auth_list']) )
                $aWhereConditions[] = '`id`IN ('.$aFilter['auth_list'].')';
            elseif ( is_array($aFilter['auth_list']) )
                $aWhereConditions[] = '`id`IN ('.implode(',',$aFilter['auth_list']).')';
        }

        // сортировка
        if ( !isset($aFilter['sort']) )
            $aFilter['sort'] = self::POSITION;
        switch ( $aFilter['sort'] ) {
            case self::TITLE:
            case 'title:ASC':
                $aOrderConditions[] = '`title` ASC';
                break;
            case 'title:DESC':
                $aOrderConditions[] = '`title` DESC';
                break;
            case self::POSITION:
                $aOrderConditions[] = '`position` ASC';
                break;
        }

        // набор полей
        if ( isset($aFilter['fields']) and $aFilter['fields'] and is_array($aFilter['fields']) ) {

            // вычислить набор полей
            $aFieldList = array_keys( self::getTreeFieldList() );
            $aFields = array_intersect($aFilter['fields'],$aFieldList);

            // собрать строку с набором полей
            $sFields = $aFields ? '`'.implode('`,`',$aFields).'`' : '*';

        } else {
            $sFields = '*';
        }

        // сборка запроса
        $sQuery =
            'SELECT [fields:q] FROM `[table_name:q]` '.
            ($aWhereConditions?'WHERE [where:q] ':'').
            ($aOrderConditions?'ORDER BY [order:q] ':'')
        ;

        // выполенение запроса
        global $odb;
        $oResult = $odb->query(
            $sQuery,
            array(
                'fields' => $sFields,
                'table_name' => static::getTableName(),
                'where' => implode(' AND ',$aWhereConditions),
                'order' => implode(' , ',$aOrderConditions)
            )
        );

        // проверка правильности выполнения запроса
        if ( $odb->errno ) {
            static::error( $odb->error );
            return array();
        }

        // сборка выходного массива
        $aOut = array();
        while ($aRow = $oResult->fetch_array(MYSQLI_ASSOC)){
            $aOut[ $aRow[self::ID] ] = $aRow;
        }

        return $aOut;

    }


    /**
     * Обобначить разделы без наследников
     * @param $aItems
     * @return array
     */
    public static function markLeafs( $aItems ) {

        if(!count($aItems)) return array();

        // набор id разделов
        $aIdList = array_keys($aItems);

        // сборка запроса
        $sQuery =
            'SELECT `parent` FROM `[table_name:q]`
             WHERE `parent` IN ([parents:q]) GROUP BY `parent`;'
        ;

        // выполенение запроса
        global $odb;
        $oResult = $odb->query(
            $sQuery,
            array(
                'table_name' => static::getTableName(),
                'parents' => implode(',',$aIdList)
            )
        );

        // сборка выходного массива
        $aHasChild = array();
        while ($aRow = $oResult->fetch_array(MYSQLI_ASSOC))
            $aHasChild[] = $aRow[self::PARENT];

        // добавить пустой контейнер тем, у кого наследников нет
        foreach ( $aIdList as $iId ) {
            if ( !in_array($iId,$aHasChild) )
                $aItems[$iId]['children'] = array();
        }

        return $aItems;

    }

    /**
     * Отдает путь для заданного по id раздела
     * @param int $iId
     * @return string
     */
    public static function getAliasPathById( $iId ) {

        $sPath = '';

        // сборка запроса
        $sQuery = 'SELECT `alias_path` FROM `[table_name:q]` WHERE `id`=[id:i]';

        // выполенение запроса
        global $odb;
        $oResult = $odb->query(
            $sQuery,
            array(
                'table_name' => static::getTableName(),
                self::ID => (int)$iId
            )
        );

        // проверка правильности выполнения запроса
        if ( $odb->errno ) {
            static::error( $odb->error );
            return $sPath;
        }

        if ($aRow = $oResult->fetch_array(MYSQLI_ASSOC)){
            $sPath = $aRow[self::ALIAS_PATH];
        }

        return $sPath;

    }

    /**
     * Отдает имя заданного по id раздела
     * @param int $iId
     * @return string
     */
    public static function getAliasById( $iId ) {

        $sPath = '';

        // сборка запроса
        $sQuery = 'SELECT `alias` FROM `[table_name:q]` WHERE `id`=[id:i]';

        // выполенение запроса
        global $odb;
        $oResult = $odb->query(
            $sQuery,
            array(
                'table_name' => static::getTableName(),
                self::ID => (int)$iId
            )
        );

        // проверка правильности выполнения запроса
        if ( $odb->errno ) {
            static::error( $odb->error );
            return $sPath;
        }

        if ($aRow = $oResult->fetch_array(MYSQLI_ASSOC)){
            $sPath = $aRow[self::ALIAS];
        }

        return $sPath;

    }

    /**
     * Отдает id заданного по псевдониму раздела в заданном родительском
     * @param string $sAlias псевдоним
     * @param int $iParent id родительского раздела
     * @return int|null
     */
    public static function getByAlias( $sAlias, $iParent ) {

        // сборка запроса
        $sQuery = 'SELECT `id` FROM `[table_name:q]` WHERE `alias`=[alias:s] AND `parent`=[parent:i] LIMIT 1';

        // выполенение запроса
        global $odb;
        $oResult = $odb->query(
            $sQuery,
            array(
                'table_name' => static::getTableName(),
                self::ALIAS => $sAlias,
                self::PARENT => $iParent
            )
        );

        // проверка правильности выполнения запроса
        if ( $odb->errno ) {
            static::error( $odb->error );
            return null;
        }

        if ($aRow = $oResult->fetch_array(MYSQLI_ASSOC)){
            return $aRow[self::ID];
        } else
            return null;

    }

    /**
     * Возвращает id родительского раздела
     * @param $iSectionId
     * @return int
     */
    public static function getParentId($iSectionId) {

        if ( $aRow = static::getById( $iSectionId ) )
            $iParentId = (int)$aRow[self::PARENT];
        else
            $iParentId = 0;

        return $iParentId;
    }

    /**
     * Возвращает набор id родительских разделов для заданного
     * @param $iSectionId
     * @param $iStopSectionId
     * @return bool
     */
    public static function getSectionParents( $iSectionId, $iStopSectionId = -1 ) {

        // выходной массив
        $aParents = array();

        // текущий обрабатываемый раздел
        $iNowParentId = $iSectionId;

        do {

            // запрос родительского раздела
            $iNowParentId = static::getParentId( $iNowParentId );

            // выходим если достигли стоп-вершины
            if($iNowParentId == $iStopSectionId) break;

            // дополнение выходного массива
            if ( $iNowParentId )
                $aParents[] = $iNowParentId;

        } while ( $iNowParentId );

        return $aParents;

    }

    /**
     * @param $iSectionId
     * @param $iVisible
     * @return bool|mysqli_result
     */
    public static function setVisible( $iSectionId, $iVisible ) {

        // сборка запроса
        $sQuery = 'UPDATE `[table_name:q]` SET `visible`=[visible:i] WHERE `id`=[id:i]';

        // выполенение запроса
        global $odb;
        $mResult = $odb->query(
            $sQuery,
            array(
                'table_name' => static::getTableName(),
                self::VISIBLE => $iVisible,
                self::ID => (int)$iSectionId
            )
        );

        // проверка правильности выполнения запроса
        if ( $odb->errno ) {
            static::error( $odb->error );
            return false;
        }

        return $mResult;

    }

    /**
     * Сохраняет запись по входному массиву
     * @param array $aRow
     * @return int
     */
    public static function saveItem( $aRow ) {

        // идентификатор
        if ( isset($aRow[self::ID]) ) {
            $iId =  (int)$aRow[self::ID];
            unset( $aRow[self::ID] );
        } else $iId = 0;

        // выбрать переменные для сохранения
        $aAllowFields = self::getTreeFieldList();
        $aSaveFields = array();
        $aSaveValues = array();
        foreach ( $aRow as $sName => $sVal ) {
            if ( isset($aAllowFields[$sName]) ) {
                $aSaveFields[] = sprintf('`%s`=[%1$s:%s]',$sName,$aAllowFields[$sName]);
                $aSaveValues[$sName] = $sVal;
            }
        }
        /** @todo Для корневых разделов не нужно строить alias_path, поэтому visible -1 */
        if(isSet($aSaveValues[self::PARENT]) && !$aSaveValues[self::PARENT]) {
            $aSaveValues[self::VISIBLE] = -1;
        }

        // если пусто - выйти

        if (empty($aSaveFields) )
            return 0;

        // составление запроса
        $sQuery =
            ($iId ? 'UPDATE `' : 'INSERT `').static::getTableName().'` '.
            'SET '.implode(',',$aSaveFields).
            ($iId ? ' WHERE `id`=[id:i]' : '');
        global $odb;

        $iResult = $odb->query( $sQuery, $aSaveValues+array( self::ID =>$iId) );

        // проверка правильности выполнения запроса

        if ( $odb->errno ) {
            static::error( $odb->error );
            return 0;
        }

        $iLastId = $odb->insert_id;

        static::updateAliasPaths(skConfig::get('section.main'));

        // вернуть id сохраненной записи или 0
        $iOut = $iResult ? ($iId ? $iId : $iLastId) : 0;
        return $iOut;

    }

    /**
     * Удаляет ветку начиная с заданного раздела + удаляет параметры
     * @param $iId - id удаляемого раздела
     * @return bool
     */
    public static function deleteItem( $iId ) {

        // все id
        $aIdList = array();

        // id текущего среза
        $aLevelIdList = array( $iId );

        // собрать все id ветки
        $iCnt = 0; // ограничивающий счетчик
        while ( !empty( $aLevelIdList ) and ++$iCnt<20 ) {

            // дополнить общий массив
            $aIdList += array_merge($aIdList,$aLevelIdList);

            // запросить набор для текущего слоя
            $aLevelIdList = static::getSubIdList( $aLevelIdList );

        }

        // удалить все разделы
        $bRes = (bool)static::deleteByIdList( $aIdList );

        // удаление связанных элементов
        $FormsAdmApi = new FormsAdmApi();
        $FormsAdmApi->deleteFormBySection( $aIdList );

        /** @todo Переделать! */

        EventsApi::removeFromSection($iId);
        NewsApi::removeFromSection($iId);
        /*  */
        return $bRes;

    }

    /**
     * Выбирает массив id подчиненных разделов
     * @param $aIn - входной массив id разделов
     * @return array
     */
    protected static function getSubIdList( $aIn ) {

        $aOut = array();

        // нет входа - выйти
        if ( empty($aIn) ) return $aOut;

        // сборка запроса
        $sQuery = 'SELECT `id` FROM `[table_name:q]` WHERE `parent` IN ([parents:q])';

        // выполенение запроса
        global $odb;
        $oResult = $odb->query(
            $sQuery,
            array(
                'table_name' => static::getTableName(),
                'parents' => implode(',',$aIn)
            )
        );

        // проверка правильности выполнения запроса
        if ( $odb->errno ) {
            static::error( $odb->error );
            return array();
        }

        // сборка выходного массива
        while ($aRow = $oResult->fetch_array(MYSQLI_ASSOC)){
            $aOut[] = $aRow[self::ID];
        }

        return $aOut;

    }

    /**
     * Удаляет набор разделов по списку id (с параметрами)
     * @param $aIdList
     * @return int
     */
    public static function deleteByIdList( $aIdList ) {

        // сборка запроса
        $sQuery = 'DELETE FROM `[table_name:q]` WHERE `id` IN ([ids:q])';

        // выполенение запроса
        global $odb;
        $odb->query(
            $sQuery,
            array(
                'table_name' => static::getTableName(),
                'ids' => implode(',',$aIdList)
            )
        );

        // проверка правильности выполнения запроса
        if ( $odb->errno ) {
            static::error( $odb->error );
            return 0;
        }

        $iRes = $odb->affected_rows;

        /**
         * удалить все параметры
         */

        // объект для работы с параметрами
        $oParam = new Parameters();

        $oParam->delByParentIdList( $aIdList );

        return $iRes;

    }

    /**
     * Возвращает значение параметра для сортировки для нового раздела
     * @param $iParentId
     * @return int
     */
    public static function getNextPosByParentId( $iParentId ) {

        // сборка запроса
        $sQuery = 'SELECT MAX(`position`) AS `val` FROM `[table_name:q]` WHERE `parent`=[parent:i]';

        // выполенение запроса
        global $odb;
        $oResult = $odb->query(
            $sQuery,
            array(
                'table_name' => static::getTableName(),
                self::PARENT => $iParentId
            )
        );

        // проверка правильности выполнения запроса
        if ( $odb->errno ) {
            static::error( $odb->error );
            return 0;
        }

        if ($aRow = $oResult->fetch_array(MYSQLI_ASSOC)){
            return $aRow['val']+1;
        } else return 0;

    }

    /**
     * Обновляет пути
     * @static
     * @param int $defaultSection
     */
    private static function updateAliasPaths($defaultSection=0) {

        $section=0;
        $level=0;
        $path='/';

        static::updateAliasPathsRec($section, $level, $path, $defaultSection);

    }

    /**
     * Рекурсивное обновление пути
     * @static
     * @param int $section
     * @param int $level
     * @param string $path
     * @param int $defaultSection
     */
    private static function updateAliasPathsRec($section=0, $level=0, $path='/', $defaultSection=0) {
        global $odb;

        // рекурсивно перестроить информацию о маршрутах в таблице main.
        // принцип обхода:
        // вершины с level < 0 не обрабатываются (потомки также не обрабатываются)
        // вершины с visible < 0 или alias = *  - пропускаются в пути

        $aData[self::LEVEL]      = $level++;
        $aData['path']       = $path;
        $aData['section']    = $section;
        $aData['table_name'] = static::getTableName();

        // обрабатываем нормальные вершины
        $sQuery = "
          UPDATE
            `[table_name:q]`
          SET
            `alias_path`=CONCAT([path:s],`alias`,'/'),
            `level`=[level:i]
          WHERE
            parent = [section:i] AND
            visible IN (0,1) AND
            alias!='';";

        $odb->query($sQuery, $aData);

        // обрабатываем вершины без алиасов
        $sQuery = "
          UPDATE
            `[table_name:q]`
          SET
            `alias_path`=CONCAT([path:s],`id`,'/'),
            `level`=[level:i]
          WHERE
            parent = [section:i] AND
            visible IN (0,1) AND
            alias='' ;";

        $odb->query($sQuery, $aData);

        // обрабатываем вершины с пропусками
        $sQuery = "
          UPDATE
            `[table_name:q]`
          SET
            `alias_path`=[path:s],
            `level`=[level:i]
        WHERE
            parent = [section:i] AND
            visible=2;";

        $odb->query($sQuery, $aData);

        // обходим дочерние вершины
        $sQuery = "
            SELECT
                id,
                alias_path,
                parent
            FROM
                `[table_name:q]`
            WHERE
                parent = [section:i] AND
                level > -1;";

        $oResult = $odb->query($sQuery, $aData);

        while ($item = $oResult->fetch_array(MYSQLI_ASSOC)) {

            $double_slash = strpos($item[self::ALIAS_PATH], '//');

            $item['table_name'] = static::getTableName();

            // исправляем пути если алиас со слешем
            if ($double_slash !== false) {

                $item[self::ALIAS_PATH] = substr($item[self::ALIAS_PATH], $double_slash + 1); // было +2

                $sQuery = "
                    UPDATE
                        `[table_name:q]`
                    SET
                        `alias_path`=[alias_path:s]
                    WHERE id=[id:i];";

                $odb->query($sQuery, $item);

            }

//             исправляем пути если первый алиас со слешем
//            отключено за ненадобностью
//            if (strpos($item['alias_path'], '/') === 0) {
//
//                $item['alias_path'] = substr($item['alias_path'], 1);
//                $sQuery = "
//                  UPDATE
//                    `[table_name:q]`
//                  SET
//                    `alias_path`=[alias_path:s]
//                  WHERE
//                    id=[id:i]";
//
//                $odb->query($sQuery, $item);
//
//            }
            /** >>>>>>>>>>>>>>>>>>>>>>>>> recursion */
            static::updateAliasPathsRec($item[self::ID], $level, $item[self::ALIAS_PATH], $defaultSection);

        }// while

        // сбрасываем пути вершин с пропусками, чтобы исключить их из адреса
        if ($level == 1) {

            $sQuery = "
              UPDATE
                `[table_name:q]`
              SET
                `alias_path`=''
              WHERE
                  parent = [section:i] AND
                  visible=2;";

            $odb->query($sQuery, $aData);

            // обходим все пути и проверяем на совпадение
            $last_path = '';

            $sQuery = "
              SELECT
                id,
                alias,
                alias_path,
                parent
              FROM
                `[table_name:q]`
              WHERE
                alias_path!='' AND
                visible IN (0,1) AND
                level > -1
              ORDER BY
                alias_path,
                level;";

            $oResult = $odb->query($sQuery, $aData);

            while ($item = $oResult->fetch_array(MYSQLI_ASSOC)){

                $item['table_name'] = static::getTableName();

                if ($last_path == $item[self::ALIAS_PATH]) {

                    $item['new_alias_path'] = substr($item[self::ALIAS_PATH], 0, -1) . '-' . $item[self::ID] . '/';

                    $sQuery = "
                      SELECT
                        COUNT(id) as `alias_duplicates`
                      FROM
                        `[table_name:q]`
                      WHERE
                        alias_path=[new_alias_path:s] AND
                        visible IN (0,1) AND
                        level > -1;";

                    $oSubresult = $odb->query($sQuery, $item);

                    $aAliasDuplicates = $oSubresult->fetch_array(MYSQL_ASSOC);

                    if ($aAliasDuplicates['alias_duplicates'] == 0) {

                        $sQuery = "
                          UPDATE
                            `[table_name:q]`
                          SET
                            `alias` = CONCAT(`alias`,'-',`id`),
                            `alias_path` =[new_alias_path:s]
                          WHERE
                            id = [id:i];";

                        $odb->query($sQuery, $item);

                    } // если нет повторений
                } // if

                $last_path = $item[self::ALIAS_PATH];
            } // while
            //
            // обходим вершины которые мы пропускали и восстанавливаем им путь (для ссылок)
            $sQuery = "
                SELECT
                    id
                FROM
                    `[table_name:q]`
                WHERE
                    `visible` = 2 AND
                    `level` > -1;";

            $oResult = $odb->query($sQuery, $aData);

            while ($item = $oResult->fetch_array(MYSQL_ASSOC)) {
//                $item['new_alias_path'] = $this->get_first_subsection_path($item['id']);
//
//                if ($new_path) {
//                    $q = "UPDATE " . MINC_DBPREFIX . "main SET `alias_path` ='$new_path'
//                      WHERE id = " . $item['id'];
//                    mysql_query($q);
//                } // if
            } // while
        } // if

        // Убираем дубль главной страницы
        if($level==1 and $defaultSection){

            $sQuery = "
                    UPDATE
                        `[table_name:q]`
                    SET
                        `alias_path`=''
                    WHERE id=[id:i];";

            $odb->query($sQuery, array(
                'table_name'=>static::getTableName() ,
                self::ID =>$defaultSection)
            );
        }
    }// func

    /**
     * @static Метод получения id раздела по id родителя
     * @param $iParentSection
     * @return bool|mixed
     */
    public static function getIdByParent( $iParentSection ){

        global $odb;

        $aData = array( self::PARENT =>$iParentSection);

        $sQuery = "
            SELECT
                `id`
            FROM
                `tree_section`
            WHERE
                `parent` = [parent:i];";

        $rResult = $odb->query($sQuery, $aData);
        $aResult = array();

        if( $rResult->num_rows ){
            while( $aRow = $rResult->fetch_array(MYSQLI_ASSOC) ){
                $aResult[] = $aRow;
            }
            return $aResult;
        }

        return false;
    }

    /**
     * Выполняет сдвиг положений для подчиненных разделов
     *      для вставки раздела середину/начало, а не в конец
     *
     * @param int $iParentId - родительский id
     * @param int $iPositionId - позиция с которой сдвигать
     * @param string $sShiftSign - знак сдвига > / >=
     * @return int
     */
    public static function shiftPositionByParent($iParentId, $iPositionId, $sShiftSign='>=') {

        global $odb;

        $sQuery = "
            UPDATE `[table:q]`
            SET `position`=`position`+1
            WHERE
                `parent` = [parent:i] AND
                `position` [sign:q] [position:i]";

        $odb->query($sQuery, array(
            'table' => static::getTableName(),
            self::PARENT =>$iParentId,
            'sign' => $sShiftSign,
            self::POSITION => $iPositionId
        ));

        return $odb->affected_rows;

    }

    /**
     * Собрать псевдоним для строки
     * @static
     * @param $sIn
     * @return string
     */
    public static function makeAlias( $sIn ) {

        // приведение к типу
        $sOut = (string)$sIn;

        // замена
        $sOut = skTranslit::change( $sOut );

        // заменить запрщенные символы и слить их в один, если рядом
        $sOut = skTranslit::changeDeprecated( $sOut );
        $sOut = skTranslit::mergeDelimiters( $sOut );

        // убрать разделители по краям
        $sOut = trim( $sOut, skTranslit::getDelimiter() );

        // обрезать
        $sOut = substr( $sOut, 0, 32 );

        return $sOut;

    }

    public static function getAllNodesId() {

        global $odb;

        $sQuery = "
            SELECT
              id
            FROM
              `[table:q]`;";

        $oResult = $odb->query($sQuery, array('table' => static::getTableName()));

        if(!$oResult->num_rows) return false;

        $aOut = array();

        while($aItem = $oResult->fetch_assoc())
            $aOut[] = $aItem[self::ID];

        return (count($aOut))? $aOut: false;
    }// func

    public static function updModifyDate($iSectionId){

        global $odb;

        $sQuery = "
            UPDATE
                `tree_section`
            SET
                `last_modified_date` = NOW()
            WHERE
                `id` = [id:i];";

        return $odb->query($sQuery, array( self::ID => $iSectionId));
    }

    public static function getModifiedDate($iSectionId){

        global $odb;

        $sQuery = "
            SELECT
                `last_modified_date`
            FROM
                `tree_section`
            WHERE
                `id` = [id:i];";

        $aDate = $odb->getRow($sQuery, array( self::ID => $iSectionId));

        return $aDate['last_modified_date'];
    }

    /**
     * Добавляет раздел (all on place)
     * @param int $iParent Id родительского раздела
     * @param string $sTitle Название раздела
     * @param int $iTemplateId Id шаблонного раздела
     * @param string $sAlias Псевдоним
     * @param bool $bVisible Флаг видимости
     * @param string $sLink Ссылка, ведущая с раздела
     * @return bool|int Возвращает Id созданного раздела либо false
     * @throws Exception
     */
    public static function addSection($iParent, $sTitle, $iTemplateId = 0, $sAlias = '', $bVisible = true, $sLink = '') {

        try {

            $aItem[self::LINK]     = $sLink;
            $aItem[self::ALIAS]    = (!empty($sAlias))? $sAlias: self::makeAlias($sTitle);
            $aItem[self::TITLE]    = $sTitle;
            $aItem[self::PARENT]   = $iParent;
            $aItem[self::VISIBLE]  = $bVisible;
            $aItem[self::POSITION] = static::getNextPosByParentId( $iParent );

            if(!$iNewSection = static::saveItem($aItem)) throw new Exception('Раздел не добавлен!');

            Policy::incPolicyVersion();
            CurrentAdmin::reloadPolicy();

            if(!$iTemplateId) return $iNewSection;

            $oParams = new Parameters();

            $aFilter = array();
            $aFilter['fields'] = array(
                self::ID,
                self::TITLE, 'value', 'show_val', 'access_level');
            $aFilter['with_access_level'] = '>0';

            // запросить данные
            $aAddParams = $oParams->getAllAsList( $iTemplateId, $aFilter, false );

            // добавить все параметры
            if(count($aAddParams))
                foreach ( $aAddParams as $aParam ) {

                    $aParam[self::ID] = 0;
                    $aParam[self::PARENT] = $iNewSection;
                    $oParams->saveParameter($aParam);
                }// add each param


            $aTplParam = $oParams->getByName( $iNewSection, '.', 'template', false );

            // нет - создать
            if ( !$aTplParam )
                $aTplParam = array(
                    self::ID => 0,
                    self::PARENT => $iNewSection,
                    'group' => '.',
                    'name' => 'template'
                );

            // сохранить значение параметра
            $aTplParam['value'] = $iTemplateId;

            // сохранить
            $oParams->saveParameter($aTplParam);


            return $iNewSection;

        } catch (UpdateException $e) {

            return false;
        }

    }

}
