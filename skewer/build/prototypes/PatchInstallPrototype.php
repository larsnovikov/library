<?php
/**
 *
 * @class PatchInstallPrototype
 *
 * @author ArmiT, $Author: sapozhkov $
 * @version $Revision: 1146 $
 * @date $Date: 2012-11-08 16:57:07 +0400 (Чт., 08 нояб. 2012) $
 * @project JetBrains PhpStorm
 * @package kernel
 */

abstract class PatchInstallPrototype extends skUpdateHelper implements skPatchInterface {

    /**
     * Описание патча
     * @var string
     */
    public $sDescription = '';

    /**
     * @throws UpdateException
     */
    public function __construct() {

        parent::__construct();
    }// constructor

    /**
     * Базовый метод запуска обновления
     * @throws skException
     * @throws UpdateException
     * @return bool
     */
    public function execute() {}// func

}// class

