<?php
/**
 * 
 * @class TabsCmsModule
 *
 * @author ArmiT, $Author: acat $
 * @version $Revision: 1172 $
 * @date $Date: 2012-11-12 15:17:31 +0400 (Пн., 12 нояб. 2012) $
 *
 */ 
class TabsCmsModule extends AdminModulePrototype {

    // набор отработавших дочерних модулей
    protected $childList = array();

    public function init(){

        $this->setParser(parserJSON);

    }// func

    /** @var string суперкласс для подчиненных элементов */
    private $sAllowedChildClass = 'AdminTabModulePrototype';

    /**
     * Отдает класс-родитель, насдедники которого могут быть добавлены в дерево процессов
     * @return string
     */
    protected function getAllowedChildClass() {
        return $this->sAllowedChildClass;
    }

    /**
     * Устанавливает суперкласс для подчиненных элементов
     * @param $sAllowedChildClassName
     */
    protected function setAllowedChildClass( $sAllowedChildClassName ) {
        $this->sAllowedChildClass = $sAllowedChildClassName;
    }

    /**
     * Состаяние. Первичная агрузка
     * @return array
     */
    protected function actionInit() {}

    /**
     * Загрузка вкладок для определенной страницы
     * @throws Exception
     */
    protected function actionLoadTabs(){

        // запросить набор параметров
        $mItemId = $this->getStr('itemId');
        $sListLabel = $this->getStr('module');
        $sTabName = $this->getStr('tab');

        /** @var \skProcess $oListContainerProcess объект контейнера списочных элементов */
        $oListContainerProcess = $this->getProcess( CmsProcessorPrototype::labelOut.'.'.LayoutCmsModule::labelLeft, psAll );
        if ( !is_object($oListContainerProcess) )
            throw new ModuleAdminErrorException('Не найден процесс контейнера списков в дереве');

        /** @var \LeftPanelCmsModule $oListContainer  */
        $oListContainer = $oListContainerProcess->getModule();
        if ( is_subclass_of( $oListContainer, 'AdminTabListModulePrototype' ) )
            throw new ModuleAdminErrorException('Процесс контейнера списков имеет неверный родительский класс');

        // проверить нальчие модуля
        $oListModule = $oListContainer->getModule( $sListLabel );

        $this->setAllowedChildClass( $oListModule->getAllowedChildClassForTab() );

        // запросить набор вкладок
        $aSubModules = $oListModule->getTabsInitList( $mItemId );

        // передать обратно в модуль
        $this->setData('itemId', $mItemId);
        $this->setData('module', $sListLabel);
        $this->setData('tab', $sTabName);

        // очистить список вкладок
        $this->childList = array();

        // дополнительные параметры
        $aAddParams = $oListModule->getTabsAddParams( $mItemId );

        // удалние всех подчиненных модулей
        $this->removeAllChildProcess();

        foreach ( $aSubModules as $sAlias => $sSubModuleName ) {

            // параметры
            $aParams = isset($aAddParams[$sAlias]) ? $aAddParams[$sAlias] : array();

            // добавление процесса
            $this->setProcess( $sListLabel.'_'.$sAlias, $sSubModuleName, $sTabName, $aParams );

        }

        // передача списка имен вызванных объектов
        $this->setData( 'children', $this->childList );
    }

    /**
     * Создает/переустанавливает подчиненный объект
     *
     * @param string $sLabel Метка вызова
     * @param string $sClassName Имя класса вызываемого модуля
     * @param string $sSelectedTab имя текущей вкладки
     * @param array $aParams Параметры вызова модуля
     * @return void
     */
    protected function setProcess( $sLabel, $sClassName, $sSelectedTab, $aParams = array() ) {

        $this->childList[] = $sLabel;

        // если есть объект
        if ( $process = $this->getChildProcess($sLabel) ) {

            // установить статус новый
            $this->setChildProcessStatus($sLabel,psNew);

        } else {

            // нет - создать
            $process = $this->addChildProcess(new skContext($sLabel,$sClassName,ctModule,$aParams));

        }

        // для активной вкладки основной интерфейс сразу
        if ( $sLabel === $sSelectedTab ) {
            $process->addRequest('cmd','init');
        } else {

            // для остальных проверить возможность загрузки упрощенного

            // задать базовые параметры
            $oRC = new ReflectionClass($process->getModuleClass());
            if ( $oRC->isSubclassOf('AdminTabModulePrototype') ) {
                $process->addRequest('cmd','initTab');
            } else {
                $process->addRequest('cmd','init');
            }

        }

        $process->addRequest('itemId',$this->getInt('itemId'));
        $process->addRequest('sectionId',$this->getInt('itemId'));

    }

}// class
