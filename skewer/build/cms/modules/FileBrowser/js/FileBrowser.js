/**
 * Библиотека для вывода раскладки файловго менеджера
 */
Ext.define('Ext.cms.FileBrowser', {
    extend: 'Ext.Viewport',
    title: buildLang.fileBrowserPanelTitle,
    height: '90%',
    width: '90%',
    layout: 'border',
    closeAction: 'hide',
    modal: true,
    componentsInited: false,

    senderData: {},

    defaults: {
        margin: '3 3 3 3'
    },

    defaultSection: 1,

    items: [{
        region: 'center',
        html: 'viewport'
    }],

    initComponent: function() {

        this.callParent();

        // событие при событии выбора раздела во всплывающем окне
        processManager.addEventListener('tabs_load', this.path, 'onSectionSelect');

        // событие при выборе файла во всплывающем окне
        processManager.addEventListener('select_file_set', this.path, 'onFileSelect');

        // идентификатор места возврата
        this.returnTo = this.parseUrl('returnTo');

    },

    execute: function( data, cmd ) {

        switch ( cmd ) {
            case 'openNode':
                this.showSection( data['nodeId'] );
                break;
            case 'init':
                // идентификатор имени модуля
                var module = this.parseUrl('module');

                if ( module ) {

                    // запрос id раздела для модуля
                    processManager.setData(this.path,{
                        cmd: 'getModuleNodeId',
                        module: module
                    });
                    processManager.postData();

                } else {

                    var iSectionId;

                    iSectionId = this.parseUrl('section');

                    if ( !iSectionId ) {
                        // попытаться запросить дерево
                        var parent = window.opener;

                        // если есть - выбрать раздел
                        if ( parent && parent['processManager'] ) {

                            iSectionId = parent['processManager'].getEventValue( 'get_section_id' );
                            this.showSection(iSectionId);
                        }
                    }

                    if ( iSectionId )
                        this.showSection(iSectionId);

                }
                break;

        }

        if ( data.error )
            sk.error( data.error );
    },

    /**
     * Открывает раздел
     * @param iSectionId
     */
    showSection:function (iSectionId) {

        var tree = processManager.getProcess(this.path + '.tree');
        if (tree) {

            var bExpandBranch = false;

            if (!iSectionId) {
                iSectionId = this.defaultSection;
                bExpandBranch = true;
            }

            tree.findSection(iSectionId);

            if (bExpandBranch) {
                var node = tree.store.getNodeById(iSectionId);

                if (!node.isLoaded() && !node.isLoading()) {
                    // развернуть, если не раскрыто
                    if (!node.isExpanded()) {
                        node.expand(); // вызовет cms событие раскрытия и расставит ext параметры для ветки
                    }
                }

            }

        }
    },

    // при воборе раздела в дереве
    onSectionSelect: function ( iSectionId ) {

        this.setSectionToFilesPanel( iSectionId );

    },

    // установить значение раздела для панели с файлами
    setSectionToFilesPanel: function( iSectionId ) {

        if ( !iSectionId )
            return false;

        // попытаться запросить дерево
        var filesPanel = processManager.getProcess( this.path+'.files' );

        // если есть - выбрать раздел
        if ( filesPanel ) {

            processManager.setData(filesPanel.path,{
                cmd: 'list',
                sectionId: iSectionId
            });

            processManager.postDataIfExists();

        }

        return true;

    },

    /**
     * При воборе файла во всплывающем окне
     * @param value
     */
    onFileSelect: function( value ) {

        if ( !window.top.opener )
            return false;

        switch ( this.returnTo ) {

            case 'ckeditor':

                if ( !window.top.opener['CKEDITOR'] )
                    break;

                window.top.opener['CKEDITOR'].tools.callFunction(this.parseUrl('CKEditorFuncNum'), value );
                window.top.close();
                window.top.opener.focus();

                break;

            case 'fileSelector':

                if ( !window.top.opener['processManager'] )
                    break;

                window.top.opener['processManager'].fireEvent('set_file', {
                    ticket: this.parseUrl('ticket'),
                    value: value
                });
                window.top.close();
                window.top.opener.focus();

                break;

        }

        return true;

    },

    parseUrl: function(name) {
        name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
        var regexS = "[\\?&]"+name+"=([^&#]*)";
        var regex = new RegExp( regexS );
        var results = regex.exec( window.location.href );
        if (null == results) {
            return '';
        }
        return results[1];
    }

});
