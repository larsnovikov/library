<?php
/**
 * Модуль для отображения раскладки фалового менеджера
 * Подчиненные модули:
 *  Дерево из основного интерфейса
 *  Панель с файлами из основного интерфейса
 *
 * @class: FileBrowserCmsModule
 *
 * @Author: User, $Author: sapozhkov $
 * @version: $Revision: 1490 $
 * @date: $Date: 2012-12-24 17:48:21 +0400 (Пн, 24 дек 2012) $
 *
 */

class FileBrowserCmsModule extends AdminModulePrototype {

    protected function actionInit() {

        // задаем раздел по умолчанию
        $this->addInitParam('defauluSection', skConfig::get('section.library'));

        // подключаем модули
        $this->addChildProcess(new skContext('tree','Tree4FileBrowserAdmModule',ctModule,array()));
        $this->addChildProcess(new skContext('files','Files4FileBrowserAdmModule',ctModule,array()));

        $this->setCmd('init');

    }

    /**
     * Пытается найти id раздела для заданного модуля или создать новый
     */
    protected function actionGetModuleNodeId() {

        // запрашиваем имя модуля
        $sModuleName = $this->get( 'module' );

        // проверяем имя модуля
        if ( !$sModuleName )
            throw new ExecuteException('Имя модуля не задано');

        // проверяем валидность имени модуля
        if ( !preg_match('/^[\w]+(Adm|Design)Module$/',$sModuleName) )
            throw new ExecuteException('Неверное имя модуля');

        // id раздела библиотек
        $iLibSectionId = skConfig::get( 'section.library' );

        // проверяем наличие раздела
        $iSectionId = Tree::getByAlias( $sModuleName, $iLibSectionId );

        // есть - отдать id
        if ( $iSectionId ) {
            $this->setCmd('openNode');
            $this->setData('nodeId',$iSectionId);
        }

        // нет раздела - создать
        else {

            if ( !class_exists($sModuleName) )
                throw new ExecuteException("Модуль [$sModuleName] не найден");

            /** @var \AdminTabModulePrototype $oModule объект модуля */
            $oModule = new $sModuleName( new skContext( 'sub', $sModuleName, ctModule, array()) );

            // проверяем классовую принадлежность
            if ( !($oModule instanceof AdminTabModulePrototype) )
                throw new ExecuteException("Модуль [$sModuleName] должен быть унаследован от интерфейса AdminTabModulePrototype");

            // достаем имя модуля
            $sModuleTitle = $oModule->getTitle();

            $iSectionId = Tree::saveItem(array(
                Tree::PARENT => $iLibSectionId,
                Tree::TITLE => $sModuleTitle,
                Tree::ALIAS => $sModuleName,
                Tree::VISIBLE => Tree::visibleShow,
                Tree::TYPE => Tree::typeDirectory,
                Tree::POSITION => Tree::getNextPosByParentId( $iLibSectionId )
            ));

            // есть - отдать id
            if ( $iSectionId ) {

                /* Обновление кеша политик доступа */
                Policy::incPolicyVersion();
                CurrentAdmin::reloadPolicy();

                $this->setCmd('openNode');
                $this->setData('nodeId',$iSectionId);
            } else {
                throw new ExecuteException("Ошибка создания новой папки для модуля [$sModuleName]");
            }

        }

    }

}
