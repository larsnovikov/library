<?php
/**
 * Модуль для отображения раскладки фалового менеджера для дизайнерского режима
 * Подчиненные модули:
 *  Панель с файлами из основного интерфейса
 */
class FileBrowser4DesignCmsModule extends AdminModulePrototype {

    protected function actionInit() {

        $this->addChildProcess(new skContext('files','Files4DesignFileBrowserAdmModule',ctModule,array()));

    }

}
