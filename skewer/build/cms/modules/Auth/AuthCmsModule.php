<?php

/**
 * @class AuthCmsModule Класс для авторизации в CMS
 *
 * @author Andy Mitrich, $Author: kolesnikov $
 * @version $Revision: 713 $
 * @date 21.12.11 12:27 $
 *
 */

class AuthCmsModule extends AdminModulePrototype {

    protected $viewMode = 'default';

//    public function execute(){
//
//        // Ловим команду, по умолчанию выводим форму
//        $sCmd = $this->getStr('cmd', 'form');
//        $sAuthTicket = $this->getStr('authkey');
//
//        if ( $sAuthTicket ){
//
//            $sUrl = skConfig::get('auth.server');
//
//            $ch = curl_init();
//
//            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
//            curl_setopt($ch, CURLOPT_FAILONERROR, 0);
//            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
//            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 50);
//            curl_setopt($ch, CURLOPT_URL, $sUrl."/auth.php");
//            curl_setopt($ch, CURLOPT_POST, 1);
//            curl_setopt($ch, CURLOPT_POSTFIELDS, array('cmd'=>'remote_login') );
//
//            $sAnswer = curl_exec($ch);
//            curl_close($ch);
//
//            if( WEBROOTPATH==$sAnswer )
//                if ($this->Auth->smsLogin('sys', $sAuthTicket)) return psExit;
//        }
//        else {
//
//            switch ( $sCmd ){
//
//                case 'login':
//
//                    // Ловим в запросе переменные _log и _pass
//                    $sLogin = $this->getStr('_log');
//                    $sPassword = $this->getStr('_pass');
//
//                    /**
//                     * Если пользователь успешно залогинен, возвращаем psExit
//                     * Если нет - выводим форму с уведомлением о неправильно введенных данных
//                     */
//
//                    if( CurrentAdmin::login($sLogin, $sPassword) ){
//
//
//                        return psExit;
//
//                    } else {
//                        $this->setData('error_login', 'Неправильно введены логин или пароль!');
//                        $sCmd = 'form';
//                    }
//
//                break;
//            }
//
//            switch ( $sCmd ){
//
//                case 'form':
//
//                break;
//            }
//
//        }
//        return psComplete;
//    }

    /**
     * Разрешает выполнение модуля без авторизации
     * @return bool
     */
    public function allowExecute() {
        return true;
    }

    /**
     * Выбор интерфейса для отображения в текущей ситуации
     * @return bool
     */
    protected function actionInit(){

        if ( $this->viewMode === 'form' ) {
            $this->showAuthForm();
        } else {
            $this->showMainPanel();
        }

    }

    /**
     * Отдает на выход форму авторизации в собственном слое
     */
    protected function showAuthForm(){

        // основная библиотека вывода
        $this->setJSONHeader('externalLib','AuthLayout');

        // добавить библиотеку
        $this->addLibClass('AuthForm');

        // задать состояние
        $this->setCmd('init');

    }

    /**
     * Отдает панель для админского интерфейса с данными авторизации
     */
    protected function showMainPanel(){

        $aUserData = CurrentAdmin::getUserData();

        // дата последнего захода
        $sDate = isSet($aUserData['lastlogin']) ? $aUserData['lastlogin'] : '';
        if ( $sDate and $sDate>1900  ) {
            $sDate = date('d.m.Y H:i', strtotime($sDate));
        } else {
            $sDate = 'это первое';
        }

        $this->setJSONHeader('init', array( 'renderData' => array(
            'username' => (isSet($aUserData['name']))? $aUserData['name']: '',
            'lastlogin' => $sDate
        )));

    }

    /**
     * Авторизация пользователя
     * @return int
     */
    protected function actionLogin(){

        // получение данных
        $sLogin = $this->getStr('login');
        $sPass = $this->getStr('pass');

        // задать состояние
        $this->setCmd('login');

        // попытка авторизации
        $bLogIn = CurrentAdmin::login($sLogin, $sPass);

        skLogger::addNoticeReport(($bLogIn?'В':'Неудачный в')."ход пользователя в систему администрирования",skLogger::buildDescription(array('ID пользователя'=>CurrentAdmin::getId(), 'Логин'=>$sLogin)),skLogger::logUsers,$this->getModuleName());

        // результат авторизации
        $this->setData('success',$bLogIn);

        // отдать результат работы метода
        return psComplete;

    }

    /**
     * Выход из системы
     * @return int
     */
    protected function actionLogout(){

        // задать состояние
        $this->setCmd('login');

        skLogger::addNoticeReport("Выход пользователя из системы администрирования",skLogger::buildDescription(array('ID пользователя'=>CurrentAdmin::getId(), 'Логин'=>$_SESSION['auth']['admin']['userData']['login'])),skLogger::logUsers,$this->getModuleName());

        // попытка авторизации
        $bLogOut = CurrentAdmin::logout();

        // результат авторизации
        $this->setData('success',$bLogOut);

        // отдать результат работы метода
        return $bLogOut ? psReset : psComplete;

    }

}
