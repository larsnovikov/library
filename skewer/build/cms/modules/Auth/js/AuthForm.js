/**
 * Форма авторизации
 */
Ext.define('Ext.cms.AuthForm', {

    extend: 'Ext.form.Panel',

    title: buildLang.authPanelTitle,
    bodyPadding: 5,
    width: 350,
    topRatio: 0.4,

    // Fields will be arranged vertically, stretched to full width
    layout: 'anchor',
    defaults: {
        labelWidth: 75,
        anchor: '100%'
    },

    // The fields
    defaultType: 'textfield',
    items: [{
        fieldLabel: buildLang.authLoginTitle,
        name: 'login',
        allowBlank: false
    },{
        fieldLabel: buildLang.authPassTitle,
        name: 'pass',
        inputType: 'password',
        allowBlank: true,
        listeners: {
            specialkey: function(field, e){
                if (e.getKey() == e.ENTER) {
                    var form = field.up('form');
                    form.onSubmit();
                }
            }
        }
    }],

    buttons: [{
        text: buildLang.authLoginButton,
        formBind: true, //only enabled once the form is valid
        disabled: true,
        handler: function() {
            this.up('form').onSubmit();
        }
    }],

    listeners: {
        afterrender: function(me){
            // выбрать первое поле
            me.down('textfield').focus();
        }
    },

    initComponent: function(){

        this.callParent();

    },

    execute: function( data, cmd ){

        switch ( cmd ) {

            case 'login':

                // если авторизация не удалась
                if ( !data['success'] ) {
                    sk.error( buildLang.authLoginIncorrect );
                } else {
                    window.location.reload();
                }

                break;

        }

    },

    onSubmit:function(){

        // иниициализация
        var form = this.getForm(),
            values = form.getValues()
        ;

        // собрать посылку
        values.cmd = 'login';
        processManager.setData(form.path,values);

        // отослать
        processManager.postData();

    }

});