/**
 * Панель авторизации в шапке cms
 */
Ext.define('Ext.cms.Auth',{
    extend: 'Ext.container.AbstractContainer',
    border: 0,
    margin: '0 0 3 0',
    padding: 0,

    //шаблонные данные для рендеринга
    renderData: {
        username: '---',
        lastlogin: '---'
    },

    childEls: ['body'],

    // шаблон для вывода
    renderTpl: [
        '<div class="login">',
            '<p class="usrname">{username}</p>',
            '<p class="lastvisit">',
                buildLang.authLastVisit,
                ': {lastlogin}',
            '</p>',
            '<div id="{id}-body" class="logoutbtn"></div>',
        '</div>'
    ],

    getTargetEl: function() {
        return this.body || this.frameBody || this.el;
    },

    initComponent:function(){

        var me = this;

        me.callParent();

        // добавляем кнопку выхода
        me.add( Ext.create('Ext.Button', {
            text: buildLang.authLogoutButton,
            cls: 'logoutbutton',
            handler: function() {
                me.logOut();
            }
        }) );

    },

    /**
     * Выход из админского интерфейса
     */
    logOut: function(){

        var me = this;

        processManager.setData(me.path, {
            cmd: 'logout'
        });

        processManager.postData();

    }

});
