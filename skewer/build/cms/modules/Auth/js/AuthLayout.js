Ext.require([
    'Ext.sk.layout.Center'
]);

Ext.define('Ext.cms.AuthLayout',{
    extend: 'Ext.Viewport',
    layout: 'sk.center',
    margin: 0,
    padding: 5,
    form: null,

    initComponent: function(){

        var me = this;

        me.form = Ext.create('Ext.cms.AuthForm',{
            region: 'center',
            path: me.path
        });

        me.items = [me.form];

        me.callParent();

    },

    execute: function(data, cmd){

        var me = this;

        me.form.execute( data, cmd );

    }

});
