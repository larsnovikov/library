Ext.Loader.setConfig({enabled: true});

// конфигурация слоя
var layerName = 'cms';
var extPrefix = 'Ext.'+layerName+'.';
var rootPath = '/skewer_build/'+layerName+'/modules/Frame/js';

// инициализация базовых инструментов
var sk;
Ext.Loader.setPath('Ext.sk', '/skewer_build/libs/pmExtJS/js');

// установка путей для слоя
Ext.Loader.setPath('Ext.'+layerName, rootPath);

//Ext.Loader.setPath('Ext.ux', '/skewer_build/libs/ExtJS/js/ux');
//Ext.require([ 'Ext.ux.CheckColumn' ]);

// конфигурация и языки
buildConfig = Ext.create(extPrefix+'Config');
buildLang = Ext.create(extPrefix+'LangRu');

Ext.onReady(function() {

    // инициализация основного набора параменных
    sk = Ext.create('Ext.sk.Init');
    processManager.addProcess('init',sk);

    // инициализация CKEditor'а
    Ext.Loader.require('/skewer_build/libs/CKEditor/ckInit');

    // инициализация браузера файлов и поля типа "файл"
    Ext.Loader.require('Ext.sk.field.FileSelector');
    processManager.addProcess('fileSelector',Ext.create('Ext.sk.FileSelector'));

});

// удержание сессии в живых
setInterval(function(){
    Ext.Ajax.request({
        url: '/skewer_build/cms/modules/Frame/keepalive.php',
        method: 'GET',
        params: {ping: 1},
        success: function (result, request) {}
    });

}, 420000); // каждые 7 минут
