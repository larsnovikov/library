/**
 * Словарь для русской языковой версии
 */
Ext.define('Ext.cms.LangRu',{
    /**
     * Общие
     */
    add: 'Добавить',
    del: 'Удалить',
    edit: 'Редактировать',
    err: 'Ошибка',
    log: '>>',
    upd: 'Редактировать',
    save: 'Сохранить',
    cancel: 'Отмена',
    confirmHeader: 'Подтверждение выролнения',
    ajax_error: 'Ошибка отправки запроса (',
    noModuleName: 'Отсутствует имя класса для создаваемого модуля',
    delRowHeader: 'Удаление записи',
    delRow: 'Удалить ',
    delRowNoName: 'запись',
    delCntItems: ' шт',
    allowDoHeader: 'Подтверждение на выполнение!',
    allowDo: 'Вы действительно хотите выполнить это действие?',
    clearLog: 'Вы действительно хотите очистить логи?',
    toolsHeader: 'Панель управления',

    /**
     * Авторизация
     */
    authPanelTitle: 'Авторизация',
    authLoginTitle: 'Имя',
    authPassTitle: 'Пароль',
    authLoginButton: 'Войти',
    authLogoutButton: 'Выйти',
    authLoginIncorrect: 'Неправильное имя пользователя или пароль',
    authLastVisit: 'Последнее посещение',

    /**
     * Дерево разделов
     */
    treePanelHeader: 'Дерево разделов',
    treeErrorNoParent: 'При отображении раздела не найден родительский',
    treeErrorParentNotSelected: 'Не выбран раздел для добавления',
    treeErrorOnDelete: 'Ошибка при удалении',
    treeErrorSetVis: 'Ошибка при установке видимости',
    treeFormHeaderUpd: 'Редактирование раздела',
    treeFormHeaderAdd: 'Добавление раздела',
    treeFormTitleTitle: 'Название раздела',
    treeFormTitleAlias: 'Псевдоним',
    treeFormTitleParent: 'Родительский раздел',
    treeFormTitleTemplate: 'Тип раздела',
    treeFormTitleLink: 'Ссылка',
    treeTitleVisible: 'Видимость',
    treeTitleVisible0: 'Скрыт',
    treeTitleVisible1: 'Видимый',
    treeTitleVisible2: 'Виден, исключен из пути',
    treeTitleType: 'Тип',
    treeNewSection: 'Новый раздел',
    treeDelRowHeader: 'Удаление раздела',
    treeDelRow: 'Удалить раздел "',


    /**
     * Набор объектов
     */
    objPanelHeader: 'Доступные объекты',

    /**
     * Редактор
     */
    editorCloseConfirmHeader: 'Подтверждение продолжения без сохранения',
    editorCloseConfirm: 'Данные не сохранены. Продолжить?',

    /**
     * Вкладка редактора контентных полей
     */
    contTabPanelTitle: 'Редактор',
    contTabFormTitle: 'Редактор элементов страницы',
    contTabErrNoFieldDef: 'В данном разделе нет полей для редактирования',
    contTabErrNoSectionId: 'Для страницы редактора нет id раздела в посылке',
    contTabErrUnknownType: 'Неизвестный тип поля `',
    contTabErrInvalidForm: 'Некоторые поля имеют неверый формат. Данные НЕ сохранены.',
    contTabMesDataSaved: 'Данные сохранены',
    contTabMesNoDataToSave: 'Данные НE сохранены',
    contTabString: 'Строка',
    contTabText: 'Текст',
    contTabHtml: 'Упрощенный HTML редактор',
    contTabWyswyg: 'HTML редактор',
    contTabCheck: 'Галочка',
    contTabFile: 'Файл',
    contTabImage: 'Изображение',
    contTabShow: 'Показать',
    contTabSelect: 'Выпадающий список',
    contTabNumber: 'Число',
    contTabCombo: 'Выпадающий список',
    contTabDate: 'Дата',
    contTabTime: 'Время',
    contTabInherit: 'Наследуемое',
    contTabSystem: 'Системное',
    contTabLocalField: ' (не наследуемое)',

    /**
     * Набор вкладок
     */
    tabsSectionTitlePrefix: 'Раздел: ',

    /**
     * Файловый менеджер
     */
    fileBrowserPanelTitle: 'Менеджер файлов',
    fileBrowserNoSelection: 'Файл не выбран',

    /**
     * Параметры
     */
    paramTitleName: 'Имя',
    paramTitleGroup: 'Группа',
    paramTitleParent: 'Раздел',
    paramTitleTitle: 'Название',
    paramTitleAccessLevel: 'Тип',
    paramTitleValue: 'Значение',
    paramTitleShowVal: 'Текстовое поле',
    paramTabHeader: 'Параметры',
    paramPanelHeader: 'Параметры раздела',
    paramFormHeaderUpd: 'Редактирование параметра',
    paramFormHeaderAdd: 'Добавление параметра',
    paramFormSaveUpd: 'Сохранить',
    paramFormClose: 'Закрыть',
    paramDelRowHeader: 'Удаление записи',
    paramDelRow: 'Удалить запись "',
    paramDelCntItems: ' шт',
    paramMesDataSaved: 'Данные сохранены',
    paramMesDataNotSaved: 'Данные НE сохранены',
    paramMesDataDeleted: 'Данные удалены',
    paramMesDataNotDeleted: 'Данные НE удалены',
    paramErrNoSectionId: 'Для страницы параметров не задано значение `sectionId`',
    paramAddForSection: 'Исправить для раздела',
    paramCopyForSection: 'Дублировать для раздела',

    /**
     * Политики доступа
     */
    polisyAllow: 'Доступно',
    polisyNone: 'Наследуется',
    polisyDeny: 'Не доступно',
    polisyMain: 'Стартовый раздел',

    /**
     * Нижняя панель
     */
    logPanelHeader: 'Информация'

});
