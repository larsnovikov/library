<?php

/**
 * @class FrameCmsApi
 * @extends skModule
 * @project Skewer
 * @package kernel
 *
 * @author Andy Mitrich, $Author: sapozhkov $
 * @version $Revision: 6 $
 * @date 16.03.12 10:44 $
 *
 */

class FrameCmsApi {

    public static function isValidBrowser(){

        $oBrowser = new skBrowser();
        $sBrowser = $oBrowser->getBrowser();

        $iValidVersion = skConfig::get("browser.$sBrowser");

        if ( !$iValidVersion || $oBrowser->getVersion()<$iValidVersion ) return false;

        return true;
    }

}//class