<?php
/**
 * 
 * @class FrameCmsModule
 *
 * @author ArmiT, $Author: sapozhkov $
 * @version $Revision: 6 $
 * @date $Date: 2012-05-18 10:52:53 +0400 (Пт., 18 мая 2012) $
 *
 */ 
class FrameCmsModule extends skModule {
    public function init(){

        $this->setParser(parserTwig);
        $this->setTemplate('index.twig');
        
    }// func

    public function execute() {

        if ( !FrameCmsApi::isValidBrowser() ){

            $this->setTemplate('not_valid.twig');
        }

        $oProcessSession = new skProcessSession();
        $sTicket = $oProcessSession->createSession();

        $this->setData('sessionId',$sTicket);
        $this->setData('layoutMode',$this->getStr('mode','cms'));
        $this->setData('moduleDir',$this->getModuleWebDir());

        return psComplete;
    }// func

    public function shutdown(){

    }// func
}// class
