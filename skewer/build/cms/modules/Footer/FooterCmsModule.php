<?php
/**
 * 
 * @class FooterCmsModule
 *
 * @author sapozhkov, $Author: sapozhkov $
 * @version $Revision: 337 $
 * @date $Date: 2012-06-20 21:00:40 +0400 (Ср., 20 июня 2012) $
 *
 */ 
class FooterCmsModule extends AdminModulePrototype {
    public function execute() {

        // отдать перекрывающий инициализационный параметр для модуля
        $this->setJSONHeader('init', array(
            'html' => $this->renderTemplate( 'view.twig' )
        ) );

        return psComplete;

    }// func

    public function shutdown(){

    }// func
}// class
