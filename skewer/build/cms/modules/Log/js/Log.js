/**
 * Нижняя панель для отображения информации
 */
Ext.create('Ext.data.Store', {
    storeId:'InterfaceLogStorage',
    fields: ['text'],
    data: []
});

Ext.define( 'Ext.cms.Log', {
    extend: 'Ext.panel.Panel',
    region: 'east',
    split: true,
    width: 700,
    autoScroll: true,
    collapsible: true,
    collapsed: true,
    title: buildLang.logPanelHeader,
    margin: '0 0 0 5',

    rowsCnt: 0,

    items: [{
        xtype: 'grid',
        store: 'InterfaceLogStorage',
        border: 0,
        hideHeaders: true,
        columns: [
            {dataIndex: 'text', flex: 1}
        ]
    }],

    tools: [{
        type:'refresh',
        tooltip: 'Очистить',
        handler: function(event, toolEl, panel){
            panel.up('panel').removeAllRows();
        }
    }],

    listeners: {
        expand: function(){
            this.doLayout();
        }
    },

    initComponent: function(){

        this.callParent();

        // навесить обработчики
        processManager.addEventListener('error',this.path, 'addRowError');
        processManager.addEventListener('log',this.path, 'addRowLog');

    },

    removeAllRows: function(){
        this.rowsCnt = 0;
        this.down('grid').getStore().removeAll();
    },

    addRow: function( text, title ){
        this.rowsCnt++;
        this.down('grid').getStore().add({
            text: '<strong>'+title+'</strong> '+text
        });
        this.doLayout();
    },

    addRowError: function( text ){
        this.addRow(text,buildLang.err+':');
    },

    addRowLog: function( text ){
        this.addRow(text,buildLang.log);
        sk.message('Ответ от сервера с отладочной информацией! (записей: '+this.rowsCnt+')','','msg-error');
    }

});
