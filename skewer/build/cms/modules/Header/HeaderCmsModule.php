<?php
/**
 * 
 * @class HeaderCmsModule
 *
 * @author ArmiT, $Author: sapozhkov $
 * @version $Revision: 337 $
 * @date $Date: 2012-06-20 21:00:40 +0400 (Ср., 20 июня 2012) $
 *
 */ 
class HeaderCmsModule extends AdminModulePrototype {

    protected function actionInit() {

        // добавить панель авторизации
        $this->addChildProcess(new skContext('auth','AuthCmsModule',ctModule,array()));

        return psComplete;

    }// func

    public function shutdown(){

    }// func
}// class
