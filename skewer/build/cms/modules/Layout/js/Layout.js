/**
 * Корневой слой вывода интерфейса
 */

Ext.define('Ext.cms.Layout',{
    extend: 'Ext.Viewport',
    id: 'view-port',
    layout: 'border',
    margin: 0,
    padding: 5,
    items: [{
        region: 'center',
        html: ''
    }],

    // после инициализации объекта
    afterRenderInterface:function(){

        if ( pageHistory )
            pageHistory.afterRender();

    }

});
