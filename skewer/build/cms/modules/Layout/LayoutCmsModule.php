<?php
/**
 * 
 * @class LayoutCmsModule
 *
 * @author ArmiT, $Author: sapozhkov $
 * @version $Revision: 1346 $
 * @date $Date: 2012-12-03 11:31:54 +0400 (Пн., 03 дек. 2012) $
 *
 */ 
class LayoutCmsModule extends AdminModulePrototype {

    const labelHeader = 'header';
    const labelLeft = 'left';
    const labelTabs = 'tabs';
    const labelLog = 'log';
    const labelFooter = 'footer';

    public function allowExecute() {
        return true;
    }

    public function execute() {

        //$this->

        //skProcessor::addHookBeforeEvent('EditorAdmModule.SaveSectionData', 'EditorAdmModule', 'eventTest');

        $this->addChildProcess(new skContext(self::labelHeader,'HeaderCmsModule',ctModule,array()));
        $this->addChildProcess(new skContext(self::labelLeft,'LeftPanelCmsModule',ctModule,array()));
        $this->addChildProcess(new skContext(self::labelTabs,'TabsCmsModule',ctModule,array()));
        if(CurrentAdmin::isSystemMode())
            $this->addChildProcess(new skContext(self::labelLog,'LogCmsModule',ctModule,array()));
        $this->addChildProcess(new skContext(self::labelFooter,'FooterCmsModule',ctModule,array()));

        return psComplete;

    }// func



}// class
