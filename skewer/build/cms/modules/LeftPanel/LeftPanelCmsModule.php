<?php
/**
 * Левая панель для основного админского интерфейса
 *
 * @class: LeftPanelCmsModule
 *
 * @Author: sapozhkov, $Author: acat $
 * @version: $Revision: 1475 $
 * @date: $Date: 2012-12-21 15:01:54 +0400 (Пт., 21 дек. 2012) $
 */

class LeftPanelCmsModule extends AdminModulePrototype {

    /**
     * Отдает конфигурацию модулей
     * @return string[]
     */
    protected function getModuleSet() {

        $aOut = array();

        $aOut['section'] = 'Tree4MainAdmModule';
        if(CurrentAdmin::isSystemMode())
            $aOut['tpl'] = 'Tree4TplAdmModule';
        $aOut['lib'] = 'Tree4LibAdmModule';
        if ( CurrentAdmin::canDo('PolicyToolModule','useControlPanel') )
            $aOut['tools'] = 'ListToolModule';

        return $aOut;

    }

    /**
     * Отдает объект модуля, который может работать с набором вкладок
     * @static
     * @param $sLabel
     * @throws Exception
     * @return null|AdminTabListModulePrototype
     */
    public function getModule( $sLabel ) {

        // проверить наличие модуля в конфигурации
        $aModuleSet = $this->getModuleSet();
        if ( !isset($aModuleSet[$sLabel]) )
            throw new Exception("Запрашиваемый модуль `$sLabel` набора вкладок не найден.");

        /** @var \skProcess $oProcess  */
        $oProcess = $this->getChildProcess( $sLabel );
        if ( !is_object($oProcess) )
            throw new Exception("Запрашиваемый модуль `$sLabel` набора вкладок не инициализирован.");

        $oListModule = $oProcess->getModule();

        // проверить его принадлежность суперклассу
        if ( !is_subclass_of($oListModule,'AdminTabListModulePrototype') )
            throw new Exception('Модуль набора вкладок не принадлежит требуемому суперклассу.');

        return $oListModule;

    }

    /**
     * Инициализация набора элементов
     * @return int
     */
    public function execute() {

        // добавление инициализированных модулей
        foreach ( $this->getModuleSet() as $sAlias => $sModuleName )
            $this->addSubPanel( $sAlias, $sModuleName );

        return psComplete;

    }// func

    /**
     * Добавление подчиненного элемента
     * @param $sAlias - псевдоним для составления пути
     * @param $sModuleName - имя модуля
     */
    protected function addSubPanel( $sAlias, $sModuleName ){

        $aParams = array();

        $this->addChildProcess(new skContext($sAlias, $sModuleName ,ctModule,$aParams));
    }

}
