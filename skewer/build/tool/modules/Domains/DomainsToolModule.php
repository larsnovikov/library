<?php
/**
 * @class DomainsToolModule
 * @extends AdminModulePrototype
 * @project Skewer
 * @package modules
 *
 * @author kolesnikov, $Author: $
 * @version $Revision:  $
 * @date $Date: $
 *
 */
class DomainsToolModule extends AdminToolTabModulePrototype {

    /**
     * @var string - Имя модуля
     */
    protected $sTabName = 'Домены';

    // id текущего раздела
    protected $iSectionId = 0;

    /**
     * Первичный обработчик
     * @throws ModuleAdminErrorException
     * @return bool
     */
    protected function preExecute(){
        CurrentAdmin::testControlPanelAccess();
    }

    /**
     * Инициализация
     */
    protected function actionInit() {

        // вывод списка
        $this->actionList();
    }

    /**
     * Вывод списка
     * @param string $sMsg
     * @return int
     */
    protected function actionList($sMsg = ''){
        // объект для построения списка
        $oList = new ExtList();

        try {

            if($sMsg) $this->addError($sMsg);

            /* Модель данных */
            $aModel = array("domain_id"=> array("name" => "domain_id",
                    "type" => "i",
                    "view" => "hide",
                    "jsView" => "hide",
                    "title" => "ID записи в таблице доменов",
                    "default" => "",
                    "hidden" => true,
                ),
                "domain"=> array("name" => "domain",
                    "type" => "s",
                    "view" => "select",
                    "title" => "Название домена",
                    "default" => "",
                ),
                "prim"=> array("name" => "prim",
                    "type" => "s",
                    "view" => "select",
                    "title" => "Основной",
                    "default" => "",
                ),
            );

            $oList->setFields( $aModel );

            $aItems = DomainsToolApi::getAllDomains();

            $oList->setValues( $aItems['items'] );


               /*
            $oList->addDockedItem(array(
                'text' => 'Добавить домен',
                'iconCls' => 'icon-add',
                'state' => 'init',
                'action' => 'addDomainForm',
            ));
                 */
        } catch(Exception $e) {
            $this->addError($e->getMessage());
        }

        // вывод данных в интерфейс
        $this->setExtInterface( $oList );

        return psComplete;
    }




}
