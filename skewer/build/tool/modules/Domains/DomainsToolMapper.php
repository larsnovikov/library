<?php
/**
 * Класс для работы с доменами для площадки
 *
 * @class: DomainsToolMapper
 *
 * @author kolesnikov
 * @version 0.1
 *
 */
class DomainsToolMapper extends skMapperPrototype {

    /**
     * @var string Имя текущей таблицы, с которой работает маппер
     */
    protected static $sCurrentTable = 'domains';

    /**
     * @var array Конфигурация полей таблицы
     */
    protected static $aParametersList = array(
        'd_id' => 'i:int:Идентификатор',
        'domain_id' => 'i:int:Глобальный идентификатор домена',
        'domain' => 's:string:Имя домена',
        'prim' => 'i:int:Основной домен',
    );

    /**
     * Имя ключевого поля
     * @var string
     */
    protected static $sKeyFieldName = 'd_id';


    public static function delByName($sDomain){

        $sQuery = "DELETE FROM `[table:q]`
                    WHERE domain=[domain:s]";

        $aData = array(
            'table' => self::$sCurrentTable,
            'domain' => $sDomain,
        );

        global $odb;

        $rResult = $odb->query($sQuery,$aData);

        return true;
    }

    /**
     * @static
     * @return bool|mysqli_result
     */
    public static function clearLog(){

        $sQuery = "DELETE FROM `".self::getCurrentTable()."`;";

        global $odb;

        return $odb->query($sQuery);

    }


}
