<?php
/**
 * API работы с резевным копированием
 *
 * @class: DomainsToolApi
 *
 * @author kolesnikov
 * @version $Revision: $
 * @date $Date: $
 *
 */
class DomainsToolApi {


    public static function getAllDomains(){

        $aOut = DomainsToolMapper::getItems(array());

        return $aOut;

    }

    public static function getMainDomain(){

        $aFilter = array('select_fields' => array('domain'),
                        'where_condition' => array( 'prim' => array (
                                                                'sign' => '=',
                                                                'value' => '1'
                                                               )));

        $mDomains = DomainsToolMapper::getItems($aFilter);

        $sDomain = $_SERVER['HTTP_HOST'];

        if(  isset($mDomains['items'][0]['domain']) )
            $sDomain = $mDomains['items'][0]['domain'];

        return $sDomain;
    }

    /**
     * получение массива редиректов на основной домен с вторичных
     * @static
     * @return array
     */
    public static function getRedirectItems(){

        $aOut = array();

        $aItems = self::getAllDomains();

        $aPrimItem = false;

        if($aItems['count']){

            foreach($aItems['items'] as $aItem){
                if($aItem['prim'])
                    $aPrimItem = $aItem;
            }

            if($aPrimItem){

                foreach($aItems['items'] as $aItem){
                    if($aItem['d_id'] != $aPrimItem['d_id'])
                        $aOut[] = array('old_url'=>$aItem['domain'], 'new_url'=>$aPrimItem['domain']);
                }


            }

        }


        return $aOut;
    }

    public static function syncDomains($aDomains){

        if(!$aDomains || !is_array($aDomains) || !count($aDomains)){

            $aRealDomain = self::getAllDomains();
            $aRealDomain = isset($aRealDomain['items']) ? $aRealDomain['items'] : array();

            foreach($aRealDomain as $aCurDomain)
                DomainsToolMapper::delItem($aCurDomain);

            SEOTemplatesToolService::setNewDomainToSiteMap();

            SEOTemplatesToolService::updateRobotsTxt(false);
            Redirect301ToolApi::makeHtaccessFile();

            return true;
        }


        $aRealDomain = self::getAllDomains();
        $aRealDomain = isset($aRealDomain['items']) ? $aRealDomain['items'] : array();

        $aUpdDomains = array();
        $aDelDomains = array();

        foreach($aDomains as $aCurDomain){
            $flag = false;
            foreach($aRealDomain as $aCurRealDomain){
                if($aCurDomain['domain_id'] == $aCurRealDomain['domain_id']){  // upd
                    $item = $aCurRealDomain;
                    $item['domain'] = $aCurDomain['domain'];
                    $item['prim'] = $aCurDomain['prim'];
                    $aUpdDomains[] = $item;
                    $flag = true;
                }
            }
            if(!$flag){ // new
                $item = array();
                $item['domain'] = $aCurDomain['domain'];
                $item['prim'] = $aCurDomain['prim'];
                $item['domain_id'] = $aCurDomain['domain_id'];
                $aUpdDomains[] = $item;

            }
        }

        foreach($aRealDomain as $aCurRealDomain){
            $flag = false;
            foreach($aDomains as $aCurDomain){
                if($aCurDomain['domain_id'] == $aCurRealDomain['domain_id'])
                    $flag = true;
            }
            if(!$flag || !count($aDomains))
                $aDelDomains[] = $aCurRealDomain['d_id'];
        }



        foreach($aUpdDomains as $aCurDomain){

            DomainsToolMapper::saveItem($aCurDomain);

        }


        foreach($aDelDomains as $aCurDomain){

            DomainsToolMapper::delItem($aCurDomain);

        }

        SEOTemplatesToolService::setNewDomainToSiteMap();

        Redirect301ToolApi::makeHtaccessFile();

        SEOTemplatesToolService::updateRobotsTxt(self::getMainDomain());

        return true;
    }


    public static function addDomain($sDomain, $iPrim=0, $iDomainId = 0){

        $aParam = array('domain_id'=>$iDomainId,'domain'=>$sDomain,'prim'=>$iPrim);

        DomainsToolMapper::saveItem($aParam);

        return true;
    }


    public static function delDomain($sDomain){

        DomainsToolMapper::delByName($sDomain);

        return true;
    }


}
