<?php
/**
 * API работы с seo шаблонами
 *
 * @class: SEOTemplateAdmModule
 *
 * @author kolesnikov
 * @version $Revision: 1316 $
 * @date $Date: 2012-11-27 18:31:02 +0400 (Вт., 27 нояб. 2012) $
 *
 */
class SEOTemplatesToolApi {


    /**
     * @static
     * @return array
     */
    public static function getListFields(){

        return array(
          //  'id',
            'sid',
            'name',
            'title',
            'description',
            'keywords',
            'info'
        );
    }

    /**
     * @static
     * @return array
     */
    public static function getFullListFields(){

        return array(
            'id',
            'sid',
            'name',
            'title',
            'description',
            'keywords',
            'info'
        );
    }

    /**
     * @static
     * @param $aFields
     * @return array
     */
    public static function getListModel($aFields){

        return SEOTemplatesToolMapper::getFullParamDefList($aFields);
    }


    /**
     * @static
     * @return array
     */
    public static function getListItems(){

        $aFilter = array();

        return SEOTemplatesToolMapper::getItems($aFilter);
    }

    /**
     * Получить шаблон c id $id
     * @static
     * @param $id
     * @return array
     */
    public static function getItemById($id){

        return SEOTemplatesToolMapper::getItem($id);

    }

    /**
     * Получить шаблон c именем $sName
     * @static
     * @param string $sName
     * @return array
     */
    public static function getItemByName($sName = ''){

        $aFilter['where_condition']['sid'] = array(
            'sign' => '=',
            'value' => $sName
        );

        return SEOTemplatesToolMapper::getItem($aFilter);

    }

    public static function parseSEOTpl($aTpl = '', $aVars = array()){

        if($aVars && count($aVars))
            foreach($aVars as $key=>$val)
                $aTpl = str_replace('['.$key.']',$val,$aTpl);

        return $aTpl;
    }



}
