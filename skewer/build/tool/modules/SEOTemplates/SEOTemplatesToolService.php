<?php
/**
 * Сервис для работы с СЕО компонентами
 *
 * @class SEOTemplatesToolService
 *
 * @author kolesnikov, $Author$
 * @version $Revision$
 * @date $Date$
 * @package kernel
 */
class SEOTemplatesToolService extends ServicePrototype {


    /**
     * постановка задачи на обновление sitemap.xml
     * @static
     * @return bool
     */
    public static function updateSiteMap(){

        $iTaskId = Tasks::addTask('обновление sitemap.xml', 3,
            Tasks::$taskWeightNormal, 'SEOTemplatesToolService',
            'makeSiteMap');
        /*
        $oTasks = new Tasks();
        $oTasks->getTaskStatus($iTaskId);
        */
        return (bool)$iTaskId;

    }

    /**
     * обновление файла sitemap.xml
     * @static
     * @return bool
     */
    public static function makeSiteMap(){

        $sTemplateDir = BUILDPATH.'tool/modules/SEOTemplates/templates/';

        // набор предустановленных в конфиге путей для парсинга
        $aConfigPaths = skConfig::get('parser.default.paths');
        if ( !is_array($aConfigPaths) ) $aConfigPaths = array();

        $aPaths = $sTemplateDir;

        /** @noinspection PhpUndefinedMethodInspection */
        if($sViewMode = skProcessor::getEnvParam('_viewMode'))
            if(file_exists($sTemplateDir.$sViewMode.DIRECTORY_SEPARATOR))
                $aPaths = $sTemplateDir.$sViewMode.DIRECTORY_SEPARATOR;

        $aTplPaths = array_merge($aConfigPaths,array($aPaths));
        $aTplPaths = array_diff($aTplPaths, array(''));

        /** @fixme Добавил путь до папки commons/templates */
        skTwig::setPath( $aTplPaths );

        // -- get data

        $aItems = SEOTemplatesToolMapper::getPageList();

        // -- parse

        skTwig::assign('items', $aItems);

        $out = skTwig::render('sitemap.twig');

        // -- save - rewrite file

        $filename = ROOTPATH."sitemap.xml";

        /** @todo проверка на существование и выставить права */
        if (!$handle = fopen($filename, 'w+'))   return false;

        if (fwrite($handle, $out) === FALSE)    return false;

        fclose($handle);

        return Tasks::$taskStatusComplete;
    }


    public static function setNewDomainToSiteMap(){

        Search::resetIndexTable();

        self::updateSiteMap();

        return true;
    }


    public static function updateRobotsTxt($sDomain){

        //$sTemplateDir = 'skewer/build/common/templates/';

        // набор предустановленных в конфиге путей для парсинга
        $aConfigPaths = skConfig::get('parser.default.paths');
        if ( !is_array($aConfigPaths) ) $aConfigPaths = array();

        /** @fixme Добавил путь до папки commons/templates */
        skTwig::setPath( $aConfigPaths ); //$aTplPaths

        // -- get data

        $bExistDomain = false;
        if( $sDomain ) $bExistDomain = true;
        if( !SysVar::isProductionServer() ) $bExistDomain = false;

        skTwig::assign('domain_exist', $bExistDomain);
        skTwig::assign('site_url', $sDomain);

        $out = skTwig::render('robots.twig');

        // -- save - rewrite file

        $filename = ROOTPATH."robots.txt";

        if (!$handle = fopen($filename, 'w+'))   return false;

        if (fwrite($handle, $out) === FALSE)    return false;

        fclose($handle);

        return true;

    }

}
