<?php
/**
 * User: kolesnikiv
 * Date: 10.05.12
 * Time: 16:48
 */
$aConfig['name']     = 'SEOTemplatesTool';
$aConfig['title']    = 'SEO шаблоны';
$aConfig['version']  = '1.000a';
$aConfig['description']  = '';
$aConfig['revision'] = '0001';
$aConfig['layer']     = 'tool';


/* Module hooks */
//$aConfig['hooks']['before']['goTest'] = array();
$aConfig['hooks']['after']['NewsAdmModule:save']  = array(
    'class' => 'SEOTemplatesToolService',
    'method' => 'updateSiteMap',
);

$aConfig['hooks']['after']['NewsAdmModule:delete']  = array(
    'class' => 'SEOTemplatesToolService',
    'method' => 'updateSiteMap',
);

$aConfig['hooks']['after']['ArticlesAdmModule:save']  = array(
    'class' => 'SEOTemplatesToolService',
    'method' => 'updateSiteMap',
);

$aConfig['hooks']['after']['ArticlesAdmModule:delete']  = array(
    'class' => 'SEOTemplatesToolService',
    'method' => 'updateSiteMap',
);

$aConfig['hooks']['after']['EditorAdmModule:save']  = array(
    'class' => 'SEOTemplatesToolService',
    'method' => 'updateSiteMap',
);

$aConfig['hooks']['after']['EventsAdmModule:save']  = array(
    'class' => 'SEOTemplatesToolService',
    'method' => 'updateSiteMap',
);

$aConfig['hooks']['after']['EventsAdmModule:delete']  = array(
    'class' => 'SEOTemplatesToolService',
    'method' => 'updateSiteMap',
);

$aConfig['hooks']['after']['DocumentsAdmModule:save']  = array(
    'class' => 'SEOTemplatesToolService',
    'method' => 'updateSiteMap',
);

$aConfig['hooks']['after']['DocumentsAdmModule:delete']  = array(
    'class' => 'SEOTemplatesToolService',
    'method' => 'updateSiteMap',
);


return $aConfig;