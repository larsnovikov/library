<?php
/**
 * Класс для работы с таблицами рассылки
 *
 * @class: SEOTemplatesToolMapper
 *
 * @author kolesnikov
 * @version 0.1
 *
 */
class SEOTemplatesToolMapper extends skMapperPrototype {

    /**
     * @var string Имя текущей таблицы, с которой работает маппер
     */
    protected static $sCurrentTable = 'seo_templates';

    /**
     * @var array Конфигурация полей таблицы
     */
    protected static $aParametersList = array(
        'id' => 'i:int:Идентификатор',
        'sid' => 's:string:Идентификатор',
        'name' => 's:string:Имя шаблона',
        'title' => 's:string:Заголовок',
        'description' => 's:string:Описание',
        'keywords' => 's:string:Ключевые слова',
        'info' => 's:show:Описание переменных'
    );

    /**
     * @var array Массив соответствий
     */
    protected static $aSearchToSEO = array(
        'News'=>'news',
        'Events'=>'events',

    );


    /**
     * Имя ключевого поля
     * @var string
     */
    protected static $sKeyFieldName = 'id';


    public static function getPageList(){

        $sQuery = "SELECT * FROM `[table:q]`";/*
                    LEFT JOIN `[sub_table:q]` ON `[sub_table:q]`.row_id=`[table:q]`.object_id AND
                                                 `[sub_table:q]`.group=`[table:q]`.class_name";  // WHERE sv_name=[name:s]
                                                */
        $aData = array(
            'table' => 'search_index',
            'sub_table' => 'seo_data',
        );

        global $odb;
        $rResult = $odb->query($sQuery,$aData);

        if ( !$rResult ) return false;

        $aPages = array();

        while( $aRow = $rResult->fetch_assoc() ){

            $aRow['frequency'] = 'always';
            $aRow['priority'] = 1;
            $aRow['url'] = 'http://'.$aRow['href'];

            $flag = true;

            if( isset(self::$aSearchToSEO[$aRow['class_name']]) ){
                $aData['item'] = $aRow['object_id'];
                $aData['group'] = self::$aSearchToSEO[$aRow['class_name']];
                $sQuery = "SELECT * FROM `[sub_table:q]` WHERE `row_id`=[item:i] AND `group`=[group:s]";
                $rAddResult = $odb->query($sQuery,$aData);
                if ( $rAddResult )
                if( $aAddRow = $rAddResult->fetch_assoc() ){
                    if($aAddRow['frequency']) $aRow['frequency'] = $aAddRow['frequency'];
                    if($aAddRow['priority']) $aRow['priority'] = $aAddRow['priority'];
                    $flag = false;
                }
            }

            if($flag){
                $aData['item'] = $aRow['section_id'];
                $aData['group'] = 'section';
                $sQuery = "SELECT * FROM `[sub_table:q]` WHERE `row_id`=[item:i] AND `group`=[group:s]";
                $rAddResult = $odb->query($sQuery,$aData);
                if ( $rAddResult )
                if( $aAddRow = $rAddResult->fetch_assoc() ){
                    if($aAddRow['frequency']) $aRow['frequency'] = $aAddRow['frequency'];
                    if($aAddRow['priority']) $aRow['priority'] = $aAddRow['priority'];
                    $flag = false;
                }
            }

            $aPages[] = $aRow;

        }

        return $aPages;
    }
}
