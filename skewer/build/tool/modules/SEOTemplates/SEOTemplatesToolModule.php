<?php
/**
 * @class SEOTemplatesToolModule
 * @extends AdminModulePrototype
 * @project Skewer
 * @package modules
 *
 * @author kolesnikov, $Author: acat $
 * @version $Revision: 1672 $
 * @date $Date: 2013-02-06 15:59:05 +0400 (Ср, 06 фев 2013) $
 *
 */
class SEOTemplatesToolModule extends AdminToolTabModulePrototype {

    protected $sTabName = 'SEO шаблоны';

    protected function preExecute() {
        CurrentAdmin::testControlPanelAccess();
    }

    public function actionInit(){

        // вывод списка
        $this->actionList();

    }


    public function actionList(){

        // объект для построения списка
        $oList = new ExtList();

        $aModel = SEOTemplatesToolApi::getListModel(array('sid','name'));
        $aModel['sid']['listColumns'] = array('width' => 150);
        $aModel['name']['listColumns'] = array('flex' => 1);

        // брасываем название панели
        $this->setPanelName('',true);

        // задать модель данных для вывода
        $oList->setFields( $aModel );

        // добавление набора данных
        $aItems = SEOTemplatesToolApi::getListItems();

        $oList->setValues( $aItems['items'] );
        $oList->setTotal( $aItems['count'] );

        $oList->addRowBtnUpdate('editForm');


        if(CurrentAdmin::isSystemMode()){

            $oList->addRowBtnDelete();

            // кнопка добавления
            $oList->addDockedItem(array(
                'text' => 'Добавить',
                'iconCls' => 'icon-add',
                'state' => 'init',
                'action' => 'addForm',
            ));

        }


        // вывод данных в интерфейс
        $this->setExtInterface( $oList );

    }

    public function actionAddForm(){

        $oForm = new ExtForm();

        $this->setPanelName('Добавление SEO шаблона',true);

        /* установить набор элементов формы */
        $aItems = SEOTemplatesToolMapper::getFullParamDefList(SEOTemplatesToolApi::getListFields());

        $oForm->setFields($aItems);  //setItems
        $oForm->setValues(array());

        $oForm->addBntSave( 'add' );
//        $oForm->addDockedItem(array(
//            'text' => 'Сохранить',
//            'iconCls' => 'icon-save',
//            'state' => 'allow_do',
//            'action' => 'add',
//            'actionText' => 'Добавить новый шаблон?',
//        ));

        $oForm->addBntCancel('list');

        $this->setExtInterface($oForm);

        return psComplete;

    }

    public function actionAdd(){

        try {

            $aData = $this->get('data');

            //$bRes = DomainManagerApi::linkDomain($iSiteId,$iDomainId);

            $bRes = SEOTemplatesToolMapper::saveItem($aData);

            if(!$bRes) throw new Exception('Ошибка: шаблон не добавлен!');
            
            $this->addNoticeReport("Добавление SEO шаблона", "", skLogger::logUsers, "SEOTemplatesToolModule");
            
        } catch(Exception $e) {
            $this->addError($e->getMessage());
        }

        // переход к списку
        $this->actionList();
        
        return psComplete;

    }

    public function actionDelete(){

        try {

            $aData = $this->get('data');

            $bRes = SEOTemplatesToolMapper::delItem($aData['id']);

            if(!$bRes) throw new Exception('Ошибка: не удалось удалить шаблон!');
            
            $this->addNoticeReport("Удаление SEO шаблона", "id шаблона $aData[id]", skLogger::logUsers, "SEOTemplatesToolModule");

        } catch(Exception $e) {
            $this->addError($e->getMessage());
        }


        // переход к списку
        $this->actionList();

        return psComplete;

    }

    public function actionEditForm(){

        $oForm = new ExtForm();

        $aData = $this->get('data');

        $this->setPanelName('Редактирование SEO шаблона',true);

        /* установить набор элементов формы */
        $aItems = SEOTemplatesToolMapper::getFullParamDefList(SEOTemplatesToolApi::getFullListFields());
        $aItems['info']['height'] = 200;
        $aItems['id']['view'] = 'hide';
        if(!CurrentAdmin::isSystemMode()){
            $aItems['sid']['view'] = 'hide';
            $aItems['name']['disabled'] = true;
            //$aItems['info']['disabled'] = true;
        }

        if( $aData['info'] )
            $aData['info'] = nl2br( $aData['info'] );

        $oForm->setFields($aItems);
        $oForm->setValues($aData);

        $oForm->addBntSave( 'update' );

        $oForm->addBntCancel('list');

        $this->setExtInterface($oForm);

        return psComplete;

    }

    public function actionUpdate(){

        try {

            $aData = $this->get('data');

            $bRes = SEOTemplatesToolMapper::saveItem($aData);

            if(!$bRes) throw new Exception('Ошибка: шаблон не изменен!');

            $this->addNoticeReport("Изменение SEO шаблона", "id шаблона $aData[id]", skLogger::logUsers, "SEOTemplatesToolModule");
            
        } catch(Exception $e) {
            $this->addError($e->getMessage());
        }

        // переход к списку
        $this->actionList();

        return psComplete;

    }



}
