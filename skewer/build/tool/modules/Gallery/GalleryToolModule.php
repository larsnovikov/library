<?php
/**
 * Модуль редактирования форматов для галереи
 */
class GalleryToolModule extends AdminToolTabModulePrototype {

    /**
     * Название вкладки модуля
     * @var string
     */
    protected $sTabName = 'Профили галереи';

    /**
     * Массив полей, выводимых колонками в списке профилей
     * @var array
     */
    protected $aProfilesListFields = array('title');

    /**
     * Массив полей, выводимых колонками в списке форматов для профиля настроек
     * @var array
     */
    protected $aFormatsListFields = array('format_id', 'title', 'width', 'height', 'active' );

    /**
     * Id текущего открытого профиля
     * @var int
     */
    protected $iCurrentProfile = 0;

    /**
     * Иницализация
     */
    protected function preExecute() {

        CurrentAdmin::testControlPanelAccess();

        /* Восстанавливаем Id текущего открытого профиля */
        $this->iCurrentProfile = $this->getInt('currentProfile');

    }

    /**
     * Установка служебных данных
     * @param \ExtPrototype $oExtIface
     */
    protected function setServiceData( ExtPrototype $oExtIface ) {

        // установить заголовок модуля
        $oExtIface->setTitle( $this->sTabName );

        // установить данные для передачи интерфейсу
        $oExtIface->setServiceData( array(
            'currentProfile' => $this->iCurrentProfile,
        ) );

    }

    /**
     * Вызывается в случае отсутствия явного обработчика
     * @return int
     */
    protected function actionInit() {

        return $this->actionGetProfilesList();

    }

    /**
     * Обработчик состояния списка профилей
     * @return int
     */
    protected function actionGetProfilesList() {

        $oList = new ExtList();

        $this->setPanelName('Профили настроек изображений',true);

        $this->iCurrentProfile = false;

        /* поля, выводимые в списке */
        $aModel = Gallery2ProfilesMapper::getFullParamDefList($this->aProfilesListFields);
        $aModel['title']['listColumns'] = array('flex' => 1);

        $oList->setFields($aModel);

        /* данные для списка */
        $aItems = Gallery2ProfilesApi::getProfilesList();

        $oList->setValues( $aItems['items'] );
        $oList->setTotal( $aItems['count'] );

        /* Кнопки "редактировать", "Удалить" */
        $oList->addRowBtnUpdate('addUpdProfile');

        $oList->addRowBtnDelete('delProfile');

        /* "Добавить" на боковой панели */
        $oList->addBntAdd('AddUpdProfile');

        $this->setExtInterface( $oList );

    }// func

    /**
     * Состояние редактирования профиля
     */
    protected function actionAddUpdProfile() {

        $oForm = new ExtForm();

        /* Данные по профилю */
        $aData = $this->get('data');

        /* Id профиля */
        $iProfileId = (is_array($aData) && isset($aData['profile_id'])) ? (int)$aData['profile_id'] : 0;

        /* Отрабатывает возврат из списка форматов к профилю */
        if($this->iCurrentProfile)
            $iProfileId = $this->iCurrentProfile;

        $this->setPanelName('Добавление профиля настроек',true);

        /* Получаем данные профиля или заготовку под новый профиль */
        $aItem = $iProfileId ? Gallery2ProfilesApi::getProfile($iProfileId) : Gallery2ProfilesApi::getProfileBlankValues();

        /* установить набор элементов формы */
        $oForm->setFields( Gallery2ProfilesMapper::getFullParamDefList(Gallery2ProfilesMapper::getModelFields()) );

        /* установить значения для элементов */
        $oForm->setValues( $aItem );

        /* добавление кнопок */

        $oForm->addBntSave('saveProfile');
        $oForm->addBntCancel();



        if ( $iProfileId ) {
            /* Кнопка "Редактировать форматы" */
            $oForm->addDockedItem(array(
                'text' => 'Форматы',
                'iconCls' => 'icon-edit',
                'state' => 'init',
                'action' => 'formatsList'
            ));

            $this->setPanelName('Редактирование профиля настроек',true);
            $this->iCurrentProfile = $iProfileId;

            $oForm->addBntSeparator('->');
            $oForm->addBntDelete();
        }

        // вывод данных в интерфейс
        $this->setExtInterface( $oForm );

    }// func

    /**
     * Сохраняет профиль
     * @throws Exception
     */
    protected function actionSaveProfile() {

        /* Сохранение данных с профиля */
        try {

            $aData = $this->get( 'data' );

            if(!count($aData)) throw new Exception ('Error: Data is not sent!');

            $iProfileId = ($aData['profile_id'])? $aData['profile_id']: false;
            /* Добавляем либо обнавляем профиль */
            Gallery2ProfilesApi::setProfile($aData, $iProfileId);

        } catch (Exception $e) {
            echo $e;
        }

        /* вывод списка */
        $this->actionGetProfilesList();

    }// func

    /**
     * Удаляет выбранный профиль
     * @throws Exception
     */
    protected function actionDelProfile() {

        /* Данные по профилю */
        $aData = $this->get('data');

        try {

            if(!isSet($aData['profile_id']) OR !(int)$aData['profile_id']) throw new Exception('Error: Element is not removed!');

            /*Удаление профиля*/
            Gallery2ProfilesApi::remove($aData['profile_id']);

        } catch (Exception $e) {

            echo $e;
        }

        /*Вывод списка профилей*/
        $this->actionGetProfilesList();

    }// func

    /**
     * Выводит список форматов для профиля
     * @throws Exception
     */
    protected function  actionFormatsList() {

        /* Данные по профилю */
        $aData = $this->get('data');

        $this->setPanelName('Форматы изображений',true);

        try {

            if(!isSet($aData['profile_id']) OR !(int)$aData['profile_id']) throw new Exception('Error: Formats not received!');

            $oList = new ExtList();

            if(!$this->iCurrentProfile)
                $this->iCurrentProfile = (int)$aData['profile_id'];

            /* поля, выводимые в списке */
            $aModel = Gallery2FormatsMapper::getFullParamDefList($this->aFormatsListFields);

            $aModel['format_id']['listColumns'] = array('hidden' => true);
            $aModel['title']['listColumns'] = array('flex' => 1);
            $aModel['active']['listColumns'] = array('width' => 150);

            $oList->setFields($aModel);

            $aItems = Gallery2FormatsApi::getByProfile($aData['profile_id']);

            $oList->setValues( $aItems );

            /* Кнопки "редактировать", "Удалить" */
            $oList->addRowBtnUpdate('addUpdFormat');
            $oList->addRowBtnDelete('delFormat');
            /* Кнопка "Редактировать форматы" */
            $oList->addDockedItem(array(
                'text' => 'К профилю',
                'iconCls' => 'icon-cancel',
                'state' => 'init',
                'action' => 'addUpdProfile'
            ));
            /* "Добавить" на боковой панели */
            $oList->addBntAdd('AddUpdFormat');

            $oList->enableDragAndDrop('sortFormats');

            $this->setExtInterface( $oList );

        } catch (Exception $e) {
            echo $e;
        }
    }// func

    /**
     * Сортировка форматов
     */
    protected function actionSortFormats() {

        $aData = $this->get( 'data' );
        $aDropData = $this->get( 'dropData' );
        $sPosition = $this->get( 'position' );

        if( !isSet($aData['format_id']) || !$aData['format_id'] ||
            !isSet($aDropData['format_id']) || !$aDropData['format_id'] || !$sPosition )
            $this->addError('Ошибка! Неверно заданы параметры сортировки');


        $aData = Gallery2FormatsMapper::getItem($aData['format_id']);
        $aDropData = Gallery2FormatsMapper::getItem($aDropData['format_id']);

        if( !Gallery2FormatsApi::sortParams($aData, $aDropData, $sPosition) )
            $this->addError('Ошибка! Неверно заданы параметры сортировки');


    }

    /**
     * Состояние добавления/редактирования формата
     */
    protected function actionAddUpdFormat() {

        try {

            $oForm = new ExtForm();

            /* Данные по формату */
            $aData = $this->get('data');

            /* Id формата */
            $iFormatId  = (is_array($aData) && isset($aData['format_id'])) ? (int)$aData['format_id'] : 0;

            $this->setPanelName('Добавление формата изображения',true);
            if($iFormatId)
                $this->setPanelName('Редактирование формата изображения',true);

            $iProfileId = 0;

            if(!$iFormatId)
                if(!$iProfileId = $this->iCurrentProfile)
                    throw new Exception ('Error: Format is not associated with the profile!');

            if(!$iProfileId)
                if(!$iProfileId = $aData['profile_id'])
                    throw new Exception('Error: Format is not associated with the profile!');



            /* Получаем данные формата или заготовку под новый формат */
            $aValues = $iFormatId ? Gallery2FormatsApi::getById($iFormatId) : Gallery2FormatsApi::getProfileBlankValues();

            $aValues['profile_id'] = $iProfileId;

            /* установить набор элементов формы */
            $aItems = Gallery2FormatsMapper::getFullParamDefList(Gallery2FormatsMapper::getModelFields());

            $oForm->setFields( $aItems );

            /* установить значения для элементов */
            $oForm->setValues( $aValues );

            /* добавление кнопок */

            $oForm->addBntSave('saveFormat');
            $oForm->addBntCancel('formatsList');

            if ( $iFormatId ) {
                $oForm->addBntSeparator('->');
                $oForm->addBntDelete('delFormat');
            }

            // вывод данных в интерфейс
            $this->setExtInterface( $oForm );

        } catch (Exception $e) {

            echo $e;
        }
    }// func

    /**
     * Сохраняет формат в профиле настроек
     * @throws Exception
     */
    protected function actionSaveFormat() {

        /* Сохранение данных формата */
        try {

            $aData = $this->get( 'data' );

            if(!count($aData)) throw new Exception ('Error: Data is not send!');

            $iFormatId = ($aData['format_id'])? $aData['format_id']: false;

            /* Привязка к профилю обязательна */
            if(!isSet($aData['profile_id']) OR !(int)$aData['profile_id']) throw new Exception('Error: Data is not send!');

            // если водяной знак - файл, то проверить, что он png
            $sWatermark = isset($aData['watermark']) ? $aData['watermark'] : '';
            if ( $sWatermark ) {
                $sPossibleFileName = ROOTPATH.$sWatermark;
                if (file_exists($sPossibleFileName)) {
                    $aFile = getimagesize($sPossibleFileName);
                    if ( $aFile[2] !== 3 )
                        $this->addError( 'В качестве водяного знака может быть использован только файл типа PNG' );
                }
            }

                /* Добавляем либо обнавляем формат */
            Gallery2FormatsApi::setFormat($aData, $iFormatId);

            $this->addMessage( 'Формат сохранен' );

        } catch (Exception $e) {
            echo $e;
        }

        /* вывод списка */
        $this->actionFormatsList();

    }// func

    /**
     * Удаляет выбранный формат
     * @throws Exception
     */
    protected function actionDelFormat() {

        /* Данные по формату */
        $aData = $this->get('data');

        try {

            if(!isSet($aData['format_id']) OR !(int)$aData['format_id']) throw new Exception('Error: Element(Format) is not removed!');

            /*Удаление формат*/
            Gallery2FormatsApi::removeFormat($aData['format_id']);

        } catch (Exception $e) {

            echo $e;
        }

        /*Вывод списка профилей*/
        $this->actionFormatsList();

    }// func

}
