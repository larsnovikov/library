<?php

/**
 * @class LoggerToolModule
 * @extends skModule
 * @project Skewer
 * @package kernel
 *
 * @author Andy Mitrich, $Author: acat $
 * @version $Revision: 1672 $
 * @date 27.01.12 13:30 $
 *
 */

class LoggerToolModule extends AdminToolTabModulePrototype {

    protected $iOnPage = 20;
    protected $sTabName = 'Логи';
    protected $iPage = 0;

    // фильтр по модулям
    protected $mModuleFilter = false;
    // фильтр по пользователю
    protected $mLoginFilter = false;
    // фильтр по уровню событий
    protected $mLevelFilter = false;
    // фильтр по типу журнала
    protected $mLogFilter = false;
    // фильтры по датам
    protected $mDateFilter1 = false;
    protected $mDateFilter2 = false;

    protected function preExecute() {

        CurrentAdmin::testControlPanelAccess();

        // номер страницы
        $this->iPage = $this->getInt('page');

        // запрос значений фильтров
        $this->mModuleFilter = $this->get('module',false);
        $this->mLoginFilter = $this->get('login',false);
        $this->mLevelFilter = $this->get('event_type',false);
        $this->mLogFilter = $this->get('log_type',false);
        $this->mDateFilter1 = $this->getDateFilter('date1');
        $this->mDateFilter2 = $this->getDateFilter('date2');

    }

    /**
     * Запрашивает значение фильтра по дате с валидацией
     * @param $sName имя фильтра
     * @return bool|string
     */
    protected function getDateFilter( $sName ) {

        // запрос значения
        $mVal = $this->getStr($sName,false);

        // проверка значения
        if ( !$mVal ) return false;

        // валидация
        if ( preg_match('/^\d{4}-\d{2}-\d{2}$/',$mVal) ) {
            return $mVal;
        } else {
            return false;
        }

    }

    public function actionInit(){

        // объект для построения списка
        $oList = new ExtList();

        $aModel = LoggerToolApi::getListModel(LoggerToolApi::getListFields());

        // задать модель данных для вывода
        $oList->setFields( $aModel );

        // добавление кнопок
        $oList->addRowBtn(array(
            'tooltip' =>'Открыть детальную страницу',
            'iconCls' => 'icon-edit',
            'action' => 'showForm',
            'state' => 'edit_form'
        ));

        // добавляем фильтр по пользователям
        $oList->addFilterSelect( 'login', LoggerToolApi::getUsersLogin(), $this->mLoginFilter, 'Пользователь' );
        // добавляем фильтр по модулям
        $oList->addFilterSelect( 'module', LoggerToolApi::getModules(), $this->mModuleFilter, 'Название модуля' );
        // добавляем фильтр по уровню событий
        $oList->addFilterSelect( 'event_type', LoggerToolApi::getEventLevels(), $this->mLevelFilter, 'Уровень события' );
        // добавляем фильтр по уровню событий
        $oList->addFilterSelect( 'log_type', LoggerToolApi::getLogTypes(), $this->mLogFilter, 'Тип журнала' );

        // фильтр по дате
        $oList->addFilterDate( 'date', '', 'Дата' );

        // логи чистить может только sys
        if (CurrentAdmin::isSystemMode()){
        // кнопка очистки логов
            $oList->addFilterButton('clearLog', 'Очистить логи', 'Удалить все записи?');
        }

        // число записей на страницу
        $oList->setOnPage( $this->iOnPage );
        $oList->setPageNum($this->iPage);

        // добавление набора данных
        $aFilter = array(
            'limit' => $this->iOnPage ? array (
                'start' => $this->iPage*$this->iOnPage,
                'count' => $this->iOnPage
            ) : false,
            'order' => array(
                'field' => 'event_time',
                'way' => 'DESC'
            )
        );

        /*
         * Фильтры
         * по названию модуля
         */
        if ( false !== $this->mModuleFilter ) {
            $aFilter['module'] = $this->mModuleFilter;
        }
        if ( false !== $this->mLoginFilter ) {
            $aFilter['login'] = $this->mLoginFilter;
        }
        // по уровню доступа
        if ( false !== $this->mLevelFilter ) {
            $aFilter['event_type'] = $this->mLevelFilter;
        }
        // по типу журнала
        if ( false !== $this->mLogFilter ) {
            $aFilter['log_type'] = $this->mLogFilter;
        }

        // если заданы оба параметра фильтра по дате
        if ( $this->mDateFilter1 and $this->mDateFilter2 ) {
            $aFilter['event_time'] = array(
                'sign' => 'BETWEEN',
                'value' => array($this->mDateFilter1, $this->mDateFilter2.' 23:59:59')
            );
        } elseif ( $this->mDateFilter1 ) {
            // если только первый
            $aFilter['event_time'] = array(
                'sign' => '>=',
                'value' => $this->mDateFilter1
            );
        } elseif ( $this->mDateFilter2 ) {
            // если только второй
            $aFilter['event_time'] = array(
                'sign' => '<=',
                'value' => $this->mDateFilter2.' 23:59:59'
            );
        }

        // добавление набора данных
        $aItems = LoggerToolApi::getListItems($aFilter);

        $oList->setValues( $aItems['items'] );
        $oList->setTotal( $aItems['count'] );

        // вывод данных в интерфейс
        $this->setExtInterface( $oList );
    }

    protected function actionClearLog(){

        // логи чистить может только sys
        if (CurrentAdmin::isSystemMode()){
            LoggerToolApi::clearLog();
        }

        $this->actionInit();
    }

    /**
     * Отображение формы
     */
    protected function actionShowForm() {

        // подключить автоматический генератор форм
        $oForm = new ExtForm();
        // номер новости
        $aData = $this->get('data');

        $iItemId = (is_array($aData) && isset($aData['id'])) ? (int)$aData['id'] : 0;

        $aItem = array();
        if ( $iItemId ) $aItem = LoggerToolApi::getLogReportById($iItemId);

        $aListFields = LoggerToolApi::getDetailFields();

        $aModel = LoggerToolApi::getListModel($aListFields);
        
		// установить набор элементов формы в зависимости от режима
        if(!CurrentAdmin::isSystemMode())
			unSet($aModel['description']);	
		
		$oForm->setFields( $aModel );
		
        // установить значения для элементов
        $oForm->setValues( $aItem );

        $oForm->addDockedItem(array(
            'text' => '_cancel',
            'iconCls' => 'icon-cancel',
            'state' => 'init',
            'action' => 'init'
        ));

        // вывод данных в интерфейс
        $this->setExtInterface( $oForm );
    }


    /**
     * Установка служебных данных
     * @param \ExtPrototype $oExtIface
     */
    protected function setServiceData( ExtPrototype $oExtIface ) {

        // установить заголовок модуля
        $oExtIface->setTitle( $this->sTabName );

        // установить данные для передачи интерфейсу
        $oExtIface->setServiceData( array(
            'module' => $this->mModuleFilter,
            'event_type' => $this->mLevelFilter,
            'log_type' => $this->mLogFilter,
            'page' => $this->iPage,
            'date1' => $this->mDateFilter1,
            'date2' => $this->mDateFilter2,
        ) );

    }

}//class