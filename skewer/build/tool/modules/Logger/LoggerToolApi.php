<?php

/**
 * @class LoggerToolApi
 * @extends skModule
 * @project Skewer
 * @package kernel
 *
 * @author Andy Mitrich, $Author: acat $
 * @version $Revision: 1406 $
 * @date 27.01.12 13:48 $
 *
 */

class LoggerToolApi {

    private static $aEventLevelList = array(
        1 => 'Критический',
        2 => 'Предупреждение',
        3 => 'Ошибка',
        4 => 'Уведомление'
    );

    private static $aLogType = array(
        71 => 'Действия пользователей',
        72 => 'Планировщик заданий',
        73 => 'Системный журнал',
        74 => 'Журнал отладки'
    );

    private static $aCalledModuleList = array(
        'Users' => 'Система управления пользователями',
        'Auth' => 'Система авторизации',
        'Tree' => 'Дерево разделов'
    );

    /* State List */

    /**
     * @static
     * @return array
     */
    public static function getListFields(){

        return array(
            'id',
            'login',
            'event_time',
            'title',
            'module_title',
            'event_title',
            'log_title',
            'ip'
        );
    }

    /**
     * @static
     * @return array
     */
    public static function getDetailFields(){

        return array(
            'id',
            'event_time',
            'event_title',
            'log_title',
            'title',
            'module_title',
            'initiator',
            'ip',
            'proxy_ip',
            'external_id',
            'description'
        );
    }

    /**
     * @static
     * @param $aFields
     * @return array
     */
    public static function getListModel($aFields){

        return skLoggerMapper::getFullParamDefList($aFields);
    }

    /**
     * @static
     * @param $aInputData
     * @return array
     */
    public static function getListItems($aInputData){

        $aItems = skLoggerMapper::getItemsList($aInputData);

        $aModules = self::getModules();

        if ( sizeof($aItems['items']) )
            foreach( $aItems['items'] as &$aItem ){

                $aItem['event_title'] = self::$aEventLevelList[$aItem['event_type']];
                $aItem['log_title'] = self::$aLogType[$aItem['log_type']];
                $aItem['module_title'] = ( isset($aModules[$aItem['module']]) )? $aModules[$aItem['module']]: '';
                $oDate = date_create_from_format('Y-m-d H:i:s', $aItem['event_time']);
                $aItem['event_time'] = $oDate->format('d.m.Y H:i');
                if( !$aItem['login'] )
                    $aItem['login'] = ' - system - ';
            }

        return $aItems;
    }

    /**
     * @static
     * @param $iItemId
     * @return array
     */
    public static function getLogReportById( $iItemId ){

        $aModules = self::getModules();
        $aItem =  skLoggerMapper::getItem($iItemId);

        // замена индексов на строки
        $aItem['event_title'] = self::$aEventLevelList[$aItem['event_type']];
        $aItem['log_title'] = self::$aLogType[$aItem['log_type']];
        $aItem['module_title'] = isset($aModules[$aItem['module']]) ? $aModules[$aItem['module']] : 'не определен';

        // форматирование данных описания
        $aDesc = json_decode($aItem['description']);
			if ( !json_last_error() )
            	$aItem['description'] = '<pre>'.print_r((array)$aDesc,true).'</pre>';

        return $aItem;
    }

    /**
     * @static
     * @return array
     */
    public static function getModules(){


        $aModules = array();
        /** @noinspection PhpUndefinedMethodInspection */
        $aLayers = skProcessor::getLayers();

        foreach( $aLayers as $sLayer ){

            if(!skConfig::isExists('buildConfig.'.$sLayer.'.modules')) continue;

            $aListModules = array_keys(skConfig::get('buildConfig.'.$sLayer.'.modules'));

            foreach( $aListModules as $sModule){

                $aModules[$sModule] = (skConfig::isExists('buildConfig.'.$sLayer.'.modules.'.$sModule.'.title'))? skConfig::get('buildConfig.'.$sLayer.'.modules.'.$sModule.'.title'): $sModule;
            }
        }

        $aModules = array_merge($aModules, self::$aCalledModuleList);
        asort($aModules);
        return $aModules;
    }


    /**
     * @static
     *
     */
    public static function getUsersLogin(){

        $aUsers = array('0' => ' - system - ');

        $aData = AuthUsersMapper::getUsersList();

        if($aData && $aData['count'])
            foreach( $aData['items'] as $aItem )
                $aUsers[$aItem['login']] = $aItem['login'];

        return $aUsers;
    }

    /**
     * @static
     * @return array
     */
    public static function getEventLevels(){

        return self::$aEventLevelList;
    }

    /**
     * @static
     * @return array
     */
    public static function getLogTypes(){

        return self::$aLogType;
    }

    public static function clearLog(){

        return skLoggerMapper::clearLog();
    }

}//class