<?php

/* main */
$aConfig['name']     = 'LoggerTool';
$aConfig['title']     = 'Логгер (админ)';
$aConfig['version']  = '1.000a';
$aConfig['description']  = 'Система логирования';
$aConfig['revision'] = '0001';
$aConfig['layer']     = 'tool';

return $aConfig;