<?php

/* main */
$aConfig['name']     = 'PolicyTool';
$aConfig['title']    = 'Политики доступа';
$aConfig['version']  = '1.000a';
$aConfig['description']  = 'Админ-интерфейс управления политиками доступа';
$aConfig['revision'] = '0001';
$aConfig['layer']     = 'tool';

$aConfig['policy'] = array( array(
    'name'    => 'allowPolicyView',
    'title'   => 'Разрешить показывать список политик',
    'default' => 1
), array(
    'name'    => 'allowUsersView',
    'title'   => 'Разрешить показывать список пользователей',
    'default' => 1
), array(
    'name'    => 'useControlPanel',
    'title'   => 'Доступ к панели управления',
    'default' => 0
), array(
    'name'    => 'useDesignMode',
    'title'   => 'Доступ в дизайнерский режим',
    'default' => 0
));

return $aConfig;
