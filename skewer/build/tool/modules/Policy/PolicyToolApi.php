<?php

/**
 * @class PolicyToolApi
 * @extends skModule
 * @project Skewer
 * @package kernel
 *
 * @author Andy Mitrich, $Author: sapozhkov $
 * @version $Revision: 1316 $
 * @date 16.12.11 14:09 $
 *
 */

class PolicyToolApi {

    public function getListFields(){

        return array('id','title','active');
    }

    public function getDetailFields(){

        return $this->getParamList(array('id','title','alias','area','active'));
    }

    /**
     * Отдает набор полей с описаниями
     * @param array $aFieldFilter - массив с набором колонок
     * @return array
     */
    public function getParamList( $aFieldFilter ) {

        return GroupPolicyMapper::getFullParamDefList( $aFieldFilter );

    }

    public function getPolicy($iPolicyId){

        return GroupPolicyMapper::getPolicyDetail($iPolicyId);
    }

    /**
     * Отдает шаблонный набор значений для добавления новой записи
     * @return array
     */
    public function getBlankValues() {
        return array(
            'title' => 'Новая политика',
            'publication_date' => date('d.m.Y',time()),
            'active' => 1
        );
    }

    /**
     * Сохранение данных функциональной политики
     * @static
     * @param array $aData - набор данных для сохранения
     * @return bool
     */
    public static function saveFuncData( $aData ) {

        // выйти, если данных недостаточно
        if ( !$aData or !isset($aData['id']) )
            return false;

        // id политики
        $iPolicyId = (int)$aData['id'];
        if ( !$iPolicyId ) return false;

        // запросить все политики
        $aAllData = skConfig::get('buildConfig.funcPolicy');

        // перебираем все группы
        foreach ( $aAllData as $sGroupName => $aGroup ) {

            // если есть элементы
            if ( isset($aGroup['items']) ) {

                // перебираем их
                foreach ( $aGroup['items'] as $aParam ) {

                    // сбор имен переменных
                    $sParamName = $aParam['name'];
                    $sModuleName = $sGroupName.'Module';

                    // собираем имя переменной
                    $sValName = sprintf('params.%s.%s', $sGroupName, $sParamName);

                    // если есть данные во входном массиве
                    if ( isset( $aData[$sValName] ) ) {

                        // пришедшее значение
                        $mValue = $aData[$sValName];

                        // название параметра
                        $sParamTitle = $aParam['title'];

                        // сохранить значение
                        Policy::setGroupActionParam( $iPolicyId, $sModuleName, $sParamName, $mValue, $sParamTitle );

                        /**
                         * todo удалить данные, которых нет во входном массиве
                         * если нет во входном массиве, но есть в сохраненных данных
                         * эту ситуацию решили оставить без изменений в записях
                         * пока не устаканится структура и принцип работы
                         */

                    }

                }

            }

        }

        return true;

    }

    /**
     * Возвращает массив для инициализации спец компонента в js, отвечающего
     *      за набор параметров по подулям в рамках политики
     * @param $iPolicyId
     * @return array
     */
    public static function getFuncDataForField( $iPolicyId ) {

        // все значения функциональных политик
        $aAllData = skConfig::get('buildConfig.funcPolicy');

        // запросить данные для данной политики
        $aSetData = Policy::getGroupActionData( $iPolicyId );

        // выходной массив
        $aOut = array(
            'name' => 'params',
            'title' => 'Функциональные настройки',
            'value' => array(
                'groups' => array()
            )
        );

        // ссылка на ветку для удобаства
        $aGroupsRef = &$aOut['value']['groups'];

        // перебираем все группы
        foreach ( $aAllData as $sGroupName => $aGroup ) {

            // новая группа
            $aNewGroup = array(
                'name' => $sGroupName,
                'title' => isset($aGroup['moduleTitle']) ? $aGroup['moduleTitle'] : $sGroupName,
                'items' => array()
            );


            if ( isset($aGroup['items']) ) {

                // перебираем все элементы
                foreach ( $aGroup['items'] as $aParam ) {

                    // сбор имен переменных
                    $sParamName = $aParam['name'];
                    $sModuleName = $sGroupName.'Module';

                    // запрос значения
                    if ( isset($aSetData[$sModuleName]) and
                         isset($aSetData[$sModuleName][$sParamName]) and
                             isset($aSetData[$sModuleName][$sParamName]['value'])
                       )
                    {
                        $aParam['value'] = $aSetData[$sModuleName][$sParamName]['value'];
                    } else {
                        $aParam['value'] = '';
                    }

                    // добавляем параметр в список
                    $aNewGroup['items'][] = $aParam;

                }
            }

            // добавляем группу
            $aGroupsRef[] = $aNewGroup;

        }

        return $aOut;

    }

}
