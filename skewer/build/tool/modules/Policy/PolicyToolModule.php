<?php

/**
 * @class PolicyToolModule
 * @extends skModule
 * @project Skewer
 * @package kernel
 *
 * @author Andy Mitrich, $Author: acat $
 * @version $Revision: 1475 $
 * @date 16.12.11 14:08 $
 *
 */

class PolicyToolModule extends AdminToolTabModulePrototype{

    /* @var \PolicyToolApi */
    private $oPolicyApi = NULL;

    // текущий номер страницы ( с 0, а приходит с 1 )
    protected $iPage = 0;

    /**
     * @var string - Имя модуля
     */
    protected $sTabName = 'Политики доступа';

    /**
     * Имя панели
     * @var string
     */
    protected $sPanelName = '';

    /**
     * Метод, выполняемый перед action меодом
     * @throws ModuleAdminErrorException
     * @return bool
     */
    protected function preExecute() {

        // номер страницы
        $this->iPage = $this->getInt('page')-1;
        if ( $this->iPage < 0 ) $this->iPage = 0;

        CurrentAdmin::testControlPanelAccess();

        // если системный админ или доступ разрешен
        if ( !CurrentAdmin::isSystemMode() and !CurrentAdmin::canDo(__CLASS__, 'allowPolicyView') )
            throw new ModuleAdminErrorException( 'accessDenied' );

    }

    /**
     * Первичное состояние
     */
    protected function actionInit() {

        $this->oPolicyApi = new PolicyToolApi();
        // вывод списка
        $this->actionList();
    }

    /**
     * Список пользователей
     */
    protected function actionList() {

        // установка заголовка панели
        $this->setPanelName('Список политик');

        // объект для построения списка
        $oList = new ExtList();

        /**
         * Модель данных
         */

        $aFieldFilter = $this->oPolicyApi->getListFields();
        $aListModel = $this->oPolicyApi->getParamList($aFieldFilter);
        // задать модель данных для вывода
        $oList->setFields( $aListModel );

        /**
         * Данные
         */

        // добавление набора данных
        $aFilter = array();

        //if(CurrentAdmin::isSystemMode())
        $aFilter['where_condition']['active'] = array("sign" => "!=", "value" => Policy::stSys); // не выводим политику сисадмина в списке

        $aItems = Policy::getPolicyList($aFilter);

        $oList->setValues( $aItems['items'] );
        $oList->setTotal( $aItems['count'] );

        /**
         * Интерфейс
         */

        // добавление кнопок
        $oList->addRowBtnUpdate();

        // кнопка добавления
        $oList->addBntAdd('show');

        // вывод данных в интерфейс
        $this->setExtInterface( $oList );
    }

    /**
     * Отображение формы
     */
    protected function actionShow() {

        // подключить автоматический генератор форм
        $oForm = new ExtForm();

        // номер записи
        $aData = $this->get('data');
        $iItemId = (is_array($aData) && isset($aData['id'] ) && $aData['id']) ? (int)$aData['id'] : $this->getInt('id');

        // установка заголовка панели
        $this->setPanelName($iItemId?'Редактирование':'Добавление',true);

        // запись новости
        $aItem = $iItemId ? $this->oPolicyApi->getPolicy($iItemId) : $this->oPolicyApi->getBlankValues();

        // установить набор элементов формы
        $aFieldFilter = $this->oPolicyApi->getDetailFields();

        // добавить поле - набор галочек
        $aFieldFilter['params'] = $oForm->getSpecificItemInitArray('CheckSet', PolicyToolApi::getFuncDataForField( $iItemId ) );

        $oForm->setFields( $aFieldFilter );

        // установить значения для элементов
        $oForm->setValues( $aItem );

        // добавление кнопок
        $oForm->addBntSave();

        // при редактировании
        if ( $iItemId ) {
            // добавить кнопку редактирования разделов
            $oForm->addDockedItem(array(
                'text' => 'Разделы',
                'iconCls' => 'icon-edit',
                'state' => '',
                'action' => 'sections',
                'addParams' => array(
                    'id' => $iItemId
                )
            ));
        }

        // кнопка отмены
        $oForm->addBntCancel();

        // при редактировании
        if ( $iItemId ) {
            // добавить кнопку удаления
            $oForm->addBntSeparator('->');
            $oForm->addBntDelete();
        }

        // вывод данных в интерфейс
        $this->setExtInterface( $oForm );
    }

    /**
     * Сохранение новости
     * @throws Exception
     */
    protected function actionSave() {

        // запросить данные
        $aData = $this->get( 'data' );
        $bHasId = isset($aData['id']) && is_numeric($aData['id']) && $aData['id'];
        $iId = 0;

        // есть данные - сохранить
        if ( $aData ) {
            // основная запись
            $iId = Policy::update( $aData );
            // набор функциональных параметров
            $aData['id'] = $iId;

            if ( !isset($aData['area']) )
                throw new Exception('Область видимости не задана');

            if ( !GroupPolicyMapper::hasArea($aData['area']) )
                throw new Exception('Не верное значение области видимости');

            PolicyToolApi::saveFuncData( $aData );
            Policy::incPolicyVersion();
        }

        // если задан id и даные сохранены
        if ( $bHasId and $iId ) {
            // вывод списка
            $this->addMessage('Политика изменена');
            $this->addModuleNoticeReport('Редактирование политики',$aData);
            $this->actionList();
        } else {
            // вывод записи на редактиорование
            $this->addMessage('Политика добавлена');
            $this->addModuleNoticeReport('Добавление политики',$aData);
            $this->set('id',$iId);
            $this->actionShow();
        }

    }

    /**
     * Удаляет запись
     */
    protected function actionDelete() {

        // запросить данные
        $aData = $this->get( 'data' );

        // id записи
        $iItemId = ( is_array($aData) and isset($aData['id']) ) ? (int)$aData['id'] : 0;

        // удаление
        $iRes = Policy::delete( $iItemId );

        if ( $iRes ) {
            $this->addMessage('Политика удалена');
            $this->addModuleNoticeReport('Удаление политики',$aData);
        } else {
            $this->addMessage('При удалении произошла ошибка');
        }

        // вывод списка
        $this->actionList();

    }

    /**
     * Сохранение политики доступа по разделам
     * @throws ModuleAdminErrorException
     */
    protected function actionSaveSections(){

        // запросить идентификатор сущности
        $iItemId = $this->getInt('id');

        // запросить данные для сохранения
        $iStartSection = $this->getInt('startSection');
        $aItemsA = $this->get('itemsAllow');
        $aItemsD = $this->get('itemsDeny');

        // проверка пришедщих данных
        if ( !$iItemId )
            throw new ModuleAdminErrorException( 'не задан идентификатор политики' );
        if ( !is_array($aItemsA) or !is_array($aItemsD) )
            throw new ModuleAdminErrorException( 'не верный формат данных' );

        // запрос данных политики
        $aData = $this->oPolicyApi->getPolicy($iItemId);
        if ( !$aData )
            throw new ModuleAdminErrorException( 'политика не найдена' );

        // сформировать массив для сохраненния
        $aSaveData = array(
            'id' => $iItemId,
            'read_enable' => implode(',',$aItemsA),
            'read_disable' => implode(',',$aItemsD),
            'start_section' => $iStartSection,
        );

        // сохранение
        $iRes = Policy::update( $aSaveData );

        if ( $iRes ) {
            $this->addMessage('Политика обновлена');
            $this->addModuleNoticeReport('Изменение разделов доступа политики',$aData);
        } else {
            $this->addMessage('Произошла ошибка при изменении разделов доступа');
        }

        $this->actionShow();

    }

    /**
     * Форма редактора доступа к разделам
     * @throws ModuleAdminErrorException
     */
    protected function actionSections(){

        // идентификатор политики
        $iItemId = $this->get('id');
        if ( !$iItemId )
            throw new ModuleAdminErrorException( 'не задан идентификатор политики' );

        // запрос данных политики
        $aData = $this->oPolicyApi->getPolicy($iItemId);
        if ( !$aData )
            throw new ModuleAdminErrorException( 'политика не найдена' );

        // объект для построения списка
        $oIface = new ExtUserFile( 'ReadSections' );

        // заголовок панели
        $this->setPanelName( 'Доступ к разделам политики "'.$aData['title'].'"' );

        // сборка дерева разделов
        $oTree = new Tree();
        $this->setData('items',$oTree->getAllSections(0));

        /* Добавляем css файл для */
        $this->addCssFile('policy_tree.css');

        // разрешенные и запрещенные разделы
        $this->setData('startSection',explode(',',$aData['start_section']));
        $this->setData('itemsAllow',explode(',',$aData['read_enable']));
        $this->setData('itemsDeny',explode(',',$aData['read_disable']));

        // кнопки
        $oIface->addBntSave('','saveSection',array('id'=>$iItemId));
        $oIface->addBntCancel('show','',array('id'=>$iItemId));

        // вывод данных в интерфейс
        $this->setExtInterface( $oIface );

    }

    /**
     * Установка служебных данных
     * @param \ExtPrototype $oExtIface
     */
    protected function setServiceData( ExtPrototype $oExtIface ) {

        // установить заголовок модуля
        $oExtIface->setTitle( $this->sTabName );

        // установить данные для передачи интерфейсу
        $oExtIface->setServiceData( array(
            'page' => $this->iPage
        ) );

    }


}
