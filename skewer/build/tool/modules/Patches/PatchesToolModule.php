<?php
/**
 *
 * @class PatchesToolModule
 *
 * @author ArmiT, $Author$
 * @version $Revision$
 * @date $Date$
 * @project Skewer
 * @package Kernel
 */
class PatchesToolModule extends AdminToolTabModulePrototype {

    /**
     * Название вкладки
     * @var string
     */
    protected $sTabName = 'Патчи';

    /**
     * Поля в списке
     * @var array
     */
    protected $aListFields = array('patch_uid', 'install_date', 'description');

    protected function preExecute() {
        CurrentAdmin::testControlPanelAccess();
    }

    protected function actionInit() {

        $this->actionList();

    }// func

    protected function actionList() {

        $this->setPanelName( 'Доступные патчи' );


        $oList = new ExtList();
        $aModel = PatchesToolMapper::getFullParamDefList($this->aListFields);
        $aModel['description']['listColumns'] = array('flex' => 1);
        $aModel['install_date']['listColumns'] = array('width' => 150);

        $oList->setFields( $aModel );

        $aItems = PatchesToolApi::getLocalList();

        $oList->setValues( $aItems );

        $oList->addRowBtn(array(
            'tooltip' =>'_upd',
            'iconCls' => 'icon-install',
            'action' => 'installPatchForm',
            'state' => 'show'
        ));

        // вывод данных в интерфейс
        $this->setExtInterface( $oList );

    }// func

    protected function actionInstallPatchForm() {

        $oForm = new ExtForm();

        try {

            $this->setPanelName('Установка патча',false);
            $aData = $this->get('data');

/*
  'patch_uid' => "blue0011#3103"
  'install_date' => "Не установлен"
  'is_install' => false
  'file' => "#3103/PatchInstall.php"
  'description' => ""
}*/

            if(!isSet($aData['file']) OR
               empty($aData['file'])  OR
               !isSet($aData['patch_uid']) OR
               empty($aData['patch_uid'])
            ) throw new ModuleAdminErrorException('Ошибка: Отсутствуют данные о патче!');

           /* Относительный путь к директории обновления */
            $aItems['patch_file'] = array(
                'name' => 'patch_file',
                'title' => 'Путь к обновлению',
                'view' => 'hide',
                'value' => $aData['file'],
                'disabled' => false,
            );


                /* Информация по ядру */
                $aItems['patch_uid'] = array(
                    'name' => 'patch_uid',
                    'title' => 'Номер обновления',
                    'view' => 'show',
                    'value' => $aData['patch_uid'],
                    'disabled' => false,
                );

                /* Информация по сборке */
                $aItems['status'] = array(
                    'name' => 'status',
                    'title' => 'Статус',
                    'view' => 'show',
                    'value' => ($aData['is_install'])? 'Установлен '.$aData['install_date']: $aData['install_date'],
                    'disabled' => false,
                );

                /* Описание если есть */
                if(!empty($aData['description']))
                    $aItems['description'] = array(
                        'name' => 'description',
                        'title' => 'Описание',
                        'view' => 'show',
                        'value' => $aData['description'],
                        'disabled' => false,
                    );

            $oForm->setFields($aItems);
            $oForm->setValues(array());

            /* Патч не устанавливали - разрешаем ставить */
            if(!$aData['is_install'])
                $oForm->addDockedItem(array(
                    'text' => 'Установить',
                    'iconCls' => 'icon-install',
                    'state' => 'allow_do',
                    'action' => 'installPatch',
                    'actionText' => 'Вы действительно хотите <strong>установить</strong> данный патч?',
                ));

            $oForm->addBntCancel('List');
            $oForm->addBntSeparator('->');

        } catch (ModuleAdminErrorException $e) {

            $this->addError($e->getMessage());
        }
        $this->setExtInterface($oForm);

        return psComplete;

    }// func

    protected function actionInstallPatch() {

        try {

            $aData = $this->get('data');

            if(!isSet($aData['patch_file']) OR empty($aData['patch_file'])) throw new UpdateException('Wrong parameters!');

            $oInstaller = new skPatchInstaller(PATCHPATH.$aData['patch_file']);

            /* Устанавливаем проверку на то, что патч еще не устанавливался */
            $oInstaller->setChecker("PatchesToolApi::checkPatch");

            $oInstaller->install();

            /* Все прошло нормально - пишем о том, что патч поставили */
            PatchesToolMapper::registerPatch(basename($aData['patch_file']), $aData['patch_file'], $oInstaller->getDescription());

        } catch(UpdateException $e) {

            $this->addError($e->getMessage());
        }

        $this->actionList();

    }// func
}// class
