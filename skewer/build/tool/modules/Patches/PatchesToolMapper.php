<?php
/**
 *
 * @class PatchesToolMapper
 *
 * @author ArmiT, $Author$
 * @version $Revision$
 * @date $Date$
 * @project JetBrains PhpStorm
 * @package kernel
 */
class PatchesToolMapper extends skMapperPrototype {

    /**
     * @var string Имя таблицы, с которой работает маппер
     */
    protected  static $sCurrentTable = 'patches';

    protected static $sKeyFieldName = 'patch_uid';

    /**
     * @var array Конфигурация полей таблицы
     */
    protected static $aParametersList = array(
        'patch_uid'     => 'i:str:Патч',
        'install_date'  => 's:date:Дата установки',
        'file'          => 's:str:Файл',
        'description'   => 's:text:Описание',
    );

    public static function registerPatch($sUID, $sFile, $sDescription = '') {
        global $odb;

        $aData['file']          = $sFile;
        $aData['patch_uid']     = $sUID;
        $aData['table_name']   = static::$sCurrentTable;
        $aData['install_date']  = date('Y-m-d H:i:s');
        $aData['description']   = $sDescription;

        $sQuery = "
            INSERT INTO
              `[table_name:q]`
            SET
              patch_uid=[patch_uid:s],
              file=[file:s],
              install_date=NOW(),
              description=[description:s];";

        $odb->query($sQuery, $aData);
        return $odb->insert_id;

    }// func

}// class
