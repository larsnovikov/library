<?php

/* main */
$aConfig['name']     = 'Redirect301Tool';
$aConfig['title']    = 'Управление редиректами 301';
$aConfig['version']  = '1.000a';
$aConfig['description']  = 'Управление редиректами 301';
$aConfig['revision'] = '0001';
$aConfig['layer']     = 'tool';

return $aConfig;
