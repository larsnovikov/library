<?php
/**
 * Класс для работы с таблицей редиректов по 301 ошибке
 *
 * @class: Redirect301ToolMapper
 *
 * @author kolesnikov
 * @version 0.8
 *
 */
class Redirect301ToolMapper extends skMapperPrototype {

    /** @var string Имя таблицы, с которой работает маппер */
    protected static $sCurrentTable = 'redirect301';

    /** @var array Конфигурация полей таблицы */
    protected static $aParametersList = array(
        'id' => 'i:hide:ID правила',
        'old_url' => 's:str:Старый адрес',
        'new_url' => 's:str:Новый адрес',
    );






}
