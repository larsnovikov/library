<?php
/**
 * API работы с
 *
 * @class: Redirect301ToolApi
 *
 * @author kolesnikov
 * @version $Revision: 1316 $
 * @date $Date: 2012-11-27 18:31:02 +0400 (Вт., 27 нояб. 2012) $
 *
 */

class Redirect301ToolApi {

    /**
     * Набор полей для списка
     * @static
     * @return array
     */
    public static function getListFields(){
        return array('id','old_url','new_url');
    }


    /**
     * @static
     * @param $aFields
     * @return array
     */
    public static function getListModel($aFields){

        return Redirect301ToolMapper::getFullParamDefList($aFields);
    }

    /**
     * @static
     * @return array
     */
    public static function getListItems(){

        $aFilter = array();

        return Redirect301ToolMapper::getItems($aFilter);
    }


    public static function makeHtaccessFile(){

        /* Массив меток, подставляемых в шаблон htaccess */
        $aItems = Redirect301ToolMapper::getItems();
        $aDomainItems = DomainsToolApi::getRedirectItems();
        $bOpenSite = (bool)SysVar::get('site_open');

        if(!$aItems) return false;

        $oUpHalper = new skUpdateHelper();
        $aData = array('redirectItems'=>array(),
            'buildVersion'=> BUILDVERSION,
            'buildName'	  => BUILDNAME,
            'buildNumber' => BUILDNUMBER,
            'inCluster'=>INCLUSTER,
            'USECLUSTERBUILD'=>USECLUSTERBUILD,
            'site_open' => $bOpenSite,
        );

        foreach($aItems['items'] as $aItem)
            $aData['redirectItems'][] = $aItem;

        foreach($aDomainItems as $aItem)
            $aData['redirectDomainItems'][] = $aItem;

        /* rewrite htaccess */

        $oUpHalper->updateHtaccess(BUILDPATH.'common/templates/',$aData);

        return true;
    }
}
