<?php
/**
 * @class Redirect301ToolModule
 * @extends skModule
 * @project Skewer
 * @package kernel
 *
 * @author kolesnikov, $Author: acat $
 * @version $Revision: 1672 $
 * @date $Date: 2013-02-06 15:59:05 +0400 (Ср, 06 фев 2013) $
 *
 */
class Redirect301ToolModule extends AdminToolTabModulePrototype {

    /**
     * @var string - Имя модуля
     */
    protected $sTabName = 'Управление редиректами 301';

    protected function preExecute() {
        CurrentAdmin::testControlPanelAccess();
    }

    protected function actionInit() {

        $this->actionList();

    }

    /**
     * Список опросов
     */
    protected function actionList() {

        // установка заголовка
        $this->setPanelName( 'Список адресов' );

        // объект для построения списка
        $oList = new ExtList();

        $aModel = Redirect301ToolApi::getListModel(Redirect301ToolApi::getListFields());

        $aModel['id']['listColumns'] = array('hidden' => true);
        $aModel['old_url']['listColumns'] = array('flex' => 1);
        $aModel['new_url']['listColumns'] = array('flex' => 1);

        // задать модель данных для вывода
        $oList->setFields( $aModel );

        // добавление набора данных
        $aItems = Redirect301ToolApi::getListItems();

        $oList->setValues( $aItems['items'] );

        $oList->addRowBtn(array(
            'tooltip' =>'_upd',
            'iconCls' => 'icon-edit',
            'action' => 'editForm',
            'state' => 'show'
        ));
        $oList->addRowBtnDelete();

        // кнопка добавления
        //$oList->addBntAdd('add');
        $oList->addDockedItem(array(
            'text' => 'Добавить',
            'iconCls' => 'icon-add',
            'state' => 'init',
            'action' => 'addForm',
        ));

        // вывод данных в интерфейс
        $this->setExtInterface( $oList );

    }

    public function actionAddForm(){

        $oForm = new ExtForm();

        $this->setPanelName('Добавление нового редиректа',true);

        /* установить набор элементов формы */
        $aItems = Redirect301ToolApi::getListModel(Redirect301ToolApi::getListFields());

        $oForm->setFields($aItems);  //setItems
        $oForm->setValues(array());

        $oForm->addDockedItem(array(
            'text' => 'Сохранить',
            'iconCls' => 'icon-save',
            'state' => 'allow_do',
            'action' => 'add',
            'actionText' => 'Добавить новое правило?',
        ));

        $oForm->addBntCancel('list');

        $this->setExtInterface($oForm);

        return psComplete;

    }

    public function actionAdd(){

        try {

            $aData = $this->get('data');

            //$bRes = DomainManagerApi::linkDomain($iSiteId,$iDomainId);

            $bRes = Redirect301ToolMapper::saveItem($aData);

            if(!$bRes) throw new Exception('Ошибка: правило не добавлено!');

            $bRes = Redirect301ToolApi::makeHtaccessFile();

            if(!$bRes) throw new Exception('Ошибка при редактировании .htaccess!');

        } catch(Exception $e) {
            $this->addError($e->getMessage());
        }

        // переход к списку
        $this->actionList();

        return psComplete;

    }

    public function actionDelete(){

        try {

            $aData = $this->get('data');

            $bRes = Redirect301ToolMapper::delItem($aData['id']);

            if(!$bRes) throw new Exception('Ошибка: не удалось удалить правило!');

            $bRes = Redirect301ToolApi::makeHtaccessFile();

            if(!$bRes) throw new Exception('Ошибка при редактировании .htaccess!');

        } catch(Exception $e) {
            $this->addError($e->getMessage());
        }


        // переход к списку
        $this->actionList();

        return psComplete;

    }

    public function actionEditForm(){

        $oForm = new ExtForm();

        $aData = $this->get('data');

        $this->setPanelName('Редактирование правила',true);

        /* установить набор элементов формы */
        $aItems = Redirect301ToolApi::getListModel(Redirect301ToolApi::getListFields());

        $oForm->setFields($aItems);
        $oForm->setValues($aData);

        $oForm->addDockedItem(array(
            'text' => 'Сохранить',
            'iconCls' => 'icon-save',
            'state' => 'allow_do',
            'action' => 'update',
            'actionText' => 'Сохранить изменения в правиле?',
        ));

        $oForm->addBntCancel('list');

        $this->setExtInterface($oForm);

        return psComplete;

    }

    public function actionUpdate(){

        try {

            $aData = $this->get('data');

            $bRes = Redirect301ToolMapper::saveItem($aData);

            if(!$bRes) throw new Exception('Ошибка: шаблон не изменен!');

            $bRes = Redirect301ToolApi::makeHtaccessFile();

            if(!$bRes) throw new Exception('Ошибка при редактировании .htaccess!');

        } catch(Exception $e) {
            $this->addError($e->getMessage());
        }

        // переход к списку
        $this->actionList();

        return psComplete;

    }

}
