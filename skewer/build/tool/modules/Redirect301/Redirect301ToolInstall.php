<?php

/**
 * @class Redirect301ToolInstall
 * @extends skModule
 * @project Skewer
 * @package adm
 *
 * @author kolesnikov, $Author: sapozhkov $
 * @version $Revision: 1316 $
 * @date $Date: 2012-11-27 18:31:02 +0400 (Вт., 27 нояб. 2012) $
 *
 */

class Redirect301ToolInstall extends skModuleInstall {

    public function init() {
        return true;
    }// func

    public function install() {
        return true;
    }// func

    public function deinstall() {
        return true;
    }// func

}//class