<?php

/**
 * @class UsersToolModule
 * @extends skModule
 * @project Skewer
 * @package kernel
 *
 * @author Andy Mitrich, $Author: acat $
 * @version $Revision: 1475 $
 * @date 15.12.11 18:21 $
 *
 */

class UsersToolModule extends AdminToolTabModulePrototype{

    // id текущего раздела
    protected $iSectionId = 0;

    // число элементов на страницу
    protected $iOnPage = 20;

    // фильтр по политике
    protected $mPolicyFilter = false;

    // фильтр по активности пользователей
    protected $mActiveFilter = false;

    // фильтр по тексту
    protected $sSearchFilter = '';

    // текущий номер страницы
    protected $iPage = 0;

    /**
     * @var string - Имя модуля
     */
    protected $sTabName = 'Пользователи';

    /**
     * Метод, выполняемый перед action меодом
     * @throws ModuleAdminErrorException
     * @return bool
     */
    protected function preExecute() {

        CurrentAdmin::testControlPanelAccess();

        // номер страницы
        $this->iPage = $this->getInt('page');

        // id текущего раздела
        $this->iSectionId = $this->getInt('sectionId');

        // фильтры
        $this->mPolicyFilter = $this->get('policy',false);
        $this->mActiveFilter = $this->get('active',false);
        $this->sSearchFilter = $this->getStr('search');

        // если не системный админ и доступ запрещен
        if ( !CurrentAdmin::isSystemMode() and !CurrentAdmin::canDo('PolicyToolModule', 'allowUsersView') )
            throw new ModuleAdminErrorException( 'accessDenied' );

    }


    /**
     * Первичное состояние
     */
    protected function actionInit() {

        // вывод списка
        $this->actionList();

    }

    protected function setListItems( ExtList &$oList ) {

        // число записей на страницу
        $oList->setOnPage( $this->iOnPage );
        $oList->setPageNum( $this->iPage );

        // добавление набора данных
        $aFilter = array(
            'limit' => $this->iOnPage ? array (
                'start' => $this->iPage*$this->iOnPage,
                'count' => $this->iOnPage
            ) : false,

            /*Сортировка только по одному полю */
            'order' => array(
                'field' => 'login',
                'direction' => 'ASC',
            )

        );

        /*
         * Фильтры
         */

        // по политике
        if ( false !== $this->mPolicyFilter ) {
            $aFilter['policy'] = (int)$this->mPolicyFilter;
        }

        // по активновти
        if ( false !== $this->mActiveFilter ) {
            $aFilter['active'] = (int)$this->mActiveFilter;
        }

        // по тексту
        if ( $this->sSearchFilter ) {
            $aFilter['search'] = $this->sSearchFilter;
        }

        // только пользователи с паролем для не системных администраторов
        if(!CurrentAdmin::isSystemMode())
            $aFilter['has_pass'] = true;

        // запросить всех пользователей
        $aItems = UsersToolApi::getUsersList($aFilter);


        // форматирование данных
        foreach ( $aItems['items'] as $iKey => $aRow ) {
            // дата последнего захода
            if ( $aRow['lastlogin'] <= 1900 )
                $aItems['items'][$iKey]['lastlogin'] = '-';
        }

        $oList->setValues( $aItems['items'] );
        $oList->setTotal( $aItems['count'] );

    }

    /**
     * Список пользователей
     */
    protected function actionList() {

        $this->setPanelName( 'Список пользователей' );

        // объект для построения списка
        $oList = new ExtList();

        /**
         * Модель данных
         */

        $aFieldFilter = UsersToolMapper::getListFields();

        $aListModel = UsersToolApi::getParamList($aFieldFilter);

        // задать модель данных для вывода
        $oList->setFields( $aListModel );

        /**
         * Фильтры
         */

        // текстовый фильтр
        $oList->addFilterText( 'search', $this->sSearchFilter );

        // добавляем фильтр по политикам
        $oList->addFilterSelect( 'policy', UsersToolApi::getAllowedPolicyList(), $this->mPolicyFilter, 'Политика' );
        //$oList->addToolbarFilter( 'policy', UsersToolApi::getAllowedPolicyList(), $this->mPolicyFilter, 'Политика' );

        // Активность
        $oList->addFilterSelect( 'active', array(
            1=>'Активен',
            0=>'Отключен'
        ), $this->mActiveFilter, 'Активность' );

        /**
         * Данные
         */
        $this->setListItems( $oList );

        /**
         * Интерфейс
         */

        // добавление кнопок
        $oList->addRowBtnUpdate();

        // кнопка добавления
        $oList->addBntAdd('show');

        // вывод данных в интерфейс
        $this->setExtInterface( $oList );
    }

    /**
     * Отображение формы
     * @throws Exception
     */
    protected function actionShow() {

        // взять id ( 0 - добавление, иначе сохранение )
        $iItemId = (int)$this->getInDataVal('id');

        // заголовок
        $this->setPanelName( $iItemId ? 'Редактирование' : 'Добавление' );

        // запись новости
        $aItem = $iItemId ? UsersToolApi::getUser($iItemId) : UsersToolApi::getBlankValues();

        // если нет требуемой записи
        if ( $iItemId and !$aItem )
            throw new Exception('Ошибка выбора записи');

        // есть id - должны быть и права на доступ
        $iItemId and UsersToolApi::testAccessToUser($iItemId);

        // подключить автоматический генератор форм
        $oForm = new ExtForm();

        // установить набор элементов формы
        $aFieldFilter = UsersToolMapper::getDetailFields( $iItemId );

        if ( $iItemId )
            $aFieldFilter['group_policy_id']['disabled'] = 'true';

        $oForm->setFields( $aFieldFilter );
        
        // установить значения для элементов
        $oForm->setValues( $aItem );

        // добавление кнопок
        $oForm->addBntSave();
        // редактирование пароля, если задан пользователь и он не тукущий системный
        if ( $iItemId and !UsersToolApi::isCurrentSystemUser($iItemId) ) {
            $oForm->addDockedItem(array(
                'text' => 'Пароль',
                'iconCls' => 'icon-edit',
                'action' => 'pass'
            ));
        }
        $oForm->addBntCancel();

        if ( $iItemId && $iItemId!=CurrentAdmin::getId()) {
            $oForm->addBntSeparator('->');
            $oForm->addBntDelete();
        }

        // вывод данных в интерфейс
        $this->setExtInterface( $oForm );
    }

    /**
     * Отображение формы
     * @throws Exception
     */
    protected function actionPass() {

        // номер записи
        $iItemId = (int)$this->getInDataVal('id');

        // id - обязательное поле
        if ( !$iItemId )
            throw new Exception('нет id');

        // текущему системному пользователю нельзя изменять логин и пароль
        if ( UsersToolApi::isCurrentSystemUser($iItemId) )
            throw new Exception('текущему системному пользователю нельзя изменять логин и пароль');

        // запись пользователя
        $aItem = AuthUsersMapper::getUserData( $iItemId, array('id','login') );

        if ( !$aItem )
            throw new Exception('Запись не найдена');

        // должны быть права на доступ
        UsersToolApi::testAccessToUser($iItemId);

        // заголовок панели
        $this->setPanelName( 'Изменение пароля' );

        // подключить автоматический генератор форм
        $oForm = new ExtForm();

        // установить набор элементов формы

        $aFieldFilter = UsersToolMapper::getFullParamDefList(array('id','login','pass','pass2'));
        $oForm->setFields( $aFieldFilter );

        // установить значения для элементов
        $oForm->setValues( $aItem );

        // добавление кнопок
        $oForm->addBntSave('savePass');
        $oForm->addBntCancel('show');

        // вывод данных в интерфейс
        $this->setExtInterface( $oForm );
    }

    /**
     * Сохранение новости
     * @throws Exception
     */
    protected function actionSave() {

        // номер записи
        $iItemId = (int)$this->getInDataVal('id');

        // поля для записи
        $aFields = $iItemId ? UsersToolMapper::getSaveFields() : UsersToolMapper::getAddFormFields();

        // взять данные
        $aData = $this->getInData( $aFields );

        if ( !$aData )
            throw new Exception('Нет данных для сохранения');

        // если добавление
        if ( !$iItemId ) {

            // проверить заданность login и pass
            $sLogin = $aData['login'];
            $sPass = $aData['pass'];

            if ( !skValidator::isLogin($sLogin)) throw new Exception('Логин задан неккоректно!');

            // проверка доступности логина
            if ( !UsersToolApi::loginIsFree( $sLogin ) )
                throw new Exception('Пользователь с таким логином уже существует');

            if ( !$sPass) throw new Exception('Не задан пароль');

            // проверить соответствие поддтверждения пароля
            if ( $aData['pass']!==$aData['pass2'] )
                throw new Exception('Пароли не совпадают');

            $aData['pass'] = Auth::buildPassword($sLogin,$sPass);

        } else {

            // обновление

            if ( !$aData['group_policy_id'] ) unset($aData['group_policy_id']);
            // должны быть права на доступ
            UsersToolApi::testAccessToUser($iItemId);

            // с себя активность снять нельзя
            if ( UsersToolApi::isCurrentUser($iItemId) and !$aData['active'] ) {
                $aData['active'] = 1;
                $this->addError('Нельзя снять активность с текущего пользователя');
            }

            // с default активность нельзя снять
            $aCurUserData = UsersToolApi::getUser($iItemId);
            if ( !CurrentAdmin::isSystemMode() and $aCurUserData['login'] == 'default' and !$aData['active'] ) {
                $aData['active'] = 1;
                $this->addError('Нельзя снять активность с default пользователя');
            }

        }

        // есть данные - сохранить
        $bRes = UsersToolApi::updUser( $aData );

        if ( $iItemId ) {
            if ( $bRes ) {
                $this->addMessage('Данные сохранены');
                $this->addModuleNoticeReport("Редактирование записи пользователя",$aData);
            } else {
                $this->addError('Данные НЕ изменены');
            }
    }   else {
            if ( $bRes ) {
                $this->addMessage('Пользователь добавлен');
                unset($aData['id']);
                unset($aData['pass']);
                unset($aData['pass2']);
                $this->addModuleNoticeReport("Создание нового пользователя",$aData);
            } else {
                $this->addError('Пользователь НЕ добавлен');
            }
        }

        // вывод списка
        $this->actionList();

    }

    /**
     * Сохранение пароля
     * @throws Exception
     * @throws ModuleAdminErrorException
     */
    protected function actionSavePass(){

        // запросить данные
        $aData = $this->get( 'data' );
        if ( !is_array($aData) )
            throw new ModuleAdminErrorException('wrong input data');

        // взять данные
        $iId = (int)(isset($aData['id']) ? $aData['id'] : 0);
        $sLogin = (string)(isset($aData['login']) ? $aData['login'] : '');
        $sPass1 = (string)(isset($aData['pass']) ? $aData['pass'] : '');
        $sPass2 = (string)(isset($aData['pass2']) ? $aData['pass2'] : '');

        // проверка наличия полей
        if ( !$iId ) throw new Exception('no `id`');

        // текущему системному пользователю нельзя изменять логин и пароль
        if ( UsersToolApi::isCurrentSystemUser($iId) )
            throw new Exception('текущему системному пользователю нельзя изменять логин и пароль');

        // проверка прав на доступ
        UsersToolApi::testAccessToUser($iId);

        // логин - обязательное поле
        if ( !$sLogin ) throw new Exception('Не задан login');

        // ошибка, если логин занят и не принадлежит изменяемому пользоватлю
        $iLoginId = UsersToolApi::getIdByLogin( $sLogin );
        if ( $iLoginId and $iLoginId !== $iId )
            throw new Exception('Пользователь с таким логином уже существует');

        // пароль - обязательное поле
        if ( !$sPass1 ) throw new Exception('Не задан пароль');

        // проверка правильности пароля
        if ( $sPass1!==$sPass2 )
            throw new Exception('Пароли не совпадают');

        // сохранить
        $aSaveArr = array(
            'id' => $iId,
            'login' => $sLogin,
            'pass' => Auth::buildPassword($sLogin,$sPass1)
        );
        $bRes = UsersToolApi::updUser( $aSaveArr );

        // выдать сообщение
        if ( $bRes ) {
            $this->addMessage('Пароль сохранен');
            unset($aSaveArr['pass']);
            $this->addModuleNoticeReport("Редактирование пароля",$aSaveArr);
        } else {
            $this->addError('Пароль НЕ изменен. Возможно он совпал со старым паролем - выберите другой пароль.');
        }

        // вывод списка
        $this->actionShow();

    }

    /**
     * Удаляет запись
     */
    protected function actionDelete() {

        // запросить данные
        $aData = $this->get( 'data' );

        // id записи
        $iItemId = ( is_array($aData) and isset($aData['id']) ) ? (int)$aData['id'] : 0;

        // проверка прав на доступ
        UsersToolApi::testAccessToUser($iItemId);

        // запросить данные пользователя
        $aUser = AuthUsersMapper::getUserData( $iItemId, array('id','login','name','email') );

        // удаление
        $bRes = UsersToolApi::delUser( $iItemId );

        if ( $bRes ) {
            Policy::incPolicyVersion();
            $this->addMessage('Пользователь удален');
            $this->addModuleNoticeReport("Удаление пользователя",$aUser);
        } else {
            $this->addError('Пользователь НЕ удален');
        }

        // вывод списка
        $this->actionList();

    }

    /**
     * Установка служебных данных
     * @param \ExtPrototype $oExtIface
     */
    protected function setServiceData( ExtPrototype $oExtIface ) {

        // установить заголовок модуля
        $oExtIface->setTitle( $this->sTabName );

        // установить данные для передачи интерфейсу
        $oExtIface->setServiceData( array(
            'sectionId' => $this->iSectionId,
            'policy' => $this->mPolicyFilter,
            'active' => $this->mActiveFilter,
            'page' => $this->iPage
        ) );

    }

}
