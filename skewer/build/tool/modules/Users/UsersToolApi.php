<?php

/**
 * @class UsersToolApi
 * @extends skModule
 * @project Skewer
 * @package kernel
 *
 * @author Andy Mitrich, $Author: sapozhkov $
 * @version $Revision: 1316 $
 * @date 15.12.11 18:25 $
 *
 */

class UsersToolApi {

    /**
     * Отдает набор полей с описаниями
     * @param array $aFieldFilter - массив с набором колонок
     * @return array
     */
    public static function getParamList( $aFieldFilter ) {

        return UsersToolMapper::getFullParamDefList( $aFieldFilter );

    }

    public static function getUsersList($aFilter){

        return AuthUsersMapper::getUsersList($aFilter);
    }

    public static function getUser($iUserId){

        return AuthUsersMapper::getUserDetail($iUserId);
    }

    public static function updUser( $aFilter ){
        $mRes = AuthUsersMapper::saveItem($aFilter);
        Policy::incPolicyVersion();
        return $mRes;
    }

    public static function delUser($iUserId){

        return AuthUsersMapper::delUser($iUserId);
    }

    /**
     * Возвращает набор доступных текущему пользователю политик для создания пользователей
     * @static
     * @return array
     */
    public static function getAllowedPolicyList(){

        $aOut = array();

        $aPolicyList = Policy::getPolicyList();

        foreach ( $aPolicyList['items'] as $aPolicy ) {
            /* Пропускаем политику системного администратора */
            if ( $aPolicy['alias']=='sysadmin' ) continue;
            if ( $aPolicy['alias']=='admin' && !CurrentAdmin::isAdminPolicy() ) continue;

            if ( $aPolicy['alias']!='default' )
                $aOut[ $aPolicy['id'] ] = $aPolicy['title'];
        }

        return $aOut;

    }

    /**
     * Возвращает id политики для создания пользователя по умолчанию
     * @static
     * @return int
     */
    public static function getDefaultPolicyId(){

        // берем политиу оператора по умолчанию
        $aFilter = array(
            'select_fields' => array(
                'id',
                'title',
                'alias',
                'active'
            ),
            'where_condition' => array(
                'alias' => array(
                    'sign' => 'LIKE',
                    'value' => 'operator'
                )
            ),
            'limit' => array(
                'start'=>0,
                'count'=>1
            )
        );

        $aPolicyList = Policy::getPolicyList( $aFilter );

        if ( $aPolicyList['count'] )
            return (int)$aPolicyList['items'][0]['id'];
        else
            return 0;

    }

    /**
     * Отдает шаблонный набор значений для добавления новой записи
     * @return array
     */
    public static function getBlankValues() {
        return array(
            'name' => 'Новый пользователь',
            'active' => 1,
            'group_policy_id' => static::getDefaultPolicyId()
        );
    }

    /**
     * Проверяет, есть ли доступ к заданному пользователю у текущего
     * @static
     * @param int $iUserId - id целевого пользователя
     * @return bool
     */
    public static function hasAccessToUser( $iUserId ){

        // взять запись
        $aUser = AuthUsersMapper::getItem( $iUserId );

        // нет записи - нет и прав
        if ( !$aUser ) return false;

        // должен быть pass
        if ( !$aUser['pass'] )
            return false;

        // прошел проверки, значит можно
        return true;

    }

    /**
     * Проверяет доступ к записи и, если нету, выбрасывает исключение
     * @static
     * @param $iUserId
     * @throws Exception
     */
    public static function testAccessToUser( $iUserId ) {
        // системному админу можно все
        if ( CurrentAdmin::isSystemMode() )
            return;
        if ( !self::hasAccessToUser($iUserId) )
            throw new Exception('Нет доступа к записи');
    }

    /** Проверяет, является ли заданный id пользователя собственным
     * @static
     * @param int $iUserId - id проверяемого пользователя
     * @return bool
     */
    public static function isCurrentUser( $iUserId ){
        return (int)$iUserId === (int)CurrentAdmin::getId();
    }

    /**
     * Проверяет, является ли заданный id пользователя собственным и при этом системным
     * @static
     * @param int $iUserId - id проверяемого пользователя
     * @return bool
     */
    public static function isCurrentSystemUser( $iUserId ){
        return self::isCurrentUser($iUserId) and CurrentAdmin::isSystemMode();
    }

    /**
     * Взять id пользователя по логину
     * @static
     * @param string $sLogin
     * @return int
     */
    public static function getIdByLogin( $sLogin ) {

        // фильтр на поиск
        $aFilter = array();
        $aFilter['where_condition']['login'] = array(
            'sign' => '=',
            'value' => $sLogin
        );

        // запрос данных
        $aUser = AuthUsersMapper::getItem( $aFilter );

        return $aUser ? (int)$aUser['id'] : 0;

    }

    /**
     * Проверяет, свободен ли логин
     * @static
     * @param string $sLogin - проверяемое значение имени пользователя
     * @return bool
     */
    public static function loginIsFree($sLogin){

        // свободно, если такого пользователя нет
        return !(bool)self::getIdByLogin($sLogin);

    }

}
