<?php

/* main */
$aConfig['name']     = 'UsersTool';
$aConfig['title']    = 'Пользователи (админ)';
$aConfig['version']  = '1.000a';
$aConfig['description']  = 'Админ-интерфейс управления пользовательскими аккаунтами';
$aConfig['revision'] = '0001';
$aConfig['layer']     = 'tool';

return $aConfig;
