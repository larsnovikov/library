<?php
/**
 * Маппер для админской части модуля работы с пользователями
 *
 * @class: UsersToolMapper
 *
 * @Author: User, $Author: sapozhkov $
 * @version: $Revision: 1316 $
 * @date: $Date: 2012-11-27 18:31:02 +0400 (Вт., 27 нояб. 2012) $
 *
 */

class UsersToolMapper extends AuthUsersMapper {

    /**
     * Дабор полей для списка
     * @static
     * @return array
     */
    public static function getListFields(){
        return array('id',
            'login',
            'name',
            'email',
            'policy',
            'active',
            'lastlogin'
        );
    }

    /**
     * Набор полей для детального отображения
     * @static
     * @return array
     */
    public static function getShowFormFields(){
        return array(
            'id',
            'login',
            'group_policy_id',
            'name',
            'email',
            'active',
            'lastlogin'
        );
    }

    /**
     * Набор полей для сохранения существующей записи
     * @static
     * @return array
     */
    public static function getSaveFields(){
        return array_diff( static::getShowFormFields(), array('login','lastlogin') );
    }

    /**
     * Набор полей для формы добавления
     * @static
     * @return array
     */
    public static function getAddFormFields(){
        return array(
            'id',
            'login',
            'pass',
            'pass2',
            'group_policy_id',
            'name',
            'email',
            'active',
        );
    }

    // дополнительный набор параметров
    protected static function getAddParamList() {
        return array(
            'id' => array(
                'listColumns' => array(
                    'hidden' => true
                )
            ),
            'login' => array(
                'allowBlank' => false
            ),
            'name' => array(
                'listColumns' => array('flex' => 1),
            ),
            'email' => array(
                'listColumns' => array('flex' => 1),
            ),
            'active' => array(
                'listColumns' => array('width' => 50),
            ),
            'lastlogin' => array(
                'listColumns' => array('width' => 120),
            ),
            'pass' => array(
                'allowBlank' => false
            ),
            'pass2' => array(
                'allowBlank' => false,
                'view' => 'pass',
                'title' => 'Подтверждение пароля'
            ),
            'policy' => array(
                'view' => 'str',
                'listColumns' => array(
                    'flex' => 1
                ),
                'title' => 'Политика доступа'
            ),
            'group_policy_id' => ExtForm::getDesc4SelectFromArray( UsersToolApi::getAllowedPolicyList() )
        );
    }

    /**
     * Набор полей для добавления записи
     * @static
     * @return array
     */
    public static function getAddFields(){
        return static::getAddFormFields();
    }

    /**
     * Отдает описание полей формы при добавлении и редактировании
     * @param mixed $mExists - флаг существования записи ( false - новая )
     * @return array
     */
    public static function getDetailFields( $mExists ){

        // если редактирование
        if ( $mExists ) {

            // описание полей
            $aRows = static::getFullParamDefList(static::getShowFormFields());

            // логин и дату последнего захода сделать нередактируемыми
            $aRows['login']['view'] = 'show';
            $aRows['lastlogin']['view'] = 'show';

        } else {

            // добавление новой записи

            // описание полей
            $aRows = static::getFullParamDefList(static::getAddFormFields());

            if ( !CurrentAdmin::isSystemMode() ){
                $iSize = sizeof($aRows['group_policy_id']['store']['data']);
                foreach( $aRows['group_policy_id']['store']['data'] as $iKey=>$aRow ) {

                    /**
                     * $aRow['v']==1 - равенство означает, что политика доступа админа
                     */
                    if ( $aRow['v']==1 ) $aRows['group_policy_id']['store']['data'] = array_slice($aRows['group_policy_id']['store']['data'], $iKey+1, $iSize);
                }
            }
        }

        return $aRows;
    }

}
