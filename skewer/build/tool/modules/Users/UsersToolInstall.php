<?php

/**
 * @class UsersToolInstall
 * @extends skModule
 * @project Skewer
 * @package tool
 *
 * @author Andy Mitrich, $Author: sapozhkov $
 * @version $Revision: 1316 $
 * @date 07.02.12 17:51 $
 *
 */

class UsersToolInstall extends skModuleInstall {

    public function init() {
        return true;
    }// func

    public function install() {
        return true;
    }// func

    public function deinstall() {
        return true;
    }// func

}//class