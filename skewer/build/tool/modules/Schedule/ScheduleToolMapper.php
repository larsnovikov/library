<?php
/**
 * Класс для работы с таблицей расписаний
 *
 * @class: ScheduleMapper
 *
 * @author sapozhkov
 * @version 0.8
 *
 */

class ScheduleToolMapper extends skMapperPrototype {

    /** @var string Имя таблицы, с которой работает маппер */
    protected static $sCurrentTable = 'schedule';

    /** @var array Конфигурация полей таблицы */
    protected static $aParametersList = array(
        'id' => 'i:hide:ID задания',
        'title' => 's:str:Название задания:Новая задача',
        'name' => 's:str:Имя задания',
        'command' => 's:str:Команда для старта',
        'priority' => 'i:int:Приоритет',
        'resource_use' => 'i:int:Ресурсоемкость',
        'target_area' => 'i:int:Область применения',
        'status' => 'i:int:Статус',
        'c_min' => 'i:int:Минута',
        'c_hour' => 'i:int:Час',
        'c_day' => 'i:int:День',
        'c_month' => 'i:int:Месяц',
        'c_dow' => 'i:int:День недели',
    );

    /**
     * Отдает все записи
     * @static
     * @return array
     */
    public static function getAllItems(){

        // формирование критериев выборки
        $aFilter = array(
            'order' => array(
                'field' => 'title',
                'way' => 'ASC'
            )
        );

        // запрос данных
        $aItems = static::getItems( $aFilter );

        return $aItems['items'];

    }

    /**
     * получение ид задания в рассписании по имени
     * @static
     * @param $name
     * @return bool
     */
    public static function getIdByName($name){


        $sQuery = "SELECT id FROM `[table:q]` WHERE `name`=[name:s]";

        $aData = array(
            'table' => 'schedule',
            'name' => $name,
        );

        global $odb;
        $rResult = $odb->query($sQuery,$aData);

        if ( !$rResult ) return false;

        if( $aRow = $rResult->fetch_assoc() ){

            $sResult = $aRow['id'];

            return $sResult;

        }

        return false;


    }

}
