<?php
/**
 * API работы с планировщиком задач
 *
 * @class: ScheduleToolModule
 *
 * @author sapozhkov
 * @version $Revision: 1316 $
 * @date $Date: 2012-11-27 18:31:02 +0400 (Вт., 27 нояб. 2012) $
 *
 */

class ScheduleToolApi {

    /**
     * Набор полей для списка
     * @static
     * @return array
     */
    public static function getListFields(){
        return array('id','title','c_min','c_hour','c_day','c_month','c_dow','priority','resource_use','target_area','status');
    }

    /**
     * дополнительный набор параметров
     * @static
     * @return array
     */
    protected static function getAddParamList() {
        return array(
            'id' => array(
                'listColumns' => array(
                    'hidden' => true
                )
            ),
            'title' => array(
                'allowBlank' => false,
                'listColumns' => array('flex' => 1),
            ),

            'priority' => new SchedulePriorityField(),
            'resource_use' => new ScheduleResourceField(),
            'target_area' => new ScheduleTargetField(),
            'status' => new ScheduleStatusField(),
            'c_min' => new ScheduleMinuteField(),
            'c_hour' => new ScheduleHourField(),
            'c_day' => new ScheduleDayField(),
            'c_month' => new ScheduleMonthField(),
            'c_dow' => new ScheduleDOWField(),

        );

    }

    /**
     * Отдает модель данных для списка
     * @return array
     */
    public static function getListModel() {

        // набор полей для списка
        $aFieldFilter = static::getListFields();

        // описания полей
        $aParamList = Ext::getParamFields(
            ScheduleToolMapper::getFullParamDefList($aFieldFilter),
            static::getAddParamList()
        );

        // описания полей
        return $aParamList;

    }

    /**
     * Отдает модель данных для формы редактирования
     * @return array
     */
    public static function getEditModel() {

        // взять все поля
        $aFieldFilter = ScheduleToolMapper::getParamNames();

        // описания полей
        $aParamList = Ext::getParamFields(
            ScheduleToolMapper::getParamDefList($aFieldFilter),
            static::getAddParamList()
        );

        return $aParamList;

    }


}
