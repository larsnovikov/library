<?php
/**
 * Интерфейс для работы с планировщиком задач
 *
 * @class: ScheduleToolModule
 *
 * @author sapozhkov
 * @version $Revision: 1316 $
 * @date $Date: 2012-11-27 18:31:02 +0400 (Вт., 27 нояб. 2012) $
 */
class ScheduleToolModule extends AdminToolTabModulePrototype {

    /** @var string - Имя модуля */
    protected $sTabName = 'Планировщик задач';

    protected function preExecute(){
        CurrentAdmin::testControlPanelAccess();
        require_once('ScheduleFields.php');
    }

    /**
     * Первичное состояние
     */
    protected function actionInit() {

        // вывод списка
        $this->actionList();

    }

    /**
     * Список пользователей
     */
    protected function actionList() {

        // установка заголовка
        $this->setPanelName( 'Список задач' );

        // объект для построения списка
        $oList = new ExtList();

        // Модель данных
        $oList->setFields( ScheduleToolApi::getListModel() );

        // Данные
        $oList->setValues( ScheduleToolMapper::getAllItems() );

        // добавление кнопоки обновления
        $oList->addRowBtnUpdate();

        // кнопка добавления
        $oList->addBntAdd('show');

        // вывод данных в интерфейс
        $this->setExtInterface( $oList );

    }

    /**
     * Отображение формы
     */
    protected function actionShow() {

        // взять id ( 0 - добавление, иначе сохранение )
        $iItemId = (int)$this->getInDataVal('id');

        // заголовок
        $this->setPanelName( $iItemId ? 'Редактирование' : 'Добавление' );

        // элемент
        $aItem = $iItemId ? ScheduleToolMapper::getItem($iItemId) : array();

        // если нет требуемой записи
        if ( $iItemId and !$aItem )
            throw new Exception('Ошибка выбора записи');

        // подключить автоматический генератор форм
        $oForm = new ExtForm();

        // модель данных формы
        $oForm->setFields( ScheduleToolApi::getEditModel() );

        // установить значения для элементов
        if ( $iItemId )
            $oForm->setValues( $aItem );
        else
            $oForm->setDefaultValues();

        // добавление кнопок
        $oForm->addBntSave();
        $oForm->addBntCancel();

        // кнопка удаления для заданного объекта
        if ( $iItemId ) {
            $oForm->addBntSeparator('->');
            $oForm->addBntDelete();
        }

        // вывод данных в интерфейс
        $this->setExtInterface( $oForm );
    }

    /**
     * Сохранение
     */
    protected function actionSave() {

        // запросить данные
        $aData = $this->get( 'data' );

        // подключить автоматический генератор форм
        $oValidator = new ExtValidator();

        // модель данных формы
        $oValidator->setFields( ScheduleToolApi::getEditModel() );

        // установить значения для элементов
        $oValidator->setValues( $aData );

        // валидация при сохранении
        if ( !$oValidator->validate($aData) )
            throw new Exception( sprintf( 'Ошибка валидации:<br />%s<br />', implode('<br />',$oValidator->getMessages()) ) );

        // преобразование сохраняемых данных объектами полей
        $aData = $oValidator->getSaveValues();

        // есть данные
        if ( $aData ){

            // сохранить
            ScheduleToolMapper::saveItem( $aData );

            // добавление записи в лог
            if ( isset($aData['id']) && $aData['id'] ) {
                $this->addMessage('Запись обновлена');
                $this->addModuleNoticeReport("Редактирование записи планировшика",$aData);
            } else {
                unset($aData['id']);
                $this->addMessage('Запись добавлена');
                $this->addModuleNoticeReport("Добавление записи планировшика",$aData);
            }
        }

        // вывод списка
        $this->actionInit();

    }


    /**
     * Удаление
     */
    protected function actionDelete() {

        // запросить данные
        $aData = $this->get( 'data' );

        // id записи
        $iItemId = ( is_array($aData) and isset($aData['id']) ) ? (int)$aData['id'] : 0;

        // удаление
        ScheduleToolMapper::delItem( $iItemId );

        // добавление записи в лог
        $this->addMessage('Запись удалена');
        $this->addModuleNoticeReport("Удаление записи планировшика",$aData);

        // вывод списка
        $this->actionInit();

    }

}
