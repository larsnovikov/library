<?php

/**
 * @class ScheduleToolInstall
 * @extends skModule
 * @project Skewer
 * @package adm
 *
 * @author Andy Mitrich, $Author: sapozhkov $
 * @version $Revision: 1316 $
 * @date 07.02.12 17:51 $
 *
 */

class ScheduleToolInstall extends skModuleInstall {

    public function init() {
        return true;
    }// func

    public function install() {
        return true;
    }// func

    public function deinstall() {
        return true;
    }// func

}//class