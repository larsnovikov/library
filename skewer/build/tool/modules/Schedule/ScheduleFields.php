<?php
/**
 * Набор классов для полей
 */

/**
 * Поле приоритет
 */
class SchedulePriorityField extends ExtFieldSelectByArray {

    /** @var array набор доступных значений */
    protected $aList = array(
        1 => 'низкий',
        2 => 'обычный',
        3 => 'высокий',
        4 => 'критический',
    );

    /** @var mixed стандартное значение */
    protected $mDefault = 2;

}

/**
 * Поле источник
 */
class ScheduleResourceField extends ExtFieldSelectByArray {

    /** @var array набор доступных значений */
    protected  $aList = array(
        3 => 'фоновая',
        4 => 'обычная',
        7 => 'ресурсоемкая',
        9 => 'критическая',
    );

    /** @var mixed стандартное значение */
    protected $mDefault = 2;

}

/**
 * Поле область применения
 */
class ScheduleTargetField extends ExtFieldSelectByArray {

    /** @var array набор доступных значений */
    protected  $aList = array(
        1 => 'площадка',
        2 => 'сервер',
        3 => 'система в целом',
    );

    /** @var mixed стандартное значение */
    protected $mDefault = 1;

}

/**
 * Поле статус
 */
class ScheduleStatusField extends ExtFieldSelectByArray {

    /** @var array набор доступных значений */
    protected $aList = array(
        1 => 'активна',
        2 => 'остановлена',
        3 => 'временно остановлена',
    );

    /** @var mixed стандартное значение */
    protected $mDefault = 1;

}

/**
 * Поле минуты
 */
class ScheduleMinuteField extends ExtFieldPrototype {

    /** @var int минимальное число */
    protected $iMin = 0;

    /** @var int максимальное число */
    protected $iMax = 59;

    /**
     * Значение по умолчанию
     * @return int
     */
    public function getDefaultVal() {
        return '';
    }

    /**
     * Проверяет доступность заданного значения
     * @return bool
     */
    function isValid() {
        $v = $this->getSaveValue();
        if ( is_null($v) ) {
            return true;
        } elseif ( is_numeric($v) ) {
            return ($v>=$this->iMin and $v<=$this->iMax );
        } else {
            return false;
        }
    }

    /**
     * Отдает текстовое название для установленного значения
     * @return string
     */
    function getText() {
        $mVal = $this->isValid() ? $this->getValue() : '-ошибка-';
        if ( is_null($mVal) ) $mVal = '*';
        return $mVal;
    }

    /**
     * Отдает расширяющее описание элемента для формы
     * @return array
     */
    function getFormFieldDesc() {
        return array(
            // пусто - значение снято
            'allowBlank' => true,
            // ограничения
            'maxValue' => $this->iMax,
            'minValue' => $this->iMin
        );

    }

    /**
     * Отдает расширяющее описание элемента для списка
     * @return array
     */
    function getListFieldDesc() {
        return array(
            'width' => 40,
            'align' => 'center',
            'text' => substr( $this->getName(), 2 )
        );
    }

    /**
     * Возвращает значение для сохранения
     * @return mixed
     */
    public function getSaveValue(){
        $mVal = $this->getValue();
        if ( $mVal === '' )
            $mVal = null;
        return $mVal;
    }

}

/**
 * Поле часы
 */
class ScheduleHourField extends ScheduleMinuteField {

    /** @var int максимальное число */
    protected $iMax = 23;

}

/**
 * Поле дни
 */
class ScheduleDayField extends ScheduleMinuteField {

    /** @var int минимальное число */
    protected $iMin = 1;

    /** @var int максимальное число */
    protected $iMax = 31;

}

/**
 * Поле "месяц"
 */
class ScheduleMonthField extends ScheduleMinuteField {

    /** @var int минимальное число */
    protected $iMin = 1;

    /** @var int максимальное число */
    protected $iMax = 12;

}

/**
 * Поле "день недели"
 */
class ScheduleDOWField extends ScheduleMinuteField {

    /** @var int минимальное число */
    protected $iMin = 1;

    /** @var int максимальное число */
    protected $iMax = 7;

}

