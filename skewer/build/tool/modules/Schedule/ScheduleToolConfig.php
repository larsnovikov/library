<?php

/* main */
$aConfig['name']     = 'ScheduleTool';
$aConfig['title']    = 'Планировщик задач';
$aConfig['version']  = '1.000a';
$aConfig['description']  = 'Интерфейс для управления расписанием задач';
$aConfig['revision'] = '0001';
$aConfig['layer']     = 'tool';

return $aConfig;
