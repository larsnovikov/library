<?php
/**
 * Интерфейс для работы с планировщиком задач
 *
 * @class: TasksManagerToolModule
 *
 * @author kolesnikov
 * @version $Revision: 1316 $
 * @date $Date: 2012-11-27 18:31:02 +0400 (Вт., 27 нояб. 2012) $
 */
class TasksManagerToolModule extends AdminToolTabModulePrototype {

    /** @var string - Имя модуля */
    protected $sTabName = 'Менеджер процессов';

    protected function preExecute(){
        CurrentAdmin::testControlPanelAccess();
    }

    /**
     * Первичное состояние
     */
    protected function actionInit() {

        // вывод списка
        $this->actionList();

    }

    /**
     * Список пользователей
     */
    protected function actionList() {

        // установка заголовка
        $this->setPanelName( 'Список процессов' );

        // объект для построения списка
        $oList = new ExtList();

        // Модель данных
        $aModel =  TasksManagerToolApi::getListModel( TasksManagerToolApi::getListFields() );
        //$aModel = array();
        $oList->setFields( $aModel );

        $aItems = TasksManagerToolApi::getListItems();

        // Данные
        $oList->setValues( $aItems['items'] );
        $oList->setTotal( $aItems['count'] );



        // добавление кнопоки обновления
        //$oList->addRowBtnUpdate();

        // кнопка добавления
        //$oList->addBntAdd('show');

        // вывод данных в интерфейс
        $this->setExtInterface( $oList );


    }



}
