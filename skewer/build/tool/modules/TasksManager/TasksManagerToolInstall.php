<?php

/**
 * @class TasksManagerInstall
 * @extends skModule
 * @project Skewer
 * @package tool
 *
 * @author kolesnikov, $Author: sapozhkov $
 * @version $Revision: 1316 $
 * @date 07.02.12 17:50 $
 *
 */

class TasksManagerToolInstall extends skModuleInstall {

    public function init() {
        return true;
    }// func

    public function install() {
        return true;
    }// func

    public function deinstall() {
        return true;
    }// func

}//class