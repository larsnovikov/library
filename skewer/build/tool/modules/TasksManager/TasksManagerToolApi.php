<?php
/**
 * API работы с менеджером процессов
 *
 * @class: TaskManagerToolApi
 *
 * @author kolesnikov
 * @version $Revision: 1316 $
 * @date $Date: 2012-11-27 18:31:02 +0400 (Вт., 27 нояб. 2012) $
 *
 */
class TasksManagerToolApi {

    /**
     * Набор полей для списка
     * @static
     * @return array
     */
    public static function getListFields(){
        return array('id','global_id','title','command','priority','resource_use','upd_time','mutex','status');
    }

    /**
     * @static
     * @param $aFields
     * @return array
     */
    public static function getListModel($aFields){

        return TaskMapper::getFullParamDefList($aFields);
    }

    public static function getListItems(){

        $aFilter = array('order' => array(
                            'field' => 'upd_time',
                            'way' => 'DESC'
                        ));

        $aItems = TaskMapper::getItems($aFilter);

        if($aItems['count'])
            foreach($aItems['items'] as $iKey=>$aItem){
                $aItems['items'][$iKey]['priority'] = Tasks::$tasksPriority[$aItem['priority']];
                $aItems['items'][$iKey]['resource_use'] = Tasks::$tasksWeight[$aItem['resource_use']];
                $aItems['items'][$iKey]['status'] = Tasks::$tasksStatus[$aItem['status']];

            }

        return $aItems;

    }

}
