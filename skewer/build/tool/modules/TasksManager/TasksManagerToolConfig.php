<?php

/* main */
$aConfig['name']     = 'TasksManagerTool';
$aConfig['title']    = 'Медеджер процессов';
$aConfig['version']  = '1.000a';
$aConfig['description']  = 'Админ-интерфейс управления процессами';
$aConfig['revision'] = '0001';
$aConfig['layer']     = 'tool';

return $aConfig;
