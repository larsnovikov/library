<?php
/**
 * @class SubscribeToolModule
 * @extends AdminModulePrototype
 * @project Skewer
 * @package kernel
 *
 * @author Andy Mitrich, kolesnikov, $Author: acat $
 * @version $Revision: 1672 $
 * @date 01.02.12 13:00 $
 *
 */

class SubscribeToolModule extends AdminToolTabModulePrototype {

    protected $sTabName = 'Рассылка';

    protected $iOnPage = 20;
    protected $iPage = 0;

    protected function preExecute() {

        $this->iPage = $this->getInt('page');

        CurrentAdmin::testControlPanelAccess();

    }

    public function actionInit(){

        $this->actionList();
    }

    /********************************** USER *********************************/

    public function actionUsers(){

        // объект для построения списка
        $oList = new ExtList();

        $oList->setAddText('<h1>Список подписчиков</h1>');

        // число записей на страницу
        $oList->setOnPage( $this->iOnPage );
        $oList->setPageNum( $this->iPage );
        $oList->setPageLoadActionName('users');

        $aModel = SubscribeToolApi::getListModel(SubscribeToolApi::getListFields());

        $aModel['email']['listColumns'] = array('flex' => 1);
        //$aModel['person']['listColumns'] = array('flex' => 1);
        //$aModel['city']['listColumns'] = array('flex' => 1);

        // задать модель данных для вывода
        $oList->setFields( $aModel );

        // добавление набора данных
        $aItems = SubscribeToolApi::getListItems($this->iPage, $this->iOnPage);

        $oList->setValues( $aItems['items'] );
        $oList->setTotal( $aItems['count'] );

        $oList->addRowBtnUpdate('editUser');
        $oList->addRowBtnDelete('delUser');

        // кнопка добавления
        $oList->addDockedItem(array(
            'text' => 'Подписчики',
            'state' => 'init',
            'action' => 'users',
        ));
        $oList->addDockedItem(array(
            'text' => 'Шаблоны',
            'state' => 'init',
            'action' => 'templates',//editMsg
        ));
        $oList->addDockedItem(array(
            'text' => 'Рассылки',
            'state' => 'init',
            'action' => 'list',//newSubscribe
        ));

        $oList->addBntSeparator();

        $oList->addDockedItem(array(
            'text' => 'Добавить подписчика',
            'iconCls' => 'icon-add',
            'state' => 'init',
            'action' => 'editUser',
        ));

        // вывод данных в интерфейс
        $this->setExtInterface( $oList );
    }

    public function actionEditUser() {

        $aData = $this->get('data');
        $iItemId = isSet($aData['id']) ? $aData['id'] : false;

        $oForm = new ExtForm();

        $oForm->setAddText('<h1>Редактирование подписчика</h1>');

        /* установить набор элементов формы */
        $aModel = Subscribe2UsersMapper::getFullParamDefList(Subscribe2UsersMapper::getModelFields());
        $aModel['email']['vtype'] = 'email';

        $aItems = array();
        if( $iItemId )
            $aItems = Subscribe2UsersMapper::getItem($iItemId);

        $oForm->setFields($aModel);  //setItems $aItems
        $oForm->setValues($aItems);

        $oForm->addBntSave( 'saveUser' );

        $oForm->addBntCancel('users');

        $this->setExtInterface($oForm);

        return psComplete;
    }

    public function actionSaveUser() {

        $aData = $this->get('data');

        try{
            // проверка входных данных
            if ( !isset($aData['email']) )
                throw new Exception('email field not found');

            $sEmail = $aData['email'];

            // валидация поля email
            if ( !$sEmail )
                throw new Exception('Не заполнено поле "E-mail"');

            if ( !filter_var( $sEmail, FILTER_VALIDATE_EMAIL ) )
                throw new Exception('Некорректно заполнено поле "E-mail"');

            Subscribe2UsersMapper::saveItem($aData);
            $this->addNoticeReport("Сохранение пользователя в рассылке", "email: $sEmail", skLogger::logUsers, "SubscribeToolModule");

        } catch ( Exception $e ) {
            $this->addError($e->getMessage());
        }
        $this->actionUsers();
    }

    public function actionDelUser() {

        $aData = $this->get('data');

        $iItemId = iSset($aData['id']) ? $aData['id'] : false;

        if( $iItemId )
            Subscribe2UsersMapper::delItem($iItemId);

        $this->actionUsers();
        $this->addNoticeReport("Пользователь удален из рассылки", "Пользователь: $aData[email]", skLogger::logUsers, "SubscribeToolModule");
    }

    /********************************** TEMPLATES *********************************/

    public function actionTemplates() {

        // объект для построения списка
        $oList = new ExtList();

        $oList->setAddText('<h1>Список шаблонов рассылок</h1>');

        $aModel = SubscribeToolApi::getTemplateListModel();

        $aModel['title']['listColumns'] = array('flex' => 1);

        // задать модель данных для вывода
        $oList->setFields( $aModel );

        // добавление набора данных
        $aItems = SubscribeToolApi::getTemplateList();
        $oList->setValues( $aItems['items'] );
        $oList->setTotal( $aItems['count'] );


        $oList->addRowBtnUpdate('editTemplate');
        $oList->addRowBtn(array(
            'tooltip' =>'_del',
            'iconCls' => 'icon-delete',
            'action' => 'delTemplate',
            'state' => 'allow_do',
            'actionText' => 'Вы действительно хотите <b>удалить</b> шаблон?'
        ));


        $oList->addDockedItem(array(
            'text' => 'Подписчики',
            'state' => 'init',
            'action' => 'users',
        ));
        $oList->addDockedItem(array(
            'text' => 'Шаблоны',
            'state' => 'init',
            'action' => 'templates',//editMsg
        ));
        $oList->addDockedItem(array(
            'text' => 'Рассылки',
            'state' => 'init',
            'action' => 'list',//newSubscribe
        ));

        $oList->addBntSeparator();

        $oList->addDockedItem(array(
            'text' => 'Добавить шаблон',
            'iconCls' => 'icon-add',
            'state' => 'init',
            'action' => 'editTemplate',
        ));

        // вывод данных в интерфейс
        $this->setExtInterface( $oList );

    }

    public function actionEditTemplate() {

        $aData = $this->get('data');
        $iItemId = isSet($aData['id']) ? $aData['id'] : false;

        $oForm = new ExtForm();

        $oForm->setAddText('<h1>Редактирование шаблона рассылок</h1>');

        /* установить набор элементов формы */
        $aModel = Subscribe2TemplateMapper::getFullParamDefList(Subscribe2TemplateMapper::getModelFields());
        $aModel['info'] = SubscribeToolApi::addTextInfoBlock();

        $aItems = array();
        if( $iItemId )
            $aItems = Subscribe2TemplateMapper::getItem($iItemId);

        $oForm->setFields($aModel);  //setItems $aItems
        $oForm->setValues($aItems);

        $oForm->addBntSave( 'saveTemplate' );
//        $oForm->addDockedItem(array(
//            'text' => 'Сохранить',
//            'iconCls' => 'icon-save',
//            'state' => 'saveTemplate',
//            'action' => 'saveTemplate',
//        ));

        $oForm->addBntCancel('templates');

        $this->setExtInterface($oForm);
        $this->addNoticeReport("Сохранение шаблона рассылок", "id $iItemId", skLogger::logUsers, "SubscribeToolModule");
        return psComplete;

    }

    public function actionSaveTemplate() {

        $aData = $this->get('data');

        Subscribe2TemplateMapper::saveItem($aData);

        $this->addNoticeReport("Сохранение шаблона для рассылки", "id $aData[id]", skLogger::logUsers, "SubscribeToolModule");
        $this->actionTemplates();

    }

    public function actionDelTemplate() {

        $aData = $this->get('data');

        $iItemId = iSset($aData['id']) ? $aData['id'] : false;

        if( $iItemId )
            Subscribe2TemplateMapper::delItem($iItemId);

        $this->actionTemplates();
        $this->addNoticeReport("Удален шаблон из рассылки", "id $iItemId", skLogger::logUsers, "SubscribeToolModule");

    }


    /********************************** SUBSCRIBE *********************************/

    public function actionList() {

        $oList = new ExtList();

        $oList->setAddText('<h1>Список рассылок</h1>');

        $aModel = SubscribeToolApi::getSubscribeListModel();

        $aModel['title']['listColumns'] = array('flex' => 1);
        $aModel['status']['listColumns'] = array('flex' => 1);

        // задать модель данных для вывода
        $oList->setFields( $aModel );

        // добавление набора данных
        $aItems = SubscribeToolApi::getSubscribeList();
        $oList->setValues( $aItems['items'] );
        $oList->setTotal( $aItems['count'] );


        $oList->addRowBtnUpdate('editSubscribe');
        $oList->addRowBtn(array(
            'tooltip' =>'_del',
            'iconCls' => 'icon-delete',
            'action' => 'delSubscribe',
            'state' => 'allow_do',
            'actionText' => 'Вы действительно хотите <b>удалить</b> рассылку?'
        ));


        $oList->addDockedItem(array(
            'text' => 'Подписчики',
            'state' => 'init',
            'action' => 'users',
        ));
        $oList->addDockedItem(array(
            'text' => 'Шаблоны',
            'state' => 'init',
            'action' => 'templates',//editMsg
        ));
        $oList->addDockedItem(array(
            'text' => 'Рассылки',
            'state' => 'init',
            'action' => 'list',//newSubscribe
        ));

        $oList->addBntSeparator();

        $oList->addDockedItem(array(
            'text' => 'Добавить рассылку',
            'iconCls' => 'icon-add',
            'state' => 'init',
            'action' => 'addSubscribeStep1',
        ));


        // вывод данных в интерфейс
        $this->setExtInterface( $oList );


    }

    public function actionAddSubscribeStep1() {

        $oForm = new ExtForm();

        $oForm->setAddText('<h1>Создание новой рассылки - выбор шаблона</h1>');

        /* установить набор элементов формы */
        $aModel = SubscribeToolApi::getChangeTemplateInterface();

        $oForm->setFields($aModel);
        $oForm->setValues(array());

        $oForm->addButton( ExtDocked::create('Дальше')
            ->setIconCls( ExtDocked::iconNext )
            ->setAction('addSubscribeStep2')
            ->unsetDirtyChecker()
        );
//        $oForm->addDockedItem(array(
//            'text' => 'Дальше',
//            'iconCls' => 'icon-next',
//            'state' => 'init',
//            'action' => 'addSubscribeStep2',
//        ));
        $oForm->addDockedItem(array(
            'text' => 'Назад',
            'iconCls' => 'icon-cancel',
            'state' => 'init',
            'action' => 'list',
        ));



        $this->setExtInterface($oForm);

        return psComplete;

    }

    public function actionAddSubscribeStep2() {

        $aData = $this->get('data');
        $iItemId = isSet($aData['tpl']) ? $aData['tpl'] : false;

        $oForm = new ExtForm();

        $oForm->setAddText('<h1>Создание новой рассылки - редактирование сообщения</h1>');

        /* установить набор элементов формы */
        $aModel = SubscribeToolApi::getEditSubscribeInterface();

        $aItems = array();
        $aTempItems = Subscribe2TemplateMapper::getItem($iItemId);
        $aItems['title'] = isSet($aTempItems['title']) ? $aTempItems['title'] : '';
        $aItems['text'] = isSet($aTempItems['content']) ? $aTempItems['content'] : '';
        $aItems['template'] = $iItemId;

        $oForm->setFields($aModel);
        $oForm->setValues($aItems);

        $oForm->addBntSave( 'saveSubscribe', 'init' );
        $oForm->addBntCancel('addSubscribeStep1');

        $this->setExtInterface($oForm);

        return psComplete;

    }

    public function actionEditSubscribe() {

        $aData = $this->get('data');
        $iItemId = isSet($aData['id']) ? $aData['id'] : false;
        if( !$iItemId )
            throw new Exception ('Ошибка: неверный идентификатор рассылки!');

        $oForm = new ExtForm();

        $oForm->setAddText('<h1>Редактирование рассылки</h1>');

        /* установить набор элементов формы */
        $aModel = SubscribeToolApi::getEditSubscribeInterface( false );

        $aTempItems = SubscribeToolMapper::getItem($iItemId);
        $aTempItems['title'] = isSet($aTempItems['title']) ? $aTempItems['title'] : '';
        $aTempItems['text'] = isSet($aTempItems['text']) ? $aTempItems['text'] : '';
        $aTempItems['template'] = $iItemId;

        // блокироум редактирование по статусу
        if( !isSet($aTempItems['status']) ||  $aTempItems['status'] != 1 ){
            $aModel['title']['view'] = 'show';
            $aModel['text']['view'] = 'show';
        }

        $oForm->setFields($aModel);
        $oForm->setValues($aTempItems);

        if( isSet($aTempItems['status']) && $aTempItems['status'] == 1 ) {

            $oForm->addDockedItem(array(
                'text' => 'Отправить',
                'iconCls' => 'icon-commit',
                'state' => 'init',
                'action' => 'sendSubscribeForm',
            ));


            $oForm->addBntSeparator();

            $oForm->addBntSave( 'saveSubscribe' );
//            $oForm->addDockedItem(array(
//                'text' => 'Сохранить',
//                'iconCls' => 'icon-save',
//                'state' => 'allow_do',
//                'action' => 'saveSubscribe',
//                'actionText' => 'Вы действительно хотите <b>сохранить</b> изменения в рассылке?'
//            ));

        }

        $oForm->addBntCancel('list');

        $this->setExtInterface($oForm);

        return psComplete;
    }

    public function actionSaveSubscribe() {

        $aData = $this->get('data');

        $iSubscribeId = SubscribeToolApi::updSubscribe($aData);

        SubscribeToolApi::updSubscribeStatus($iSubscribeId, 1);
        
        $this->addNoticeReport("Сохранение рассылки", "id $iSubscribeId", skLogger::logUsers, "SubscribeToolModule");

        $this->actionList();
    }

    public function actionDelSubscribe() {

        $aData = $this->get('data');

        $iItemId = iSset($aData['id']) ? $aData['id'] : false;

        if( $iItemId )
            SubscribeToolApi::delSubscribe($iItemId);
        
        $this->addNoticeReport("Удаление рассылки", "id $iItemId", skLogger::logUsers, "SubscribeToolModule");
        
        $this->actionList();

    }

    public function actionSendSubscribeForm() {

        $aData = $this->get('data');
        $iItemId = isSet($aData['id']) ? $aData['id'] : false;
        if( !$iItemId )
            throw new Exception ('Ошибка: неверный идентификатор рассылки!');

        $oForm = new ExtForm();

        $oForm->setAddText('<h1>Отправка рассылки</h1>');

        /* установить набор элементов формы */
        $aModel = SubscribeToolApi::getSendSubscribeInterface();

        $aTempItems = SubscribeToolMapper::getItem($iItemId);
        $aTempItems['title'] = isSet($aTempItems['title']) ? $aTempItems['title'] : '';
        $aTempItems['text'] = isSet($aTempItems['text']) ? $aTempItems['text'] : '';

        $aModel['title']['view'] = 'show';
        $aModel['text']['view'] = 'show';

        $aModel['test_mail']['subtext'] = 'На этот адрес отправляется тестовое письмо.';

        unset($aModel['status']);
        unset($aModel['template']);

        $oForm->setFields($aModel);
        $oForm->setValues($aTempItems);

        $oForm->addDockedItem(array(
            'text' => 'Отправить подписчикам',
            'iconCls' => 'icon-commit',
            'state' => 'allow_do',
            'action' => 'sendSubscribe',
            'actionText' => 'Вы действительно хотите <b>выполнить рассылку</b> по базе подписчиков?'
        ));

        $oForm->addDockedItem(array(
            'text' => 'Тестовое письмо',
            'iconCls' => 'icon-commit',
            'state' => 'init',
            'action' => 'sendToEmailSubscribe',
            'doNotUseTimeout' => true
        ));

        $oForm->addBntCancel('list');

        $this->setExtInterface($oForm);

        return psComplete;

    }

    public function actionSendSubscribe() {

        try{

            $aData = $this->get('data');

            $iItemId = iSset($aData['id']) ? $aData['id'] : false;

            if( !$iItemId )
                throw new Exception ('Ошибка: неверный идентификатор сообщения!');

            $iSubId = SubscribeToolMapper::addMailer($iItemId);

            if( !$iSubId )
                throw new Exception ('Ошибка: Рассылка не добавлена!');

            SubscribeToolApi::updSubscribeStatus($iItemId, 2);

            Tasks::addTask('Рассылка сообщений',Tasks::$taskPriorityLow,Tasks::$taskWeightHigh,'SubscribeToolService','sendMailer',array($iSubId));

            $this->addMessage('Рассылка успешно создана');
            $this->addNoticeReport("Создание рассылки", "", skLogger::logUsers, "SubscribeToolModule");

        } catch ( Exception $e ) {
            $this->addError($e->getMessage());
        }

        $this->actionList();

        return psComplete;

    }

    public function actionSendToEmailSubscribe() {

        try{

            $aData = $this->get('data');

            $iItemId = iSset($aData['id']) ? $aData['id'] : false;
            $sTestMail = iSset($aData['test_mail']) ? $aData['test_mail'] : false;

            if( !$iItemId || !$sTestMail )
                throw new Exception ('Ошибка: неверный идентификатор сообщения!');

            if( !SubscribeToolApi::sendTestMailer($iItemId, $sTestMail) )
                throw new Exception ('Ошибка: Не удалось отправить тестовое сообщение!');

            $this->addMessage('Рассылка успешно отправлена на E-mail');
            $this->addNoticeReport("Рассылка успешно отправлена на E-mail", "", skLogger::logUsers, "SubscribeToolModule");

        } catch ( Exception $e ) {
            $this->addError($e->getMessage());
        }

        $this->actionList();

        return psComplete;

    }

}//class