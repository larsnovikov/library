<?php

/**
 * @class SubscribeToolApi
 * @extends skModule
 * @project Skewer
 * @package kernel
 *
 * @author Andy Mitrich, $Author: acat $
 * @version $Revision: 1433 $
 * @date 01.02.12 13:15 $
 *
 */

class SubscribeToolApi {

    /**
     * @static
     * @return array
     */
    public static function getListFields(){

        return array(
            'email',
//            'person',
//            'city'
        );
    }

    /**
     * @static
     * @param $aFields
     * @return array
     */
    public static function getListModel($aFields){

        return Subscribe2UsersMapper::getFullParamDefList($aFields);
    }

    /**
     * @static
     * @param $iPage
     * @param $iOnPage
     * @return array
     */
    public static function getListItems($iPage, $iOnPage){

        $aFilter = array(
            'limit'=>array(
                'start'=>$iPage*$iOnPage,
                'count'=>$iOnPage
            )
        );

        return Subscribe2UsersMapper::getItems($aFilter);
    }

    public static function delItem($iItemId){

        return Subscribe2UsersMapper::delItem($iItemId);
    }


    public static function saveSubscribeTpl($aData){

        if(isset($aData['title']))
            SysVar::set('MsgTplTitle',$aData['title']);

        if(isset($aData['tpl']))
            SysVar::set('MsgTplText',$aData['tpl']);

        return true;
    }

    //***********************************************************

    public static function getTemplateList() {

        return Subscribe2TemplateMapper::getItems();
    }

    public static function getTemplateListModel() {

        return Subscribe2TemplateMapper::getFullParamDefList( array('title') );
    }

    /**
     * Список выбора шаблона для рассылки
     * @return array
     */
    public static function getChangeTemplateInterface() {

        $aTempItems = self::getTemplateList();

        $aTempItems = (isset($aTempItems['items']) ? $aTempItems['items'] : array());
        $aTempItems = array_merge(array(array('id'=>0,'title'=>' - Новый шаблон - ')), $aTempItems);

        $aItems['tpl'] = array(
            'name' => 'tpl',
            'title' => 'Шаблон письма',
            'type' => 's',
            'view' => 'select',
            'value' => 0,
            'valueField' => 'id',
            'displayField' => 'title',
//            'value' => $iForm,
            'store' => array(
                'fields' => array(
                    '0' => 'id',
                    '1' => 'title',
                ),
                'data' => $aTempItems
            )
            //'disabled' => true,
        );

        return $aItems;
    }

    public static function getSubscribeListModel() {

        return SubscribeToolMapper::getFullParamDefList( array('title','status') );
    }

    public static function getSubscribeList() {

        $aFilter = array('order' => array('field' => 'id','way' => 'DESC'));
        $aItems = SubscribeToolMapper::getItems( $aFilter );

        if( isset($aItems['items']) && !empty($aItems['items']) )
            foreach($aItems['items'] as $iKey=>$aItem){

                $aItems['items'][$iKey]['status'] = self::getStatusName( $aItem['status'] );

            }

        return $aItems;
    }

    public static function updSubscribe($aData){

        return SubscribeToolMapper::saveItem($aData);
    }

    public static function updSubscribeStatus($iSubscribeId, $iStatus) {

        $aData = array('id' => $iSubscribeId, 'status' => $iStatus);

        return SubscribeToolMapper::saveItem($aData);
    }

    public static function delSubscribe($iSubscribeId) {

        SubscribeToolMapper::delPostingByMsg($iSubscribeId);

        SubscribeToolMapper::delItem($iSubscribeId);
    }


    /**
     * Интерфейс добавления/редактирования рассылки
     * @param bool $bNewItem
     * @return array
     */
    public static function getEditSubscribeInterface( $bNewItem = true ) {

        $aItems = SubscribeToolMapper::getFullParamDefList(SubscribeToolMapper::getModelFields());

        $aItems['status'] = array_merge(
            array('name' => 'status','title' => 'статус','value' => 1,'disabled' => true),
            ExtForm::getDesc4SelectFromArray(self::getStatusArr())
        );

//        $aCurItems = self::getTemplateList();
//        $aCurItems = isSet($aCurItems['items']) ? $aCurItems['items'] : array();
//        $aItems['template'] = array_merge(
//            array('name' => 'template','title' => 'шаблон','disabled' => true),
//            ExtForm::getDesc4Select($aCurItems, 'id', 'title')
//        );

        $aItems['template'] = array(
            'name' => 'template',
            'title' => 'шаблон',
            'view' => 'hide'
        );

        $aItems['info'] = self::addTextInfoBlock();

        return $aItems;
    }

    /**
     * Интерфейс отправка рассылки
     * @return array
     */
    public static function getSendSubscribeInterface() {

        $aItems = self::getEditSubscribeInterface( false );

        $aItems['test_mail'] = array(
            'name' => 'test_mail',
            'title' => 'Тестовый E-mail',
            'view' => 's',
            'value' => ''
        );

        unSet($aItems['info']);

        return $aItems;
    }

    public static function getStatusArr() {

        return array('Ошибка','Формирование','Ожидание отправки','Отправляется','Отправленно');
    }

    public static function getStatusName( $iStatusId ) {

        $aStatus = self::getStatusArr();

        return isset($aStatus[$iStatusId]) ? $aStatus[$iStatusId] : $aStatus[0];

    }

    /**
     * Отправка тестового сообщения рассылки
     * @param $iMailerId Ид сообщения
     * @param $sTargetMail Тестовый адрес
     * @return bool
     */
    public static function sendTestMailer($iMailerId, $sTargetMail) {

        $sNoReplayMail = skConfig::get('notifications.noreplay_email');

        $sMailPerson = str_replace('/','',WEBROOTPATH);
        $sSendMail = Parameters::getByName(skConfig::get('section.sectionRoot'),'.','send_email');
        $sMailFrom = !empty($sSendMail) ? $sSendMail['value'] : $sNoReplayMail;

        if( !($aItem = SubscribeToolMapper::getItem($iMailerId)) )
            return false;

        $sSubject = $aItem['title'];
        $sCurBody = $aItem['text'];

        // замена ссылки на отписку для каждого пользователя
        $sCurBody = str_replace('[адрес сайта]',str_replace('/','',WEBROOTPATH),$sCurBody);
        $sCurBody = str_replace('[список новостей]',self::getLastNewsForMailer(),$sCurBody);

        $sUnSubscribeLink = 'http://'.$_SERVER['HTTP_HOST'].skRouter::rewriteURL('[152][SubscribeModule?cmd=unsubscribe&email='.$sTargetMail.'&token='.md5('unsub'.$sTargetMail.'010').']');
        $sUnSubscribeLink = '<a href="'.$sUnSubscribeLink.'">отказаться от рассылки</a>';
        $sCurBody = str_replace('[ссылка отписаться]',$sUnSubscribeLink,$sCurBody);


        $aMail = skMailer::getMail($sSubject, $sCurBody, $sMailFrom, $sMailPerson);
        $sucess = skMailer::sendReadyMail($aMail,$sTargetMail);

        return $sucess;
    }

    public static function getLastNewsForMailer(){

        $aNews = SubscribeToolMapper::getLastNews();

        if( !count($aNews) ) return false;

        foreach($aNews as $iKey => $aCurNews){

            $sHref = (!empty($aCurNews['news-alias']))? skRouter::rewriteURL('['.$aCurNews['parent_section'].'][NewsModule?news-alias='.$aCurNews['news-alias'].']'): skRouter::rewriteURL('['.$aCurNews['parent_section'].'][NewsModule?news_id='.$aCurNews['id'].']');
            $sHref = $_SERVER['HTTP_HOST'].$sHref;
            $aNews[$iKey]['href'] = $sHref;

        }

        $sNewText = skParser::parseTwig('mailerNews.twig',array('aNews'=>$aNews),BUILDPATH.'tool/modules/Subscribe/templates/');

        return $sNewText;
    }

    public static function addTextInfoBlock() {
        return array(
            'name' => 'info',
            'title' => 'Метки замены',
            'view' => 'show',
            'disabled' => false,
            'value' => nl2br("[ссылка отписаться] - ссылка отписаться от рассылки \n[адрес сайта] - URL адрес сайта \n[список новостей] - список новостей за 7 дней")
        );
    }




}//class