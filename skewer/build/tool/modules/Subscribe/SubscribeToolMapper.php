<?php
/**
 * Класс для работы с таблицами рассылки
 *
 * @class: SubscribeToolMapper
 *
 * @author kolesnikov
 * @version 0.1
 *
 */

class SubscribeToolMapper extends skMapperPrototype {

    /**
     * Таблица модели
     * @var string
     */
    protected  static $sCurrentTable = 'subscribe_msg';

    /**
     * Модель данных
     * @var array
     */
    protected static $aParametersList = array(
        'id'        => 'i:hide:id',
        'title'     => 's:str:Заголовок',
        'text'      => 's:wyswyg:Текст сообщения',
        'template'  => 'i:int:Шаблон',
        'status'    => 'i:int:Статус',
    );


    /**
     * @static
     * @return bool|mixed
     */
    public static function getLastNews(){

        $sQuery = "SELECT * FROM `news` WHERE `publication_date` > SUBDATE(NOW(), INTERVAL 1 WEEK) ORDER BY `publication_date` DESC";

        global $odb;
        $rResult = $odb->query($sQuery);

        if(!$rResult) return false;

        $aNews = array();
        while($aCurNews = $rResult->fetch_array()){
            $aNews[] = $aCurNews;
        }

        return $aNews;
    }

    public static function addTextMailer($sNewText){

        $sQuery = "INSERT INTO `[table:q]` (`title`, `text`) VALUES ([title:s], [text:s])";

        $sNewTitle = SysVar::get('MsgTplTitle');
        $sNewTitle = str_replace('[название сайта]',$_SERVER['HTTP_HOST'],$sNewTitle);
        $sNewText = str_replace('[список новостей]',$sNewText,SysVar::get('MsgTplText'));

        $aData = array(
            'table' => 'subscribe_msg',
            'title' => $sNewTitle,
            'text' => $sNewText
        );


        global $odb;
        $rResult = $odb->query($sQuery,$aData);

        if ( !$rResult || !$odb->insert_id) return false;

        return $odb->insert_id;

    }

    public static function addNewMailer($sTitle, $sText){

        $sQuery = "INSERT INTO `[table:q]` (`title`, `text`) VALUES ([title:s], [text:s])";

        $aData = array(
            'table' => 'subscribe_msg',
            'title' => $sTitle,
            'text' => $sText
        );


        global $odb;
        $rResult = $odb->query($sQuery,$aData);

        if ( !$rResult || !$odb->insert_id) return false;

        return $odb->insert_id;

    }

    /**
     * Добавление новой рассылки
     * @static
     * @param int $iBodyId
     * @return bool|int
     */
    public static function addMailer($iBodyId){

        // получение целевый email-ов
        $sUserList = self::getUserList();

        $sQuery = "INSERT INTO `[table:q]` (list, state, last_pos, id_body, id_from)
                                    VALUES ([user_list:s], [state:i], [last_pos:i], [body:i], 0)";

        $aData = array(
            'table' => 'subscribe_posting',
            'user_list' => $sUserList,
            'state' => 0,
            'last_pos' => 0,
            'body' => $iBodyId,
        );


        global $odb;
        $rResult = $odb->query($sQuery,$aData);

        if ( !$rResult || !$odb->insert_id) return false;

        return $odb->insert_id;
    }

    /**
     * Получение списка email-ов пользователей из подписки
     * @static
     * @return string
     */
    private static function getUserList(){

        $sUserList = '';

        $sQuery = "SELECT email FROM `subscribe_users`";

        global $odb;
        $rResult = $odb->query($sQuery);

        while($aRow = $rResult->fetch_assoc()){
            if($sUserList) $sUserList .= ',';
            $sUserList .= $aRow['email'];
        }

        return $sUserList;
    }

    /**
     * сброс повисших рассылок
     * @static
     * @return bool
     */
    public static function resetErrorMailer(){

        $query = "UPDATE subscribe_posting SET state=0 WHERE `post_date` < (NOW() - INTERVAL 10 MINUTE) AND state>10";
        mysql_query($query);

        return true;
    }

    /**
     * выделение рассылки
     * @static
     * @param int $iMailerId
     * @return int|bool
     */
    public static function mutMailer($iMailerId = 0){

        $iMailerId = (int)$iMailerId;
        $tkn = rand(1,200)*100+date('s');

        $sQuery = "UPDATE `[table:q]` SET state=[state:i], post_date=NOW()
                                    WHERE state=[state_search:i] [cur_id:q] LIMIT 1";

        $aData = array(
            'table' => 'subscribe_posting',
            'state' => $tkn,
            'state_search' => 0,
            'cur_id' => $iMailerId?"AND id=$iMailerId":'',
        );

        global $odb;
        $rResult = $odb->query($sQuery,$aData);

        if(!$rResult || !$odb->affected_rows) return false;

        return $tkn;
    }

    public static function getMutMailer($iMutexToken){

        $aResult = array();

        if($iMutexToken){

            $sQuery = "SELECT subscribe_msg.id, subscribe_msg.text as text,
                          subscribe_msg.title as title,
                          subscribe_posting.list, subscribe_posting.id_from, subscribe_posting.last_pos,
                          subscribe_posting.id as postingid,
                          subscribe_posting.id_body as textid
                  FROM subscribe_posting, subscribe_msg
                  WHERE subscribe_posting.id_body = subscribe_msg.id AND state = [token:s]
                  ORDER BY post_date ASC";

            $aData = array(
                'token' => $iMutexToken,
            );

            global $odb;
            $rResult = $odb->query($sQuery,$aData);

            if(!$rResult) return false;

            while($aRow = $rResult->fetch_assoc()) {

                $aResult[] = $aRow;

            }

        }

        return $aResult;

    }

    /**
     * Обновление кол-ва отосланных сообщений для задачи рассыльщика сообещний
     * @static
     * @param $iPosringId
     * @param $iLastPost
     * @return bool
     */
    public static function updateLastPostMailer($iPosringId,$iLastPost){

        $sQuery = "UPDATE `[table:q]` SET last_pos=[last_pos:i], state=[state:i] WHERE id='[cur_id:i]'";

        $aData = array(
            'table' => 'subscribe_posting',
            'last_pos' => $iLastPost,
            'state' => 0,
            'cur_id' => $iPosringId,
        );

        global $odb;
        $rResult = $odb->query($sQuery,$aData);

        if(!$rResult) return false;

        return true;

    }

    /**
     * Установка статуса разослано для заданной рассылки
     * @static
     * @param $iPosringId
     * @param $iBodyId
     * @return bool
     */
    public static function setReadyMailer($iPosringId,$iBodyId){

        $sQuery = "UPDATE `[table:q]` SET state=[state:i] WHERE id='[cur_id:i]'";

        $aData = array(
            'table' => 'subscribe_posting',
            'state' => 1,
            'cur_id' => $iPosringId,
        );

        global $odb;
        $rResult = $odb->query($sQuery,$aData);

        if(!$rResult) return false;

        //$query = "UPDATE subscribe_msg SET status = 3 WHERE id = $iBodyId";
        //mysql_query($query);

        return true;
    }


    public static function clearPostingLog(){

        $sQuery = "DELETE FROM `subscribe_posting`;";

        global $odb;

        return $odb->query($sQuery);

    }

    public static function delPostingByMsg( $iMsgId ){

        global $odb;

        $sQuery = "DELETE FROM `[table:q]` WHERE `id_body`=[cur_id:i];";

        $aData = array(
            'table' => 'subscribe_posting',
            'cur_id' => (int)$iMsgId
        );

        return $odb->query($sQuery,$aData);

    }
}
