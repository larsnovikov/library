<?php
/**
 *
 * @project Skewer
 * @package Modules
 *
 * @author kolesnikov, $Author: sapozhkov $
 * @version $Revision: 1316 $
 * @date $Date: 2012-11-27 18:31:02 +0400 (Вт., 27 нояб. 2012) $
 */
class SubscribeToolService extends ServicePrototype{

    /**
     * Добавление нового шаблона сообщения
     * @static
     * @return int
     */
    private static function addTextMailer(){

        $sNewText = SubscribeToolApi::getLastNewsForMailer();

        $iMailerTextId = SubscribeToolMapper::addTextMailer($sNewText);

        return $iMailerTextId;
    }

    /**
     * Добавление нового задания на рассылку
     * @static
     * @param $iMailerTextId
     * @return bool|int
     */
    private static function addPostMailer($iMailerTextId){

        // добавление новой отправки
        $iMailerId = SubscribeToolMapper::addMailer($iMailerTextId);

        return $iMailerId;
    }



    /**
     * Метод создает рассылку
     * @return int
     */
    public function makeMailer(){

        // добавление новой рассылки
        $iMailerTextId = self::addTextMailer();

        if(!$iMailerTextId)
            return Tasks::$taskStatusComplete;

        $iMailerId = self::addPostMailer($iMailerTextId); // добавление задания на рассылку в базу

        if(!$iMailerId)
            return Tasks::$taskStatusNotComplete;

        // добавление задания на рассылку в задачу
        Tasks::addTask('Рассылка сообщений',Tasks::$taskPriorityLow,Tasks::$taskWeightHigh,'SubscribeToolService','sendMailer',array($iMailerId));

        return Tasks::$taskStatusComplete;
    }


    /**
     * Метод производит одну интерацию рассылки
     * @param int $iMailerId
     * @return int
     */
    public function sendMailer($iMailerId){

        $iMailerStatus = Tasks::$taskStatusNotComplete;

        $sNoReplayMail = skConfig::get('notifications.noreplay_email');

        $sMailPerson = str_replace('/','',WEBROOTPATH);
        $sSendMail = Parameters::getByName(3,'.','send_email');
        $sMailFrom = !empty($sSendMail) ? $sSendMail['value'] : $sNoReplayMail;

        $iMaxCountMsg = 50;     //Количество отправляемых сообщений за 1 раз
        $iCountMsg = 0;         //Счетчик отправляемых сообщений

        // сбрасываем повисшие рассылки
        //SubscribeToolMapper::resetErrorMailer();

        //выбираем рассылку
        $iMutexToken = SubscribeToolMapper::mutMailer($iMailerId);

        $aCurMailer = SubscribeToolMapper::getMutMailer($iMutexToken);

        foreach( $aCurMailer as $row ){

            $iPostingId	= $row['postingid'];
            $sUserList	= $row['list'];
            $sSubject	= trim($row['title']);
            $message	= $row['text'];
            $iBodyId	= $row['id'];
            $iLastPos	= $row['last_pos'];
            $iTextId    = $row['textid'];

            SubscribeToolApi::updSubscribeStatus($iTextId, 3);

            //получаем список пользователей
            $aUserList		= explode(',',$sUserList);

            for($i = $iLastPos; $i < count($aUserList); $i++){

                $value = trim(str_replace(',', '',$aUserList[$i]));

                if ($value){

                    $message = str_replace('[адрес сайта]',str_replace('/','',WEBROOTPATH),$message);
                    $message = str_replace('[список новостей]',SubscribeToolApi::getLastNewsForMailer(),$message);

                    // замена ссылки на отписку для каждого пользователя
                    $sUnSubscribeLink = 'http://'.$_SERVER['HTTP_HOST'].skRouter::rewriteURL('[152][SubscribeModule?cmd=unsubscribe&email='.$value.'&token='.md5('unsub'.$value.'010').']');
                    $sUnSubscribeLink = '<a href="'.$sUnSubscribeLink.'">отказаться от рассылки</a>';
                    $sCurBody = str_replace('[ссылка отписаться]',$sUnSubscribeLink,$message);

                    $aMail = skMailer::getMail($sSubject, $sCurBody, $sMailFrom, $sMailPerson);
                    $sucess = skMailer::sendReadyMail($aMail,$value);

                    if( !$sucess ){
                        SubscribeToolMapper::updateLastPostMailer($iPostingId,$i+1);
                        return Tasks::$taskStatusNotComplete;
                    }

                    $iCountMsg++;

                }

                //если текущая рассылка закончена выставляем соответствующий статус
                if ($i==(count($aUserList)-1)) {

                    SubscribeToolMapper::setReadyMailer($iPostingId,$iBodyId);

                    $iMailerStatus = Tasks::$taskStatusComplete;

                    SubscribeToolApi::updSubscribeStatus($iTextId, 4);

                }

                //Если достиголи предела работы крона записываем  положение и выходим
                if ($iCountMsg == $iMaxCountMsg){

                    SubscribeToolMapper::updateLastPostMailer($iPostingId,$i+1);

                    return Tasks::$taskStatusRepeat;

                };
            }

        }

        return $iMailerStatus;
    }

}
