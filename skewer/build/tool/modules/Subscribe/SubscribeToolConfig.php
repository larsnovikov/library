<?php

/* main */
$aConfig['name']     = 'SubscribeTool';
$aConfig['title']    = 'Рассылка (админ)';
$aConfig['version']  = '1.000a';
$aConfig['description']  = 'Админ-интерфейс управления рассылкой новостей';
$aConfig['revision'] = '0001';
$aConfig['layer']     = 'tool';

return $aConfig;
