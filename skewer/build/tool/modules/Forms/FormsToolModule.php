<?php

/**
 * @class FormsToolModule
 * @extends skModule
 * @project Skewer
 * @package kernel
 *
 * @author Andy Mitrich, $Author: sapozhkov $
 * @version $Revision: 1305 $
 * @date 16.01.12 13:16 $
 *
 */

class FormsToolModule extends AdminToolTabModulePrototype {

    public $iSectionId;
    public $iCurrentForm = 0;
    public $enableSettings = 0;
    /**
     * @var null|\FormsAdmApi
     */
    var $oFormApi = NULL;

    /**
     * @var string - Имя модуля
     */
    protected $sTabName = 'Конструктор форм';

    protected function preExecute() {

        CurrentAdmin::testControlPanelAccess();

        // id текущего раздела
        $this->iCurrentForm = $this->getInt('form_id');

        if ( $this->getInt('sectionId') )
            $this->iSectionId = $this->getInt('sectionId');

        if ( !$this->iSectionId )
            $this->iSectionId = $this->getEnvParam('sectionId');

        $this->oFormApi = new FormsAdmApi();
    }

    protected function actionInit() {

        $this->setPanelName('Выбор формы');

        $this->actionList();
    }

    /**
     * Список новостей
     */
    protected function actionList() {

        $this->iCurrentForm = 0;

        // объект для построения списка
        $oList = new ExtList();

        $aModel = $this->oFormApi->getFormModel($this->oFormApi->getFormListFields());
        $aModel['form_title']['listColumns'] = array('flex' => 2);
        $aModel['form_section']['listColumns'] = array('flex' => 2);
        $aModel['form_handler_value']['listColumns'] = array('flex' => 1); // поле email
        $aModel['form_active']['listColumns'] = array('width' => 100);

        if( !CurrentAdmin::isSystemMode() ){
            //unset($aModel['form_title']);
            unset($aModel['form_handler_type']);
        }

        // убираем раздел для показа до лучших времен
        unset($aModel['form_section']);

        // задать модель данных для вывода
        $oList->setFields( $aModel );

        // добавление набора данных
        $aItems = $this->oFormApi->getFormList();

        // для несистемного админа не показывать формы-обработчик
        if( !CurrentAdmin::isSystemMode() && $aItems['count'] )
            foreach( $aItems['items'] as $iKey=>$aItem ) {
                if( $aItem['form_handler_type'] == 'toMethod' ) //$aItem['form_is_template'] || !$aItem['form_section'] ||
                    unset($aItems['items'][$iKey]);
            }


        $oList->setValues( $aItems['items'] );

        // сортировка
        $oList->addSorter('title');

        // добавление кнопок
        $oList->addRowBtnUpdate();


        $oList->addRowBtnDelete();

        // кнопка добавления
        $oList->addBntAdd('show');



        // вывод данных в интерфейс
        $this->setExtInterface( $oList );

    }

    /**
     * Отображение формы
     */
    protected function actionShow($aSubData = array()) {

        // подключить автоматический генератор форм
        $oForm = new ExtForm();

        // номер опроса
        $aData = $this->get('data');
        if( is_array($aSubData) && !empty($aSubData) ) $aData = array_merge($aData, $aSubData);

        $iItemId = (is_array($aData) && isset($aData['form_id'])) ? (int)$aData['form_id'] : 0;

        if ( !$iItemId ) $iItemId = $this->iCurrentForm;

        $aItem = $iItemId ? $this->oFormApi->getFormById($iItemId): $this->oFormApi->getBlankValues($this->iSectionId,isset($aData['form_title'])?$aData['form_title']:'Форма');

        // установить набор элементов формы
        $aItems = $this->oFormApi->getFormModel($this->oFormApi->getFormDetailFields());

        // скрываем поле раздел для показа до лучших времен =) //if ( $this->enableSettings==0 )
        unset( $aItems['form_section'] );

        // изменение интерфейса для разных политик администраторов
        //$aItems['form_section']['disabled'] = true;
        if( !CurrentAdmin::isSystemMode() ){
            unset( $aItems['form_handler_type'] );
            unset( $aItems['form_is_template'] );
            //unset( $aItems['form_title'] );
        } else {
            $aItems['form_handler_value']['title'] = 'Значение обработчика';
        }

        $aItems['form_handler_value']['subtext'] = 'По умолчанию используется адрес <b>'.Parameters::getValByName(3,'.', 'email').'</b>';
        $aItems['form_redirect']['subtext'] = 'По умолчанию используется текущий раздел';

        $oForm->setFields( $aItems );

        // установить значения для элементов
        $oForm->setValues( $aItem );

        $oForm->addBntSave();
        $oForm->addBntCancel();

        if ( $iItemId ) {

            $oForm->addDockedItem(array(
                'text' => 'Элементы формы',
                'iconCls' => 'icon-edit',
                'state' => 'paramsList',
                'action' => 'paramsList'
            ));

            $oForm->addDockedItem(array(
                'text' => 'Текст автоответа',
                'iconCls' => 'icon-edit',
                'state' => 'answerDetail',
                'action' => 'answerDetail'
            ));

            $this->iCurrentForm = $iItemId;

            $oForm->addBntSeparator('->');
            if( CurrentAdmin::isSystemMode() )
                $oForm->addBntDelete();
        }

        // вывод данных в интерфейс
        $this->setExtInterface( $oForm );
    }

    /**
     * Сохранение опроса
     */
    protected function actionSave() {

        // запросить данные
        $aData = $this->get( 'data' );

        if( !isset($aData['form_handler_type']) || !$aData['form_handler_type'] )
            $aData['form_handler_type'] = 'toMail';

        // есть данные - сохранить
        if ( $aData )
            $this->iCurrentForm = $this->oFormApi->updForm( $aData );
        
        $this->addNoticeReport("Форма в 'Конструктор форм' изменена", "<pre>".print_r($aData,true)."</pre>", skLogger::logUsers, "FormsToolModule");

        $this->actionList();
    }


    /**
     * Удаляет запись
     */
    protected function actionDelete() {

        // запросить данные
        $aData = $this->get( 'data' );

        // id записи
        $iItemId = ( is_array($aData) and isset($aData['form_id']) ) ? (int)$aData['form_id'] : 0;

        // удаление
        $this->oFormApi->delForm( $iItemId );
        $this->addNoticeReport("Форма в 'Конструктор форм' удалена", "<pre>".print_r($aData,true)."</pre>", skLogger::logUsers, "FormsToolModule");
        $this->actionList();
    }

    protected function setParamListItems( ExtList &$oList, $iItemId ) {

        // добавление набора данных
        $aItems = $this->oFormApi->getParamsList($iItemId);

        $oList->setValues( $aItems['items'] );
        $oList->setTotal( $aItems['count'] );

    }


    protected function actionParamsList(){

        // объект для построения списка
        $oList = new ExtList();

        // активируем сортировку
        $oList->enableDragAndDrop( 'sortParamsList' );

        $aModel = $this->oFormApi->getParamModel($this->oFormApi->getParamsListFields());
        $aModel['param_title']['listColumns'] = array('flex' => 1);
        $aModel['param_name']['listColumns'] = array('flex' => 1);
        $aModel['param_type']['listColumns'] = array('flex' => 1);
        $aModel['param_required']['listColumns'] = array('width' => 140);
        //$aModel['param_priority']['listColumns'] = array('width' => 80);

        // задать модель данных для вывода
        $oList->setFields( $aModel );
        $aData = $this->get( 'data' );

        $iItemId = ( is_array($aData) and isset($aData['form_id']) ) ? (int)$aData['form_id'] : 0;

        if ( !$iItemId )
            $iItemId = $this->iCurrentForm;

        // число записей на страницу
        //$oList->setOnPage( $this->iOnPage );

        $this->setParamListItems($oList, $iItemId);

        //$oList->addSorter('param_priority');

        // добавление кнопок
        $oList->addRowBtn(array(
            'tooltip' =>'_upd',
            'iconCls' => 'icon-edit',
            'action' => 'showParamForm',
            'state' => 'edit_form'
        ));

        $oList->addRowBtn(array(
            'text' => '_delete',
            'iconCls' => 'icon-delete',
            'state' => 'paramDelete',
            'action' => 'paramDelete'
        ));

        // кнопка добавления
        $oList->addBntAdd('showParamForm');
        $oList->addDockedItem(array(
            'text' => 'Назад',
            'iconCls' => 'icon-cancel',
            'state' => 'edit_form',
            'action' => 'show'
        ));

        // вывод данных в интерфейс
        $this->setExtInterface( $oList );
    }

    protected function actionSortParamsList() {

        $aData = $this->get( 'data' );
        $aDropData = $this->get( 'dropData' );
        $sPosition = $this->get( 'position' );

        if( !isSet($aData['param_id']) || !$aData['param_id'] ||
            !isSet($aDropData['param_id']) || !$aDropData['param_id'] || !$sPosition )
            $this->addError('Ошибка! Неверно заданы параметры сортировки');

        $aData = Forms2ParamsMapper::getItem($aData['param_id']);
        $aDropData = Forms2ParamsMapper::getItem($aDropData['param_id']);

        if( !FormsAdmApi::sortParams($aData, $aDropData, $sPosition) )
            $this->addError('Ошибка! Неверно заданы параметры сортировки');

    }

    /**
     * Отображение формы
     */
    protected function actionShowParamForm() {

        // подключить автоматический генератор форм
        $oForm = new ExtForm();
        // номер новости
        $aData = $this->get('data');

        $iItemId = (is_array($aData) && isset($aData['param_id'])) ? (int)$aData['param_id'] : 0;

        $aFormItems = $this->oFormApi->getParamModel($this->oFormApi->getParamsDetailFields());

        // sub info
        $aFormItems['param_description']['labelAlign'] = 'left';
        $aFormItems['param_default']['labelAlign'] = 'left';
        $aFormItems['param_default']['subtext'] =
    "<ul>
        <li> • Текстовое поле - строка</li>
        <li> • Параметр галочка - поле \"значение по умолчанию\" должно быть обязательно заполнено текстом, который будет отправлен при установленной галочке!</li>
        <li> • Выподающий список, переключатели - строка вида <b>value:title;</b> (каждая запись с новой строки)</li>
        <li> • Выподающий список, переключатели - строка вида <b>ClassName::methodName()</b></li>
        <li> • Где <b>methodName()</b> - публичный метод класса, принимающий необязательный аргумент-массив</li>
    </ul>";

        // установить набор элементов формы
        $oForm->setFields( $aFormItems );

        // установить значения для элементов
        $aItems = $iItemId ? $this->oFormApi->getParamById($iItemId): $this->oFormApi->getBlankParamValues();
        $oForm->setValues( $aItems );

        $oForm->addBntSave( 'paramSave' );
        $oForm->addBntCancel('paramsList');

        if ( $iItemId ) {
            $oForm->addBntSeparator('->');
            $oForm->addDockedItem(array(
                'text' => 'Удалить',
                'iconCls' => 'icon-delete',
                'state' => 'paramDelete',
                'action' => 'paramDelete'
            ));
        }

        // вывод данных в интерфейс
        $this->setExtInterface( $oForm );
    }

    /**
     * Сохранение
     */
    protected function actionParamSave() {

        // запросить данные
        $aData = $this->get( 'data' );

        $aData['form_id'] = $this->iCurrentForm;

        // есть данные - сохранить
        if ( $aData ){

            if( !isset($aData['param_name']) || !$aData['param_name'] )
                $aData['param_name'] = $aData['param_title'];

            $aData['param_name'] = skFiles::makeURLValidName($aData['param_name'],false);

            if( !$aData['param_maxlength'] )
                $aData['param_maxlength'] = 255;

            // поддержка уникальности идентификатора
            FormsAdmApi::isUniqueParamName($aData, $this->iCurrentForm);

            $this->oFormApi->updParam( $aData );
        }

        // вывод списка
        $this->actionParamsList();

    }

    /**
     * Удаляет запись
     */
    protected function actionParamDelete() {

        // запросить данные
        $aData = $this->get( 'data' );

        $aData['form_id'] = $this->iCurrentForm;
        // удаление
        $this->oFormApi->delParam( $aData );

        // вывод списка
        $this->actionParamsList();

    }

    protected function actionAnswerDetail() {

        // подключить автоматический генератор форм
        $oForm = new ExtForm();

        // запись ответа
        $aItem = $this->oFormApi->getAnswerByFormId($this->iCurrentForm);

        // установить набор элементов формы
        $oForm->setFields( $this->oFormApi->getAnswerModel($this->oFormApi->getAnswerDetailFields()) );

        // установить значения для элементов
        $oForm->setValues( $aItem );

        $oForm->addBntSave( 'answerSave' );
        $oForm->addBntCancel('show');

        // вывод данных в интерфейс
        $this->setExtInterface( $oForm );

    }

    protected function actionAnswerSave() {

        // запросить данные
        $aData = $this->get( 'data' );

        $aData['form_id'] = $this->iCurrentForm;

        // есть данные - сохранить
        if ( $aData ){

            Forms2AnswerMapper::saveItem( $aData );
        }

        // вывод списка
        $this->actionShow();

    }

    /**
     * Установка служебных данных
     * @param \ExtPrototype $oExtIface
     */
    protected function setServiceData( ExtPrototype $oExtIface ) {

        // установить заголовок модуля
        $oExtIface->setTitle( $this->sTabName );

        // установить данные для передачи интерфейсу
        $oExtIface->setServiceData( array(
            'section' => $this->iSectionId,
            'form_id' => $this->iCurrentForm,
            'enableSettings' => $this->enableSettings
        ) );

    }

} //class