<?php
/**
 * Маппер для работы с записями бэкапов
 */
class BackupToolModel extends skModelPrototype {

    /**
     * Отдает описание модели
     * @var array
     */
    protected static $aParametersList = array(
        'id'          => 'i:hide:ID записи',
        'site_id'     => 'i:hide:Привязка к площадке',
        'backup_file' => 's:str:Имя файла',
        'date'        => 's:str:Дата и время',
        'status'      => 'i:str:Статус',
        'comments'    => 's:text:Комментарии'
    );

    /**
     * Отдает набор полей для просмотра списка
     */
    public static function getListFields() {
        return array(
            'id',
            'date',
            'mode',
            'size',
            'backup_file',
        );
    }

    /**
     * Отдает дополнительный набор параметров для конфигурации полей
     * @return array
     */
    protected static function getAddParamList(){
        return array(
            'id' => array(
                'listColumns' => array('hidden' => true)
            ),
            'date' => array(
                'listColumns' => array('width' => 120)
            ),
            'mode' => array(
                'title' => 'Режим',
                'listColumns' => array('width' => 50)
            ),
            'backup_file' => array(
                'listColumns' => array('flex' => 1)
            ),
            'size' => array(
                'title' => 'Размер',
                'listColumns' => array('width' => 80)
            ),
        );
    }

}
