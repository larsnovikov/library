<?php
/**
 * @class BackupToolModule
 * @extends AdminModulePrototype
 * @project Skewer
 * @package modules
 *
 * @author kolesnikov, $Author: acat $
 * @version $Revision: 1672 $
 * @date 01.02.12 13:00 $
 *
 */
class BackupToolModule extends AdminToolTabModulePrototype {

    protected $sTabName = 'Резервные копии';

    protected function preExecute() {
        CurrentAdmin::testControlPanelAccess();
    }

    public function ActionInit(){
        // объект для построения списка
        $oList = new ExtList();

        $aModel = BackupToolModel::getFullParamDefList( BackupToolModel::getListFields() );

        // задать модель данных для вывода
        $oList->setFields( $aModel );

        // добавление набора данных
        $aItems = BackupToolApi::getListItems();

        // форматирование элементов
        if(isSet($aItems['items']) AND count($aItems['items']))
	        foreach ( $aItems['items'] as $iKey => $aItem ) {
	            $aItem['size_sort'] = skFiles::sizeToSortStr( $aItem['size'] );
	            $aItem['size'] = skFiles::sizeToStr( $aItem['size'] );
	            $aItems['items'][$iKey] = $aItem;
	        }

        $oList->setValues( $aItems['items'] );

        $oList->addRowBtn(array(
            'tooltip' =>'Восстановить',
            'iconCls' => 'icon-recover',
            'action' => 'recoverForm',
            'state' => 'init'
        ));

//        $oList->addRowBtn(array(
//            'tooltip' =>'Скачать',
//            'iconCls' => 'icon-save',
//            'action' => 'downloadFile',
//            'state' => 'init'
//        ));

        $oList->addRowBtn(array(
            'tooltip' =>'_del',
            'iconCls' => 'icon-delete',
            'action'  => 'remove',
            'state'   => 'delete'
        ));


        $oList->addDockedItem(array(
            'text' => 'Настройки расписания',
            'iconCls' => 'icon-edit',
            'state' => 'init',
            'action' => 'toolsForm',
        ));

        $oList->addDockedItem(array(
            'text' => 'Сделать резервную копию',
            'iconCls' => 'icon-add',
            'state' => 'allow_do',
            'action' => 'createBackup',
            'actionText' => 'Вы действительно хотите <b>создать</b> резервную копию?',
            'doNotUseTimeout' => true,
        ));

        // вывод данных в интерфейс
        $this->setExtInterface( $oList );
    }


    public function actionToolsForm(){

        $aData = BackupToolService::getBackupSetting();

        // объект для построения списка
        $oForm = new ExtForm();

        $aItems = array();

        /* Файл */
        $aItems['bs_enable'] = array(
            'name' => 'bs_enable',
            'title' => 'Использовать локальные настройки',
            'view' => 'check',
            'value' => $aData['bs_enable'],
            //'disabled' => true,
        );

        $aItems['bs_day'] = array(
            'name' => 'bs_day',
            'title' => 'Кол-во за сутки',
            'view' => 'int',
            'value' => $aData['bs_day'],
            //'disabled' => true,
        );

        $aItems['bs_week'] = array(
            'name' => 'bs_week',
            'title' => 'Кол-во за неделю',
            'view' => 'int',
            'value' => $aData['bs_week'],
            //'disabled' => true,
        );

        $aItems['bs_month'] = array(
            'name' => 'bs_month',
            'title' => 'Кол-во за месяц',
            'view' => 'int',
            'value' => $aData['bs_month'],
            //'disabled' => true,
        );
                              /*
        $aItems['bs_hour'] = array(
            'name' => 'bs_hour',
            'title' => 'Время запуска, час',
            'view' => 'int',
            'value' => $aData['bs_hour'],
            //'disabled' => true,
        );

        $aItems['bs_min'] = array(
            'name' => 'bs_min',
            'title' => 'Время запуска, мин',
            'view' => 'int',
            'value' => $aData['bs_min'],
            //'disabled' => true,
        );
                            */
        $oForm->setFields($aItems);
        $oForm->setValues(array());

        $oForm->addBntSave('saveTools');
        
        $oForm->addDockedItem(array(
            'text' => 'К списку',
            'iconCls' => 'icon-cancel',
            'state' => 'init',
            'action' => 'init',
        ));

        $this->setExtInterface($oForm);

        return psComplete;

    }

    public function actionSaveTools(){

        $aData = $this->get('data');

        BackupToolApi::setBackupSetting($aData);

        $this->actionToolsForm();

        return psComplete;
    }


    public function actionCreateBackup(){

        $iStatus = BackupToolApi::createNewBackup();

        $this->addMessage( 'Статус выполнения задачи: '.Tasks::getStatusTitle($iStatus) );
        $this->addNoticeReport("Создание бекапа", "", skLogger::logUsers, "BackupToolModule");

        $this->actionInit();

    }


    public function actionRecoverForm(){

        $aData = $this->get('data');

        $oForm = new ExtForm();

        $this->setPanelName('Мастер восстановления площадки',true);

        /* Id резервной копии */
        $aItems['id'] = array(
            'name' => 'id',
            'title' => '',
            'view' => 'hide',
            'value' => $aData['id'],
            'disabled' => false,
        );

        /* Файл */
        $aItems['file'] = array(
            'name' => 'file',
            'title' => 'Файл',
            'view' => 'str',
            'value' => $aData['backup_file'],
            'disabled' => true,
        );

        /* Дата создания */
        $aItems['creation_date'] = array(
            'name' => 'creation_date',
            'title' => 'Дата создания копии',
            'view' => 'str',
            'value' => $aData['date'],
            'disabled' => true,
        );

        /* Делать ли резервную копию перед разворачиванием площадки */
        $aItems['before_backup'] = array(
            'name' => 'before_backup',
            'title' => 'Сделать резервную копию перед восстановлением',
            'view' => 'check',
            'value' => 1,
        );

        $oForm->setFields($aItems);
        $oForm->setValues(array());

        $oForm->addDockedItem(array(
            'text' => 'Восстановить',
            'iconCls' => 'icon-recover',
            'state' => 'allow_do',
            'action' => 'recover',
            'actionText' => 'Вы действительно хотите <b>восстановить</b> резервную копию?',
        ));
        $oForm->addBntCancel('init');
        $oForm->addBntSeparator('->');

        $this->setExtInterface($oForm);

        return psComplete;


    }


    public function actionRecover(){

        try {

            $aData = $this->get('data');

            if(!isSet($aData['id']) OR !$iBackupId = $aData['id']) throw new Exception('Recover error: Backup is undefined!');

            $bCreateBeforeBackup = (isSet($aData['before_backup']) AND $aData['before_backup'])? true: false;

            /* Получить данные по резервной копии */
            if($bCreateBeforeBackup) {

                //$mError = false;
                //$sDescription = 'Создано перед восстановлением из резервной копии от '.$aBackupItem['date'];
                //if(!$this->createBackup($aBackupItem['site_id'], 3, $sDescription, $mError)) throw new Exception($mError);
                BackupToolApi::createNewBackup();

            }

            //$mError = false;
            //if(!$this->recoverBackup($aSiteItem['name'], $aBackupItem['backup_file'], $mError)) throw new Exception($mError);
            BackupToolApi::recoverBackup(array($iBackupId));

            $this->addMessage('Площадка успешно восстановлена!');
            $this->addNoticeReport("Площадка успешно восстановлена из бекапа", "", skLogger::logUsers, "BackupToolModule");
        } catch (Exception $e) {
            $this->addError($e->getMessage());
        }

        $this->actionInit();

        return psComplete;
    }


    public function actionRemove(){

        $aData = $this->get('data');

        BackupToolApi::removeBackup($aData);
        $this->addNoticeReport("Бекап удален", "", skLogger::logUsers, "BackupToolModule");
        $this->actionInit();

        return psComplete;

    }

    public function actionDownloadFile(){

        $aData = $this->get('data');

        $sToken = BackupToolService::getDownloadFileToken($aData);

        if(!$sToken) throw new Exception('Ошибка! Не удалось получиь разрешение на скачивание архива.');

        $sLink = str_replace('index','downloadBackup',CLUSTERGATEWAY);
        $sLink .= '?token='.$sToken;

        $this->setData('link',$sLink);

        // дополнительная библиотека для отображения
        $this->addLibClass( 'BackupFile' );
        $oInterface = new ExtUserFile( 'BackupFile' );
        $this->setExtInterface( $oInterface );

        return psComplete;
    }

}
