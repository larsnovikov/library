<?php
/**
 *
 * @class BackupToolService
 *
 * @author kolesnikov, $Author$
 * @version $Revision$
 * @date $Date$
 * @project JetBrains PhpStorm
 * @package kernel
 */
class BackupToolService extends ServicePrototype {


    public static function makeBackup($sMode = 'schedule'){

        $oClient = skGateway::connect(CLUSTERGATEWAY, APPKEY);

        if(!($oClient instanceof skGatewayClient)) throw new GatewayException('Error init;');

        $iResultStatus = 3;

        $aParam = array($sMode);

        $oClient->addMethod('HostTools', 'makeSiteBackup', $aParam, function($mResult, $mError) use (&$iResultStatus) {

            if($mError){

                $iResultStatus = 4;

                throw new Exception($mError);

            }

        });

        if(!$oClient->doRequest()) return false;

        return $iResultStatus;

    }


    public static function getBackupList(){

        $oClient = skGateway::connect(CLUSTERGATEWAY, APPKEY);

        if(!($oClient instanceof skGatewayClient)) throw new GatewayException('Error init;');

        $aBackupList = array();

        $oClient->addMethod('HostTools', 'getBackupList', null, function($mResult, $mError) use (&$aBackupList) {

            if($mError)
                throw new Exception($mError);

            $aBackupList = $mResult;

        });

        if(!$oClient->doRequest()) return false;

        return $aBackupList;

    }

    /**
     * получение настроек копирование от сервиса sms
     * @throws GatewayException
     * @throws Exception
     * @return bool
     */
    public static function getBackupSetting(){

        $oClient = skGateway::connect(CLUSTERGATEWAY, APPKEY);

        if(!($oClient instanceof skGatewayClient)) throw new GatewayException('Error init;');

        $aSetting = array();

        $oClient->addMethod('HostTools', 'getLocalBackupSetting', null, function($mResult, $mError) use (&$aSetting) {

            if($mError)  throw new Exception($mError);

            $aSetting = $mResult;

        });

        if(!$oClient->doRequest()) return false;

        return $aSetting;

    }

    /**
     * получение общекластерных настроек резервного копирования от сервиса sms
     * @static
     * @throws GatewayException
     * @throws Exception
     * @return array
     */
    public static function getBackupGlobalSetting(){

        $oClient = skGateway::connect(CLUSTERGATEWAY, APPKEY);

        if(!($oClient instanceof skGatewayClient)) throw new GatewayException('Error init;');

        $aSetting = array();

        $oClient->addMethod('HostTools', 'getGlobalBackupSetting', null, function($mResult, $mError) use (&$aSetting) {

            if($mError)  throw new Exception($mError);

            $aSetting = $mResult;

        });

        if(!$oClient->doRequest()) return false;

        return $aSetting;

    }


    /**
     * @static
     * @param $aData
     * @return bool|int
     * @throws GatewayException
     * @throws Exception
     */
    public static function getDownloadFileToken($aData){

        // id site_id mode backup_file date status comments

        $oClient = skGateway::connect(CLUSTERGATEWAY, APPKEY);

        if(!($oClient instanceof skGatewayClient)) throw new GatewayException('Error init;');

        $aParam = array($aData['id']);

        $iToken = 0;

        $oClient->addMethod('HostTools', 'getDownloadFileToken', $aParam, function($mResult, $mError) use (&$iToken) {

            if($mError)  throw new Exception($mError);

            $iToken = $mResult;

        });

        if(!$oClient->doRequest()) return false;

        return $iToken;
    }



}
