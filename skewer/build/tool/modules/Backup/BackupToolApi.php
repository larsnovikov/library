<?php
/**
 * API работы с резевным копированием
 *
 * @class: BackupToolModule
 *
 * @author kolesnikov
 * @version $Revision: 1316 $
 * @date $Date: 2012-11-27 18:31:02 +0400 (Вт., 27 нояб. 2012) $
 *
 */
class BackupToolApi {

    public static function getListItems(){

        return BackupToolService::getBackupList();

    }

    /**
     * обновление времени запуска создания резервной копии (используется сервисом sms)
     * @static
     * @param string $sTime
     * @return bool
     */
    public static function updBackupTime($sTime){

        $aTaskGlobalTime = explode(':', $sTime);

        $mTaskId = ScheduleToolMapper::getIdByName('make_backup');

        // постановка/снятие задачи в планировщике
        if(!$mTaskId){

            $command = array('class'=>'BackupToolService',
                'method'=>'makeBackup',
                'parameters'=>array());

            $command = json_encode($command);

            $aData = array(
                'title' => 'Создание резервной копии',
                'name' => 'make_backup',
                'command' => $command,
                'priority' => '1',
                'resource_use' => '7',
                'target_area' => '3',
                'status' => '1',
                'c_min' => $aTaskGlobalTime[1],
                'c_hour' => $aTaskGlobalTime[0],
                'c_day' => NULL,
                'c_month' => NULL,
                'c_dow' => NULL);

            ScheduleToolMapper::saveItem( $aData );

        }else{

            $aData = array(
                'c_min' => $aTaskGlobalTime[1],
                'c_hour' => $aTaskGlobalTime[0]
            );

            if($mTaskId) $aData['id'] = $mTaskId;

            ScheduleToolMapper::saveItem( $aData );

        }

        return true;

    }


    /**
     * установка параметров резервного копирования
     * @static
     * @param array $aSetting
     * @return bool
     * @throws Exception|GatewayException
     */
    public static function setBackupSetting($aSetting){

        $oClient = skGateway::connect(CLUSTERGATEWAY, APPKEY);

        if(!($oClient instanceof skGatewayClient)) throw new GatewayException('Error init;');

        $aParam = array($aSetting);

        /** @noinspection PhpUnusedParameterInspection */
        $oClient->addMethod('HostTools', 'setLocalBackupSetting', $aParam, function($mResult, $mError) {

            if($mError)  throw new Exception($mError);

        });

        if( !$oClient->doRequest() )
            throw new GatewayException("Ошибка соединения с SMS");


        $mTaskId = ScheduleToolMapper::getIdByName('make_backup');

        // постановка/снятие задачи в планировщике
        if( !$mTaskId ) {

            $aGlobalSetting = BackupToolService::getBackupGlobalSetting();

            $aTaskGlobalTime = explode(':', $aGlobalSetting['bs_time']);

            $command = array('class'=>'BackupToolService',
                'method'=>'makeBackup',
                'parameters'=>array());

            $command = json_encode($command);

            $aData = array(
                'title' => 'Создание резервной копии',
                'name' => 'make_backup',
                'command' => $command,
                'priority' => '1',
                'resource_use' => '7',
                'target_area' => '3',
                'status' => '1',
                'c_min' => $aTaskGlobalTime[1],
                'c_hour' => $aTaskGlobalTime[0],
                'c_day' => NULL,
                'c_month' => NULL,
                'c_dow' => NULL);

            if($mTaskId) $aData['id'] = $mTaskId;

            ScheduleToolMapper::saveItem( $aData );

        }

        return true;

    }


    /**
     * создание новой резервной копии
     * @static
     * @return int
     * @throws Exception|GatewayException
     */
    public static function createNewBackup(){

        $iTaskId = Tasks::addTask('создание пользовательской резервной копии', 1,
                                    Tasks::$taskWeightCritic, 'BackupToolService',
                                    'makeBackup', array('user'));
        if ( !$iTaskId )
            throw new Exception('При создании бэкапа задача не создана');

        $oTasks = new Tasks();
        $iStatus = $oTasks->getTaskStatus($iTaskId);

        return $iStatus;
    }


    /**
     * удаление ранее созданной резервной копии
     * @static
     * @param array $aData
     * @return bool
     * @throws Exception|GatewayException
     */
    public static function removeBackup($aData){

        $oClient = skGateway::connect(CLUSTERGATEWAY, APPKEY);

        if(!($oClient instanceof skGatewayClient)) throw new GatewayException('Error init;');

        /** @noinspection PhpUnusedParameterInspection */
        $oClient->addMethod('HostTools', 'removeBackup', $aData, function($mResult, $mError) {

            if($mError)  throw new Exception($mError);

        });

        if(!$oClient->doRequest()) return false;

        return true;

    }


    /**
     * запрос на восстановление сайта из резервной копии
     * @static
     * @param array $aData
     * @return bool
     * @throws Exception|GatewayException
     */
    public static function recoverBackup($aData){

        // todo положить задачу в очередь

        // дернуть

        $oClient = skGateway::connect(CLUSTERGATEWAY, APPKEY);

        if(!($oClient instanceof skGatewayClient)) throw new GatewayException('Error init;');

        /** @noinspection PhpUnusedParameterInspection */
        $oClient->addMethod('HostTools', 'recoverBackup', $aData, function($mResult, $mError) {

            if($mError)  throw new Exception($mError);

        });

        if(!$oClient->doRequest()) return false;

        return true;
    }

}
