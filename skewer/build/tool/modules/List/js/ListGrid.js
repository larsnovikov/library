/**
 * Хранилище для списка площадок
 */
Ext.define('Ext.tool.ListGrid', {

    extend: 'Ext.grid.Panel',

    cont: null,
    store: null,

    height: '100%',
    width: '100%',
    border: 0,

    columns: [{
        text: 'Module',
        dataIndex: 'id',
        hidden: true
    },{
        dataIndex: 'title',
        flex: 5
    }],
    hideHeaders: true,
    multiSelect: false,

    listeners: {

        itemclick: function( model, record ){

            // поставить блокировку отправки
            processManager.setBlocker();

            // активировать событие выбора элемента
            this.up('panel').selectItem( record.data.id );

            // снять блокировку отправки
            processManager.unsetBlocker();

            // отправить данные
            processManager.postDataIfExists();

        }
    }
});