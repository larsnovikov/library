<?php
/**
 * Created by JetBrains PhpStorm.
 * User: User
 * Date: 26.07.12
 * Time: 13:14
 * To change this template use File | Settings | File Templates.
 */
class ListToolModule extends AdminTabListModulePrototype {

    /**
     * Отдает класс-родитель, насдедники которого могут быть добавлены в дерево процессов
     * в качестве вкладок
     * @return string
     */
    public function getAllowedChildClassForTab() {
        return 'AdminToolTabModulePrototype';
    }
    /**
     * Возвращает true, если модуль, доступный только в режиме системного администратора запрашивается из-под
     * политики более ограниченной в правах.
     * @static
     * @param $aItem
     * @return bool
     */
    protected function checkAccess($aItem) {

        // системному можно все
        if ( CurrentAdmin::isSystemMode() )
            return true;

        // елси есть условие и оно ложно - пропустить
        if ( isset($aItem['condition']) and !$aItem['condition'] ){
            return false;
        }

        // остальным разрешить
        return true;

    }// func

    /**
     * Отдает инициализационный массив для набора вкладок
     * @param int|string $mRowId идентификатор записи
     * @return string[]
     */
    public function getTabsInitList( $mRowId ) {

        foreach ( $this->getModuleList() as $aItem ) {
            if ( $aItem['id'] === $mRowId )
                return array( $aItem['id'] => $aItem['name'] );
        }
        return array();

    }

    /**
     * Набор модулей с условиями
     * !CurrentAdmin::canDo нужно приводить в типу bool иначе может не сработать
     *          (может вернуть null, а не false)
     * @return array
     */
    private function getModuleList() {
        $aModules = array( array(
            'id' => 'users',
            'name' => 'UsersToolModule',
            'title' => 'Пользователи',
            'condition' => (bool)CurrentAdmin::canDo('PolicyToolModule', 'allowUsersView')
        ), array(
            'id' => 'policy',
            'name' => 'PolicyToolModule',
            'title' => 'Политики доступа',
            'condition' => (bool)CurrentAdmin::canDo('PolicyToolModule', 'allowPolicyView')
        ), array(
            'id' => 'forms',
            'name' => 'FormsToolModule',
            'title' => 'Конструктор форм',
        ), array(
            'id' => 'gallery',
            'name' => 'GalleryToolModule',
            'title' => 'Галерея. Профили',
            'condition' => CurrentAdmin::isSystemMode()
        ), array(
            'id' => 'logger',
            'name' => 'LoggerToolModule',
            'title' => 'Система логирования',
        ), array(
            'id' => 'subscr',
            'name' => 'SubscribeToolModule',
            'title' => 'Рассылка',
        ), array(
            'id' => 'shedule',
            'name' => 'ScheduleToolModule',
            'title' => 'Планировщик задач',
            'condition' => CurrentAdmin::isSystemMode()
        ), array(
            'id' => 'backup',
            'name' => 'BackupToolModule',
            'title' => 'Резервные копии',
        ), array(
            'id' => 'seotpl',
            'name' => 'SEOTemplatesToolModule',
            'title' => 'SEO шаблоны',
        ), array(
            'id' => 'taskman',
            'name' => 'TasksManagerToolModule',
            'title' => 'Менеджер процессов',
            'condition' => CurrentAdmin::isSystemMode()
        ), array(
            'id' => 'redirect',
            'name' => 'Redirect301ToolModule',
            'title' => 'Управление редиректами 301',
        ), array(
            'id' => 'patches',
            'name' => 'PatchesToolModule',
            'title' => 'Патчи',
        ), array(
            'id' => 'domains',
            'name' => 'DomainsToolModule',
            'title' => 'Домены',
        ), array(
            'id' => 'mainBanner',
            'name' => 'MainBannerAdmModule',
            'title' => 'Баннеры в шапке',
        ), array(                    'id' => 'status',
            'name' => 'SiteStatusToolModule',
            'title' => 'Статус сайта',
        ));

        $aOut = array();

        foreach($aModules as $aItem) {
            /* Checking access for each module */
            if(!$this->checkAccess($aItem)) continue;
            $aOut[] = array(
                'id' => $aItem['id'],
                'name' => $aItem['name'],
                'title' => $aItem['title'],
            );
        }

        return $aOut;
    }

    /**
     * Задает дополнительные параметры для вкладок
     * @static
     * @param $mRowId
     * @return array
     */
    public function getTabsAddParams( $mRowId ) {
        return array(
            'forms' => array(
                'enableSettings' => 1
            ),
        );
    }


    /**
     * Задает список модулей
     * @return int
     */
    public function actionInit(){

        // команда на инициализацию
        $this->setCmd( 'init' );

        // добавить библиотеку отображения
        $this->addLibClass('ListGrid');

        // запрос списка площадок
        $this->setData('items', $this->getModuleList());

        return psComplete;

    }

}
