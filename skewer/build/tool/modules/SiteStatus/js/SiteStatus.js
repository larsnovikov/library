/**
 * Created with JetBrains PhpStorm.
 * User: User
 * Date: 02.08.12
 * Time: 13:38
 * To change this template use File | Settings | File Templates.
 */
Ext.define('Ext.tool.SiteStatus',{

    extend: 'Ext.panel.Panel',

    execute: function( data) {

        var message;

        if ( data['siteStatus'] )
            message = 'Сайт открыт.';
        else
            message = 'Сайт закрыт.';

        // вывод текста
        this.add({
            padding: 10,
            border: 0,
            html: message
        });

    }

});
