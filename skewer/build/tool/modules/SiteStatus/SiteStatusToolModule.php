<?php
/**
 * Модуль для закрытия/открытия клиентской части сайта
 */
class SiteStatusToolModule extends AdminToolTabModulePrototype {

    /**
     * @var string - Имя модуля
     */
    protected $sTabName = 'Статус';

    // перед выполнением
    protected function preExecute() {
        CurrentAdmin::testControlPanelAccess();
    }

    /**
     * Инициализация
     */
    public function actionInit() {

        /* Строим список изображений */
        $oInterface = new ExtUserFile( 'SiteStatus' );

        $bActive = (bool)SysVar::get('site_open');

        $this->setData('siteStatus',$bActive);


        if ( $bActive ) {

            $oInterface->addDockedItem(array(
                'text' => 'Закрыть площадку',
                'iconCls' => 'icon-stop',
                'action' => 'stop'
            ));


        } else {

            $oInterface->addDockedItem(array(
                'text' => 'Открыть площадку',
                'iconCls' => 'icon-visible',
                'action' => 'start'
            ));

        }

        $this->setExtInterface( $oInterface );

    }

    /**
     * Закрывает площадку
     */
    protected function actionStop() {

        SysVar::set('site_open',0);

        Redirect301ToolApi::makeHtaccessFile();

        $this->actionInit();

    }

    /**
     * Открывает площадку
     */
    protected function actionStart() {

        SysVar::set('site_open',1);

        Redirect301ToolApi::makeHtaccessFile();

        $this->actionInit();

    }

}
