<?php
/**
 * Интерфейс для объектов, работающих с деревьями
 */
interface TreeInterface {

    /**
     * Отдает имя таблицы
     * @static
     * @abstract
     * @return string
     */
    static function getTableName();

//    /**
//     * Отдает имя поля для сортировки
//     * Может возвращать пустую строку
//     * @static
//     * @abstract
//     * @return string
//     */
//    static function getSortFieldName();
//
//    /**
//     * @static возвращает набор допустимых полей для таблицы
//     * @return array
//     */
//    static function getTreeFieldList();

}
