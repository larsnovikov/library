<?php
/**
 * Интерфейс для объектов-записей
 */
interface RowInterface {

    /**
     * Отдает набор пар <имя поля>-<однобуквенный тип> (i/s/...)
     * @abstract
     * @return mixed
     */
    static function getFieldTypes();

    /**
     * Отдает имя таблицы в базе
     * @static
     * @abstract
     * @return string
     */
    static function getTableName();

}
