<?php

/**
 * @class GroupPolicyFuncMapper
 * @extends skModule
 * @project Skewer
 * @package kernel
 *
 * @author Andy Mitrich, $Author: sapozhkov $
 * @version $Revision: 6 $
 * @date 24.01.12 17:10 $
 *
 */

class GroupPolicyFuncMapper extends skMapperPrototype{

    /**
     * @var string Имя текущей таблицы, с которой работает маппер
     */
    protected static $sCurrentTable = 'group_policy_func';

    /**
     * @static Метод возвращает имя рабочей таблицы
     * @return string
     */
    public static function getCurrentTable() {

        return static::$sCurrentTable;
    }

    /**
     * @static Метод возвращает список полей рабочей таблицы
     * @return array
     */
    public static function getParametersList() {

        return static::$aParametersList;
    }

    /**
     * @var array Конфигурация полей таблицы
     */
    protected static $aParametersList = array(
        'policy_id' => 'i:hide:Идентификатор политики',
        'module_name' => 's:str:Имя модуля',
        'param_name' => 's:str:Имя параметра модуля',
        'value' => 's:str:Значение параметра',
        'title' => 's:str:Заголовок действия'
    );

    public static function setGroupFuncParam($aInputData){

        return static::saveItem($aInputData);
    }

    /**
     * Удаляет значение параметра функционального уровня для группы
     * @static
     * @param array $aInputData
     * @return bool
     */
    public static function delGroupFuncParam($aInputData){

        // собрать переменные
        $iPolicyId = isset($aInputData['policy_id']) ? (int)$aInputData['policy_id'] : 0;
        $sModuleClassName = isset($aInputData['module_name']) ? (string)$aInputData['module_name'] : '';
        $sParamName = isset($aInputData['param_name']) ? (string)$aInputData['param_name'] : '';

        // если нет хоть одного параметра - выйти
        if ( !$iPolicyId or !$sModuleClassName or !$sParamName )
            return false;

        // собрать запрос
        $sQuery = '
            DELETE
            FROM `[table_name:q]`
            WHERE
              `policy_id`=[policy_id:i] AND
              `module_name`=[module_name:i] AND
              `param_name`=[param_name:i]
            ;
        ';

        // выполнить запрос
        global $odb;
        $odb->query($sQuery, array(
            'table_name' => static::getCurrentTable(),
            'policy_id' => $iPolicyId,
            'module_name' => $sModuleClassName,
            'param_name' => $sParamName
        ));

        // вернуть результат
        return (bool)$odb->affected_rows;

    }

    /**
     * @static Выборка функциональных политик
     * @param $aFilter
     * @return array|bool
     */
    public static function getGroupFuncData($aFilter=array()){

        return static::getItems($aFilter);
    }


}//class