<?php
/**
 * Класс работы с таблицей css параметров - css_data_params
 *
 * @class: CssParamsMapper
 *
 * @Author: User, $Author: acat $
 * @version: $Revision: 963 $
 * @date: $Date: 2012-10-03 18:05:56 +0400 (Ср., 03 окт. 2012) $
 *
 */

class CssParamsMapper extends skMapperPrototype {

    /**
     * @var string Имя таблицы, с которой работает маппер
     */
    protected  static $sCurrentTable = 'css_data_params';

    /**
     * @static Метод возвращает имя рабочей таблицы
     * @return string
     */
    public static function getCurrentTable() {

        return self::$sCurrentTable;
    }

    /**
     * @static Метод возвращает список полей рабочей таблицы
     * @return array
     */
    public static function getParametersList() {

        return self::$aParametersList;
    }

    /**
     * @var array Конфигурация полей таблицы
     */
    protected static $aParametersList = array(
        'id' => 'i:hide:Номер',
        'name' => 's:str:Псевдоним',
        'group' => 'i:int:Группа',
        'layer' => 's:str:Слой',
        'title' => 's:str:Название',
        'type' => 's:str:Тип',
        'value' => 's:str:Значение',
        'default_value' => 's:str:По умолчанию',
        'range' => 's:str:Диапазон'
    );

    /**
     * Отдает набор параметров по id группы
     * @static
     * @param $iGroupId
     * @return mixed
     */
    public static function getParamListByGroupId( $iGroupId ) {

        // Собираем массив фильтра
        $aFilter = array(
            'select_fields' => array('id','title','value','type'),
            'where_condition' => array(
                'group' => array(
                    'sign' => '=',
                    'value' => $iGroupId
                )
            ),
            'order' => array('field' => 'id', 'way' => 'ASC')
        );

        // запросить значения
        $aResponse = static::getItems($aFilter);

        // отдать
        return $aResponse['items'];

    }


    public static function getGroupByParam( $iParamId ) {

        // Собираем массив фильтра
        $aFilter = array(
            'select_fields' => array('group'),
            'where_condition' => array(
                'id' => array(
                    'sign' => '=',
                    'value' => $iParamId
                )
            )
        );

        // запросить значения
        $aResponse = static::getItems($aFilter);

        // отдать
        return isset($aResponse['items'][0]['group']) ? $aResponse['items'][0]['group'] : false;

    }

    /**
     * @static Метод сохранения записи, без обновления поля value
     * @param array $aInputData
     * @return bool
     */
    public static function insertItem( $aInputData = array() ){

        if ( !$aInputData ) return false;

        global $odb;
        $aData      = array();
        $aFields    = array();
        $aUpdateFields = array();
        /*
         * Проходимся по массиву переданных извне параметров и, при совпадении варианта switch с параметром входного массива,
         * добавляем соответствующий элемент к массиву плейсхолдеров и к строке запроса
         */
        foreach( $aInputData as $sKey=>$sValue ){

            $aFieldParams = static::getParameter($sKey);

            if ( isset($aFieldParams) && sizeof($aFieldParams) ){

                $aFields[] = "`$sKey`=[$sKey:{$aFieldParams['type']}]";
                if ( $sKey!='value' )
                    $aUpdateFields[] = "`$sKey`=[$sKey:{$aFieldParams['type']}]";
                $aData[$sKey] = $sValue;
            }

        }//foreach $aNewsData

        if ( sizeof($aFields) ){

            // Если массив с элементами запроса не пуст - собираем из него строку
            $sFields = implode(',', $aFields);
            $sUpdateFields = implode(',', $aUpdateFields);

            $sTableName = static::getCurrentTable();

            $sQuery = "
                INSERT INTO
                    `$sTableName`
                SET
                    $sFields
                ON DUPLICATE KEY UPDATE
                    $sUpdateFields;";

            // выполнение запроса
            $odb->query($sQuery,$aData);

            return ($odb->affected_rows or $odb->insert_id);
        }

        return false;
    }

    public static function saveParams($aInputData){

        self::saveItem($aInputData);
    }//function saveGroup()

    public static function clearTable(){

        global $odb;

        $sQuery = "TRUNCATE TABLE `".self::$sCurrentTable."`";
        return $odb->query($sQuery);
    }

}
