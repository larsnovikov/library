<?php

/**
 * @class GroupPolicyMapper Класс-маппер, обслуживающий таблицы group_policy, group_policy_data, group_policy_func
 * @extends skModule
 * @project Skewer
 * @package kernel
 *
 * @author Andy Mitrich, $Author: sapozhkov $
 * @version $Revision: 6 $
 * @date 12.12.11 10:32 $
 *
 */

class GroupPolicyMapper extends skMapperPrototype{

    /**
     * @var string Имя текущей таблицы, с которой работает маппер
     */
    protected static $sCurrentTable = 'group_policy';

    /**
     * @static Метод возвращает имя рабочей таблицы
     * @return string
     */
    public static function getCurrentTable() {

        return static::$sCurrentTable;
    }

    /**
     * @static Метод возвращает список полей рабочей таблицы
     * @return array
     */
    public static function getParametersList() {

        return static::$aParametersList;
    }

    /**
     * @var array Конфигурация полей таблицы
     */
    protected static $aParametersList = array(
        'id' => 'i:hide:ID политики',
        'alias' => 's:str:Псевдоним',
        'title' => 's:str:Название политики',
        'area' => 's:str:Область действия',
        'active' => 'i:check:Активность',
        'del_block' => 'i:str:Запретить к удалению'
    );

    /** @var array набор допустимых областей видимостей */
    protected static $aAreaList = array(
        'public'=>'Публичная часть',
        'admin'=>'Админская часть',
    );

    /**
     * Отдает набор допустимых областей видимости
     * @static
     * @return array
     */
    public static function getAreaList() {
        return self::$aAreaList;
    }

    /**
     * Отдает флаг наличия такой области видимости
     * @static
     * @param string $sArea
     * @return bool
     */
    public static function hasArea( $sArea ) {
        return isset(self::$aAreaList[$sArea]);
    }

    // дополнительный набор параметров
    protected static function getAddParamList() {
        return array(
            'id' => array(
                'listColumns' => array(
                    'hidden' => true
                )
            ),
            'title' => array(
                'listColumns' => array(
                    'flex' => 1,
                )
            ),
            'area' => ExtForm::getDesc4SelectFromArray( self::getAreaList() ),
            'read_enable' => array(
                'view' => 'hide',
            ),
            'read_disable' => array(
                'view' => 'hide',
            ),
            'active' => array(
                'listColumns' => array(
                    'width' => 90,
                    'align' => 'center'
                )
            ),
        );
    }

    /**
     * @static Обновление кэша доступных для чтения разделов для политики доступа
     * @param int $iPolicyId ID политики доступа для которой обновляется кэш
     * @param $aInputData Список разделов
     * @param $iPolicyVersion
     * @return bool|mysqli_result
     */
    public static function updateCacheByPolicyId($iPolicyId, $aInputData, $iPolicyVersion){

        global $odb;

        // Формируем строку разделов из массива
        $sReadCache = implode(',', $aInputData['read_access']);

        $aData = array(
            'policy_id'  => $iPolicyId,
            'cache_read' => $sReadCache,
            'version'    => $iPolicyVersion,
        );

        $sQuery =
            "UPDATE
                `group_policy_data`
            SET
                `cache_read` = [cache_read:s],
                `version` = [version:i]
            WHERE
                `policy_id`=[policy_id:i];";

        return $rResult = $odb->query($sQuery, $aData);

    }//function updateCacheByPolicyId()

    /**
     * @static Разобрать список параметров для таблицы
     * @param array $aColumnFilter Фильтр возвращаемых полей
     * @return array
     */
    public static function getPolicyParamsList($aColumnFilter=array()){

        // выходная переменная
        $aOut = array();

        // набор полей
        if ( $aColumnFilter ) {
            $aFields = array();
            foreach ( $aColumnFilter as $sName )
                if ( !in_array($sName,self::$aParametersList) )
                    $aFields[$sName] = self::$aParametersList[$sName];
        } else $aFields = static::$aParametersList;

        // перебрать все поля
        foreach ( $aFields as $sName => $sValues ) {

            // разобрать
            $aValues = explode(':',$sValues);

            // дополнить выходной массив
            $aOut[$sName] = array(
                'name' => $sName,
                'type' => $aValues[0],
                'view' => $aValues[1],
                'title'=> $aValues[2]
            );

        }

        return $aOut;
    }

    /**
     * Отдает список политик по фильтру
     * @static
     * @param $aFilter
     *      limit - ограничение выборки. должно содержать элементы:
     *          start - с какого
     *          count - сколько выводить
     * @return array
     */
    public static function getPolicyList($aFilter=array()){

        if ( !is_array($aFilter) ) $aFilter = array();

        global $odb;

        $aData = array();

        $sLimitCondition = '';
        // Обрабатываем ограничение по количеству выбираемых элементов
        if ( isset($aFilter['limit']) && $aFilter['limit'] ){

            $aData['start_limit'] = $aFilter['limit']['start'];
            $aData['count_limit'] = $aFilter['limit']['count'];
            $sLimitCondition = " LIMIT [start_limit:i], [count_limit:i]";
        }

        $sQuery = "
            SELECT
            SQL_CALC_FOUND_ROWS
                gp.`id`,
                gp.`title`,
                gp.`alias`,
                gp.`active`
            FROM `group_policy` AS gp
            $sLimitCondition;";

        //var_dump($odb->debug($sQuery, $aData));
        $rResult = $odb->query($sQuery, $aData);

        if ( !$rResult ) return false;

        // Собирается результирующий массив
        $aItems = array('items'=>array());

        while( $aRow = $rResult->fetch_array(MYSQLI_ASSOC) ){

            $aItems['items'][] = $aRow;
        }

        // Получаем общее число записей из таблицы
        $sCountQuery = "SELECT FOUND_ROWS() as `rows`;";
        $aCount = $odb->getRow($sCountQuery);
        $aItems['count'] = $aCount['rows'];

        return $aItems;
    }

    public static function getPolicyDetail($iPolicyId){

        global $odb;

        $aData = array('id'=>$iPolicyId);

        $sQuery = "
            SELECT
                gp.`id`,
                gp.`title`,
                gp.`alias`,
                gp.`area`,
                gp.`active`,
                gpd.`start_section`,
                gpd.`read_enable`,
                gpd.`read_disable`
            FROM
                `group_policy` AS gp
            LEFT JOIN `group_policy_data` AS gpd ON gp.`id`=gpd.`policy_id`
            WHERE
                gp.`id`=[id:i];";

        $aResult = $odb->getRow($sQuery, $aData);
        if ( !$aResult ) return false;

        return $aResult;
    }


    public static function delPolicy($iPolicyId) {

        global $odb;

        $aData = array('id'=>$iPolicyId);

        $sQuery = "
            DELETE
            FROM
                `group_policy`
            USING `users` INNER JOIN group_policy ON `users`.`group_policy_id`=group_policy.`id`
            WHERE
                `users`.`id`=[id:i] AND
                group_policy.`del_block`<>1;";

        //var_dump($odb->debug($sQuery, $aData));
        return $odb->query($sQuery, $aData);
    }// func


    public static function getGroupPolicyHeader($iPolicyId) {

        global $odb;

        $sQuery = "
            SELECT
              *
            FROM
              `group_policy`
            WHERE
              id=[id:i];";

        $mRes = $odb->getRow($sQuery, array('id' => $iPolicyId));

        return (count($mRes))? $mRes: false;
    }

}
