<?php

/**
 * @class GroupPolicyDataMapper
 * @extends skModule
 * @project Skewer
 * @package kernel
 *
 * @author Andy Mitrich, $Author: sapozhkov $
 * @version $Revision: 6 $
 * @date 24.01.12 17:13 $
 *
 */

class GroupPolicyDataMapper extends skMapperPrototype{

    /**
     * @var string Имя текущей таблицы, с которой работает маппер
     */
    protected static $sCurrentTable = 'group_policy_data';

    /** @var string Имя ключевого поля */
    protected static $sKeyFieldName = 'policy_id';

    /**
     * @static Метод возвращает имя рабочей таблицы
     * @return string
     */
    public static function getCurrentTable() {

        return static::$sCurrentTable;
    }

    /**
     * @static Метод возвращает список полей рабочей таблицы
     * @return array
     */
    public static function getParametersList() {

        return static::$aParametersList;
    }

    /**
     * @var array Конфигурация полей таблицы
     */
    protected static $aParametersList = array(
        'policy_id' => 'i:hide:Идентификатор политики',
        'start_section' => 'i:hide:Стартовый раздел после авторизации',
        'read_disable' => 's:str:Список разделов, начиная с которых вглубь дерева разделов запрещается доступ',
        'read_enable' => 's:str:Список разделов, начиная с которых вглубь дерева разрешается доступ только на чтение',
        'cache_read' => 's:str:Кэш списка разделов, доступных для чтения',
        'version' => 'i:check:Счетчик версий для контроля кеша политик'
    );

    /**
     * @static Метод выборки списка доступных для чтения разделов по id политики
     * @param $iPolicyId
     * @return array
     */
    public static function readAccessFields($iPolicyId){

        if ( !$iPolicyId ) return array();

        // Формируем фильтр
        $aFilter = array(
            'select_fields' => array( 'read_disable','read_enable'),
            'where_condition' => array(
                'policy_id' => array(
                    'sign' => '=',
                    'value' => $iPolicyId
                )
            )
        );

        // Вызываем универсальный метод выборки
        $aTempArray = static::getItems($aFilter);

        return $aTempArray['items'][0];
    }

    /**
     * @static Считываем кэш, содержащий список разделов, доступных для чтения
     * @param $iPolicyId!
     * @return array
     */
/*    public static function getGroupPolicyData($iPolicyId){

        global $odb;

        $sQuery = "
            SELECT
              *
            FROM
              `group_policy`
            WHERE
              id=[id:i];";

        var_dump($odb->debug($sQuery, array('id' => $iPolicyId)));

        $mRes = $odb->getRow($sQuery, array('id' => $iPolicyId));

        return (count($mRes))? $mRes: false;
    }*/

    /**
     * @static Считываем кэш, содержащий список разделов, доступных для чтения
     * @param $aFilter
     * @return array
     */
    public static function getGroupPolicyData($aFilter) {

        // Выполняем выборку
        $aTempArray = static::getItems($aFilter);

        if ( $aTempArray['items'] ) return $aTempArray['items'][0];
        else return array();
    }// func

    /**
     * Сохраняет данные доступа для группы
     * @static
     * @param $aInputData
     * @return bool|int
     */
    public static function setGroupAccess($aInputData){
       return static::saveItem($aInputData);
    }

    /**
     * Создает запись данных доступа, если она отсутствует
     * @static
     * @param int $iID
     */
    public static function createRowIfNotExists($iID) {
        $iID = (int)$iID;
        if ( !self::getItem($iID) ) {
            $sQuery = "INSERT INTO `[table_name:q]` SET `policy_id`=[id:i]";
            global $odb;
            $odb->query($sQuery, array(
                'id' => $iID,
                'table_name' => self::getCurrentTable()
            ));
        }
    }

}