<?php
/**
 * Маппер для работы с базой SEO данных
 */
class SEODataMapper extends skMapperPrototype {

    /** @var string Имя таблицы, с которой работает маппер */
    protected static $sCurrentTable = 'seo_data';

    /** @var array Конфигурация полей таблицы */
    protected static $aParametersList = array(
        'id' => 'i:hide:ID',
        'group' => 's:str:Группа данных',
        'row_id' => 'i:int:Id целевой записи',
        'title' => 's:str:Название страницы',
        'keywords' => 's:text:Ключевые слова',
        'description' => 's:text:Описание страницы',
        'frequency' => 's:str:Частота обновления',
        'priority' => 's:int:Приоритет',
    );

    /**
     * Отдает список доступных значений поля "частота обновления"
     * @static
     * @return array
     */
    public static function getFrequencyList() {
        return array(
            '' => '-Не задано-',
            'newer' => 'Никогда',
            'dayly' => 'Каждый день',
            'weekly' => 'Каждую неделю',
            'always' => 'Всегда',
        );
    }

}
