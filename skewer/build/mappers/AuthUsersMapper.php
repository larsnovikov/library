<?php

/**
 * @class AuthUsersMapper Класс-маппер, обслуживающий таблицы user, user_policy_data, user_policy_func
 * @extends skModule
 * @project Skewer
 * @package kernel
 *
 * @author andymitrich, $Author: acat $
 * @version $Revision: 1264 $
 * @date 12.12.11 10:32 $
 *
 */

class AuthUsersMapper extends skMapperPrototype {

    private static $sDefaultLogin = 'default';
    /**
     * @var string Имя таблицы, с которой работает маппер
     */
    protected static $sCurrentTable = 'users';

    /**
     * @static Метод возвращает список полей рабочей таблицы
     * @return array
     */
    public static function getParametersList() {

        return self::$aParametersList;
    }

    /**
     * @var array Конфигурация полей таблицы
     */
    protected static $aParametersList = array(
        'id' => 'i:hide:ID пользователя',
        'global_id' => 'i:hide:Идентификатор пользователя на мастер сервере',
        'login' => 's:str:Логин',
        'pass' => 's:pass:Пароль',
        'group_policy_id' => 's:str:Политика доступа',
        'active' => 's:check:Активна',
        'lastlogin' => 's:str:Последний заход',
        'name' => 's:str:Имя пользователя',
        'email' => 's:str:E-mail пользователя',
        'cache' => 's:html:Скомпилированное значение политики доступа',
        'version' => 's:str:Счетчик версий для контроля кеша политик'
    );

    public static function checkUser( $aInputData, $bAuto = false ){

        if ( !$aInputData ) return false;

        global $odb;

        $sPasswordCondition = ( !$bAuto )? " (u.`pass`=[password:s] OR u.`global_id`>0 ) AND": '';
        $sActiveCondition = (isset($aInputData['active'])) ? "u.`active`=[active:s] AND":'';

        /* Только активные политики, либо политика системного администратора */
        $sActiveConditionGroup = (isset($aInputData['active'])) ? '(gp.`active`='.Policy::stActive.' OR gp.`active`='.Policy::stSys.') AND':'';


        $sQuery = "
            SELECT
                u.*,
                gp.id AS policy_id,
                gp.alias AS policy_alias
            FROM
                `users` AS u
            INNER JOIN `group_policy` AS gp ON u.`group_policy_id`=gp.`id`
            WHERE
                u.`login`=[login:s] AND
                $sPasswordCondition
                $sActiveCondition
                $sActiveConditionGroup
                gp.`area`=[login_area:s];";

        $aUser = $odb->getRow($sQuery, $aInputData);
        unSet($aUser['pass']);
        return ( $aUser )? $aUser: false;
    }

    /**
     * @static Получаем информацию о пользователе по его ID
     * @param $iUserId
     * @param array $aFileds - набор требуемых полей
     * @return array|bool
     */
    public static function getUserData($iUserId, $aFileds=array()){

        if ( !$iUserId ) return false;

        global $odb;

        $aData = array(
            'id' => $iUserId,
            'fields' => $aFileds ? '`'.implode('`,`',$aFileds).'`' : '*'
        );

        $sQuery = "
            SELECT [fields:q]
            FROM `users`
            WHERE id = [id:i]
            LIMIT 0,1;";

        $aResult = $odb->getRow($sQuery, $aData);

        return ( sizeof($aResult) )? $aResult: false;
    }// function getUserData()

    /**
     * @static Получаем информацию о пользователе по его логину
     * @param $sUserName
     * @return array|bool
     */
    public static function getUserDataByName($sUserName){

        if ( !$sUserName ) return false;

        global $odb;

        $aData = array(
            'login' => $sUserName,
        );

        $sQuery = "
            SELECT *
            FROM `users`
            WHERE login = [login:s]
            LIMIT 0,1;";

        $aResult = $odb->getRow($sQuery, $aData);

        return ( sizeof($aResult) )? $aResult: false;
    }// function getUserData()

    /**
     * @static Метод для получения информации из таблицы для дефолтного пользователя по логину default
     * @return array|bool
     */
    public static function getDefaultUserData(){

        global $odb;

        $aData = array( 'login' => self::$sDefaultLogin );

        $sQuery = "
            SELECT
                *
            FROM
                `users`
            WHERE
                login = [login:s]
            LIMIT
                0,1;";

        $aResult = $odb->getRow($sQuery, $aData);

        return ( sizeof($aResult) )? $aResult: false;
    }// function getUserData()

    /**
     * @static Выбираем информацию по групповой политике доступа
     * @param $iGroupPolicyId
     * @return array|bool
     */
    public static function getGroupPolicy($iGroupPolicyId){

        if ( !$iGroupPolicyId ) return false;

        global $odb;

        $aData = array( 'policy_id' => $iGroupPolicyId );

        $sQuery = "
            SELECT
                *
            FROM
                `group_policy` AS gp
            INNER JOIN `group_policy_data` AS gpd ON gpd.policy_id = gp.id
            WHERE
                gp.id = [policy_id:i]
            LIMIT
                0,1;";

        //echo "<XMP>"; print_r($odb->debug($sQuery, $aData)); echo "</XMP>";
        $aResult = $odb->getRow($sQuery, $aData);

        return ( sizeof($aResult) )? $aResult: false;
    }//function getGroupPolicy()

    /**
     * Возвращает список пользователей с возможность фильрации
     * @static
     * @param $aFilter - массив для фильтрации
     *      limit - ограничение по количеству - массив с 2 элементами
     *          start - с какого
     *          count - сколько
     *      order - порядок - массив с 2 элементами
     *          field - имя поля
     *          direction - направление ( ASC / DESC )
     *      policy - int id политики
     *      active - int активность записей
     *      search - str полнотекстовый поиск по полям login, name и email
     *      has_pass - bool наличие пароля
     * @return array
     */
    public static function getUsersList($aFilter=array()){

        if ( !is_array($aFilter) )
            $aFilter = array();

        global $odb;

        $aData = array();

        $sLimitCondition = '';
        $sWhereCondition = '';
        $sOrderCondition = '';

        // Обрабатываем ограничение по количеству выбираемых элементов
        if ( isset($aFilter['limit']) && $aFilter['limit'] ){

            $aData['start_limit'] = $aFilter['limit']['start'];
            $aData['count_limit'] = $aFilter['limit']['count'];
            $sLimitCondition = " LIMIT [start_limit:i], [count_limit:i]";
        }

        /* Сортировки */
        if ( isset($aFilter['order']) && $aFilter['order'] ){

            $aData['order_field']     = $aFilter['order']['field'];
            $aData['order_direction'] = $aFilter['order']['direction'];

            $sOrderCondition = " ORDER BY [order_field:q] [order_direction:q]";
        }

        /* Фильтр по групповой политике */
        if ( isset($aFilter['policy']) && $aFilter['policy'] > 0 ){

            $aData['policy_id'] = $aFilter['policy'];

            $sWhereCondition .= " AND u.group_policy_id=[policy_id:i]";
        }

        /* Фильтр по статусу */
        if ( isset($aFilter['active']) AND $aFilter['active'] > -1 ){

            $aData['active'] = $aFilter['active'];

            $sWhereCondition .= " AND u.active=[active:i]";
        }

        /* Полнотекстовый поиск по полю */
        if ( isset($aFilter['search']) AND !empty($aFilter['search']) ){

            $aData['search'] = $aFilter['search'];

            $sWhereCondition .= " AND ( `login` LIKE '%[search:q]%' OR  `email` LIKE '%[search:q]%' OR  `name` LIKE '%[search:q]%')";
        }

        /* Только с паролем */
        if ( isset($aFilter['has_pass']) AND $aFilter['has_pass'] ){
            $sWhereCondition .= " AND `pass`!='' ";
        }

        if ( !CurrentAdmin::isAdminPolicy() ){

            $aData['policy_id'] = 1;
            /* todo Нужно переделать таким образом, чтобы можно было определять какие политики являются админскими */
            $sWhereCondition .= " AND (u.alias!='admin' OR u.alias!='sysadmin') ";
        }

        $sQuery = "
            SELECT
            SQL_CALC_FOUND_ROWS
                u.`id`,
                u.`login` AS `login`,
                u.`lastlogin` AS `lastlogin`,
                u.`name` AS `name`,
                u.`active` AS `active`,
                u.`email` AS `email`,
                gp.`title` AS `policy`
            FROM
                `users` AS u
            LEFT JOIN `group_policy` AS gp ON u.group_policy_id=gp.id
            WHERE 1
            $sWhereCondition
            $sOrderCondition
            $sLimitCondition;";

        $rResult = $odb->query($sQuery, $aData);

        if ( !$rResult ) return false;

        // Собирается результирующий массив
        $aItems = array('items'=>array());

        while( $aRow = $rResult->fetch_array(MYSQLI_ASSOC) ){

            $aItems['items'][] = $aRow;
        }

        // Получаем общее число записей из таблицы
        $sCountQuery = "SELECT FOUND_ROWS() as `rows`;";
        $aCount = $odb->getRow($sCountQuery);
        $aItems['count'] = $aCount['rows'];

        return $aItems;
    }

    public static function getUserDetail($iUserId){

        global $odb;

        $aData = array('id'=>$iUserId);

        $sQuery = "
            SELECT
                u.`id`,
                u.`login`,
                u.`lastlogin`,
                u.`group_policy_id`,
                u.`name`,
                u.`active`,
                u.`email`,
                gp.`title` AS `policy`
            FROM
                `users` AS u
            LEFT JOIN `group_policy` AS gp ON u.group_policy_id=gp.id
            WHERE
                u.`id`=[id:i];";

        //var_dump($odb->debug($sQuery, $aData));
        $aResult = $odb->getRow($sQuery, $aData);

        if ( !$aResult ) return false;

        return $aResult;
    }

    public static function delUser($iUserId){

        global $odb;

        $aData = array('id'=>$iUserId);

        $sQuery = "
            DELETE
            FROM
                `users`
            WHERE
                `id`=[id:i] AND
                `del_block`<>1;";

        $odb->query($sQuery, $aData);

        return $odb->affected_rows;
    }

}//class
