<?php
/**
 * Created by JetBrains PhpStorm.
 * User: user
 * Date: 11.04.12
 * Time: 17:57
 * To change this template use File | Settings | File Templates.
 */
class TaskMapper extends skMapperPrototype{

    /**
     * @var string Имя текущей таблицы, с которой работает маппер
     */
    protected static $sCurrentTable = 'task_queue';

    /**
     * @var array Конфигурация полей таблицы
     */
    protected static $aParametersList = array(

        'id' => 'i:hide:id задания',
        'global_id' => 'i:hide:Глобальный id',
        'title' => 's:str:Название задания',
        'command' => 's:text:Команда',
        'priority' => 's:str:Приоритет',
        'resource_use' => 'i:int:Ресурсоемкость',
        'target_area' => 'i:int:Область действия',
        'upd_time' => 's:date:Время модификации',
        'mutex' => 'i:int:Мьютекс',
        'status' => 'i:int:Статус',
        'exec_counter' => 'i:int:Количество вызовов'
    );

    /**
     * Устанавливает статус задания в локальной очереди
     * @static
     * @param $id
     * @param $iStatus
     * @return bool
     */
    public static function setStatus($id, $iStatus){

        $sQuery = "UPDATE `[table:q]`
                    SET `status` = [status:i], `mutex` = [setmutex:i]
                    WHERE `id`=[id:i] AND `mutex` = [mutex:i]";

        $aData = array(
            'table' => self::$sCurrentTable,
            'status' => $iStatus,
            'id' => $id,
            'mutex' => ($iStatus==Tasks::$taskStatusNew)?0:1,
            'setmutex' => ($iStatus==Tasks::$taskStatusRunning)?1:0,
        );

        global $odb;
        $odb->query($sQuery,$aData);

        return ($odb->affected_rows>0)?true:false;

    }

    /**
     * Устанавливает глобальный идентификатор задания
     * @static
     * @param int $id
     * @param int $iGlobalId
     * @return bool
     */
    public static function setGlobalId($id, $iGlobalId){

        $sQuery = "UPDATE `[table:q]`
                    SET `global_id` = [global_id:i]
                    WHERE `id`=[id:i]";

        $aData = array(
            'table' => self::getCurrentTable(),
            'global_id' => $iGlobalId,
            'id' => $id,
        );

        global $odb;
        $odb->query($sQuery,$aData);

        return ($odb->affected_rows>0)?true:false;

    }

    /**
     * Метод сохранения записи
     * @static
     * @param array $aInputData
     * @param string $sMutexMode
     * @return bool|int
     */
    public static function saveItem( $aInputData = array(), $sMutexMode='undefined' ) {

        /*
         * Модифицирована с учетом мьютексов
         */

        if ( !$aInputData ) return false;

        // имя ключевого поля - обязательное
        $sKeyFieldName = static::getKeyFieldName();

        // флаг наличия поля идентификатора
        $bFoundId = false;
        // значение поля идентификатора
        $mIdKeyVal = 'NULL';

        // Хвост запроса для мьютекса
        $sMutexQuery = '';


        global $odb;
        $aData      = array();
        $aFields    = array();

        /*******************************************
         * Обрабатываем режим использования мьютекса
         */

        if(isset($aInputData['mutex'])){

            $iMutex = $aInputData['mutex'];
            unset($aInputData['mutex']);

            if($sMutexMode == 'undefined' and isset($aInputData['status']))
                switch($aInputData['status']){

                    case 2:
                    case 3:
                    case 4:
                    case 6:
                        $sMutexMode = 'mutex';
                        break;

                    case 1:
                        $sMutexMode = 'freeTask';
                        break;

                    default:
                        $sMutexMode = 'withoutMutex';
                        break;
                } // switch mutexMode


            switch($sMutexMode){

                case 'withoutMutex':
                    $sMutexQuery = '';
                    break;

                case 'freeTask':
                    $sMutexQuery = ' AND `mutex`=0 ';
                    break;

                default:
                    $sMutexQuery = ' AND `mutex`='.$iMutex.' ';
                    break;

            } // switch

        } // if mutex

        /*
         * Проходимся по массиву переданных извне параметров и, при совпадении варианта switch с параметром входного массива,
         * добавляем соответствующий элемент к массиву плейсхолдеров и к строке запроса
         */
        foreach( $aInputData as $sKey=>$sValue ){

            // запросить описание поля
            $aFieldParams = static::getParameter($sKey);

            // если поле найдено
            if ( $aFieldParams ){

                // если поле - первичный ключ
                if ( $sKeyFieldName === $sKey ) {

                    if ( $sValue ){
                        /** @fixme Что это? (real_escape_string)  */
                        $mIdKeyVal = $odb->real_escape_string($sValue);
                        $bFoundId = true;
                    }
                    else {

                        $mIdKeyVal = 'NULL';
                    }

                } else {

                    // тип данных
                    $sType = $aFieldParams['type'];

                    if ( is_null($sValue) ) {
                        $sType = 'q';
                        $sValue = 'NULL';
                    }

                    // добавить в список элементов на сохранение
                    $aFields[] = "`$sKey`=[$sKey:{$sType}]";

                    // добавить в массив для меток в запросе
                    $aData[$sKey] = $sValue;

                }

            }

        }//foreach $aNewsData

        if ( sizeof($aFields) ){

            // Если массив с элементами запроса не пуст - собираем из него строку
            $sFields = implode(',', $aFields);

            // имя паблицы
            $aData['sTableName'] = static::getCurrentTable();

            // имя и значение ключевого поля
            $aData['sKeyFieldName'] = $sKeyFieldName;
            $aData['mIdKeyVal'] = $mIdKeyVal;

            // если найден id
            if ( $bFoundId ) {

                $sQuery = "
                    UPDATE `[sTableName:q]`
                    SET $sFields
                    WHERE `[sKeyFieldName:q]`=[mIdKeyVal:q] $sMutexQuery";

            } else {

                $sQuery = "
                    INSERT INTO `[sTableName:q]`
                    SET $sFields";

            }

            // выполнение запроса
            $odb->query($sQuery,$aData);

            // если сохранение
            if ( $bFoundId ) {

                if ( $odb->affected_rows!=-1 )
                    return $aInputData[$sKeyFieldName];
                else
                    return false;

            } else {
                // вставка - вернуть новый id
                return $odb->insert_id;
            }

        }

        return false;
    }


    /**
     * метод проверяет существование задачи с заданной командой
     * @static
     * @param string $sCommand
     * @return int
     */
    public static function findTaskWithCommand($sCommand){

        $sQuery = "SELECT id FROM `[table:q]`
                    WHERE command=[command:s] AND (status=[status_new:i] OR status=[status_running:i])";

        $aData = array(
            'table' => self::getCurrentTable(),
            'command' => $sCommand,
            'status_new' => Tasks::$taskStatusNew,
            'status_running' => Tasks::$taskStatusRunning
        );

        global $odb;
        $rResult = $odb->query($sQuery,$aData);

        $aRow = $rResult->fetch_assoc();

        return $aRow ? (int)$aRow['id'] : 0;

    }

    /**
     * метод маркерует задачу, выбранную для обработки
     * @static
     * @param int $iItemId
     * @return int
     */
    public static function holdItem($iItemId){

        $sQuery = "UPDATE `[table:q]` SET `mutex` = [mutex:i] WHERE `id`=[id:i] AND `mutex` = [not_mutex:i]";

        $aData = array(
            'table' => self::getCurrentTable(),
            'id' => $iItemId,
            'mutex' => 1,
            'not_mutex' => 0
        );

        global $odb;
        $odb->query($sQuery,$aData);

        return $odb->affected_rows;

    }


    /**
     * метод выбирает из потока приоритетную задачу для выполнения
     * @static
     * @return bool|int
     */
    public static function getPriorityItem(){

        $iItemId = 0;

        $sQuery = "SELECT id FROM `[table:q]`
                    WHERE `mutex` = [mutex:i] AND (status = 1 OR status = 6)
                    ORDER BY `priority` DESC, `upd_time` ASC LIMIT 1";

        $aData = array(
            'table' => self::getCurrentTable(),
            'mutex' => 0,
        );

        global $odb;
        $rResult = $odb->query($sQuery,$aData);

        if ( !$rResult ) return false;

        if( $aRow = $rResult->fetch_assoc() ){

            $iItemId = $aRow['id'];

        }

        return $iItemId;
    }


    /**
     * метод возвращает суммарный индекс загруженности кластера
     * @static
     * @return bool|int
     */
    public static function getSumWeight(){

        $iSubWeight = 0;

        $sQuery = "SELECT resource_use
                    FROM `[table:q]`
                    WHERE `mutex` = [mutex:i]";

        $aData = array(
            'table' => self::getCurrentTable(),
            'mutex' => 1,
        );

        global $odb;
        $rResult = $odb->query($sQuery,$aData);

        if ( !$rResult ) return false;

        while( $aRow = $rResult->fetch_assoc() ){

            $iSubWeight += $aRow['resource_use'];

        }

        return $iSubWeight;

    }

    /**
     * удаление старых повисших задач
     * @static
     * @return int
     */
    public static function delOldOutTimeTasks(){

        $sQuery = "UPDATE `[table:q]` SET `mutex` = 0, `status` = [status_new:s]  WHERE `mutex`=1 AND `upd_time` < DATE_SUB(NOW(),INTERVAL 5 HOUR)";

        $aData = array(
            'table' => self::getCurrentTable(),
            'status_new' => Tasks::$taskStatusTimeout
        );

        global $odb;
        $odb->query($sQuery,$aData);

        $sQuery = "DELETE FROM `[table:q]` WHERE `upd_time` < DATE_SUB(NOW(),INTERVAL 7 DAY)";

        $aData = array(
            'table' => self::getCurrentTable(),
            'status' => Tasks::$taskStatusComplete,
        );

        $odb->query($sQuery,$aData);

        return $odb->affected_rows;


    }

    /**
     * удаление старых выполненных задач
     * @static
     * @return int
     */
    public static function delOldTasks(){

        $sQuery = "DELETE FROM `[table:q]` WHERE `status`=[status:i] AND `upd_time` < DATE_SUB(NOW(),INTERVAL 12 HOUR)";

        $aData = array(
            'table' => self::getCurrentTable(),
            'status' => Tasks::$taskStatusComplete,
        );

        global $odb;
        $odb->query($sQuery,$aData);

        return $odb->affected_rows;

    }

    /**
     * поиск и маркировка повисших задач
     * @static
     * @return int
     */
    public static function findOutTimeTasks(){

        $sQuery = "UPDATE `[table:q]` SET `status` = [new_status:i] WHERE `status`=[old_status:i] AND `upd_time` < DATE_SUB(NOW(),INTERVAL 1 HOUR)"; // HOUR

        $aData = array(
            'table' => self::getCurrentTable(),
            'old_status' => Tasks::$taskStatusRunning,
            'new_status' => Tasks::$taskStatusTimeout,
        );

        global $odb;
        $odb->query($sQuery,$aData);

        return $odb->affected_rows;

    }

    /**
     * @static
     * @return bool|mysqli_result
     */
    public static function clearLog(){

        $sQuery = "DELETE FROM `".self::getCurrentTable()."`;";

        global $odb;

        return $odb->query($sQuery);

    }




}
