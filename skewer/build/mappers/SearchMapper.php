<?php

/**
 * @class SearchMapper
 * @extends skModule
 * @project Skewer
 * @package kernel
 *
 * @author Andy Mitrich, $Author: sapozhkov $
 * @version $Revision: 6 $
 * @date 02.02.12 16:41 $
 *
 */

class SearchMapper extends skMapperPrototype {

    protected  static $sCurrentTable = 'search_index';

    public static function getCurrentTable() {

        return self::$sCurrentTable;
    }

    public static function getParametersList() {

        return self::$aParametersList;
    }

    protected static $aParametersList = array(

        'id' => 'i:str:Идентификатор записи',
        'search_text' => 's:str:Полный текст страницы по которому осуществляется поиск',
        'search_title' => 's:str:Заголовок страницы для поисковой выдачи',
        'status' => 'i:str:состояние записи',
        'href' => 's:str:Ссылка на страницу',
        'class_name' => 's:str:Имя модуля поставщика данных',
        'object_id' => 'i:str:Идентификатор записи поставщика данных',
        'lang' => 'i:str:Идентификатор языка',
        'section_id' => 'i:str:ID раздела',
        'modify_date' => 's:str:Дата обновления записи',
        'change_frequency' => 's:str:Частота обхода страниц для sitemap',
        'priority' => 's:str:Приоритет для sitemap'
    );

    /**
     * @static
     * @param $aInputData
     * @return bool|mysqli_result
     */
    public static function removeFromIndex($aInputData){

        global $odb;

        $sQuery = "
            DELETE
            FROM
                `search_index`
            WHERE
                `class_name` = [class_name:s] AND
                `object_id` = [object_id:i];";

        return $odb->query($sQuery, $aInputData);
    }

    /**
     * @static
     *
     */
    public static function executeSearch($sSearchText, $iPage, $iOnPage, $iSearchType, $iSearchSection){

        global $odb;

        $aData = array(
            'start' => ($iPage-1)*$iOnPage,
            'record_count' => $iOnPage
        );

        switch( $iSearchType ){

            case 0:
            case 1:

                $aTextParts = explode(' ', $sSearchText);
                $aTextParts = array_diff($aTextParts, array(''));

                foreach( $aTextParts as &$sPart ){

                    $sPart = trim($sPart);

                    if ( $iSearchType==0 )
                        $sPart = $sPart.'*';
                    if ( $iSearchType==1 )
                        $sPart = '+'.$sPart.'*';
                }

                $aData['search_text'] = implode(' ', $aTextParts);
            break;

            case 2:

                $aData['search_text'] = '"'.$sSearchText.'"';
            break;
        }

        $sSectionCondition = '';
        if ( $iSearchSection ){

            $aData['section_id'] = $iSearchSection;
            $sSectionCondition = ' `section_id` = [section_id:i] AND ';
        }

        $sQuery = "
            SELECT
            SQL_CALC_FOUND_ROWS
                `search_title`,
                `search_text`,
                `href`,
                `class_name`,
                `section_id`,
                MATCH (`search_title`, `search_text`) AGAINST ([search_text:s] IN BOOLEAN MODE) AS `rel`
            FROM `search_index`
            WHERE
                $sSectionCondition
                MATCH (`search_title`, `search_text`) AGAINST ([search_text:s] IN BOOLEAN MODE) > 0
            ORDER BY `rel` DESC
            LIMIT [start:i], [record_count:i];";

        //echo "<XMP>"; print_r($odb->debug($sQuery, $aData)); echo "</XMP>";
        $rResult = $odb->query($sQuery, $aData);

        $aItems = array();
        while( $aRow = $rResult->fetch_assoc() ){

            $aRow['module_title'] = skConfig::get('buildConfig.Page.modules.'.$aRow['class_name'].'.title');
            $aItems['items'][] = $aRow;
        }

        $aRows = $odb->getRow("SELECT FOUND_ROWS() as rows;");
        $aItems['count'] = $aRows['rows'];

        return $aItems;
    }

    public static function clearSearchTable(){

        global $odb;

        $sQuery = "TRUNCATE TABLE `".self::$sCurrentTable."`";
        return $odb->query($sQuery);
    }

}//class