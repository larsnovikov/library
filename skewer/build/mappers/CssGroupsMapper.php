<?php
/**
 * Класс работы с таблицей групп css параметров - css_data_groups
 *
 * @class: CssGroupsMapper
 *
 * @Author: User, $Author: sapozhkov $
 * @version: $Revision: 6 $
 * @date: $Date: 2012-05-18 10:52:53 +0400 (Пт., 18 мая 2012) $
 *
 */

class CssGroupsMapper extends skMapperPrototype {

    /**
     * @var string Имя таблицы, с которой работает маппер
     */
    protected  static $sCurrentTable = 'css_data_groups';

    /**
     * @static Метод возвращает имя рабочей таблицы
     * @return string
     */
    public static function getCurrentTable() {

        return self::$sCurrentTable;
    }

    /**
     * @static Метод возвращает список полей рабочей таблицы
     * @return array
     */
    public static function getParametersList() {

        return self::$aParametersList;
    }

    /**
     * @var array Конфигурация полей таблицы
     */
    protected static $aParametersList = array(
        'id' => 'i:hide:Номер',
        'name' => 's:str:Псевдоним',
        'title' => 's:str:Название',
        'parent' => 'i:int:Родительская группа',
        'layer' => 's:str:Слой',
        'visible' => 'i:check:Видимость',
        'priority' => 'i:int:Положение'
    );

    public static function saveGroup($aInputData){

        $aInputData['parent'] = self::getGroupIdByName($aInputData['parent'], $aInputData['layer']);

        self::saveItem($aInputData);
    }//function saveGroup()

    public static function getGroupIdByName( $sGroupName, $sLayerKey='default' ){

        $aFilter = array(
                'select_fields' => array(
                    'id'
                ),
                'where_condition' => array(
                    'name' => array(
                        'sign' => 'LIKE',
                        'value' => $sGroupName
                    ),
                    'layer' => array(
                        'sign' => 'LIKE',
                        'value' => $sLayerKey
                    )
                )
            );

        $aTempParent = self::getItems($aFilter);

        return ( isset($aTempParent['items'][0]['id']) )? $aTempParent['items'][0]['id']: 0;

    }//function getGroupIdByName()

    public static function clearTable(){

        global $odb;

        $sQuery = "TRUNCATE TABLE `".self::$sCurrentTable."`";
        return $odb->query($sQuery);
    }

}
