<?php
/**
 * Класс helper для вывода системных и сервисных данных в шаблонах
 * @class Vars
 * @author ArmiT, $Author: sapozhkov $
 * @version $Revision: 6 $
 * @date $Date: 2012-05-18 10:52:53 +0400 (Пт., 18 мая 2012) $
 * @project Skewer
 * @package Build
 *
 */
class Vars {

    /**
     * Возвращает текущую дату в указанном формате
     * @param  string $sFormat Формат возвращаемого значения
     * @return string
     */
    public function getDate($sFormat) {

        return date($sFormat);
    }// func

    /**
     * Возвращает базовый URL
     * @return string|bool
     */
    public function baseUrl() {

        return skConfig::get('url.root');
    }// func
}// class
