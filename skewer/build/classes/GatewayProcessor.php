<?php
/**
 *
 * @class GatewayProcessor
 *
 * @author ArmiT, $Author: acat $
 * @version $Revision: 1642 $
 * @date $Date: 2013-02-01 15:19:54 +0400 (Пт, 01 фев 2013) $
 * @project JetBrains PhpStorm
 * @package kernel
 */
class GatewayProcessor extends skProcessorPrototype implements skProcessorInterface {

    /**
     * Экземпляр сервера
     * @var null|skGatewayServer
     */
    protected $oGatewayServer = null;

    /**
     *  Вызывается после прихода заголовка
     * @param skGatewayServer $oServer
     * @param array $aHeaders
     */
    public function onHeaderLoad(&$oServer, $aHeaders) {

        /* Здесь смотрим - если есть флаг того, что пакет зашифрован, то пытаемся по заголовкам
           определить, кто прислал пакет и получить ключ площадки */

        $oServer->setKey(APPKEY);

    }// func
    
    /**
     * Шлюз работает всегда
     */
    public function isAllowedStart(){
        return true;
    }// func    

    /**
     * Корневой метод выполнения процессора
     * @static
     * @return bool|string
     */
    final public function build() {


        skConfig::set('debug.parser', true);
        /* Режим отладки дляшаблонизатора */
        (skConfig::get('debug.parser'))? skTwig::enableDebug(): skTwig::disableDebug();

        $this->oGatewayServer = new skGatewayServer(skGatewayServer::StreamTypeEncrypt);

        $this->oGatewayServer->addParentClass('ServicePrototype');

        $oCrypt = new skBlowfish();
        $oCrypt->setIv(skConfig::get('security.vector'));

        $this->oGatewayServer->onLoadHeaderHandler(array($this, 'onHeaderLoad'));
        $this->oGatewayServer->onEncrypt(array($oCrypt, 'encrypt'));
        $this->oGatewayServer->onDecrypt(array($oCrypt, 'decrypt'));

        $this->oGatewayServer->handler();

    }// func

}// class
