<?php

/**
 * Класс для основного админского слоя. также сторит интерфейс выбора файлов
 */

class CmsProcessor extends CmsProcessorPrototype {

    /**
     * Отдает имя ключа для сессионного хранилища
     * @return string
     */
    protected function getSessionKeyName() {
        return 'key';
    }

    /**
     * Возвращает имя модуля основного слоя
     * @return string
     */
    public function getLayoutModuleName() {

        // вычисление типа вывода
        /** @noinspection PhpUndefinedMethodInspection */
        $aGlobalParams = skProcessor::getJSONHeaders();
        $sLayoutMode = isset($aGlobalParams['layoutMode']) ? $aGlobalParams['layoutMode'] : '';

        // установка корневого модуля для вывода
        switch ( $sLayoutMode ) {
            case 'fileBrowser':
                return 'FileBrowserCmsModule';
            case 'designFileBrowser':
                return 'FileBrowser4DesignCmsModule';
            default:
                return 'LayoutCmsModule';
        }

    }

    /**
     * Возвращает имя модуля авторизации
     * @return string
     */
    public function getAuthModuleName() {
        return 'AuthCmsModule';
    }

    /**
     * Возвращает имя первично инициализируемого модуля
     * @return string
     */
    public function getFrameModuleName() {
        return 'FrameCmsModule';
    }

    /**
     * Возвращает имя первично инициализируемого модуля при отсутствии авторизации
     * @return string
     */
    public function getFrameAuthModuleName() {
        return '';
    }

    /**
     * Отдает базовый url для сервиса
     * @return string
     */
    public function getBaseUrl() {
        return '/admin/';
    }

}
