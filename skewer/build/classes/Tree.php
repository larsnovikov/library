<?php

/**
 * Класс для работы с деревом разделов
 * @class Tree
 * @extends skModule
 * @project Skewer
 * @package kernel
 *
 * @author Andy Mitrich
 * @version $Revision: 1172 $
 * @date 07.03.12 15:24 $
 *
 */

class Tree extends TreePrototype {

    const visibleHiden = 0;
    const visibleShow = 1;
    const visibleHideFromPath = 2;

    /**
     * Отдает имя таблицы
     * @static
     * @return string
     */
    public static function getTableName() {
        return 'tree_section';
    }

    public $aFilterArray = array();

    /** тип - раздел */
    const typeSection = 0;

    /** тип - папка */
    const typeDirectory = 1;

    /** id для подстановки в набор шаблонов */
    const tplDirId = -1;

    // набор допустимых режимов работы
    const modeNone = 0;
    const modeArray = 1;
    const modePolicy = 2;
    const modeCurrentUser = 3;
    const modeCurrentAdmin = 4;

    protected $iMode = self::modeArray;
    protected $iPolicyId = NULL;

    public function setPolicyFilter($iPolicyId){

        $this->iMode = self::modePolicy;
        $this->iPolicyId = $iPolicyId;
        return true;
    }

    public function setCurrentUserFilter(){

        $this->iMode = self::modeCurrentUser;
        return true;
    }

    public function setCurrentAdminFilter(){

        if ( CurrentAdmin::isSystemMode() )
            $this->iMode = false;
        else $this->iMode = self::modeCurrentAdmin;
        return true;
    }

    public function setNodeFilter($aFilter){

        $this->aFilterArray = $aFilter;
        return true;
    }

    public function unsetFilter(){

        $this->iMode = self::modeNone;
        $this->aFilterArray = array();
        return true;
    }

    /**
     * @return array|bool
     */
    public function getFilterArray(){

        switch( $this->iMode ){

            case self::modeNone:

                return false;
            break;

            case self::modeArray:

                return array();
            break;

            case self::modePolicy:

                if ( $this->iPolicyId ){
                    //получить массив разделов по слою и политике
                    $aSections = Policy::getGroupPolicyData($this->iPolicyId);
                    return $aSections['read_access'];
                }
                else return array();
            break;

            case self::modeCurrentUser:

                return CurrentUser::getReadableSections();
            break;

            case self::modeCurrentAdmin:

                return CurrentAdmin::getReadableSections();
            break;

            default:
                return false;
            break;
        }
    }

    /**
     * Служебные переменные для построителя разделов в линию
     */
    protected $aAllSections = array();
    protected $aSectionsOut = array();
    protected $aSectionsByParent = array();

    /**
     * Выбирает во внутренние переменные все разделы
     * @return bool
     */
    private function getAllSectionsInner(){

        $aSections = $this->getFilterArray();
        $sSectionFilter = '';
        if ( is_array($aSections) ){

            if ( sizeof($aSections) )
                $sSectionFilter = " WHERE `id` IN (".implode(',',$aSections).")";
            else false;
        }

        // собрать запрос для выборки разделов
        $sQuery = "SELECT `id`, `title`, `parent` FROM `tree_section` $sSectionFilter ORDER BY `position`";

        // выполнить запрос
        global $odb;
        $oResult = $odb->query($sQuery);

        // проверить, чтобы ошибок не было
        if ( $odb->errno ) return false;

        // выбрать все полученные результаты
        $aAllSections = array();
        while ($aRow = $oResult->fetch_array(MYSQLI_ASSOC))
            $aAllSections[ (int)$aRow['id'] ] = $aRow;

        // сформировать массив зависимостей по родителям
        $aByParent = array();
        foreach ( $aAllSections as $iId => $aRow )
            $aByParent[ $aRow['parent'] ][] = $iId;

        $this->aAllSections = $aAllSections;

        // вынести в глобальный массив
        $this->aSectionsByParent = $aByParent;

        return true;

    }


    /**
     * Выбирает Все разделы с заданного в виде одноуровнего раздела
     * @param int $iSectionId
     * @return array
     */
    public function getSectionsInLine( $iSectionId ) {

        // если первичная выборка не прошла
        if ( !$this->getAllSectionsInner() )
            return array();

        // очистка контейнера для результирующего массива
        $this->aSectionsOut = array();

        // добавить первый, если задан
        if ( $iSectionId and isset($this->aAllSections[$iSectionId]) ) {
            $this->aSectionsOut[ $iSectionId ] = array(
                'id' => $iSectionId,
                'title' => $this->aAllSections[$iSectionId]['title'],
                'parent' => $this->aAllSections[$iSectionId]['parent'],
            );
        }

        // запустить обходчик и выстроить разделы в линию
        $this->collectSectionsToLine( $iSectionId );

        $aOut = array_values($this->aSectionsOut);

        // очистить переменные
        $this->aAllSections = array();
        $this->aSectionsOut = array();
        $this->aSectionsByParent = array();

        return $aOut;

    }

    /**
     * Выбирает Все разделы с заданного в виде дерева
     * @param int $iSectionId
     * @return array
     */
    public function getAllSections( $iSectionId ) {

        // если первичная выборка не прошла
        if ( !$this->getAllSectionsInner() )
            return array();

        // очистка контейнера для результирующего массива
        $this->aSectionsOut = array();

        // запустить обходчик и выстроить разделы в линию
        $this->collectSections( $iSectionId, $this->aSectionsOut );

        $aOut = array_values($this->aSectionsOut);

        // очистить переменные
        $this->aAllSections = array();
        $this->aSectionsOut = array();
        $this->aSectionsByParent = array();

        return $aOut;

    }

    /**
     * Собирает разделы в дерево
     * @param $iSectionId
     * @param $aItems
     * @param int $iCnt счетчик рекурсивного входа
     * @return bool
     */
    protected function collectSections( $iSectionId, &$aItems, $iCnt=0 ) {

        // ограничение вложенности
        if ( $iCnt > 5 )
            return false;

        // выйти, если нет такого раздела
        if ( !isset($this->aSectionsByParent[$iSectionId]) )
            return false;

        // перебрать подчиненные разделы
        foreach ( $this->aSectionsByParent[$iSectionId] as $iSubItemId ) {

            // нет элемента - прервать
            if ( !isset($this->aAllSections[$iSubItemId]) )
                continue;

            // текущий раздел
            $aSubItem = $this->aAllSections[$iSubItemId];
            $aSubItem['children'] = array();

            // вызвать рекурсивно для наследников
            $this->collectSections( $iSubItemId, $aSubItem['children'], $iCnt+1 );

//            // сброс индексов
//            $aSubItem['children'] = array_values($aSubItem['children']);

            // добавление элемента
            $aItems[] = $aSubItem;

        }

        return true;
    }

    /**
     * @param $iSectionId
     * @param bool|string $sPrefix
     * @return bool
     */
    protected function collectSectionsToLine( $iSectionId, $sPrefix=false ) {

        // ограничение вложенности
        if ( strlen($sPrefix) > 5 )
            return false;

        // выйти, если нет такого раздела
        if ( !isset($this->aSectionsByParent[$iSectionId]) )
            return false;

        // не задан префикс - выбрать в зависимости от корневого раздела
        if ( $sPrefix===false )
            $sPrefix = $iSectionId ? '-' : '';

        // перебрать подчиненные разделы
        foreach ( $this->aSectionsByParent[$iSectionId] as $iSubItemId ) {

            // нет элемента - прервать
            if ( !isset($this->aAllSections[$iSubItemId]) )
                continue;

            // текущий раздел
            $aSubItem = $this->aAllSections[$iSubItemId];

            // защита от зацикливания
            if ( isset( $this->aSectionsOut[$iSubItemId] ) )
                return false;

            // добавить текущий
            $this->aSectionsOut[$iSubItemId] = array(
                'id' => $aSubItem['id'],
                'title' => $sPrefix.$aSubItem['title'],
                'parent' => $aSubItem['parent'],
            );

            // вызвать рекурсивно для наследников
            $this->collectSectionsToLine( $iSubItemId, $sPrefix.'-' );

        }

        return true;

    }

}