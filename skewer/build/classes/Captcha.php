<?php

/**
 * @class Captcha
 * @extends skModule
 * @project Skewer
 * @package kernel
 *
 * @author andymitrich, $Author: acat $
 * @version $Revision: 1672 $
 * @date 18.01.12 14:11 $
 *
 */

class Captcha {

    # settings
    static private $instance = null;
    static private $sword    = "";

    var $width       = 110;
    var $height      = 60;
    var $count       = 4;
    var $sumbols     = '1234567890';
    var $bgcolor     = '#ffffff';
    var $textColor   = '#359500';
    static $key      = 'captcha_secret_code';
    static $fontFile = '';
    var $fontSize    = 24;
    var $useBorder   = true;
    var $borderColor = '#000000';
    var $distort     = true;
    # methods

    private function __construct(){}

    private function __clone(){}

    public static function getInstance() {

        if (!extension_loaded('gd')) return false;
        if (self::$instance == null) self::$instance = new captcha();

        self::$fontFile = BUILDPATH.'common/fonts/font3.ttf';
        return self::$instance;
    }

    // func

    private function RGB2HEX($color = ""){

    $out = array('r'=>0xFF,'g'=>0xFF,'b'=>0xFF);
    if(empty($color)) return $out;

    $out['r'] = hexdec('0x'.substr($color, 1, 2));
    $out['g'] = hexdec('0x'.substr($color, 3, 2));
    $out['b'] = hexdec('0x'.substr($color, 5, 2));

    return $out;
    }// func

    private function myImageBlur($im){
        $width = imagesx($im);
        $height = imagesy($im);
        $distance = 1;

        $temp_im = ImageCreateTrueColor($width,$height);
        ImageCopy($temp_im,$im,0,0,0,0,$width,$height);
        $pct = 27; // blur level
        ImageCopyMerge($temp_im, $im, 0, 0, 0, $distance, $width-$distance, $height-$distance, $pct);
        ImageCopyMerge($im, $temp_im, 0, 0, $distance, 0, $width-$distance, $height, $pct);
        ImageCopyMerge($temp_im, $im, 0, $distance, 0, 0, $width, $height, $pct);
        ImageCopyMerge($im, $temp_im, $distance, 0, 0, 0, $width, $height, $pct);

        ImageDestroy($temp_im);
    }//func

    function setKey($newKey){
    if(!empty($newKey))
      self::$key = $newKey;
    }// func

    function compare_captcha($guess){
    $out = false;
    if(empty($guess))                 return $out;
    if(!isset($_SESSION[self::$key])) return $out;
    if(empty($_SESSION[self::$key]))  return $out;

    if($_SESSION[self::$key] == $guess) $out = true;
    unset($_SESSION[self::$key]);

    return $out;
    }// func

    public static function check_captcha($code, $bRefresh = true){
        $out = false;
        if(empty($code))                 return $out;
        if(!isset($_SESSION[self::$key])) return $out;
        if(empty($_SESSION[self::$key]))  return $out;

        // проверяем колличетво неудачных попыток на одну капчу
        if ($_SESSION['countCheckCaptcha']<0)
            return $out; 
        
        $_SESSION['countCheckCaptcha']--;
    
        if($_SESSION[self::$key] == $code) $out = true;
        if($bRefresh)
            $_SESSION[self::$key] = rand(1000, 9999);
        return $out;
    }// func

    public function get_captcha(){

        // колличетво попыток проверки капчи
        $_SESSION['countCheckCaptcha'] = 3;
        
        $img  = imagecreate($this->width, $this->height);
        $img2 = imagecreate($this->width, $this->height);
        $bg   = $this->RGB2HEX($this->bgcolor);
        $brd_color = $this->RGB2HEX($this->borderColor);
        
    
        imagecolorallocate($img, $bg['r'], $bg['g'], $bg['b']);
        imagecolorallocate($img2, $bg['r'], $bg['g'], $bg['b']);
        $brd_color = imagecolorallocate($img2, $brd_color['r'], $brd_color['g'], $brd_color['b']);
    
        if(!isSet($_SESSION['randNum'])) $_SESSION['randNum'] = 0;
            $_SESSION['randNum']++;
    
        if($this->count)
          for($i=0;$i<$this->count;$i++){
             $txtcolor = $this->RGB2HEX($this->textColor);
             $txtcolor = imagecolorallocate($img, $txtcolor['r'], $txtcolor['g'], $txtcolor['b']);
             $rs = $this->sumbols[mt_rand(0, strlen($this->sumbols)-1)];
    
             imagettftext( $img, $this->fontSize, mt_rand(-35, 35), 8+($i*$this->fontSize)+mt_rand(3,6),  ($this->height / 2)+($this->fontSize/2)-2,  $txtcolor, self::$fontFile, $rs);
             self::$sword .= $rs;
          }// create text
    
        //     imageline($img, 0, $this->height/2, $this->width, $this->height/2, $brd_color);
    
        if($this->distort){
           $q = mt_rand(-7, 7);
          for($x=0; $x<=$this->width-1; $x++){
            for($y=0; $y<=$this->height-1; $y++){
    
              $old_color = imagecolorat($img, $x, $y);
              $ny = sin(deg2rad( 300/$this->width * $x)-$q);
              imagecopy($img2, $img, $x, 0+($ny*($this->height/4)), $x, 0, 1, $this->height);
            }//y
          }//x
        $this->myImageBlur($img2);
        $img = $img2;
        }// wave
    
    
        if($this->useBorder){
          imageline($img, 0, 0, 0, $this->height, $brd_color);
          imageline($img, $this->width-1, 0, $this->width-1, $this->height, $brd_color);
          imageline($img, 0, 0, $this->width, 0, $brd_color);
          imageline($img, 0, $this->height-1, $this->width, $this->height-1, $brd_color);
        }// use border
    
        $_SESSION[self::$key] = self::$sword;
        imagegif($img);
    }// func

}

//class
?>