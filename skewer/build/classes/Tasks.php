<?php
/**
 *
 *
 * @class: Tasks
 *
 * @Author: sapozhkov, Acat, kolesnikov
 * @version: $Revision: 866 $
 * @date: $Date: 2012-09-26 18:46:43 +0400 (Ср., 26 сент. 2012) $
 *
 */

class Tasks {

    public static $taskStatusNew           = 1;
    public static $taskStatusRunning       = 2;
    public static $taskStatusComplete      = 3;
    public static $taskStatusNotComplete   = 4;
    public static $taskStatusTimeout       = 5;
    public static $taskStatusRepeat        = 6;

    public static $tasksStatus = array('Ставится','Новая','Запущена','Выполнена','Ошибка','Зависла','Ожидает повтора');

    public static $taskPriorityLow         = 1;
    public static $taskPriorityNormal      = 2;
    public static $taskPriorityHigh        = 3;
    public static $taskPriorityCritical    = 4;

    public static $tasksPriority = array('','Низкий','Обычный','Высокий','Критический');

    public static $taskWeightLow           = 3;
    public static $taskWeightNormal        = 4;
    public static $taskWeightHigh          = 7;
    public static $taskWeightCritic        = 9;

    public static $tasksWeight = array(2=>'Низкий',3=>'Низкий',4=>'Обычный',7=>'Высокий',9=>'Критический');

    public static $maxWeight               = 10;  // максимальный размер локального сайта


    /**
     * Добавить задание в локальную очередь
     * @param string $title - Название задания в системе администрирования
     * @param int $priority - приоритет задания
     * @param int $resource_use - ресурсоемкость задания
     * @param string $sClassName - название вызываемого класса
     * @param string $sMethodName - имя вызываемого метода
     * @param bool|array $mParameters -  параметры
     * @throws GatewayException
     * @throws Exception
     * @return int | bool
     */
    public static function addTask( $title, $priority , $resource_use, $sClassName, $sMethodName, $mParameters = array() ) {

        $command = array('class'=>$sClassName,
                         'method'=>$sMethodName,
                         'parameters'=>$mParameters);

        $command = json_encode($command);

        if($sClassName == 'SchedulerJSONCommand') $command = $sMethodName;

        // отмена добавления задачи, если задача с такой командой добавленна
        if( $iTaskId = TaskMapper::findTaskWithCommand($command) ) return $iTaskId;

        $aInputData = array('global_id' => 0,
                            'title' => $title,
                            'command' => $command,
                            'priority' => $priority,
                            'resource_use' => $resource_use,
                            'mutex' => 0,
                            'status' => self::$taskStatusNew,
                            'exec_counter' => 0);

        $iTaskId = TaskMapper::saveItem($aInputData,'freeTask');

        if(self::inCluster()){ //  Обработка записи в глобальной очереди

            try{

                $oClient = skGateway::connect(CLUSTERGATEWAY, APPKEY);

                if(!($oClient instanceof skGatewayClient)) throw new GatewayException('Error init;');

                $aParam = array($_SERVER['HTTP_HOST'], $title, $command, $priority , $resource_use);

                $oClient->addMethod('HostTools', 'addTask', $aParam, function($iGlobalId, $mError) use ($iTaskId) {

                    if($mError)  throw new Exception($mError);

                    // update global ID
                    $mRes = TaskMapper::setGlobalId($iTaskId,$iGlobalId);

                    if(!$mRes)  throw new Exception('Ошибка при сохранении глобального ID задачи');

                });

                if(!$oClient->doRequest()) throw new Exception($oClient->getError());


            } catch(GatewayException $e) {

                echo $e->getMessage();
                return false;

            } catch(Exception $e) {

                return false;

            }

        }

        return $iTaskId;
    }

    /**
     * Запуск одной или несколькиз задач из очереди на выполнение
     * @return bool
     */
    function execute( ) {

        if(rand(1,10) == 5) self::collectGarbage();

        do {

            $bExit = true;

            $oTask = self::getFirst();

            if($oTask == null) break;

            $iTaskStatus = $oTask->getStatus();

            if( (($iTaskStatus == self::$taskStatusNew) || ($iTaskStatus == self::$taskStatusRepeat)) && $oTask->canExecute() ){

                $oTask->execute();

                $bExit = false;

            }

        } while (!$bExit);


        return true;

    }

    /**
     * Получить статус задачи по $iTaskId и выволнить ее, если она находится в ожидании
     * @param int $iTaskId - локальный id задачи
     * @return int
     */
    public function getTaskStatus( $iTaskId ){

        $oTask = $this->getById($iTaskId);

        $iTaskStatus = $oTask->getStatus();

        if( ($iTaskStatus == self::$taskStatusNew || $iTaskStatus == self::$taskStatusRepeat) && $oTask->canExecute() ){

            $oTask->execute();

            $iTaskStatus = $oTask->getStatus();

        }

        return $iTaskStatus;
    }

    /**
     * Получить из очереди объект задачи по id
     * @param $iTaskId - локальный id задачи
     * @return object || null
     */
    public function getById( $iTaskId ) {

        $oTask = new TaskObject($iTaskId);

        return $oTask;

    }

    /**
     * Получить из очереди объект задачи ближайшей к исполнению
     * @return TaskObject|null
     */
    public static function getFirst() {

        $oTask = null;

        if($iItemId = TaskMapper::getPriorityItem())
            $oTask = new TaskObject($iItemId);

        return $oTask;

    }

    /**
     * При работе в кластере - отправляет список заданий в очереди с актуальными статусами,
     * регистрирует новые задания в глобальной очереди, стохастически запускает сборщик мусора
     * @throws Exception
     * @return boolean
     */
    public static function syncronize() {

        $oClient = skGateway::connect(CLUSTERGATEWAY, APPKEY);

        if(!($oClient instanceof skGatewayClient)) throw new Exception('Error init;');

        $aItems = TaskMapper::getItems();

        $aParam = array($_SERVER['HTTP_HOST'], $aItems);

        /** @noinspection PhpUnusedParameterInspection */
        $oClient->addMethod('HostTools', 'syncronizeTask', $aParam, function($mResult, $mError) {

            if($mError)  throw new Exception($mError);

            //if(!$mResult)  throw new Exception('Ошибка при синхронизации глобальных статусов задач');

        });

        if(!$oClient->doRequest()) return false;

        return true;

    }

    /**
     * Удаляет из очереди выполненные задания, перезапускает или удаляет подвисшие задания
     * по таймауту
     * @return boolean
     */
    public static function collectGarbage() {

        // удаление старых повисших задач
        TaskMapper::delOldOutTimeTasks();

        // поиск новых повисших задач
        TaskMapper::findOutTimeTasks();

        // удаление старых задач
        TaskMapper::delOldTasks();

        // синхронизация
        self::syncronize();

        return true;

    }

    /**
     * Определяет, работает ли сервер в кластере
     * @return bool
     */
    public static function inCluster(){

        return skProcessor::inCluster();

    }

    /**
     * Отадет название статуса по id
     * @param int $iStatus
     * @return string
     */
    public static function getStatusTitle( $iStatus ) {
        return isset(self::$tasksStatus[$iStatus]) ? self::$tasksStatus[$iStatus] : '-не определен-';
    }

}
