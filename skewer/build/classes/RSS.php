<?php

/**
 * @class RSS
 * @extends skModule
 * @project Skewer
 * @package kernel
 *
 * @author Andy Mitrich, $Author: sapozhkov $
 * @version $Revision: 6 $
 * @date 24.01.12 10:37 $
 *
 */

class RSS {

    private static $aModuleList = array(
        'News',
        'Events'
    );

    /**
     * Преестроение RSS файла
     * @static
     * @return bool|string
     * @throws Exception
     */
    public static function buildRssFile(){

        $aRSSItems = self::getRssContent();

        if ( !sizeof($aRSSItems) ) return false;

        $aData['aItems'] = $aRSSItems;
        $aData['sSitename'] = WEBROOTPATH;
        $aData['sDate'] = date('r');
        $sFile = skParser::parseTwig('rss_template.xml',$aData);
        
        // проверка наличия директории
        $sDir = ROOTPATH.'files/rss';
        if ( !file_exists($sDir) ) {
            if ( !@mkdir($sDir) )
                throw new Exception('Невозможно создать директорию для RSS файла');;
            chmod($sDir,0777);
        }

        $rFile = @fopen($sDir.'/feed.rss', 'w+');

        if ( $rFile===false ) throw new Exception('Невозможно открыть RSS файл для записи');

        fwrite($rFile, $sFile);

        return $sFile;

    }

    public static function getRssContent(){

        $aItems = array();

        foreach( self::$aModuleList as $sModule ){

            $sModuleName = $sModule.'Api';

            if ( !class_exists($sModuleName) ) continue;

            /** @noinspection PhpUndefinedMethodInspection */
            $aTItems = $sModuleName::getRSSData();

            $aItems = array_merge($aItems, $aTItems['items']);
        }

        usort($aItems, 'self::cmpArray');

        array_splice($aItems, 20);

        return $aItems;
    }

    private static function cmpArray($a, $b) {

        if ($a['unix_time'] == $b['unix_time']) return 0;
        return ($a['unix_time'] < $b['unix_time']) ? 1 : -1;
    }

} //class