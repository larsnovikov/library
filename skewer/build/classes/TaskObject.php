<?php
/**
 *
 *
 * @class: TaskObject
 *
 * @Author: sapozhkov, Acat, ilya
 * @version: $Revision: 866 $
 * @date: $Date: 2012-09-26 18:46:43 +0400 (Ср., 26 сент. 2012) $
 *
 */

class TaskObject {

    public $aTask = array();


    function __construct($iTaskId) {

        if ( !$iTaskId )
            throw new Exception('Не задан id задачи');

        $this->aTask = TaskMapper::getItem($iTaskId);

        if ( !$this->aTask )
            throw new Exception('Задача не найдена');

    }


    /**
     * Определяет, работает ли сервер в кластере
     * @return bool
     */
    protected function inCluster(){

        return Tasks::inCluster();

    }


    /**
     * Возвращает локальный id задания
     * @return int
     */
    function getId() {

        return (isset($this->aTask['id'])) ? $this->aTask['id'] : null;

    }

    /**
     * Возвращает id задания в общесерверной очереди
     * @return int
     */
    function getGlobalId() {

        return (isset($this->aTask['global_id'])) ? $this->aTask['global_id'] : null;

    }

    /**
     * Устанавливает глобальный id задания
     * @param $iGlobalId;
     * @return boolean
     */
    function setGlobalId($iGlobalId) {
        $this->aTask['global_id'] = $iGlobalId;
        return true;
    }


    /**
     * Возвращает значение индекса ресурсоемкости процесса
     * @return int
     */
    function getWeight() {

        return (isset($this->aTask['resource_use'])) ? $this->aTask['resource_use'] : 1000000;

    }


    /**
     * Сохраняет объект задания в локальную очередь
     * @return boolean
     */
    function save() { /***/

        if(!isset($this->aTask['status'])) return false;

        $xRes = TaskMapper::saveItem($this->aTask);

        return $xRes?true:false;

    }


    /**
     * Возвращает статус задания в локальной очереди
     * @return int
     */
    function getStatus() {

        return $this->aTask['status'];

    }

    /**
     * Устанавливает статус задания в локальной очереди
     * @param $iStatus
     * @throws Exception
     * @return boolean
     */
    function setStatus($iStatus) {
        // смена статуса происходит только в том случае если задание захвачено текущим процессом
        // либо задание свободно (не захвачено другим процессом)

        $this->aTask['status'] = $iStatus;

        if(!TaskMapper::setStatus($this->getId(), $iStatus)){

            TaskMapper::setStatus($this->getId(), Tasks::$taskStatusNotComplete);

            return false;

        }

        if($this->inCluster()){

            try{

                $oClient = skGateway::connect(CLUSTERGATEWAY, APPKEY);

                if(!($oClient instanceof skGatewayClient)) throw new Exception('Error init;');

                $aParam = array($this->getGlobalId(), $iStatus);

                /** @noinspection PhpUnusedParameterInspection */
                $oClient->addMethod('HostTools', 'setTaskStatus', $aParam, function($mResult, $mError) {

                    if($mError)  throw new Exception($mError);

                });

                if(!$oClient->doRequest()) throw new Exception($oClient->getError());

            } catch(Exception $e) {

                skLogger::dump('error TaskObject.setStatus: '.$e->getMessage());

                TaskMapper::setStatus($this->getId(), Tasks::$taskStatusNotComplete);

                return false;

            }

        }

        return true;

    }

    /**
     * Запуск задачи на выполнение - если это разрешено менеджером очереди и захват
     * @throws Exception
     * @return bool
     */
    function execute() { /***/

        if(!$this->setStatus(Tasks::$taskStatusRunning)) return false;

        $sCommand = $this->aTask['command'];

        try{

            if(!$sCommand) throw new Exception('Команда не задана');

            $sCommand = json_decode($sCommand,true);

            if(!isset($sCommand['class']) || !isset($sCommand['method']))
                throw new Exception('Формат не верный');

            $oCurClass = new ReflectionClass($sCommand['class']);

            if( !($oCurClass instanceof ReflectionClass) )
                throw new Exception('Не создан класс');

            if( $oCurClass->getParentClass()->name != 'ServicePrototype')
                throw new Exception('Попытка запуска неразрешенного класса');

            $oCurObj = new $sCommand['class']();

            if( !method_exists($oCurObj,$sCommand['method']) )
                throw new Exception('Попытка запуска несуществующего метода');

            $iStatus = call_user_func_array(array($oCurObj,$sCommand['method']),$sCommand['parameters']);

            if(!$iStatus) $iStatus = Tasks::$taskStatusNotComplete;

        } catch(Exception $e) {

            echo $e->getMessage();
            skLogger::dump('error TaskObject.execute: '.$e->getMessage());

            $iStatus = Tasks::$taskStatusNotComplete;

        }

        if(!$this->setStatus($iStatus)) return false;

        return true;

    }



    /**
     * Произвести захват задачи для последующего исполнения
     * @return bool
     */
    function canExecute() {

        // производит мьютексный захват задачи в локальной очереди и сохраняет значение мьютекса в свойство
        // если захват не удался - возвращает false
        // проверка на наличие свободного ресурса

        //обработка гловальной очереди
        if($this->inCluster()) return $this->canExecuteGlobal();

        // обработка локальной очереди
        $iSumWeight = TaskMapper::getSumWeight();

        if ($iSumWeight + $this->getWeight() > Tasks::$maxWeight)
            return false;

        // проверка на мютекс
        $iRes = TaskMapper::holdItem($this->getId());

        if($iRes == 1)
            $this->aTask['mutex'] = 1;

        return ($iRes == 1);

    }

    /**
     * Произвести захват задачи в общесерверной очереди для последующего исполнения
     * @throws Exception
     * @return bool
     */
    function canExecuteGlobal() { /***/

        // производит мьютексный захват задачи в очереди на сервере и сохраняет значение мьютекса в свойство
        // если захват не удался - возвращает false

        try{

            $oClient = skGateway::connect(CLUSTERGATEWAY, APPKEY);

            if(!($oClient instanceof skGatewayClient)) throw new Exception('Error init;');

            $aParam = array($this->getGlobalId());

            $oSelf = $this;

            $oClient->addMethod('HostTools', 'canExecuteTask', $aParam, function($mResult, $mError) use ($oSelf) {

                if($mError)  throw new Exception($mError);

                if(!$mResult)  throw new Exception('Ошибка при захвате задачи в глобальной очереди.');

                $iRes = TaskMapper::holdItem($oSelf->getId());

                if($iRes != 1)  throw new Exception('Ошибка при захвате задачи в локальной очереди.');

                $oSelf->aTask['mutex'] = 1;

            });

            if(!$oClient->doRequest()) throw new Exception($oClient->getError());

            return true;

        } catch(Exception $e) {

            skLogger::dump('request error: "'.$e->getMessage().'" #canExecuteGlobal');

            TaskMapper::holdItem($this->getId());

            $this->setStatus(Tasks::$taskStatusNotComplete);

            return false;
        }

    }

    /**
     * Зарегистрировать задание в общесерверной очереди заданий
     * @return bool
     */
    function register() { /***/

        // регистрируем задание в общерверной очереди
        // прописываем глобальный id в свойства и в локальную очередь



        return true;

    }


} // class