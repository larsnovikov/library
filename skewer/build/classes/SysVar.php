<?php
/**
 * Класс для работы с системными переменными
 */
class SysVar {

    /**
     * @var string Имя текущей таблицы, с которой работает маппер
     */
    protected static $sCurrentTable = 'sys_vars';

    /**
     * возвращает значение системной переменной
     * @static
     * @param string $sName
     * @return string|bool
     */
    public static function get($sName){

        $sQuery = "SELECT `sv_value` FROM `[table:q]` WHERE sv_name=[name:s]";

        $aData = array(
            'table' => self::$sCurrentTable,
            'name' => $sName,
        );

        global $odb;
        return $odb->getValue($sQuery,'sv_value',$aData);

    }

    /**
     * сохранение значения системной переменной
     * @static
     * @param string $sName название переменной
     * @param mixed $mValue значение переменной
     * @return bool
     */
    public static function set( $sName, $mValue ) {

        $sQuery = "INSERT INTO `[table:q]` (sv_name, sv_value)
                    VALUES ([name:s], [value:s])
                    ON DUPLICATE KEY UPDATE sv_value=[value:s]";

        $aData = array(
            'table' => self::$sCurrentTable,
            'name' => $sName,
            'value' => $mValue
        );

        global $odb;
        $odb->query($sQuery,$aData);

        return ($odb->affected_rows>0);

    }

    /**
     * Возворащает состояние сервера (продакшн/тест)
     * @return bool
     * @throws GatewayException
     */
    public static function isProductionServer() {

        if(!INCLUSTER) return true;

        $oClient = skGateway::connect(CLUSTERGATEWAY, APPKEY);

        if(!($oClient instanceof skGatewayClient)) throw new GatewayException('Error init;');

        $bResultStatus = false;

        $oClient->addMethod('HostTools', 'isProductionServer', array(), function($mResult, $mError) use (&$bResultStatus) {

                $bResultStatus = $mResult;

        });

        if(!$oClient->doRequest()) return false;

        return $bResultStatus;
    }

}