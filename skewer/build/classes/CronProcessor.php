<?php
/**
 * Процессор для добавления и выполнения задач по расписанию
 * @class CronProcessor
 *
 * @author kolesnikov, $Author: sapozhkov $
 * @version $Revision: 1346 $
 * @date 02.05.12 $Date: 2012-12-03 11:31:54 +0400 (Пн., 03 дек. 2012) $
 * @project Skewer
 * @package Build
 */
class CronProcessor extends skProcessorPrototype implements skProcessorInterface{

    public function build(){

        // стохастическое удаление старых задач
        if(rand(1,10) < 3)
            Tasks::collectGarbage();

        // добавление задач из планировщика

        $iStartTime = time();
        $iLastStartTime = (int)SysVar::get('SheduleLastStartTime');//(int)ScheduleToolMapper::getLastStart();

        $aScheduleItems = ScheduleToolMapper::getAllItems();

        for($iCurTime = $iLastStartTime; $iCurTime < $iStartTime; $iCurTime+=60){

            $sCurTime = date('i:H:d:m:w',$iCurTime);
            $aCurTime = explode(':',$sCurTime);

            foreach($aScheduleItems as $aCurTask){

                 if( $aCurTask['status'] &&
                     (is_null($aCurTask['c_min']) || $aCurTask['c_min']==(int)$aCurTime[0]) &&
                     (is_null($aCurTask['c_hour']) || $aCurTask['c_hour']==(int)$aCurTime[1]) &&
                     (is_null($aCurTask['c_day']) || $aCurTask['c_day']==(int)$aCurTime[2]) &&
                     (is_null($aCurTask['c_month']) || $aCurTask['c_month']==(int)$aCurTime[3]) &&
                     (is_null($aCurTask['c_dow']) || $aCurTask['c_dow']==(int)$aCurTime[4])){

                     Tasks::addTask($aCurTask['title'],$aCurTask['priority'],$aCurTask['resource_use'],'SchedulerJSONCommand',$aCurTask['command']);
                 }

            }

        }

        // save last start time
        //ScheduleToolMapper::saveLastStart($iStartTime);
        SysVar::set('SheduleLastStartTime',$iStartTime);

        // добавление задач на выполнение из очереди задач
        $iStartTime = microtime();

        do {

            $bExit = true;

            /** @var TaskObject $oTask */
            $oTask = Tasks::getFirst();

            if($oTask == null) break;

            $iTaskStatus = $oTask->getStatus();

            if( (($iTaskStatus == Tasks::$taskStatusNew) || ($iTaskStatus == Tasks::$taskStatusRepeat)) && $oTask->canExecute() ){

                $oTask->execute();

                $bExit = false;

            }

            if(microtime() - $iStartTime > 5000) $bExit = true;
            // $bExit = true; // для пошаговой отладки

        } while (!$bExit);


        return true;
    }

}
