<?php
/**
 * Менеджер процессов. Запускает page на выполнение.
 *
 * @class PageProcessor
 * @project Skewer
 * @package Build
 * @Author ArmiT, $Author: acat $
 * @version $Revision: 1433 $
 * @date $Date: 2012-12-18 16:15:33 +0400 (Вт., 18 дек. 2012) $
 *
 */
class PageProcessor extends skProcessorPrototype implements skProcessorInterface {

     /**
     * Запускает на выполнение корневой процесс, выводит результат работы дерева процессов
     * @static
     * @return bool|string
     */
    public function build() {

        /* Добавялем класс Design для доступа в шаблонах */
        skProcessor::setParserHelper(new Design());

        skProcessor::setParserHelper(new Vars());

        skResponse::sendHeaders();

        $this->sRequestURL = $this->sURL = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];

        /* Определяем стартовый раздел */
        skConfig::set('section.main', Auth::getMainSection());

        /*работаем с параметрами GET POST*/
        $this->aGet = $_GET;
        $this->oRouter = new skRouter($this->sURL, $this->aGet);

        if(!$this->iPageId = $this->oRouter->getSection(skConfig::get('url.root'), skConfig::get('section.main')))
            $this->setPage(page404);

        /** @var $oPage skProcess */
        $oPage = null;

        do {

            // Проверяем доступ к разделу
            if ( !CurrentUser::canRead($this->iPageId) ) $this->setPage(pageAuth);

            $this->removeProcess('out');

            $oPage = $this->addProcess(new skContext( 'out', 'PageModule', ctModule, array('pageId'=>$this->iPageId)));
            $iStatus = $this->executeProcessList();

        } while($iStatus == psExit);

        $oPage->render();
        $this->sOut = skRouter::rewriteURLs($oPage->getOut());
//        $this->sOut = $oPage->getOut();
        return $this->sOut;
    }// funс

    /**
     * Управление потоком выполнения процессов
     * @param integer $iPage Константа страницы редиректа
     * @return bool
     */
    public function setPage($iPage = page404) {

        $this->sURL ='';

        switch($iPage){
            case page404:
                skResponse::send404Headers();
                $this->iPageId = skConfig::get('section.404');
                break;
            case pageAuth:
                skResponse::sendAuthHeaders();
                $this->iPageId = skConfig::get('section.auth');
                break;
        }// set page

        return true;
    }// func

}// class
