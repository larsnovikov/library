<?php

/**
 * @class CSSParser
 * @extends skModule
 * @project Skewer
 * @package Build
 *
 * @author Andy Mitrich, $Author: acat $
 * @version $Revision: 1210 $
 * @date 05.12.11 15:28 $
 *
 */

class CSSParser {

    /** Переменная, которая хранит массив групп, упоминающихся в css-файлах
     * @var $aGroups array
     */
    private $aGroups;

    /** Переменная, которая хранит массив параметров, упоминающихся в css-файлах
     * @var $aParams array
     */
    private $aParams;

    /** @var int вес по умолчанию для сортировки файлов */
    protected $iDefaultWeight = 1;

    /** Функция импорта CSS-параметров
     * @param $aInputData
     * @param int $bWithUpdate
     * @return bool
     */
    public function analyzeCSSFiles($aInputData, $bWithUpdate = 1){

        $oDesignManager = new DesignManager();

        if ( !$aInputData ) return false;

        // Чистим переменные класса
        $this->aGroups = array();
        $this->aParams = array();

        // Внешний цикл по слоям
        foreach( $aInputData as $aLayer ){

            // Цикл по условиям
            foreach( $aLayer as $aCondition ){

                // Цикл по файлам
                foreach( $aCondition as $sFile ){

                    // Вызов функции импорта для каждого CSS-файла
                    $this->analyzeFile($sFile);
                }
            }
        }
        /*
         * Проверка наличия флага "Запись в БД"
         * Если флаг стоит - вызов метода для обновления информации в БД
         */
        if ( $bWithUpdate )
            $oDesignManager->updateDesignSettings(array('groups'=>$this->aGroups, 'params'=>$this->aParams));

        return true;
    }//function analyzeCSSFiles()

    /** Функция импорта CSS-параметров конкретного файла
     * @throws Exception
     * @param $sPathFile
     * @return bool
     */
    public function analyzeFile($sPathFile){

        try{

            // Если запрашиваемого файла не существует - выдать исключение
            if ( !file_exists($sPathFile) ) throw new Exception('Файла не существует!');

            // Открываем файл
            $rFile = fopen($sPathFile ,'r');
            $sFile = '';

            // Чтение файла
            while( !feof($rFile) )
                $sFile.= fread($rFile, 8000);

            // Ищем совпадение по паттерну
            preg_match_all( '/\/\*{1}\s*(?<command>layer|group|param|const)\:(?<content>.*)\*\/{1}/xUi', $sFile, $aMatches);

            if ( isset($aMatches['command']) && sizeof($aMatches['command']) ){

                $sCurrentGroup = '';
                $sCurrentLayer = '';

                // Проходимся по массиву совпадений, по ветке комманд
                foreach( $aMatches['command'] as $iKey=>$sCommand ){

                    // Делим значение для комманды по |
                    $aLineParts = explode('|', $aMatches['content'][$iKey]);
                    array_walk( $aLineParts, function(&$value){ $value = trim($value); } );

                    // Свитч по комманде
                    switch($sCommand){

                        case 'layer':

                            // Установить текущий слой
                            $sCurrentLayer = $aLineParts[0];
                        break;

                        case 'group':

                            /*
                             * Добавить группу в массив групп
                             * Установить текущую группу
                             */
                            if ( isset($aLineParts[1]) )
                                $this->aGroups[$sCurrentLayer][$aLineParts[0]] = $aLineParts[1];
                            $sCurrentGroup = $aLineParts[0];
                        break;

                        case 'param':

                            /*
                             * Добавить параметр в массив параметров
                             */
                            $this->aParams[$sCurrentLayer][$sCurrentGroup.'.'.$aLineParts[0]] = array(
                                'title' => $aLineParts[1],
                                'type' => $aLineParts[2],
                                'default' => $aLineParts[3]
                            );
                        break;

                        case 'const':

                            //TODO Обработка констант
                        break;
                    }
                }

                return true;
            }
            else return false;
        }
        catch( Exception $e ){

            var_dump($e);
        }
        return false;
    }//function analyzeCSSFiles()

    /** Метод парсинга параметров в CSS-файлы
     * @param array $aCSSFiles
     * @return bool
     */
    public function parseCSSFiles($aCSSFiles = array()){

        if ( !$aCSSFiles ) return false;

        $oDesignManager = new DesignManager();
        $this->aParams = $oDesignManager->getParams();
        //echo "<XMP>"; print_r($this->aParams); echo "</XMP>";

        /*
         * Проходимся по массиву CSS-файлов
         * Внешний цикл по слою
         */
//        $sTimeStamp = md5(microtime());

        foreach( $aCSSFiles as $sLayerKey=>$aLayer ){

            // Цикл по условию
            foreach( $aLayer as $sConditionKey=>$aCondition ){

                // Сформировать имя конечного файла для кэширования
                //$sMergedFile = skConfig::get('cache.css').skFiles::createValidName($sLayerKey.'_'.$sConditionKey.'_'.$sTimeStamp).'.css';
                $sMergedFile = skConfig::get('cache.css').skFiles::makeURLValidName($sLayerKey.'_'.$sConditionKey.'.css');
                $rFile = fopen($sMergedFile, "w+");
                flock($rFile, LOCK_EX);

                // Цикл по файлам
                foreach ( $aCondition as $sFileName ){
                    // Парсинг конкретного файла
                    fwrite($rFile, $this->parseFile($sFileName));
                }

                flock($rFile, LOCK_UN);
                fclose($rFile);

                $aCSSFiles[$sLayerKey][$sConditionKey] = str_replace(ROOTPATH, '/', $sMergedFile);
            }
        }

        return $aCSSFiles;
    }//function parseCSSFiles()


    //************************* CSS MATH PARSING ******************************************\

    /**
     * массив сигнатур матиматических операций в порядке убывания приоритетов выполнения
     * @return array
     */
    public function getMathOperation(){
        return array(' / ',' * ',' - ',' + ');
    }

    /**
     * проверяет наличие в строке сигнатуры математической операции
     * @param $sExp
     * @return bool
     */
    public function isMathString($sExp){

        $aOperations = $this->getMathOperation();
        foreach($aOperations as $sOperation)
            if( strpos($sExp,$sOperation) !== false )
                return true;

        return false;
    }

    /**
     * Возвращает найденую сигнатуру с максимальным приоритетом
     * @param $sExp
     * @return string|bool
     */
    public function findMathOperation($sExp){

        $aOperations = $this->getMathOperation();

        foreach($aOperations as $iKey=>$sOperation)
            if(strpos($sExp,$aOperations[$iKey]))
                return $sOperation;

        return false;
    }

    /**
     * Выполняет математическую операцию $sOperation с учетом размерности с операндами $firstOperand и $lastOperand
     * @param $sOperation
     * @param $firstOperand
     * @param $lastOperand
     * @return string
     * @throws Exception
     */
    public function doMathOperation($sOperation, $firstOperand, $lastOperand){

        // разбор операндов на число-размерность
        $firstOperandDimension = $this->findDimension($firstOperand);
        $firstOperand = substr($firstOperand,0,strlen($firstOperand)-strlen($firstOperandDimension));
        $lastOperandDimension = $this->findDimension($lastOperand);
        $lastOperand = substr($lastOperand,0,strlen($lastOperand)-strlen($lastOperandDimension));

        // счераем размерность результата
        if( $firstOperandDimension == $lastOperandDimension ) {
            $sResultDimension = $firstOperandDimension;
        } elseif( !$firstOperandDimension ) {
            $sResultDimension = $lastOperandDimension;
        } elseif( !$lastOperandDimension ) {
            $sResultDimension = $firstOperandDimension;
        } else {
            throw new Exception('CSS Parser ERROR: incorrect input vars!');
        }

//        skLogger::dump('!!calc!! ['.$firstOperand.']['.$firstOperandDimension.']'.
//                        $sOperation.
//                        '['.$lastOperand.']['.$lastOperandDimension.']');

        if(!is_numeric($firstOperand) && !is_numeric($lastOperand))
            throw new Exception('CSS Parser ERROR: incorrect input vars!');

        // считаем числовой результат
        switch( $sOperation ) {
            case ' + ':
                $sExpression = $firstOperand + $lastOperand;
                break;
            case ' * ':
                $sExpression = $firstOperand * $lastOperand;
                break;
            case ' - ':
                $sExpression = $firstOperand - $lastOperand;
                break;
            case ' / ':
                $sExpression = $firstOperand / $lastOperand;
                break;
            default:
                throw new Exception('CSS Parser ERROR: incorrect input vars!');
        }

        if( $sResultDimension == 'px' )
            $sExpression = (int)$sExpression;

        return $sExpression.$sResultDimension;
    }

    /**
     * Поиск размерности в операнде $sOperand
     * @param $sOperand
     * @return string
     */
    public function findDimension($sOperand){

        if(strpos($sOperand,'em')) return 'em';
        if(strpos($sOperand,'px')) return 'px';
        if(strpos($sOperand,'%')) return '%';

        return '';
    }


    /**
     * Выполняет математическое преобразование в строке $sExpression
     * @param string $sExpression
     * @return string
     */
    public function calcMathExpressing($sExpression){

        if( $sOperation = $this->findMathOperation($sExpression) ){

            $iOperationPos = strpos($sExpression,$sOperation); // получаем позицию операции

            // ищем начало выражение
            $iBeginExpression = $iOperationPos;
            while( $iBeginExpression > 0) {
                if( $sExpression[$iBeginExpression-1] == ' ' ) break;
                $iBeginExpression--;
            }

            // ищем конец выражения
            $iEndExpression = $iOperationPos+3;
            while( $iEndExpression < (strlen($sExpression)-3) ) {
                if( $sExpression[$iEndExpression+1] == ' ' ) break;
                $iEndExpression++;
            }

            $sCurResult = $this->doMathOperation($sOperation,
                substr($sExpression,$iBeginExpression,$iOperationPos-$iBeginExpression),
                substr($sExpression,$iOperationPos+3,$iEndExpression-$iOperationPos-2));


            if( $sCurResult ){

                $sNewExpression = substr($sExpression,0,$iBeginExpression). $sCurResult.substr($sExpression,$iEndExpression+1);

                $sExpression = $sNewExpression;

            }

        }

        return $sExpression;
    }

    /**
     * Поиск и выполнение в строке $sExp математических преобразований
     * @param string $sExp
     * @return string
     */
    public function calcMathString($sExp){

        $sOldExp = $sExp;
        try {

            $k = 0;
            while( $this->isMathString($sExp) ){

                //skLogger::dump('F > '.$sExp);

                $sExp = $this->calcMathExpressing($sExp);

                //skLogger::dump('E > '.$sExp);

                $k++;

                if($k > 30) break;// ограничение на бесконечный цикл
            }

        } catch (Exception $e) {

            $sExp = $sOldExp;
        }


        return $sExp;
    }

    /**
     * Метод парсинга конкретного файла
     * @param $sFileName
     * @throws Exception
     * @return string
     */
    public function parseFile( $sFileName ){

        $sMergedFile = '';
        $sCurrentLayer = '';
        $sCurrentGroup = '';

        try{
            // Открываем файл
            if ( ($rFile = fopen($sFileName,'r'))===false  ) throw new Exception('Невозможно открыть файл '.$sFileName.' для чтения!');

            while( !feof($rFile) ){

                // Читаем файл по строкам
                $sFileLine = fgets($rFile);

                // Проверяем на совпадения
                preg_match_all( '/\/\*{1}\s*(?<command>layer|group|param|const)\:(?<content>.*)\*\/{1}/xUi', $sFileLine, $aMatches);

                if ( count($aMatches['command']) ){

                    //Интерпретация комманды

                    // Разбиваем по |, обрезаем пробелы
                    $aLineParts = explode('|', $aMatches['content'][0]);
                    array_walk( $aLineParts, function(&$value){ $value = trim($value); } );

                    switch( $aMatches['command'][0] ){

                        case 'layer':

                            // Установить текущий слой
                            $sCurrentLayer = $aLineParts[0];
                        break;

                        case 'group':

                            // Установить текущую группу
                            $sCurrentGroup = $aLineParts[0];
                        break;
                    }

                    // Добавить к выходной переменной
                    //$sMergedFile.= $sFileLine;
                } // if
                else {
                    //Парсинг параметров в строке

                    $iOffset = 0;

                    $sCurrentLine = '';

                    // Цикл по строке
                    while( ($iCommandStart = strpos($sFileLine, '[', $iOffset))!==false ){

                        // Если найдена и открывающая, и закрывающая квадратные скобки
                        if( $iCommandEnd = strpos($sFileLine, ']', $iCommandStart) ){

                            // Добавляем в выходную переменную все, что до открывающей скобки
                            $sCurrentLine.= substr($sFileLine, $iOffset, $iCommandStart-$iOffset);

                            // Вырезаем выражение для подстановки
                            $sParamExpression = substr($sFileLine, $iCommandStart+1,$iCommandEnd-$iCommandStart-1);

                            // Ищем значение выражения в массиве параметров и подставляем его в выходную переменную
                            if ( ($iPoint = strrpos($sParamExpression, '.'))!==false ) {

                                // Если в выражении есть точка, значит это глобальный параметр - проверяем его наличие в массиве параметров
                                if(isset($this->aParams[$sCurrentLayer][$sParamExpression])){

                                    // если нашли - заменяем значение
                                    $sParamExpression = $this->aParams[$sCurrentLayer][$sParamExpression]['value'];

                                } // if
                                else {
                                    // проверям на глобальный параметр с указанием слоя
                                    if(strrpos($sParamExpression, '..')){

                                        list($sTmpLayer, $sTmpPath) = explode('..',$sParamExpression,2);
                                        if(isset($this->aParams[$sTmpLayer][$sTmpPath])){
                                            // если нашли - заменяем значение
                                            $sParamExpression = $this->aParams[$sTmpLayer][$sTmpPath]['value'];
                                        }

                                    } // if
                                    else {

                                        // если нигде ненашли - возвращаем все как было
                                        $sParamExpression = '['.$sParamExpression.']';

                                    } // else
                                } // else



                            } // if
                            else $sParamExpression = (isset($this->aParams[$sCurrentLayer][$sCurrentGroup.'.'.$sParamExpression]))? $this->aParams[$sCurrentLayer][$sCurrentGroup.'.'.$sParamExpression]['value']:"[$sParamExpression]";

                            // добавляем результат выражения в скобках
                            $sCurrentLine.= $sParamExpression;
                            $iOffset = $iCommandEnd+1;
                        }
                        else {
                            break;
                        }
                    } // while

                    // Добавляем все, что лежит между закрывающей скобкой и концом строки
                    $sCurrentLine .= substr($sFileLine, $iOffset, strlen($sFileLine)-$iOffset);

                    // выполняем математику в выражении
                    $sCurrentLine = $this->calcMathString($sCurrentLine);

                    $sMergedFile .= $sCurrentLine;
                } // else
            }

            return $sMergedFile;
        }
        catch(Exception $e){
            var_dump($e);
        }
        
        return false;
    }//function parseFile()

    //TODO Перенести в отдельный класс
    public function clearCSSCache($sDirName){

        if( !is_dir($sDirName) ) return false;

        $aEntry = dir($sDirName);

        /** @noinspection PhpUndefinedFieldInspection */
        if( $aEntry->handle )
            while( false !== ($entry = $aEntry->read()) ){
                if( $entry != '.' and $entry != '..' and !is_dir($entry))
                    unlink($sDirName.$entry);

        }
        $aEntry->close();
        return true;
    }

    /**
     * Собирает набор css файлов, разбивает их по группам, сортирует
     * и отдает в виде структурированного массива
     * @param array $aInputFiles набор css файлов с описанием
     * @return array|bool
     */
    public function rebuildCSSArray($aInputFiles){

        if( $aInputFiles ){

            $aTempCSSFiles = array();

            foreach( $aInputFiles as $aFileGroup ){

                foreach( $aFileGroup as $sFileKey=>$sFile ){

                    if ( is_array($sFile) ){

                        $sCurrentLayer = ( isset($sFile['layer']) )? $sFile['layer']: Design::versionDefault;
                        $sCurrentCondition = ( isset($sFile['condition']) )? $sFile['condition']: 'default';
                        $iWeight = ( isset($sFile['weight']) )? $sFile['weight']: $this->iDefaultWeight;
                        $sCompiledPath = str_replace("/skewer_build/", BUILDPATH, $sFileKey);

                        $aTempCSSFiles[] = array(
                            'path' => $sFileKey,
                            'layer' => $sCurrentLayer,
                            'condition' => $sCurrentCondition,
                            'weight' => $iWeight,
                            'compiledPath' => $sCompiledPath
                        );
                    }
                    else{

                        $sCompiledPath = str_replace("/skewer_build/", BUILDPATH, $sFile);

                        $aTempCSSFiles[] = array(
                            'path' => $sFile,
                            'layer' => Design::versionDefault,
                            'condition' => 'default',
                            'weight' => $this->iDefaultWeight,
                            'compiledPath' => $sCompiledPath
                        );
                    }
                }
            }

            // сортировка по весам
            usort($aTempCSSFiles, function($a,$b){
                return $b['weight'] - $a['weight'];
            });

            $aCSSFiles = array();

            if ( sizeof($aTempCSSFiles) )

                foreach( $aTempCSSFiles as $aFile){

                    $aCSSFiles[$aFile['layer']][$aFile['condition']][] = $aFile['compiledPath'];
                }

            return $aCSSFiles;

        }
        return false;
    }

}
