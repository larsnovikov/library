<?php
/**
 *
 * @class Policy Класс для работы с политиками доступа
 *
 * @author Andrew, $Author: acat $
 * @version $Revision: 1475 $
 * @date $Date: 2012-12-21 15:01:54 +0400 (Пт., 21 дек. 2012) $
 * @project skewer
 * @package Build
 */
class Policy
{

    /** статус активности "Системный администратор сайта" */
    const stSys = -1;
    /** статус активности "Деактивирован" */
    const stInactive = 0;
    /** статус активности "Активирован" */
    const stActive = 1;

    /**
     * Обновить кеш политик доступа
     * @static
     * @param int $iPolicyId id политики
     * @return bool
     */
    public static function updateCache($iPolicyId){

        // Получить массив разделов, доступных для чтения по ID политики
        $aSectionAccess = self::getAccessArray($iPolicyId);

        // Запись в БД сформированного кэша
        GroupPolicyMapper::updateCacheByPolicyId($iPolicyId, $aSectionAccess, self::getPolicyVersion());
        
        return false;
    }

    /**
     * @static Метод получения массива разделов, доступных для чтения, для заданной политики доступа
     * @param int $iPolicyId ID политики доступа
     * @return array
     */
    public static function getAccessArray($iPolicyId){

        $aAccessArray = array(
            'read_access' => array()
        );
        $aAccessTypes = array();

        // Прочитать из БД - какие корневые разделы разрешены для чтения
        $aTempAccessTypes = GroupPolicyDataMapper::readAccessFields($iPolicyId);

        // Разбиваем прочитанные строки по запятой и пересобираем массив
        if ( $aTempAccessTypes && sizeof($aTempAccessTypes) )
            foreach( $aTempAccessTypes as $sKey=>$sValue ){

                $aTempSections = array();
                if ( $sValue ){

                    $aTempSections = explode(',', $sValue);
                }

                $aAccessTypes[$sKey] = $aTempSections;
            }

        // Вызаваем метод построения массива разделов, доступных для чтения
        self::getTree($aAccessArray, $aAccessTypes);

        return $aAccessArray;
    }

    /**
     * @static Рекурсивный метод построения массива разделов, доступных для чтения
     * @param $aSectionsArray Ссылка на результирующий массив
     * @param $aAccessTypes Массив возможных типов доступа к разделу
     * @param int $iState Текущее состояние
     * @param int $iParentSection Текущий родитель
     * @return bool
     */
    public static function getTree(&$aSectionsArray, $aAccessTypes, $iState = 0, $iParentSection = 0){

        // Получаем массив ID разделов по текущему родителю
        $aId = Tree::getIdByParent($iParentSection);

        // Проход по массиву полученных ID
        if ( $aId && sizeof($aId) )
            foreach( $aId as $aRow ){

                // Устанавливаем текущий ID
                $iCurrentId = $aRow['id'];
                // Устанавливаем текущее состояние
                $iCurrentState = $iState;

                // Проходимся по каждому из типов доступа к разделу и содержащимся в нем id корневых разделов, для которых задается доступ
                foreach( $aAccessTypes as $sKey=>$aTypeArray ){

                    // Если текущий ID содержится в массиве ID для рассматриваемого типа доступа - устанавливаем соответствующее текущее состояние
                    if( in_array($iCurrentId, $aTypeArray) )
                        switch($sKey){
                            case 'read_disable':
                                $iCurrentState = 0;
                            break;
                            case 'read_enable':
                                $iCurrentState = 1;
                            break;
                    }
                }

                // Если текущее состояние 0 - ID не добавляется к списку разрешенных для чтения разделов, если больше 0 ( в данном случае 1 ) - добавляется
                if ( $iCurrentState ) $aSectionsArray['read_access'][] = $iCurrentId;

                // Рекурсивный вызов метода с установленным текущим состоянием для всех подразделов текущего ID
                self::getTree($aSectionsArray, $aAccessTypes, $iCurrentState, $iCurrentId);
            }

        return true;
    }// func

    /**
     * Возвращает массив параметров функционального уровня групповой политики
     * @static
     * @param $iPolicyId
     * @return array|bool
     */
    public static function getGroupActionData($iPolicyId){

        $aFilter = array(
            'select_fields' => array('module_name','param_name','value','title'),
            'where_condition' => array(
                'policy_id' => array(
                    'sign' =>'=',
                    'value' => $iPolicyId
                )
            )
        );

        // Выборка функциональных политик из БД
        $aGroupFuncData = GroupPolicyFuncMapper::getGroupFuncData($aFilter);

        // Перемобираем массив в нужном виде:
        // <имя модуля> => <имя параметра> => <название,значение>
        $aResultData = array();
        if ( sizeof($aGroupFuncData['items']) )
            foreach($aGroupFuncData['items'] as $aItem){
                $aResultData[$aItem['module_name']][$aItem['param_name']] = array('title' => $aItem['title'], 'value' => $aItem['value']);
            }

        return $aResultData;
    }

    /**
     * Установить значение параметра функционального уровня для группы
     * @static
     * @param int $iPolicyId id политики
     * @param string $sModuleClassName имя класса модуля
     * @param string $sParamName имя параметра модуля
     * @param mixed $mValue значение
     * @param $sTitle
     * @return bool
     */
    public static function setGroupActionParam($iPolicyId, $sModuleClassName, $sParamName, $mValue, $sTitle){

        $aInputData = array(
            'policy_id' => $iPolicyId,
            'module_name' => $sModuleClassName,
            'param_name' => $sParamName,
            'value' => $mValue,
            'title' => $sTitle
        );

        return GroupPolicyFuncMapper::setGroupFuncParam($aInputData);
    }

    /**
     * Здалить значение параметра функционального уровня для группы
     * @static
     * @param int $iPolicyId id политики
     * @param string $sModuleClassName имя класса модуля
     * @param string $sParamName имя параметра модуля
     * @return bool
     */
        public static function delGroupActionParam($iPolicyId, $sModuleClassName, $sParamName){

        $aInputData = array(
            'policy_id' => $iPolicyId,
            'module_name' => $sModuleClassName,
            'param_name' => $sParamName,
            'value' => 0
        );

        return GroupPolicyFuncMapper::setGroupFuncParam($aInputData);
    }

    /**
     * @static Возвращает массив данных групповой политики
     * @param $iPolicyId
     * @throws Exception
     * @return array
     */
    public static function getGroupPolicyData($iPolicyId){

        // Формируем фильтр
        $aFilter = array(
            'select_fields' => array( 'cache_read', 'version', 'start_section' ),
            'where_condition' => array(
                'policy_id' => array(
                    'sign' => '=',
                    'value' => $iPolicyId
                )
            )
        );

        $aResultArray = array();
        // Выбираем кэш списка разделов, доступных для чтения
        $aCacheArrays = GroupPolicyDataMapper::getGroupPolicyData($aFilter);

        if ( empty($aCacheArrays) )
            throw new Exception('Ошибка при загрузке данных политики доступа');

        $aResultArray['start_section'] = $aCacheArrays['start_section'];

        $aResultArray['version'] = $aCacheArrays['version'];
        // Разбиваем список разделов, доступных для чтения по запятой и формируем из них массив
        $aResultArray['read_access'] = ( !empty($aCacheArrays['cache_read']) )? explode(',', $aCacheArrays['cache_read']): array();
        // Получаем массив функциональных политик доступа
        $aResultArray['actions_access'] = self::getGroupActionData($iPolicyId);

        return $aResultArray;
    }

    /**
     * Отдает заголовочную запись таблицы групп политик
     * @static
     * @param int $iPolicyId
     * @return array|bool
     */
    public static function getPolicyHeader($iPolicyId){

        $aResultArray = GroupPolicyMapper::getGroupPolicyHeader($iPolicyId);

        return $aResultArray;
    }


    /**
     * Добавить ветку запрещения/разрешения чтения/записи для группы
     * @static
     * @param $iPolicyId
     * @param $aReadEnable
     * @param $aReadDisable
     * @param $iStartSection
     * @return bool
     */
    public static function setGroupAccess($iPolicyId, $aReadEnable, $aReadDisable, $iStartSection) {

        $aData = array();
        $aData['policy_id']     = (int)$iPolicyId;
        $aData['read_enable']   = implode(',',$aReadEnable);
        $aData['read_disable']  = implode(',',$aReadDisable);
        $aData['start_section'] = (int)$iStartSection;

        GroupPolicyDataMapper::setGroupAccess($aData);

        Policy::incPolicyVersion();

        return false;
    }// func

    /**
     * Добавить ветку запрещения/разрешения чтения/записи для пользователя
     * @static
     * @param int $sectionId id раздела
     * @param int $accessLevel уровень доступа
     * @param int $userId id пользователя
     * @return bool
     */
    public static function setUserAccessFrom(/** @noinspection PhpUnusedParameterInspection */$sectionId, /** @noinspection PhpUnusedParameterInspection */$accessLevel, /** @noinspection PhpUnusedParameterInspection */$userId){

        return false;
    }

    /**
     * Отменить (исключить из политики) ветку доступа для пользователя
     * @static
     * @param int $sectionId id раздела
     * @param int $userId id пользователя
     * @return bool
     */
    public static function unsetUserAccessFrom(/** @noinspection PhpUnusedParameterInspection */$sectionId, /** @noinspection PhpUnusedParameterInspection */$userId) {

        return false;
    }

    public static function incPolicyVersion() {

        global $odb;

        $sQuery = "
            UPDATE
                `policy_version_counter`
            SET
                version = version + 1;";

        $odb->query($sQuery);

        return true;
    }// func


    public static function getPolicyVersion() {

        global $odb;

        $sQuery = "
            SELECT
              version
            FROM
              `policy_version_counter`;";

        return $odb->getValue($sQuery, 'version');

    }// func

    /**
     * Отдает список политик по фильтру
     * @param array $aFilter
     * @return array
     */
    public static function getPolicyList($aFilter=array()){

        return GroupPolicyMapper::getItems($aFilter);
    }

    /**
     * Сохраняет запись политики
     * @static
     * @param $aData
     * @return int|bool
     */
    public static function update( $aData ) {

        // TODO Решить вопрос с единым именованием столбца для ID политики
        $aData['policy_id'] = $aData['id'];
        $iId = GroupPolicyMapper::saveItem( $aData );
        GroupPolicyDataMapper::createRowIfNotExists( $iId );
        $iDataId = GroupPolicyDataMapper::saveItem($aData);
        self::incPolicyVersion();
        return $iId ? $iId : ($iDataId ? $iDataId : 0);

    }

    /**
     * Удаляет политику
     * @static
     * @param $iItemId
     * @return bool|int
     */
    public static function delete($iItemId) {

        $res = GroupPolicyMapper::delItem( $iItemId );
        self::incPolicyVersion();

        return $res;

    }


} // class
