<?php
/**
 * Класс для работы с SEO данными для всех типов объектов
 *
 * Основные поля
 *  заголовок
 *  описание
 *  ключевые слова
 *  частота обновления
 *  приоритет
 */
class SEOData {

    /** префикс имен полей */
    const fieldPrefix = 'seodata';

    /**
     * Отдает данные для заданных группы и id
     * @static
     * @param string $sGroupName имя группы
     * @param int $iRowId id целевой строки
     * @return array
     */
    public static function get( $sGroupName, $iRowId ) {

        $aFilter = array(
            'where_condition' => array(
                'group' => array(
                    'sign' => '=',
                    'value' => $sGroupName
                ),
                'row_id' => array(
                    'sign' => '=',
                    'value' => $iRowId
                ),
            )
        );

        $aData = SEODataMapper::getItem( $aFilter );

        return $aData;

    }

    /**
     * @static
     * @param string $sGroupName имя группы
     * @param int $iRowId id целевой строки
     * @param array $aData данные для сохранения
     * @return int
     */
    public static function set( $sGroupName, $iRowId, $aData ) {

        // запросить существующую запись
        $aOldData = self::get( $sGroupName, $iRowId );

        // флаг наличия данных
        $bHasData = self::hasData( $aData );

        // если есть данные - сохранить/обновить
        if ( $bHasData ) {

            // id для сохраняемой записи
            $aData['id'] = $aOldData ? $aOldData['id'] : 0;
            $aData['group'] = $sGroupName;
            $aData['row_id'] = $iRowId;

            // сохранение
            return SEODataMapper::saveItem($aData);

        }

        // если данных нет, а запись есть
        elseif ( $aOldData ) {

            // удалить
            self::del( $sGroupName, $iRowId );

        }

        return 0;

    }

    /**
     * Удаляет запись
     * @static
     * @param string $sGroupName имя группы
     * @param int $iRowId id целевой строки
     * @return bool
     */
    public static function del( $sGroupName, $iRowId ) {

        // запросить существующую запись
        $aData = self::get( $sGroupName, $iRowId );

        return $aData ? SEODataMapper::delItem((int)$aData['id']) : false;

    }

    /**
     * Отдает флаг наличия данных
     * @static
     * @param array $aData
     * @return bool
     */
    protected static function hasData( $aData ) {

        // набор значимых полей
        $aFields = SEOAdmMapper::getDataFields();

        // пытаемся найти значиения в пришедшем массиве
        foreach ( $aFields as $sName )
            if ( isset($aData[$sName]) and $aData[$sName] )
                return true;

        // если не нашли
        return false;

    }

    /**
     * Отдает набор полей для редактирования в JS интерфейсе
     * @static
     * @param string $sGroupName имя группы
     * @param int $iRowId id целевой строки
     * @return array
     */
    public static function getJSFields( $sGroupName, $iRowId ) {

        // запрос данных
        $aData = self::get( $sGroupName, $iRowId );

        // инициализация полей
        Ext::init();
        $oForm = new ExtForm();
        $oForm->setFields( SEOAdmMapper::getFullParamDefList( SEOAdmMapper::getDataFields() ) );
        $oForm->setValues( $aData );

        // набор исходных параметров
        $aSrcFields = $oForm->getInterfaceArray();

        // преобразование набора полей
        $aItems = array();
        foreach ( $aSrcFields['items'] as $aField ) {
            $aField['name'] = sprintf(
                '%s_%s_%s',
                self::fieldPrefix,
                $sGroupName,
                $aField['name']
            );
            $aItems[] = $aField;
        }

        return array(
            'name' => 'seo_'.$sGroupName,
            'collapsed' => !$aData,
            'type' => 'specific',
            'view' => 'specific',
            'extendLibName' => 'SEOFields',
            'layerName' => 'adm',
            'title' => 'SEO данные',
            'group_name' => $sGroupName,
            'row_id' => $iRowId,
            'data' => self::get($sGroupName, $iRowId),
            'fieldConfig' => $aItems
        );

    }

    /**
     * Сохранение данных, пришедших из админского интерфейса
     * @static
     * @param string $sGroupName имя группы
     * @param int $iRowId id целевой строки
     * @param array $aData массив на сохранение
     * @return bool
     */
    public static function saveJSData( $sGroupName, $iRowId, $aData ) {

        // данные для
        $aSaveData = array();

        // префикс полей
        $sPrefix = sprintf( '%s_%s_', self::fieldPrefix, $sGroupName );

        // набор полей для сохранения
        $aAllowFields = SEOAdmMapper::getDataFields();

        foreach ( $aAllowFields as $sName ) {

            // полное имя (с префиксом)
            $sFullName = $sPrefix.$sName;

            // проверить наличие поля
            if ( !array_key_exists( $sFullName, $aData ) )
                continue;

            // добавить в массив на сохранение
            $aSaveData[$sName] = $aData[$sFullName];

        }

        // если нечего сохранять
        if ( !$aSaveData )
            return false;

        return self::set( $sGroupName, $iRowId, $aSaveData );

    }

    /**
     * Добавляет набор SEO полей к форме
     * @static
     * @param ExtForm $oForm
     * @param string $sGroupName имя группы
     * @param int $iRowId id целевой строки
     */
    public static function appendExtForm( ExtForm $oForm, $sGroupName, $iRowId ) {

        // описание спец поля
        $aField = SEOData::getJSFields( $sGroupName, $iRowId );

        // созадние объекта
        $oField = Ext::makeFieldObject( $aField );

        // добавление объекта поля к списку
        $oForm->addField( $oField );

        // добавление дополнительной JS библиотеки
        $oForm->addLibClass( 'SEOFields', 'adm', 'SEO' );

    }

}
