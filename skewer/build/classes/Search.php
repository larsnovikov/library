<?php
/**
 * Created by JetBrains PhpStorm.
 * User: ACat
 * Date: 31.01.12
 * Time: 17:38
 * To change this template use File | Settings | File Templates.
 */
class Search {

    var $classNames = array(
        'catalog'     => 'Каталог',
        'gallery'     => 'Фотогалерея',
        'guest_book'  => 'Гостевая книга',
        'news'        => 'Новости',
        'section'     => 'Текстовые разделы',
    );

    var $frequency = array(
        'news'       => 'weekly',
        'catalog'    => 'weekly',
        'gallery'    => 'weekly',
        'section'    => 'weekly',
        'guest_book' => 'weekly',
    );

    var $allowFrequency = array(
        'always',
        'hourly',
        'daily',
        'weekly',
        'monthly',
        'yearly',
        'never',
    );

    /**
     * @static
     * @param $sSearchTitle
     * @param $sSearchText
     * @param $iObjectId
     * @param $sClassName
     * @param $iSectionId
     * @param int $iLanguage
     * @return bool|int
     */
    public static function addToIndex($sSearchTitle, $sSearchText, $iObjectId, $sClassName, $iSectionId, $sHref,$iLanguage=1){

        $aData = array(
            'search_title' => $sSearchTitle,
            'search_text' => $sSearchText,
            'object_id' => $iObjectId,
            'class_name' => $sClassName,
            'section_id' => $iSectionId,
            'lang' => $iLanguage,
            'href' => $sHref
        );

        return SearchMapper::saveItem($aData);

    } // function

    /**
     * @static
     * @param $iObjectId
     * @param $sClassName
     * @return bool|mysqli_result
     */
    public static function removeFromIndex($iObjectId, $sClassName){

        $aData = array(
            'object_id' => $iObjectId,
            'class_name' => $sClassName
        );

        return SearchMapper::removeFromIndex($aData);
    } // function

    /**
     * Фукнция, позволяющая перезаполнить таблицу поискового индекса
     * TODO Сделать возможность задавать отдельные позиции или классы для перестройки индекса
     * @static
     * @param array $aKey
     * @return bool
     */
    public static function resetIndexTable($aKey = array()){

        $aIndexData = array();

        $aModules = array(
            'Page', 'Events', 'News', 'Articles'
        );

        foreach( $aModules as $sModule ){

            $sModule = $sModule.'AdmApi';
            $aIndexData = array_merge($aIndexData, $sModule::getIndexData());
        }

        SearchMapper::clearSearchTable();

        foreach( $aIndexData as $aIndexRow ){

            self::addToIndex($aIndexRow['search_title'], $aIndexRow['search_text'], $aIndexRow['id'], $aIndexRow['class_name'], $aIndexRow['parent_section'], $aIndexRow['href']);
        }

        return true;
    }

    public static function updateIndex($iRow = 0){

    } // function

} // class
