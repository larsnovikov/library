<?php
/**
 * Created by JetBrains PhpStorm.
 * User: User
 * Date: 28.11.12
 * Time: 15:16
 * To change this template use File | Settings | File Templates.
 */
class ConsoleProcessor extends skProcessorPrototype implements skProcessorInterface {

    /** @var array Набор входных параметров в cli режиме */
    protected $aArgs = array();

    /**
     * Задает набор входных параметров в cli режиме
     * @param array $aArgs
     */
    public function setCliArgs( $aArgs ) {
        $this->aArgs = $aArgs;
    }

    /**
     * Корневой метод выполнения процессора
     * @static
     * @throws Exception
     * @return bool|string
     */
    final public function build() {

        $argv = $this->aArgs;

        if(!isSet($argv[1])) $argv[1] = 'help';

        switch($argv[1]){
            case 'install':

                if(!isSet($argv[2])) die(' Неверные аргументы!');
                $sModuleName = $argv[2];

                $oInstaller = new skInstaller($sModuleName);
                $oInstaller->registerModule();
                print('Complete!');

                break;

            case 'deinstall':

                if(!isSet($argv[2])) die(' Неверные аргументы!');
                $sModuleName = $argv[2];

                $oInstaller = new skInstaller($sModuleName);
                $oInstaller->unregisterModule();
                print("Complete!\r\n");

                break;

            case 'reinstall':

                if(!isSet($argv[2])) die(' Неверные аргументы!');
                $sModuleName = $argv[2];

                $oInstaller = new skInstaller($sModuleName);
                $oInstaller->unregisterModule();
                $oInstaller->registerModule();
                print("Complete!\r\n");

                break;

            case 'applyPatch':

                try{
                    $oUpdateHelper = new skUpdateHelper();
                    $oUpdateHelper->installPatch( $argv[2] );
                } catch ( UpdateException $e ) {
                    die( $e->getMessage()."\r\n" );
                }

                print("Complete!\r\n");

                break;

            case 'createTest':

                try{

                    if(!isSet($argv[2]))
                        throw new Exception('Неверные аргументы!');
                    $sClassName = (string)$argv[2];

                    // флаг запуса
                    $bRun = isSet($argv[3]) && $argv[3]==='-r';

                    $sOut = $this->createTest( $sClassName );
                    echo "$sOut\r\n";
                    if ( $bRun ) {
                        echo $this->runTest( $sClassName );
                        echo "\r\n";
                    }


                } catch ( Exception $e ) {
                    die( $e->getMessage()."\r\n" );
                }

                break;

            case 'runTest':

                try{

                    if(!isSet($argv[2]))
                        throw new Exception('Неверные аргументы!');
                    $sClassName = (string)$argv[2];

                    echo $this->runTest( $sClassName );
                    echo "\r\n";

                } catch ( Exception $e ) {
                    die( $e->getMessage()."\r\n" );
                }

                break;

            case 'help':
            default:

                print("\r\n/************* Skewer console ************/\r\n");
                print("Доступны следующие комманды:\r\n");
                print("install <moduleName> - Установить модуль\r\n");
                print("deinstall <moduleName> - Удалить модуль\r\n");
                print("reinstall <moduleName> - Переустановить модуль\r\n");
                print("applyPatch <patchName> - Установить патч\r\n");
                print("createTest <className> [-r]- Создать тест [и запустить его]\r\n");
                print("runTest <className>/-a - Запустить тест/все тесты\r\n");
                print("\r\n/*****************************************/\r\n");
                break;
        }


    }// func

    /**
     * Создает файл теста для заданного класса
     * @param $sClassName
     * @return string строка вывода консоли
     * @throws Exception
     */
    protected function createTest( $sClassName ) {

        if ( !class_exists( $sClassName ) )
            throw new Exception('Класс не найден!');

        // имя тестового класса
        $sTestClassName = $sClassName.'Test';

        // путь к файлу с исходным классом
        $sPath = skAutoloader::getClassPath( $sClassName );

        // путь до файла
        $sTestPath = $this->getTestFilePath( $sClassName );

        // директория для файла
        $sTestPathDir = dirname($sTestPath);
        if ( !is_dir($sTestPathDir) )
            mkdir($sTestPathDir, 0777, true);

        $sCmd = sprintf(
            'phpunit-skelgen --test -- %s %s %s %s',
            $sClassName,
            $sPath,
            $sTestClassName,
            $sTestPath
        );

        return shell_exec( escapeshellcmd($sCmd) );

    }

    /**
     * Отдает путь для файла теста заданного класса
     * @param $sClassName
     * @return string
     */
    protected function getTestFilePath( $sClassName ) {
        $sPath = skAutoloader::getClassPath( $sClassName );
        $sTestPath = str_replace( RELEASEPATH, $this->getTestDir(), $sPath );
        $sTestPath = dirname( $sTestPath ) . '/' . $sClassName . 'Test.php';
        return $sTestPath;
    }

    /**
     * Отдает имя директории для тестов
     * @return string
     */
    protected function getTestDir() {
        return ROOTPATH . 'tests/';
    }

    /**
     * Запускае тест в консоли
     * @param $sClassName
     * @return string строка вывода консоли
     * @throws Exception
     */
    protected function runTest( $sClassName ) {

        if ( $sClassName === '-a' ) {
            $sPathToTest = $this->getTestDir();
        } else {
            if ( !class_exists( $sClassName ) )
                throw new Exception('Класс не найден!');

            $sPathToTest = $this->getTestFilePath( $sClassName );
        }

        $sCmd = sprintf(
            'phpunit --colors --configuration %sinit.xml -v %s',
            $this->getTestDir(), $sPathToTest
        );

        return shell_exec( escapeshellcmd($sCmd) );

    }

}// class
