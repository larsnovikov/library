<?php
/**
 *
 *
 * @class: Catalog
 *
 * @Author: User, $Author: sapozhkov $
 * @version: $Revision: 6 $
 * @date: $Date: 2012-05-18 10:52:53 +0400 (Пт., 18 мая 2012) $
 *
 */

ft::init();

class Catalog {

    /** @var bool флаг "инициализировано" */
    protected static $bInited = false;

    /**
     * Отдает корневую директорию модуля
     * @static
     * @return string
     */
    protected static function getDir() {
        return BUILDPATH.'/libs/Catalog/';
    }

    /** string префикс имен таблиц */
    const tablePrefix = 'c_';

    /**
     * Инициализация
     * @static
     */
    public static function init() {
        ft::init();
        if ( self::$bInited )
            return;
        self::$bInited = true;
        spl_autoload_register(array(__CLASS__,'autoload'));
    }

    /**
     * Отдает имя файла класса, если найдет
     * @static
     * @param $sClassName
     */
    protected static function autoload( $sClassName ) {
        if ( preg_match('/^c[A-Z][A-z]*$/',$sClassName) ) {
            $sFile =  self::getDir() . $sClassName . '.php';
            if ( is_file($sFile) ) {
                /** @noinspection PhpIncludeInspection */
                require_once $sFile;
            }
        }
    }

    /**
     * Инициализирует модель, если она еще не была загружена
     * @static
     * @param $sModelName
     * @return \ftModel|null
     */
    protected static function getModel( $sModelName ) {
        // todo раскомментировать когда будет система автоматического обновления кэша моделей
//        if ( !ftCache::exists(self::tablePrefix.$sModelName) ){
            /** @noinspection PhpIncludeInspection */
            include_once( self::getDir() . 'model/' . $sModelName . '.php' );
//        }
        return ftCache::get(self::tablePrefix.$sModelName);
    }

    /**
     * Отдает модель для описания сущностей
     * @static
     * @return \ftModel|null
     */
    public static function getEntityModel() {
        return self::getModel('entity');
    }

    /**
     * Отдает модель для описания полей сущности
     * @static
     * @return \ftModel|null
     */
    public static function getFieldModel() {
        return self::getModel('field');
    }

    /**
     * Отдает модель для описания атрибутов полей сущности
     * @static
     * @return \ftModel|null
     */
    public static function getAttrModel() {
        return self::getModel('attr');
    }

    /**
     * Отдает модель для описания шаблонов атрибутов полей сущности
     * @static
     * @return \ftModel|null
     */
    public static function getAttrTplModel() {
        return self::getModel('attr_tpl');
    }

    /**
     * Отдает модель для описания групп полей сущности
     * @static
     * @return \ftModel|null
     */
    public static function getFieldGroupModel() {
        return self::getModel('field_group');
    }

}
