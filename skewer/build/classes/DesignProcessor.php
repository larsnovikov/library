<?php
/**
 * 
 * Менеджер процессов для системы администрирования
 * @class DesignProcessor
 * @project Skewer
 * @package Build
 *
 * @extends skProcessorPrototype
 * @implements skProcessorInterface
 * @author ArmiT, $Author: acat $
 * @version $Revision: 974 $
 * @date $Date: 2012-10-05 12:54:23 +0400 (Пт., 05 окт. 2012) $
 *
 *
 */ 
class DesignProcessor extends CmsProcessorPrototype {

    /**
     * Отдает имя ключа для сессионного хранилища
     * @return string
     */
    protected function getSessionKeyName() {
        return 'designKey';
    }

    /**
     * Возвращает имя модуля основного слоя
     * @return string
     */
    public function getLayoutModuleName() {
        return 'LayoutDesignModule';
    }

    /**
     * Возвращает имя модуля авторизации
     * @return string
     */
    public function getAuthModuleName() {
        return 'AuthCmsModule';
    }

    /**
     * Возвращает имя первично инициализируемого модуля
     * @return string
     */
    public function getFrameModuleName() {
        return 'FrameDesignModule';
    }

    /**
     * Возвращает имя первично инициализируемого модуля при отсутствии авторизации
     * @return string
     */
    public function getFrameAuthModuleName() {
        return 'FrameCmsModule';
    }

    /**
     * Отдает базовый url для сервиса
     * @return string
     */
    public function getBaseUrl() {
        return '/design/';
    }

}
