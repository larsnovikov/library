<?php
/**
 * Инструменты площадки, разрешенные к удвленному запуску
 * @class HostService
 *
 * @author ArmiT, $Author: acat $
 * @version $Revision: 1614 $
 * @date $Date: 2013-01-28 18:36:46 +0400 (Пн, 28 янв 2013) $
 * @project JetBrains PhpStorm
 * @package kernel
 */
class HostTools extends ServicePrototype {

    /**
     * Возвращает статус площадки
     * @return string
     */
    public function getStatus() {

        $aOut['status'] = '200OK';
        $aOut['host']   = $_SERVER['HTTP_HOST'];
        $aOut['build_number'] = BUILDNUMBER;
        $aOut['build_name']  = BUILDNAME;
        return skYaml::dump($aOut);

    }// func

    /**
     * Устанавливает патч
     * @param string $sPatchFile путь к исполняемому файлу патча относительно корня директории обновлений
     * @throws skGatewayExecuteException
     * @return bool
     */
    public function installPatch($sPatchFile) {

        try {

            $oUpdateHelper = new skUpdateHelper();
            $oUpdateHelper->installPatch( $sPatchFile );

        } catch(UpdateException $e) {

            /* что-то пошло не так. Файл обновления имеет неверный формат либо не достаточно входных параметров */
            throw new skGatewayExecuteException($e->getMessage());

        }

        return true;
    }// func

    /**
     * Запуск после обновления
     * @param bool $bClear
     * @return bool
     */
    public function UpdateComplete($bClear = true) {

        // clear
        if($bClear) {
			skLoggerMapper::clearLog();
	        TaskMapper::clearLog();
	        DomainsToolMapper::clearLog();
	        SubscribeToolMapper::clearPostingLog();
		}
		
        // upd
        Redirect301ToolApi::makeHtaccessFile();
        Search::resetIndexTable();
        SEOTemplatesToolService::makeSiteMap();
        SEOTemplatesToolService::updateRobotsTxt(false);

        return true;
    }// func


    /**
     * @param $sTime
     * @return bool
     */
    public function updTimeBackup($sTime) {

        BackupToolApi::updBackupTime($sTime);

        return true;
    }


    public function updRobotTxt($sDomain = false){

        SEOTemplatesToolService::updateRobotsTxt($sDomain);

        return true;
    }


    public function syncDomain($aDomains){

        DomainsToolApi::syncDomains($aDomains);

        return true;
    }

    /**
     * Изменение пароля для политики admin
     * @param $sNewPass
     * @return bool|int
     */
    public function replaceAdmPass($sNewPass){

        $iLoginId = UsersToolApi::getIdByLogin( 'admin' );

        if(!$iLoginId)
            return false;

        $aSaveArr = array(
            'id' => $iLoginId,
            'login' => 'admin',
            'pass' => Auth::buildPassword('admin',$sNewPass)
        );

        $bRes = UsersToolApi::updUser( $aSaveArr );

        return $bRes;
    }
    
    /**
     * Разрещает либо запрещает работу процессоров в
     * @param bool $bEnabled
     * @return bool
     */
    public function enableProcessor($bEnabled = true) {
    
        SysVar::set('ProcessorEnable', (int)$bEnabled);
    
    }// func    

}// class
