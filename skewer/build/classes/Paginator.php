<?php
/**
 * 
 * @class Paginator
 *
 * @author ArmiT, $Author: sapozhkov $
 * @version $Revision: 6 $
 * @date $Date: 2012-05-18 10:52:53 +0400 (Пт., 18 мая 2012) $
 * @project Skewer
 * @package Build
 *
 * @example
 * $aURLParams['NewsModule'] array('onpage'=>100);
 * $aParams['onPage'] = 100;
 * $aParams['onGroup'] = 10;
 * $aParams['useGroups'] = true;
 * $aParams['useEdges'] = true;
 * $aParams['useItems'] = true;
 * Paginator::getPageLine(1, 100, 5, $aURLParams, $aParams);
 */ 
class Paginator {

    static public function getPageLine($iPage, $iCount, $iSectionId, $aURLParams = array(), $aParams = array()){

        // init default settings

        $iOnPage       = (isSet($aParams['onPage']))? $aParams['onPage']: 10;
        $iOnGroup      = (isSet($aParams['onGroup']))? $aParams['onGroup']: 10;
        $bUseEdges     = (isSet($aParams['useEdges']))? $aParams['useEdges']: true;
        $bUseItems     = (isSet($aParams['useItems']))? $aParams['useItems']: true;
        $bUsePages     = (isSet($aParams['usePages']))? $aParams['usePages']: true;
        $bUseGroups    = (isSet($aParams['useGroups']))? $aParams['useGroups']: true;
        //$bUseShortKeys = (isSet($aParams['useShortKeys']))? $aParams['useShortKeys']: true;

        if(!$iOnPage)           return false;
        if(!count($aURLParams)) return false;

        //$aURLParams['newsModule']['mode'] = 'onindex';
        //$aURLParams['newsModule']['onpage'] = 50;
        reset($aURLParams);
        $aGet = array();
        list($sModuleName, $aURLParams) = each($aURLParams);

        if(count($aURLParams))
            foreach($aURLParams as $sKey=>$sValue)
                $aGet[] = $sKey.'='.$sValue;

        $aPages['module']     = $sModuleName;
        $aPages['sectionId']  = $iSectionId;
        $aPages['parameters'] = (count($aGet))? implode('&', $aGet): '';

        $iPagesCount   = ceil($iCount/$iOnPage);
        $iCurrentGroup = ceil($iPage/$iOnGroup);
        $iGroupsCount  = ceil($iPagesCount/$iOnGroup);
        $iGroupCounter = (($iCurrentGroup-1)*$iOnGroup);

        if($bUseItems)
            if($iPagesCount) {
                $aPages['itemsIsActive'] = true;
                for($i=$iGroupCounter; $i<$iGroupCounter+$iOnGroup; ++$i) {

                    if($i == $iPagesCount) break;

                    $aItem['title'] = $i+1;
                    $aItem['page']  = $i+1;
                    $aItem['isActive'] = (($i+1)==$iPage)? true: false;
                    $aPages['items'][] = $aItem;
                }// each item
            }// if pages count

        if($bUsePages){

            $aPages['prevPage']['isActive'] = ($iPage > 1)? true: false;
            $aPages['prevPage']['page'] =  ($iPage > 1)? $iPage - 1: $iPage;
            $aPages['prevPage']['title'] = 'Предыдущая';

            $aPages['nextPage']['isActive'] = ($iPage < $iPagesCount)? true: false;
            $aPages['nextPage']['page'] = ($iPage < $iPagesCount)? $iPage + 1: $iPage;
            $aPages['nextPage']['title'] = 'Следующая';
        }// use previos/next pageLinks

        if($bUseEdges) {

            $aPages['firstPage']['isActive'] = ($iPage > 1)? true: false;
            $aPages['firstPage']['page'] = 1;
            $aPages['firstPage']['title'] = '<<';


            $aPages['lastPage']['isActive'] = ($iPage < $iPagesCount)? true: false;
            $aPages['lastPage']['page'] = $iPagesCount;
            $aPages['lastPage']['title'] = '>>';

        }// use edges Links

        if($bUseGroups) {

            $aPages['prevGroup']['isActive'] = ($iCurrentGroup > 1)? true: false;
            $aPages['prevGroup']['page'] = ($iCurrentGroup > 1)? $iOnGroup*($iCurrentGroup-1): 1;
            $aPages['prevGroup']['title'] = '...';

            $aPages['nextGroup']['isActive'] = ($iGroupsCount && ($iCurrentGroup < $iGroupsCount))? true: false;
            $aPages['nextGroup']['page'] = ($iGroupsCount && ($iCurrentGroup < $iGroupsCount))? ($iOnGroup*$iCurrentGroup)+ 1: $iOnGroup*$iCurrentGroup;
            $aPages['nextGroup']['title'] = '...';

        }// use previos/next groupsLinks

        return $aPages;
    }// func

}// class
