<?php
/**
 * Библиотека для автоматического изменения размеров изображений на странице
 * и добавления всплывающих JS окон
 *
 * @class: ImageResizer
 * @project Skewer
 * @package Build
 *
 * @Author: sapozhkov, $Author: sokol $
 * @version: $Revision: 532 $
 * @date: $Date: 2012-08-01 10:51:46 +0400 (Ср., 01 авг. 2012) $
 *
 */

class ImageResize {

    /**
     * Директория для изображений с измененным размером
     */
    const resizeDir = 'img/resize/';

    /**
     * Класс для JS обработки на странице
     */
    const addJsClass = 'js_use_resize';

    /**
     * Преобразует текст с html разметкой: Для изображений с заданными шириной и высотой
     * создает копию с измененными размерами и оборачивает изображения в теги <a /> со
     * ссылкой на первичное изображение. Текже ссылкам добавляется класс из переменной addJsClass
     * для JS обработки на клиентской стороне
     *
     * @static
     * @param string $sInHtml - текст с html разметкой
     * @param int [$iSectionId] - id раздела для сохранения
     * @param bool [$bPrivate] - флаг закрытого раздела
     * @return string
     */
    public static function wrapTags($sInHtml, $iSectionId=0, $bPrivate=false) {

        $sOutHtml = $sInHtml;

        // ищем фотки в тексте все кроме лежащих в папке resized
        $aAnchorMatches = array();
        preg_match_all('/((<a [^>]*>)[ ]*)?(<img[^>]*?src[ ]*=[ ]*"?\'?([^"^\']+)"?\'?[^>]*>)/i', $sInHtml, $aAnchorMatches);

        // Loop through matches and find if replacements are necessary.
        // $matches[0]: All complete image tags and preceeding anchors.
        // $matches[1]: The anchor tag of each match (if any).
        // $matches[2]: The anchor tag and trailing whitespace of each match (if any).
        // $matches[3]: The complete img tag.
        // $matches[4]: The src value of each match.
        foreach ($aAnchorMatches[3] as $iKey => $sMatch) {

            $sAnchorText = $aAnchorMatches[0][$iKey];
            $sImgSrc = $aAnchorMatches[4][$iKey];

            // проверяем на присутствие размеров
            $aWidth = array();
            $aHeight = array();
            preg_match_all( '{[^-]width[ ]?=?:?[ ]?"?\'?([0-9]+)(%)?}i', $sMatch, $aWidth);
            preg_match_all('{[^-]height[ ]?=?:?[ ]?"?\'?([0-9]+)(%)?}i', $sMatch, $aHeight);
            preg_match_all('{margin[ ]?=?:?[ ]?"?\'?([0-9]+)(%)?}i', $sMatch, $aMargin);

            // флаг на проценты
            $bPerWidth  = (bool)(count($aWidth[2]) and $aWidth[2][0] != '');
            $bPerHeight = (bool)(count($aHeight[2]) and $aHeight[2][0] != '');

            //ширина и высота
            $iWidth =  count($aWidth[1]) ? $aWidth[1][0] : 0;
            $iHeight = count($aHeight[1]) ? $aHeight[1][0] : 0;
            $iMargin = count($aMargin[1]) ? 0 : 5;

            // проверяем на присутствие класса self::addJsClass
            if (!($iWidth || $iHeight) || preg_match('{class[ ]?=[ ]?"?\'?'.self::addJsClass.'}i', $aAnchorMatches[1][$iKey])) continue;

            // получение лин. размеров и типа картинки
            $sRealFullName = ROOTPATH.$sImgSrc;
            if ( !file_exists($sRealFullName) )
                continue;

            list($iRealWidth, $iRealHeight, $iType) = getimagesize($sRealFullName);

            // завершить шаг цикла, если файл не прочитан
            if ( !$iRealWidth or !$iRealHeight or !$iType )
                continue;

            // если размеры совпадают, делать ничего не надо
            if ( $iRealHeight==$iHeight and $iRealWidth==$iWidth ){
                if($iMargin){ // добавление отступа
                    $sNewString = str_replace('style="', 'style="margin:'.$iMargin.'px; ', $sAnchorText);
                    $sNewString = str_replace("<img ", '<img original="' . str_replace(ROOTPATH, "/", $sImgSrc) . '" ', $sNewString);
                    $sOutHtml = preg_replace(self::preg_regex_to_pattern($sAnchorText), $sNewString, $sOutHtml, 1);
                }
                continue;
            }


            // если заданы проценты, а не пиксели
            if ($bPerWidth) $iWidth = round($iRealWidth * $iWidth / 100);
            if ($bPerHeight) $iHeight = round($iRealHeight * $iHeight / 100);

            // если один из параметров не задан
            //ширина
            if (!$iWidth) {
                $iWidth = round($iRealWidth * $iHeight / $iRealHeight);
            }
            //высота
            if (!$iHeight) {
                $iHeight = round($iRealHeight * $iWidth / $iRealWidth);
            }

            // папка для загрузки
            $sFileDir = skFiles::createFolderPath( $iSectionId.'/resize/', $bPrivate );
            if ( !$sFileDir ) continue;

            // разбираем старое имя
            $sRealName = substr(strrchr($sRealFullName, '/'), 1);
            $sFileExtension = substr(strrchr($sRealName, '.'), 1);
            $sRealName = substr($sRealName, 0, strpos($sRealName, "."));

            //новое имя
            $sNewName = sprintf('%s_%d_%d.%s', $sRealName, $iWidth, $iHeight, $sFileExtension );
            $sNewFullName = $sFileDir.$sNewName;

            // если файла с таким именем нет - создаем
            if ( !file_exists($sNewFullName) ) {

                // копирование фотки в нужную папку с нужными размерами
                $hImageP = imagecreatetruecolor($iWidth, $iHeight);

                // флаг "на создание"
                $make_photo = true;

                // при создании файла поддерживаемого типа - создать
                switch ($iType) {
                    case 1: // gif
                        $hImage = imagecreatefromgif($sRealFullName);
                        break;
                    case 2: // jpeg
                        $hImage = imagecreatefromjpeg($sRealFullName);
                        break;
                    case 3: // png
                        $hImage = imagecreatefrompng($sRealFullName);
                        break;
                    default:
                        // неподдерживаемый тип - снять флаг
                        $make_photo = false;
                        $hImage = null;
                } // switch

                // проверка наличия флага создания
                if ($make_photo) {

                    // создать изображение с новыми размерами
                    imagecopyresampled($hImageP, $hImage, 0, 0, 0, 0, $iWidth, $iHeight, $iRealWidth, $iRealHeight);

                    // сохранить на диск
                    switch ($iType) {
                        case 1:
                            imagegif($hImageP, $sNewFullName);
                            break;
                        case 2:
                            imagejpeg($hImageP, $sNewFullName, 80);
                            break;
                        case 3:
                            imagepng($hImageP, $sNewFullName);
                            break;
                    } // switch

                    chmod($sNewFullName,0777);

                    // сбросить контейнеры
                    unset($hImage);
                    unset($hImageP);

                }

            }

            //меняем адрес у картинки
            $sWebDir = str_replace(ROOTPATH, "/", $sFileDir);
            $sNewString = str_replace($sImgSrc, $sWebDir.$sNewName, $sAnchorText);

            // добавляем отступ, если его небыло [ilya:14.05.12]
            if($iMargin)
                $sNewString = str_replace('style="', 'style="margin:'.$iMargin.'px; ', $sNewString);


            if ($aAnchorMatches[1][$iKey] == '') { // если нет ссылки - оборачиваем
                $sNewString = '<a href="' . $sImgSrc . '" class="'.self::addJsClass.'" data-fancybox-group="button" >' . $sNewString . '</a>';
            }
            else { //если есть - добавляем параметр с адресом оригинальной картинки
                $sNewString = str_replace("<img ", '<img original="' . str_replace(ROOTPATH, "/", $sImgSrc) . '" ', $sNewString);
            }

            $sOutHtml = preg_replace(self::preg_regex_to_pattern($sAnchorText), $sNewString, $sOutHtml, 1);

        }

        return $sOutHtml;

    } // function

    /**
     * Восстановление оригинального html кода из результата работы преобразователя
     *
     * @static
     * @param $sInHtml - текст с html разметкой
     * @return string
     */
    public static function restoreTags($sInHtml){

        $sOutHtml = $sInHtml;

        // ищем картинки в ссылках
        $aAnchorMatches = array();
        preg_match_all('/((<a [^>]*>)[ ]*)(<img[^>]*?src[ ]*=[ ]*"?\'?([^"^\']+)"?\'?[^>]*>)[ ]*<\/a>/i', $sInHtml, $aAnchorMatches);

        // перебираем то, что нашли
        foreach ($aAnchorMatches[0] as $iKey => $sMatch) {

            $sAnchorText = $aAnchorMatches[0][$iKey];
            $sAnchorTag = $aAnchorMatches[1][$iKey];
            $sImgText = $aAnchorMatches[3][$iKey];
            $sImgSrc = $aAnchorMatches[4][$iKey];

            // проверка на наличие спец. класса self::addJsClass
            $isResized = preg_match('/class[ ]?=[ ]?"?\'?' . self::addJsClass . '/', $sAnchorTag);

            // путь к первоначальному файлу от корня
            if ($isResized) { // со спец классом
                $aSrcMatch = array();
                preg_match_all('{href[ ]?=[ ]?"?\'?([^\'^"]+)}i', $sAnchorTag, $aSrcMatch);
                if (count($aSrcMatch[1])) $sSrcFile = $aSrcMatch[1][0];
                else continue;
            }
            else { //ссылка не на увеличенное изображения
                $aSrcMatch = array();
                preg_match_all('{original[ ]?=[ ]?"?\'?([^\'^"]+)}i', $sImgText, $aSrcMatch);
                if (count($aSrcMatch[1])) $sSrcFile = $aSrcMatch[1][0];
                else continue;
            }

            // полное имя файла назначения
            $sFullDstFileName = ROOTPATH.$sSrcFile;

            // полное имя исходного файла
            $sFullSrcFileName = ROOTPATH.substr($sImgSrc, 1);

            // если такого нет, а исходный есть - копируем ресайзеный туда с нужным именем
            if ( file_exists($sFullSrcFileName) and !file_exists( $sFullDstFileName )) {
                copy($sFullSrcFileName, $sFullDstFileName);
            }

            // заменяем
            if ($isResized) { // со спец классом
                $sNewString = $sImgText;
            }

            //ссылка не на увеличенное изображения
            else {
                $sNewString = preg_replace('{original[ ]?=[ ]?"?\'?([^\'^"]+)"?\'?[ ]?}i', "", $sAnchorText);
            }

            $sNewString = str_replace($sImgSrc, $sSrcFile, $sNewString);
            $sOutHtml = preg_replace(self::preg_regex_to_pattern($sAnchorText), $sNewString, $sOutHtml, 1);

        }

        return $sOutHtml;

    } // function

    /**
     * Возвращает строку, пригодную к использованию в качестве шаблона.
     * Строка окружается разделителями и к ней добавляются модификаторы
     *
     * @static
     * @param string $raw_regex - шаблон
     * @param string $modifiers [optional] - модификаторы
     * @return string
     */
    protected static function preg_regex_to_pattern($raw_regex, $modifiers = '') {

        if (! preg_match('{\\\\(?:/;$)}', $raw_regex)) {
            $cooked = preg_replace('!/!', '\/', $raw_regex);
        } else {
            $pattern = '{ [^\\\\/]+ |\\\\. |( / |\\\\$ ) }sx';

            $f = create_function('$matches', '
            if (empty($matches[1]))
            return $matches[0];
            else
            return "\\\\" . $matches[1];  // code.
            ');

            $cooked = preg_replace_callback($pattern, $f, $raw_regex);
        }

        return "/$cooked/$modifiers";
    }

}
