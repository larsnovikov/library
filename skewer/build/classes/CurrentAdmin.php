<?php
/**
 *
 * @class CurrentAdmin
 *
 * @author ArmiT, $Author: sapozhkov $
 * @version $Revision: 1346 $
 * @date $Date: 2012-12-03 11:31:54 +0400 (Пн., 03 дек. 2012) $
 * @project Skewer
 * @package Build
 */
class CurrentAdmin extends CurrentUserPrototype {

    protected static $sLayer = 'admin';

    /**
     * Если текущий пользователь - системный администратор возвращает true
     * @static
     * @return bool
     */
    public static function isSystemMode() {

        return (isSet($_SESSION['auth'][self::$sLayer]['userData']['systemMode']) AND
            $_SESSION['auth'][self::$sLayer]['userData']['systemMode'])? true: false;

    }// func

    /**
     * Возвращает true, если текущий пользователь системы является администратором либо системным администратором
     * или false в противном случае
     * @return bool
     */
    public static function isAdminPolicy(){

        if(self::isSystemMode()) return true;

        return (isSet($_SESSION['auth'][self::$sLayer]['userData']['policyAlias']) AND
            $_SESSION['auth'][self::$sLayer]['userData']['policyAlias'] == 'admin')? true: false;
    }// func

    public static function canRead($sectionId){

        if(static::isSystemMode()) return true;

        return Auth::isReadable(static::$sLayer, $sectionId);

    }// func

    public static function getReadableSections() {

        if(static::isSystemMode()) {

            $oTree     = new Tree();
            $aSections = $oTree->getAllNodesId();
            return $aSections;

        }
        return (isSet($_SESSION['auth'][static::$sLayer]['read_access']))? $_SESSION['auth'][static::$sLayer]['read_access']: false;

    }// func

    /**
     * Проверяет права доступа к контрольной панели и если их нет - выкидывает исключение
     * @static
     * todo вынести в проверку в родительский класс модулей
     * @throws ModuleAdminErrorException
     */
    public static function testControlPanelAccess() {
        if ( !CurrentAdmin::canDo('PolicyToolModule','useControlPanel') )
            throw new ModuleAdminErrorException('Access denied');
    }

    /**
     * Проверяет можно ли использовать в дизайнерский режим
     * @static
     */
    public static function allowDesignAccess() {
        return self::isSystemMode() or CurrentAdmin::canDo('PolicyToolModule','useDesignMode');
    }

}// class
