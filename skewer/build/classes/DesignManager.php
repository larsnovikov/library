<?php
/**
 * 
 * @class DesignManager
 *
 * @author ArmiT, $Author: acat $
 * @version $Revision: 963 $
 * @date $Date: 2012-10-03 18:05:56 +0400 (Ср., 03 окт. 2012) $
 * @project Skewer
 * @package Build
 */ 
class DesignManager {


    private $oDB = null;

    /** @var string имя таблицы групп */
    private static $sTableGroups = 'css_data_groups';

    /** @var string имя таблицы параметров */
    private static $sTableParams = 'css_data_params';

    public function __construct() {
        global $odb;

        $this->oDB = &$odb;

        return true;
    }// constructor


//    private function GroupGet_maxPriority($sParent) {
//
//        $iFormId = (int)$iFormId;
//        if(!$iFormId) return false;
//
//        $query = "
//            SELECT
//                MAX(p.param_priority) AS max_priority
//            FROM `".MINC_DBPREFIX."forms_parameters` AS p
//            WHERE
//                p.form_id=$iFormId
//            LIMIT 0, 1;";
//
//        $result = mysql_query($query);
//        if(!mysql_num_rows($result)) return false;
//
//        return mysql_result($result, 0, 'max_priority');
//    }// func

    /**
     * Метод по обновлению групп и параметров в базе
     * @param array $aUpdateParams
     * @return bool
     */
    public function updateDesignSettings( $aUpdateParams = array() ){

        if ( !$aUpdateParams ) return false;

        // Проход по переданным группам
        if ( sizeof($aUpdateParams['groups']) ){

            // Проход по массиву слоев
            foreach( $aUpdateParams['groups'] as $sLayerKey=>$aLayer ){

                // Проход по массиву групп
                foreach( $aLayer as $sGroupKey=>$sGroup ){

                    $aData = array();
                    $iPoint = strrpos($sGroupKey,'.');

                    $aData['name'] = $sGroupKey;
                    $aData['parent'] = ( $iPoint!==false )? substr($sGroupKey, 0,$iPoint): '';
                    $aData['layer'] = $sLayerKey;
                    $aData['title'] = $sGroup;

                    // Сохранить в БД
                    CssGroupsMapper::saveGroup( $aData );
                }
            }
        }

        // Проход по переданным css-параметрам
        if( sizeof($aUpdateParams['params']) ){

            // Проход по слоям
            foreach($aUpdateParams['params'] as $sLayerKey=>$aLayer){

                // Проход по параметрам
                foreach( $aLayer as $sParamKey=>$aParameter ){


                    $aData = array();
                    // Выделить точку в имени параметра
                    $iPoint = strrpos($sParamKey,'.');
                    $sGroupName = ( $iPoint!==false )? substr($sParamKey, 0,$iPoint): '';

                    $aData['name'] = $sParamKey;
                    // Получить id группы по её имени
                    $aData['group'] = CssGroupsMapper::getGroupIdByName($sGroupName, $sLayerKey);
                    $aData['layer'] = $sLayerKey;
                    $aData['title'] = $aParameter['title'];
                    $aData['type'] = $aParameter['type'];
                    $aData['default_value'] = $aParameter['default'];
                    $aData['value'] = $aParameter['default'];

                    // Вставить параметр
                    CssParamsMapper::insertItem($aData);
                }
            }
        }
        return true;
    }//function updateDesignSettings()
    
    public function getGroupByName($sGroupName, $sLayer='default') {

        $sQuery = '
            SELECT
                *
            FROM
                `[tableGroups:q]`
            WHERE
                `name`=[groupName:s],
                `layer`=[layer:s]
            LIMIT 0, 1;';

        $oResult = $this->oDB->query($sQuery, array(
            'tableGroups' => self::$sTableGroups,
            'groupName' => $sGroupName,
            'layer' => $sLayer
        ));
        if(!$oResult->num_rows) return false;

        return $oResult->fetch_array(MYSQL_ASSOC);

    }// func

    /**
     * Добавление / изменение параметра
     * @param $aData
     * @return bool
     */
    public static function saveCSSParam( $aData ) {

        return CssParamsMapper::saveItem( $aData );

    }// func

    /**
     * Сохраняет значение параметра по id
     * @static
     * @param $iId - id записи
     * @param $sValue - значение для сохранения
     * @return bool
     */
    public static function saveCSSParamValue( $iId, $sValue ) {

        /*
         * Правки по задаче #5510
         * При попытке установки в режиме дизайнера пустого значения для параметра и, при условии, что параметр - типа url,
         * автоматически подставляется заглушка empty.gif.
         * Правки сделаны для того, чтобы имелась возможность сбросить картинку и чтобы при этом не поехала верстка.
         */
        $sType = DesignManager::getParamTypeById($iId);

        if ( $sType=='url' && !$sValue)
            $sValue = '/skewer_build/modules/Page/images/empty.gif';

        if( $sType == 'px' )
            $sValue = !$sValue ? ($sValue==0 ? 0 : '') : ((int)$sValue).'px';

        if( $sType == 'em' )
            $sValue = !$sValue ? ($sValue==0 ? 0 : '') : ((float)$sValue).'em';

        if( $sType == 'size' )
            $sValue = (is_numeric($sValue) && $sValue!='0') ? ($sValue.'px') : $sValue;

        return CssParamsMapper::saveItem( array(
            'id' => $iId,
            'value' => $sValue
        ) );

    }

    /**
     * Откатывает значение параметра на стандартное
     * @static
     * @param int $iId
     * @return bool
     */
    public static function revertSCCParam( $iId ){

        $aData = CssParamsMapper::getItem( (int)$iId );

        if ( !$aData )
            return false;

        $aData['value'] = $aData['default_value'];

        return (bool)CssParamsMapper::saveItem( $aData );
    }

    /**
     * Запросить все параметры группы
     * @param int $sGroupId - id группы
     * @return array|bool
     */
    public static function getParamsByGroup( $sGroupId ) {

        return CssParamsMapper::getParamListByGroupId( $sGroupId );

    }// func


    /**
     * Получение группы для параметра
     * @param int $iParamId - id параметра
     * @return mixed
     */
    public static function getGroupByParam( $iParamId ) {

        return CssParamsMapper::getGroupByParam( $iParamId );

    }


    /**
     * Возвращает иерархически структурированный массив начиная с вершины $sGroupsPath. Если указан $bVisibleOnly = 1,
     * то возвращаются вершины с visible = 1
     * @param int $iGroupId корневая категория для построения дерева. (Если пусто '' - строится все дерево)
     * @param int $bVisibleOnly Выводить только видимые вершины
     * @param bool|array $aFields - набор необходимых полей или false для полной выборки
     * @param string $sLayer
     * @return array
     */
    public function getGroupsTree($iGroupId = 0, $bVisibleOnly = 1, $aFields=false, $sLayer='default') {

        // выходная переменная
        $aOut = array();

        // набор полей
        $sFileds = (is_array($aFields) and $aFields) ? '`groups`.`'.implode('`,`groups`.`',$aFields).'`' : '`params`.*';

        // сборка запроса
        $sQuery = '
            SELECT
                [fields:q]
                ,COUNT(`params`.`id`) AS `cnt`
            FROM `[tableGroups:q]` AS `groups`
            LEFT JOIN `[tableParams:q]` AS `params` ON `params`.`group`=`groups`.`id`
            WHERE
                `groups`.`layer`=[layer:s] AND
                `groups`.`parent`=[path:s]
                [vis_condition:q]
            GROUP BY `groups`.`id`
            ORDER BY `groups`.`priority` DESC, `groups`.`title` ASC;
        ';

        // данные для запроса
        $aData = array(
            'path' => $iGroupId,
            'fields' => $sFileds,
            'tableGroups' => self::$sTableGroups,
            'tableParams' => self::$sTableParams,
            'vis_condition' => $bVisibleOnly?' AND `groups`.`visible`=1 ':'',
            'layer' => $sLayer
        );

        // выполнение запроса
        $oResult = $this->oDB->query($sQuery, $aData);

        // обойти все полученные данные
        while($aCurrentGroup = $oResult->fetch_array(MYSQL_ASSOC)) {

            // рекурсивно запросить подчиненные данные
            $aCurrentGroup['children'] = $this->getGroupsTree($aCurrentGroup['id'], $bVisibleOnly,$aFields, $sLayer);

            // дополнить выходноя массив
            $aOut[] = $aCurrentGroup;

        }

        // отдать набор
        return $aOut;

    }// func

    /**
     * Отдает список всех групп
     * @static
     * @param string $sLayer слой
     * @param array $aFields набор полей
     * @return array
     */
    public static function getGroupList( $sLayer='default', $aFields = array() ) {

        // выходная переменная
        $aOut = array();

        // набор полей
        $sFileds = (is_array($aFields) and $aFields) ? '`'.implode('`,`',$aFields).'`' : '*';

        // сборка запроса
        $sQuery = '
            SELECT
                [fields:q]
            FROM
                `[tableGroups:q]`
            WHERE
                layer=[layer:s]
            ORDER BY `name` ASC;
        ';

        // данные для запроса
        $aData = array(
            'fields' => $sFileds,
            'tableGroups' => self::$sTableGroups,
            'layer' => $sLayer
        );

        global $odb;

        // выполнение запроса
        $oResult = $odb->query($sQuery, $aData);

        // обойти все полученные данные
        while($aGroup = $oResult->fetch_array(MYSQL_ASSOC)) {

            // дополнить выходноя массив
            $aOut[] = $aGroup;

        }

        // отдать набор
        return $aOut;
    }

    /**
     * Возвращает список групп до текущей от 0-го уровня по $sCurrentGroupPath
     * @param string $sCurrentGroupPath Путь по названиям групп от корня до требуемой
     * @param string $sLayer режим отображения (по-умолчанию или pda версия)
     * @return array|bool Возвращает массив, отсортированный по группам в порядке из вложенности либо false
     */
    public function getGroupPath($sCurrentGroupPath, $sLayer='default') {

        $aOut = array();

        $aParents = explode('.', $sCurrentGroupPath);
        foreach($aParents as $sGroup){
            if(!$aCurrent = $this->getGroupByName($sGroup, $sLayer)) return false;
            $aOut[] = $aCurrent;
        }

        return $aOut;
    }// func

    /*
     * Метод для выборки css-параметров из БД
     * @return array
     */
    public function getParams() {

        // Формируем фильтр
        $aFilter = array(
            'select_fields' => array(
                'name',
                'layer',
                'value'
            ),
            'where_condition' => array(
                'group' => array(
                    'sign' => '<>',
                    'value' => 0
                )
            )
        );

        // Получаем массив параметров
        $aTempParams = CssParamsMapper::getItems($aFilter);
        $aParams = array();

        /*
         * Пересобираем массив в необходимый вид
         * array[<имя слоя>][<имя параметра>] = <значение параметра>
         */
        foreach( $aTempParams['items'] as $aItem ){

            $aParams[$aItem['layer']][$aItem['name']] = $aItem;
        }

        return $aParams;

    }// func

    public static function getParam( $sKey, $sLayer ){

        $sLayer = ( !$sLayer )? 'default': $sLayer;

        $aFilter = array(
            'where_condition' => array(
                'name' => array(
                    'sign' => 'LIKE',
                    'value' => $sKey
                ),
                'layer' => array(
                    'sign' => 'LIKE',
                    'value' => $sLayer
                )
            )
        );

        $aParam = CssParamsMapper::getItems($aFilter);

        return ( $aParam['count'] )? $aParam['items'][0]: false;
    }

    public static function clearCSSTables(){

        CssGroupsMapper::clearTable();
        CssParamsMapper::clearTable();

        return true;
    }

    public static function getParamTypeById($iParamId){

        $aParam = CssParamsMapper::getItem($iParamId);

        return (sizeof($aParam))? $aParam['type']: false;
    }

}// class
