<?php
/**
 * FT Constructor
 * Класс первичных вызовов
 */

class ft {

    /** @var bool флаг "инициализировано" */
    protected static $bInited = false;

    /**
     * Инициализация
     * @static
     */
    public static function init() {
        if ( self::$bInited )
            return;
        self::$bInited = true;
        spl_autoload_register(array(__CLASS__,'autoload'));
    }

    /**
     * Отдает имя файла класса, если найдет
     * @static
     * @param $sClassName
     * @return bool
     */
    protected static function autoload( $sClassName ) {

        // если не начинается с ft
        if ( !preg_match('/^ft(?<is_proc>[emvw])?(?<is_mod>Model)?[A-Z]{0,1}?[A-z]*?(?<is_exc>Exception)?$/',$sClassName, $aMatch) )
            return false;

        // если класс модели
        if ( isset($aMatch['is_mod']) and $aMatch['is_mod'] )
            $sFile = BUILDPATH . '/libs/ft/model/'.$sClassName.'.php';

        // если исключение
        elseif ( isset($aMatch['is_exc']) and $aMatch['is_exc'] )
            $sFile = BUILDPATH . '/libs/ft/exception/'.$sClassName.'.php';

        // если процессор поля
        elseif ( isset($aMatch['is_proc']) and $aMatch['is_proc'] ) {
            $sProcLetter = $aMatch['is_proc'];
            $sFile = BUILDPATH . '/libs/ft/proc'.$sProcLetter.'/'.$sClassName.'.php';

            if ( $bRes = file_exists($sFile) ) {
                /** @noinspection PhpIncludeInspection */
                require_once $sFile;
                // проверить задан ли класс
                if ( !($bRes = class_exists( $sClassName )) )
                    ftFnc::error( new ftException("В файле $sClassName.php не найден класс $sClassName") );
            } else {
                ftFnc::error( new ftException("Файл $sClassName.php не найден") );
            }
            return $bRes;

        }

        // иначе обчный класс
        else
            $sFile = BUILDPATH . '/libs/ft/'.$sClassName.'.php';

        if ( is_file($sFile) ) {
            /** @noinspection PhpIncludeInspection */
            require_once $sFile;
            return true;
        }

        return false;

    }

    /**
     * Отдает описание сущности
     * @static
     * @param $sEntityName
     * @param bool $bJustLoaded - флаг для рекурсивного вызова самой себя
     * @return ftModel|null
     */
    static public function getModel( $sEntityName, $bJustLoaded=false ) {

        $oDef = null;

        // если уже есть в кэше
        if ( ftCache::exists( $sEntityName ) ) {
            $oDef = ftCache::get( $sEntityName );
//            self::egInited() && ftEntityGen::nullLifeCnt( $sEntityName );
        }

//        // иначе если можно загружать и подгружены динамические таблицы
//        elseif ( !$bJustLoaded and self::egInited() ) {
//
//            // если не верное имя - исправить и попробовать запросить в кэше
//            if ( !ftEntityGen::checkName( $sEntityName ) ) {
//                $sEntityName = ftEntityGen::rightName( $sEntityName );
//                $oModel = self::getEntityDef( $sEntityName, true );
//            } // if
//
//            // если не найдено, запросить из базы
//            if ( !$oModel and !ftEntityGen::isMissedEntity( $sEntityName ) ) {
//                $def = ftEntityGen::getEntityDef( $sEntityName );
//                if ( $def ) {
//                    // если нашли - положить в кэш
//                    ftEntityGen::addToCache( $sEntityName, $def );
//                    $oModel = $def;
//                }
//                else {
//                    // нет - занести с список промахов запросов
//                    ftEntityGen::addMissEntity( $sEntityName );
//                }
//            }
//
//        }

        return $oDef;
    }

    /**
     * Отдает объект - редактор сущности
     * @static
     * @param $sEntityName
     * @param string $sEntityTitle
     * @return \ftEntity
     */
    public static function entity( $sEntityName, $sEntityTitle='' ) {
        return new ftEntity( $sEntityName, $sEntityTitle );
    }


}
