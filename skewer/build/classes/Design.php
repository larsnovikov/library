<?php

/**
 * @class Design
 * @extends skModule
 * @project Skewer
 * @package kernel
 *
 * @author Andy Mitrich, $Author: acat $
 * @version $Revision: 1437 $
 * @date 27.01.12 16:07 $
 *
 */

class Design {

    /** версия сайта - обычный сайт */
    const versionDefault = 'default';

    /** версия сайта - pda версия */
    const versionPda = 'pda';

    /** директория хранения файлов */
    const addDir = 'files/css/';

    /**
     * Лидает надор доступных слоев
     * @return array
     */
    public static function getVersionList() {
        return array(
            self::versionDefault,
            self::versionPda
        );
    }

    /**
     * @static Метод получения значения css-параметра
     * @param $sGroup Название группы для параметра
     * @param $sKey Название параметра
     * @param bool $sLayer Название слоя
     * @return string|null
     */
    public function get($sGroup, $sKey, $sLayer = false){

        /** @noinspection PhpUndefinedMethodInspection */
        $sLayer = ( !$sLayer )? skProcessor::getEnvParam('_viewMode'): $sLayer;

        $aParam = DesignManager::getParam($sGroup.'.'.$sKey, $sLayer);

        if ( $aParam['value'] ) return $aParam['value'];

        return null;

    }

    /** имя флага активности режима дизайнера */
    protected static $sGlobalFlagName = '__design_mode_active';

    /**
     * Устанавливает глобальных флаг активации дизайнерского режима
     * @static
     *
     */
    public static function setModeGlobalFlag() {
        $_SESSION[self::$sGlobalFlagName] = true;
    }

    /**
     * Отдает true, если режим дизайнера активен
     * @static
     * @return bool
     */
    public static function modeIsActive() {
        return ( isset($_SESSION[self::$sGlobalFlagName]) and $_SESSION[self::$sGlobalFlagName] );
    }

    /**
     * Отдает имя директории дизайнерского режима для клиентской части
     * @static
     * @return string
     */
    public static function getDirList() {
        $sDir = '/skewer_build/design/modules/Frame/';
        return array(
            'jsDir'  => $sDir.'js/design/',
            'cssDir' => $sDir.'css/design/',
        );
    }

    /**
     * По url вычисляет состояние - pda / обычный сайт
     * @param string $sUrl
     * @return string
     */
    public static function getVersionByUrl( $sUrl ) {

        if($aUrlParts = parse_url($sUrl))
            if(strpos($aUrlParts['host'],'m.')===0 or strpos($aUrlParts['path'],'/pda')===0  )
                return self::versionPda;

        return self::versionDefault;

    }

    /**
     * Отдает название версии сайта по псевдониму
     * @param $sType
     * @return string
     */
    public static function getVersionTitle( $sType ) {
        switch ( $sType ) {
            case self::versionDefault:
                return 'Обычная версия';
            case self::versionPda:
                return 'Мобильная версия';
            default:
                return '-не определена-';
        }
    }

    /**
     * Отдает заведомо допустимый тип отображения
     * @param $sType
     * @return string
     */
    public static function getValidVersion( $sType ) {
        if ( !in_array( $sType, self::getVersionList() ) )
            return self::versionDefault;
        else return $sType;
    }

    /**
     * Отдает системный путь к директории дополнительных файлов
     * @return string
     */
    public static function getAddCssDirPath() {
        return ROOTPATH.self::addDir;
    }

    /**
     * @param string $sViewMode
     * @return string
     */
    public static function getAddCssFilePath( $sViewMode ) {
        return sprintf('%sadd_%s.css',
            self::getAddCssDirPath(),
            self::getValidVersion($sViewMode)
        );
    }

    /**
     * Отдает набор подключенных CSS файлов
     * @return array
     */
    public static function getCSSFiles() {
        return skLinker::getCSSFiles();
    }

    /**
     * Отдает набор подключенных JS файлов
     * @return array
     */
    public static function getJSFiles() {
        return skLinker::getJSFiles();
    }

}