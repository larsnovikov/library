<?php
/**
 *
 * @class AjaxProcessor
 *
 * @author ArmiT, $Author: sapozhkov $
 * @version $Revision: 6 $
 * @date $Date: 2012-05-18 10:52:53 +0400 (Пт., 18 мая 2012) $
 * @project Skewer
 * @package build
 */
class AjaxProcessor extends skProcessorPrototype implements skProcessorInterface {

    /**
     * Запускает на выполнение корневой процесс, выводит результат работы дерева процессов
     * @static
     * @return bool|string
     */
    public function build() {

        skResponse::sendHeaders();
        /*работаем с параметрами GET POST*/
        $this->aGet = $_GET;

        $bCmdGetParam = skRequest::get(null, 'cmd', $sCmd );
        $bModuleGetParam = skRequest::get(null, 'moduleName', $sModule);


        if(!($bCmdGetParam and $bModuleGetParam)) {

            $sCmd    = (isSet($_GET['cmd']))? $_GET['cmd']: false;
            $sModule = (isSet($_GET['moduleName']))? $_GET['moduleName']: false;

        }// if

        if(!$sCmd OR !$sModule) die('BAD REQUEST');

        //skRequest::setPOSTParam('cmd', $sCmd);

        $oProcess = null;



        // @todo Проверяем авторизацию


        $oProcess = $this->addProcess(new skContext( 'out', $sModule.'Module', ctModule, array()));
        $iStatus = $this->executeProcessList();

        $oProcess->render();
        $this->sOut = array(
            'html' => $oProcess->getOut(),
            'status' => $oProcess->getStatus(),
        );

        if(count($aData = $oProcess->getData()))
            $this->sOut['data'] = $aData;

        return json_encode($this->sOut);

    }// func

}// class
