<?php
/**
 * Вспомогательный класс для автопостроителя
 *
 * @class: Ext
 * @author: sapozhkov
 */

class Ext {

    /** @var bool флаг "инициализировано" */
    protected static $bInited = false;

    /**
     * Инициализация
     * @static
     */
    public static function init() {
        if ( self::$bInited )
            return;
        self::$bInited = true;
        spl_autoload_register(array(__CLASS__,'autoload'));
    }

    /**
     * Отдает имя файла класса, если найдет
     * @static
     * @param $sClassName
     * @return bool
     */
    protected static function autoload( $sClassName ) {

        // если не начинается с ft
        if ( !preg_match('/^Ext?(?<is_field>Field)?(?<is_docked>Docked)?[A-Z]{0,1}?[A-z]*?$/',$sClassName, $aMatch) )
            return false;

        // если класс поле
        if ( isset($aMatch['is_field']) and $aMatch['is_field'] )
            $sFile = BUILDPATH . 'libs/ExtBuilder/field/'.$sClassName.'.php';

        // если класс поле
        elseif ( isset($aMatch['is_docked']) and $aMatch['is_docked'] )
            $sFile = BUILDPATH . 'libs/ExtBuilder/docked/'.$sClassName.'.php';

        // иначе обчный класс
        else
            $sFile = BUILDPATH . 'libs/ExtBuilder/'.$sClassName.'.php';

        if ( is_file($sFile) ) {
            /** @noinspection PhpIncludeInspection */
            require_once $sFile;
            return true;
        }

        return false;

    }

    /**
     * Оттдает собранный набор объектов полей
     * @static
     * @param array $aBaseParams набор элементов, собранный мапером
     * @param array $aAddParams дополнительные параметры
     * @throws Exception
     * @return array
     */
    public static function getParamFields( array $aBaseParams, array $aAddParams ) {

        // выходной массив
        $aOut = array();

        // перебор базовых полей
        foreach ( $aBaseParams as $sName => $aBaseParam ) {

            if ( isset( $aAddParams[$sName] ) ) {

                // дополнительный параметр
                $mParam = $aAddParams[$sName];

                // если объект
                if ( is_object( $mParam ) ) {

                    /** @var ExtFieldPrototype $oParam объект - дополнительный параметр */
                    $oParam = $mParam;

                    // должен быть наследником прототипа Ext поля
                    if ( !is_subclass_of($oParam,'ExtFieldPrototype') )
                        throw new Exception("Поле {$sName} не является наследником Ext поля");

                    // инициировать базовый массив
                    $oParam->setBaseDesc($aBaseParam);

                }

                // если массив
                elseif ( is_array($mParam) ) {

                    // создаем объект из массивов
                    $oParam = new ExtFieldByArray( $aBaseParam, $mParam );

                } else {
                    throw new Exception("Поле {$sName} имеет неверное расширение");
                }

            } else {

                // создаем объект из базового массива
                $oParam = new ExtFieldByArray( $aBaseParam );

            }

            $aOut[] = $oParam;

        }

        return $aOut;

    }

    /**
     * Отдает объект даботы с полями, формируя его из того, что подали на вход
     * @param array|ExtFieldPrototype $mField данные с описанием элемента
     * @throws Exception
     * @return ExtFieldPrototype
     */
    public static function makeFieldObject( $mField ){

        if ( is_object($mField) ) {

            // должен быть наследником прототипа Ext поля
            if ( !is_subclass_of($mField,'ExtFieldPrototype') )
                throw new Exception("Ошибка создания поля автопостроителя. Объект не является наследником ExtFieldPrototype");

            return $mField;

        }

        // если массив
        elseif ( is_array($mField) ) {

            // создаем объект из массивов
            return new ExtFieldByArray( $mField );

        } else {

            throw new Exception("Ошибка создания поля автопостроителя. Неверный формат даннх инициализации.");

        }

    }

}

Ext::init();
