<?php
/**
 *
 * @class Users
 *
 * @author Andrew, $Author: sokol $
 * @version $Revision: 532 $
 * @date $Date: 2012-08-01 10:51:46 +0400 (Ср., 01 авг. 2012) $
 * @project Skewer
 * @package Build
 */
class Users {

    /** Метод для получения персональной информации о пользователе
     * @param $iUserId
     * @return array|bool
     */
    public static function getUserData($iUserId){

        /**
         * Если переданный ID пользователя больше 0, то считывается информация конкретного пользователя по его ID
         * Если ID равен нулю, меньше его или не был передан - считывается инфо дефолтного пользователя
         */
        if( $iUserId )
            $aUserData =  AuthUsersMapper::getUserData($iUserId);
        else
            $aUserData =  AuthUsersMapper::getDefaultUserData();

        return $aUserData;
    }

    /**
     * Получить информацию о групповой политике по её ID
     * @param $iGroupPolicyId
     * @return array|bool
     */
    public function getGroupPolicy($iGroupPolicyId){

        if ( !$iGroupPolicyId ) return false;

        return AuthUsersMapper::getGroupPolicy($iGroupPolicyId);
    }// function getGroupPolicyId()

    /**
     * Получить информацию о персональной политике по её ID
     * @param $iGroupPolicyId
     * @return bool
     */
    public function getPersonalPolicy($iGroupPolicyId){

        return true;
    }

    /**
     * Возвращает шаблонный массив свойств пользователя
     * @static
     * @param int $policyId id политики
     * @return array|bool
     */
    public static function getPropertiesTemplate($policyId){

        return false;
    }


    /**
     * Возвращает массив свойств пользователя (Имя, Email, город и тп)
     * @static
     * @param int $iUserId id пользователя
     * @return array|bool
     */
    public static function getProperties($iUserId){

        return false;
    }


    /**
     * Возвращает значение свойства пользователя
     * @static
     * @param string $propertyName название свойства
     * @return array|bool
     */
    public static function getProperty($propertyName){

        return false;
    }

    /**
     * Созранить массив свойств пользователя
     * @static
     * @param int $userId id пользователя
     * @param array $data массив значений
     * @return bool
     */
    public static function setProperties($userId, $data){

        return false;
    }

    /**
     * Созранить массив свойств пользователя
     * @static
     * @param string $propertyName название свойства
     * @param mixed $value значение
     * @return bool
     */
    public static function setProperty($propertyName, $value){

        return false;
    }

    public static function updateLoginTime($iUserId) {

        $aData['id'] = (int)$iUserId;
        $aData['lastlogin'] = date('Y-m-d H:i:s');

        return AuthUsersMapper::saveItem($aData);

    }// func


    public static function getUserDataByName($sUserName){

        $aUserData =  AuthUsersMapper::getUserDataByName($sUserName);

        return $aUserData;
    }


} // class
