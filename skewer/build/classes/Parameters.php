<?php
/**
 * Класс для работы с параметрами разделов
 * @class Parameters
 * @project Skewer
 * @package Build
 */

class Parameters extends skMapperPrototype {

    const sTableName = 'parameters'; // имя таблицы
    const iLevelLimit = 10;    // ограничение вложенности

    // разделитель имен
    const sNameDel = ':';

    /** идентификатор записи */
    const ID = 'id';

    /** id родительского раздела */
    const PARENT = 'parent';

    /** имя группы (метка) */
    const GROUP = 'group';

    /** имя параметра */
    const NAME = 'name';

    /** название параметра */
    const TITLE = 'title';

    /** значение параметра */
    const VALUE = 'value';

    /** тип параметра */
    const ACCESS_LEVEL = 'access_level';

    /** расширенное значение параметра */
    const SHOW_VAL = 'show_val';

    /** код параметра типа wyswyg */
    const paramWyswyg = 3;
    /** код параметра типа текст */
    const paramText = 2;

    /**
     * @var array Конфигурация полей таблицы
     */
    protected static $aParametersList = array(
        self::ID => 'i:hide:Номер',
        self::PARENT => 'i:hide:Раздел',
        self::GROUP => 's:str:Метка',
        self::NAME => 's:str:Имя',
        self::TITLE => 's:str:Название',
        self::VALUE => 's:str:Значение',
        self::ACCESS_LEVEL => 'i:int:Тип',
        self::SHOW_VAL => 's:text:Текстовое поле'
    );

    // дополнительный набор параметров
    protected static function getAddParamList() {
        $aHidden = array(
            'view' => 'hide',
            'listColumns' => array(
                'hidden' => true
            )
        );
        return array(
            self::ID => $aHidden,
            self::PARENT => $aHidden,
            self::NAME => array( 'listColumns' => array( 'width' => 100 ) ),
            self::TITLE => array( 'listColumns' => array( 'width' => 200 ) ),
            self::VALUE => array( 'listColumns' => array( 'width' => 200 ) ),
        );
    }

    /**
     * Дополняет массив параметров значениями для формы
     * @static
     * @param $iSectionId
     * @return array
     */
    public static function getAddParamForForm( $iSectionId ) {

        // стандартный набор
        $aParams = self::getAddParamList();

        // доступные группы
        $aParams[self::GROUP] = array_merge(ExtForm::getDesc4SelectFromArray( Parameters::getAllGroups($iSectionId) ), array(
            'forceSelection' => false,
            'allowBlank' => false,
            'editable' => true
        ));

        // доступные типы
        $aParams[self::ACCESS_LEVEL] = ExtForm::getDesc4CustomField( 'ParamsTypeField' );

        // отдать результат
        return $aParams;

    }

    /**
     * @static возвращает набор допустимых полей для таблицы параметров
     * @return array
     */
    protected static function getParamFieldList() {
        $aOut = array();
        foreach ( self::$aParametersList as $aName => $aVal ) {
            $aOut[$aName] = $aVal[0];
        }
        return $aOut;
    }

    /**
     * Отдает набор полей для вывода списка
     * @static
     * @return array
     */
    public static function getListFields() {
        return array(
            self::ID,
            self::PARENT,
            self::GROUP,
            self::NAME,
            self::TITLE,
            self::VALUE,
        );
    }

    /**
     * Отдает набор полей для формы
     * @static
     */
    public static function getDetailFields() {
        return array_keys(self::$aParametersList);
    }

    /**
     * @static возвращает массив с описаниями типов по access_level
     * @return array
     */
    public static function getParamTypes(){
        return array(
            0 => array( 'type'=>'hide', 'val'=> self::VALUE ),
            1 => array( 'type'=>'str', 'val'=> self::VALUE ),
            self::paramText => array( 'type'=>'text', 'val'=> self::SHOW_VAL ),
            self::paramWyswyg => array( 'type'=>'wyswyg', 'val'=> self::SHOW_VAL ),
            5 => array( 'type'=>'check', 'val'=> self::VALUE ),
            6 => array( 'type'=>'file', 'val'=> self::VALUE ),
            7 => array( 'type'=>'html', 'val'=> self::SHOW_VAL ),
            8 => array( 'type'=>'show', 'val'=> self::VALUE ),
            10 => array( 'type'=>'num', 'val'=> self::VALUE ),
            15 => array( 'type'=>'date', 'val'=> self::VALUE ),
            16 => array( 'type'=>'time', 'val'=> self::VALUE ),
            20 => array( 'type'=>'inherit', 'val'=> self::VALUE ),
        );
    }

    /**
     * @func __construct - констурктор
     * @return \Parameters
     */
    public function __construct() {
    }

    /**
     * @func error - внутренняя функция регистрации ошибк
     * @param $text
     * @return string
     */
    protected static function error( $text ) {
        echo '<b>Error:</b> '.$text.'<br>';
        return $text;
    }

    /**
     * Запросить все параметры раздела
     * @param $iSectionId - идентификатор раздела
     * @param array $aFilter - набор фильтров
     * @param bool $bRec - рекурсивынй сбор параметров
     * @return array
     */
    public static function getAll( $iSectionId, $aFilter=array(), $bRec=true ) {

        // запрос дерева параметров
        $aParams = self::getAllAsList( $iSectionId, $aFilter, $bRec );

        // выходная параменная
        $aOut = array();

        // сборка выходного массива
        foreach ( $aParams as $aRow ) {
            $aOut[$aRow[self::GROUP]][$aRow[self::NAME]] = $aRow;
        }

        return $aOut;

    }// func

    /**
     * Запросить все параметры раздела
     * @param $iSectionId - идентификатор раздела
     * @param array $aFilter - набор фильтров
     * @param bool $bRec
     * @internal param bool $bRec - рекурсивынй сбор параметров
     * @return array
     */
    public static function getAllSimple( $iSectionId, $aFilter=array(), $bRec = true ) {

        // запрос дерева параметров
        $aParams = self::getAllAsList( $iSectionId, $aFilter, $bRec );

        // выходная параменная
        $aOut = array();

        foreach ( $aParams as $aRow )
            $aOut[$aRow[self::GROUP]][$aRow[self::NAME]] =($aRow[self::GROUP] != '.')? $aRow[self::VALUE]: $aRow;

        return $aOut;

    }// func

    /**
     * Запрос полного рекурсивного нобора в виде списка, а не дерева
     * @param $iSectionId - идентификатор раздела
     * @param array $aFilter - набор фильтров
     * @param bool $bRec
     * @return array
     */
    public static function getAllAsList( $iSectionId, $aFilter=array(), $bRec=true ) {

        // выходная параменная
        $aFilter[self::PARENT] = $iSectionId;
        $aFilter['rec'] = $bRec;
        $aParams = self::getByFilter( $aFilter );

        return $aParams;

    }

    /**
     * Отдает набор параметров по имени и группе
     * @static
     * @param $sName - имя параметра
     * @param $sGroup - имя группы
     * @return array
     */
    public static function getListByName( $sName, $sGroup ) {

        // составление фильтра
        $aFilter = array(
            self::GROUP => $sGroup,
            self::NAME => $sName,
        );

        // вывод параметров
        return self::getByFilter($aFilter);

    }

    /**
     * Запрос параменной по имени и группе
     * @param $iSectionId - id родительского раздела
     * @param $sGroup - имя группы
     * @param $sName - имя параметра
     * @param bool $bRec - флаг рекурсивного спуска
     * @return array
     */
    public static function getByName( $iSectionId, $sGroup, $sName, $bRec=true ) {

        // составление фильтра
        $aFilter = array(
            self::PARENT => $iSectionId,
            self::GROUP => $sGroup,
            self::NAME => $sName,
            'rec' => $bRec
        );

        // выборка
        $aParams = self::getByFilter($aFilter);

        // вывод параметра
        return (empty($aParams) ? array() : $aParams[0]);

    }

    /**
     * Запросить параметры по фильтру
     *  Допустимые параметры
     * @param array $aFilter - массив параметров
     *      parent - id раздела
     *      group - str группа параметра
     *      name - str имя параметра
     *      fields - str/array набор полей
     *      sort - str сортировка
     *      rec - рекурсивыный вызов
     *      simple - вывод параметров в виде название => значение
     *      with_access_level - true - только с alias_level!=0
     *                          >0 - только с alias_level>0
     * @return array
     */
    public static function getByFilter( $aFilter ) {

        // выходная параменная
        $aParams = array();

        // набор директив для запроса
        $aWhereConditions = array();

        // набор значений
        $aWhereValues = array();

        // флаг целевой фильтрации (по имени)
        $sFilterByName = '';

        // id раздела поиска
        $iSectionId = 0;

        // фильтр по родителю
        if ( isset($aFilter[self::PARENT]) ){
            $aWhereConditions[] = '`parent`=[parent:i]';
            $aWhereValues[self::PARENT] = $aFilter[self::PARENT];
            $iSectionId = (int)$aFilter[self::PARENT];
        }

        // группа параметров
        if ( isset($aFilter[self::GROUP]) ){
            $aWhereConditions[] = '`group`=[group:s]';
            $aWhereValues[self::GROUP] = $aFilter[self::GROUP];
            $sFilterByName = $aFilter[self::GROUP];
        }

        // имя параметра
        if ( isset($aFilter[self::NAME]) ){
            $aWhereConditions[] = '`name`=[name:s]';
            $aWhereValues[self::NAME] = $aFilter[self::NAME];
            $sFilterByName = ($sFilterByName?$sFilterByName.self::sNameDel:'').$aFilter[self::NAME];
        }

        // значение параметра
        if ( isset($aFilter[self::VALUE]) ){
            $aWhereConditions[] = '`value`=[value:s]';
            $aWhereValues[self::VALUE] = $aFilter[self::VALUE];
            $sFilterByName = ($sFilterByName?$sFilterByName.self::sNameDel:'').$aFilter[self::VALUE];
        }

        // набор полей для выборки
        if ( isset($aFilter['fields']) and $aFilter['fields'] ) {

            // определение типа пришедшей строки
            if ( is_array($aFilter['fields']) )
                $aFields = $aFilter['fields'];
            else
                $aFields = explode(',',str_replace(' ','',$aFilter['fields']));

            // добавление обязательных полей
            $aFields = array_merge($aFields,array( self::GROUP,
                self::NAME
            ));

            // объединение в строку
            $sFields = '`'.implode('`,`',$aFields).'` ';

        } else {

            // сборка строки с именами полей для выборки
            $sFields = '`'.implode('`,`',array( self::ID,
                self::PARENT,
                self::GROUP,
                self::NAME,
                self::VALUE,
                self::TITLE,
                self::ACCESS_LEVEL,
                self::SHOW_VAL
            )).'` ';

        }

        // флаг рекурсивного обхода
        $bRec = isset($aFilter['rec']) && $aFilter['rec'];

        // Флаг вывода упрощенного списка параметров
        $bSimple = isset($aFilter['simple']) && $aFilter['simple'];

        // сортировка
        if ( isset($aFilter['sort']) and $aFilter['sort'] ) {
            $aOrderConditions = explode(',',str_replace(' ','',$aFilter['sort']));
        } else {
            $aOrderConditions = array( self::GROUP,
                self::NAME
            );
        }

        // только с alias_level!=0
        if ( isset($aFilter['with_access_level']) and $aFilter['with_access_level'] ) {
            if ( $aFilter['with_access_level']==='>0' )
                $aWhereConditions[] = '`access_level`>0';
            else
                $aWhereConditions[] = '`access_level`!=0';
        }

        // сборка запроса
        $sQuery =
            'SELECT '.$sFields.
            'FROM `'.self::sTableName.'` '.
            ($aWhereConditions?'WHERE '.implode(' AND ',$aWhereConditions).' ':'').
            ($aOrderConditions?'ORDER BY `'.implode('`,`',$aOrderConditions).'` ':'')
        ;

        // выполенение запроса
        global $odb;
        $oResult = $odb->query( $sQuery, $aWhereValues);

        // проверка правильности выполнения запроса
        if ( $odb->errno ) {
            self::error( $odb->error );
            return $aParams;
        }

        // сборка выходного массива
        while ($aRow = $oResult->fetch_array(MYSQLI_ASSOC)) {
            $sPrefix = $bRec ? '' : (isset($aRow[self::PARENT])?$aRow[self::PARENT]:$iSectionId).self::sNameDel;
            $aParams[$sPrefix.$aRow[self::GROUP].self::sNameDel.$aRow[self::NAME]] = ($bSimple)? $aRow[self::VALUE]: $aRow;
        }

        // рекурсивная выборка, если нужна
        $sTemplateId = isset($aParams['.'.self::sNameDel.'template']) ? (int)$aParams['.'.self::sNameDel.'template'][self::VALUE] : 0;

        // если найдены параметры
        if ( $aParams ) {
            if ( $sFilterByName )
                return array_values($aParams);
        } else {
            // при поиске по имени шаблон можно запросить отдельно
            if ( !$sTemplateId and $iSectionId and $sFilterByName and $bRec and $sFilterByName !== '.'.self::sNameDel.'template' ) {
                $sTemplateId = (int)self::getValByName( $iSectionId, '.', 'template', false );
            }
        }

        if ( $bRec and $sTemplateId ) {

            // защита от зацикленного рекурсивного вызова
            $aParentFilter = $aFilter;
            $aParentFilter[self::PARENT] = $sTemplateId;
            $aParentFilter['rec_limit'] = (isset($aFilter['rec_limit']) ? $aParentFilter['rec_limit'] : self::iLevelLimit)-1;
            if ( $aParentFilter['rec_limit']<=0 )
                return array_values($aParams);

            // рекурсивно запросить родительские параметры
            $aParentParams = self::getByFilter($aParentFilter);
            
            // перебор параметров родительской метки
            foreach ( $aParentParams as $aParentParam ) {

                // сборка составного имени параметра
                $sFullParamName = $aParentParam[self::GROUP].self::sNameDel.$aParentParam[self::NAME];

                // если нет такого параметра
                if ( !isset($aParams[$sFullParamName]) ) {

                    // добавить параметр
                    $aParams[$sFullParamName] = $aParentParam;

                }

            }

        }

        // вывод результатов
        return array_values($aParams);
        
    }

    /**
     * Запрос значения параметра по имени
     * @param $iSectionId - id родительского раздела
     * @param $sGroup - имя группы
     * @param $sName - имя параметра
     * @param bool $bRec - флаг рекурсивного спуска
     * @return string
     */
    public static function getValByName($iSectionId, $sGroup, $sName, $bRec=false) {

        $aParams = self::getByNameAndFilter( $iSectionId, $sGroup, $sName, $bRec, array( 'fields' => self::VALUE ) );

        // вывод параметра
        $aParam = (empty($aParams) ? array() : $aParams[0]);
        return ($aParam and isset($aParam[self::VALUE])) ? $aParam[self::VALUE] : '';

    }

    /**
     * Запрос значения параметра по имени
     * @param $iSectionId - id родительского раздела
     * @param $sGroup - имя группы
     * @param $sName - имя параметра
     * @param bool $bRec - флаг рекурсивного спуска
     * @return string
     */
    public static function getTextByName($iSectionId, $sGroup, $sName, $bRec=false) {

        $aParams = self::getByNameAndFilter( $iSectionId, $sGroup, $sName, $bRec, array( 'fields' => self::SHOW_VAL ) );

        // вывод параметра
        $aParam = (empty($aParams) ? array() : $aParams[0]);
        return ($aParam and isset($aParam[self::SHOW_VAL])) ? $aParam[self::SHOW_VAL] : '';

    }

    /**
     * Запрос параметра по имени с шаблоном фильтра
     * @param int $iSectionId - id родительского раздела
     * @param string $sGroup - имя группы
     * @param string $sName - имя параметра
     * @param bool $bRec - флаг рекурсивного спуска
     * @param string|array $mFields шаблон фильтра
     * @return array
     */
    protected static function getByNameAndFilter($iSectionId, $sGroup, $sName, $bRec=true, $mFields='') {

        // составление фильтра
        $aFilter = array(
            'fields' => self::toArray($mFields),
            self::PARENT => $iSectionId,
            self::GROUP => $sGroup,
            self::NAME => $sName,
            'rec' => $bRec
        );

        // выборка
        return self::getByFilter($aFilter);
        
    }
    
    /**
     * привдение к массиву
     * @param string|array $mIn - входной параметр
     * @return array
     */
    protected static function toArray( $mIn ) {
      
        // проверка на пустоту
        if ( !$mIn )
            return array();
        
        // если массив
        if ( is_array($mIn) ) {
            return $mIn;
        } elseif ( is_string($mIn) ) {
            
            // если строка - разобрать
            $aOut = array();
            $aRows = explode(',', $mIn);
            foreach ( $aRows as $sRow )
                $aOut[] = trim($sRow);
            return $aOut;
            
        } else {
            // иначе просто вернуть пустой
            return array();
        }
      
    }

    /**
     * Запрос параметра по id
     * @param int|array $mId - id параметра
     * @param string|array $mFields - набор полей
     * @return array
     */
    public static function getById( $mId, $mFields='' ) {

        $aId = self::getIdList($mId);
        if ( !$aId ) return array();

        // набор полей
        $aFields = self::toArray($mFields);
        $sFields = $aFields ? '`'.implode('`,`',$aFields).'` ' : '*';

        // выполенение запроса
        global $odb;
        $oResult = $odb->query( 'SELECT '.$sFields.' FROM `'.self::sTableName.'` WHERE `id` IN ('.implode(',',$aId).')' );

        // проверка правильности выполнения запроса
        if ( $odb->errno ) {
            self::error( $odb->error );
            return array();
        }

        // выходная переменная
        $aOut = array();

        if ( is_numeric($mId) ) {
            // одна строка
            if ( $aRow = $oResult->fetch_array(MYSQLI_ASSOC) ) {
                $aOut = $aRow;
            }
        } else {
            // набор строк
            while ( $aRow = $oResult->fetch_array(MYSQLI_ASSOC) ) {
                $aOut[] = $aRow;
            }
        }

        return $aOut;

    }

    /**
     * Сохраняет параметр по входному массиву
     * @param array $aRow
     * @return int
     */
    public static function saveParameter( $aRow ) {

        // идентификатор
        if ( isset($aRow[self::ID]) ) {
            $iId =  (int)$aRow[self::ID];
            unset( $aRow[self::ID] );
        } else $iId = 0;

        // выбрать переменные для сохранения
        $aAllowFields = self::getParamFieldList();
        $aSaveFields = array();
        $aSaveValues = array();
        foreach ( $aRow as $sName => $sVal ) {
            if ( isset($aAllowFields[$sName]) ) {
                $aSaveFields[] = sprintf('`%s`=[%1$s:%s]',$sName,$aAllowFields[$sName]);
                $aSaveValues[$sName] = $sVal;
            }
        }

        // если пусто - выйти
        if (empty($aSaveFields) )
            return 0;

        // составление запроса
        $sQuery =
            ($iId ? 'UPDATE `' : 'INSERT `').self::sTableName.'` '.
            'SET '.implode(',',$aSaveFields).
            ($iId ? ' WHERE `id`=[id:i]' : '');
        global $odb;
        $iResult = $odb->query( $sQuery, $aSaveValues+array( self::ID =>$iId) );

        // проверка правильности выполнения запроса
        if ( $odb->errno ) {
            self::error( $odb->error );
            return 0;
        }

        // вернуть id сохраненной записи или 0
        return $iResult ? ($iId ? $iId : $odb->insert_id) : 0;

    }

    /**
     * Удаление параметров по id (набору id)
     * @param int|array $mId
     * @param bool|int|array $mParent
     * @return int число затронутых строк
     */
    public static function delById( $mId, $mParent = false ) {

        // составление набора id
        $aId = self::getIdList($mId);
        if ( !$aId ) return 0;

        // составление запроса
        $sQuery =
            'DELETE FROM `'.self::sTableName.'` '.
            'WHERE `id` IN ('.implode(',',$aId).')'
        ;

        // если задан родитель
        if ( $mParent !== false ) {
            $aParent = self::getIdList($mParent);
            $sQuery .= ' AND `parent` IN ('.implode(',',$aParent).')';
        }

        // выполнение запроса
        global $odb;
        $odb->query( $sQuery );

        // проверка правильности выполнения запроса
        if ( $odb->errno ) {
            self::error( $odb->error );
            return 0;
        }

        // вернуть id сохраненной записи или 0
        return $odb->affected_rows;

    }

    /**
     * Удаление параметров по набору родительских id
     * @param $aIdList
     * @return int
     */
    public static function delByParentIdList( $aIdList ) {

        // составление набора id
        $aIdList = self::getIdList($aIdList);
        if ( empty($aIdList) ) return 0;

        // составление запроса
        $sQuery =
            'DELETE FROM `'.self::sTableName.'` '.
            'WHERE `parent` IN ('.implode(',',$aIdList).')'
        ;

        // выполнение запроса
        global $odb;
        $odb->query( $sQuery );

        // проверка правильности выполнения запроса
        if ( $odb->errno ) {
            self::error( $odb->error );
            return 0;
        }

        // количаство затронутых строк
        $iResult = $odb->affected_rows;
        return $iResult;

    }

    /**
     * Удаляет параметр $sName группы $sGroup в разделе $iParent
     * @static
     * @param int $iParent Id родительского раздела
     * @param string $sName Имя удаляемого параметра
     * @param string $sGroup Имя группы в которой находится удаляемый параметр
     * @return bool Возвращает true если удаление прошло успешно либо false в противном случае
     * @throws Exception
     */
    public static function removeByName( $iParent, $sName, $sGroup = '.' ) {
        global $odb;

        try {

            if(!(int)$iParent OR empty($sName) OR empty($sGroup))
            throw new Exception();


            $sQuery = '
                DELETE FROM
                    `[table:q]`
                WHERE
                    `parent`=[parent:s] AND
                    `group`=[group:s] AND
                    `name` =[name:s];';

            $aData['table']  = self::sTableName;
            $aData[self::PARENT] = $iParent;
            $aData[self::NAME]   = $sName;
            $aData[self::GROUP]  = $sGroup;

            $odb->query( $sQuery, $aData );

        } catch(Exception $e) {

            return false;
        }

        return true;

    }// func

    /**
     * Отдает массив id из массива объектов/массивов и просто int
     * @param mixed $mIdList
     * @return array
     */
    public static function getIdList( $mIdList ) {

        $aIdList = array();

        if ( is_numeric($mIdList) ) {

            // если просто число
            $aIdList[] = (int)$mIdList;

        } elseif ( is_array($mIdList) ) {

            if ( isset($mIdList[self::ID]) ) {
                $aIdList[] = (int)$mIdList[self::ID];
                return $aIdList;
            }

            // если массив значений
            foreach( $mIdList as $mRow ) {
                if ( is_numeric($mRow) ) {
                    $aIdList[] = (int)$mRow;
                } elseif ( is_object($mRow) ) {
                    // внутри объект
                    if ( isset($mRow->{self::ID}) )
                        $aIdList[] = (int)$mRow->{self::ID};
                } elseif ( is_array($mRow) ) {
                    // внутри массив
                    if ( isset( $mRow[self::ID] ) )
                        $aIdList[] = (int)$mRow[self::ID];
                }
            }

        } elseif (is_object($mIdList)) {

            // если одно значение
            if ( isset($mIdList->{self::ID}) )
                $aIdList[] = (int)$mIdList->{self::ID};
            
        }

        return $aIdList;

    }

    /**
     * Отдает набор родителей данного раздела
     * @static
     * @param int $iSectionId
     * @return array
     */
    public static function getParentList( $iSectionId ) {

        $aOut = array();

        $iCnt = 0;
        while ( $iSectionId and ++$iCnt<10 ) {

            // найти параметр
            $aParam = self::getByName( $iSectionId, '.', 'template' );
            if ( !$aParam )
                break;

            // определить id родителя
            $iSectionId = (int)$aParam[self::VALUE];
            if ( !$iSectionId )
                break;

            // добавить в список
            $aOut[] = $iSectionId;

        }

        return $aOut;

    }

    /**
     * Извлекает длинну, класс и шаблон из поля value для wysiwyg редактора
     * @static
     * @param string $sValue
     * @return array
     */
    public static function extractWysiwygParams( $sValue ) {

        // формат значения - height:cssClass:parserInclude
        //
        // 1. одно значение интерпретируется в зависимости от типа как height или как cssClass
        // 2. несколько значений интерпретируется в порядке: height, cssClass, parserInclude
        //    если необходимо пропустить значения необходимо сохранить необходимое количество
        //    разделителей (например ::content.twig - пропускает значения height и cssClass)


        $aParamKeys = array('height', 'cssClass'/*, 'parserInclude'*/);

        $aRow = array();
        foreach($aParamKeys as $sKey) $aRow[$sKey]='';

        if(strpos($sValue, ':')){

            $aData = explode(':', $sValue);
            while(count($aData) and count($aParamKeys))
                $aRow[array_shift($aParamKeys)] = trim(array_shift ($aData));

            // задать высоту
            if($aRow['height']) $aRow['height'] = (int)$aRow['height'];

        }

        else {

            $iIsNumeric = (int)$sValue;
            // если в значении число - взять за высоту
            if( $iIsNumeric ) $aRow['height'] = $iIsNumeric;
            elseif ( $sValue )
                $aRow['cssClass'] = trim($sValue);

        }

        return $aRow;

    }

    /**
     * Отдает все доступные для раздела группы
     * @static
     */
    private static function getAllGroups( $iSectionId ) {
        $aParams = self::getAllAsList( $iSectionId, array('fields'=>array(
            self::NAME,
            self::GROUP,
            self::VALUE
        )) );
        $aGroupList = array();
        foreach ( $aParams as $aItems ) {
            $sGroup = $aItems[self::GROUP];
            if ( !in_array($sGroup,$aGroupList) )
                $aGroupList[] = $sGroup;
        }
        sort($aGroupList);
        return array_combine($aGroupList,$aGroupList);
    }

}
