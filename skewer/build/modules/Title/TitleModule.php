<?php

/**
 * @class TitleModule
 * @extends skModule
 * @project Skewer
 * @package kernel
 *
 * @author andymitrich, $Author: sapozhkov $
 * @version $Revision: 6 $
 * @date 09.12.11 9:30 $
 *
 */

class TitleModule extends skModule{

    var $iSectionId;
    var $hideTitle;
    var $altTitle = '';

    public function init(){

        $this->iSectionId = $this->getEnvParam('sectionId');
        $this->setParser(parserTwig);
    }

    public function execute(){

        
        if(!empty($this->altTitle)){
            $this->setData('title', $this->altTitle);
        }
        else {
            $oTree = new Tree();
            $aTitle = $oTree->getById($this->iSectionId);
            $this->setData('title', $aTitle['title']);
        }
        
        $this->setData('hideTitle', $this->hideTitle);
        $this->setTemplate('view.twig');

        return psComplete;
    }

}//class
?>