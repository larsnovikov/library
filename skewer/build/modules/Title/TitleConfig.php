<?php

/* main */
$aConfig['name']     = 'Title';
$aConfig['title']    = 'Модуль заголовка';
$aConfig['version']  = '1.000a';
$aConfig['description']  = 'Модуль заголовка';
$aConfig['revision'] = '0001';
$aConfig['layer']     = 'Page';

/* Module css inclusions */

$aConfig['css'] = array();
$aConfig['js'] = array();

return $aConfig;