<?php
class MenuModule extends skModule {

    var $templateFile = 'leftMenu.twig';
    var $parentSection = 3;
    var $customSections = '';
    public $openAll = '';
    public $showOnPDAPage;
    public $hiddenItemsOnPDA;

	public function init() {

        $this->setParser(parserTwig);

        if ( !empty($this->hiddenItemsOnPDA) ){

            $this->hiddenItemsOnPDA = explode(',', $this->hiddenItemsOnPDA);
            $this->hiddenItemsOnPDA = array_diff($this->hiddenItemsOnPDA, array(''));
        }
    }// func
	
	public function execute() {

        $to = $this->getEnvParam('sectionId');

//        if ( $this->getViewMode()=='pda' ){
//
//            if ( !empty($this->showOnPDAPage) ){
//                $aSections = explode(',', $this->showOnPDAPage);
//                $aSections = array_diff($aSections, array(''));
//                if ( !in_array($to, $aSections ) ) return psRendered;
//            }
//        }

        $iMainSection = skConfig::get('section.main');

        $from = $this->parentSection;
        $tree = new Tree();

        $mode = 'normal';
        if($this->openAll) $mode = 'openAll';
        if($this->customSections) $mode = 'custom';

        // HACK - для главного меню версии PDA
        if( ($this->getViewMode() == 'pda') && $this->openAll && ($to != $iMainSection) ) //
            $from = $to;

        switch($mode){
            case 'normal':
                $parents = $tree->getSectionParents($to, 3);
                // Проверяем находится ли Текущий раздел в дереве меню
                if(in_array($from, $parents))
                    $items = $this->getMenuTree($from, $to, $this->getSubsections($to));
                else
                    $items = $this->getMenuFlat($from);
                break;

            case 'openAll':

                $items = $this->getMenuOpened($from, $to, 2);
                break;

            case 'custom':
                $idList = explode(',',$this->customSections);

                foreach($idList as &$sectionId) $sectionId = (int)$sectionId;
                $items = $this->getCustomSections($idList, $to);
                break;

        } // switch

        $this->setData('items', $items);
        if ( $iMainSection != $to ) $this->setData('hideMenu', 1);
        $this->setTemplate($this->templateFile);

        return psComplete;
    }// func

    public function getSubsections($from) {
        $tree = new Tree();

        $items = $tree->getSubItems($from);
        foreach($items as &$item){
            $item['href'] = ($item['link']) ? $item['link'] : '['.$item['id'].']';
            $item['selected'] = false;
        } // foreach
        if($items) return $items;
        return null;
    } // function
    
    public function getMenuTree($from, $to, $tail=null) {

        $tree = new Tree();
        if($parent = $tree->getParentId($to) and $from != $to){
            $items = $tree->getSubItems($parent);

            foreach($items as $iKey=>&$item){

                $item['href'] = ($item['link']) ? $item['link'] : '['.$item['id'].']';
                if($item['id']==$to){
                    $item['selected'] = true;
                    if($tail) $item['items'] = $tail;
                } // if
                else {
                    $item['selected'] = false;
                } // else

                if ( $this->getViewMode()=='pda' && sizeof($this->hiddenItemsOnPDA) )
                    if ( in_array($iKey, $this->hiddenItemsOnPDA) ) unset( $items[$iKey] );
            } // foreach
            $items = $this->getMenuTree($from, $parent, $items);

            return $items;
        } // if parent
        else {
            return $tail;
        } // else
    } // function
    

    public function getMenuFlat($from) {

        $tree = new Tree();
        $items = $tree->getSubItems($from);

        foreach($items as $iKey=>&$item){

            $item['href'] = ($item['link']) ? $item['link'] : '['.$item['id'].']';
            $item['selected'] = false;

            if ( $this->getViewMode()=='pda' && sizeof($this->hiddenItemsOnPDA) )
                if ( in_array($iKey, $this->hiddenItemsOnPDA) ) unset( $items[$iKey] );
            
        } // foreach

        return $items;
        
    } // function


    public function getCustomSections($idList, $currentSection){

        $tree = new Tree();
        $items = $tree->getByIdList( $idList , true);
        foreach($items as $iKey=>&$item){

            $item['href'] = ($item['link']) ? $item['link'] : '['.$item['id'].']';
            $item['selected'] = ($item['id']==$currentSection);

            if ( $this->getViewMode()=='pda' && sizeof($this->hiddenItemsOnPDA) )
                if ( in_array($iKey, $this->hiddenItemsOnPDA) ) unset( $items[$iKey] );
        } // foreach

        return $items;
    }

    public function getMenuOpened($from, $to, $numOfLevels) {

        if($numOfLevels < 1) return null;
        $tree = new Tree();
        $selectedNodes = $tree->getSectionParents($to, 3);
        $selectedNodes[] = $to;

        $parents = array($from);

        $tree = new Tree();
        $itemsLayers = array();

        // собираем вершины по слоям
        for ($level = 1; $level <= $numOfLevels; $level++){
            if(!count($parents)) break;

            $itemsLayers[$level] = $tree->getSubItems($parents);
            $parents = array();
            foreach($itemsLayers[$level] as $item) $parents[] = $item['id'];
        } // for

        // клеим слои в общее дерево
        for ($level = $numOfLevels; $level>1; $level--){

            if(isset($itemsLayers[$level]))
            foreach($itemsLayers[$level] as $item){

                $item['href'] = ($item['link']) ? $item['link'] : '['.$item['id'].']';
                $item['selected'] = false;
                if(in_array($item['id'], $selectedNodes)) $item['selected'] = true;

                $itemsLayers[$level - 1][$item['parent']]['items'][$item['id']] = $item;

                if ( $this->getViewMode()=='pda' && sizeof($this->hiddenItemsOnPDA) )
                    if ( in_array($item['id'], $this->hiddenItemsOnPDA) )
                        unset( $itemsLayers[$level - 1][$item['parent']]['items'][$item['id']] );

            } // foreach

        } // for

        foreach($itemsLayers[1] as $itemId=>$item){

            $itemsLayers[1][$itemId]['href'] = ($item['link']) ? $item['link'] : '['.$item['id'].']';
            $itemsLayers[1][$itemId]['selected'] = false;
            if(in_array($itemId, $selectedNodes)) $itemsLayers[1][$itemId]['selected'] = true;

            if ( $this->getViewMode()=='pda' && sizeof($this->hiddenItemsOnPDA) )
                if ( in_array($itemId, $this->hiddenItemsOnPDA) )
                    unset( $itemsLayers[1][$itemId] );

            if( !$item['visible'] ) // не выводим скрытые разделы
                unset( $itemsLayers[1][$itemId] );
        } // foreach

        $result = $itemsLayers[1];
        unset($itemsLayers);

        return $result;

    } // function


    public function shutdown() {
        
    }// func
}// class
