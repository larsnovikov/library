$(function(){

    $("ul.js-pda-menu").hide();

    $("#js-pda-button").click(function(){

        var oUl = $(this).parent().find("ul.js-pda-menu");
        if ( oUl.is(":visible") ){
            oUl.slideUp();
            $(this).find('img').attr('src', '/skewer_build/modules/Page/images/btn_showmenu.gif');
        }
        else {
            oUl.slideDown();
            $(this).find('img').attr('src', '/skewer_build/modules/Page/images/btn_hidemenu.gif');
        }
    });
});