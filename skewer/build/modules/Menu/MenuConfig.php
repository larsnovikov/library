<?php

/* main */
$aConfig['name']     = 'Menu';
$aConfig['title']    = 'Меню';
$aConfig['version']  = '1.000a';
$aConfig['description']  = 'Модуль меню';
$aConfig['revision'] = '0001';
$aConfig['layer']     = 'Page';

/* Module css inclusions */
$aConfig['css'] = array();

$aConfig['js'] = array(
    '/skewer_build/modules/Menu/js/Menu.js'
);

return $aConfig;