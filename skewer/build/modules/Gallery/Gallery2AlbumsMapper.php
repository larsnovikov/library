<?php
/**
 * Модель для альбомов галереи
 * @class Gallery2AlbumsMapper
 *
 * @author ArmiT, $Author: acat $
 * @version $Revision: 1343 $
 * @date $Date: 2012-11-30 17:14:11 +0400 (Пт., 30 нояб. 2012) $
 * @project Skewer
 * @package Modules
 */
class Gallery2AlbumsMapper extends skMapperPrototype {

    /**
     * Первичный ключ
     * @var string
     */
    protected static $sKeyFieldName = 'album_id';

    /**
     * Таблица модели
     * @var string
     */
    protected  static $sCurrentTable = 'photogallery_albums';

    /**
     * Модель данных
     * @var array
     */
    protected static $aParametersList = array(

        'owner'         => 's:hide:Модуль-владелец фотоальбома',
        'title'         => 's:str:Название альбома:Новый альбом',
        'alias'         => 's:str:Псевдоним',
        'visible'       => 'i:check:Выводить на сайте:1',
        'album_id'      => 'i:hide:id альбома',
        'priority'      => 'i:hide:Индекс сортировки',
        'profile_id'    => 'i:select:Профиль обработки изображений',
        'section_id'    => 'i:hide:Id раздела',
        'album_image'   => 's:hide:Миниатюра альбома',
        'description'   => 's:text:Описание',
        'creation_date' => 's:str:Дата создания',

    );

    /**
     * Отправка дополнительных настроек к полям
     * @static
     * @return array
     */
    public static function getAddParamList() {

        return array(
            'profile_id'    => ExtForm::getDesc4SelectFromArray(Gallery2ProfilesApi::getActiveProfilesShortList()),
            'creation_date' => array('disabled' => true),
        );
    }// func

    /**
     * @param $iSectionId
     * @return array
     */
    public static function getAllBySection($iSectionId) {
        global $odb;

        $sQuery = '
            SELECT *
             , ( SELECT count(image_id) FROM `[img_table_nage:q]` AS imgs WHERE imgs.album_id=alb.album_id ) as album_count
             , ( SELECT thumbnail FROM `[img_table_nage:q]` AS imgs WHERE imgs.album_id=alb.album_id ORDER BY `priority` DESC LIMIT 1 ) as album_img
            FROM `[table_name:q]` AS alb
            WHERE `section_id`=[section:i]
            ORDER BY priority DESC;
        ';

        $rResult = $odb->query($sQuery, array(
            'table_name' => self::$sCurrentTable,
            'img_table_nage' => 'photogallery_images',
            'section' => (int)$iSectionId
        ));

        $aItems = array();

        while( $aRow = $rResult->fetch_assoc() ){

            $aItems[$aRow['album_id']] = $aRow;
        }

        return $aItems;
    }

    /**
     * Возвращает максимальный индекс сортировки в разделе
     * @static
     * @param $iSectionId
     * @return int
     */
    public static function getMaxPriorityBySection($iSectionId) {
        global $odb;

        $sQuery = '
            SELECT
              MAX(priority) AS `count`
            FROM `[table_name:q]`
            WHERE `section_id`=[section:i];
        ';

        $bResult = $odb->query($sQuery, array(
            'table_name' => self::$sCurrentTable,
            'section' => (int)$iSectionId
        ));

        $bResult = $bResult->fetch_assoc();
        return $bResult['count'];

    }// func

    /**
     * Выполняет сдвиг положений для подчиненных альбомов
     * @param $iSectionId
     * @param $iStartPos
     * @param $iEndPos
     * @param string $sSign
     * @return int
     */
    public static function shiftAlbumPosition($iSectionId,$iStartPos,$iEndPos,$sSign = '+'){

        global $odb;

        $sQuery = "
            UPDATE `[table:q]`
            SET `priority`=`priority` [sign:q] 1
            WHERE
                `section_id` = [section:i] AND
                `priority` > [start:i] AND
                `priority` < [end:i]";

        $odb->query($sQuery, array(
            'table' => self::$sCurrentTable,
            'section' => $iSectionId,
            'sign' => $sSign,
            'start' => $iStartPos,
            'end' => $iEndPos
        ));

        return $odb->affected_rows;
    }

    /**
     * меняет значение для поля сортировки альбома на заданное
     * @param $iAlbumId
     * @param $iPos
     * @return int
     */
    public static function changeAlbumPosition($iAlbumId, $iPos){

        global $odb;

        $sQuery = "
            UPDATE `[table:q]`
            SET `priority`=[pos:i]
            WHERE
                `album_id` = [album:i]";

        $odb->query($sQuery, array(
            'table' => self::$sCurrentTable,
            'album' => $iAlbumId,
            'pos' => $iPos
        ));

        return $odb->affected_rows;
    }

    /**
     * изменение флага видимости на сайте
     * @param $iAlbumId
     * @return int
     */
    public static function toggleActive($iAlbumId) {

        global $odb;

        $sQuery = "
            UPDATE `[table:q]`
            SET `visible`= NOT `visible`
            WHERE
                `album_id` = [album:i]";

        $odb->query($sQuery, array(
            'table' => self::$sCurrentTable,
            'album' => $iAlbumId
        ));

        return $odb->affected_rows;

    }

}// class
