<?php
/**
 * Модуль фотогаллереи
 * @class GalleryModule
 *
 * @author ArmiT, $Author: acat $
 * @version $Revision: 1437 $
 * @date $Date: 2012-12-19 14:36:22 +0400 (Ср., 19 дек. 2012) $
 * @project Skewer
 * @package Modules
 */
class GalleryModule extends skModule {

    public $AlbumsList  = 'albumsList.twig';
    public $AlbumDetail = 'showAlbum.twig';
    private $iCurrentSection;

    public function init() {

        $this->setParser(parserTwig);
        $this->iCurrentSection = $this->getEnvParam('sectionId');

    }// func

    public function execute() {

        $sAlbumAlias  = urldecode($this->getStr('album-alias', ''));

        return (!$sAlbumAlias)? $this->getAlbums() : $this->showAlbum($sAlbumAlias);

    }// func

    public function getAlbums() {

        try {
            if(!$this->iCurrentSection) throw new Exception('Section not found!');

            $aItems = Gallery2AlbumsApi::getBySection($this->iCurrentSection, false);

            if($aItems['count'])
                foreach($aItems['items'] as $iKey=>$aAlbum) {
                    $aItems['items'][$iKey]['imagesCount'] = Gallery2ImagesApi::getCountByAlbum($aAlbum['album_id']);
                    $aItems['items'][$iKey]['image'] = Gallery2AlbumsApi::getFirstActiveImage($aAlbum['album_id']);
                }// each album

            $this->setData('albums', $aItems);
            $this->setData('sectionId', $this->iCurrentSection);
            $this->setTemplate($this->AlbumsList);

        } catch (Exception $e) {

            $this->setPage(page404);
            return psExit;
        }
        return psComplete;
    }// func

    public function showAlbum($mAlbumId) {

        try {

            $this->addJSFile( 'galleryInit.js' );

            if(!$mAlbumId) throw new Exception('Album not found!');

            $aAlbum = Gallery2AlbumsApi::getByAlias($mAlbumId, 0, true, true);

            if( !$aAlbum ) throw new Exception('Album not found!');

            /** @var skProcess $oTitleModule */
            $oTitleModule = $this->getProcess('out.title', psAll);
            if($oTitleModule->getStatus() != psComplete) return psWait;

            $oTitleModule->setData('title', $aAlbum['title']);
            $this->setData('album', $aAlbum);
            $this->setTemplate($this->AlbumDetail);

            // Метатеги для списка новостей
            $SEOData = $this->getEnvParam('SEOTemplates', array());
            $SEOData['galleryDetail'] = array('title'=>'','description'=>'','keywords'=>'');
            if($aCurSEOData = SEOData::get('gallery',$aAlbum['album_id'])){
                $SEOData['galleryDetail']['title'] = $aCurSEOData['title'];
                $SEOData['galleryDetail']['description'] = $aCurSEOData['description'];
                $SEOData['galleryDetail']['keywords'] = $aCurSEOData['keywords'];
            }
            $this->setEnvParam('SEOTemplates', $SEOData);
            $SEOVars = $this->getEnvParam('SEOVars', array());
            $SEOVars['Название фотогалереи'] = $aAlbum['title'];
            $SEOVars['название фотогалереи'] = mb_strtolower($aAlbum['title']);
            $this->setEnvParam('SEOVars', $SEOVars);

        } catch(Exception $e) {

            $this->setPage(page404);
            return psExit;
        }

        return psComplete;
    }// func

}// class
