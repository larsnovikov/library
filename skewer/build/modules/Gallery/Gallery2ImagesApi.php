<?php
/**
 * API для работы с изображениями
 * @class Gallery2PhotosApi
 *
 * @author ArmiT, $Author: acat $
 * @version $Revision: 1024 $
 * @date $Date: 2012-10-10 16:09:09 +0400 (Ср., 10 окт. 2012) $
 * @project Skewer
 * @package Modules
 */
class Gallery2ImagesApi {

    /**
     * Возвращает список изображений по фильтру $aFilter
     * @static
     * @param $aFilter
     * @return array|bool
     */
    public static function getImages($aFilter) {

        return Gallery2ImagesMapper::getItems($aFilter);

    }

    /**
     * Возвращает список изображений для альбома $iAlbumId. Если данные не найдены вернется false;
     * @static
     * @param $iAlbumId
     * @param bool $bWithoutHidden Указатель на необходимость выборки без учета видимости
     * @return bool|array
     */
    public static function getFromAlbum($iAlbumId, $bWithoutHidden = false) {

        if($bWithoutHidden)
            $aFilter['where_condition']['visible'] = array( 'sign' => '=', 'value' => 1 );

        $aFilter['where_condition']['album_id'] = array( 'sign' => '=', 'value' => (int)$iAlbumId );
        $aFilter['order']                     = array('field' => 'priority', 'way' => 'DESC');
        $aItems = self::getImages($aFilter);

        return (count($aItems['items']))? $aItems['items']: false;

    }

    /**
     * Возвращает количество Изображений в альбоме $iAlbumId
     * @static
     * @param int $iAlbumId Id Альбома
     * @return int
     */
    public static function getCountByAlbum($iAlbumId) {

        return Gallery2ImagesMapper::getCountByAlbum($iAlbumId);

    }

    /**
     * Возвращает максимальный индекс сортировки в альбоме $iAlbumId
     * @static
     * @param int $iAlbumId Id Альбома
     * @return int
     */
    public static function getMaxPriorityByAlbum($iAlbumId) {

        return Gallery2ImagesMapper::getMaxPriorityByAlbum($iAlbumId);

    }

    /**
     * Возвращает данные записи изображения $iImageId
     * @static
     * @param int $iImageId Id изображения
     * @return bool|array Возвращает массив данных либо false в случае отсутствия записи.
     */
    public static function getImage($iImageId) {

        if(!(int)$iImageId) return false;

        $aFilter['limit']                       = array( 'start' => 0, 'count' => 1 );
        $aFilter['where_condition']['image_id'] = array( 'sign'  => '=',    'value' => (int)$iImageId );

        $aItems =  self::getImages($aFilter);

        if(!count($aItems['items'])) return false;

        return $aItems['items'][0];

    }

}// class
