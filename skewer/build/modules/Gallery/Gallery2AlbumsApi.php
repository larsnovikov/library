<?php
/**
 * API для работы с альбомами
 * @class Gallery2AlbumsApi
 *
 * @author ArmiT, $Author: acat $
 * @version $Revision: 1211 $
 * @date $Date: 2012-11-15 18:52:15 +0400 (Чт., 15 нояб. 2012) $
 * @project Skewer
 * @package Modules
 */
class Gallery2AlbumsApi {

    /**
     * Возвращает результаты выборки (альбомы) по фильтру $aFilter
     * @param array $aFilter Фильтр выборки
     * @return bool|array Возвращет массив результатов либо false
     */
    public static function getAlbums($aFilter) {

        return Gallery2AlbumsMapper::getItems($aFilter);

    }// func


    /**
     * Возвращает данные альбома $iAlbumId
     * @param int $iAlbumId Id запрашиваемого альбома
     * @param bool $bWithImages Указатель на необходимость выборки вместе с изображениями
     * @param bool $bWithoutHidden без скрытых фото
     * @return bool|array
     */
    public static function getById($iAlbumId, $bWithImages = false, $bWithoutHidden = false) {

        if(!(int)$iAlbumId) return false;

        $aFilter['limit']                       = array( 'start' => 0, 'count' => 1 );
        $aFilter['where_condition']['album_id'] = array( 'sign'  => '=',    'value' => (int)$iAlbumId );

        $aItems = self::getAlbums($aFilter);

        $aItems = (count($aItems['items']))? $aItems['items'][0]: false;

        if(!$aItems) return false;

        if(!$bWithImages) return $aItems;

        $aImages = Gallery2ImagesApi::getFromAlbum($iAlbumId, $bWithoutHidden);

        if($aImages)
            foreach($aImages as $aImage) {
                if(!$aImage['images_data'] = json_decode($aImage['images_data'], true)) continue;
                $aItems['images'][] = $aImage;
            }// each image

        return $aItems;
    }// func


    /**
     * Возвращает данные альбома $sAlbumAlias
     * @param string $sAlbumAlias Alias запрашиваемого альбома
     * @param int $iSectionId
     * @param bool $bWithImages Указатель на необходимость выборки вместе с изображениями
     * @param bool $bWithoutHidden без скрытых фото
     * @return bool|array
     */
    public static function getByAlias($sAlbumAlias, $iSectionId = 0, $bWithImages = false, $bWithoutHidden = false) {

        if(!$sAlbumAlias) return false;

        $aFilter['limit']                       = array( 'start' => 0, 'count' => 1 );
        $aFilter['where_condition']['alias'] = array( 'sign'  => '=',    'value' => $sAlbumAlias );
        if( $iSectionId )
            $aFilter['where_condition']['section_id '] = array( 'sign'  => '=', 'value' => $iSectionId );

        $aItems = self::getAlbums($aFilter);

        $aItems = (count($aItems['items']))? $aItems['items'][0]: false;

        if(!$aItems) return false;

        if(!$bWithImages) return $aItems;

        $iAlbumId = $aItems['album_id'];

        $aImages = Gallery2ImagesApi::getFromAlbum($iAlbumId, $bWithoutHidden);

        if($aImages)
            foreach($aImages as $aImage) {
                if(!$aImage['images_data'] = json_decode($aImage['images_data'], true)) continue;
                $aItems['images'][] = $aImage;
            }// each image

        return $aItems;
    }// func


    /**
     * Возвращает массив видимых (активных) альбомов из раздела $iSectionId
     * @param int $iSectionId Id раздела
     * @param bool $bAll Указатель на необходимость выдорки всех альбомов раздела
     * @param int $iPage
     * @param int $iOnPage
     * @return bool|array Возвращает массив найденных альбомов либо false
     */
    public static function getBySection($iSectionId, $bAll = true, $iPage= 0 , $iOnPage = 0) {

        if(!(int)$iSectionId) return false;

        if(!(!$iPage AND !$iOnPage))
            $aFilter['limit'] = array( 'start' => $iPage, 'count' => $iOnPage );

        $aFilter['order'] = array('field' => 'priority', 'way' => 'DESC');
        $aFilter['where_condition']['section_id'] = array('sign' => '=', 'value' => (int)$iSectionId);

        if(!$bAll)
            $aFilter['where_condition']['visible'] = array('sign' => '=', 'value' => 1);

        return self::getAlbums($aFilter);

    }// func

    /**
     * Получить список всех альбомов в разделе
     * @param int $iSectionId
     * @return array
     */
    public static function getAllBySection($iSectionId) {

        if(!(int)$iSectionId) return array();

        return Gallery2AlbumsMapper::getAllBySection($iSectionId);
    }

    /**
     * Возвращает Id профиля настроек по $iAlbumId
     * @static
     * @param int $iAlbumId Id альбома
     * @return int|bool
     */
    public static function getProfileId($iAlbumId) {

        if(!(int)$iAlbumId) return false;

        $aFilter['select_fields']                 = array('profile_id');
        $aFilter['limit']                         = array( 'start' => 0, 'count' => 1 );
        $aFilter['where_condition']['album_id'] = array( 'sign'  => '=',    'value' => (int)$iAlbumId );

        $aOut = Gallery2AlbumsMapper::getItem($aFilter);

        return ($aOut['profile_id'])? $aOut['profile_id']: false;

    }// func

    public static function getFirstActiveImage($iAlbumId) {

        if(!(int)$iAlbumId) return false;

        $aFilter['select_fields']                 = array('images_data');
        $aFilter['limit']                         = array( 'start' => 0, 'count' => 1 );
        $aFilter['where_condition']['album_id']   = array( 'sign'  => '=',    'value' => (int)$iAlbumId );
        $aFilter['where_condition']['visible']    = array( 'sign'  => '=',    'value' => 1 );
        $aFilter['order']                         = array('field' => 'priority', 'way' => 'DESC');

        $aOut = Gallery2ImagesApi::getImages($aFilter);

       if(!$aOut['count']) return false;

       if(!$aImages = json_decode($aOut['items'][0]['images_data'], true)) return false;

       if(isSet($aImages['preview'])){
           return $aImages['preview']['file'];
       }else {
           reset($aImages);
           $aImages = current($aImages);
           return $aImages['file'];
       }
    }// func

}// class
