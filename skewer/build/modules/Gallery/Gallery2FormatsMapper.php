<?php
/**
 * Маппер и модель данных для форматов изображений фотогаллереи
 * @class Gallery2FormatsMapper
 *
 * @author ArmiT, $Author: acat $
 * @version $Revision: 1343 $
 * @date $Date: 2012-11-30 17:14:11 +0400 (Пт., 30 нояб. 2012) $
 * @project Skewer
 * @package Modules
 */
class Gallery2FormatsMapper extends skMapperPrototype {

    protected  static $sCurrentTable = 'photogallery_formats';

    protected static  $sKeyFieldName = 'format_id';

    protected static $aParametersList = array(

        'title'           => 's:str:Название:Новый формат',
        'name'            => 's:str:Техническое имя',
        'width'           => 'i:int:Ширина (px):0',
        'height'          => 'i:int:Высота(px):0',
        'active'          => 'i:check:Использовать формат:1',
        'format_id'       => 'i:hide:id',
        'resize_on_larger_side'  => 'i:check:Ресайз по большей стороне:1',
        'scale_and_crop'  => 'i:check:Вписывать изображение:1',
        'as_template'     => 'i:hide:Использовать как шаблон:0',
        'use_watermark'   => 'i:check:Использовать водяные знаки:0',
        'watermark'       => 's:file:Текст водяного знака',
        'profile_id'      => 'i:hide:id профиля',
        'watermark_align' => 'i:select:Выравнивание водяного знака:84',
        'position'        => 'i:hide:Позиция при сортировки',

    );

    public static function delFormat($iFormatId) {

        global $odb;

        $sQuery = '
            DELETE
            FROM `[table_name:q]`
            WHERE `format_id`=[format_id:i];
        ';

        $bResult = $odb->query($sQuery, array(
            'table_name' => self::$sCurrentTable,
            'format_id' => $iFormatId
        ));

        return ((bool)$bResult)? true: false;

    }// func

    public static function delFormatsByProfile($iProfileId) {

        global $odb;

        $sQuery = '
            DELETE
            FROM `[table_name:q]`
            WHERE `profile_id`=[profile_id:i];
        ';

        $bResult = $odb->query($sQuery, array(
            'table_name' => self::$sCurrentTable,
            'profile_id' => $iProfileId
        ));

        return ((bool)$bResult)? true: false;

    }// func


    public static function getAddParamList() {

        return array(
            'watermark_align' => ExtForm::getDesc4SelectFromArray(Gallery2FormatsApi::getWatermarkAlignTypes()),
        );
    }// func


    /**
     * Выполняет сдвиг положений параметров
     * @param $iFormId
     * @param $iStartPos
     * @param $iEndPos
     * @param string $sSign
     * @return int
     */
    public static function shiftParamPosition($iFormId,$iStartPos,$iEndPos,$sSign = '+'){

        global $odb;

        $sQuery = "
            UPDATE `[table:q]`
            SET `position`=`position` [sign:q] 1
            WHERE
                `profile_id` = [form:i] AND
                `position` > [start:i] AND
                `position` < [end:i]";

        $odb->query($sQuery, array(
            'table' => self::$sCurrentTable,
            'form' => $iFormId,
            'sign' => $sSign,
            'start' => $iStartPos,
            'end' => $iEndPos
        ));

        return $odb->affected_rows;
    }

    /**
     * меняет значение для поля сортировки параметра на заданное
     * @param $iParamId
     * @param $iPos
     * @return int
     */
    public static function changeParamPosition($iParamId, $iPos){

        global $odb;

        $sQuery = "
            UPDATE `[table:q]`
            SET `position`=[pos:i]
            WHERE
                `format_id` = [param_id:i]";

        $odb->query($sQuery, array(
            'table' => self::$sCurrentTable,
            'param_id' => $iParamId,
            'pos' => $iPos
        ));

        return $odb->affected_rows;
    }

    public static function getMaxPosition($iProfileId, $iCurItemId = 0){

        global $odb;

        $iCurItemId = (int)$iCurItemId;

        $aData = array(
            'profile_id' => $iProfileId,
            'table' => self::$sCurrentTable,
            'add_where' => $iCurItemId ? "`format_id`<>$iCurItemId AND" : ''
        );

        $sQuery = "
            SELECT
                MAX(`position`) as `pos`
            FROM
                `[table:q]`
            WHERE [add_where:q]
                `profile_id` = [profile_id:i];";

        $aValue = $odb->getRow($sQuery, $aData);

        return $aValue['pos'];
    }
}// class

