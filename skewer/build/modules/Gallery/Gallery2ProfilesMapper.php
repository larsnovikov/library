<?php
/**
 * Маппер и модель данных для профилей настройки
 * @class Gallery2ProfilesMapper
 *
 * @author ArmiT, $Author: sapozhkov $
 * @version $Revision: 6 $
 * @date $Date: 2012-05-18 10:52:53 +0400 (Пт., 18 мая 2012) $
 * @project Skewer
 * @package Modules
 */
class Gallery2ProfilesMapper extends skMapperPrototype {

    protected  static $sCurrentTable = 'photogallery_profiles';

    protected static $aParametersList = array(

        'title'         => 's:str:Название',
        'active'        => 'i:check:Выводить в списке форматов',
        'profile_id'    => 'i:hide:id',
        'as_template'   => 'i:hide:Использовать как шаблон',

    );

    public static function delProfile($iProfileId) {

        global $odb;

        $sQuery = '
            DELETE
            FROM `[table_name:q]`
            WHERE `profile_id`=[profile_id:i];
        ';

        $bResult = $odb->query($sQuery, array(
            'table_name' => self::$sCurrentTable,
            'profile_id' => $iProfileId
        ));

        return ((bool)$bResult)? true: false;
    }// func

}// class
