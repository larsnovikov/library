<?php

/* main */
$aConfig['name']     = 'Gallery';
$aConfig['title']     = 'Галерея';
$aConfig['version']  = '1.000a';
$aConfig['description']  = 'Модуль фотогаллереи';
$aConfig['revision'] = '0001';
$aConfig['layer']     = 'Page';

/* Module hooks */
$aConfig['hooks']['before']['goTest'] = array();
$aConfig['hooks']['after']['goTest']  = array();

/* Module css/js inclusions */

$aConfig['css'] = array(
    //'/skewer_build/modules/Page/css/main.css'=> array('weight' => 1000),
);

return $aConfig;