<?php
/**
 *
 * @class Gallery2ImagesMapper
 *
 * @author ArmiT, $Author: acat $
 * @version $Revision: 1343 $
 * @date $Date: 2012-11-30 17:14:11 +0400 (Пт., 30 нояб. 2012) $
 * @project JetBrains PhpStorm
 * @package kernel
 */
class Gallery2ImagesMapper extends skMapperPrototype {

    /**
     * Название таблицы
     * @var string
     */
    protected  static $sCurrentTable = 'photogallery_images';

    /**
     * Первичный ключ
     * @var string
     */
    protected static $sKeyFieldName = 'image_id';

    /**
     * Модель данных
     * @var array
     */
    protected static $aParametersList = array(


        'title'         => 's:str:Название фотографии',
        'source'        => 's:str:Исходное изображение',
        'visible'       => 'i:check:Выводить',
        'priority'      => 'i:hide:Индес сортировки',
        'image_id'      => 'i:hide:Id изображения',
        'album_id'      => 'i:hide:Id альбома',
        'thumbnail'     => 's:str:Миниатюра для списка',
        'images_data'   => 's:hide:Массив списка изображений по форматам',
        'description'   => 's:text:Описание',
        'creation_date' => 's:str:Дата создания',


    );

    /**
     * Возвращает количество изображений в альбоме
     * @static
     * @param $iAlbumId
     * @return int
     */
    public static function getCountByAlbum($iAlbumId) {
        global $odb;

        $sQuery = '
            SELECT
              COUNT(image_id) AS `count`
            FROM `[table_name:q]`
            WHERE `album_id`=[album_id:i];
        ';

        $bResult = $odb->query($sQuery, array(
            'table_name' => self::$sCurrentTable,
            'album_id' => (int)$iAlbumId
        ));

        $bResult = $bResult->fetch_assoc();
        return $bResult['count'];

    }// func

    /**
     * Возвращает максимальный индекс сортировки в альбоме
     * @static
     * @param $iAlbumId
     * @return int
     */
    public static function getMaxPriorityByAlbum($iAlbumId) {
        global $odb;

        $sQuery = '
            SELECT
              MAX(priority) AS `count`
            FROM `[table_name:q]`
            WHERE `album_id`=[album_id:i];
        ';

        $bResult = $odb->query($sQuery, array(
            'table_name' => self::$sCurrentTable,
            'album_id' => (int)$iAlbumId
        ));

        $bResult = $bResult->fetch_assoc();
        return $bResult['count'];

    }// func

    /**
     * Выполняет сдвиг положений для подчиненных изображений
     * @param $iAlbumId
     * @param $iStartPos
     * @param $iEndPos
     * @param string $sSign
     * @return int
     */
    public static function shiftImagePosition($iAlbumId,$iStartPos,$iEndPos,$sSign = '+'){

        global $odb;

        $sQuery = "
            UPDATE `[table:q]`
            SET `priority`=`priority` [sign:q] 1
            WHERE
                `album_id` = [album:i] AND
                `priority` > [start:i] AND
                `priority` < [end:i]";

        $odb->query($sQuery, array(
            'table' => self::$sCurrentTable,
            'album' => $iAlbumId,
            'sign' => $sSign,
            'start' => $iStartPos,
            'end' => $iEndPos
        ));

        return $odb->affected_rows;
    }

    /**
     * меняет значение для поля сортировки изображения на заданное
     * @param $iImageId
     * @param $iPos
     * @return int
     */
    public static function changeImagePosition($iImageId, $iPos){

        global $odb;

        $sQuery = "
            UPDATE `[table:q]`
            SET `priority`=[pos:i]
            WHERE
                `image_id` = [image:i]";

        $odb->query($sQuery, array(
            'table' => self::$sCurrentTable,
            'image' => $iImageId,
            'pos' => $iPos
        ));

        return $odb->affected_rows;
    }

    public static function changeImageActive($iImageId){

        global $odb;

        $sQuery = "
            UPDATE `[table:q]`
            SET `visible`= NOT `visible`
            WHERE
                `image_id` = [image:i]";

        $odb->query($sQuery, array(
            'table' => self::$sCurrentTable,
            'image' => $iImageId
        ));

        return $odb->affected_rows;

    }

}
