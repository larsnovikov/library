<?php
/**
 * API для работы с профилями настроек
 * @class Gallery2ProfilesApi
 * @author ArmiT, $Author: acat $
 * @version $Revision: 961 $
 * @date $Date: 2012-10-03 16:29:35 +0400 (Ср., 03 окт. 2012) $
 * @project Skewer
 * @package Modules
 */
class Gallery2ProfilesApi {

    /**
     * Добавляет либо обновляет данные профиль обработки изображений. Если указан $iProfileId,
     * то происходит обновление записи.
     * @param  array $aData Массив данных. Заполняемые в aData поля:
     * profile_id,
     * title,
     * as_template,
     * active
     * @param bool|int $iProfileId Id обновляемого профиля
     * @return bool|int Возвращет Id созданной записи либо количество затронутых строк
     * либо false в случае ошибки.
     */
    public static function setProfile($aData, $iProfileId = false) {

        unSet($aData['profile_id']);
        if((int)$iProfileId) $aData['profile_id'] = (int)$iProfileId;

        return Gallery2ProfilesMapper::saveItem($aData);

    }// func

    /**
     * Создает новый профиль настроек галереи. Если указан $iTemplateId, то профиль
     * Создается на основе уже существующего с id = $iTemplateId
     * @param bool|int $iTemplateId - Id существующего профиля
     * @param array $aData Значения полей профиля. Заполняемые в aData поля:
     * title,
     * as_template,
     * active
     * @return bool|int Возвращает Id созданного профиля либо false в случае неудачи.
     */
    public static function addProfile($aData, $iTemplateId = false) {

        if(isSet($aData['profile_id'])) unSet($aData['profile_id']);

        $iNewId = self::setProfile($aData);

        if(!$iNewId) return false;

        if((int)$iTemplateId) {

            $aTemplateFormats = Gallery2FormatsApi::getByProfile($iTemplateId);
            if(!count($aTemplateFormats)) return false;

            if(isSet($aTemplateFormats))
                foreach($aTemplateFormats as $aFormatItem) {

                    unSet($aFormatItem['format_id']);
                    unSet($aFormatItem['profile_id']);
                    $aFormatItem['as_template'] = 0;
                    Gallery2FormatsApi::addFormat($iNewId, $aFormatItem);

                }// each format

            return $iNewId;

        }// based on template

        return $iNewId;

    }// func

    /**
     * Обновляет данные профиля $iProfileId
     * @param int $iProfileId Id обновляемого профиля
     * @param array $aData Данные для замены. Заполняемые в aData поля:
     * title,
     * as_template,
     * active
     * @return bool Возвращает true в случае успешного обнавления либо false
     */
    public static function updateProfile($iProfileId, $aData) {

        return self::setProfile($aData, $iProfileId);

    }// func

    /**
     * Удаляет профайл $iProfileId
     * @param int $iProfileId Id удялемого профиля настроек
     * @return bool
     */
    public static function remove($iProfileId) {

        If(!(int)$iProfileId) return false;

        $bProfile = Gallery2ProfilesMapper::delProfile($iProfileId);
        $bFormats = Gallery2FormatsMapper::delFormatsByProfile($iProfileId);

        return ($bProfile AND $bFormats);
    }// func

    /**
     * Устанавливает профиль как шаблон
     * @param int $iProfileId Id профиля настроек
     * @param bool $bIsTemplate Флаг, указывающий пренадлежность профиля к шаблонам
     * @return bool
     */
    public static function setAsTemplate($iProfileId, $bIsTemplate = false) {

        If(!(int)$iProfileId) return false;

        $aData['as_template'] = (int)$bIsTemplate;

        return self::setProfile($aData, $iProfileId);

    }// func

    /**
     * Возвращает список профилей настроек по фильтру $aFilter
     * @param array $aFilter Фильтр выборки
     * @return array|bool Возвращает массив найденных элементов либо false
     */
    public static function getProfiles($aFilter) {
        return Gallery2ProfilesMapper::getItems($aFilter);

    }// func

    /**
     * Возвращает список профилей настроек по фильтру $aFilter
     * @param int $iPage
     * @param int $iOnPage
     * @return array|bool Возвращает массив найденных элементов либо false
     */
    public static function getProfilesList($iPage= 1 , $iOnPage = 0) {

        $aFilter = array();

        if ( $iOnPage ) {
            $iPage = ($iPage)? --$iPage: $iPage;
            $aFilter['limit'] = array( 'start' => $iPage, 'count' => $iOnPage );
        }

        return Gallery2ProfilesMapper::getItems($aFilter);

    }// func

    public static function getActiveProfilesShortList() {

        $aFilter['select_fields']             = array('profile_id', 'title');
        $aFilter['where_condition']['active'] = array( 'sign' => '=', 'value' => 1 );
        $aFilter['order']                     = array('field' => 'title', 'way' => 'ASC');

        $aItems = Gallery2ProfilesMapper::getItems($aFilter);

        if(!count($aItems['items'])) return false;

        $aOut = array();

        foreach($aItems['items'] as $aItem)
             $aOut[$aItem['profile_id']] = $aItem['title'];

        return $aOut;

    }// func

    /**
     * Возвращает данные профиля $iProfileId. Если указан $bGetFormats, в ключе "formats" будет возвращен список связных
     * с профилем форматов
     * @param int $iProfileId Id запрашиваемого профиля
     * @param bool $bGetFormats Указывает на необходимость выборки связных форматов
     * @return bool|array Возвращвет данные профиля либо false
     */
    public static function getProfile($iProfileId, $bGetFormats = false) {

        if(!(int)$iProfileId) return false;

        $aFilter['limit']                         = array( 'start' => 0, 'count' => 1 );
        $aFilter['where_condition']['profile_id'] = array( 'sign' => '=', 'value' => (int)$iProfileId );

        $mOut = self::getProfiles($aFilter);

        if( !isSet($mOut['items'][0]) ) return false;
        $mOut = $mOut['items'][0];


        if(!$bGetFormats) return $mOut;

        $mOut['items'] = Gallery2FormatsApi::getByProfile($iProfileId);

        return $mOut;

    }// func

    /**
     * Возвращает список названий и id профилей, являющихся шаблонами либо false
     * @param bool $bActiveOnly Указывает на необходимость выборки только активных профилей
     * @return array|bool
     */
    public static function getTemplates($bActiveOnly = false) {

        $aFilter['where_condition']['as_template'] = array( 'sign' => '=', 'value' => '1' );

        if($bActiveOnly)
            $aFilter['where_condition']['active'] = array( 'sign' => '=', 'value' => '1' );

        return self::getProfiles($aFilter);

    }// func

    /**
     * Изменяет Активность профиля (флаг применимости)
     * @param int $iProfileId Id существующего формата
     * @param bool $bActive
     * @return bool
     */
    public static function changeActive($iProfileId, $bActive = true) {

        If(!(int)$iProfileId) return false;

        $aData['active'] = (int)$bActive;

        return self::setProfile($aData, $iProfileId);

    }// func


    /**
     * Отдает шаблонный набор значений для добавления новой записи
     * @static
     * @return array
     */
    public static function getProfileBlankValues() {
        return array(
            'title' => 'Новый профиль',
            'as_template' => 0,
            'active' => 1
        );
    }

    /**
     * Возвращает Id первого активного профиля
     * @static
     * @return bool
     */
    public static function getFirstActiveProfileId() {

        $aFilter['select_fields']             = array('profile_id');
        $aFilter['limit']                     = array( 'start' => 0, 'count' => 1 );
        $aFilter['where_condition']['active'] = array( 'sign' => '=', 'value' => 1 );
        $aFilter['order']                     = array('field' => 'profile_id', 'way' => 'ASC');

        $aRes = self::getProfiles($aFilter);

        return (count($aRes['items']))? $aRes['items'][0]['profile_id']: false;

    }// func

}// class
