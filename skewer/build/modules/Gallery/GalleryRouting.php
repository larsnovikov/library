<?php
/**
 * @class: GalleryRouting
 *
 * @Author: ArmiT, $Author: acat $
 * @version: $Revision: 1211 $
 * @date: $Date: 2012-11-15 18:52:15 +0400 (Чт., 15 нояб. 2012) $

 */
class GalleryRouting implements skRoutingInterface {
    /**
     * Возвращает паттерны разбора URL
     * @static
     * @return bool | array
     */
    public static function getRoutePatterns() {

        return array(
            '/album-alias/',
            //'/*album/album_id(int)/',
        );
    }// func
}// class
