<?php
/**
 * API для работы с форматами изображений
 * @class Gallery2FormatsApi
 *
 * @author ArmiT, $Author: acat $
 * @version $Revision: 1343 $
 * @date $Date: 2012-11-30 17:14:11 +0400 (Пт., 30 нояб. 2012) $
 * @project Skewer
 * @package Modules
 */
class Gallery2FormatsApi {

    protected static $aWatermarkAlignTypes = array(

        alignWatermarkTopLeft     => 'Верхний левый угол',
        alignWatermarkTopRight    => 'Верхний правый угол',
        alignWatermarkBottomLeft  => 'Нижний левый угол',
        alignWatermarkBottomRight => 'Нижний правый угол',
        alignWatermarkCenter      => 'По центру',

    );


    /**
     * Добавляет либо обновляет данные формата изображения. Если указан $iFormatId,
     * то происходит обновление записи.
     * @param  array $aData Массив данных
     * @param bool|int $iFormatId Id обновляемого формата
     * @return bool|int Возвращет Id созданной записи, true, если запись была обновлена
     * либо false в случае ошибки.
     */
    public static function setFormat($aData, $iFormatId = false) {

        if($iFormatId) {
            $aData['format_id'] = (int)$iFormatId;
        } else {
            $aData['position'] = Gallery2FormatsMapper::getMaxPosition($aData['profile_id']) + 1;
        }
        return Gallery2FormatsMapper::saveItem($aData);

    }// func

    /**
     * Добавляет новый формат. Если указан $iTemplateId запись создается на основе шаблона
     * @param int $iProfileId Id профиля-владельца
     * @param array $aData Данные формата
     * @param bool $iTemplateId Id шаблона либо false
     * @return bool|int Возвращет Id созданной записи
     * либо false в случае ошибки.
     */
    public static function addFormat($iProfileId, $aData, $iTemplateId = false) {

        if(!(int)$iProfileId) return false;

        if($iTemplateId) {


            $aTemplate = self::getById($iTemplateId);

            if(count($aTemplate)) {
                unSet($aTemplate['format_id']);
                unSet($aTemplate['profile_id']);
                $aTemplate['is_template'] = 0;

                $aData = $aTemplate;
            }
        }// use template

        if(!isSet($aData['profile_id'])) $aData['profile_id'] = $iProfileId;

        return self::setFormat($aData);

    }// func

    /**
     * Обновляет данные формата.
     * @param int $iFormatId Id изменяемого формата
     * @param array $aData Данные формата
     * @return bool
     */
    public function updateFormat($iFormatId, $aData) {

        if(!(int)$iFormatId) return false;

        return self::setFormat($aData, $iFormatId);

    }// func

    /**
     * Удаляет формат с id = $iFormatId
     * @param int $iFormatId
     * @return bool
     */
    public static function removeFormat($iFormatId) {

        If(!(int)$iFormatId) return false;

        return Gallery2FormatsMapper::delFormat($iFormatId);
    }// func

    /**
     * Устанавливает формат как шаблон
     * @param int $iFormatId Id формата
     * @param bool $bIsTemplate Флаг, указывающий пренадлежность формата к шаблонам
     * @return bool
     */
    public function setAsTemplate($iFormatId, $bIsTemplate = false) {

        If(!(int)$iFormatId) return false;

        $aData['as_template'] = (int)$bIsTemplate;

        return self::setFormat($aData, $iFormatId);

    }// func

    /**
     * Возвращает набор форматов по фильтру
     * @param array $aFilter - Массив параметров выборки
     * @return bool|array Возвращает массив найденных элеметов либо false
     */
    public function getFormats($aFilter) {

        return Gallery2FormatsMapper::getItems($aFilter);

    }// func

    /**
     * Возвращает данные формата с ID $iFormatId
     * @static
     * @param int $iFormatId ID искомого формата
     * @return bool|array
     */
    public static function getById($iFormatId) {

        if(!(int)$iFormatId) return false;

        $aFilter['limit']                         = array( 'start' => 0, 'count' => 1 );
        $aFilter['where_condition']['format_id'] = array( 'sign' => '=', 'value' => (int)$iFormatId );

        $mOut = self::getFormats($aFilter);

        return (isSet($mOut['items'][0]))? $mOut['items'][0]: false;

    }// func


    public static function getByName($sFormatName) {

        if(!$sFormatName) return false;

        $aFilter['limit']                         = array( 'start' => 0, 'count' => 1 );
        $aFilter['where_condition']['name'] = array( 'sign' => '=', 'value' => $sFormatName );

        $mOut = self::getFormats($aFilter);

        return (isSet($mOut['items']))? $mOut['items']: false;

    }

    /**
     * Возвращает набор форматов по id профиля, равному $iProfileId
     * @param int $iProfileId
     * @return bool|array Возвращает массив найденных элеметов либо false
     */
    public static function getByProfile($iProfileId) {

        if(!(int)$iProfileId) return false;
        $aFilter['where_condition']['profile_id'] = array( 'sign' => '=', 'value' => (int)$iProfileId );
        $aFilter['order'] = array( 'field' => 'position', 'way' => 'ASC' );
        $mOut = self::getFormats($aFilter);
        return (isSet($mOut['items']))? $mOut['items']: false;

    }

    /**
     * Возвращает набор форматов-шаблонов
     * @param bool $bActiveOnly Указывает на необходимость выборки только активных профилей
     * @return bool|array Возвращает массив найденных элеметов либо false
     */
    public function getTemplates($bActiveOnly = false) {

        $aFilter['where_condition']['as_template'] = array( 'sign' => '=', 'value' => '1' );

        if($bActiveOnly)
            $aFilter['where_condition']['active'] = array( 'sign' => '=', 'value' => '1' );

        return self::getFormats($aFilter);

    }// func

    /**
     * Изменяет Активность формата (флаг применимости)
     * @param int $iFormatId Id существующего формата
     * @param bool $bActive
     * @return bool
     */
    public function changeActive($iFormatId, $bActive = true) {

        If(!(int)$iFormatId) return false;

        $aData['active'] = (int)$bActive;

        return self::setFormat($aData, $iFormatId);

    }

    /**
     * Отдает шаблонный набор значений для добавления новой записи
     * @static
     * @return array
     */
    public static function getProfileBlankValues() {

        return Gallery2FormatsMapper::getBlankModel();

    }

    /**
     * Возвращает типи выравнивания водяных знаков для изображений
     * @static
     * @return array
     */
    public static function getWatermarkAlignTypes()
    {
        return self::$aWatermarkAlignTypes;
    } // func


    /**
     * @param $aItem
     * @param $aTarget
     * @param string $sOrderType
     * @return bool
     */
    public static function sortParams($aItem, $aTarget, $sOrderType='before') {

        $sSortField = 'position';

        // должны быть в одной форме
        if( $aItem['profile_id'] != $aTarget['profile_id'] )
            return false;

        // выбираем напрвление сдвига
        if( $aItem[$sSortField] > $aTarget[$sSortField] ){

            $iStartPos = $aTarget[$sSortField];
            if( $sOrderType=='before' ) $iStartPos--;
            $iEndPos = $aItem[$sSortField];
            $iNewPos = $sOrderType=='before' ? $aTarget[$sSortField] : $aTarget[$sSortField] + 1;
            Gallery2FormatsMapper::shiftParamPosition($aItem['profile_id'], $iStartPos, $iEndPos, '+');
            Gallery2FormatsMapper::changeParamPosition($aItem['format_id'],$iNewPos);

        } else {

            $iStartPos = $aItem[$sSortField];
            $iEndPos = $aTarget[$sSortField];
            if( $sOrderType=='after' ) $iEndPos++;
            $iNewPos = $sOrderType=='after' ? $aTarget[$sSortField] : $aTarget[$sSortField] - 1;
            Gallery2FormatsMapper::shiftParamPosition($aItem['profile_id'], $iStartPos, $iEndPos, '-');
            Gallery2FormatsMapper::changeParamPosition($aItem['format_id'],$iNewPos);

        }

        return true;

    }
}// class
