<?php
/**
 *
 * @project Skewer
 * @package Modules
 *
 * @author kolesnikov, $Author: sapozhkov $
 * @version $Revision: 6 $
 * @date $Date: 2012-05-18 10:52:53 +0400 (Пт., 18 мая 2012) $
 */
class PageService extends ServicePrototype{



    public function addToLog($sTitle, $sDescription){

        $mRes = skLogger::addToLog($sTitle, $sDescription,'Page',1,1);

        return Tasks::$taskStatusComplete;
    }


}
