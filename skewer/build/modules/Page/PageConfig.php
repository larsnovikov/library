<?php

/* main */
$aConfig['name']     = 'Page';
$aConfig['title']    = 'Страница сайта';
$aConfig['version']  = '1.000a';
$aConfig['description']  = 'Модуль сборки типовой страницы сайта по параметрам раздела';
$aConfig['revision'] = '0001';
$aConfig['layer']     = 'Page';

/* Module hooks */
$aConfig['hooks']['before']['goTest'] = array();
$aConfig['hooks']['after']['goTest']  = array(
//
//    'class' => 'TestNewsApi',
//    'method' => 'goTestAfterHandler',
);

/* Функциональные политики */
$aConfig['policy'][] = array(
    'name'    => 'allowPageRead',
    'title'   => 'Разрешить чтение страницы',
    'default' => 1
);

$aConfig['policy'][] = array(
    'name'    => 'allowPageWrite',
    'title'   => 'Разрешить запись страницы',
    'default' => 1
);


/* Module css inclusions */

$aConfig['css'] = array(
    '/skewer_build/modules/Page/css/main.css'=> array('weight' => 1000),
    '/skewer_build/modules/Page/css/main.ie.css' => array('condition' => 'IE 7', 'layer' => Design::versionDefault),
    '/skewer_build/modules/Page/css/main.pda.css' => array('layer' => Design::versionPda, 'weight' => 1000),
);

// дополнитьельные css файлы
foreach ( Design::getVersionList() as $sVersion ) {
    $sFileName = Design::getAddCssFilePath($sVersion);
    if ( file_exists($sFileName) ) {
        $aConfig['css'][$sFileName] = array(
            'layer' => $sVersion
        );
    }
}

$aConfig['js'] = array(
    '/skewer_build/modules/Page/js/pageInit.js',
);
return $aConfig;