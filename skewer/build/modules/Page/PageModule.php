<?php
/**
 * Модуль построения раздела публичной части сайта
 * @class PageModule
 * @extends skModule
 * @project Skewer
 * @package kernel
 *
 * @author ArmiT, $Author: acat $
 * @version $Revision: 1437 $
 * @date $Date: 2012-12-19 14:36:22 +0400 (Ср., 19 дек. 2012) $
 *
 */
class PageModule extends skModule {
    /**
     * Id текущего раздела
     * @var int
     */
    public $pageId = 6;

    public function init() {

        //$this->addJSFile('pageInit.js');
        //$this->addJSFile('ie.js', 'IE');
        //$this->addCSSFile('main.css');
        //$this->addCSSFile('main.ie.css', 'IE 7');
        //skRequest::


    }// func

    /**
     * Исполнение модуля
     * @return int
     */
    public function execute() {

        $oParam = new Parameters();
        $this->pageId =  $this->getInt('pageId', $this->pageId);
        $this->setEnvParam('sectionId', $this->pageId);
        $aParamsList = $oParam->getAllSimple( $this->pageId, array('fields'=>array('value','show_val')));

        if ( Design::modeIsActive() ) {
            $this->setData('designMode', Design::getDirList() );
        }

        $this->setData('sectionId',$this->pageId);


        if(isSet($aParamsList['.']))
            if(count($aParamsList['.']))
                foreach($aParamsList['.'] as $sLabel=>$aValue)
                    $this->setData($sLabel, array('value' => $aValue['value'], 'text' => $aValue['show_val']));

        // Скрывам дату последнего обновления страницы если выставлена соответствующая галочка
        if ( isset($aParamsList['content']['hideDate']) && !$aParamsList['content']['hideDate'] ) {
            $tModify = strtotime( Tree::getModifiedDate($this->pageId));
            if ( $tModify>0 )
                $this->setData('modify_date', date('d-m-Y',$tModify));
        }

        // Обновляем текущий год
        if(isset($aParamsList['.']) and isset($aParamsList['.']['copyright']))
            $this->setData('copyright',array('value' => $aParamsList['.']['copyright']['value'], 'text' => str_replace('[Year]', date('Y'),$aParamsList['.']['copyright']['show_val'])));

        // SEO Метатеги для страницы
        $SEOData = $this->getEnvParam('SEOTemplates', array());
        $SEOData['page'] = array('title'=>'','description'=>'','keywords'=>'');
        if($aCurSEOData = SEOData::get('section',$this->pageId)){
            $SEOData['page']['title'] = $aCurSEOData['title'];
            $SEOData['page']['description'] = $aCurSEOData['description'];
            $SEOData['page']['keywords'] = $aCurSEOData['keywords'];
        }
        if( isset($aParamsList['.']['staticContent']) && $aParamsList['.']['staticContent']['show_val'] ){
            $SEOData['text'] = array('title'=>'','description'=>'','keywords'=>'');
            if($aCurSEOData = SEOData::get('section',$this->pageId)){
                $SEOData['text']['title'] = $aCurSEOData['title'];
                $SEOData['text']['description'] = $aCurSEOData['description'];
                $SEOData['text']['keywords'] = $aCurSEOData['keywords'];
            }
        }
        $this->setEnvParam('SEOTemplates', $SEOData);
        $SEOVars = $this->getEnvParam('SEOVars', array());
        $SEOVars['Название страницы'] = Tree::getTitleById($this->pageId);
        $SEOVars['название страницы'] = mb_strtolower($SEOVars['Название страницы']);
        // get path line
        $stopSections = Parameters::getByName($this->pageId,'pathLine','stopSections');
        if( $stopSections && isSet($stopSections['value']) )
            $stopSections = explode(',',$stopSections['value']);
        $tree = new Tree();
        $parents = $tree->getSectionParents($this->pageId, skConfig::get('section.sectionRoot'));
        $goodParents = array();
        if( count($parents) )
            while( $item = array_shift($parents) ){
                if( !in_array($item,$stopSections) ) array_unshift($goodParents, $item);
                else break;
            } // while
        $items = $tree->getByIdList($goodParents);
        $SEOVars['Цепочка разделов до главной'] = '';
        $SEOVars['Цепочка разделов до страницы'] = '';
        if( !empty($items) )
            foreach($items as &$item){
                $SEOVars['Цепочка разделов до главной'] .= ' '.$item['title'].'.';
                $SEOVars['Цепочка разделов до страницы'] = $item['title'].'. '.$SEOVars['Цепочка разделов до страницы'];
            } // foreach
        $SEOVars['цепочка разделов до главной'] = mb_strtolower($SEOVars['Цепочка разделов до главной']);
        $SEOVars['цепочка разделов до страницы'] = mb_strtolower($SEOVars['Цепочка разделов до страницы']);
        // Название сайта
        $aSiteName = Parameters::getByName(3,'.','site_name');
        $SEOVars['Название сайта'] = isSet($aSiteName['value']) ? $aSiteName['value'] : '';

        $this->setEnvParam('SEOVars', $SEOVars);

        /*
         * 
         *  Вставка CSS может подвергаться изменениям
         *
         */

        $sViewMode = $this->getViewMode();

        if ( $sViewMode=='pda' )
            /** @noinspection PhpUndefinedMethodInspection */
            if ( skProcessor::getEnvParam('sectionId')==skConfig::get('section.main') )
                $this->setData('show_search', 1);
            else $this->setData('back_href', 1);

        $aCSSFIles = skConfig::get('buildConfig.'.skProcessor::getLayerName().'.css.'.$sViewMode);
        $aJSFIles  = skConfig::get('buildConfig.'.skProcessor::getLayerName().'.js.'.$sViewMode);

        if($aCSSFIles)
            foreach($aCSSFIles as $sCondition=>$aFiles){
                skLinker::addCSSFile($aFiles, ($sCondition=='default')? false:$sCondition );
            }

        if($aJSFIles)
            foreach($aJSFIles as $sCondition=>$aFiles)
                skLinker::addJSFile($aFiles, ($sCondition=='default')? false:$sCondition );


        /* ------------------------------------------------------------------------------ */

        if(count($aParamsList)) {

            /* Обрабатываем шаблон расположения (layout) */


            if( skProcessor::getEnvParam('_viewMode') != 'pda' and
                isset($aParamsList['.layout']) and
                count($aParamsList['.layout']) ){

                $aZones = array();
                foreach($aParamsList['.layout'] as $sZone=>$sZoneVal){

                    if($sZone != '.title' and $sZoneVal){
                        $aZoneSource = explode(',', $sZoneVal);
                        foreach($aZoneSource as $label){

                            // добавляем инклуды объектных меток
                            if(isset($aParamsList[$label]['.layoutInclude']) and
                                $aParamsList[$label]['.layoutInclude']){

                                $aZones[$sZone][] = array(
                                    'templateFile' => trim($aParamsList[$label]['.layoutInclude']),
                                    'templateDir' => $this->getModuleDir().'templates/'

                                );

                            } // if

                            // обрабатываем прямую вставку
                            else {
                                $aZones[$sZone][] = trim($label);
                            } // else

                        } // foreach

                    }// if

                } // foreach

                $this->setData('.layout', $aZones);

            } // if


            // формирование дерева процессов //

            foreach($aParamsList as $sGroupName => $aParams) {
                if(isSet($aParams['object']) && $aParams['object']) {

                    /*Добавление процесса*/
                    $this->addChildProcess(new skContext( $sGroupName, $aParams['object'], ctModule, $aParams));
                }
            }// each

        } // if если есть параметры

        $sTplFile = $this->getData('templateFile');

        $sTplFile = (isSet($sTplFile['value']))? $sTplFile['value']: 'error.twig';

        if($version = $this->getStr('version') == 'print') $sTplFile = 'print.twig';

        $this->setData('SiteName', WEBROOTPATH);

        $this->setTemplate($sTplFile);
        return psComplete;
        
    }// func
}// class

?>
