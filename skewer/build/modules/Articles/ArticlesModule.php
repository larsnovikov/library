<?php

/**
 * Модуль системы статей
 * @class ArticlesModule
 * @extends skModule
 * @project Skewer
 * @package kernel
 *
 * @author kolesnikov, $Author: $
 * @version $Revision: $
 * @date $Date: $
 *
 */

class ArticlesModule extends skModule {

    public $parentSections;
    public $onPage = 10;
    public $listTemplate = 'list.twig';
    public $detailTemplate = 'detail_page.twig';
    public $titleOnMain = 'Новости';
    public $showFuture;
    public $showOnMain;  // отменяет фильтр по разделам
    public $allArticles;
    public $sortArticles;
    public $showList;
    // public $usePageLine = 1;

    private $iCurrentSection;

    public $aParameterList = array(
        'order'    => 'DESC',
        'future'   => '',
        'byDate'   => '',
        'on_page'  => '',
        'on_main'  => '',
        'section'  => '',
        'active'  => '',
        'all_articles' => '',
        'show_list'=> '',
    );

    public function init() {

        $this->setParser(parserTwig);
        $this->iCurrentSection = $this->getEnvParam('sectionId');
        $this->aParameterList['on_page'] = $this->onPage;

        if ( $this->allArticles ) $this->aParameterList['all_articles'] = 1;
        if ( $this->showOnMain ) $this->aParameterList['all_articles'] = 1;

        if ( $this->parentSections ) $this->aParameterList['section'] = $this->parentSections;
        else $this->aParameterList['section'] = $this->iCurrentSection;

        if ( $this->showFuture ) $this->aParameterList['future'] = 1;
        if ( $this->sortArticles ) $this->aParameterList['order'] = $this->sortArticles;

        if ( $this->showList ) $this->aParameterList['show_list'] = 1;

        return true;

    }// func

    public function execute() {

        $iPage       = $this->getInt('page', 1);
        $iArticlesId     = $this->getInt('articles_id', 0);
        $sArticlesAlias  = $this->getStr('articles-alias', '');
        $sDateFilter = $this->getStr('date');

        if ( !empty($sDateFilter) ) {
            $sDateFilter = date('Y-m-d', strtotime($sDateFilter));
            $this->aParameterList['byDate'] = $sDateFilter;
        }

        /*
         * Если в запросе передается ID или её алиас, значит необходимо вывести конкретную статью;
         * если нет - выводим список, разбитый по страницам.
         */
        if ( ($iArticlesId || $sArticlesAlias) && !$this->showList ) {

            $aArticlesItem = array();

            // Если есть ID - выбираем её по ID
            if ( $iArticlesId )
                $aArticlesItem = ArticlesApi::getArticlesById($iArticlesId);

            // Если есть алиас - выбираем по нему
            if ( $sArticlesAlias )
                $aArticlesItem = ArticlesApi::getArticlesByAlias($sArticlesAlias);

            $hideDate = Parameters::getByName($this->iCurrentSection,'content','hideDate');
            if( isset($hideDate['value']) && $hideDate['value'] )
                unset($aArticlesItem['last_modified_date']);

            if ( $aArticlesItem && sizeof($aArticlesItem) ){

                $this->setData('aArticlesItem', $aArticlesItem);

                // Убрать статический контент
                $oPage = $this->getProcess('out', psAll);
                if( $oPage->getStatus() != psComplete ) return psWait;
                $oPage->setData('staticContent', array());

                // Метатеги для списка новостей
                $SEOData = $this->getEnvParam('SEOTemplates', array());
                $SEOData['articlesDetail'] = array('title'=>'','description'=>'','keywords'=>'');
                if($aCurSEOData = SEOData::get('articles',$aArticlesItem['id'])){
                    $SEOData['articlesDetail']['title'] = $aCurSEOData['title'];
                    $SEOData['articlesDetail']['description'] = $aCurSEOData['description'];
                    $SEOData['articlesDetail']['keywords'] = $aCurSEOData['keywords'];
                }
                $this->setEnvParam('SEOTemplates', $SEOData);
                $SEOVars = $this->getEnvParam('SEOVars', array());
                $SEOVars['Название статьи'] = $aArticlesItem['title'];
                $SEOVars['название статьи'] = mb_strtolower($aArticlesItem['title']);
                $this->setEnvParam('SEOVars', $SEOVars);

                // добавляем элемент в pathline
                $oProcessPathLine = $this->getProcess('out.pathLine', psAll);
                if($oProcessPathLine instanceof skProcess){
                    $oProcessPathLine->setStatus(psNew);
                    $this->setEnvParam('pathline_additem',
                        array( 'id' => $aArticlesItem['id'],
                            'title' => $aArticlesItem['title'],
                            'alias_path' => $aArticlesItem['articles-alias'],
                            'link' => ''
                        )
                    );
                }
            }

            $this->setTemplate($this->detailTemplate);

        } else {

            // Получаем список новостей
            $aArticlesList = ArticlesApi::getArticlesList( $this->showList ? 1 : $iPage, $this->aParameterList );

            if ( sizeof($aArticlesList['items']) ){

                $this->setData('aArticlesList', $aArticlesList['items']);
                $this->setData('articles_section', $this->parentSections);

                $aURLParams = array();

                if(!empty($sDateFilter)) $aURLParams['date'] = $sDateFilter;

                $this->getPageLine($iPage, $aArticlesList['count'], $this->iCurrentSection, $aURLParams, array('onPage'=>$this->aParameterList['on_page']));
            }

            $this->setData('titleOnMain',$this->titleOnMain);

            $this->setTemplate($this->listTemplate);

        }

        /** @var skProcess $page  */
        // Откладываем запуск (перезапускаем) SEOMetatags после себя
        $page = $this->getProcess('out.SEOMetatags', psAll);
        if($page instanceof skProcess) $page->setStatus(psNew);

        return psComplete;
    }// func

    public function shutdown() {

    }// func
}// class
