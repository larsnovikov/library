<?php
/**
 * @class: ArticlesRouting
 *
 * @Author: kolesnikov, $Author: $
 * @version: $Revision: $
 * @date: $Date: $

 */
class ArticlesRouting implements skRoutingInterface {
    /**
     * Возвращает паттерны разбора URL
     * @static
     * @return bool | array
     */
    public static function getRoutePatterns() {

        return array(
            '/articles-alias/',
            '/articles_id(int)/',
            '/*page/page(int)/*date/date/',
            '/*date/date/',
            '/*page/page(int)/'
        );
    }// func
}
