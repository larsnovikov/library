<?php
/**
 *
 * @class ArticlesApi
 *
 * @author kolesnikov, $Author: $
 * @version $Revision: $
 * @date $Date: $
 * @package kernel
 */

class ArticlesApi {

    /**
     * Метод по выборке из базы конкретной новости по ID
     * @param $iArticlesId
     * @return array|bool
     */
    public static function getArticlesById($iArticlesId){

        $aItem = ArticlesMapper::getItem($iArticlesId);
        if ( isset($aItem['publication_date']) )
            $aItem['publication_date'] = date("d.m.Y", strtotime($aItem['publication_date']));

        $tModified = strtotime($aItem['last_modified_date']);
        if ( $tModified > 0 ) {

            $aItem['last_modified_date'] = date("d.m.Y H:i", $tModified);
        }
        else unset($aItem['last_modified_date']);

        return $aItem;
    }

    /**
     * @static
     * @param $sAlias
     * @return array
     */
    public static function getArticlesByAlias($sAlias){

        $aFilter = array(
            'where_condition' => array(
                'articles-alias' => array(
                    'sign' => '=',
                    'value' => $sAlias
                )
            )
        );

        $aItem = ArticlesMapper::getItem($aFilter);

        if( empty($aItem) ) return false;

        $aItem['publication_date'] = date("d.m.Y", strtotime($aItem['publication_date']));

        $tModified = strtotime($aItem['last_modified_date']);
        if ( $tModified > 0 ) {

            $aItem['last_modified_date'] = date("d.m.Y H:i", $tModified);
        }
        else unset($aItem['last_modified_date']);

        return $aItem;
    }

    /**
     * Метод по выборке списка новостей из базы
     * @static
     * @param $iPage
     * @param $aParameterList
     * @return array|bool
     */
    public static function getArticlesList($iPage, $aParameterList){

        // Собираем массив фильтра
        $aFilter = array(
            'select_fields' => array(
                'id',
                'articles-alias',
                'title',
                'author',
                'publication_date',
                'announce',
                'parent_section',
                'hyperlink'
            ),
            'where_condition' => array(/*
                'parent_section' => array(
                    'sign' => 'IN',
                    'value' => $aParameterList['section'],
                ),*/
                'active' => array(
                    'sign' => '=',
                    'value' => 1
                )

            ),
            'order' => array(
                'field' => 'publication_date',
                'way' => $aParameterList['order']
            ),
            'limit' => array(
                'start' => ($iPage-1)*$aParameterList['on_page'],
                'count' => $aParameterList['on_page']
            )
        );

        /* Если есть фильтр по дате */
        if(isSet($aParameterList['byDate']) && !empty($aParameterList['byDate']))
            $aFilter['where_condition']['publication_date'] = array(
                'sign' => 'BETWEEN',
                'value' => array(
                    $aParameterList['byDate'].' 00:00:00',
                    $aParameterList['byDate'].' 23:59:59',

                )
            );


        if ( !$aParameterList['all_articles'] && $aParameterList['section'])
            $aFilter['where_condition']['parent_section'] = array(
                'sign' => 'IN',
                'value' => $aParameterList['section']
            );

        if ( $aParameterList['future'] )
            $aFilter['where_condition']['publication_date'] = array(
                'sign' => '>',
                'value' => date('Y-m-d H:i:s', time())
            );

        $aItems = ArticlesMapper::getItems($aFilter);

        foreach( $aItems['items'] as &$aItem ){

            $aItem['publication_date'] = date("d.m.Y", strtotime($aItem['publication_date']));
        }

        return $aItems;

    }

    public static function getRSSData(){

        $aFilter = array(
            'order' => array(
                'field' =>
                'publication_date',
                'way' => 'DESC'
            ),
            'limit' => array(
                'start' => 0,
                'count' => 20
            )
        );

        $aItems = ArticlesMapper::getItems($aFilter);

        foreach ( $aItems['items'] as &$aValue ){

            $sLink = ( !empty($aValue['articles-alias']) )? skRouter::rewriteURL('['.$aValue['parent_section'].'][ArticlesModule?articles-alias='.$aValue['articles-alias'].']'): skRouter::rewriteURL('['.$aValue['parent_section'].'][ArticlesModule?articles_id='.$aValue['id'].']');
            $aValue['title'] = strip_tags($aValue['title']);
            $aValue['title'] = htmlspecialchars($aValue['title']);
            $aValue['publication_date'] = date('r', strtotime($aValue['publication_date']));
            $aValue['unix_time'] = strtotime($aValue['publication_date']);
            $aValue['link'] = $_SERVER['HTTP_HOST'].$sLink;
        }

        return $aItems;
    }

    /**
     * Удаляет события из раздела $iSectionId
     * @static
     * @param $iSectionId
     * @return bool|int
     */
    public static function removeFromSection($iSectionId) {

        if(!(int)$iSectionId) return false;

        return ArticlesMapper::removeFromSection($iSectionId);

    }// func

}

