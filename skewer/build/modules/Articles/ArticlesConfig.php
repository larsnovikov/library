<?php

/* main */
$aConfig['name']     = 'Articles';
$aConfig['version']  = '1.000a';
$aConfig['title']    = 'Статьи';
$aConfig['description']  = 'Модуль статей';
$aConfig['revision'] = '0001';
$aConfig['layer']     = 'Page';


/* Функциональные политики */
$aConfig['policy'][] = array(
    'name'    => 'allowArticlesListRead',
    'title'   => 'Разрешить чтение списка статей',
    'default' => 1
);

$aConfig['policy'][] = array(
    'name'    => 'allowArticlesDetailRead',
    'title'   => 'Разрешить чтение детальной статей',
    'default' => 1
);

$aConfig['hooks']['after']  = array();

/* Module clientside inclusions */

$aConfig['css'] = array(
    //'/skewer_build/modules/News/css/news.css',
);

$aConfig['js'] = array();

return $aConfig;