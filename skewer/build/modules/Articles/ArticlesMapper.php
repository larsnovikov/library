<?php

/**
 * @class ArticlesMapper
 * @extends skModule
 * @project Skewer
 * @package kernel
 *
 * @author kolesnikov, $Author: $
 * @version $Revision: $
 * @date $Date: $
 *
 */

class ArticlesMapper extends skMapperPrototype{

    /**
     * @var string Имя таблицы, с которой работает маппер
     */
    protected  static $sCurrentTable = 'articles';

    /**
     * @static Метод возвращает имя рабочей таблицы
     * @return string
     */
    public static function getCurrentTable() {

        return self::$sCurrentTable;
    }

    /**
     * @static Метод возвращает список полей рабочей таблицы
     * @return array
     */
    public static function getParametersList() {

        return self::$aParametersList;
    }

    /**
     * @var array Конфигурация полей таблицы
     */
    protected static $aParametersList = array(
        'id' => 'i:hide:Номер',
        'articles-alias' => 's:str:Псевдоним',
        'parent_section' => 'i:hide:Родительский раздел',
        'author' => 's:str:Автор',
        'publication_date' => 's:date:Дата публикации',
        'publication_time' => 's:time:Время публикации',
        'title' => 's:str:Название',
        'announce' => 's:html:Анонс',
        'full_text' => 's:html:Полный текст',
        'active' => 'i:check:Активность',
        'hyperlink' => 's:str:Ссылка',
        'last_modified_date' => 's:hide:Дата последнего изменения'
    );

    // дополнительный набор параметров
    protected static function getAddParamList() {
        return array(
            'id' => array(
                'view' => 'hide',
                'listColumns' => array(
                    'hidden' => true
                )
            ),
            'publication_date' => array(
                'listColumns' => array( 'hidden' => true )
            ),
            'publication_show_val' => array(
                'listColumns' => array( 'text' => 'Дата публикации' )
            ),
            'title' => array(
                'listColumns' => array( 'flex' => 1 )
            ),
            'active' => array(
                'listColumns' => array( 'width' => 75 )
            ),
            'announce' => array(
                'view' => 'wyswyg'
            ),
            'full_text' => array(
                'view' => 'wyswyg',
                'height' => 600
            )
        );
    }

    /**
     * @static Метод для выборки списка новостей по фильтру
     * @param $aFilter
     * @return array|bool
     */
    public static function getArticles($aFilter){

        return static::getItems($aFilter);
    }//function getNews()

    /**
     * @static Метод для выборки одной новости
     * @param $aFilter
     * @return array
     */
    public static function getArticlesById($aFilter){

        // Получаем новость
        $aItem = self::getItems($aFilter);

        return ( sizeof($aItem) )? $aItem['items'][0]: array();
    }//function getNewsById()

    public static function removeFromSection($iSectionId) {

        global $odb;

        $sQuery = '
            DELETE
            FROM `[table_name:q]`
            WHERE `parent_section`=[section:i];
        ';

        return $odb->query($sQuery, array(
            'table_name' => static::getCurrentTable(),
            'section' => $iSectionId,
        ));

    }// func

    /**
     * @static
     * @param $sAlias
     * @return mixed
     */
    public static function checkAlias($sAlias){

        global $odb;

        $aData = array('alias' => $sAlias);

        $sQuery = "
            SELECT
                `id`
            FROM
                `news`
            WHERE
                `news-alias` = [alias:s];";

        $rResult = $odb->query($sQuery, $aData);

        $aItems = array();

        while( $aRow = $rResult->fetch_assoc() ){

            $aItems[$aRow['id']] = $aRow['id'];
        }

        return $aItems;
    }

}//class
?>