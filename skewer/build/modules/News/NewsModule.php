<?php

/**
 * Модуль новостной системы
 * @class NewsModule
 * @extends skModule
 * @project Skewer
 * @package kernel
 *
 * @author Andy Mitrich, $Author: acat $
 * @version $Revision: 1340 $
 * @date $Date: 2012-11-30 15:30:03 +0400 (Пт., 30 нояб. 2012) $
 *
 */

class NewsModule extends skModule {

    public $parentSections;
    public $onPage = 10;
    public $listTemplate = 'list.twig';
    public $detailTemplate = 'detail_page.twig';
    public $titleOnMain = 'Новости';
    public $showArchive;
    public $showFuture;
    public $showOnMain;  // отменяет фильтр по разделам
    public $allNews;
    public $sortNews;
    public $showList;
   // public $usePageLine = 1;
    
    private $iCurrentSection;

    public $aParameterList = array(
        'order'    => 'DESC',
        'future'   => '',
        'byDate'   => '',
        'on_page'  => '',
        'on_main'  => '',
        'section'  => '',
        'archive'  => '',
        'all_news' => '',
        'show_list'=> '',
    );

    public function init() {

        $this->setParser(parserTwig);
        $this->iCurrentSection = $this->getEnvParam('sectionId');
        $this->aParameterList['on_page'] = $this->onPage;

        if ( $this->allNews ) $this->aParameterList['all_news'] = 1;
        if ( $this->showOnMain ) $this->aParameterList['all_news'] = 1;

        if ( $this->parentSections ) $this->aParameterList['section'] = $this->parentSections;
        else $this->aParameterList['section'] = $this->iCurrentSection;

        if ( $this->iCurrentSection==skConfig::get('section.main') ) $this->aParameterList['on_main'] = 1;
        if ( $this->showArchive ) $this->aParameterList['archive'] = 1;
        if ( $this->showFuture ) $this->aParameterList['future'] = 1;
        if ( $this->sortNews ) $this->aParameterList['order'] = $this->sortNews;

        if ( $this->showList ) $this->aParameterList['show_list'] = 1;

        return true;

    }// func

    public function execute() {

        $iPage       = $this->getInt('page', 1);
        $iNewsId     = $this->getInt('news_id', 0);
        $sNewsAlias  = $this->getStr('news-alias', '');
        $sDateFilter = $this->getStr('date');

        if ( !empty($sDateFilter) ) {
            $sDateFilter = date('Y-m-d', strtotime($sDateFilter));
            $this->aParameterList['byDate'] = $sDateFilter;
        }

        /*
         * Если в запросе передается ID новости или её алиас, значит необходимо вывести конкретную новость;
         * если нет - выводим список новостей, разбитый по страницам.
         */
        if ( ($iNewsId || $sNewsAlias) && !$this->showList ) {

            $aNewsItem = array();

            // Если есть ID новости - выбираем её по ID
            if ( $iNewsId )
                $aNewsItem = NewsApi::getNewsById($iNewsId);

            // Если есть алиас новости - выбираем по нему
            if ( $sNewsAlias )
                $aNewsItem = NewsApi::getNewsByAlias($sNewsAlias);

//            $hideDate = Parameters::getByName($this->iCurrentSection,'content','hideDate');
//            if( isset($hideDate['value']) && $hideDate['value'] )
                unset($aNewsItem['last_modified_date']);

            if ( $aNewsItem && sizeof($aNewsItem) ){

                $this->setData('aNewsItem', $aNewsItem);

                // Убрать статический контент
                $oPage = $this->getProcess('out', psAll);
                if( $oPage->getStatus() != psComplete ) return psWait;
                $oPage->setData('staticContent', array());

                // Метатеги для списка новостей
                $SEOData = $this->getEnvParam('SEOTemplates', array());
                $SEOData['newsDetail'] = array('title'=>'','description'=>'','keywords'=>'');
                if($aCurSEOData = SEOData::get('news',$aNewsItem['id'])){
                    $SEOData['newsDetail']['title'] = $aCurSEOData['title'];
                    $SEOData['newsDetail']['description'] = $aCurSEOData['description'];
                    $SEOData['newsDetail']['keywords'] = $aCurSEOData['keywords'];
                }
                $this->setEnvParam('SEOTemplates', $SEOData);
                $SEOVars = $this->getEnvParam('SEOVars', array());
                $SEOVars['Название новости'] = $aNewsItem['title'];
                $SEOVars['название новости'] = mb_strtolower($aNewsItem['title']);
                $this->setEnvParam('SEOVars', $SEOVars);

                // добавляем элемент в pathline
                $oProcessPathLine = $this->getProcess('out.pathLine', psAll);
                if($oProcessPathLine instanceof skProcess){
                    $oProcessPathLine->setStatus(psNew);
                    $this->setEnvParam('pathline_additem',
                        array( 'id' => $aNewsItem['id'],
                            'title' => $aNewsItem['title'],
                            'alias_path' => $aNewsItem['news-alias'],
                            'link' => ''
                        )
                    );
                }
            }

            $this->setTemplate($this->detailTemplate);

        } else {

            // Получаем список новостей
            $aNewsList = NewsApi::getNewsList( $this->showList ? 1 : $iPage, $this->aParameterList );

            if ( sizeof($aNewsList['items']) ){

                $this->setData('aNewsList', $aNewsList['items']);
                $this->setData('news_section', $this->parentSections);

                $aURLParams = array();

                if(!empty($sDateFilter)) $aURLParams['date'] = $sDateFilter;

                $this->getPageLine($iPage, $aNewsList['count'], $this->iCurrentSection, $aURLParams, array('onPage'=>$this->aParameterList['on_page']));
            }

            $this->setData('titleOnMain',$this->titleOnMain);

            $this->setTemplate($this->listTemplate);

        }

        /** @var skProcess $page  */
        // Откладываем запуск (перезапускаем) SEOMetatags после себя
        $page = $this->getProcess('out.SEOMetatags', psAll);
        if($page instanceof skProcess) $page->setStatus(psNew);

        return psComplete;
    }// func

    public function shutdown() {

    }// func
}// class
