<?php
/**
 * @class: NewsRouting
 *
 * @Author: ArmiT, $Author: sapozhkov $
 * @version: $Revision: 6 $
 * @date: $Date: 2012-05-18 10:52:53 +0400 (Пт., 18 мая 2012) $

 */
class NewsRouting implements skRoutingInterface {
    /**
     * Возвращает паттерны разбора URL
     * @static
     * @return bool | array
     */
    public static function getRoutePatterns() {

        return array(
                '/news-alias/',
                '/news_id(int)/',
                '/*page/page(int)/*date/date/',
                '/*date/date/',
                '/*page/page(int)/'
        );
    }// func
}// class
