<?php
/**
 * 
 * @class NewsApi
 *
 * @author ArmiT, $Author: acat $
 * @version $Revision: 1464 $
 * @date $Date: 2012-12-20 18:26:11 +0400 (Чт., 20 дек. 2012) $
 * @project JetBrains PhpStorm
 * @package kernel
 */

class NewsApi {

    /**
     * Метод по выборке из базы конкретной новости по ID
     * @param $iNewsId
     * @return array|bool
     */
    public static function getNewsById($iNewsId){

        $aItem = NewsMapper::getItem($iNewsId);
        if ( isset($aItem['publication_date']) )
            $aItem['publication_date'] = date("d.m.Y", strtotime($aItem['publication_date']));

        $tModified = strtotime($aItem['last_modified_date']);
        if ( $tModified > 0 ) {

            $aItem['last_modified_date'] = date("d.m.Y H:i", $tModified);
        }
        else unset($aItem['last_modified_date']);

        return $aItem;
    }//function getNewsById()

    /**
     * @static
     * @param $sAlias
     * @return array
     */
    public static function getNewsByAlias($sAlias){

        $aFilter = array(
            'where_condition' => array(
                'news-alias' => array(
                    'sign' => '=',
                    'value' => $sAlias
                )
            )
        );



        $aItem = NewsMapper::getItem($aFilter);

        if( empty($aItem) ) return false;

        $aItem['publication_date'] = date("d.m.Y", strtotime($aItem['publication_date']));

        $tModified = strtotime($aItem['last_modified_date']);
        if ( $tModified > 0 ) {

            $aItem['last_modified_date'] = date("d.m.Y H:i", $tModified);
        }
        else unset($aItem['last_modified_date']);

        return $aItem;
    }

    /**
     * Метод по выборке списка новостей из базы
     * @static
     * @param $iPage
     * @param $aParameterList
     * @return array|bool
     */
    public static function getNewsList($iPage, $aParameterList){

        // Собираем массив фильтра
        $aFilter = array(
            'select_fields' => array(
                'id',
                'news-alias',
                'title',
                'publication_date',
                'announce',
                'parent_section',
                'full_text',
                'hyperlink'
            ),
            'where_condition' => array(/*
                'parent_section' => array(
                    'sign' => 'IN',
                    'value' => $aParameterList['section'],
                ),*/
                'active' => array(
                    'sign' => '=',
                    'value' => 1
                )

            ),
            'order' => array(
                'field' => 'publication_date',
                'way' => $aParameterList['order']
            ),
            'limit' => array(
                'start' => ($iPage-1)*$aParameterList['on_page'],
                'count' => $aParameterList['on_page']
            )
        );

        /* Если есть фильтр по дате */
        if(isSet($aParameterList['byDate']) && !empty($aParameterList['byDate']))
            $aFilter['where_condition']['publication_date'] = array(
                'sign' => 'BETWEEN',
                'value' => array(
                    $aParameterList['byDate'].' 00:00:00',
                    $aParameterList['byDate'].' 23:59:59',

                    )
            );

        if ( $aParameterList['on_main'] || $aParameterList['archive'] ){

            if( $aParameterList['on_main'] )
                $aFilter['where_condition']['on_main'] = array(
                        'sign' => '=',
                        'value' => 1
                    );

            if( $aParameterList['archive'] )
                $aFilter['where_condition']['archive'] = array(
                        'sign' => '=',
                        'value' => 1
                    );
        }
        else
            if ( !$aParameterList['all_news'] && $aParameterList['section'])
                $aFilter['where_condition']['parent_section'] = array(
                    'sign' => 'IN',
                    'value' => $aParameterList['section']
                );

        if ( $aParameterList['future'] )
            $aFilter['where_condition']['publication_date'] = array(
                'sign' => '>',
                'value' => date('Y-m-d H:i:s', time())
            );

        $aItems = NewsMapper::getItems($aFilter);

        foreach( $aItems['items'] as &$aItem ){

            $aItem['publication_date'] = date("d.m.Y", strtotime($aItem['publication_date']));
        }

        return $aItems;

    }//function getNewsList()

    public static function getRSSData(){

        $aFilter = array(
            'order' => array(
                'field' =>
                'publication_date',
                'way' => 'DESC'
            ),
            'limit' => array(
                'start' => 0,
                'count' => 20
            )
        );

        $aItems = NewsMapper::getItems($aFilter);

        foreach ( $aItems['items'] as &$aValue ){

            $sLink = ( !empty($aValue['news-alias']) )? skRouter::rewriteURL('['.$aValue['parent_section'].'][NewsModule?news-alias='.$aValue['news-alias'].']'): skRouter::rewriteURL('['.$aValue['parent_section'].'][NewsModule?news_id='.$aValue['id'].']');
            $aValue['title'] = strip_tags($aValue['title']);
            $aValue['title'] = htmlspecialchars($aValue['title']);
            $aValue['publication_date'] = date('r', strtotime($aValue['publication_date']));
            $aValue['unix_time'] = strtotime($aValue['publication_date']);
            $aValue['link'] = $_SERVER['HTTP_HOST'].$sLink;
        }

        return $aItems;
    }

    /**
     * Удаляет события из раздела $iSectionId
     * @static
     * @param $iSectionId
     * @return bool|int
     */
    public static function removeFromSection($iSectionId) {

        if(!(int)$iSectionId) return false;

        return NewsMapper::removeFromSection($iSectionId);

    }// func

}
