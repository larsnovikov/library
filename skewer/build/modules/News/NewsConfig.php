<?php

/* main */
$aConfig['name']     = 'News';
$aConfig['version']  = '1.000a';
$aConfig['title']    = 'Новости';
$aConfig['description']  = 'Модуль новостной системы';
$aConfig['revision'] = '0001';
$aConfig['layer']     = 'Page';

/* Module hooks */
//$aConfig['hooks']['after']['removeSection']  = array(
//
//    'class' => 'NewsApi',
//    'method' => 'removeFromSection',
//);


/* Функциональные политики */
$aConfig['policy'][] = array(
    'name'    => 'allowNewsListRead',
    'title'   => 'Разрешить чтение списка новостей',
    'default' => 1
);

$aConfig['policy'][] = array(
    'name'    => 'allowNewsDetailRead',
    'title'   => 'Разрешить чтение детальной новости',
    'default' => 1
);

$aConfig['hooks']['after']  = array();

/* Module clientside inclusions */

$aConfig['css'] = array(
    //'/skewer_build/modules/News/css/news.css',
);

$aConfig['js'] = array();

return $aConfig;