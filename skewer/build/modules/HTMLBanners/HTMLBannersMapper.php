<?php

/**
 * @class HTMLBannersMapper
 * @extends skModule
 * @project Skewer
 * @package kernel
 *
 * @author Andy Mitrich, $Author: sokol $
 * @version $Revision: 532 $
 * @date 19.12.11 15:03 $
 *
 */

class HTMLBannersMapper extends skMapperPrototype {

    /**
     * @var string Имя таблицы, с которой работает маппер
     */
    protected  static $sCurrentTable = 'banners';

    /**
     * @static Метод возвращает имя рабочей таблицы
     * @return string
     */
    public static function getCurrentTable() {

        return self::$sCurrentTable;
    }// function getCurrentTable()

    /**
     * @var array Конфигурация полей таблицы
     */
    protected static $aParametersList = array(
        'id' => 'i:hide:Номер баннера',
        'title' => 's:str:Название',
        'content' => 's:wyswyg:Текст баннера',
        'active' => 'i:check:Активность',
        'on_main' => 'i:check:На главной',
        'on_allpages' => 'i:check:На всех страницах',
        'on_include' => 'i:check:На внутренних страницах',
        'location' => 's:str:Позиция',
        'locationTitle' => 's:str:Позиция',
        'section' => 'i:str:Раздел для показа',
        'sort' => 'i:str:Порядок',
    );

    /**
     * @static Выбираем баннеры для внутренних страниц
     * Условие, по которому баннер выводится на внутренней странице содержит в себе следующие составные части:
     * 1. Активность баннера - баннер должен быть активным
     * 2. Или раздел вывода, определенный для баннера должен совпадать с текущим разделом
     *    или должен стоять флаг "Выводить на внутренних страницах" и раздел вывода, указанный для баннера, должен быть в числе родительских для текущего раздела
     *    или должен стоять флаг "Выводить на всех страницах"
     * 3. Конкретная заданная позиция вывода, для которой выбираются баннеры
     * @param $aParams
     * @return array
     */
    public static function getBannersOnInternal($aParams){

        global $odb;

        $aData = array(
            'location' => $aParams['location'],
            'current_section' => $aParams['current_section'],
            'parent_sections' => $aParams['parent_sections']
        );

        $sQuery = "
            SELECT
                `title`,
                `content`
            FROM
                `banners`
            WHERE
                `active` = 1 AND
                ( `section` = [current_section:i] OR
                ( `on_include` = 1 AND `section` IN ([parent_sections:q]) ) OR
                `on_allpages` = 1 ) AND
                `location` LIKE [location:s]
            ORDER BY
                `sort`;";

        $rResult = $odb->query($sQuery, $aData);

        $aBanners = array();
        while( $aRow = $rResult->fetch_array(MYSQLI_ASSOC) ){
            $aBanners[] = $aRow;
        }

        return $aBanners;
    }// function getBannersOnInternal()

    /**
     * @static Выбираем баннеры на главную страницу
     * Условие, по которому баннер выводится на главную страницу содержит в себе следующие составные части:
     * 1. Активность баннера - баннер должен быть активным
     * 2. Или определенный для баннера раздел вывода должен совпадать с текущим разделом, который по факту является главной страницей
     *    или должен стоять флаг "выводить на главную"
     * 3. Конкретная заданная позиция вывода, для которой выбираются баннеры
     * @param $aParams Входные параметры
     * @return array
     */
    public static function getBannersOnMain($aParams){

        global $odb;

        $aData = array(
            'location' => $aParams['location'],
            'current_section' => $aParams['current_section']
        );

        $sQuery = "
            SELECT
               `title`,
               `content`
            FROM
                `banners`
            WHERE
                `active` <> 0 AND
                ( `section` = [current_section:i] OR `on_main`=1 ) AND
                `location` LIKE [location:s]
            ORDER BY
                `sort`;";

        $rResult = $odb->query($sQuery, $aData);

        // Собираем результирующий массив
        $aBanners = array();
        while( $aRow = $rResult->fetch_array(MYSQLI_ASSOC) ){
            $aBanners[] = $aRow;
        }

        return $aBanners;
    }// function getBannersOnMain()

    /**
     * @static Метод сохранения баннера
     * @param $aInputData
     * @return bool
     */
    public static function updBanner($aInputData){

        // Если во входящем массиве с данными нет значения сортироки или оно равно нулю - получаем максимальный порядок для текущей позиции
        if ( !isset($aInputData['sort']) || !$aInputData['sort'] )
            $aInputData['sort'] = self::getMaxOrder($aInputData['location'])+1;

        // Сохраняем баннер
        return self::saveItem($aInputData);
    }// function updBanner()

    /**
     * @static Метод для получения максимального значения поля "sort" для текущей позиции
     * @param $sLocation Позиция, для которой выбирам максимальный порядок
     * @return mixed
     */
    public static function getMaxOrder($sLocation){

        global $odb;

        $aData = array('location'=>$sLocation);

        $sQuery = "
            SELECT
                MAX(`sort`) as sort
            FROM
                `banners`
            WHERE
                `location` LIKE [location:s];";

        $aValue = $odb->getRow($sQuery, $aData);

        return $aValue['sort'];
    }// function getMaxOrder()

    protected static function getAddParamList(){

        return array(
            'id' => array(
                'view' => 'hide',
                'listColumns' => array(
                    'hidden' => true
                )
            ),
            'location' => ExtForm::getDesc4SelectFromArray(HTMLBannersAdmApi::getBannerLocations()),
            'section' => ExtForm::getDesc4SelectFromArray(HTMLBannersAdmApi::getSectionTitle()),
            'title' => array(
                'listColumns' => array( 'flex' => 1 )
            ),
            'on_main' => array(
                'listColumns' => array( 'width' => 75 )
            ),
            'archive' => array(
                'listColumns' => array( 'width' => 75 )
            ),
            'on_allpages' => array(
                'listColumns' => array( 'width' => 75 )
            ),
            'on_include' => array(
                'listColumns' => array( 'width' => 75 )
            )

        );
    }// function getAddParamList()


}// class
