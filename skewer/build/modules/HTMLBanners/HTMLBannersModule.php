<?php

/**
 * @class HTMLBannersModule
 * @extends skModule
 * @project Skewer
 * @package kernel
 *
 * @author Andy Mitrich, $Author: armit $
 * @version $Revision: 249 $
 * @date 19.12.11 13:29 $
 *
 */

class HTMLBannersModule extends skModule {

    public $Location = 'left';
    /* @var null|\HTMLBannersApi */
    public $oBannerApi = NULL;
    private $sFileTemplate;
    public $SectionId = 0;
    /**
     * @var int ID главной страницы
     */
    private $defaultSection = 0;

    public function init(){

        $this->oBannerApi = new HTMLBannersApi();
        $this->setParser(parserTwig);
        $this->SectionId = $this->getEnvParam('sectionId');
        $this->defaultSection = skConfig::get('section.main');
        $this->sFileTemplate = 'banner_'.$this->Location.'.twig';
    }

    public function execute(){

        $aParams = array();
        $aParams['location'] = $this->Location;
        $aParams['current_section'] = $this->SectionId;

        if ( $this->SectionId == $this->defaultSection ){
            $aBanners = $this->oBannerApi->getBannersOnMain($aParams);
        }

        else{

            $oTree = new Tree();
            $aParentSections = $oTree->getSectionParents($this->SectionId);
            if ( $aParentSections )
                $aParams['parent_sections'] = implode(',', $aParentSections);
            else $aParams['parent_sections'] = $this->SectionId;

            $aBanners = $this->oBannerApi->getBannersOnInternal($aParams);
        }

        if ( $aBanners )
            $this->setData('aBanners', $aBanners);

        $this->setTemplate($this->sFileTemplate);

        return psComplete;
    }

    public function shutdown(){

    }

}//class