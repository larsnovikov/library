<?php

/* main */
$aConfig['name']     = 'HTMLBanners';
$aConfig['title']    = 'Баннерная система';
$aConfig['version']  = '1.000a';
$aConfig['description']  = 'Модуль баннерной системы';
$aConfig['revision'] = '0001';
$aConfig['layer']     = 'Page';

/* Module css inclusions */
$aConfig['css'] = array();

$aConfig['js'] = array();

return $aConfig;