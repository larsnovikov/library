<?php

/**
 * @class HTMLBannersApi
 * @extends skModule
 * @project Skewer
 * @package kernel
 *
 * @author Andy Mitrich, $Author: sapozhkov $
 * @version $Revision: 6 $
 * @date 19.12.11 13:47 $
 *
 */

class HTMLBannersApi {

    public function getBannersOnMain( $aParams ){

        return HTMLBannersMapper::getBannersOnMain($aParams);
    }// function getBannersByFilter()

    public function getBannersOnInternal( $aParams ){

        return HTMLBannersMapper::getBannersOnInternal($aParams);
    }// function getBannersByFilter()

}// class
