<?php
/**
 *
 * @class SEOMetatagsModule
 *
 * @author kolesnikov, $Author: sapozhkov $
 * @version $Revision: 1346 $
 * @date $Date: 2012-12-03 11:31:54 +0400 (Пн., 03 дек. 2012) $
 * @project Skewer
 * @package kernel
 */
class SEOMetatagsModule extends skModule {


    /**
     * Первичная инициализация
     */
    public function init(){

    }

    /**
     * Выполнение модуля
     * @return int
     */
    public function execute() {

        $page = $this->getProcess('out', psAll);
        if($page->getStatus() != psComplete) return psWait;


        $SEOData = $this->getEnvParam('SEOTemplates',false);
        $SEOVars = $this->getEnvParam('SEOVars',false);

        $aTpls = array_keys($SEOData);
        $sTpl = 'page';
        if(in_array('text',$aTpls)) $sTpl = 'text';
        foreach(array('galleryDetail','newsDetail','eventsDetail','articlesDetail') as $sCurTpl)
            if(in_array($sCurTpl,$aTpls)) $sTpl = $sCurTpl;

        if( $SEOData[$sTpl] ) { //var_dump($sTpl);

            $aMainSEOTpl = SEOTemplatesToolApi::getItemByName('page');
            $aSEOTpl = SEOTemplatesToolApi::getItemByName($sTpl);
            if( !is_array($aSEOTpl) || !count($aSEOTpl) )
                $aSEOTpl = false;

            if( isset($SEOData[$sTpl]['title']) && $SEOData[$sTpl]['title'] ){
                $page->setData('SEOTitle', $SEOData[$sTpl]['title']);
            } elseif( $aSEOTpl['title'] ) {
                $sCur = SEOTemplatesToolApi::parseSEOTpl($aSEOTpl['title'],$SEOVars);
                $page->setData('SEOTitle', $sCur);
            } elseif( $aMainSEOTpl['title'] ) {
                $sCur = SEOTemplatesToolApi::parseSEOTpl($aMainSEOTpl['title'],$SEOVars);
                $page->setData('SEOTitle', $sCur);
            }

            if( isset($SEOData[$sTpl]['description']) && $SEOData[$sTpl]['description'] ){
                $page->setData('SEODescription', $SEOData[$sTpl]['description']);
            } elseif( $aSEOTpl['description'] ) {
                $sCur = SEOTemplatesToolApi::parseSEOTpl($aSEOTpl['description'],$SEOVars);
                $page->setData('SEODescription', $sCur);
            } elseif( $aMainSEOTpl['description'] ) {
                $sCur = SEOTemplatesToolApi::parseSEOTpl($aMainSEOTpl['description'],$SEOVars);
                $page->setData('SEODescription', $sCur);
            }

            if( isset($SEOData[$sTpl]['keywords']) && $SEOData[$sTpl]['keywords'] ){
                $page->setData('SEOKeywords', $SEOData[$sTpl]['keywords']);
            } elseif( $aSEOTpl['keywords'] ) {
                $sCur = SEOTemplatesToolApi::parseSEOTpl($aSEOTpl['keywords'],$SEOVars);
                $page->setData('SEOKeywords', $sCur);
            } elseif( $aMainSEOTpl['keywords'] ) {
                $sCur = SEOTemplatesToolApi::parseSEOTpl($aMainSEOTpl['keywords'],$SEOVars);
                $page->setData('SEOKeywords', $sCur);
            }

        }

        return psRendered;
    }// func


}
