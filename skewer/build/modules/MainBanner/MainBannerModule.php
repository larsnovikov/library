<?php
/**
 * @class MainBannerModule
 * @extends skModule
 * @project Skewer
 * @package kernel
 *
 * @author kolesnikov, $Author: $
 * @version $Revision: $
 * @date $Date: $
 *
 */
class MainBannerModule extends skModule {

    private $iCurrentSection;


    public function init() {

        $this->setParser(parserTwig);

        $this->iCurrentSection = $this->getEnvParam('sectionId');

    }


    public function execute() {

        /* получаем список доступных баннеров */
        $aBanners = MainBanner2BannersAdmApi::getBannersListForSection( $this->iCurrentSection );

        /* выбираем баннер */
        $iBannerCount = count($aBanners);

        if( !$iBannerCount )
            return psComplete;

        // todo написать ротацию на сессии
        $iBannerId = rand(0,$iBannerCount-1);

        $aCurrentBanner = $aBanners[$iBannerId];
        $iBannerId = $aBanners[$iBannerId]['id'];

        /* получаем список слайдов выбранного банера */
        $aSlides = MainBanner2SlidesAdmMapper::getSlidesForBanner($iBannerId);


        /* вывод баннера */
        if( count($aSlides) == 1 ) {

            $this->setData('image', $aSlides[0]);
            $this->setTemplate('image.twig');

        } elseif ( count($aSlides) > 1 ) {

            $aBannerTools = MainBanner2BannersAdmApi::getAllTools();
            $aBannerTools['bullet'] = $aCurrentBanner['bullet'];
            $aBannerTools['scroll'] = $aCurrentBanner['scroll'];

            $this->setData('tools', $aBannerTools);

            $this->setData('banner', $aSlides);

            $this->setTemplate('banner.twig');

            // add CSS
            $this->addCSSFile('bjqs.css');

            // add JavaScript
            $this->addJSFile('bjqs-1.3.min.js');
            $this->addJSFile('initBanner.js');
        }


        return psComplete;
    }

}
