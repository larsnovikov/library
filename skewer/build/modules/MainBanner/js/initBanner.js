jQuery(document).ready(function($) {

    var mb_scroll = $('#js_main_banner').attr('mb_scroll') == '1';
    var mb_bullet = $('#js_main_banner').attr('mb_bullet') == '1';
    var mb_next_button = 'Next';
    var mb_prev_button = 'Prev';
    var animtype = $('#js_main_banner').attr('animtype');
    var animduration = $('#js_main_banner').attr('animduration');
    var animspeed = $('#js_main_banner').attr('animspeed');
    var hoverpause = $('#js_main_banner').attr('hoverpause');
    var banner_w = $('#js_main_banner').attr('banner_w');
    var banner_h = $('#js_main_banner').attr('banner_h');


    $('#js_main_banner').bjqs({
        'animtype'      : animtype,
        'height' : banner_h,
        'width' : banner_w,
        'responsive' : true,

        animduration : animduration,
        animspeed : animspeed,

        hoverpause : hoverpause,

        showcontrols : mb_scroll,
        nexttext : mb_next_button,
        prevtext : mb_prev_button,
        showmarkers : mb_bullet,
        centermarkers : false

    });

});