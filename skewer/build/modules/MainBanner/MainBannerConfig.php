<?php

/* main */
$aConfig['name']     = 'MainBanner';
$aConfig['version']  = '1.000a';
$aConfig['title']    = 'Баннер в шапке';
$aConfig['description']  = 'Баннер в шапке';
$aConfig['revision'] = '0001';
$aConfig['layer']     = 'Page';


/* Функциональные политики */
//$aConfig['policy'][] = array(
//    'name'    => 'allowArticlesListRead',
//    'title'   => 'Разрешить чтение списка статей',
//    'default' => 1
//);
//
//$aConfig['policy'][] = array(
//    'name'    => 'allowArticlesDetailRead',
//    'title'   => 'Разрешить чтение детальной статей',
//    'default' => 1
//);

$aConfig['hooks']['after']  = array();

/* Module clientside inclusions */


return $aConfig;