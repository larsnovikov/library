<?php

/**
 * @class GuestBookMapper
 * @extends skModule
 * @project Skewer
 * @package kernel
 *
 * @author kolesnikov, $Author: $
 * @version $Revision: $
 * @date 01.02.12 12:56 $
 *
 */

class GuestBookMapper extends skMapperPrototype {

    /** статус "новый" */
    const statusNew = 0;

    /** статус "одобрен" */
    const statusApproved = 1;

    /** статус "отклонен" */
    const statusRejected = 2;

    /* Common Part */

    /**
     * Имя таблицы, с которой работает маппер
     * @var string
     */
    protected static $sCurrentTable = 'guest_book';

    /**
     * @static Метод возвращает имя рабочей таблицы
     * @return string
     */
    public static function getCurrentTable() {

        return self::$sCurrentTable;
    }

    /**
     * @static Метод возвращает список полей рабочей таблицы
     * @return array
     */
    public static function getParametersList() {

        return self::$aParametersList;
    }

    /** Конфигурация полей таблицы
     * @var array
     */
    protected static $aParametersList = array(
        'id' => 'i:hide:Идентификатор записи',
        'parent' => 'i:hide:Раздел',
        'date_time' => 's:str:Дата публикации',
        'name' => 's:str:Имя автора',
        'email' => 's:str:Электронный адрес',
        'content' => 's:text:Сообщение',
        'city' => 's:str:Город',
        'status' => 'i:select:Статус',

    );


}//class