<?php

/* main */
$aConfig['name']     = 'GuestBook';
$aConfig['title']    = 'Модуль отзывов';
$aConfig['version']  = '1.000a';
$aConfig['description']  = 'Модуль отзывов';
$aConfig['revision'] = '0001';
$aConfig['layer']     = 'Page';

/* Module css inclusions */

$aConfig['css'] = array();
$aConfig['js'] = array();

return $aConfig;