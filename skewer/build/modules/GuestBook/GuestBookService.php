<?php
/**
 *
 * @project Skewer
 * @package Modules
 *
 * @author kolesnikov, $Author: $
 * @version $Revision:  $
 * @date $Date: $
 */
class GuestBookService extends ServicePrototype{

    public function addNewComment(FormsEntity $oData){

        $aData = $oData->getData();
        $aParam = $oData->getParams();

        $aFields = array(); // fio email city comment
        $aFields['parent'] = isset($aData['form_section'])?$aData['form_section']:0;
        $aFields['date_time'] = date('Y-m-d H:i:s');
        $aFields['status'] = GuestBookMapper::statusNew;

        foreach($aParam as $oCurParam){
            $aCurData = $oCurParam->getData();
            $aFields[$aCurData['param_name']] = strip_tags($aCurData['param_default']);
        }

        GuestBookMapper::saveItem($aFields);

        return true;
    }

}
