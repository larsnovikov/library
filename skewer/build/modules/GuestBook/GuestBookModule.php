<?php

/**
 * @class GuestBookModule
 * @extends skModule
 * @project Skewer
 * @package kernel
 *
 * @author kolesnikov, $Author: $
 * @version $Revision: $
 * @date  $
 *
 */

class GuestBookModule extends skModule{

    var $iSectionId;
    var $hideTitle;
    var $altTitle = '';

    public $onPage = 10;
    private $iCurrentSection;

    public function init(){

        $this->iSectionId = $this->getEnvParam('sectionId');
        $this->iCurrentSection = $this->getEnvParam('sectionId');
        $this->setParser(parserTwig);
    }

    protected function dateToText($sDate) {
        $sNewDate = $sDate;
        $aNameMonth = array('01'=>'января','02'=>'февраля','03'=>'марта','04'=>'апреля','05'=>'мая','06'=>'июня','07'=>'июля','08'=>'августа','09'=>'сентября','10'=>'октября','11'=>'ноября','12'=>'декабря');
        $arr = explode(' ',$sDate);
        $res = array();
        $res['date'] = $arr[0]; $res['time'] = $arr[1];
        list($res['year'],$res['month'],$res['day']) = explode('-',$arr[0]);
        list($res['hour'],$res['min'],$res['sec']) = explode(':',$arr[1]);
        if( isset($aNameMonth[$res['month']]) )
            $sNewDate = $res['day'].' '.$aNameMonth[$res['month']].' '.$res['year'];
        return $sNewDate;
    }

    public function execute(){

        // номер текущей страницы
        $iPage = $this->getInt('page', 1);

        // выбрать только одобренные
        $aFilter = array(
            'where_condition' => array(
                'status' => array (
                    'sign' => '=',
                    'value' => '1'
                )
            ),
            'limit' => array(
                'start' => ($iPage-1)*$this->onPage,
                'count' => $this->onPage
            ),
            'order' => array(
                'field' => 'date_time',
                'way' => 'DESC'
            )

        );
        $aData = GuestBookMapper::getItems( $aFilter );

        // расставить отформатированииые даты в записях
        foreach($aData['items'] as $iKey=>$aItem){
            $aItem['date_time'] = $this->dateToText($aItem['date_time']);
            $aItem['content'] = nl2br($aItem['content']);
            $aData['items'][$iKey] = $aItem;
        }

        // задать данные для парсинга
        $this->setData('list',$aData);
        $this->setData('form', '!');

        $oFormEmbededContext = new skContext('form','FormsModule', ctModule, array('FormName'=>'otzivi'));
        $this->addChildProcess($oFormEmbededContext);


        // задать шаблон
        $this->setTemplate('view.twig');

        // ссыки постраничного
        $this->getPageLine($iPage, $aData['count'], $this->iCurrentSection, array(), array('onPage'=>$this->onPage));

        return psComplete;
    }

}