<?php

/**
 * @class GuestBookInstall
 * @extends skModule
 * @project Skewer
 * @package kernel
 *
 * @author kolesnikov, $Author:  $
 * @version $Revision: $
 * @date $
 *
 */

class GuestBookInstall extends skModuleInstall {

    public function init() {
        return true;
    }// func

    public function install() {
        return true;
    }// func

    public function deinstall() {
        return true;
    }// func

}//class