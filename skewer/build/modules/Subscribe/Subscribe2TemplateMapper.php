<?php
/**
 * Модель для шаблонов рассылки
 * @class Gallery2AlbumsMapper
 *
 * @author kolesnikiv, $Author: $
 * @version $Revision: $
 * @date $Date: $
 * @project Skewer
 * @package Modules
 */
class Subscribe2TemplateMapper extends skMapperPrototype {

    /**
     * Таблица модели
     * @var string
     */
    protected  static $sCurrentTable = 'subscribe_templates';

    /**
     * Модель данных
     * @var array
     */
    protected static $aParametersList = array(

        'id'         => 'i:hide:id',
        'title'      => 's:str:Заголовок',
        'content'    => 's:wyswyg:Текст сообщения',

    );


}
