<?php

/**
 * @class SubscribeApi
 * @extends skModule
 * @project Skewer
 * @package kernel
 *
 * @author Andy Mitrich, kolesnikov, $Author: acat $
 * @version $Revision: 1173 $
 * @date 03.02.12 10:12 $
 *
 */

class SubscribeApi {

    /**
     * @static Метод для проверки наличия адреса e-mail в базе
     * @param $sEmail
     * @return int
     */
    public static function checkEmail($sEmail){

        $bResult = Subscribe2UsersMapper::checkEmail($sEmail);
        return (int)$bResult;
    }

    /**
     * @static Метод для добавления нового подписчика в базу
     * @param FormsEntity $oForm
     * @return bool|int
     */
    public static function addSubscriber(FormsEntity $oForm){

        $aParams = $oForm->getParams();
        $aData = array();

        /* @var $oParam Forms2ParamsEntity */
        foreach( $aParams as $oParam ){

            if ( !($oParam instanceof Forms2ParamsEntity) ) continue;

            $aData[$oParam->getName()] = $oParam->getDefault();
        }

        if ( sizeof($aData) ){

            return Subscribe2UsersMapper::saveItem($aData);
        }
        else return false;
    }

    /**
     * @static Метод удаления подписчика
     * @param $sEmail
     * @return int
     */
    public static function delSubscriber( $sEmail ){

        return (int)Subscribe2UsersMapper::delSubscriber($sEmail);
    }

}//class