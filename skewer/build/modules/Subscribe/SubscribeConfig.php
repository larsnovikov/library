<?php

/* main */
$aConfig['name']     = 'Subscribe';
$aConfig['title']    = 'Рассылка';
$aConfig['version']  = '1.000a';
$aConfig['description']  = 'Модуль рассылки';
$aConfig['revision'] = '0001';
$aConfig['layer']     = 'Page';

/* Module css inclusions */

$aConfig['css'] = array();
$aConfig['js'] = array(
    '/skewer_build/modules/Subscribe/js/Subscribe.js'
);

return $aConfig;