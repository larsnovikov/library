<?php
/**
 * Модель для шаблонов рассылки
 * @class Gallery2AlbumsMapper
 *
 * @author kolesnikiv, $Author: $
 * @version $Revision: $
 * @date $Date: $
 * @project Skewer
 * @package Modules
 */
class Subscribe2UsersMapper extends skMapperPrototype {

    /**
     * Таблица модели
     * @var string
     */
    protected  static $sCurrentTable = 'subscribe_users';

    /**
     * Модель данных
     * @var array
     */
    protected static $aParametersList = array(

        'id'        => 'i:hide:id',
        'email'     => 's:str:E-mail',
//        'person'    => 's:str:Контактное лицо',
//        'city'      => 's:str:Город',
        'ticket'    => 's:hide: ',

    );

    /**
     * @static Удаление подписчика по Email
     * @param $sEmail
     * @return bool|mysqli_result
     */
    public static function delSubscriber($sEmail){

        global $odb;

        $aData = array(
            'table' => self::$sCurrentTable,
            'email' => $sEmail
        );

        $sQuery = "
            DELETE
            FROM
                `[table:q]`
            WHERE
                `email` = [email:s];";

        return $odb->query($sQuery, $aData);
    }

    /**
     * @static Проверка наличия e-mai в БД
     * @param $sEmail
     * @return bool
     */
    public static function checkEmail($sEmail){

        $aFilter = array(
            'where_condition' => array(
                'email' => array(
                    'sign' => '=',
                    'value' => $sEmail
                )
            )
        );

        $aResult = self::getItem($aFilter);

        return (sizeof($aResult))? true: false;
    }



}