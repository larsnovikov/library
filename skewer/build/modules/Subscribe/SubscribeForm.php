<?php

$aForm = array();

/*  Form Description  */

$aForm['form_id'] = 101;
$aForm['form_title'] = 'Подписка на рассылку';
$aForm['form_captcha'] = 1;
$aForm['form_params'] = array();

/*  Parameters Description  */
/*
$aParam = array();
$aParam['param_id'] = 1;
$aParam['param_name'] = 'person';
$aParam['param_title'] = 'Контактное лицо';
$aParam['param_description'] = '';
$aParam['param_type'] = 1;
$aParam['param_type_title'] = 'input';
$aParam['param_required'] = 1;
$aParam['param_default'] = '';
$aParam['param_maxlength'] = 255;
$aParam['param_validation_type'] = 'text';
$aParam['param_depend'] = '';
$aParam['param_man_params'] = '';

$aForm['form_params'][] = $aParam;

$aParam = array();
$aParam['param_id'] = 2;
$aParam['param_name'] = 'city';
$aParam['param_title'] = 'Город';
$aParam['param_description'] = '';
$aParam['param_type'] = 1;
$aParam['param_type_title'] = 'input';
$aParam['param_required'] = 1;
$aParam['param_default'] = '';
$aParam['param_maxlength'] = 255;
$aParam['param_validation_type'] = 'text';
$aParam['param_depend'] = '';
$aParam['param_man_params'] = '';

$aForm['form_params'][] = $aParam;
*/
$aParam = array();
$aParam['param_id'] = 1;
$aParam['param_name'] = 'email';
$aParam['param_title'] = 'Ваш e-mail';
$aParam['param_description'] = '';
$aParam['param_type'] = 1;
$aParam['param_type_title'] = 'input';
$aParam['param_required'] = 1;
$aParam['param_default'] = '';
$aParam['param_maxlength'] = 255;
$aParam['param_validation_type'] = 'email';
$aParam['param_depend'] = '';
$aParam['param_man_params'] = '';

$aForm['form_params'][] = $aParam;

return $aForm;