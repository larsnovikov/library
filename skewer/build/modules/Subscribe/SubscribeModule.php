<?php

/**
 * @class SubscribeModule
 * @extends skModule
 * @project Skewer
 * @package kernel
 *
 * @author Andy Mitrich, $Author: acat $
 * @version $Revision: 1173 $
 * @date 26.01.12 15:47 $
 *
 */

class SubscribeModule extends skModule {

    /* @var \FormsApi */
    var $oFormsApi;
    var $AnswersTemplate = 'answers.twig';

    public function init(){

        $this->oFormsApi = new FormsApi(array());
    }

    public function execute(){

        $sCmd = $this->getStr('cmd', 'show');

        switch( $sCmd ){

            case 'show':

                $oForm = $this->oFormsApi->createFormByModel(include BUILDPATH.'modules/Subscribe/SubscribeForm.php');

                if ( !($oForm instanceof FormsEntity) ) return psRendered;

                $this->setData('oForm', $oForm);
                $this->setData('iRandVal', rand(0, 1000));
                $this->setTemplate('form.twig');

                return psComplete;

            break;

            case 'addSubscriber':

                $this->setTemplate($this->AnswersTemplate);
                $bEmail = SubscribeApi::checkEmail($this->getStr('email'));

                if ( $bEmail ) {

                    $this->setData('duplicate_email', 1);
                    return psComplete;
                }

                $sErrorHandler = '';

                $oForm = $this->oFormsApi->createFormByModel(include BUILDPATH.'modules/Subscribe/SubscribeForm.php');

                if ( !($oForm instanceof FormsEntity) ) return psRendered;

                $oForm = $this->oFormsApi->validateForm($oForm, $_POST, $sErrorHandler);

                # форма не найдена - сообщяем о попытке взлома
                if( !($oForm instanceof FormsEntity) ) {
                    $this->setData($sErrorHandler, 1);
                    return psComplete;
                }

                $bRes = SubscribeApi::addSubscriber($oForm);

                $sData = ($bRes)? 'success': 'error';
                $this->setData($sData, 1);


                return psComplete;

            break;

            case 'unsubscribe_ajax':

                $sEmail = $this->getStr('email');

                $bEmail = SubscribeApi::checkEmail($sEmail);

                if ( !$bEmail )
                    $this->setData('out', 0);
                else
                    $this->setData('out', SubscribeApi::delSubscriber($sEmail));

                return psRendered;
            break;

            case 'unsubscribe':

                $this->setTemplate($this->AnswersTemplate);

                $sEmail = $this->getStr('email');
                $sToken = $this->getStr('token');

                $bEmail = SubscribeApi::checkEmail($sEmail);

                if ( md5('unsub'.$sEmail.'010') != $sToken || !$bEmail ){
                    $this->setData('subscriber_not_delete', 1);
                }else{
                    $this->setData('subscriber_delete', SubscribeApi::delSubscriber($sEmail));
                }



                return psComplete;

            break;

        }

    }

    public function shutdown(){

    }

} //class