<?php

/* main */
$aConfig['name']     = 'EventsPicker';
$aConfig['title']     = 'Календарь событий';
$aConfig['version']  = '1.000a';
$aConfig['description']  = 'Модуль вывода календаря событий';
$aConfig['revision'] = '0001';
$aConfig['layer']     = 'Page';

$aConfig['js'] = array(

    '/skewer_build/modules/EventsPicker/js/datepicker.js',
);
return $aConfig;