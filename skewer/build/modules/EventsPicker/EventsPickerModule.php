<?php
/**
 * Модуль календаря для новостей
 * @class EventsPickerModule
 *
 * @author ArmiT, $Author: sapozhkov $
 * @version $Revision: 6 $
 * @date $Date: 2012-05-18 10:52:53 +0400 (Пт., 18 мая 2012) $
 * @project Skewer
 * @package modules
 */
class EventsPickerModule extends skModule {

    public function execute() {

        $this->setData('test', 'test');

        //$this->addJSFile('datepicker.js', false);
        $this->setTemplate('view.twig');
        return psComplete;
    }// func
}// class
