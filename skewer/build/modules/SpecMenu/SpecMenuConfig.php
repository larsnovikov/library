<?php

/* main */
$aConfig['name']     = 'SpecMenu';
$aConfig['title']     = 'Специальное меню';
$aConfig['version']  = '1.000a';
$aConfig['description']  = 'Специальное меню';
$aConfig['revision'] = '0001';
$aConfig['layer']     = 'Page';


/* Module clientside inclusions */

$aConfig['js'] = array(
    '/skewer_build/modules/SpecMenu/js/SpecMenu.js'
);

return $aConfig;