<?php
class SpecMenuModule extends skModule {

	/*
    
    * Подключить JS обработчик кнопок
    * Заработать AJAX слой для мелких компанентов    
    
    */
    
    
    public function init() {
        
    }// func
	
	public function execute() {
            
        $cmd = $this->getStr('cmd','show');

        switch($cmd){

            case 'show':
            default:

                $page = $this->getProcess('out', psAll);
                if($page->getStatus() != psComplete) return psWait;

                $fontSize = "min";
                $fontSizes = array('min'=>'', 'mid'=>'14', 'big'=>'16');

                if(isset($_SESSION['SpecMenuModule']['fontSize'])
                and array_key_exists($_SESSION['SpecMenuModule']['fontSize'], $fontSizes))
                    $fontSize = $_SESSION['SpecMenuModule']['fontSize'];

                $page->setData('specMenu_bodyFontSize', $fontSizes[$fontSize]);
                switch ($fontSize){
                    case 'min': $page->setData('specMenu_minSelected', 'on'); break;
                    case 'mid': $page->setData('specMenu_midSelected', 'on'); break;
                    case 'big': $page->setData('specMenu_bigSelected', 'on'); break;
                } // switch

                $sQueryString = ( strpos($_SERVER['REQUEST_URI'], '?')!==false )? $_SERVER['REQUEST_URI'].'&version=print': '?version=print';

                $page->setData('printversion', $sQueryString);

                break;

            case 'setSize_ajax':

                $fontSize = $this->getStr('fontSize','min');
                $_SESSION['SpecMenuModule']['fontSize'] = $fontSize;
                break;
        } // switch

        return psRendered;
    }// func


    public function shutdown() {
        
    }// func
}// class
