<?php
/**
 * По списку id выводит дерево разделов
 * @class SitemapModule
 *
 * @author ArmiT, $Author: acat $
 * @version $Revision: 1433 $
 * @date $Date: 2012-12-18 16:15:33 +0400 (Вт., 18 дек. 2012) $
 * @project Skewer
 * @package kernel
 */
class SitemapModule extends skModule {
    /**
     * Список id разделов, от которых строить дерево разделов
     * @var string|array
     */
    var $rootSections = '';

    /**
     * Экземпляр класса для работы с деревьями
     * @var null|\Tree
     */
    protected $oTree = null;

    /**
     * Первичная инициализация
     */
    public function init(){

        $this->oTree = new Tree('section');
    }

    /**
     * Выполнение модуля
     * @return int
     */
    public function execute() {

        $this->rootSections = explode(',',$this->rootSections);

        if(!count($this->rootSections)) return psRendered;

        $iMainSection = skConfig::get('section.main');

        $aItems[$iMainSection] = $this->oTree->getById($iMainSection);
        $aItems[$iMainSection]['href'] = (!empty($aItems[$iMainSection]['link']))? $aItems[$iMainSection]['link'] : '['.$aItems[$iMainSection]['id'].']';

        foreach($this->rootSections as $iSectionId) {

            $aSubItems = $this->oTree->getSubItems($iSectionId);

            if(!count($aSubItems)) continue;

            foreach ($aSubItems as $aItem){
                if($aRow = $this->makeSiteMap($aItem['id']))
                    if( $aItem['id'] != $iMainSection )
                        $aItems[] = $aRow;
            }

        }

        $this->setData('items', $aItems);

        $this->setTemplate('view.twig');
        return psComplete;
    }// func

    /**
     * Возвращает дерево(массив) разделов
     * @param $iRootSection
     * @return array|bool
     */
    protected function makeSiteMap($iRootSection) {

        $aItems = $this->oTree->getById($iRootSection);

        if($aItems['visible'] != 1) return false;

        $aItems['href'] = (!empty($aItems['link']))? $aItems['link'] : '['.$aItems['id'].']';
        $aSubItems = $this->oTree->getSubItems($iRootSection);

        if(count($aSubItems))
            foreach($aSubItems as $aItem)
                if($aRows = $this->makeSiteMap($aItem['id']))
                    $aItems['items'][] = $aRows;

        return $aItems;
    }// func

}// class
