<?php

/* main */
$aConfig['name']     = 'Sitemap';
$aConfig['title']     = 'Карта сайта';
$aConfig['version']  = '1.000a';
$aConfig['description']  = 'Карта сайта';
$aConfig['revision'] = '0001';
$aConfig['layer']     = 'Page';

return $aConfig;
