<?php
/**
 *
 * @class SocialButtonsModule
 *
 * @author kolesnikov, $Author: sokol $
 * @version $Revision: 532 $
 * @date $Date: 2012-08-01 10:51:46 +0400 (Ср., 01 авг. 2012) $
 * @project Skewer
 * @package kernel
 */
class SocialButtonsModule extends skModule {

    public $soclinkContent = 0;
    public $soclinkNews = 0;
    public $soclinkGoods = 0;
    public $soclinkGallery = 0;

    /**
     * Первичная инициализация
     */
    public function init(){

    }

    /**
     * Выполнение модуля
     * @return int
     */
    public function execute() {

        $bShow = false;

        /** @var skProcess $contentModule */
        $contentModule = $this->getProcess('out.content', psAll);
        if($contentModule instanceof skProcess){

            if($contentModule->getStatus() != psComplete) return psWait;

            switch($contentModule->getModuleClass()){
                case 'NewsModule':
                    $newsAlias = -1;
                    if($contentModule->oRouter->getStr('news-alias', $newsAlias) ||
                        $contentModule->oRouter->getStr('news_id', $newsAlias)){

                        if($this->soclinkNews) $bShow = true;

                    }

                break;

                case 'EventsModule':
                    $eventsAlias = -1;
                    if($contentModule->oRouter->getStr('event-alias', $eventsAlias) ||
                        $contentModule->oRouter->getStr('event_id', $eventsAlias)){

                        if($this->soclinkNews) $bShow = true;

                    }

                    break;

                case 'GalleryModule':
                    $newsAlias = -1;
                    if($contentModule->oRouter->getStr('album-alias', $newsAlias) ||
                        $contentModule->oRouter->getStr('album_id', $newsAlias)){

                        if($this->soclinkGallery) $bShow = true;

                    }

                break;

                // todo [ilya:15.05.12] сделат для каталога

            }


        }elseif($this->soclinkContent){

            $bShow = true;

        }



        $this->setTemplate('block.twig');

        $this->setData('show', $bShow);

        return psComplete;
    }// func


}
