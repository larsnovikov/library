<?php

/**
 * @class PollAnswersMapper
 * @extends skModule
 * @project Skewer
 * @package kernel
 *
 * @author andymitrich, $Author: sapozhkov $
 * @version $Revision: 6 $
 * @date 12.01.12 16:05 $
 *
 */

class Poll2AnswersMapper extends skMapperPrototype{

    protected static $sCurrentTable = 'polls_answers';

    public static function getCurrentTable() {

        return static::$sCurrentTable;
    }// function getCurrentTable()

    /**
     * @var array Конфигурация полей таблицы polls_answers
     */
    protected static $aParametersList = array(
        'answer_id' => 'i:hide:Номер ответа',
        'title' => 's:str:Ответ',
        'parent_poll' => 's:str:Родительский опрос',
        'value' => 'i:str:Значение',
        'sort' => 'i:str:Порядок'
    );

    public static function updAnswer($aInputData){

        // Если во входящем массиве с данными нет значения сортироки или оно равно нулю - получаем максимальный порядок для текущей позиции
        if ( !isset($aInputData['sort']) || !$aInputData['sort'] )
        $aInputData['sort'] = self::getMaxOrder($aInputData['parent_poll'])+1;

        // Сохраняем опрос
        return self::saveItem($aInputData);
    }// function updAnswer()

    public static function getMaxOrder($iParentPoll){

        global $odb;

        $aData = array('parent_poll'=>$iParentPoll);

        $sQuery = "
            SELECT
                MAX(`sort`) as sort
            FROM
                `polls_answers`
            WHERE
                `parent_poll` = [parent_poll:i];";

        $aValue = $odb->getRow($sQuery, $aData);

        return $aValue['sort'];
    }// function getMaxOrder()

    public static function delAnswer($aInputData){

        if ( !$aInputData ) return false;

        global $odb;

        $aData = array(
            'answer_id' => $aInputData['answer_id'],
            'parent' => $aInputData['parent_poll']
        );

        $sQuery = "
            DELETE
            FROM
                `polls_answers`
            WHERE
                `answer_id` = [answer_id:i] AND
                `parent_poll` = [parent:i];";

        return $odb->query($sQuery, $aData);
    }// function delAnswer()

    public static function addVote($aInputData){

        global $odb;

        $aData = array(
            'answer' => $aInputData['answer'],
            'poll' => $aInputData['poll']
        );

        $sQuery = "
            UPDATE
                `polls_answers`
            SET
                `value` = `value` + 1
            WHERE
                `parent_poll` = [poll:i] AND
                `answer_id` = [answer:i];";

        return $odb->query($sQuery, $aData);
    }

    public static function getAnswers($aFilter){

        return static::getItems($aFilter);
    }

    protected static function getAddParamList(){

        return array(
            'answer_id' => array(
                'view' => 'hide',
                'listColumns' => array(
                    'hidden' => true
                )
            ),
            'parent_poll' => array(
                'view' => 'hide',
                'listColumns' => array(
                    'hidden' => true
                )
            ),
            'title' => array(
                'title' => 'Ответ',
                'listColumns' => array(
                    'flex' => 1
                )
            ),
            'sort' => array(
                'listColumns' => array(
                    'width' => 55,
                    'align' => 'center'
                )
            )
        );
    }// function getAddParamList()

}//class