<?php

/**
 * @class PollInstall
 * @extends skModule
 * @project Skewer
 * @package kernel
 *
 * @author Andy Mitrich
 * @version $Revision: 6 $
 * @date 13.01.12 10:12 $
 *
 */

class PollInstall extends skModuleInstall {

    public function init() {
        return true;
    }// func

    public function install() {
        return true;
    }// func

    public function deinstall() {
        return true;
    }// func

}// class