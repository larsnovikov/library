<?php

/**
 * @class PollModule Модуль голосования
 * @extends skModule
 * @project Skewer
 * @package kernel
 *
 * @author Andy Mitrich, $Author: armit $
 * @version $Revision: 249 $
 * @date 29.12.11 10:23 $
 *
 */

class PollModule extends skModule{

    public $Location = 'right';
    /* @var null|\PollApi */
    public $oPollApi = NULL;
    private $sFileTemplate;
    private $sFileAnswerTemplate;
    public $SectionId = 0;
    /**
    * @var int ID главной страницы
    */
    private $defaultSection = 0;

    public function init(){

       $this->oPollApi = new PollApi();
       $this->setParser(parserTwig);
       $this->SectionId = $this->getEnvParam('sectionId');
       $this->defaultSection = skConfig::get('section.main');
       $this->sFileTemplate = 'poll_'.$this->Location.'.twig';
       $this->sFileAnswerTemplate = 'answer_'.$this->Location.'.twig';
    }

    public function execute(){

        $sCmd = $this->getStr('cmd','show');

        switch($sCmd){

            case 'show':
            default:

                $aParams = array();
                $aParams['location'] = $this->Location;
                $aParams['current_section'] = $this->SectionId;

                if ( $this->SectionId == $this->defaultSection ){
                    $aPolls = $this->oPollApi->getPollsOnMain($aParams);
                }
                else{

                    $oTree = new Tree();
                    $aParentSections = $oTree->getSectionParents($this->SectionId);
                    if ( $aParentSections )
                        $aParams['parent_sections'] = implode(',', $aParentSections);
                    else $aParams['parent_sections'] = $this->SectionId;

                    $aPolls = $this->oPollApi->getPollsOnInternal($aParams);
                }

                if ( $aPolls )
                    $this->setData('aPolls', $aPolls);

                $this->setTemplate($this->sFileTemplate);

                return psComplete;

            break;

            case 'vote_ajax':

                $iPollId = $this->getInt('poll');
                $iAnswerId = $this->getInt('answer');
                $aOut = array();

                $this->oPollApi->addVote(array('poll'=>$iPollId,'answer'=>$iAnswerId));

                $aOut['aPoll'] = $this->oPollApi->getPollHeader($iPollId);
                $aAnswers = $this->oPollApi->getAnswers($iPollId);
                $aOut['aAnswers'] = $aAnswers['items'];
                $aOut['iAllCount'] = $aAnswers['answers_count'];

                $sRendered = $this->renderTemplate($this->sFileAnswerTemplate,$aOut);
                $this->setData('out', $sRendered);

                return psRendered;
            break;

            case 'showResults_ajax':

                $iPollId = $this->getInt('poll');
                $aOut = array();
                $aOut['aPoll'] = $this->oPollApi->getPollHeader($iPollId);
                $aAnswers = $this->oPollApi->getAnswers($iPollId);
                $aOut['aAnswers'] = $aAnswers['items'];
                $aOut['iAllCount'] = $aAnswers['answers_count'];

                $sRendered = $this->renderTemplate($this->sFileAnswerTemplate,$aOut);
                $this->setData('out', $sRendered);

                return psRendered;
            break;
        }

    }

    public function shutdown(){

    }


}// class