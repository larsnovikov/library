<?php

/* main */
$aConfig['name']     = 'Poll';
$aConfig['title']     = 'Голосование';
$aConfig['version']  = '1.000a';
$aConfig['description']  = 'Голосование';
$aConfig['revision'] = '0001';
$aConfig['layer']     = 'Page';

$aConfig['js'] = array(
    '/skewer_build/modules/Poll/js/Poll.js'
);

return $aConfig;