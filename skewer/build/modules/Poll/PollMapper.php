<?php

/**
 * @class PollMapper Маппер для модуля голосования
 * @extends skModule
 * @project Skewer
 * @package kernel
 *
 * @author Andy Mitrich, $Author: sapozhkov $
 * @version $Revision: 6 $
 * @date 29.12.11 10:45 $
 *
 */

class PollMapper extends skMapperPrototype {

    /**
     * @var string Имя таблицы, с которой работает маппер
     */
    protected  static $sCurrentTable = 'polls';

    /**
     * @static Метод возвращает имя рабочей таблицы
     * @return string
     */
    public static function getCurrentTable() {

        return static::$sCurrentTable;
    }// function getCurrentTable()

    /**
     * @var array Конфигурация полей таблицы
     */
    protected static $aParametersList = array(
        'id' => 'i:hide:Номер опроса',
        'title' => 's:str:Заголовок опроса',
        'question' => 's:str:Вопрос',
        'active' => 'i:check:Активность',
        'on_main' => 'i:check:Отображать на главной',
        'on_allpages' => 'i:check:Отображать на всех страницах',
        'on_include' => 'i:check:Отображать на внутренних страницах',
        'location' => 's:str:Позиция для вывода',
        'locationTitle' => 's:str:Позиция для вывода',
        'section' => 'i:str:Раздел для показа',
        'sort' => 'i:str:Порядок',
    );

    protected static function getAddParamList(){
        return array(
            'id' => array(
                'view' => 'hide',
                'listColumns' => array(
                    'hidden' => true
                )
            ),
            'active' => array(
                'listColumns' => array(
                    'hidden' => true
                )
            ),
            'title' => array(
                'listColumns' => array(
                    'flex' => 1
                )
            ),
            'location' => array_merge(ExtForm::getDesc4SelectFromArray(PollAdmApi::getPollLocations()), array('listColumns' => array('hidden'=>true))),
            'section' => ExtForm::getDesc4SelectFromArray(PollAdmApi::getSectionTitle()),
            'on_main' => array(
                'title' => 'На главной',
                'listColumns' => array(
                    'width' => 70,
                    'align' => 'center'
                )
            ),
            'on_allpages' => array(
                'title' => 'На всех',
                'listColumns' => array(
                    'width' => 50,
                    'align' => 'center'
                )
            ),
            'on_include' => array(
                'title' => 'На внутренних',
                'listColumns' => array(
                    'width' => 90,
                    'align' => 'center'
                )
            ),
            'sort' => array(
                'title' => 'Порядок',
                'listColumns' => array(
                    'width' => 55,
                    'align' => 'center'
                )
            )
        );
    }// function getAddParamList()

    public static function getPollsOnInternal($aParams){

        global $odb;

        $aData = array(
            'location' => $aParams['location'],
            'current_section' => $aParams['current_section'],
            'parent_sections' => $aParams['parent_sections']
        );

        $sQuery = "
            SELECT
                *
            FROM
                `polls`
            WHERE
                `active` = 1 AND
                ( `section` = [current_section:i] OR
                ( `on_include` = 1 AND `section` IN ([parent_sections:q]) ) OR
                `on_allpages` = 1 ) AND
                `location` LIKE [location:s]
            ORDER BY
                `sort`;";

        $rResult = $odb->query($sQuery, $aData);

        $aPolls = array();
        while( $aRow = $rResult->fetch_array(MYSQLI_ASSOC) ){

            $aFilter = array(
                'where_condition' => array(
                    'parent_poll' => array(
                        'sign' => '=',
                        'value' => $aRow['id']
                    )
                ),
                'order' => array(
                    'field' => 'sort',
                    'way' => 'ASC'
                )
            );

            $aAnswers = Poll2AnswersMapper::getAnswers($aFilter);
            $aRow['answers'] = $aAnswers['items'];

            $aPolls[] = $aRow;
        }

        return $aPolls;
    }// function getPollsOnInternal()

    public static function getPollsOnMain($aParams){

        global $odb;

        $aData = array(
            'location' => $aParams['location'],
            'current_section' => $aParams['current_section']
        );

        $sQuery = "
            SELECT
                *
            FROM
                `polls`
            WHERE
                `active` <> 0 AND
                ( `section` = [current_section:i] OR `on_main`=1 ) AND
                `location` LIKE [location:s]
            ORDER BY
                `sort`;";

        $rResult = $odb->query($sQuery, $aData);

        // Собираем результирующий массив
        $aPolls = array();
        while( $aRow = $rResult->fetch_array(MYSQLI_ASSOC) ){

            $aFilter = array(
                'where_condition' => array(
                    'parent_poll' => array(
                        'sign' => '=',
                        'value' => $aRow['id']
                    )
                ),
                'order' => array(
                    'field' => 'sort',
                    'way' => 'ASC'
                )
            );

            $aAnswers = Poll2AnswersMapper::getAnswers($aFilter);
            $aRow['answers'] = $aAnswers['items'];

            $aPolls[] = $aRow;
        }

        return $aPolls;
    }// function getPollsOnMain()

    public static function delPoll($iPollId){

        if ( !$iPollId ) return false;

        global $odb;

        $aData['poll_id'] = $iPollId;

        $sQuery = "
            DELETE
            FROM
                `polls`
            WHERE
                `id` = [poll_id:i];";

        if ( $odb->query($sQuery, $aData) ){

            $sAnsQuery = "
                DELETE
                FROM
                    `polls_answers`
                WHERE
                    `parent_poll` = [poll_id:i];";

            $odb->query($sAnsQuery, $aData);

            return true;
        }

        return false;
    }

    /**
     * @static Метод сохранения опроса
     * @param $aInputData
     * @return bool
     */
    public static function updPoll($aInputData){

        // Если во входящем массиве с данными нет значения сортироки или оно равно нулю - получаем максимальный порядок для текущей позиции
        if ( !isset($aInputData['sort']) || !$aInputData['sort'] )
            $aInputData['sort'] = self::getMaxOrder($aInputData['location'])+1;

        // Сохраняем опрос
        return static::saveItem($aInputData);
    }// function updPoll()

    /**
     * @static Метод для получения максимального значения поля "sort" для текущей позиции
     * @param $sLocation Позиция, для которой выбирам максимальный порядок
     * @return mixed
     */
    public static function getMaxOrder($sLocation){

        global $odb;

        $aData = array('location'=>$sLocation);

        $sQuery = "
            SELECT
                MAX(`sort`) as sort
            FROM
                `polls`
            WHERE
                `location` LIKE [location:s];";

        $aValue = $odb->getRow($sQuery, $aData);

        return $aValue['sort'];
    }// function getMaxOrder()

    public static function getPolls($sLocation){

        global $odb;

        $aPolls = array();
        $aData = array('location'=>$sLocation);

        $sQuery = "
            SELECT
                *
            FROM
                `polls`
            WHERE
                `location` LIKE [location:s]
            ORDER BY
                `sort`;";

        $rResult = $odb->query($sQuery, $aData);

        while( $aRow = $rResult->fetch_array(MYSQLI_ASSOC) ){

            $aData['parent'] = $aRow['id'];

            $sAnswerQuery = "
                SELECT
                    *
                FROM
                    `polls_answers`
                WHERE
                    `parent_poll` = [parent:i]
                ORDER BY
                    `sort`;";

            $rAnswerResult = $odb->query($sAnswerQuery, $aData);

            $aAnswers = array();
            while( $aAnswerRow = $rAnswerResult->fetch_array(MYSQLI_ASSOC) )
                $aAnswers[] = $aAnswerRow;

            $aRow['answers'] = $aAnswers;

            $aPolls[] = $aRow;
        }

        return $aPolls;
    }// function getPolls()

}// class