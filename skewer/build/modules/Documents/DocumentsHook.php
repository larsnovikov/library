<?php
/**
 * @depricated Не используется
 * @class DocumentsHook
 *
 * @author ArmiT, $Author: sapozhkov $
 * @version $Revision: 6 $
 * @date $Date: 2012-05-18 10:52:53 +0400 (Пт., 18 мая 2012) $
 * @project Skewer
 * @package kernel
 */ 
class DocumentsHook extends skHook {

    public function registerHook(){
        $this->addBeforeHook('goTest',__CLASS__, 'test');
        $this->addAfterHook('goTest',__CLASS__, 'test');
    }// func

}// class
