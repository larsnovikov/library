<?php

/**
 * Модуль новостной системы
 * @class DocumentsModule
 * @extends skModule
 * @project Skewer
 * @package kernel
 *
 * @author Andy Mitrich, $Author: kolesnikov $
 * @version $Revision: 456 $
 * @date $Date: 2012-07-24 09:56:14 +0400 (Вт, 24 июл 2012) $
 *
 */

class DocumentsModule extends skModule {

    public $parentSections;
    public $onPage = 10;
    public $listTemplate = 'list.twig';
    public $detailTemplate = 'detail_page.twig';
    public $showArchive;
    public $showFuture;
    public $showOnMain;  // отменяет фильтр по разделам
    public $allNews;
    public $sortNews;
   // public $usePageLine = 1;
    
    private $iCurrentSection;

    public $aParameterList = array(
        'order'    => 'DESC',
        'future'   => '',
        'byDate'   => '',
        'on_page'  => '',
        'on_main'  => '',
        'section'  => '',
        'archive'  => '',
        'all_news' => '',
    );

    public function init() {

        $this->setParser(parserTwig);
        $this->iCurrentSection = $this->getEnvParam('sectionId');
        $this->aParameterList['on_page'] = $this->onPage;

        if ( $this->allNews ) $this->aParameterList['all_news'] = 1;
        if ( $this->showOnMain ) $this->aParameterList['all_news'] = 1;

        if ( $this->parentSections ) $this->aParameterList['section'] = $this->parentSections;
        else $this->aParameterList['section'] = $this->iCurrentSection;

        if ( $this->iCurrentSection==skConfig::get('section.main') ) $this->aParameterList['on_main'] = 1;
        if ( $this->showArchive ) $this->aParameterList['archive'] = 1;
        if ( $this->showFuture ) $this->aParameterList['future'] = 1;
        if ( $this->sortNews ) $this->aParameterList['order'] = $this->sortNews;

        return true;

    }// func

    public function execute() {

        $iPage       = $this->getInt('page', 1);
        $iNewsId     = $this->getInt('news_id', 0);
        $sNewsAlias  = $this->getStr('news-alias', '');
        $sDateFilter = $this->getStr('date');

        if ( !empty($sDateFilter) ) {
            $sDateFilter = date('Y-m-d', strtotime($sDateFilter));
            $this->aParameterList['byDate'] = $sDateFilter;
        }

        /*
         * Если в запросе передается ID новости или её алиас, значит необходимо вывести конкретную новость;
         * если нет - выводим список новостей, разбитый по страницам.
         */
        if ( $iNewsId || $sNewsAlias ) {

            $aNewsItem = array();

            // Если есть ID новости - выбираем её по ID
            if ( $iNewsId )
                $aNewsItem = DocumentsApi::getNewsById($iNewsId);

            // Если есть алиас новости - выбираем по нему
            if ( $sNewsAlias )
                $aNewsItem = DocumentsApi::getNewsByAlias($sNewsAlias);

//            $hideDate = Parameters::getByName($this->iCurrentSection,'content','hideDate');
//            if( isset($hideDate['value']) && $hideDate['value'] )
                unset($aNewsItem['last_modified_date']);

            if ( sizeof($aNewsItem) ){

                $this->setData('aNewsItem', $aNewsItem);

                // Убрать статический контент
                $oPage = $this->getProcess('out', psAll);
                if( $oPage->getStatus() != psComplete ) return psWait;
                $oPage->setData('staticContent', array());

            }

            $this->setTemplate($this->detailTemplate);

            // Метатеги для списка новостей
            $SEOData = $this->getEnvParam('SEOTemplates', array());
            $SEOData['docsDetail'] = array('title'=>'','description'=>'','keywords'=>'');
            if($aCurSEOData = SEOData::get('news',$aNewsItem['id'])){
                $SEOData['docsDetail']['title'] = $aCurSEOData['title'];
                $SEOData['docsDetail']['description'] = $aCurSEOData['description'];
                $SEOData['docsDetail']['keywords'] = $aCurSEOData['keywords'];
            }
            $this->setEnvParam('SEOTemplates', $SEOData);
            $SEOVars = $this->getEnvParam('SEOVars', array());
            $SEOVars['Название документа'] = $aNewsItem['title'];
            $SEOVars['название документа'] = mb_strtolower($aNewsItem['title']);
            $this->setEnvParam('SEOVars', $SEOVars);

            // добавляем элемент в pathline
            $oProcessPathLine = $this->getProcess('out.pathLine', psAll);
            if($oProcessPathLine instanceof skProcess){
                $oProcessPathLine->setStatus(psNew);
                $this->setEnvParam('pathline_additem',
                    array( 'id' => $aNewsItem['id'],
                        'title' => $aNewsItem['title'],
                        'alias_path' => $aNewsItem['news-alias'],
                        'link' => ''
                    )
                );
            }


        } else {

            $aNewsList = array();

            // Получаем список новостей
            $aNewsList = DocumentsApi::getNewsList($iPage, $this->aParameterList);

            if ( sizeof($aNewsList['items']) ){

                $this->setData('aNewsList', $aNewsList['items']);
                $this->setData('news_section', $this->parentSections);

                $aURLParams = array();

                if(!empty($sDateFilter)) $aURLParams['date'] = $sDateFilter;

                $this->getPageLine($iPage, $aNewsList['count'], $this->iCurrentSection, $aURLParams, array('onPage'=>$this->aParameterList['on_page']));
            }

            $this->setTemplate($this->listTemplate);

        }

        /** @var skProcess $page  */
        // Откладываем запуск (перезапускаем) SEOMetatags после себя
        $page = $this->getProcess('out.SEOMetatags', psAll);
        if($page instanceof skProcess) $page->setStatus(psNew);

        return psComplete;
    }// func

    public function shutdown() {

    }// func
}// class
