<?php

/**
 * @class DocumentsMapper
 * @extends skModule
 * @project Skewer
 * @package kernel
 *
 * @author andymitrich, $Author: kolesnikov $
 * @version $Revision: 417 $
 * @date 02.12.11 12:14 $
 *
 */

class DocumentsMapper extends skMapperPrototype{

    /**
     * @var string Имя таблицы, с которой работает маппер
     */
    protected  static $sCurrentTable = 'documents';

    /**
     * @static Метод возвращает имя рабочей таблицы
     * @return string
     */
    public static function getCurrentTable() {

        return self::$sCurrentTable;
    }

    /**
     * @static Метод возвращает список полей рабочей таблицы
     * @return array
     */
    public static function getParametersList() {

        return self::$aParametersList;
    }

    /**
     * @var array Конфигурация полей таблицы
     */
    protected static $aParametersList = array(
        'id' => 'i:hide:Номер',
        'news-alias' => 's:str:Псевдоним',
        'parent_section' => 'i:hide:Родительский раздел',
        'publication_date' => 's:date:Дата публикации',
        'publication_time' => 's:time:Время публикации',
        'title' => 's:str:Название',
        'announce' => 's:html:Анонс',
        'full_text' => 's:html:Полный текст',
        'active' => 'i:check:Активность',
        'archive' => 'i:check:Архив',
        'on_main' => 'i:check:На главной',
        'hyperlink' => 's:str:Ссылка',
        'last_modified_date' => 's:hide:Дата последнего изменения'
    );

     // дополнительный набор параметров
    protected static function getAddParamList() {
        return array(
            'id' => array(
                'view' => 'hide',
                'listColumns' => array(
                    'hidden' => true
                )
            ),
            'title' => array(
                'listColumns' => array( 'flex' => 1 )
            ),
            'active' => array(
                'listColumns' => array( 'width' => 75 )
            ),
            'on_main' => array(
                'listColumns' => array( 'width' => 75 )
            ),
            'archive' => array(
                'listColumns' => array( 'width' => 75 )
            ),
            'announce' => array(
                'view' => 'wyswyg'
            ),
            'full_text' => array(
                'view' => 'wyswyg'
            )
       );
    }

    /**
     * @static Метод для выборки списка новостей по фильтру
     * @param $aFilter
     * @return array|bool
     */
    public static function getNews($aFilter){

        return static::getItems($aFilter);
    }//function getNews()

    /**
     * @static Метод для выборки одной новости
     * @param $aFilter
     * @return array
     */
    public static function getNewsById($aFilter){

        // Получаем новость
        $aItem = self::getItems($aFilter);

        return ( sizeof($aItem) )? $aItem['items'][0]: array();
    }//function getNewsById()

    public static function removeFromSection($iSectionId) {

        global $odb;

        $sQuery = '
            DELETE
            FROM `[table_name:q]`
            WHERE `parent_section`=[section:i];
        ';

        return $odb->query($sQuery, array(
            'table_name' => static::getCurrentTable(),
            'section' => $iSectionId,
        ));

    }// func

    /**
     * @static
     * @param $sAlias
     * @return mixed
     */
    public static function checkAlias($sAlias){

        global $odb;

        $aData = array('alias' => $sAlias);

        $sQuery = "
            SELECT
                `id`
            FROM
                `documents`
            WHERE
                `news-alias` = [alias:s];";

        $rResult = $odb->query($sQuery, $aData);

        $aItems = array();

        while( $aRow = $rResult->fetch_assoc() ){

            $aItems[$aRow['id']] = $aRow['id'];
        }

        return $aItems;
    }

}//class
?>