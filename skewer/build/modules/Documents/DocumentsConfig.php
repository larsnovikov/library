<?php

/* main */
$aConfig['name']     = 'Documents';
$aConfig['version']  = '1.000a';
$aConfig['title']    = 'Документы';
$aConfig['description']  = 'Модуль документов';
$aConfig['revision'] = '0001';
$aConfig['layer']     = 'Page';

/* Module hooks */
//$aConfig['hooks']['after']['removeSection']  = array(
//
//    'class' => 'NewsApi',
//    'method' => 'removeFromSection',
//);


/* Функциональные политики */
$aConfig['policy'][] = array(
    'name'    => 'allowDocumentsListRead',
    'title'   => 'Разрешить чтение списка документов',
    'default' => 1
);

$aConfig['policy'][] = array(
    'name'    => 'allowDocumentsDetailRead',
    'title'   => 'Разрешить чтение детальной документов',
    'default' => 1
);

$aConfig['hooks']['after']  = array();

/* Module clientside inclusions */

$aConfig['css'] = array(
    //'/skewer_build/modules/News/css/news.css',
);

$aConfig['js'] = array();

return $aConfig;