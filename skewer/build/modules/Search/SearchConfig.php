<?php

/* main */
$aConfig['name']     = 'Search';
$aConfig['title']    = 'Поиск';
$aConfig['version']  = '1.000a';
$aConfig['description']  = 'Модуль поиска по сайту';
$aConfig['revision'] = '0001';
$aConfig['layer']     = 'Page';

$aConfig['search_types'] = array(
    'any_words' => 0,
    'all_words' => 1,
    'phrase' => 2
);

$aConfig['css'] = array();

$aConfig['js'] = array();

return $aConfig;