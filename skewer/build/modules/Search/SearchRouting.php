<?php

/**
 * @class SearchRouting
 * @extends skModule
 * @project Skewer
 * @package kernel
 *
 * @author andymitrich, $Author: sapozhkov $
 * @version $Revision: 6 $
 * @date 09.02.12 17:17 $
 *
 */

class SearchRouting implements skRoutingInterface {

    /**
     * Возвращает паттерны разбора URL
     * @static
     * @return bool | array
     */
    public static function getRoutePatterns() {

        return array(
                '/*page/page(int)/*date/date/',
                '/*date/date/',
                '/*page/page(int)/'
        );
    }// func
}//class