<?php

/**
 * @class SearchModule
 * @extends skModule
 * @project Skewer
 * @package kernel
 *
 * @author Andy Mitrich, $Author: armit $
 * @version $Revision: 249 $
 * @date 08.02.12 17:04 $
 *
 */

class SearchModule extends skModule {

    public $iCurrentSection;
    public $onPage = 10;
    private $aSearchTypes = array();

    public function init(){

        $this->aSearchTypes = $this->getConfigParam('search_types');
    }

    public function execute() {

        $iPage       = $this->getInt('page', 1);
        $this->iCurrentSection = $this->getEnvParam('sectionId');

        $sSearchText = $this->getStr('search_text');
        $sCmd = $this->getStr('cmd', 'view_form');

        $sView = ( $this->iCurrentSection==175 )? 'full': 'mini';

        if ( empty($sSearchText) && $sView=='full'){

                $this->setData('not_found', 1);
                $this->setTemplate('results.twig');
                return psComplete;
        }

        $iSearchType = $this->getInt('search_type', 0);
        $iSearchSection = $this->getInt('search_section', 0);

        switch( $sView ){

            case 'mini':

                $this->setTemplate('mini_form.twig');
                return psComplete;

            break;

            case 'full':

                $oTree = new Tree();
                $this->setData('aSections', $oTree->getSectionsInLine(3));
                unset($oTree);

                $this->setData('any_words', $this->aSearchTypes['any_words']);
                $this->setData('all_words', $this->aSearchTypes['all_words']);
                $this->setData('phrase', $this->aSearchTypes['phrase']);

                $aItems = SearchApi::executeSearch($sSearchText, $iPage, $this->onPage, $iSearchType, $iSearchSection);

                    if ( is_array($aItems) && $aItems['count'] ) {

                    $this->setData('result_count', $aItems['count']);

                    if ( sizeof($aItems['items']) ){

                        $this->setData('aItems', $aItems['items']);
                    }

                    $this->getPageLine($iPage, $aItems['count'], $this->iCurrentSection, array('search_text' => $sSearchText, 'search_type' => $iSearchType, 'search_section' => $iSearchSection), array('onPage'=>$this->onPage));

                }
                else {

                    $this->setData('not_found', 1);
                }
                $this->setTemplate('results.twig');
                return psComplete;
            break;

            default:

                $this->setData('not_found', 1);
                $this->setTemplate('results.twig');
                return psComplete;
            break;
        }

    }

    public function shutdown(){

    }

}//class