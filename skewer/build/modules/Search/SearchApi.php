<?php

/**
 * @class SearchApi
 * @extends skModule
 * @project Skewer
 * @package kernel
 *
 * @author Andy Mitrich, $Author: sapozhkov $
 * @version $Revision: 6 $
 * @date 09.02.12 8:31 $
 *
 */

class SearchApi {

    private static $iLength = 500;

    /**
     * @static
     * @param $sSearchText
     * @param int $iPage
     * @param int $iOnPage
     * @return array|bool
     */
    public static function executeSearch($sSearchText, $iPage = 1, $iOnPage = 10, $iSearchType = 0, $iSearchSection = 0){

        if ( $iSearchType==0 || $iSearchType==1 ){

            $oStemmer = new skStemmer();

            $aSearch = explode(' ', $sSearchText);
            $aSearch = array_diff($aSearch, array(''));

            foreach($aSearch as &$sSearch){

                $sSearch = $oStemmer->stem_word($sSearch);
            }

            $sSearchText = implode(' ', $aSearch);
        }

        $aItems = SearchMapper::executeSearch($sSearchText, $iPage, $iOnPage, $iSearchType, $iSearchSection);

        if ( !$aItems['count'] ) return false;

        if ( sizeof($aItems['items']) )
            foreach($aItems['items'] as $iKey=>&$aItem){

                $aItem['number'] = $iKey+1+($iPage-1)*$iOnPage;

                if ( strlen($aItem['search_text']) > self::$iLength ){
                    $aItem['search_text'] =  substr($aItem['search_text'], 0, self::$iLength);
                    $aItem['search_text'] = substr($aItem['search_text'], 0, strrpos($aItem['search_text'], ' ' )).' ...';
                }
            }
        else return array();

        return $aItems;
    }

    /**
     * @static
     * @param $a
     * @param $b
     * @return int
     */
    private static function cmpArray($a, $b) {

        if ($a['rel'] == $b['rel']) return 0;
        return ($a['rel'] < $b['rel']) ? 1 : -1;
    }

}//class