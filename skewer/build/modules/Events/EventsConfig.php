<?php

/* main */
$aConfig['name']     = 'Events';
$aConfig['title']    = 'Мероприятия';
$aConfig['version']  = '1.000a';
$aConfig['description']  = 'Модуль мероприятий';
$aConfig['revision'] = '0001';
$aConfig['layer']     = 'Page';


$aConfig['hooks']['after']['removeSection']  = array(

    'class' => 'EventsApi',
    'method' => 'removeFromSection',
);

/* Module css inclusions */
$aConfig['css'] = array(

    '/skewer_build/modules/Events/css/datepicker.css',

);

$aConfig['js'] = array(

    '/skewer_build/modules/Events/js/date.js',
    '/skewer_build/modules/Events/js/datepicker.js',

);

return $aConfig;