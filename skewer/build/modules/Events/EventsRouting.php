<?php

/**
 * @class EventsRouting
 * @extends skModule
 * @project Skewer
 * @package kernel
 *
 * @author andymitrich, $Author: sapozhkov $
 * @version $Revision: 6 $
 * @date 06.02.12 16:15 $
 *
 */

class EventsRouting implements skRoutingInterface {
    /**
     * Возвращает паттерны разбора URL
     * @static
     * @return bool | array
     */
    public static function getRoutePatterns() {

        return array(
            '/event-alias/',
            '/event_id(int)/',
            '/*page/page(int)/*date/date/',
            '/*date/date/',
            '/*page/page(int)/'
        );
    }// func
}// class
