<?php

/**
 * @class EventsApi
 * @extends skModule
 * @project Skewer
 * @package kernel
 *
 * @author Andy Mitrich, $Author: sokol $
 * @version $Revision: 532 $
 * @date 06.02.12 11:03 $
 *
 */

class EventsApi {

    public static function getEventById($iItemId){

        $aItem = EventsMapper::getItem($iItemId);

        if ( $aItem ) {
            $aItem['publication_date'] = date('d.m.Y',strtotime($aItem['publication_date']));

            $tModified = strtotime($aItem['last_modified_date']);
            if ( $tModified > 0 ) {

                $aItem['last_modified_date'] = date("d.m.Y H:i", $tModified);
            }
            else unset($aItem['last_modified_date']);
        }

        return $aItem;
    }

    public static function getEventByAlias($sAlias){

        $aFilter = array(
            'where_condition' => array(
                'event-alias' => array(
                    'sign' => '=',
                    'value' => $sAlias
                )
            )
        );

        $aItem = EventsMapper::getItem($aFilter);

        if ( $aItem ) {
            $aItem['publication_date'] = date('d.m.Y',strtotime($aItem['publication_date']));

            $tModified = strtotime($aItem['last_modified_date']);
            if ( $tModified > 0 ) {

                $aItem['last_modified_date'] = date("d.m.Y H:i", $tModified);
            }
            else unset($aItem['last_modified_date']);
        }

        return $aItem;
    }

    public static function getEventsList($iPage, $aParameterList){

        $aFilter = array(
            'where_condition' => array(
                'active' => array(
                    'sign' => '=',
                    'value' => 1
                )
            ),
            'order' => array(
                'field' => 'date_start',
                'way' => $aParameterList['order']
            ),
            'limit' => array(
                'start' => ($iPage-1)*$aParameterList['onPage'],
                'count' => $aParameterList['onPage']
            )
        );

        if ( $aParameterList['onMain'] || $aParameterList['archive'] ){

            if( $aParameterList['onMain'] )
                $aFilter['where_condition']['on_main'] = array(
                    'sign' => '=',
                    'value' => 1
                );

            if( $aParameterList['archive'] )
                $aFilter['where_condition']['archive'] = array(
                    'sign' => '=',
                    'value' => 1
                );
        }
        else
            if( !$aParameterList['all_news'] && $aParameterList['section'] ){
                $aFilter['where_condition']['parent_section'] = array(
                    'sign' => 'IN',
                    'value' => $aParameterList['section']
                );
            }

        $aItems = array();
        $aItems = EventsMapper::getItems($aFilter);

        if ( $aItems )
            foreach ( $aItems['items'] as $iKey => &$aItem ){

                $aItem['publication_date'] = date('d.m.Y',strtotime($aItem['publication_date']));
//                $aStart = explode('.',date('d.m.Y',strtotime($aItem['date_start'])));
//                $aEnd   = explode('.',date('d.m.Y',strtotime($aItem['date_end'])));
//                $aNow   = explode('.',date('d.m.Y'));
//                $sNow   = date('Y-m-d h:i:s');
//
//                /* фильтр по датам начала и окончания */
//
//                    /* Даты равны себе и текущей */
//                if(!count(array_diff($aStart, $aEnd)) AND !count(array_diff($aStart, $aNow))) {
//
//                    $aItems['items'][$iKey]['----'] = 'true';
//                    continue;
//                }
//
//                /* Текущая в диапазоне */
//                if($aItem['date_start'] <= $sNow AND $aItem['date_end'] >= $sNow) {
//                    $aItems['items'][$iKey]['----'] = 'true';
//                    continue;
//                }
//
//                $aItems['items'][$iKey]['----'] = 'false';

            }



        return $aItems;
    }

    public static function getRSSData(){

        $aFilter = array(
            'order' => array(
                'field' =>
                'publication_date',
                'way' => 'DESC'
            ),
            'limit' => array(
                'start' => 0,
                'count' => 20
            )
        );

        $aItems = EventsMapper::getItems($aFilter);

        foreach ( $aItems['items'] as &$aValue ){

            $sLink = (!empty($aValue['event-alias']))? skRouter::rewriteURL('['.$aValue['parent_section'].'][EventsModule?event-alias='.$aValue['event-alias'].']'): skRouter::rewriteURL('['.$aValue['parent_section'].'][EventsModule?event_id='.$aValue['id'].']');
            $aValue['title'] = strip_tags($aValue['title']);
            $aValue['title'] = htmlspecialchars($aValue['title']);
            $aValue['publication_date'] = date('r', strtotime($aValue['publication_date']));
            $aValue['unix_time'] = strtotime($aValue['publication_date']);
            $aValue['link'] = $_SERVER['HTTP_HOST'].$sLink;

        }

        return $aItems;
    }

    /**
     * Удаляет события из раздела $iSectionId
     * @param $iSectionId
     * @return bool|int
     */
    public static function removeFromSection($iSectionId) {

        if(!(int)$iSectionId) return false;

        return EventsMapper::removeFromSection($iSectionId);
    }// func

}//class