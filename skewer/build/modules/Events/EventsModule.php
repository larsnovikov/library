<?php

/**
 * @class EventsModule
 * @extends skModule
 * @project Skewer
 * @package kernel
 *
 * @author Andy Mitrich, $Author: acat $
 * @version $Revision: 1173 $
 * @date 06.02.12 11:02 $
 *
 */

class EventsModule extends skModule{

    public $parentSections;
    public $showArchive;
    public $showOnMain;
    public $sortNews = "ASC";
    public $onPage = 10;
    public $listTemplate = 'list.twig';
    public $detailTemplate = 'detail_page.twig';
    private $iCurrentSection = 0;
    private $aParameterList = array(
        'onMain' => 0,
        'archive' => 0,
        'all_news' => 0,
    );


    public function init() {

        $this->setParser(parserTwig);
        $this->iCurrentSection = $this->getEnvParam('sectionId');
        $this->aParameterList['onPage'] = $this->onPage;
        if ( $this->showOnMain ) $this->aParameterList['all_news'] = 1;

        if ( $this->parentSections ) $this->aParameterList['section'] = $this->parentSections;
        else $this->aParameterList['section'] = $this->iCurrentSection;

        if ( $this->iCurrentSection==skConfig::get('section.main') ) $this->aParameterList['onMain'] = 1;
        if ( $this->showArchive ) $this->aParameterList['archive'] = 1;
        if ( $this->sortNews ) $this->aParameterList['order'] = $this->sortNews;

        return true;
    }

    public function execute() {

        $iPage          = $this->getInt('page', 1);
        $iEventId       = $this->getInt('event_id', 0);
        $sEventAlias    = $this->getStr('event-alias', '');
        $sDateFilter    = $this->getStr('date');

        if ( $iEventId || $sEventAlias ) {
        /* Обрабатываем детальную */

            $aEvent = array();
            // Получаем мероприятие по ID
            if ( $iEventId )
                $aEvent = EventsApi::getEventById($iEventId);

            if ( $sEventAlias )
                $aEvent = EventsApi::getEventByAlias($sEventAlias);

//            $hideDate = Parameters::getByName($this->iCurrentSection,'content','hideDate');
//            if( isset($hideDate['value']) && $hideDate['value'] )
                unset($aEvent['last_modified_date']);

            if ( sizeof($aEvent) ){

                $this->setData('aEvent', $aEvent);

                $oPage = $this->getProcess('out', psAll);
                if( $oPage->getStatus() != psComplete ) return psWait;
                $oPage->setData('staticContent', array());
                $aTitle = $oPage->getData('seoTitle');
                if(isSet($aTitle['value'])){
                    if(isset($aEvent['title']))
                        $aTitle['value'] = $aEvent['title'].' - '.$aTitle['value'];
                    $oPage->setData('seoTitle',$aTitle);
                }
            }

            $this->setTemplate($this->detailTemplate);

            // Метатеги для списка новостей
            $SEOData = $this->getEnvParam('SEOTemplates', array());
            $SEOData['eventsDetail'] = array('title'=>'','description'=>'','keywords'=>'');
            if($aCurSEOData = SEOData::get('events',$iEventId)){
                $SEOData['eventsDetail']['title'] = $aCurSEOData['title'];
                $SEOData['eventsDetail']['description'] = $aCurSEOData['description'];
                $SEOData['eventsDetail']['keywords'] = $aCurSEOData['keywords'];
            }
            $this->setEnvParam('SEOTemplates', $SEOData);
            $SEOVars = $this->getEnvParam('SEOVars', array());
            $SEOVars['Название события'] = $aEvent['title'];
            $SEOVars['название события'] = mb_strtolower($aEvent['title']);
            $this->setEnvParam('SEOVars', $SEOVars);

            // добавляем элемент в pathline
            $oProcessPathLine = $this->getProcess('out.pathLine', psAll);
            if($oProcessPathLine instanceof skProcess){
                $oProcessPathLine->setStatus(psNew);
                $this->setEnvParam('pathline_additem',
                    array( 'id' => $aEvent['id'],
                        'title' => $aEvent['title'],
                        'alias_path' => $aEvent['event-alias'],
                        'link' => ''
                    )
                );
            }

        }
        else {

            $aNewsList = array();
            $this->setTemplate($this->listTemplate);

            // Получаем список мероприятий

            $aEventsList = EventsApi::getEventsList($iPage, $this->aParameterList);

            if ( !sizeof($aEventsList['items']) ) return psComplete;

            /* ---- */
            foreach ( $aEventsList['items'] as $iKey => $aItem ) {

                $aStart = explode('.',date('d.m.Y',strtotime($aItem['date_start'])));
                $aEnd   = explode('.',date('d.m.Y',strtotime($aItem['date_end'])));
                $aNow   = explode('.',date('d.m.Y'));
                $sNow   = date('Y-m-d h:i:s');

                /* фильтр по датам начала и окончания */

                /* Даты равны себе и текущей */
                if(!count(array_diff($aStart, $aEnd)) AND !count(array_diff($aStart, $aNow)))
                continue;

                /* Текущая в диапазоне */
                if($aItem['date_start'] <= $sNow AND $aItem['date_end'] >= $sNow)
                continue;

                //$aItems['items'][$iKey]['----'] = 'false';

                unSet($aEventsList['items'][$iKey]);
            }// each item
            /* ---- */


            $this->setData('aEventsList', $aEventsList['items']);

            $this->setData('events_section', $this->parentSections);

            $aURLParams = array();

            if(!empty($sDateFilter)) $aURLParams['date'] = $sDateFilter;

            $this->getPageLine($iPage, $aEventsList['count'], $this->iCurrentSection, $aURLParams, array('onPage'=>$this->aParameterList['onPage']));
        }

        /** @var skProcess $page  */
        // Откладываем запуск (перезапускаем) SEOMetatags после себя
        $page = $this->getProcess('out.SEOMetatags', psAll);
        if($page instanceof skProcess) $page->setStatus(psNew);

        return psComplete;
    }// func
}//class