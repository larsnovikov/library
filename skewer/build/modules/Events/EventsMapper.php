<?php

/**
 * @class EventsMapper
 * @extends skModule
 * @project Skewer
 * @package kernel
 *
 * @author Andy Mitrich, $Author: sokol $
 * @version $Revision: 532 $
 * @date 06.02.12 11:06 $
 *
 */

class EventsMapper extends skMapperPrototype{

    /**
     * @var string Имя таблицы, с которой работает маппер
     */
    protected  static $sCurrentTable = 'events';

    /**
     * @static Метод возвращает имя рабочей таблицы
     * @return string
     */
    public static function getCurrentTable() {

        return self::$sCurrentTable;
    }

    /**
     * @static Метод возвращает список полей рабочей таблицы
     * @return array
     */
    public static function getParametersList() {

        return self::$aParametersList;
    }

    /**
     * @var array Конфигурация полей таблицы
     */
    protected static $aParametersList = array(
        'id' => 'i:hide:Номер',
        'event-alias' => 's:str:Псевдоним',
        'parent_section' => 'i:hide:Родительский раздел',
        'publication_date' => 's:date:Дата публикации',
        'publication_time' => 's:time:Время публикации',
        'date_start' => 's:date:Дата начала',
        'date_end' => 's:date:Дата окончания',
        'title' => 's:str:Название',
        'announce' => 's:wyswyg:Анонс',
        'full_text' => 's:wyswyg:Полный текст',
        'active' => 'i:check:Активность',
        'archive' => 'i:check:Архив',
        'on_main' => 'i:check:На главной',
        'hyperlink' => 's:str:Ссылка',
        'last_modified_date' => 's:str:Дата последнего изменения'
    );

    // дополнительный набор параметров
    protected static function getAddParamList() {
       return array(
           'id' => array(
               'view' => 'hide',
               'listColumns' => array(
                   'hidden' => true
               )
           ),
           'title' => array(
               'listColumns' => array( 'flex' => 1 )
           ),
           'active' => array(
               'listColumns' => array( 'width' => 75 )
           ),
           'on_main' => array(
               'listColumns' => array( 'width' => 75 )
           ),
           'archive' => array(
               'listColumns' => array( 'width' => 75 )
           )
       );
    }


    public static function removeFromSection($iSectionId) {

        global $odb;

        $sQuery = '
            DELETE
            FROM `[table_name:q]`
            WHERE `parent_section`=[section:i];
        ';

        return $odb->query($sQuery, array(
            'table_name' => static::getCurrentTable(),
            'section' => $iSectionId,
        ));

    }// func

    public static function checkAlias($sAlias){

        global $odb;

        $aData = array('alias' => $sAlias);

        $sQuery = "
            SELECT
                `id`
            FROM
                `events`
            WHERE
                `event-alias` = [alias:s];";

        $rResult = $odb->query($sQuery, $aData);

        $aItems = array();

        while( $aRow = $rResult->fetch_assoc() ){

            $aItems[$aRow['id']] = $aRow['id'];
        }

        return $aItems;
    }


}//class