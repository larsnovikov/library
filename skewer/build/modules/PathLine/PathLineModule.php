<?php
class PathLineModule extends skModule {

    var $stopSections = '';
    var $withMain = false;
    public $additionItem = array();
	
    public function init() {
        $this->setParser(parserTwig);
    }// func
	
	public function execute() {

        $stopSections = explode(',', $this->stopSections);
        $to = $this->getEnvParam('sectionId');
        $from = skConfig::get('section.sectionRoot');

        $tree = new Tree();
        $parents = $tree->getSectionParents($to, $from);
        $filtredParents = array();

        if(count($parents))  {
            while($item = array_shift($parents)){
                if(!in_array($item,$stopSections)) array_unshift($filtredParents, $item);
                else break;
            } // while
        }
        

        //if( count($filtredParents) )
        if( $to != skConfig::get('section.main') )
            array_push($filtredParents, $to);
        $items = $tree->getByIdList($filtredParents);

        // add items
        if( $add_item = $this->getEnvParam('pathline_additem') )
            $items[] = $add_item;

        if( !empty($items) )
        foreach($items as &$item){
            $item['href'] = ($item['link']) ? $item['link'] : $item['alias_path'];
            $item['selected'] = ($item['id']==$to)&&(!$add_item) || isset($add_item['id'])&&($add_item['id']==$item['id']);
        } // foreach

        if( $this->withMain && is_array($items) && count($items) )
            $this->setData('main_page', 1);

        if( $this->withMain || (count($items) > 1))
            $this->setData('items', $items);

        $this->setTemplate('pathLine.twig');

        return psComplete;
    }// func

    public function shutdown() {
        
    }// func
}// class
