<?php

/**
 * @class FormsMapper
 * @extends skModule
 * @project Skewer
 * @package kernel
 *
 * @author Andy Mitrich, $Author: sapozhkov $
 * @version $Revision: 1346 $
 * @date 16.01.12 10:41 $
 *
 */

require_once BUILDPATH.'modules/Forms/FormsEntity.php';
require_once BUILDPATH.'modules/Forms/Forms2ParamsEntity.php';

class FormsMapper extends skMapperPrototype{

    /* Common Part */

    /**
     * @var string Имя таблицы, с которой работает маппер
     */
    protected  static $sCurrentTable =   'forms';

    /**
     * @static Метод возвращает имя рабочей таблицы
     * @return string
     */
    public static function getCurrentTable() {

        return self::$sCurrentTable;
    }

    /**
     * @static Метод возвращает список полей рабочей таблицы
     * @return array
     */
    public static function getParametersList() {

        return self::$aParametersList;
    }

    /**
     * Имя ключевого поля
     * @var string
     */
    protected static $sKeyFieldName = 'form_id';

    public static function getKeyFieldName() {
        return static::$sKeyFieldName;
    }

    /**
     * @var array Конфигурация полей таблицы
     */
    protected static $aParametersList = array(
        'form_id' => 'i:hide:ИД формы',
        'form_title' => 's:str:Название формы',
        'form_section' => 'i:hide:Раздел для показа',
        'form_handler_type' => 's:str:Тип обработчика',
        'form_handler_value' => 's:str:Значение обработчика',
        'form_active' => 'i:check:Форма активна',
        'form_captcha' => 'i:check:Включить капчу',
        'form_is_template' => 'i:check:Форма является шаблоном',
        'form_answer' => 'i:check:Отправлять автоответ на почту пользователя',
        'form_redirect' => 's:str:Адрес страницы с результатом успешной отправки формы'
    );

    /* Admin Interface */

    /**
     * @static
     * @param bool $isTemplates
     * @return array
     */
    public static function getTemplatesForForms($isTemplates = true){

        global $odb;

        $sQuery = "
            SELECT
                `form_id`,
                `form_title`
            FROM
                `forms`".
            ( $isTemplates ? "WHERE `form_is_template`=1;" : '' );

        $rResult = $odb->query($sQuery);

        $aTemplateForms = array();
        while($aRow = $rResult->fetch_array(MYSQLI_ASSOC)){
            //$aRow['form_title'] = 'Шаблон формы: '.$aRow['form_title'];
            $aRow['form_id'] = (int)$aRow['form_id'];
            $aTemplateForms[] = $aRow;
        }

        return $aTemplateForms;
    }

    public static function delForm($iFormId){

        if ( !$iFormId ) return false;

        global $odb;

        $aData['form_id'] = $iFormId;

        $sQuery = "
            DELETE
            FROM
                `forms`
            WHERE
                `form_id` = [form_id:i];";

        return $odb->query($sQuery, $aData);
    }

    public static function delFormBySection($mSectionId){

        if ( !$mSectionId || (is_array($mSectionId) && empty($mSectionId)) ) return false;

        global $odb;

        if( !is_array($mSectionId) )
            $aData['form_section'] = $mSectionId;
        else
            $aData['form_section'] = implode(',',$mSectionId);

        $sQuery = "
            DELETE
            FROM
                `forms`
            WHERE
                `form_section` IN ([form_section:q]);";

        return $odb->query($sQuery, $aData);
    }

    public static function updForm($aInputData){

        global $odb;

        // Сохраняем опрос
        static::saveItem($aInputData);

        return $odb->insert_id;
    }// function updForm()

    /* Public Interface */

    public static function getFormBySectionId($iSectionId){

        if ( !$iSectionId ) return false;

        global $odb;

        $aData = array(
            'section_id' => $iSectionId,
            'active' => 1
        );

        $sQuery = "
            SELECT
                f.*
            FROM
                `forms` AS f
            INNER JOIN `forms_section` AS fs ON fs.form_id=f.form_id
            WHERE
                fs.section_id = [section_id:i] AND
                f.form_active = [active:i]
            LIMIT 0, 1;";

        $aResult = $odb->getRow($sQuery, $aData);

        if ( !$aResult ) return false;

        $oForm =  new FormsEntity();

        $oForm->setId($aResult['form_id']);
        $oForm->setTitle($aResult['form_title']);
        $oForm->setSectionId($aResult['form_section']);
        $oForm->setHandlerType($aResult['form_handler_type']);
        $oForm->setHandlerValue($aResult['form_handler_value']);
        $oForm->setActive($aResult['form_active']);
        $oForm->setCaptcha($aResult['form_captcha']);
        $oForm->setIsTemplate($aResult['form_is_template']);
        $oForm->setFormRedirect($aResult['form_redirect']);
        $oForm->setFormAnswer($aResult['form_answer']);
        $oForm->setParams(array());

        return $oForm;
    }

    public static function checkFormsBySectionId($iSectionId){

        if ( !$iSectionId ) return false;

        global $odb;

        $aData = array(
            'section_id' => $iSectionId
        );

        $sQuery = "
            SELECT
                f.form_id
            FROM
                `forms` AS f
            WHERE
                f.form_section = [section_id:i]
            LIMIT 0, 1;";

        $aResult = $odb->getRow($sQuery, $aData);

        return ( sizeof($aResult) )? $aResult['form_id']: false;
    }

    public static function getFormById($iFormId){

        if ( !$iFormId ) return false;

        global $odb;

        $aData = array('form_id' => $iFormId);

        $sQuery = "
            SELECT
                f.*
            FROM
                `forms` AS f
            WHERE
                f.form_id = [form_id:i]
            LIMIT 0, 1;";

        $aForm = $odb->getRow($sQuery, $aData);

        if ( !(sizeof($aForm)) ) return false;

        $oForm =  new FormsEntity();

        $oForm->setId($aForm['form_id']);
        $oForm->setTitle($aForm['form_title']);
        $oForm->setSectionId($aForm['form_section']);
        $oForm->setHandlerType($aForm['form_handler_type']);
        $oForm->setHandlerValue($aForm['form_handler_value']);
        $oForm->setActive($aForm['form_active']);
        $oForm->setCaptcha($aForm['form_captcha']);
        $oForm->setIsTemplate($aForm['form_is_template']);
        $oForm->setFormAnswer($aForm['form_answer']);
        $oForm->setFormRedirect($aForm['form_redirect']);
        $oForm->setParams(array());

        return $oForm;
    }

    public static function getFormByName($sFormName){

        if ( !$sFormName ) return false;

        global $odb;

        $aData = array('form_name' => $sFormName);

        $sQuery = "
            SELECT
                f.*
            FROM
                `forms` AS f
            WHERE
                f.form_name = [form_name:s]
            LIMIT 0, 1;";

        $aForm = $odb->getRow($sQuery, $aData);

        if ( !(sizeof($aForm)) ) return false;

        $oForm =  new FormsEntity();

        $oForm->setId($aForm['form_id']);
        $oForm->setTitle($aForm['form_title']);
        $oForm->setSectionId($aForm['form_section']);
        $oForm->setHandlerType($aForm['form_handler_type']);
        $oForm->setHandlerValue($aForm['form_handler_value']);
        $oForm->setActive($aForm['form_active']);
        $oForm->setCaptcha($aForm['form_captcha']);
        $oForm->setIsTemplate($aForm['form_is_template']);
        $oForm->setFormRedirect($aForm['form_redirect']);
        $oForm->setFormAnswer($aForm['form_answer']);
        $oForm->setParams(array());

        return $oForm;
    }

    protected static function getAddParamList(){

        $aSettings = skConfig::get('buildConfig.Page.modules.Forms.settings');

        return array(
            'form_id' => array(
                'view' => 'hide',
                'listColumns' => array(
                    'hidden' => true
                )
            ),
            'form_handler_type' => array(
                'view' => 'select',
                'valueField' => 'key',
                'displayField' => 'val',
                'store' => array(
                    'fields' => array('key','val'),
                    'data' => array(array('key'=>'toMail','val'=>'toMail'),array('key'=>'toMethod','val'=>'toMethod'))
                )
            ),
            'form_handler_value' => array(
                'title' => 'E-mail адрес получателя'
            ),
            'form_section' => ExtForm::getDesc4SelectFromArray(self::getSectionTitle())
        );
    }// function getAddParamList()


    public static function getSectionTitle(){

        $oTree = new Tree();
        $aSections = $oTree->getSectionsInLine(skConfig::get('section.sectionRoot'));
        unset($oTree);
        $aResult = array();

        foreach( $aSections as $aSection )
            $aResult[$aSection['id']] = $aSection['title'];
        /*
        $aResult = array();

        // составление фильтра
        $aFilter = array(
            'name' => 'object',
            'value' => 'FormsModule',
            'rec' => true,
        );

        // выборка
        $aParams = Parameters::getByFilter($aFilter);
                   Tree::get
        foreach( $aParams as $aSection ){

            $aResult[$aSection['parent']] = Tree::getTitleById( $aSection['parent'] );

        } */

        return $aResult;
    }

}//class
