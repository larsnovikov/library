<?php

/**
 * @class FormsEntity
 * @extends skModule
 * @project Skewer
 * @package kernel
 *
 * @author Andy Mitrich, $Author: acat $
 * @version $Revision: 1173 $
 * @date 17.01.12 18:15 $
 *
 */

require_once BUILDPATH.'modules/Forms/Entity.php';
class FormsEntity extends Entity{

    /*getters*/
    public function getId() {
        return $this->data['form_id'];
    }
    public function getTitle() {
        return $this->data['form_title'];
    }
    public function getSectionId() {
        return $this->data['form_section'];
    }
    public function getHandlerType() {
        return $this->data['form_handler_type'];
    }
    public function getHandlerValue() {
        return $this->data['form_handler_value'];
    }
    public function getActive() {
        return $this->data['form_active'];
    }
    public function getCaptcha() {
        return $this->data['form_captcha'];
    }
    public function getIsTemplate() {
        return $this->data['form_is_template'];
    }
    public function getFormAnswer() {
        return $this->data['form_answer'];
    }
    public function getFormRedirect() {
        return $this->data['form_redirect'];
    }

    /**
     * @return Forms2ParamsEntity[]
     */
    public function getParams() {
        return $this->data['form_params_objects'];
    }

    // Динамически заполняется при построении формы
    public function getRules(){
        return $this->data['rules'];
    }

    /*setters*/
    public function setId($val) {
        $this->data['form_id'] = (int)$val;
    }
    public function setTitle($val) {
        $this->data['form_title'] = (String)$val;
    }
    public function setSectionId($val) {
        $this->data['form_section'] = (int)$val;
    }
    public function setHandlerType($val) {
        $this->data['form_handler_type'] = (String)$val;
    }
    public function setHandlerValue($val) {
        $this->data['form_handler_value'] = (String)$val;
    }
    public function setActive($val) {
        $this->data['form_active'] = (int)$val;
    }
    public function setCaptcha($val) {
        $this->data['form_captcha'] = (int)$val;
    }
    public function setIsTemplate($val) {
        $this->data['form_is_template'] = (int)$val;
    }
    public function setFormAnswer($val) {
        $this->data['form_answer'] = (int)$val;
    }
    public function setParams($val) {
        $this->data['form_params_objects'] = $val;
    }
    public function setRules($val) {
        $this->data['rules'] = $val;
    }
    public function setFormRedirect($val) {
        $this->data['form_redirect'] = $val;
    }

    public function findEmailField() {

        $mResult = false;

        if( isset($this->data['form_params_objects']) )
        foreach( $this->data['form_params_objects'] as $oCurParam ) {

            if( isset($oCurParam->data['param_validation_type']) && $oCurParam->data['param_validation_type'] == 'email' )
                $mResult = $oCurParam->data['param_default'];

        }

        return $mResult;
    }

}//class