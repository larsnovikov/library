<?php

$aConfig['name']     = 'Forms';
$aConfig['title']     = 'Конструктор форм';
$aConfig['version']  = '1.000a';
$aConfig['description']  = 'Модуль конструктора форм';
$aConfig['revision'] = '0001';
$aConfig['layer']     = 'Page';

$aConfig['css'] = array(
);

$aConfig['js'] = array(
    '/skewer_build/modules/Forms/js/formValidator.js',
    '/skewer_build/modules/Forms/js/jquery.validate.min.js',
    '/skewer_build/modules/Forms/js/message_ru.js'
);

$aConfig['settings'] = array(
    'validation_types' => array(
        'text' => 'Текст',
        'number' => 'Целые и дробные числа',
        'digits' => 'Целые числа',
        'url' => 'Адрес URL',
        'date' => 'Дата',
        //'creditcard' => 'Номер кредитной карты',
        'email' => 'E-mail'
    ),
    'field_types' => array(
        '1' => 'Текстовое поле',
        '2' => 'Многострочное текстовое поле',
        '5' => 'Галочка',
        '6' => 'Загрузка файла',
        '8' => 'Группа переключателей',
        '9' => 'Выпадающий список',
        '11' => 'Разделитель',
        '13' => 'Календарь'
    ),
    'handler_types' => array(
        'toMail' => 'Отправить на e-mail'
    )
);

return $aConfig;