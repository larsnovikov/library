<?php
/**
 * @class Forms2SectionMapper
 * @extends skModule
 * @project Skewer
 * @package kernel
 *
 * @author kolesnikov, $Author: $
 * @version $Revision: $
 * @date $Date: $
 *
 */

class Forms2SectionMapper extends skMapperPrototype {

    /**
     * @var string Имя таблицы, с которой работает маппер
     */
    protected  static $sCurrentTable = 'forms_section';

    /**
     * @static Метод возвращает имя рабочей таблицы
     * @return string
     */
    public static function getCurrentTable() {

        return self::$sCurrentTable;
    }

    /**
     * @static Метод возвращает список полей рабочей таблицы
     * @return array
     */
    public static function getParametersList() {

        return self::$aParametersList;
    }

    /**
     * @var array Конфигурация полей таблицы
     */
    protected static $aParametersList = array(
        'id' => 'i:hide:ИД записи',
        'form_id' => 'i:hide:ИД формы',
        'section_id' => 's:str:ИД раздела'
    );

    /**
     * Возвращает идентификатор формы для раздела $iSectionId
     * @param int $iSectionId Идентификатор раздела
     * @return int|bool
     */
    public static function getFormBySectionId($iSectionId) {

        $aFilter = array('where_condition' => array('section_id' => array (
            'sign' => '=',
            'value' => $iSectionId)
        ));

        $aItem = self::getItem($aFilter);

        return ( $aItem && isSet($aItem['form_id']) ) ? $aItem['form_id'] : false;
    }

    public static function delFormBySection($iSectionId) {

        global $odb;

        $sQuery = '
            DELETE
            FROM `[table_name:q]`
            WHERE `section_id`=[section_id:i];
        ';

        $odb->query($sQuery, array(
            'table_name' => self::$sCurrentTable,
            'section_id' => $iSectionId
        ));

        return $odb->affected_rows;
    }


    public static function linkFormToSection($iFormId, $iSectionId) {

        $aFilter = array('form_id'=>$iFormId, 'section_id'=>$iSectionId);

        self::saveItem( $aFilter );

        return true;
    }

}
