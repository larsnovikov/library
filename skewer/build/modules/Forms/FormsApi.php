<?php

/**
 * @class FormsApi
 * @extends skModule
 * @project Skewer
 * @package kernel
 *
 * @author Andy Mitrich, $Author: acat $
 * @version $Revision: 1269 $
 * @date 16.01.12 10:01 $
 *
 */

/** @noinspection PhpIncludeInspection */
require_once BUILDPATH.'modules/Forms/FormsEntity.php';
require_once BUILDPATH.'modules/Forms/Forms2ParamsEntity.php';

class FormsApi {

    private $aParamTypes       = array();
    private $defMaxLength      = 255;
    private $aFormHandlers     = array();
    private $aValidRules        = array();
    private $aParamValidations = array();
    private $aFormHandlerTypes = array();

    function __construct ($aSettings = array()) {

        // Пишем в переменные класса настройки из конфига
        if ( isset($aSettings['validation_types']) )
            $this->aParamValidations = $aSettings['validation_types'];

        if ( isset($aSettings['handler_types']) )
            $this->aFormHandlerTypes = $aSettings['handler_types'];

        if ( isset($aSettings['field_types']) )
            $this->aParamTypes = $aSettings['field_types'];

        return true;
    }// constructor

    /*   Forms Operate   */
    /**
     * Метод выборки формы по её ID
     * @param $iSectionId
     * @param bool $bGetParams
     * @return bool|FormsEntity
     */
    public function getFormBySectionId($iSectionId, $bGetParams = false) {

        $iSectionId = (int)$iSectionId;
        if(!$iSectionId) return false;

        // Выборка заголовочной информации по форме
        $oForm = FormsMapper::GetFormBySectionId($iSectionId);

        if ( !($oForm instanceof FormsEntity) ) return false;

        if(!$bGetParams) return $oForm;

        // Выборка параметров формы
        $oForm = Forms2ParamsMapper::getParams($oForm);

        return $oForm;
    }// func

    public function getFormById($iFormId, $bGetParams = false) {

        $iFormId = (int)$iFormId;
        if( !$iFormId ) return false;

        $oForm = FormsMapper::getFormById($iFormId);

        if(!$bGetParams) return $oForm;

        $oForm = Forms2ParamsMapper::getParams($oForm);

        return $oForm;
    }// func

    public function getFormByName($sFormName, $bGetParams = false) {

        if( !$sFormName ) return false;

        $oForm = FormsMapper::getFormByName($sFormName);

        if(!$bGetParams) return $oForm;

        $oForm = Forms2ParamsMapper::getParams($oForm);

        return $oForm;
    }// func

    public function validateParam(Forms2ParamsEntity $oParam, $sValue) {

        if ( is_array($sValue) ) return false;
        if( !($oParam instanceof Forms2ParamsEntity) ) return false;

        $iMin = 1;
        $iMax = ( $oParam->getMaxLength() )? $oParam->getMaxLength(): $this->defMaxLength;

        if( empty($sValue) AND !$oParam->getRequired() ) return true;

        switch($oParam->getValidType()) {

            case 'text':
                if (mb_strlen($sValue,'UTF-8') >= $iMin AND mb_strlen($sValue,'UTF-8') <= $iMax) return true;
            break;

            case 'digits':
                if (preg_match("/^\d{".$iMin.','.$iMax."}$/",$sValue)) return true;
            break;

            case 'number':
                if (preg_match("/^[\-]?\d+[\.]?\d*$/",$sValue)) return true;
            break;

            case 'email':
                if (preg_match("/^[\da-z\_\-\.\+]+@[\da-z_\-\.]+\.[a-z]{2,5}$/i",$sValue)) return true;
            break;

            case 'url':
                if (filter_var($sValue, FILTER_VALIDATE_URL) !== false)
                    if (mb_strlen($sValue,'UTF-8') >= $iMin AND mb_strlen($sValue,'UTF-8') <= $iMax) return true;
            break;

            case 'phone':
                if (mb_strlen($sValue,'UTF-8') >= $iMin AND mb_strlen($sValue,'UTF-8') <= $iMax) return true; # сейчас только как текст
            break;

            case 'date':
                if (mb_strlen($sValue,'UTF-8') >= $iMin AND mb_strlen($sValue,'UTF-8') <= $iMax) return true; # сейчас только как текст
            break;

            case 'creditcard':
                if (mb_strlen($sValue,'UTF-8') >= $iMin AND mb_strlen($sValue,'UTF-8') <= $iMax) return true; # сейчас только как текст
            break;

            case 'login':
                if (preg_match("/^[\da-z\_\-]{".$iMin.','.$iMax."}$/i",$sValue)) return true;
            break;

            case 'md5':
                if (preg_match("/^[\da-z]{32}$/i",$sValue)) return true;
            break;

            default:
                return false;
        }// switch

        return false;
    }// func

    public function sendFormToMail(FormsEntity $oForm, $sMailFrom = '', $sLetterTemplate = 'letter.twig', $sTemplateDir = ''){

        if( empty($sMailFrom) ) return false;
        if( empty($sLetterTemplate) ) return false;

        $sBody = skParser::parseTwig($sLetterTemplate,array('oForm'=>$oForm), $sTemplateDir);

        // add attach file
        $aAttachFile = array();
        $aFormParams = $oForm->getParams();
        if( isset($aFormParams) && sizeof($aFormParams) )
            foreach($aFormParams as $oParam)
                if( $oParam->getTypeTitle() == 'file' ) {

                    $sCurItem = $oParam->getDefault();
                    $aAttachFile[$oParam->getTitle()] = $sCurItem;
                }


        if(!$oForm->getHandlerValue()) return false;

        if( count($aAttachFile) )
            $sRes = skMailer::sendMailWithAttach($oForm->getHandlerValue(), $sMailFrom, $oForm->getTitle(), $sBody, $aAttachFile, 'utf-8');
        else
            $sRes = skMailer::sendMail($oForm->getHandlerValue(), $sMailFrom, $oForm->getTitle(), $sBody, 'utf-8');

        return $sRes;
    }

    public function sendFormToMethod(FormsEntity $oForm, $sMethod = ''){

        if( !$sMethod ) return false;

        list($sObjectName, $sMethodName) = explode('.',$sMethod);

        if(!isset($sObjectName) || !isset($sMethodName))
            throw new Exception('Формат не верный');

        $oCurClass = new ReflectionClass($sObjectName);

        if( !($oCurClass instanceof ReflectionClass) )
            throw new Exception('Не создан класс');

        if( $oCurClass->getParentClass()->name != 'ServicePrototype')
            throw new Exception('Попытка запуска неразрешенного класса');

        $oCurObj = new $sObjectName();

        if( !method_exists($oCurObj,$sMethodName) )
            throw new Exception('Попытка запуска несуществующего метода');

        $sRes = call_user_func_array(array($oCurObj,$sMethodName),array($oForm));

        return $sRes;
    }

    public function getValidRules() {
        return $this->aValidRules;
    }// func

    private function buildValidRules(FormsEntity $oForm) {

        $aParams = array();
        $aParams = $oForm->getParams();

        $aOut = array();

        /* @var $oParam Forms2ParamsEntity */
        foreach ( $aParams as $oParam ){

            $aTempOut = array();
            $aTempOut['required']  = ($oParam->getRequired()) ? true: false;
            $aTempOut['maxlength'] = $oParam->getMaxLength();

            if($oParam->getValidType() != 'text')
               $aTempOut[$oParam->getValidType()] = true;

            $aOut[$oParam->getName()] = $aTempOut;
        }
        return $aOut;
    }// func

    /**
     * Метод для создания объекта формы на определенной модели
     */
    public function createFormByModel(array $aModel){

        $oForm = new FormsEntity();

        $aFormParams = ( isset($aModel['form_params']) )? $aModel['form_params']: array();
        unset($aModel['form_params']);
        $oForm->setData($aModel);

        if ( sizeof($aFormParams) ){

            $aParams = array();

            foreach( $aFormParams as $aFormParameter ){

                $oParam = new Forms2ParamsEntity();
                $oParam->setData($aFormParameter);
                $aParams[] = $oParam;
            }

            $oForm->setParams($aParams);
        }

        $this->buildFormRules($oForm);

        return $oForm;
    }

    /**
     * Метод возвращает массив для построения формы
     */
    public function buildFormRules(FormsEntity $oForm){

        $aParams = array();
        $aParams = $oForm->getParams();
        $aRules = array();

        /* @var $oParam Forms2ParamsEntity */
        foreach ( $aParams as $oParam ){

            $aTempRow = array();
            $aTempRow['required']  = ($oParam->getRequired()) ? true: false;
            $aTempRow['maxlength'] = $oParam->getMaxLength();

            if($oParam->getValidType() != 'text')
                $aTempRow[$oParam->getValidType()] = true;

            $aRules['rules'][$oParam->getName()] = $aTempRow;
        }

        $this->aValidRules = $aRules['rules'];

        // Если есть капча
        if( $oForm->getCaptcha()) {

            $aRules['rules']['captcha'] = array(
                'required'=>1,
                'maxlength'=>50,
                'digits'=>1
            );
        }

        $oForm->setRules(json_encode($aRules));

        return $oForm;
    }

    /**
     * Метод, позволяющий построить форму на основании
     * @param $iSectionId
     * @return bool|FormsEntity
     */
    public function buildFormBySection( $iSectionId ){

        // Получить форму по ID раздела
        $oForm = $this->getFormBySectionId($iSectionId, true);

        // Если форма не выбрана - закончить выполнение
        if ( !($oForm instanceof FormsEntity) ) return false;

        return $this->buildFormRules($oForm);
    }

    public function buildFormByName( $sFromName ){

        // Получить форму по ID раздела
        $oForm = $this->getFormByName($sFromName, true);

        // Если форма не выбрана - закончить выполнение
        if ( !($oForm instanceof FormsEntity) ) return false;

        return $this->buildFormRules($oForm);
    }

    public function buildForm($mForm, $aData = array(), &$sErrorHandler = ''){

        try{
            if ( is_numeric($mForm) )
                $mForm = $this->getFormById($mForm, true);

            if ( !($mForm instanceof FormsEntity) ){

                $sErrorHandler = 'Not Form';
                return true;
            }

            $mForm = $this->buildFormRules($mForm);

            return $this->validateForm($mForm, $aData, $sErrorHandler);
        }
        catch(Exception $e){

            $sErrorHandler = $e->getMessage();
        }
    }

    public function validateForm(FormsEntity $oForm, $aInputData = '', &$sErrorHandler = ''){

        try{
            #проверяем капчу - если надо
            if( $oForm->getCaptcha() ){

                if ( isset($aInputData['captcha']) ){

                    $sCaptcha = $aInputData['captcha'];

                    if( !$sCaptcha OR !Captcha::check_captcha($sCaptcha,false) ) {

                        $sErrorHandler = 'captcha_error';
                        return false;
                    }
                }
                else {

                    $sErrorHandler = 'send_captcha_error';
                    return false;
                }
            }
            # принять и обработать данные (дублирующая проверка на серверной стороне)
            $aNewParams = array();
            $aFormParams = $oForm->getParams();

            if( isset($aFormParams) && sizeof($aFormParams) )
                foreach($aFormParams as &$oParam) {

                    /** @var $oParam Forms2ParamsEntity  */
                    $sVal = '';

                    switch ( $oParam->getType()) {

                        case '6': //link file

                            $sParamName = $oParam->getName();

                            if(is_uploaded_file($_FILES[$sParamName]['tmp_name'])) {

                                # Ошибка загрузки
                                if($_FILES[$sParamName]['error'])
                                    throw new Exception('fileupload_error');

                                # проверяем размер # размеры недопустим, выходим
                                if($_FILES[$sParamName]['size'] > 4*1024*1024)//$oParam->getMaxLength()
                                    throw new Exception('file_maxsize_error');

                                $fc = file_get_contents($_FILES[$sParamName]['tmp_name']);

//                                # создаем файл
//                                $sFileName = skFiles::generateUniqFileName(PRIVATE_FILEPATH, $_FILES[$sParamName]['name']);
//                                //$sFileName = MINC_BASEPATH.$this->oFB->getPathUpload().$sFileName.'.'.$sExt;
//                                if(!move_uploaded_file($_FILES[$oParam->getName()]['tmp_name'], $sFileName))
//                                    throw new Exception('fileupload_error');

                                //$aUploadedFiles[] = $_FILES[$sParamName]['name'];
                                $oParam->setTitle($_FILES[$sParamName]['name']); // финт ушами =) todo
                                //$sVal = $sFileName = str_ireplace(ROOTPATH, WEBROOTPATH, $sFileName);
                                $sVal = $fc;
                            }// if

                        break;

                        default:

                            if(isSet($aInputData[$oParam->getName()])) {

                                $sVal = $aInputData[$oParam->getName()];
                                // Валидация параметров на стороне сервера
                                if( $this->validateParam($oParam, $sVal) === false ) {
                                    $sErrorHandler = 'validation_error';
                                    return false;
                                }
                            }// isset param
                        break;
                    }// switch

                    $oParam->setDefault($sVal);
            }// foreach

            if( !sizeof($aFormParams) ) {
                $sErrorHandler = 'data_error';
                return false;
            }

            $oForm->setParams($aFormParams);

            return $oForm;
        }
        catch(Exception $e){

            $sErrorHandler = $e->getMessage();
        }
    }

} //class