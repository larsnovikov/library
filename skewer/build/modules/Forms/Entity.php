<?php

/**
 * @class Entity
 * @extends skModule
 * @project Skewer
 * @package kernel
 *
 * @author Andy Mitrich, $Author: sapozhkov $
 * @version $Revision: 6 $
 * @date 18.01.12 10:38 $
 *
 */

class Entity {

    protected $data = array();

    public function getData() {
        return $this->data;
    }// func

    public function setData($aData) {
        if(!is_array($aData)) return false;
        $this->data = $aData;
        return true;
    }// func
} //class