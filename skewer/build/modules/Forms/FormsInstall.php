<?php

/**
 * @class FormsInstall
 * @extends skModule
 * @project Skewer
 * @package Modules
 *
 * @author Andy Mitrich, $Author: sapozhkov $
 * @version $Revision: 6 $
 * @date 19.01.12 11:37 $
 *
 */

class FormsInstall extends skModuleInstall{

    public function init() {

        print("the forms init\r\n");
        return true;
    }// func

    public function install() {
        print("the forms install\r\n");

        return true;
    }// func

    public function deinstall() {
        print("the forms deinstall\r\n");
        return true;
    }// func
} //class