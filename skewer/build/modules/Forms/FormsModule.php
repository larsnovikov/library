<?php

/**
 * @class FormsModule
 * @extends skModule
 * @project Skewer
 * @package kernel
 *
 * @author Andy Mitrich, $Author: acat $
 * @version $Revision: 1173 $
 * @date 16.01.12 10:00 $
 *
 */

class FormsModule extends skModule{

    /* @var null|\FormsApi */
    public $oFormsApi = NULL;
    private $FormTemplate;
    private $AnswersTemplate;
    private $LetterTemplate;
    private $ErrorTemplate;
    public $SectionId = 0;
    public $FormId = 0;
    public $FormName = '';

    public function init(){

        // Читаем массив настроек из конфига
        $aSettings = $this->getConfigParam('settings', 'Page');
        // Передаем его по ссылке в API
        $this->oFormsApi = new FormsApi($aSettings);
        $this->setParser(parserTwig);
        $this->SectionId = $this->getEnvParam('sectionId');
        // Шаблон отображения формы
        $this->FormTemplate = 'form.twig';
        // Шаблон отображения ответов
        $this->AnswersTemplate = 'answers.twig';
        // Шаблон отображения ошибок
        $this->ErrorTemplate = 'error.twig';
        // Шаблон письма
        $this->LetterTemplate = 'letter.twig';
    }

    public function execute(){

        $sCmd = $this->getStr('cmd', 'showForm');

        switch( $sCmd ){

            // Состояние отображения формы
            case 'showForm':

                // Строим форму на основании имеющегося id раздела
                if( $this->FormName )
                    $oForm = $this->oFormsApi->buildFormByName($this->FormName);
                else
                    $oForm = $this->oFormsApi->buildFormBySection($this->SectionId);

                if ( !($oForm instanceof FormsEntity) ) {

                    $this->setTemplate($this->ErrorTemplate);

                    return psComplete;

                } else {

                    $this->setData('oForm', $oForm);

                    $this->setData('iRandVal', rand(0, 1000));

                    $this->setData('form_section', Tree::getAliasPathById($this->SectionId));

                    $this->setTemplate($this->FormTemplate);

                }
            break;

            // Состояние отправки формы
            case 'sendForm':

                $iFormId    = $this->getInt('form_id');
                $sErrorHandler = '';

                try {

                    $oForm = $this->oFormsApi->buildForm($iFormId, $_POST, $sErrorHandler);

                    # форма не найдена - сообщяем о попытке взлома
                    if( !($oForm instanceof FormsEntity) ) throw new Exception($sErrorHandler);

                    $bRes = false;

                    # Если не нахватались ошибок - по виду обработчика отправить данные

                    switch( $oForm->getHandlerType() ){

                        case 'toMail':

                            // Если в форме(шаблон!) не задано значение обработчика(куда отсылаем!), берем системный e-mail
                            if( !$oForm->getHandlerValue() ) $oForm->setHandlerValue(Parameters::getValByName(skConfig::get('section.sectionRoot'),'.', 'email'));

                            // no-replay mail
                            $sNoReplayMail = skConfig::get('notifications.noreplay_email');

                            // Если в форме(с сайта) не задано значение email(кто отправляет!), берем no-replay
                            if( !($sMailFrom = $oForm->findEmailField()) )
                                $sMailFrom = $sNoReplayMail;

                            // Посылаем e-mail
                            $bRes = $this->oFormsApi->sendFormToMail($oForm, $sMailFrom/*$oForm->getHandlerValue()*/, $this->LetterTemplate, $this->getModuleDir().$this->getTplDirectory());

                            // отправляем уведомление об отправки сообщения - автоответ
                            if( $bRes && $oForm->getFormAnswer() && ($sNoReplayMail != $sMailFrom) ){

                                $aFormAnswer = Forms2AnswerMapper::getAnswerByFormId($oForm->getId());

                                if( is_array($aFormAnswer) && !empty($aFormAnswer) ){

                                    skMailer::sendMail($sMailFrom, $sNoReplayMail, $aFormAnswer['answer_title'], $aFormAnswer['answer_body'], 'utf-8');

                                }
                            }

                        break;

                        case 'toMethod':

                            $bRes = $this->oFormsApi->sendFormToMethod($oForm, $oForm->getHandlerValue());
                        break;

                        default:
                            throw new Exception('handler_not_found');# обработчик неизвестен - выходим
                        break;
                    }// switch handler

                    # вывести сообщение об успешной / неуспешной отправке
                    if( $oForm->getFormRedirect() ){
                        skResponse::sendRedirectHeaders( $oForm->getFormRedirect() );
                        return;
                    }

                    $sData = ($bRes)? ($oForm->getHandlerType()=='toMethod'?'success_otziv':'success'): 'error';
                    $this->setData($sData, 1);
                    $this->setData('form_section', $this->SectionId);

                    //skProcessor
                }
                catch(Exception $e) {
                    $this->setData($e->getMessage(), 1);
                }

                $this->setTemplate($this->AnswersTemplate);
            break;

            // Ajax-проверка капчи
            case 'captcha_ajax':

                $bRefresh = (isset($_POST['b62516184fb6ef591f45bd4974b753']) && $_POST['b62516184fb6ef591f45bd4974b753'] == 'true')? true: false;
                $sCaptcha = $this->getStr('code');
                $sCaptcha = Captcha::check_captcha($sCaptcha, $bRefresh);

                if ( $sCaptcha ) {

                    $this->setData('out', 1);

                } else {

                    if ( isset($_SESSION['site_language']) AND $_SESSION['site_language'] )
                        $sMessage = 'Wrong code!';
                    else
                        $sMessage = 'Код с картинки введен неверно!';
                    $this->setData('out', $sMessage);
                }

                return psRendered;
            break;
        }

        return psComplete;
    }

    public function shutdown(){

    }

}//class