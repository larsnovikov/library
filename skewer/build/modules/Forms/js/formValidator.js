$(function(){

    /*применяем правила валидации к полям*/
    var sRules = $('#_rules').val();
    if(sRules) {
        sRules = eval('(' + sRules + ')');
        //console.log(sRules);
        var formId  = $('#_rules').closest("form").attr('id');
        var FormNum = $('#'+formId+' input#form_id').val();

        var bFormSubmitAllow = false;

        if(formId) {

            var oValidator =$('#'+formId).validate(sRules);
            $('#'+formId).submit(function() {

                if(bFormSubmitAllow) return true;

                if($("#captcha").size()) {
                    var oRequest     = new Object;
                    var sCode    = $("#captcha").val();

                    $.post('/ajax/ajax.php',{ moduleName:'Forms', cmd: 'captcha_ajax', code: sCode, b62516184fb6ef591f45bd4974b753:false }, function(mResponse){

                        var oResponse = eval("("+mResponse+")");
                        var sResponse = oResponse.data.out;

                        if(sResponse == '1') {
                            bFormSubmitAllow = true;
                            $('#'+formId).submit();
                        } else {
                            reloadImg('img_captcha');
                            oValidator.showErrors({"captcha": "Код введен неверно!"});
                        }

                    });

                    return false;
                }
            });

        }// if form id
    }// if rules

    /*релоад каптчи*/

    $('.img_captcha').click(function(){
            return reloadImg('img_captcha');
    });

    /* calendar - инициализация календарика */
//    $.datepicker.formatDate('yy-mm-dd');

    $.datepicker.setDefaults({
        showOn: 'both',
        buttonImageOnly: true,
        buttonImage: '/skewer_build/libs/datepicker/images/calendar.jpg',
        dateFormat: 'dd.mm.yy'  //ЗДЕСЬ ЗАДАЕТСЯ ФОРМАТ ДЛЯ КАЛЕНДАРЯ ФОРМЫ
    });


    jQuery.validator.addMethod('date', function(value, element, param) {
        if(value == '') return true;
        //console.log(new RegExp("^([0-2]\d|3[01])\.(0\d|1[012])\.(\d{4})$").test(value));
        var parts = value.split('.');
        if (parts.length != 3) return false;
        if( parseInt(parts[2]) < 1000 || parseInt(parts[2]) > 9999 ) return false;
        if( parseInt(parts[1]) < 1 || parseInt(parts[1]) > 12 ) return false;
        if( parseInt(parts[0]) < 1 || parseInt(parts[0]) > 31 ) return false;
        var tmpDate = new Date(parts[2], parseInt(parts[1]) - 1, parts[0], 12);
        //console.log(/Invalid|NaN/.test(tmpDate));
        return !/Invalid|NaN/.test(tmpDate);
    });

    //$('.js_init_datepicker').attachDatepicker({yearRange: '1900:'+$(this).attr('cur_year')});
    $('.js_init_datepicker').datepicker();

});

function reloadImg(id) {

    var obj = document.getElementById(id);

    if(!obj) return;

    var src = obj.src;
    var pos = src.indexOf('?');

    if (pos >= 0) {
        src = src.substr(0, pos);
    }

    var date = new Date();
    obj.src = src + '?v=' + date.getTime();

    return false;
}