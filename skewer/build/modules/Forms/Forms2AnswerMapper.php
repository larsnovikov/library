<?php
/**
 * @class Forms2AnswerMapper
 * @extends skModule
 * @project Skewer
 * @package kernel
 *
 * @author kolesnikov, $Author: $
 * @version $Revision: $
 * @date $Date: $
 *
 */

class Forms2AnswerMapper extends skMapperPrototype {

    /**
     * @var string Имя таблицы, с которой работает маппер
     */
    protected  static $sCurrentTable = 'forms_answer';

    /**
     * @static Метод возвращает имя рабочей таблицы
     * @return string
     */
    public static function getCurrentTable() {

        return self::$sCurrentTable;
    }

    /**
     * @static Метод возвращает список полей рабочей таблицы
     * @return array
     */
    public static function getParametersList() {

        return self::$aParametersList;
    }

    /**
     * @var array Конфигурация полей таблицы
     */
    protected static $aParametersList = array(
        'answer_id' => 'i:hide:ИД формы ответа',
        'form_id' => 'i:hide:ИД формы',
        'answer_title' => 's:str:Заголовок письма',
        'answer_body' => 's:text:Текст письма'
    );


    public static function getAnswerByFormId($iFormId) {

        $aFilter = array('where_condition' => array('form_id' => array (
            'sign' => '=',
            'value' => $iFormId)
        ));

        return self::getItem($aFilter);
    }

}
