<?php

/**
 * @class Forms2ParamsEntity
 * @extends skModule
 * @project Skewer
 * @package kernel
 *
 * @author Andy Mitrich, $Author: acat $
 * @version $Revision: 1173 $
 * @date 17.01.12 18:16 $
 *
 */

require_once BUILDPATH.'modules/Forms/Entity.php';
class Forms2ParamsEntity extends Entity{

    /*getters*/
    public function getId() {
        return $this->data['param_id'];
    }// func
    public function getFormId() {
        return $this->data['form_id'];
    }// func
    public function getSectionId() {
        return $this->data['param_section_id'];
    }// func
    public function getName() {
        return $this->data['param_name'];
    }// func
    public function getTitle() {
        return $this->data['param_title'];
    }// func
    public function getDescription() {
        return $this->data['param_description'];
    }// func
    public function getType() {
        return $this->data['param_type'];
    }// func
    public function getTypeTitle() {
        return $this->data['param_type_title'];
    }// func
    public function getRequired() {
        return $this->data['param_required'];
    }// func
    public function getDefault() {
        return $this->data['param_default'];
    }// func
    public function getMaxLength() {
        return $this->data['param_maxlength'];
    }// func
    public function getValidType() {
        return $this->data['param_validation_type'];
    }// func
    public function getDepend() {
        return $this->data['param_depend'];
    }// func
    public function getManParams() {
        return $this->data['param_man_params'];
    }// func
    public function getPriority() {
        return $this->data['param_priority'];
    }// func
    public function getItems() {
        return $this->data['param_items'];
    }// func
    public function getYear() {
        return date('Y');
    }// func

    /*setters*/

    public function setId($val) {
        $this->data['param_id'] = (int)$val;
    }// func
    public function setFormId($val) {
        $this->data['form_id'] = (int)$val;
    }// func
    public function setSectionId($val) {
        $this->data['param_section_id'] = (int)$val;
    }// func
    public function setName($val) {
        $this->data['param_name'] = (String)$val;
    }// func
    public function setTitle($val) {
        $this->data['param_title'] = (String)$val;
    }// func
    public function setDescription($val) {
        $this->data['param_description'] = (String)$val;
    }// func
    public function setType($val) {
        $this->data['param_type'] = (int)$val;
    }// func
    public function setTypeTitle($val) {
        $this->data['param_type_title'] = (String)$val;
    }// func
    public function setRequired($val) {
        $this->data['param_required'] = (int)$val;
    }// func
    public function setDefault($val) {
        $this->data['param_default'] = $val;
    }// func
    public function setMaxLength($val) {
        $this->data['param_maxlength'] = (int)$val;
    }// func
    public function setValidType($val) {
        $this->data['param_validation_type'] = (String)$val;
    }// func
    public function setDepend($val) {
        $this->data['param_depend'] = (String)$val;
    }// func
    public function setManParams($val) {
        $this->data['param_man_params'] = (String)$val;
    }// func
    public function setPriority($val) {
        $this->data['param_priority'] = (int)$val;
    }// func
    public function setItems($val) {
        $this->data['param_items'] = $val;
    }// func

} //class