<?php

/**
 * @class Forms2ParamsMapper
 * @extends skModule
 * @project Skewer
 * @package kernel
 *
 * @author Andy Mitrich, $Author: acat $
 * @version $Revision: 1173 $
 * @date 16.01.12 10:42 $
 *
 */

class Forms2ParamsMapper extends skMapperPrototype{

    /**
     * @var string Имя таблицы, с которой работает маппер
     */
    protected  static $sCurrentTable = 'forms_parameters';

    protected static $sKeyFieldName = 'param_id';

    /**
     * @static Метод возвращает имя рабочей таблицы
     * @return string
     */
    public static function getCurrentTable() {

        return self::$sCurrentTable;
    }

    /**
     * @static Метод возвращает список полей рабочей таблицы
     * @return array
     */
    public static function getParametersList() {

        return self::$aParametersList;
    }

    /**
     * @var array Конфигурация полей таблицы
     */
    protected static $aParametersList = array(
        'param_id' => 'i:hide:ИД параметра',
        'form_id' => 'i:hide:ИД формы',
        'param_section_id' => 'i:str:Раздел для показа',
        'param_name' => 's:str:Идентификатор',
        'param_title' => 's:str:Название параметра',
        'param_description' => 's:text:Описание параметра',
       	'param_type' => 's:str:Тип параметра',
       	'param_required' => 'i:check:Обязательный параметр',
       	'param_default' => 's:text:Значение по умолчанию',
       	'param_maxlength' => 's:str:Максимальная длина',
       	'param_validation_type' => 's:str:Тип валидации',
       	'param_depend' => 's:str:Зависимое поле',
       	'param_man_params' => 's:str:Поле для спецстилей',
       	'param_priority' => 'i:hide:Порядок'
    );

    /* Admin Interface*/

    public static function updParameter($aInputData, $bControlPriority = true){

        if ( $bControlPriority ){

            // Если во входящем массиве с данными нет значения сортироки или оно равно нулю - получаем максимальный порядок для текущей позиции
            if ( !isset($aInputData['param_priority']) || !$aInputData['param_priority'] )
            $aInputData['param_priority'] = self::getMaxOrder($aInputData['form_id'],$aInputData['param_id'])+1;
        }
        // Сохраняем опрос
        return self::saveItem($aInputData);
    }// function updParameter()

    public static function getMaxOrder($iFormId, $iCurItemId = 0){

        global $odb;

        $iCurItemId = (int)$iCurItemId;

        $aData = array( 'form_id' => $iFormId,
                        'add_where' => $iCurItemId ? "`param_id`<>$iCurItemId AND" : ''
                      );

        $sQuery = "
            SELECT
                MAX(`param_priority`) as param_priority
            FROM
                `forms_parameters`
            WHERE [add_where:q]
                `form_id` = [form_id:i];";

        $aValue = $odb->getRow($sQuery, $aData);

        return $aValue['param_priority'];
    }// function getMaxOrder()

    public static function delParameter($aInputData){

        if ( !$aInputData ) return false;

        global $odb;

        $aData = array(
            'param_id' => $aInputData['param_id'],
            'form_id' => $aInputData['form_id']
        );

        $sQuery = "
            DELETE
            FROM
                `forms_parameters`
            WHERE
                `param_id` = [param_id:i] AND
                `form_id` = [form_id:i];";

        return $odb->query($sQuery, $aData);
    }// function delParameter()

    public static function delParameterByForm($iForm){

        if ( !$iForm ) return false;

        global $odb;

        $aData = array(
            'form_id' => $iForm
        );

        $sQuery = "
            DELETE
            FROM
                `forms_parameters`
            WHERE
                `form_id` = [form_id:i];";

        return $odb->query($sQuery, $aData);
    }// function delParameter()

    /* Public Interface */

    public static function getParams(FormsEntity $oForm){

        global $odb;

        $aData = array( 'form_id'=>$oForm->getId() );
        $sQuery = "
            SELECT
                p.*
            FROM
                `forms_parameters` AS p
            WHERE
                p.form_id = [form_id]
            ORDER BY
                p.param_priority ASC;";

        $rResult = $odb->query($sQuery, $aData);
        $aParams = array();

        while( $aRow = $rResult->fetch_array(MYSQLI_ASSOC) ) {

            $oParam = new Forms2ParamsEntity();

            $oParam->setId($aRow['param_id']);
            $oParam->setFormId($aRow['form_id']);
            $oParam->setSectionId($aRow['param_section_id']);
            $oParam->setName($aRow['param_name']);
            $oParam->setTitle($aRow['param_title']);
            $oParam->setDescription($aRow['param_description']);
            $oParam->setRequired($aRow['param_required']);
            $oParam->setDefault($aRow['param_default']);
            $oParam->setMaxLength($aRow['param_maxlength']);
            $oParam->setValidType($aRow['param_validation_type']);
            $oParam->setDepend($aRow['param_depend']);
            $oParam->setManParams($aRow['param_man_params']);
            $oParam->setPriority($aRow['param_priority']);
            $oParam->setType($aRow['param_type']);

            switch($aRow['param_type']){

                // Input Type
                case '1':

                    $oParam->setTypeTitle('input');
                break;

                // Textarea Type
                case '2':

                    $oParam->setTypeTitle('textarea');
                break;

                // CheckBox Type
                case '5':

                    $oParam->setTypeTitle('checkbox');
                break;

                // LoadFile Type
                case '6':

                    $oParam->setTypeTitle('file');
                break;

                // Radio Type
                case '8':

                    $oParam->setDefault(preg_replace('/\x0a+|\x0d+/Uimse', '', $oParam->getDefault()));
                    $aRows = explode(';', trim($oParam->getDefault()));
                    $aRows = array_diff($aRows, array(''));

                    //if(!is_array($aRows)) break;
                    //if(!count($aRows))    break;

                    $aItems = array();
                    if ( is_array($aRows) && count($aRows) )
                    foreach ($aRows as $iKey => $sRow) {

                        $aItem = array();

                        if(strpos($sRow, ':'))
                            list($aItem['value'], $aItem['title']) = explode(':', $sRow);
                        else
                            $aItem['value'] = $aItem['title'] = $sRow;

                        $aItem['value']          = trim($aItem['value']);
                        $aItem['checked']        = (!$iKey)? 'checked': '';
                        $aItem['param_id']       = $oParam->getName().$iKey;
                        $aItem['param_name']     = $oParam->getName();

                        $aItems[] = $aItem;
                    }// foreach

                    $oParam->setItems($aItems);
                    $oParam->setTypeTitle('radio');
                break;

                // Select Type
                case '9':

                    $oParam->setDefault(preg_replace('/\x0a+|\x0d+/Uimse', '', $oParam->getDefault()));
                    $aRows = explode(';', trim($oParam->getDefault()));
                    $aRows = array_diff($aRows, array(''));

                    //if(!is_array($aRows)) break;
                    //if(!count($aRows))    break;

                    $aItems = array();
                    if ( is_array($aRows) && count($aRows) )
                    foreach ($aRows as $iKey=>$sRow) {

                        $aItem = array();

                        if(strpos($sRow, ':'))
                            list($aItem['value'], $aItem['title']) = explode(':', $sRow);
                        else
                            $aItem['value'] = $aItem['title'] = $sRow;

                        $aItem['value'] = trim($aItem['value']);
                        $aItem['selected'] = (!$iKey) ? 'selected' : '';
                        $aItems[] = $aItem;
                    }// foreach

                    array_unshift($aItems, array('value'=>'','title'=>$oParam->getDescription(),'selected'=> 'selected'));

                    $oParam->setItems($aItems);
                    $oParam->setTypeTitle('select');

                break;

                // Delimiter Type
                case '11':

                    $oParam->setTypeTitle('delimiter');
                break;

                // Calendar Type
                case '13':

                    $oParam->setTypeTitle('calendar');
                break;
            }

            $aParams[] = $oParam;

        }// while

        $oForm->setParams($aParams);

        return $oForm;
    }

    protected static function getAddParamList(){

        $aSettings = skConfig::get('buildConfig.Page.modules.Forms.settings');

        return array(
            'param_id' => array(
                'view' => 'hide',
                'listColumns' => array(
                    'hidden' => true
                )
            ),
            'form_id' => array(
                'view' => 'hide',
                'listColumns' => array(
                    'hidden' => true
                )
            ),
            'param_type' => ExtForm::getDesc4SelectFromArray($aSettings['field_types']),
            'param_validation_type' => ExtForm::getDesc4SelectFromArray($aSettings['validation_types'])
        );
    }// function getAddParamList()


    /**
     * Выполняет сдвиг положений параметров
     * @param $iFormId
     * @param $iStartPos
     * @param $iEndPos
     * @param string $sSign
     * @return int
     */
    public static function shiftParamPosition($iFormId,$iStartPos,$iEndPos,$sSign = '+'){

        global $odb;

        $sQuery = "
            UPDATE `[table:q]`
            SET `param_priority`=`param_priority` [sign:q] 1
            WHERE
                `form_id` = [form:i] AND
                `param_priority` > [start:i] AND
                `param_priority` < [end:i]";

        $odb->query($sQuery, array(
            'table' => self::$sCurrentTable,
            'form' => $iFormId,
            'sign' => $sSign,
            'start' => $iStartPos,
            'end' => $iEndPos
        ));

        return $odb->affected_rows;
    }

    /**
     * меняет значение для поля сортировки параметра на заданное
     * @param $iParamId
     * @param $iPos
     * @return int
     */
    public static function changeParamPosition($iParamId, $iPos){

        global $odb;

        $sQuery = "
            UPDATE `[table:q]`
            SET `param_priority`=[pos:i]
            WHERE
                `param_id` = [param_id:i]";

        $odb->query($sQuery, array(
            'table' => self::$sCurrentTable,
            'param_id' => $iParamId,
            'pos' => $iPos
        ));

        return $odb->affected_rows;
    }

}//class