var msgCt = null;
Ext.define('Ext.sk.Messager', {
    createBox: function(t, s, addCls){
        return '<div class="msg'+(addCls?' '+addCls:'')+'">'+(t?'<h3>'+t+'</h3>':'')+'<p>' + s + '</p></div>';
    },

    msg : function(title, text, addCls, time){
        if(!msgCt){
            msgCt = Ext.core.DomHelper.insertFirst(document.body, {id:'msg-div'}, true);
        }
        if ( !text || typeof text === 'undefined' ){
            text = title;
            title = '';
        }
        var m = Ext.core.DomHelper.append(msgCt, this.createBox(title, text, addCls), true);
        m.hide();
        m.slideIn('t').ghost("t", { delay: time ? time : 2000, remove: true});
    },

    init : function(){}

});
