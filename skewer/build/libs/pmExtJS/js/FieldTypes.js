/**
 * Типы полей редакторов
 */

Ext.define('Ext.sk.FieldTypes',{
    types: {
        hide: {
            type_id: 0,
            title: buildLang.contTabSystem,
            xtype: 'hiddenfield',
            hideInContent: true
        },
        str: {
            type_id: 1,
            title: buildLang.contTabString,
            xtype: 'textfield',
            rendererList: {
                link: function(value,meta,record,rowIndex,colIndex){

                    // собрать данные
                    var col = this.columns[colIndex],
                        linkHrefTpl = col['linkHrefTpl'] || '',
                        linkTitleTpl = col['linkTitleTpl'] || '',
                        linkBlank = col['linkBlank'] || '',
                        href = sk.parseTpl( linkHrefTpl, record.data ),
                        title = sk.parseTpl( linkTitleTpl, record.data )
                    ;

                    // нет ссылки - просто отдать значение
                    if ( !href ) return value;

                    // собрать строку
                    return Ext.String.format(
                        '<a href="{0}"{2}>{1}</a>',
                        href,
                        title,
                        linkBlank?'target="_blank"':''
                    );

                }
            },
            listEditableSettings: {
                editor: {
                    xtype: 'textfield'
                }
            }
        },
        text: {
            type_id: 2,
            title: buildLang.contTabText,
            xtype: 'textareafield',
            baseSettings: {
                margin: '0 5 5 0',
                labelAlign: 'top'
            }
        },
        wyswyg: {
            type_id: 3,
            title: buildLang.contTabWyswyg,
            xtype: 'ckeditor',
            baseSettings: {
                labelAlign: 'top',
                margin: '0 5 30 0',
                height: 300
            }
        },
        // todo 4 imagefile
        check: {
            type_id: 5,
            title: buildLang.contTabCheck,
            xtype: 'checkboxfield',
            baseSettings: {
                uncheckedValue: '',
                inputValue: 1
            },
            listSettings: {
                renderer: function(value){
                    return (value && value!=='0') ? '+' : '';
                },
                align: 'center'
            },
            listEditableSettings: {
                xtype: 'checkcolumn',
                editor: {
                    xtype: 'checkbox',
                    cls: 'x-grid-checkheader-editor'
                },
                listeners: {
                    checkchange: function( self, index ) {
                        processManager.getMainContainer(self).setLoading(true);
                        processManager.sendDataFromMainContainer( self, {
                            cmd: self['listSaveCmd'],
                            data: self.up('gridpanel').getStore().getAt( index).data
                        } );
                    }
                },
                align: 'center'
            },
            updField: function( field ){
                if ( field.value && field.value!=='0' )
                    field.checked = true;
                return field;
            }
        },
        file: {
            type_id: 6,
            title: buildLang.contTabFile,
            xtype: 'selectfilefield'
        },
        html: {
            type_id: 7,
            title: buildLang.contTabHtml,
            xtype: 'htmleditor',
            baseSettings: {
                labelAlign: 'top',
                height: 250
            }
        },
        show: {
            type_id: 8,
            title: buildLang.contTabShow,
            xtype: 'displayfield',
            baseSettings: {
                fieldBodyCls: 'builder-show-field'
            }
        },
        select: {
            type_id: 9,
            title: buildLang.contTabCombo,
            xtype: 'combo',
            baseSettings: {
                mode: 'local',
                triggerAction: 'all',
                forceSelection: true,
                allowBlank: false,
                editable: false,
                displayField: 'value',
                valueField:'value',
                queryMode: 'local',
                store:  {
                    fields: ['value'],
                    data: []
                }
            }
        },
        num: {
            type_id: 10,
            title: buildLang.contTabNumber,
            xtype: 'numberfield'
        },
        date: {
            type_id: 15,
            title: buildLang.contTabDate,
            xtype: 'datefield',
            baseSettings: {
                format: 'd.m.Y'//todo d.m.Y
            }
        },
        time: {
            type_id: 16,
            title: buildLang.contTabTime,
            xtype: 'timefield',
            baseSettings: {
                format: 'H:i'
            }
        },
        inherit: {
            type_id: 20,
            title: buildLang.contTabInherit,
            hideInContent: true,
            xtype: 'textareafield'
        },
        specific: {
            updField: function( field, moduleLayer ){

                if ( field.layerName )
                    moduleLayer = field.layerName;
                else if ( !moduleLayer )
                    moduleLayer = buildConfig.layerName;

                if ( field['extendLibName'] ) {
                    var extendClassName = 'Ext.'+moduleLayer+'.'+field['extendLibName'];
                    return Ext.create(extendClassName,field);
                } else {
                    return field;
                }

            }
        },
        pass: {
            title: 'Пароль',
            xtype: 'textfield',
            baseSettings: {
                inputType: 'password'
            }
        }

    },

    // получить набор типов в виде объекта
    getTypesAsObject: function(){
        return this.types;
    },

    /**
     * Формирование набора полей для формы по инициализационному массиву
     * @param items
     * @param layerName
     */
    createFields: function( items, layerName ) {

        var me = this,
            outItems = [],
            field,
            fieldKey,
            fieldDef,
            attrName,
            cmsXTypes = me.getTypesAsObject(),
            mType
        ;

        // добавление полей
        for ( fieldKey in items ) {

            // описание поля
            fieldDef = items[fieldKey];

            if ( fieldDef['customField'] ) {

                // название
                if ( !fieldDef.fieldLabel )
                    fieldDef.fieldLabel = fieldDef.title;


                // добавить поле в форму
                outItems.push( Ext.create( 'Ext.'+layerName+'.'+fieldDef['customField'], fieldDef ) );

                continue;
            }

            // проверка на существование типа поля
            if ( typeof cmsXTypes[fieldDef.type] === 'undefined' )
                throw buildLang.contTabErrUnknownType+fieldDef.type+'`';

            // описание поля
            mType = cmsXTypes[fieldDef.type];

            // описание для ExtJS
            field = {
                name: fieldDef.name,
                fieldLabel: fieldDef.title,
                value: fieldDef.value
            };

            // дополнение незаполненными полями
            for (attrName in fieldDef) {
                if ( typeof field[attrName] === 'undefined' )
                    field[attrName] = fieldDef[attrName];
            }

            // дополнение базовыми настройками
            if ( typeof mType === 'object' && mType.baseSettings ) {
                for (attrName in mType.baseSettings) {
                    if ( typeof field[attrName] === 'undefined' )
                        field[attrName] = mType.baseSettings[attrName];
                }
            }

            // тип поля
            if ( typeof mType === 'object' ) {

                // тип
                if ( mType.xtype )
                    field.xtype = mType.xtype;

                // модификация
                if ( mType.updField )
                    field = mType.updField( field, layerName );

            } else {
                if ( !field.xtype )
                    field.xtype = mType;
            }

            // добавить поле в форму
            outItems.push( field );

            if ( fieldDef['subtext'] ) {

                outItems.push( {
                    xtype: 'displayfield',
                    baseCls: 'form-field-subtext',
                    fieldLabel: '',
                    hideEmptyLabel: false,
                    value: fieldDef['subtext']
                } );

            }

        }

        return outItems;

    }


});
