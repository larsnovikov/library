/**
 * Класс для формирования групп полей
 */

Ext.define('Ext.sk.FieldGroup',{

    extend: 'Ext.form.FieldSet',
    alias: 'widget.editorgroupfield',

    defaultType: 'textfield',
    collapsible: true,
    collapsed: false,
    padding: '0 0 0 5',

    fieldConfig: [],

    initComponent: function(){

        var me = this;

        this.items = Ext.create('Ext.sk.FieldTypes').createFields( this.fieldConfig, this.layerName );

        me.title = '<a class="cursor_hand" onclick="changeGroupBlockState( this ); return false;">'+me.title+'</a>';

        me.callParent();

    }

});

/**
 * открывает/закрывает группу
 */
changeGroupBlockState = function( linkTag ) {
    var element = Ext.get(linkTag).dom.parentElement.parentElement.parentElement;
    var cont = Ext.ComponentManager.get( element.id );
    cont.expand();
};
