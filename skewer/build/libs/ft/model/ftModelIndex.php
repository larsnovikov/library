<?php
/**
 * Класс для работы с индексами модели
 */

class ftModelIndex {

    /** @var string имя сущности */
    protected $sName;

    /** @var array описание индекса */
    protected $aIndex = array();

    /** @var array набор значений по умолчанию */
    protected $aDefaultArray = array(
        'fields' => array(),
        'index_type' => 'BTREE',
        'unique' => false,
    );

    /**
     * Конструктор
     * @param string $sIndexName имя поля
     * @param array $aIndexModel описание поля
     */
    public function __construct( $sIndexName, $aIndexModel ) {

        // если формат данных заведомо левый - то выходим
        if(!is_array($aIndexModel))
            throw new ftException('Неверный тип контейнера описания');

        // сохраняем описание
        $this->aIndex = array_merge($this->aDefaultArray, $aIndexModel);

        // проверка наличия полей
        if ( !$this->getFileds() )
            throw new ftException('Нельзя создавать индекс без полей');

        if ( !$sIndexName ) {
            $aFields = $this->getFileds();
            $sIndexName = $aFields[0];
        }

        // имя сущности
        if ( $sCoverName = $this->getCoverName() )
            $sIndexName = $sCoverName;
        $this->setName($sIndexName);

        if ( !$this->sName )
            throw new ftException('Отсутствует имя индекса');

    }

    /**
     * Отдает базовый набор для генерации поля
     * @static
     * @param array $aFields набор полей
     * @param string $sAlias тип индекса
     * @return array
     */
    public static function getBaseDesc( $aFields, $sAlias ) {

        $bUnique = false;
        $sIndexType = 'BTREE';
//        $sSqlPrefix = sprintf('%s `%s`', strtoupper($sType), $sIndexName);

        // перекрывающая переменная имени
        $sIndexName = '';

        switch ( strtolower($sAlias) ) {
            case 'primary':
                $sIndexName = $sAlias;
//                $sSqlPrefix = 'PRIMARY KEY';
            case 'unique':
                $bUnique = true;
                break;
            case 'fulltext':
                $sIndexType = 'FULLTEXT';
            case 'index':
            case 'key':
                break;
            default:
                throw new ftModelException('Не могу создать индекс `'.$sAlias.'`');
        }

        // формирование описания поля
        return array(
            'name' => $sIndexName,
            'fields' => $aFields,
            'alias' => $sAlias,
            'index_type' => $sIndexType,
            'unique' => $bUnique,
        );

    }

    /**
     * Добавляет атрибут
     * @param $sAttrName
     * @param $mVal
     */
    public function setAttr( $sAttrName, $mVal ) {
        $this->aIndex[$sAttrName] = $mVal;
    }

    /**
     * Отдает атрибут
     * @param $sAttrName
     * @return mixed|null
     */
    public function getAttr( $sAttrName ) {
        return isset($this->aIndex[$sAttrName]) ? $this->aIndex[$sAttrName] : null;
    }

    /**
     * Отдает имя поля
     * @return string
     */
    public function getName(){
        return $this->sName;
    }

    /**
     * Устанавливает имя поля
     * @param string $sName
     * @return string
     */
    public function setName( $sName ){
        $this->sName = mysql_real_escape_string($sName);
    }

    /**
     * Отдает перекрывющее имя индекса, если оно задано в массиве параметров
     * @return string
     */
    protected function getCoverName() {
        return (string)$this->getAttr('name');
    }

    /**
     * Отдает им япервого поля
     * @return string
     */
    public function getFirstFieldName() {
        $aFields = $this->getAttr('fields');
        if ( !is_array($aFields) ) return '';
        return (string)array_shift($aFields);
    }

    /**
     * Отдает набор полей индекса
     * @return string[]
     */
    public function getFileds() {
        return $this->getAttr('fields');
    }

    /**
     * Отдает значение уникальности
     * @return bool
     */
    public function getUnique() {
        return (bool)$this->getAttr('unique');
    }

    /**
     * Отдает флаг "уникальное"
     * @return bool
     */
    public function isUnique() {
        return (bool)$this->getUnique();
    }

    /**
     * Отдает псевдоним типа
     * @return bool
     */
    public function getTypeAlias() {
        return (string)$this->getAttr('alias');
    }

    /**
     * Отдает тип индакса
     * @return bool
     */
    public function getIndexType() {
        return (string)$this->getAttr('index_type');
    }

}
