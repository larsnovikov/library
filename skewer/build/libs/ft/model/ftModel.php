<?php
/**
 * Класс для хранения описания модели
 * Является провайдером данных для всех ft классов
 */

class ftModel {

    /** @var string имя сущности */
    protected $sName;

    /** @var array контейнер заголовочных данных описания */
    protected $aEntity = array();

    /** @var ftModelField[] контейнер полей */
    protected $aFields = array();

    /** @var array наборы колонок */
    protected $aColumns = array();

    /** @var array набор индексов */
    protected $aIndexes = array();

    /**
     * Конструктор
     * @param array $aModel описание сущности
     */
    public function __construct( $aModel ) {

        // если формат данных заведомо левый - то выходим
        if(!is_array($aModel))
            throw new ftException('Неверный тип контейнера описания');

        if ( !isset($aModel['entity']) or !isset($aModel['fields']) )
            throw new ftException('Отсутствуют необходимые контейнеры');

        // заголовочные данные описания
        $this->aEntity = $aModel['entity'];
        if ( !is_array($this->aEntity) )
            throw new ftException('Неверный контейнер описания');

        // имя сущности
        $this->sName = isset($this->aEntity['name']) ? (string)$this->aEntity['name'] : '';
        $this->sName = str_replace("'", "", $this->sName);
        if ( !$this->sName )
            throw new ftException('Отсутствует имя сущности');

        // добавление полей
        $aFields = $aModel['fields'];
        if ( !is_array($aFields))
            throw new ftException('Неверный контейнер полей');
        foreach ( $aFields as $sFieldName => $aField ) {
            $oFileld = new ftModelField( $sFieldName, $aField );
            $this->aFields[ $oFileld->getName() ] = $oFileld;
        }

        $this->aIndexes = isset($aModel['indexes']) ? $aModel['indexes'] : array();
        $this->aColumns = isset($aModel['columns']) ? $aModel['columns'] : array();

    }

    /**
     * Отдает массив описания
     * @static
     * @return array
     */
    public function getModelArray() {
        $aFields = array();
        foreach ( $this->aFields as $oField ) {
            $aFields[ $oField->getName() ] = $oField->getModelArray();
        }
        $aOut = array(
            'entity' => $this->aEntity,
            'fields' => $aFields,
            'columns' => $this->aColumns,
            'indexes' => $this->aIndexes
        );
        return $aOut;
    }

    /**
     * Добавляет атрибут
     * @param $sAttrName
     * @param $mVal
     */
    public function setAttr( $sAttrName, $mVal ) {
        $this->aEntity[$sAttrName] = $mVal;
    }

    /**
     * Отдает атрибут
     * @param $sAttrName
     * @return mixed|null
     */
    public function getAttr( $sAttrName ) {
        return isset($this->aEntity[$sAttrName]) ? $this->aEntity[$sAttrName] : null;
    }

    /**
     * Отдает имя сущности
     * @return string
     */
    public function getName(){
        return $this->sName;
    }

    /**
     * Отдает имя таблицы
     * @return string
     */
    public function getTableName() {
        return mysql_real_escape_string($this->getTablePrefix().$this->getName());
    }

    /**
     * Задает префикс таблицы
     * @param $sPrefix
     */
    public function setTablePrefix( $sPrefix ) {
        $this->setAttr('tablePrefix',(string)$sPrefix);
    }

    /**
     * Отдает префикс таблицы
     * @return string
     */
    public function getTablePrefix() {
        return (string)$this->getAttr('tablePrefix');
    }

    /**
     * Отдает название сущности
     * @return string
     */
    public function getTitle(){
        $sTitle = $this->getAttr('title');
        return $sTitle ? $sTitle : $this->getName();
    }

    /**
     * Отдает набор полей
     * @return \ftModelField[]
     */
    public function getFileds() {
        return $this->aFields;
    }

    /**
     * Задает имя родительского поля
     * @param string $sFieldName
     */
    public function setParentField($sFieldName) {
        $this->setAttr('parent_field',(string)$sFieldName);
    }

    /**
     * Отдает имя родительского поля
     * @return string
     */
    public function getParentField() {
        return (string)$this->getAttr('parent_field');
    }

    /**
     * Задает тип связи
     * @param string $sType
     */
    public function setConnectionType($sType) {
        $this->setAttr('connection_type',(string)$sType);
    }

    /**
     * Отдает тип связи
     * @return string
     */
    public function connectionType() {

        // тип установлен в описании
        if ( !is_null($this->getAttr('connection_type')) ) return $this->getAttr('connection_type');

        // проверка на жесткую связь "один к одному" ---
        if ( $this->getParentEntity() ) return '---';

        // проверка на "многие ко многим"
        if ( $this->getSourceEntity() ) return '><';

        // проверка на "один ко многим"
        if ( $this->hasParentField() ) return '-<';

        // иначе "один к одному"
        else return '--';

    }

    /**
     * Задает тип связи
     * @param string $sType
     */
    public function setSourceEntity($sType) {
        $this->setAttr('source_entity',(string)$sType);
    }

    /**
     * Отдает тип связи
     * @return string
     */
    public function getSourceEntity() {
        return (string)$this->getAttr('source_entity');
    }

    /**
     * Задает родительскую сущность
     * @param string $sEntityName
     */
    public function setParentEntity($sEntityName) {
        $this->setAttr('parent_entity',(string)$sEntityName);
    }

    /**
     * Отдает родительскую сущность
     * @return string
     */
    public function getParentEntity() {
        return (string)$this->getAttr('parent_entity');
    }

    /**
     * Отдает объект поля
     * @param $sFieldName
     * @return \ftModelField|null
     */
    public function getFiled( $sFieldName ) {
        return $this->hasField($sFieldName) ? $this->aFields[$sFieldName] : null;
    }

    /**
     * Определяет наличие поля
     * @static
     * @param string $sFieldName имя поля
     * @return bool
     */
    public function hasField( $sFieldName ) {
        return isset( $this->aFields[$sFieldName] );
    }

    /**
     * Отдает флаг наличия поля привязки к родительской записи
     * @return bool
     */
    public function hasParentField() {
        return $this->hasField('_parent');
    }

    /**
     * Отдает набор имен полей
     * @return array
     */
    public function getAllFieldNames() {
        return array_keys($this->aFields);
    }

    /**
     * Добавляет псевдоним для набора полей
     * @param $sSetName
     * @param $mFieldList
     * @return string
     */
    public function addColumnSet( $sSetName, $mFieldList ) {

        // набор полей
        $aFieldList = $this->makeFieldsSet( $mFieldList );

        return $this->aColumns[$sSetName] = implode(',',$aFieldList);

    }

    /**
     * Отдает все наборы колонок, которые есть
     * @static
     * @return array
     */
    public function getColumnSetList() {
        return $this->aColumns;
    }

    /**
     * Отдает набор колонок по псевдониму
     * @param $sSetName
     * @return array
     */
    public function getColumnSet( $sSetName ) {
        $mFields =  isset($this->aColumns[$sSetName]) ? $this->aColumns[$sSetName]: array();
        return ftFnc::toArray($mFields);
    }

    /**
     * Отдает массив - заготовку для описания сущности
     * @param $sEntityName
     * @param string $sEntityTitle
     * @return array|bool
     */
    public static function getBlankArray($sEntityName,$sEntityTitle='') {
        $sEntityName = str_replace("'", "", trim($sEntityName));
        if(!$sEntityName) return false;
        $aModel = array();
        $aModel['entity']['name']=$sEntityName;
        $aModel['entity']['title'] =  $sEntityTitle ? $sEntityTitle : $sEntityName;
        $aModel['entity']['tablePrefix'] = 'ft_';
        $aModel['fields'] = array();
        $aModel['columns'] = array();
        $aModel['indexes'] = array();
        foreach (ftFnc::processorTypes() as $sProcessorName)
            $aModel[$sProcessorName] = array();
        return $aModel;
    }

    /**
     * Приводит набор имен полей к единому виду и фильтрует по текущему составу полей
     * @param array|string $mFieldList набор полей
     * @return array
     */
    public function makeFieldsSet( $mFieldList ){

        // выходной массив
        $aOutFieldList = array();

        // если пуст запрос - вернуть пустой массив
        $aFieldList = ftFnc::toArray($mFieldList);

        // перебрать все поля
        foreach ($aFieldList as $sFieldName ) {
            // добавить только существующие
            if ( $this->hasField($sFieldName) )
                $aOutFieldList[] = $sFieldName;
        }

        return $aOutFieldList;

    }

    /**
     * Добавляет поле к сущности
     * @param string $sFieldName
     * @param string $sDatatype тип в базе, может быть с размером в скобках
     * @param string $sTitle
     * @return \ftEntity
     */
    public function addField( $sFieldName, $sDatatype='varchar', $sTitle='' ) {
        $this->aFields[ $sFieldName ] = new ftModelField( $sFieldName, ftModelField::getBaseDesc($sDatatype, $sTitle) );
    }

    /**
     * Добавляет поле как объект
     * @param string $sFieldName
     * @param ftModelField $oField
     */
    public function addFieldObject( $sFieldName, ftModelField $oField ) {
        $this->aFields[ $sFieldName ] = $oField;
    }

    /**
     * Удаляет поле
     * @param string $sFieldName имя поля
     * @return bool
     */
    public function delField( $sFieldName ) {
        $bFound = $this->hasField($sFieldName);
        if ( $bFound )
            unset( $this->aFields[$sFieldName] );
        return $bFound;
    }

    /**
     * Отдает набор индексов
     * @return ftModelIndex[]
     */
    public function getIndexes() {
        return $this->aIndexes;
    }

    /**
     * Отдает индекс по имени
     * @param string $sIndexName
     * @return ftModelIndex|null
     */
    public function getIndex( $sIndexName ) {
        return isset($this->aIndexes[$sIndexName]) ? $this->aIndexes[$sIndexName] : null;
    }

    /**
     * Добавляет индекс
     * @param string $sIndexName имя индекса
     * @param array $aFields набор полей
     * @param string $sIndexType тип индекса
     * @param bool $bUnique флаг уникальности
     * @return string имя индекса
     */
    public function addIndex( $sIndexName, $aFields, $sIndexType, $bUnique ) {

        // форматирование набора полей
        $aFields = $this->makeFieldsSet($aFields);

        $oIndex = new ftModelIndex( $sIndexName, array(
            'fields' => $aFields,
            'index_type' => $sIndexType,
            'unique' => $bUnique,
        ));
        if ( !$oIndex ) return null;

        return $this->addIndexObject( $oIndex );

    }

    /**
     * Добавляет индекс по псевдонимк типа
     * @param string $sIndexName имя индекса
     * @param array $aFields набор полей
     * @param string $sType псевдоним типа
     * @return string имя индекса
     */
    public function addIndexByAlias( $sIndexName, $aFields, $sType ) {

        $oIndex = new ftModelIndex( $sIndexName, ftModelIndex::getBaseDesc($aFields,$sType) );
        if ( !$oIndex ) return null;

        return $this->addIndexObject( $oIndex );

    }

    /**
     * Добавляет индекс к списку
     * @param ftModelIndex $oIndex
     * @return string имя индекса
     */
    protected function addIndexObject( ftModelIndex $oIndex ) {

        // сгенерировать имя
        $sIndexName = $oIndex->getName();

        $sFirstField = $oIndex->getFirstFieldName();
        if ( !$sFirstField )
            throw new ftException('Не заданы поля для индекса');

        $cnt = 1;
        while ( isset($this->aIndexes[$sIndexName]) ) {
            $sIndexName = $sFirstField.'_'.++$cnt;
            $oIndex->setName( $sIndexName );
            if ( $cnt > 10 )
                throw new ftModelException('Не могу создать индекс для сущности `'.$this->getName().'` - достигнут предел перебора');
        }

        $this->aIndexes[$sIndexName] = $oIndex;

        return $oIndex->getName();

    }

    /**
     * Определяет, является ли сущность мультиязычной
     * @return bool
     */
    public function isMultilang() {
        // если есть хоть одно мультиязычное поле
        foreach ( $this->getFileds() as $oField )
            if ( $oField->isMultilang() )
                return true;
        return false;
    }

}
