<?php
/**
 * Класс описания поля сущности
 * ver 2.00
 *
 * todo Переписать
 * 1. сделать прототип с моделью попроще (без спец ft полей)
 * 2. этот класс сделать расширением описанного выше
 * 3. отказаться от описаний массивом
 * 4. сделать класс импорта/экспорта ( массив, yaml )
 *
 */

class ftModelField {

    /** @var string имя сущности */
    protected $sName;

    /** @var string название сущности */
    protected $sTitle;

    /** @var array описание поля */
    protected $aFieldModel = array();

    /** @var array набор параметров */
    protected $aParams = array();

    /**
     * Конструктор
     * @param string $sFieldName имя поля
     * @param array $aFieldModel описание поля
     * @throws ftException
     */
    public function __construct( $sFieldName, $aFieldModel ) {

        // если формат данных заведомо левый - то выходим
        if(!is_array($aFieldModel))
            throw new ftException('Неверный тип контейнера описания');

        // сохраняем описание
        $this->aFieldModel = $aFieldModel;

        // имя поля
        $this->sName = (string)$sFieldName;
        if ( !$this->sName )
            throw new ftException('Отсутствует имя поля');

        // название сущности
        $this->sTitle = (isset($aFieldModel['title']) and $aFieldModel['title']) ?
            (string)$aFieldModel['title'] :
            $this->getName();

    }

    /**
     * Отдает базовый набор для генерации поля
     * @static
     * @param string $sDatatype
     * @param string $sTitle
     * @return array
     */
    public static function getBaseDesc( $sDatatype, $sTitle ) {

        // задан размер
        if ( preg_match( '/^(\w+)[(](\d+)[)]$/i', $sDatatype, $find ) ){
            $sDatatype = $find[1];
            $size = $find[2];
        } else $size = '';

        if ( $sDatatype==='str' )
            $sDatatype = 'varchar';

        // todo сделать проверку доступных типов

        // формирование описания поля
        return array(
            'datatype' => $sDatatype,
            'multilang' => 0,
            'type' => 1,
            'required' => 1,
            'fictitious' => false,
            'size' => $size,
            'title' => $sTitle,
            'editor' => '',
            'widget' => array(),
            'validator' => array(),
            'modificator' => array()
        );

    }

    /**
     * Добавляет атрибут
     * @param $sAttrName
     * @param $mVal
     */
    public function setAttr( $sAttrName, $mVal ) {
        $this->aFieldModel[$sAttrName] = $mVal;
    }

    /**
     * Отдает атрибут
     * @param $sAttrName
     * @return mixed|null
     */
    public function getAttr( $sAttrName ) {
        return isset($this->aFieldModel[$sAttrName]) ? $this->aFieldModel[$sAttrName] : null;
    }

    /**
     * Отдает массив описания
     * @static
     * @return array
     */
    public function getModelArray() {
        return $this->aFieldModel;
    }

    /**
     * Задает значение флага "фиктивное поле"
     * @param bool $mVal
     */
    public function setFictitious( $mVal ) {
        $this->setAttr('fictitious',(bool)$mVal);
    }

    /**
     * Задает значение флага "фиктивное поле"
     * @return bool
     */
    public function getFictitious() {
        return (bool)$this->getAttr('fictitious');
    }

    /**
     * Флаг фиктивного поля
     * @return bool
     */
    public function isFictitious() {
        return $this->getFictitious();
    }

    /**
     * Отдает имя поля
     * @return string
     */
    public function getName(){
        return $this->sName;
    }

    /**
     * Отдает название поля
     * @return string
     */
    public function getTitle(){
        return $this->sTitle;
    }

    /**
     * Отдает тип переменной в базе
     * @return string
     */
    public function getDatatype() {
        return (string)$this->getAttr('datatype');
    }

    /**
     * Отдает тип переменной в базе с размерностью в скобках, если есть
     * @return string
     */
    public function getDatatypeFull() {

        // тип
        $sOut = $this->getDatatype();

        // расширить размерностью, если задана
        if ( $this->getSize() )
            $sOut .= sprintf('(%d)',$this->getSize());

        // для int по умолчанию размерность 11
        elseif ( $sOut === 'int' )
            $sOut .= '(11)';

        return $sOut;

    }

    /**
     * Отдает размер переменной в базе
     * @return int
     */
    public function getSize() {
        return (int)$this->getAttr('size');
    }

    /**
     * Задает переметр "обязательное"
     * @param $mVal
     * @return string
     */
    public function setRequired( $mVal ) {
        return (string)$this->setAttr('required',(int)(bool)$mVal);
    }

    /**
     * Задает значение мультиязычности
     * @param $iVal
     */
    public function setMultilang( $iVal ) {
        $this->setAttr('multilang',(int)$iVal);
    }

    /**
     * Отдает значение мультиязычности
     * @return int
     */
    public function getMultilang() {
        return (int)$this->getAttr('multilang');
    }

    /**
     * Отдает флаг "мультиязычное"
     * @return bool
     */
    public function isMultilang() {
        return (bool)$this->getMultilang();
    }

    /**
     * Возвращает true, если поле - подчиненная сущность
     * @return bool
     */
    public function isEntity() {
        return false;
    }

    /**
     * Отдает имя редактора
     * @return string
     */
    public function getEditorName() {
        return (string)$this->getAttr('editor');
    }

    /**
     * Устанавливает имя редактора для поля
     * @param string $sEditorName
     */
    public function setEditorName( $sEditorName ) {
        $this->setAttr('editor', (string)$sEditorName);
        // todo если создан объект - убить его
    }

    /**
     * Отдает массив с набором параметров
     * @return array
     */
    public function getParameterList() {
        return $this->aParams;
    }

    /**
     * Сохраняет массив с набором параметров
     * @param $aParamList
     */
    public function setParameterList( $aParamList ) {
        $this->aParams = $aParamList;
    }

    /**
     * Отдает значение параметра
     * @static
     * @param $sParamName
     * @return mixed|null
     */
    public function getParameter( $sParamName ) {
        return isset($this->aParams[$sParamName]) ? $this->aParams[$sParamName] : null;
    }

    /**
     * Задает значение параметра
     * @static
     * @param $sParamName
     * @param $mValue
     */
    public function setParameter( $sParamName, $mValue ) {
        $this->aParams[(string)$sParamName] = $mValue;
    }

    /**
     * Добавить в контейнер описания сущности - уникальное значение
     * @param $sContName
     * @param $sValue
     */
    protected function addToArrayUnique( $sContName, $sValue ) {
        if ( !in_array($sValue, $this->aFieldModel[$sContName]) )
            $this->aFieldModel[$sContName][] = $sValue;
    }

    /**
     * Добавление виджета
     * @param $sPName
     */
    public function addWidget( $sPName ) {
        $this->addToArrayUnique('widget', $sPName);
    }

    /**
     * Добавление виджета
     * @param $sPName
     */
    public function addModificator( $sPName ) {
        $this->addToArrayUnique('modificator', $sPName);
    }

    /**
     * Добавление виджета
     * @param $sPName
     */
    public function addValidator( $sPName ) {
        $this->addToArrayUnique('validator', $sPName);
    }

}
