<?php
/**
 * Модель данных для поля подчиненной сущности
 */

class ftModelFieldSub extends ftModelField {

    /**
     * Возвращает true, если поле - подчиненная сущность
     * @return bool
     */
    public function isEntity() {
        return true;
    }

    /** @var \ftModel объет описание подчиненной сущности */
    protected $oModel;

    /**
     * Отдает имя сущности
     * @return string
     */
    public function getEntityName() {
        return $this->oModel->getName();
    }
    
    /**
     * Конструктор
     * @param string $sFieldName имя поля
     * @param \ftModel $oModel
     * @param string $sConnectionType
     * @param string $sAltTitle
     */
    public function __construct( $sFieldName, ftModel $oModel, $sConnectionType='', $sAltTitle='' ) {

        // сохраняем описание
        $this->oModel = $oModel;

        // имя сущности
        $this->sName = (string)$sFieldName;
        if ( !$this->sName )
            throw new ftException('Отсутствует имя поля');

        // название сущности
        $this->sTitle = $sAltTitle ? $sAltTitle : $this->getEntityName();

        // запись типа cвязи в сущность
        if ( $sConnectionType )
            $oModel->setConnectionType( $sConnectionType );

        // добавление записи о содержащем поле в сущность
        $oModel->setParentField( $sFieldName );

        // связь типа МкМ
        if ( $sConnectionType === '><' )
            $oModel->setSourceEntity( $this->getEntityName() );

        // жесткая связь типа 1к1
        if ( $sConnectionType === '---' ){
            $oModel->setParentEntity( $this->getEntityName() );
            $mSubEntity = ft::entity($this->getEntityName());
            $mSubEntity
                ->setParentEntity( $this->getEntityName() )
                ->setParentField( $sFieldName )
                ->setConnectionType( '---' )
                ->save()
            ;
            if ( !$oModel->hasField('_parent') )
                ftFnc::error( new ftModelException('Для связи типа "---" должно быть задано поле родительской записи ('.$this->getEntityName().')') );
        }

    }

    /**
     * Отдает объект модели
     * @return ftModel
     */
    public function getModel() {
        return $this->oModel;
    }

    /**
     * Отдает тип переменной в базе
     * @return string
     */
    public function getDatatype() {
        return 'int';
    }

    /**
     * Флаг фиктивного поля
     * @return bool|string
     */
    public function isFictitious() {
        if ( parent::isFictitious() )
            parent::isFictitious();
        return $this->getModel()->connectionType();
    }

    /**
     * Отдает флаг "мультиязычное"
     * @return bool
     */
    public function isMultilang() {
        return false;
    }

}
