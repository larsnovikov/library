<?php

/**
 *
 * Набор функций для работы генератором сущностей
 * var 0.80
 *
 * $Author: sapozhkov $
 * $Revision: 2768 $
 * $Date: 2011-04-22 12:21:01 +0400 (Птн, 22 Апр 2011) $
 *
 */  

define('FT_EG_DATA_VERSION', 'ft_eg_data_version');         // версия данных EntityGen
define('FT_EG_LIFE_CNT', 'ft_eg_life_cnt');                 // набор загруженных сущностей с временем последнего вызова
define('FT_EG_MISS_ENTITY', 'ft_eg_miss_entity');           // набор не найденных сущнстей
define('FT_EG_ID_CACHE', 'ft_eg_id_cache');                 // переменная для хначения связок id - eg_table_name
define('FT_EG_USER_CACHE', 'ft__eg_user_cache');            // переменная для хранения пользовательских данных в кэше, 
                                                            //    сброс при обновлении ftEntityGen 
class ftEntityGen {
    
    // число циклов жизни сущности в кэше
    const maxLifeCnt = 25;
    
    // eg модель изменилась - обновить версию модели данных
    static public function updModelVersion() {
        
        // обновить параметр
        $query = 'UPDATE `parameters` SET `value`=1+`value` '.
                 "WHERE `parent`=3 AND `position`='.' AND `name`='ft_eg_model_ver'";
        mysql_query($query);
        
        // нет обновленных - создать
        if ( !mysql_affected_rows() ) {
            $query = 'INSERT INTO `parameters` (`id`, `parent`, `name`, `position`, `value`, `title`, `access_level`, `show_val`) '.
                     "VALUES (NULL, '3', 'ft_eg_model_ver', '.', '1', 'Версия модели ft entity_gen', '0', '')";
            mysql_query($query);
        }
        
        if ( !mysql_affected_rows() )
            self::error( 'Ошибка создания версии модели данных ftEntityGen.' );

    }

    /**
     * Отдает композитную версию данных
     * @static
     * @param $iMajorVer
     * @param int $iMinorVer
     * @return float
     */
    public static function getFormatedVersion( $iMajorVer, $iMinorVer=0 ){

        // если минорная версия не задана -
        if ( !$iMinorVer )
            $iMinorVer = self::getVersion();

        return (float)sprintf('%d.%06d', floor($iMajorVer), $iMinorVer);

    }
    
    /**
     * Отдает версию данных
     * @static
     * @return int
     */
    public static function getVersion(){
        $iVersion = 0;
        // если версия задана массиве параметров
        global $parameters;
        if ( !empty($parameters) and
             isset($parameters['.']) and
             isset($parameters['.']['ft_eg_model_ver'])
            )
        {
            $iVersion = (int)$parameters['.']['ft_eg_model_ver']['value'];
        }

        // не задана - прямой запрос
        else {
            $iVersion = self::getVersionFromBase();
        }
        return $iVersion;
    }

    /**
     * Отдает версию данных из базы
     * @static
     * @return int
     */
    public static function getVersionFromBase(){
        $iVersion = 0;
        $query = 'SELECT `value` FROM `'.MINC_DBPREFIX.'parameters` '.
                 "WHERE `parent`=3 AND `position`='.' AND `name`='ft_eg_model_ver'";
        $res = mysql_query($query);
        if ( !mysql_errno() and $res = mysql_fetch_assoc($res) ) {
            $iVersion = (int)$res['value'];
        }
        return $iVersion;
    }



    // проверка версии данных ftEntityGen
    static public function modelVersionCheck() {
        
        // текущая версия данных
        $ft_eg_ver = self::getVersion();

        // создать контейнер, если его нет
        if ( !isset($_SESSION[FT_EG_DATA_VERSION] ) )
            $_SESSION[FT_EG_DATA_VERSION] = -1;
        if ( !self::cache_isset() )
            self::cache_clear();
        
        // если версия данных устарела
        if ( $_SESSION[FT_EG_DATA_VERSION] < $ft_eg_ver ) {
            
            // запомнить текущую версию
            $_SESSION[FT_EG_DATA_VERSION] = $ft_eg_ver;

            // стереть кэш, пользователя, чтобы туда автоматически загрузились новые модели данных
            foreach ( self::cache_get(FT_EG_LIFE_CNT) as $entity_name => $cnt ) { 
                self::delFromCache( $entity_name );
            }
            
            // очистить кэш
            self::cache_clear();
                
        } // if
        
        self::updCounters();
        
    } // function modelVersionCheck
    
    // обновить счетчики циклов жизни сущностей и запустить сборщик мусора
    static public function updCounters() {
        
        if ( !self::cache_isset(FT_EG_LIFE_CNT) )
            self::cache_clear(FT_EG_LIFE_CNT);
        
        // иначе обойти все загруженные поля
        else {
            
            foreach ( self::cache_get(FT_EG_LIFE_CNT) as $entity_name => $cnt ) {
            	
                // обновить счетчик
                self::cache_set(FT_EG_LIFE_CNT,$entity_name,$cnt+1);
                
                // если привышено максималное число циклов жизни
                if ( $cnt > self::maxLifeCnt ) {
                    
                    // удалить счетчик
                    self::cache_del(FT_EG_LIFE_CNT,$entity_name);
                    
                    // удалить запись из кэша
                    self::delFromCache( $entity_name );
                    
                } // if
                    
            } // foreach
            unset($cnt);
        }
        
    } // function updCounters

    // запросить описание сущности
    static public function getEntityDef( $entity_name ) {
        return ft::table('eg_entity')->getEntityDef( $entity_name );
    } // function getEntityDef

    // проверка корректности имени
    static public function checkName( $entity_name ) {
        // начинаться должно с `egt_`
        return (bool)(strpos($entity_name, 'egt_') === 0);
    } // function checkName
    
    // приведение имени к правильному виду
    static public function rightName( $entity_name ) {
        if ( !self::checkName( $entity_name ) )
            $entity_name = 'egt_'.$entity_name;
        return $entity_name;
    } // function rightName
    
    // положить описание сущности в кэш
    static public function addToCache( $entity_name, $def ) {
        
        // занесение в кэш
        $_SESSION[FT_CACHE][$entity_name] = $def;
        self::nullLifeCnt($entity_name);
        
        // если задан id, добавить в кэш доступа по id
        if ( isset( $def['entity']['eg_id'] ) and $def['entity']['eg_id'] )
            $_SESSION[FT_EG_ID_CACHE][ $def['entity']['eg_id'] ] = $entity_name;
        
        // сохранение мультиязычной версии если есть
        ftFnc::isMultilang($def) and 
            $lang_def=ftLang::getLangEntity($def)->getModel() and
            $_SESSION[FT_CACHE][$entity_name.'_lang']=$lang_def;
                    
        return true;
    } // function addToCache
    
    // удаление записи о сущности из кэша 
    static public function delFromCache( $entity_name ) {
        if ( ftFnc::entityExists( $entity_name ) ) {
            
            // удаление мультиязычной версии если есть
            $lang_entity_name = $entity_name.'_lang';
            $def = &$_SESSION[FT_CACHE][$entity_name];
            if ( ftFnc::isMultilang($def) and ftFnc::entityExists( $lang_entity_name ) ) 
                unset($_SESSION[FT_CACHE][$lang_entity_name]);
            
            // кэш доступа по id
            self::cache_del(FT_EG_ID_CACHE,$def['entity']['eg_id']);
            
            // удаление основной записи
            unset($_SESSION[FT_CACHE][$entity_name]);

            return true;
            
        } else return false;
    } // function delFromCache
    
    // есть в списке промахов
    static public function isMissedEntity( $entity_name ) {
        return self::cache_isset(FT_EG_MISS_ENTITY,$entity_name);
    } // function isMissedEntity
    
    // добавить в список промахов
    static public function addMissEntity( $entity_name ) {
        if ( self::isMissedEntity($entity_name) )
            return false;
        self::cache_set(FT_EG_MISS_ENTITY,$entity_name,$entity_name);
        return true;
    } // function addMissEntity
    
    // установить в 0 счетчик циклов с момента последнего запроса
    static public function nullLifeCnt( $entity_name ) {
        if ( self::checkName($entity_name) and !preg_match('/_lang$/', $entity_name) )
            self::cache_set(FT_EG_LIFE_CNT,$entity_name,0);
    } // function nullLifeCnt
    
    // сформировать имя таблицы
    static public function getTableName( $alias, $type_id=0 ) {
        $type_id = (int)$type_id;
        $type_prefix = $type_id ? ft::table('eg_entity_type')->getPrefixById($type_id) : '';
        return self::rightName( $type_prefix.$alias );
    } // function getTableName
    
    // запросить языковые имена для сущности
    static public function getLangTitles( $entity_name ) {
        
        $out = array();
        
        if ( !self::checkName($entity_name) )
            return $out;
        
        $lang = ftLang::lang();
        
        //---------------------------------------
        // генерация запроса с данными для генерации
        $query = ft::query( 'eg_entity' )
            ->table_name( $entity_name )
            ->dataLang($lang)
            ->columns(array( 'id','title' ))
        ;
        // выполнение запроса
        $entity = $query->selectOne();
        
        if ( !$entity )
            return $out;
            
        $query = ft::query( 'eg_field' )
            ->dataLang($lang)
            ->_parent( $entity['id'] )
            ->columns(array( 'id','alias','title' ))
        ;
        $fields = $query->selectAll();
        
        foreach ($fields as $field) {
            if ( $field['title'] )
                $out[ $field['alias'] ] = $field['title'];
        } // foreach
        
        return $out;
    } // function getLangTitles
    
    // запросить имя сущности по id
    static public function getEntityNameById( $id, $type_id=0 ) {
        
        $id = (int)$id;
        
        // проверить в кэше
        if ( $def = self::cache_get(FT_EG_ID_CACHE,$id) )
            return $def;
        
        // запрос имени из базы
        $query = ft::query( 'eg_entity' )
            ->id($id)
            ->columns(array( 'id','table_name' ))
        ;
        
        // если задан тип - включить в запрос
        $type_id = (int)$type_id;
        if ( $type_id ) $query->type( $type_id );
        
        // выборка
        $row = $query->selectOne();
        
        return self::cache_set(FT_EG_ID_CACHE,$id,($row ? $row['table_name'] : ''));
        
    } // function getEntityNameById
    
    // установить значение кэша
    static public function cache_set( $branch_name, $value_name, $value='' ) {
        if ( func_num_args()===3 )
            return $_SESSION[FT_EG_USER_CACHE][$branch_name][$value_name] = $value;
        else
            return $_SESSION[FT_EG_USER_CACHE][$branch_name] = $value_name;
    } // function cache_set
    
    // запросить наличие значения в кэше
    static public function cache_isset( $branch_name='', $value_name='' ) {
        
        $n = func_num_args();
        
        // нет аргументов
        if ( !isset( $_SESSION[FT_EG_USER_CACHE])  )
            return false;
        
        // просто наличие
        if ( !$n ) return true;
        
        // 1 аргумент
        if ( !isset( $_SESSION[FT_EG_USER_CACHE][$branch_name])  )
            return false;
        if ( $n===1 ) return true;

        // 2 аргумента
        if ( !isset( $_SESSION[FT_EG_USER_CACHE][$branch_name][$value_name])  )
            return false;
        return true;

    } // function cache_isset
    
    // запросить значение кэша
    static public function cache_get( $branch_name='', $value_name='' ) {
        if ( !func_num_args() ) {
            return self::cache_isset() ? $_SESSION[FT_EG_USER_CACHE] : array();
        }
        
        // запрос всей ветки
        elseif ( func_num_args() === 1 ) {
            return self::cache_isset( $branch_name ) ? 
                $_SESSION[FT_EG_USER_CACHE][$branch_name] : 
                array();
        }
        // запрос конкретного значения
        else {
            return self::cache_isset( $branch_name, $value_name ) ? 
                $_SESSION[FT_EG_USER_CACHE][$branch_name][$value_name] : 
                null;
        }
    } // function cache_get
    
    // очистка кэша
    static public function cache_clear( $branch_name='' ) {
        if ( func_num_args() )
            $_SESSION[FT_EG_USER_CACHE][$branch_name] = array();
        else
            $_SESSION[FT_EG_USER_CACHE] = array();
    } // function cache_clear
    
    // здалить значение кэша
    static public function cache_del( $branch_name, $value_name='' ) {
        
        // удаление всей ветки
        if ( func_num_args() === 1 ) {
            if ( $res = self::cache_isset( $branch_name ) ) 
                unset( $_SESSION[FT_EG_USER_CACHE][$branch_name] );
        }
        
        // удаление конкретного значения
        else {
            if ( $res = self::cache_isset( $branch_name, $value_name ) ) 
                unset( $_SESSION[FT_EG_USER_CACHE][$branch_name][$value_name] ); 
        }
        return $res;
    } // function cache_del
    
}
