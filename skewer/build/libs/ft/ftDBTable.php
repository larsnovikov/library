<?php
/**
 * Класс для изменения структуры таблиц по модели
 */
class ftDBTable {

    /** @var array набор зарезервированных описаний полей */
    protected static $aReservedFieldTypes = array(
        'id' => 'int NOT NULL auto_increment',
        '_parent' => 'int default 0 NOT NULL',
        '_group' => "varchar(100) NOT NULL default ''",
        '_parent_entity' => "varchar(200) NOT NULL default ''",
        '_add_date' => "datetime NOT NULL default '0000-00-00 00:00:00'",
        '_upd_date' => "datetime NOT NULL default '0000-00-00 00:00:00'",
    );

    /**
     * Проверяет нлаличие таблицы по имени
     * @param string $sTableName
     * @return bool
     */
    public static function tableExists($sTableName){
        $rList = self::query('SHOW TABLES');
        while ($row = $rList->fetch_array(MYSQLI_NUM) ) {
            if ($sTableName==$row[0]) {
                return true;
            }
        }
        return false;
    }

    /**
     * Создает таблицу связей 2 таблиц
     * @param string $sSourceEntityName имя первичной таблицы
     * @param string $sTargetEntityName имя привязываемой таблицы
     * @param bool $bCheckOnly только проверка, создавать не нужно
     * @return string пусто если таблица есть
     */
    public static function repairConnectionTable($sSourceEntityName, $sTargetEntityName, $bCheckOnly=false) {

        // имя таблицы
        $table_name = 'ft_rel_'.$sSourceEntityName.'2'.$sTargetEntityName;

        if( self::tableExists($table_name) ){
            $status = $bCheckOnly ? '' : 'exist';
        } else {
            if ( $bCheckOnly )
                return "no connection table $sSourceEntityName-$sTargetEntityName";
            $query = "CREATE TABLE IF NOT EXISTS `$table_name` (".
            "`s_id` INT NOT NULL, ".
            "`t_id` INT NOT NULL, ".
            "KEY `s_id` (`s_id`), ".
            "KEY `t_id` (`t_id`))";

            $status = self::query($query,false);

            if ( self::getQueryError() )
                ftFnc::error( new ftInnerException('Ошибка создания таблицы связи') );

        }

        return $status;

    }

    /**
     * Добавляет модели поля по таблице
     * @static
     * @param ftModel $oModel
     * @return void
     */
    protected static function generateFieldsFromTable( ftModel $oModel ) {

        // список полей
        $rFields = self::query('SHOW COLUMNS FROM `'.$oModel->getTableName().'`',false);
        if ( self::getQueryError() ) {
            ftFnc::error( new ftException( self::getQueryError() ) );
            return;
        }

        // собираем данные по полям
        while( $aField=$rFields->fetch_array(MYSQLI_ASSOC) ) {

            // добавляем поле
            $oModel->addField( $aField['Field'], $aField['Type'] );

        }

    }

    /**
     * Добавляет модели индексы по таблице
     * @static
     * @param ftModel $oModel
     * @return void
     */
    protected static function generateIndexesFromTable( ftModel $oModel ) {

        // запросить все индексы
        $rIndexes = self::query( 'SHOW INDEXES FROM `'.$oModel->getTableName().'`' );

        $aIndexes = array();

        // сгруппировать индексы
        while ( $aRow = $rIndexes->fetch_array(MYSQLI_ASSOC) ) {
            $sIndexName = $aRow['Key_name'];
            if ( !isset($aIndexes[$sIndexName]) ) {
                $aIndexes[$sIndexName] = array(
                    'name' => $sIndexName,
                    'index_type' => $aRow['Index_type'],
                    'fields' => array(),
                    'unique' => (bool)!$aRow['Non_unique']
                );
            }
            $aIndexes[$sIndexName]['fields'][$aRow['Seq_in_index']] = $aRow['Column_name'];
        }

        // добавление индексов
        try {
            foreach ( $aIndexes as $aIndex ) {
                ksort($aIndex['fields']);
                $oModel->addIndex( $aIndex['name'], $aIndex['fields'], $aIndex['index_type'], $aIndex['unique'] );
            }
        } catch ( ftException $e ) {
            ftFnc::error($e);
        }

    }

    /**
     * Генерирует модель по реальной таблице в базе
     * @param string $sTableName имя таблицы
     * @param string $sPrefix префикс в имени таблицы
     * @return \ftModel|null
     */
    public static function generateFromTable( $sTableName, $sPrefix ) {

        // проверить на наличие
        if ( !self::tableExists($sTableName) )
            return null;

        $oModel = new ftModel( ftModel::getBlankArray( $sTableName ) );
        $oModel->setTablePrefix($sPrefix);

        // генерация набора полей
        self::generateFieldsFromTable( $oModel );

        // генерация набора индексов
        self::generateIndexesFromTable( $oModel );

        // отдать модель
        return $oModel;

    }

    /**
     * Обновляет таблицу в базе данных по моделям
     * @param ftModel $oNewModel
     * @param ftModel $oOldModel
     * @return bool
     */
    protected static function updateTable( ftModel $oNewModel, ftModel $oOldModel ) {

        /**
         * @var ftModelField|ftModelFieldSub $oNewField поле новой модели
         * @var ftModelField $oOldField поле старой модели
         */

        // набор полей для удаления
        $aDropFieldList = array();

        // проверяем поля которые были добавлены или изменены
        foreach ( $oNewModel->getFileds() as $oNewField ) {

            // флаг фиктивного поля
            if ( $sConnType = $oNewField->isFictitious() ) {
                if ( $sConnType === '><' ) {
                    self::repairTable( $oNewField->getModel() );
                }
                $aDropFieldList[] = $oNewField->getName();
                continue;
            }

            // модель старого поля
            $oOldField = $oOldModel->getFiled($oNewField->getName());

            // если поля в таблице нет
            if( !$oOldField ){

                self::addTableField( $oNewModel, $oNewField );

            }

            // иначе проверяем не изменилось ли поле
            elseif( $oOldField->getDatatype()!==$oNewField->getDatatypeFull()
                    and !in_array($oNewField->getName(), ftFnc::reservedNames()))
            {

                self::updTableField( $oNewModel, $oNewField );

            }

        }

        // проверяем поля которые были удалены
        foreach( $oOldModel->getFileds() as $oOldField ) {

            // если нет в списке на удаление
            if ( !in_array($oOldField->getName(), $aDropFieldList) ) {
                // если присутствует в новой модели
                if ( $oNewModel->hasField($oOldField->getName()) ) continue;;
            }

            self::query(sprintf('ALTER TABLE `%s` DROP `%s`',$oOldModel->getTableName(), $oOldField->getName()));

        }

        self::updateTableIndexes( $oNewModel, $oOldModel );

        return true;

    }

    /**
     * Добавляет поле к таблице
     * @param ftModel $oModel
     * @param ftModelField $oField
     */
    protected static function addTableField( ftModel $oModel, ftModelField $oField ) {

        // формат запроса на добавление
        $sQueryFormat = 'ALTER TABLE `%s` ADD `%s` %s %s';

        // выполнить запрос
        self::useUpdFieldTpl( $sQueryFormat, $oModel, $oField );

    }

    /**
     * Обновляет поле в таблице
     * @param ftModel $oModel
     * @param ftModelField $oField
     */
    protected static function updTableField( ftModel $oModel, ftModelField $oField ) {

        // формат запроса на обновление
        $sQueryFormat = 'ALTER TABLE `%s` CHANGE `%s` `%2$s` %s %s';

        // выполнить запрос
        self::useUpdFieldTpl( $sQueryFormat, $oModel, $oField );

    }

    /**
     * Применяет шаблон запроса для изменения полей таблицы
     * @static
     * @param $sQueryFormat
     * @param ftModel $oModel
     * @param ftModelField $oField
     */
    protected static function useUpdFieldTpl( $sQueryFormat, ftModel $oModel, ftModelField $oField ) {

        // описание поля
        $sRFieldType = '';

        // если поле служебное - создаем его по хитрому
        if ( isset(self::$aReservedFieldTypes[$oField->getName()]) )
            $sRFieldType = self::$aReservedFieldTypes[$oField->getName()];

        // нет спец описания - задать как есть
        if ( !$sRFieldType )
            $sRFieldType = sprintf('%s %s', $oField->getDatatypeFull(), 'NOT NULL');

        $sLastFieldName = '';
        foreach ( $oModel->getFileds() as $oTmpField ) {
            if ( $oTmpField->getName() === $oField->getName() )
                break;
            if ( ! $oTmpField->isFictitious() )
                $sLastFieldName = $oTmpField->getName();
        }

        // часть запроса с позиционированием
        $sPositionQuery = $sLastFieldName ? "AFTER `$sLastFieldName`" : "AFTER `id`";

        // собрать запрос
        $sQuery = sprintf($sQueryFormat,
            $oModel->getTableName(),
            $oField->getName(),
            $sRFieldType,
            $sPositionQuery
        );

        // выполнить
        self::query( $sQuery );

    }

    protected static function equalIndexes( ftModelIndex $oNewIndex, ftModelIndex $oOldIndex ) {

        // сравнение по полям
        if ( $oNewIndex->getFileds() !== $oOldIndex->getFileds() )
            return false;

        // сравнение по уникальности
        if ( $oNewIndex->isUnique() !== $oOldIndex->isUnique() )
            return false;

        // сравнение по уникальности
        if ( $oNewIndex->getIndexType() !== $oOldIndex->getIndexType() )
            return false;

        return true;

    }

    /**
     * Отдает префикс для запроса добавления индекса
     * @param ftModelIndex $oIndex
     * @return string
     */
    protected static function getSqlIndexPrefix( ftModelIndex $oIndex ) {

        $sType = $oIndex->getTypeAlias();

        if ( !$sType )
            throw new ftModelException('В описании индакса отсутствует обязательный параметр `type`.');

        if ( strtolower($sType) === 'primary' )
            $sOut = 'PRIMARY KEY';
        else
            $sOut = sprintf('%s `%s`', strtoupper($sType), $oIndex->getName());

        return $sOut;

    }

    /**
     * Обновляет индексы таблицы в базе данных по моделям
     * @param ftModel $oNewModel
     * @param ftModel $oOldModel
     */
    protected static function updateTableIndexes( ftModel $oNewModel, ftModel $oOldModel ) {

        /**
         * @var ftModelIndex $oNewIndex новый индекс
         * @var ftModelIndex $oOldIndex старый индекс
         */

        $aOldIndexes = $oOldModel->getIndexes();

        // соответствие типа и выбираемых полей
        foreach ($oNewModel->getIndexes() as $oNewIndex) {

            // старый индекс
            $oOldIndex = $oOldModel->getIndex( $oNewIndex->getName() );

            // если нет в старой таблице индекса - добавить
            if ( !$oOldIndex ) {
                self::addTableIndex( $oOldModel, $oNewIndex );
            }

            // если есть старый - сравнить индексы
            elseif ( !self::equalIndexes( $oNewIndex, $oOldIndex ) ) {

                // убить старый индекс
                self::dropTableIndex( $oOldModel, $oOldIndex );

                // добавить новый индекс
                self::addTableIndex( $oOldModel, $oNewIndex );

            }

            // убрать из списка - обработано
            unset($aOldIndexes[$oNewIndex->getName()]);

        }

        // убрать оставшиеся индексы, поскольку их нет в описании
        foreach ($aOldIndexes as $oOldIndex) {
            self::dropTableIndex( $oOldModel, $oOldIndex );
        }

    }

    /**
     * Добавляет индекс
     * @param \ftModel $oModel
     * @param \ftModelIndex $oIndex
     * @return bool
     */
    protected static function addTableIndex( ftModel $oModel, ftModelIndex $oIndex ) {
        self::query( sprintf(
            'ALTER TABLE `%s` ADD %s (`%s`)',
            $oModel->getTableName(),
            self::getSqlIndexPrefix( $oIndex ),
            implode('`,`',$oIndex->getFileds())
        ));
        return !(bool)self::getQueryError();
    }


    /**
     * Удляет индекс
     * @param \ftModel $oModel
     * @param \ftModelIndex $oIndex
     * @return bool
     */
    protected static function dropTableIndex( ftModel $oModel, ftModelIndex $oIndex ) {
        self::query( sprintf(
            'ALTER TABLE `%s` DROP INDEX `%s`',
            $oModel->getTableName(),
            $oIndex->getName()
        ));
        return !(bool)self::getQueryError();
    }

    /**
     * Перестроить таблицу в соответствии с заданным описанием
     * @param \ftModel $oNewModel объект описания сущности
     * @return bool
     */
    public static function repairTable(ftModel $oNewModel) {

        // проверка таблиц связей (с их созданием в случае отсутствия)
        if( $oNewModel->connectionType() === '><' ) {
            self::repairConnectionTable( $oNewModel->getSourceEntity(), $oNewModel->getName() );
        }

        // модель в базе данных
        $oOldModel = self::generateFromTable($oNewModel->getTableName(), $oNewModel->getTablePrefix());

        // если таблица уже есть
        if( $oOldModel ) {

            // обновить её
            $bRes = self::updateTable( $oNewModel, $oOldModel );

        }

        else {

            // создать её
            $bRes = self::createTable($oNewModel);

        }

        // если таблица мультиязычная
        if ( $oNewModel->isMultilang() ) {

            // создаем новую таблицу с языковым расширением
            ftLang::getLangEntity($oNewModel)->build();

        }

        // не мультиязычнаня - проверить чтобы не было языковой таблицы
        else {

            $sLangTableName = $oNewModel->getTableName() .'_lang';
            if ( self::tableExists($sLangTableName) )
                self::deleteTable( $oNewModel, true );

        }

        return $bRes;

    }

    /**
     * Создает таблицу по модели
     * @param ftModel $oModel
     * @return bool
     */
    protected static function createTable( ftModel $oModel ) {
        $fields = array();

        $sQuery = 'CREATE TABLE IF NOT EXISTS `' . $oModel->getTableName() . '` (';

        // перебираем поля данных
        foreach ($oModel->getFileds() as $oField) {

            $sFieldName = $oField->getName();

            // фиктивные поля добавлять не надо
            if ($oField->isFictitious())
                continue;

            $sFieldType = '';

            $sNull = "NOT NULL";

            // если задан спец формат параметра
            if (isset(self::$aReservedFieldTypes[$sFieldName]))
                $sFieldType = self::$aReservedFieldTypes[$sFieldName];

            // если формат еще не задан
            if (!$sFieldType)
                $sFieldType = $oField->isEntity() ? 'int' : $oField->getDatatypeFull() . ' ' . $sNull;

            // так mysql делает (чтобы не было конфликта при перестроении)
            if ($sFieldType === 'int')
                $sFieldType = 'int(11)';

            // создаем поле
            $fields[] = sprintf('`%s` %s', $oField->getName(), $sFieldType);

        }

        // добавить поля в запрос
        $sQuery .= implode(', ', $fields);

        // индексы
        $aIndexes = array();
        foreach ($oModel->getIndexes() as $oIndex) {

            $aIndexes[] = sprintf(
                '%s (`%s`)',
                self::getSqlIndexPrefix($oIndex),
                implode('`,`', $oIndex->getFileds())
            );

        }

        // добавить индексы в запрос
        $sQuery .= ", " . implode(', ', $aIndexes);

        // собрать хвост запроса
        $sQuery .= ") ENGINE=MyISAM DEFAULT CHARSET=utf8;";

        // выполнить запрос
        self::query($sQuery);

        return !self::getQueryError();

    }

    /**
     * Удаляет таблицу
     * @param ftModel $oModel
     * @param bool $bLangOnly
     * @return bool
     */
    public static function deleteTable( ftModel $oModel, $bLangOnly=false ) {

        // удалять можно только автогенерированные таблицы
        if ( !ftFnc::egInited() or !ftEntityGen::checkName($oModel->getTableName()) )
            return false;

        $bRes = true;
        if ( !$bLangOnly ) {

            // выполнение запроса на удаление
            self::query(sprintf('DROP TABLE `%s`', $oModel->getTableName()));

            // не должно быть ошибок
            $bRes = !self::getQueryError();

        }

        // если сущность мультиязычная - удалить языковую таблицу
        if ( $bLangOnly or $oModel->isMultilang() ) {
            $query = sprintf('DROP TABLE `%s_lang`', $oModel->getTableName());
            self::query($query);
        }

        // результат выполнения
        return $bRes;

    }

    /**
     * переименовать таблицу
     * @param \ftModel $oModel
     * @param $sNewName
     * @return bool
     */
    public static function renameTable( ftModel $oModel, $sNewName ) {

        if ( !$sNewName )
            return false;

        $sNewName = mysql_real_escape_string( $sNewName );

        // переименовывать можно только автогенерированные таблицы
        if ( !ftFnc::egInited() or !ftEntityGen::checkName($oModel->getName()) )
            return false;

        // выполнение запроса на удаление
        self::query(sprintf('RENAME TABLE `%s` TO `%s`', $oModel->getName(), $sNewName));

        $bRes = !self::getQueryError();

        // если сущность мультиязычная - удалить языковую таблицу
        if ( $oModel->isMultilang() )
            self::query(sprintf('RENAME TABLE `%s_lang` TO `%s_lang`', $oModel->getName(), $sNewName));

        // результат выполнения
        return $bRes;

    }

    /**
     * Выполняет запрос в рамках объекта
     * @param $query
     * @param bool $bShowError
     * @return bool|mysqli_result
     */
    protected static function query( $query, $bShowError=true ) {
        global $odb;
        ftFnc::queryDebuger( $query );
        $r = $odb->query( $query );
        if ( $bShowError )
            if ($odb->errno) ftFnc::error(new ftInnerException($odb->error));
        return $r;
    }

    protected static function getQueryError() {
        global $odb;
        $odb->error;
    }

}
