<?php

/**
 *
 * Набор функций для работы с языковыми версиями
 * var 0.80
 *
 * $Author: sapozhkov $
 * $Revision: 6 $
 * $Date: 2012-05-18 10:52:53 +0400 (Пт., 18 мая 2012) $
 *
 */  

class ftLang {
    
    const delimiter = '|';
    
    /**
     * инициализация языковой переменной
     * @static
     * @return bool
     */
    static public function init() {

        global $aLangList;
        if ( !ftFnc::hasLanguages() )
            return false;
        global $ft_language;
        if ( isset($ft_language) )
            return true;
        
        // задание параметров, если их нет
        foreach ($aLangList as $sKey=>$sVal)
            if ( !isset($aLangList[$sKey]['alias']) )
                $aLangList[$sKey]['alias'] = substr($aLangList[$sKey]['name'], 0, 1);
        
        $ft_language = array(
            'default' => $aLangList[$aLangList['default']]['alias'],
            'now' => $aLangList[$aLangList['default']]['alias'],
            'list' => array(),
        );

        // todo установка текущего языка
        $ft_language['now'] = 'ru';

        foreach ($aLangList as $key=>$value) {
            if ( $key !== 'default' )
                $ft_language['list'][$value['alias']] = array(
                    'alias' => $value['alias'],
                    'title' => $value['locale_name'],
                );
        }

        return true;

    }
    
    /**
     * текущий язык
     * @static
     * @return mixed
     */
    static public function lang(){
        global $ft_language;
        return $ft_language['now'];
    }
    
    /**
     * язык по умолчанию
     * @static
     * @return mixed
     */
    static public function defaultLang (){
        global $ft_language;
        return $ft_language['default'];
    }
    
    /**
     * текущий язык не является языком по умолчанию
     * @static
     * @return bool|mixed
     */
    static public function notDefaultLang() {
        $lang = self::lang();
        return $lang===self::defaultLang() ? false : $lang;
    }
    
    /**
     * набор языков
     * @static
     * @param bool $num_index
     * @return array
     */
    static public function langList ( $num_index=false ){
        global $ft_language;
        if ( !$ft_language )
            return array();
        return $num_index ? array_values($ft_language['list']) : $ft_language['list'];
    }
    
    /**
     * набор языков без языка по умолчанию
     * @static
     * @return array
     */
    static public function notDefaultLangList() {
        $list = self::langList();
        unset( $list[ self::defaultLang() ] );
        return $list;
    }
    
    /**
     * набор языков с числовыми индексами
     * @static
     * @param bool $num_index
     * @return array
     */
    static public function listInt ( $num_index=false ){
        return self::langList(1);
    }
    
    /**
     * имеет ли набор такой язык
     * @static
     * @param $lang
     * @return bool
     */
    static public function hasLang ( $lang ){
        global $ft_language;
        return isset( $ft_language['list'][$lang] );
    }

    /**
     * Отдает объект языковой версии
     * @static
     * @param \ftModel $oModel
     * @return ftEntity
     */
    static public function getLangEntity( ftModel $oModel ) {
        
        // создаем новую таблицу с языковым расширением
        $oLangEntity = ft::entity($oModel->getName().'_lang',$oModel->getTitle().'(lang)')
            ->clear()
        
            ->addField( '_id', 'int(11)' )
            ->addField( '_lang', 'varchar(7)' )
            
            ->selectFields('_id,_lang')
                ->addIndex()
            
        ;
        
        // добавление мульиязычных полей
        foreach ($oModel->getFileds() as $oField)
            if ( $oField->isMultilang() )
                $oLangEntity->addFieldObject($oField->getName(), $oField);

        return $oLangEntity->dropMultilang( true );
        
    }

}

ftLang::init();
