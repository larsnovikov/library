<?php

/*
 * Редактор сущностей
 * ver 2.00
 */

class ftEntity {
    
    /** @var string имя сущности */
    protected $sEntityName;

    public function entityName(){ return $this->sEntityName; } // аксессор
    
    /** @var string название сущности */
    protected $sEntityTitle;

    /** @var ftModel описание сущноcти */
    protected $oModel;
    
    /** @var bool статус существования сущности */
    protected $bExists;

    /** @var array набор выбранных полей */
    protected $aSelectedFields = array();
    
    /** @var bool Флаг наличия полей, подключенных из внешней сущности */
    protected $hasExternalFields = false;
 
    /** @var bool Флаг наличия полей, подключенных из динамической внешней сущности */
    protected $hasDinamicExternalFields = false;

    /**
     * Консткуктор
     * @param string $sEntityName псевдоним сущности
     * @param string $sEntityTitle название сущности
     * @return \ftEntity
     */
    public function __construct( $sEntityName, $sEntityTitle='' ){

        // имя и название сущности
        $this->sEntityName = (string)$sEntityName;
        $this->sEntityTitle = $sEntityTitle ? (string)$sEntityTitle : $this->sEntityName;

        // взять описание сущность
        $oModel = ft::getModel( $sEntityName );
        
        // если описание валидно
        if ( $oModel ){
        
            // заполнить системные переменные
            $this->oModel = $oModel;
            $this->bExists = true;
            
        }
        
        // если описание не валидно или сущности нет
        else {

            $oModel = new ftModel( ftModel::getBlankArray($sEntityName, $sEntityTitle) );

            $this->bExists = false;
        
        }

        $this->oModel = $oModel;
        
        return $this;

    }

    /**
     * Отдает имя сущности
     * @return string
     */
    public function getEntityName() {
        return $this->oModel->getName();
    }

    /**
     * Отдает true - если сущность уже задана
     * @return bool
     */
    public function exists() {
        return $this->bExists;
    }
    
    /**
     * Возвращает массив описания сущности
     * @return \ftModel
     */
    public function getModel() {
        return $this->oModel;
    }
    
    /**
     * Распечатывает описание сущности
     * @return \ftEntity
     */
    public function showModel() {
        echo('<pre>');print_r( $this->oModel->getModelArray() );echo('</pre>');
        return $this;
    }
    
    /**
     * Генерирует ошибку
     * @param $e
     */
    protected function error( $e ){
        ftFnc::error( $e );
    }

    /**
     * Определяет наличие поля в описании
     * @param string $sFieldName имя поля
     * @return bool
     */
    public function fieldExists( $sFieldName ) {
        return $this->oModel->hasField( $sFieldName );
    }

    /**
     * Возвращает массив выбранных полей
     * @return array
     */
    public function selectedField(){
        return $this->aSelectedFields;
    }

    /**
     * Сбросить выборку полей
     * @param string $fields
     * @return \ftEntity
     */
    public function unselect( $fields='' ) {
        
        // нет параметров - сбросить все
        if ( !func_num_args() ) {
            $this->aSelectedFields = array();
        }
        
        // есть параметр - сбросить заданные
        else {
            $fields = ftFnc::toArray($fields);
            $this->aSelectedFields = array_diff($this->aSelectedFields,$fields);
        }
        
        return $this;

    }

    /**
     * Выбирает поле
     * @param string|ftModelField $mField поле
     * @param bool $bAdd - флаг "добавть к существующей выборке"
     * @return \ftEntity
     */
    public function selectField( $mField, $bAdd=false ){
    
        // сбросить выборку полей, если нет флага добавления
        if ( !$bAdd )
            $this->unselect();

        $sFieldName = is_object($mField) ? $mField->getName() : $mField;
        
        // добавить поле в список выбранных
        if ( $this->fieldExists($sFieldName) )
            $this->aSelectedFields[] = $sFieldName;
        
        return $this;
        
    }

    /**
     * Выбирает несколько полей
     * @param string|array $mFieldList набор полей
     * @return \ftEntity
     */
    public function selectFields( $mFieldList ){
    
        // формирование набора
        $aFieldList = $this->oModel->makeFieldsSet( $mFieldList );
        
        // задание выбранных полей
        $this->aSelectedFields = $aFieldList;
        
        return $this;
        
    }

    /**
     * Выбирает все поля
     * @return \ftEntity
     */
    public function selectAllFields(){
        $this->aSelectedFields = $this->oModel->getAllFieldNames();
        return $this;
    }

    /**
     * Выбирает набор полей по типу
     * @param string|array $mTypes
     * @param bool $bAdd
     * @return \ftEntity
     */
    public function selectFieldsByType( $mTypes, $bAdd=false ){
        
        // сбросить выборку полей, если нет флага добавления
        if ( !$bAdd )
            $this->unselect();
        
        // приведение к типу
        $aTypes = ftFnc::toArray($mTypes);

        // выборка указанных с размерностью
        $aTypesWithSize = array();
        foreach ($aTypes as $iKey=>$sType) {
            if ( strpos($sType,'(') !== false ){
                $aTypesWithSize[] = $sType;
                unset($aTypes[$iKey]);
            }
        }

        // перебрать все поля
        foreach ($this->oModel->getFileds() as $oField ) {

            // если не подчиненная сущность и тип подходит, то добавить
            if ( !$oField->isEntity() ){

                if( in_array($oField->getDatatype(), $aTypes) )
                    $this->selectField($oField, true);
                elseif ( $aTypesWithSize and in_array(sprintf('%s(%d)',$oField->getDatatype(),$oField->getSize()), $aTypesWithSize) )
                    $this->selectField($oField, true);
            }

        }

        return $this;
        
    }


    /**
     * Выбирает набор полей по админскому модификатору
     * @param $mTypes
     * @param bool $add
     * @return \ftEntity
     */
    public function selectFieldsByEditor( $mTypes, $add=false ){
        
        // сбросить выборку полей, если нет флага добавления
        if ( !$add )
            $this->unselect();

        $aTypes = ftFnc::toArray( $mTypes );
        
        // перебрать все поля
        foreach ( $this->oModel->getFileds() as $oField ) {
            
            if ( !$oField->isEntity() and $oField->getEditorName() and in_array($oField->getEditorName(), $aTypes) )
                $this->selectField($oField, true);
            
        }

        return $this;
        
    }


    /**
     * Устанавливает редактор
     * @param string $sEditorName имя редактора
     * @param string|null $mFields набор целевых полей, если пусто - используется текущий
     * @return \ftEntity
     */
    public function setEditor( $sEditorName, $mFields=null ) {
        
        // набор полей
        $aFieldList = $mFields ? $this->oModel->makeFieldsSet( $mFields ) : $this->aSelectedFields;
        
        // установка редакторов всем полям
        foreach ( $aFieldList as $sFieldName ) {
            $oField = $this->oModel->getFiled($sFieldName);
            if ( $oField )
                $oField->setEditorName( $sEditorName );
        }

        return $this;
        
    }


    /**
     * Добавляет псевдоним для набора полей
     * @param string $sSetName имя набора
     * @param string|array $mFieldList набор полей
     * @return \ftEntity
     */
    public function addColumnSet( $sSetName, $mFieldList ) {
        $this->oModel->addColumnSet( $sSetName, $mFieldList );
        return $this;
    }
    
    /**
     * Очищает сущность
     * @return \ftEntity
     */
    public function clear() {

        // заполнить системные переменные
        $this->oModel = new ftModel( ftModel::getBlankArray( $this->getEntityName(), $this->oModel->getTitle() ) );

        // добавление id
        $this
            ->addField( 'id', 'int(11)', 'Номер' )
                ->setEditor('show')
                ->addIndex( 'PRIMARY' )
        ;

        return $this;
        
    }

    /**
     * Задает префикс базы данных
     * @param $sPrefix
     * @return \ftEntity
     */
    public function setTablePrefix( $sPrefix ) {
        $this->oModel->setTablePrefix($sPrefix);
        return $this;
    }

    /**
     * Копирует описания сущности
     * @param $sNewEntityName
     * @param string $sNewEntityTitle
     * @return \ftEntity
     */
    public function cloneEntity( $sNewEntityName, $sNewEntityTitle='' ){

        // имя сущности
        $sNewEntityName = (string)$sNewEntityName;
        $sNewEntityTitle = $sNewEntityTitle ? (string)$sNewEntityTitle : $this->getEntityName();

        // заготовка для новой сущности
        $oClone = ft::entity($sNewEntityName,$sNewEntityTitle)->clear();

        // копирование описания
        $oClone->oModel = $this->oModel;

        return $oClone;
        
    }

    /**
     * Добавляет поле к сущности
     * @param string $sFieldName
     * @param string $sDatatype
     * @param string $sTitle
     * @return \ftEntity
     */
    public function addField( $sFieldName, $sDatatype='varchar', $sTitle='' ){
    
        // заполнение поля если пусто
        if ( !$sTitle )
            $sTitle = $sFieldName;
        
        // дополнения списка полей
        $this->oModel->addField( $sFieldName, $sDatatype, $sTitle );

        // выбрать добавленное поле
        $this->selectField( $sFieldName );
        
        return $this;
        
    }

    /**
     * Добавляет поле с уже сформированным объектом
     * @param $sFieldName
     * @param \ftModelField $oFiled
     */
    public function addFieldObject( $sFieldName, ftModelField $oFiled ) {
        $this->oModel->addFieldObject( $sFieldName, $oFiled );
    }

    /**
     * Добавить поля из внешней сущности
     * @param string $sEntityName
     * @throws Exception
     * @return \ftEntity
     */
    public function addExternalFields( $sEntityName ) {

        try {

            // запросить описание сущности
            $oAddModel = ft::getModel( $sEntityName );

            if ( !$oAddModel )
                throw new Exception('При создании сущности `'.$this->getEntityName()."` не найдена расширяющая сущность `$sEntityName`");

            // поставить флаг наличия внешних полей
            $this->hasExternalFields = true;

            // поставить флаг наличия полей, подключенных из динамической внешней сущности
            if ( ftFnc::egInited() and ftEntityGen::rightName($sEntityName) )
                $this->hasDinamicExternalFields = true;

            // добавить нужные поля
            foreach ( $oAddModel->getFileds() as $oField ) {

                // имя поля
                $sFieldName = $oField->getName();

                // служебно поле `id` не обрабатываем
                if ( $sFieldName === 'id' )
                    continue;

                // удалить поле, если оно уже есть
                if ( $this->oModel->hasField($sFieldName) ) {

                    // слить наборы параметров
                    $oField->setParameterList( array_merge(
                        $this->oModel->getFiled($sFieldName)->getParameterList(),
                        $oField->getParameterList())
                    );

                    // удалить поле
                    $this->oModel->delField($sFieldName);

                }

                // добавить новое поле
                $this->oModel->addFieldObject($sFieldName,$oField);

            }

            // добавление колонок из внешней сущности
            foreach( $oAddModel->getColumnSetList() as $sColName => $mColVal ) {
                $this->oModel->addColumnSet($sColName,$mColVal);
            }

            // снять выделение с полей
            $this->unselect();

        } catch(Exception $e){
            ftFnc::error($e);
        }

        return $this;

    }

    /**
     * Добавляет подчиненную сущность к текущей
     * @param $sFieldName
     * @param string|ftEntity $mSubEntity
     * @param string $sConnectionType
     * @param string $sAltTitle
     * @return \ftEntity
     */
    public function addSubEntity( $sFieldName, $mSubEntity , $sConnectionType='', $sAltTitle='' ) {
    
        // описание подчиненной сущности
        $oModel = null;
        
        // если текстовое имя 
        if ( is_string($mSubEntity) ) {
            $oModel = ft::getModel($mSubEntity);
        }

        // если передан объект типа "Редактор сущностей"
        elseif ( is_object($mSubEntity) and is_a($mSubEntity, 'ftEntity') ){
            $oModel = $mSubEntity->getModel();
        }

        else {
            ftFnc::error( new ftModelException(sprintf(
                'Переданы неверные данные при определении `%s`.`%s`',
                $this->getEntityName(),
                $sFieldName
            )));
            return $this;
        }
        
        // выйти, если нет данных
        if ( !$oModel ) {
            $this->unselect();
            return $this;
        }

        // генерация поля - подчиненной сущности
        $oField = new ftModelFieldSub( $sFieldName, $oModel, $sConnectionType, $sAltTitle );

        // добавление поля в список
        $this->oModel->addFieldObject($sFieldName,$oField);

        // выбрать добавленное поле
        $this->selectField( $sFieldName );

        return $this;
        
    }

    /**
     * Добавляет запись о родительской сущности
     * @param string $sEntityName
     * @return \ftEntity
     */
    public function setParentEntity( $sEntityName ) {
        $this->oModel->setParentEntity( $sEntityName );
        return $this;
    }

    /**
     * Добавляет запись об имени родительского поля
     * @param string $sFieldName
     * @return \ftEntity
     */
    public function setParentField( $sFieldName ) {
        $this->oModel->setParentField( $sFieldName );
        return $this;
    }

    /**
     * Добавляет запись о типе связи
     * @param string $sType
     * @return \ftEntity
     */
    public function setConnectionType( $sType ) {
        $this->oModel->setConnectionType( $sType );
        return $this;
    }

    /**
     * Устанавливает параметр Required для полей в базе
     * @param $value
     * @param mixed $mFields
     * @return \ftEntity
     */
    public function setRequired( $value, $mFields=false ) {
        
        // набор полей
        $aFieldList = $mFields ? $this->oModel->makeFieldsSet( $mFields ) : $this->aSelectedFields;
        
        // установка параметров для полей
        foreach ( $aFieldList as $sFieldName ) {
            $oField = $this->oModel->getFiled($sFieldName);
            if ( $oField )
                $oField->setRequired( $value );
        }
        
        return $this;
        
    }
    
    /**
     * Сохраненяет сущность в кэше
     * @return \ftEntity
     */
    public function save() {

        // сохранение сущности в кэше
        ftCache::set( $this->getEntityName(), $this->oModel->getModelArray() );

        // если таблица мультиязычная
        if ( $this->oModel->isMultilang() ) {
            // сохранить и языковую
            ftLang::getLangEntity($this->oModel)->save();
        }

        // если есть динамические внешние поля
        if ( $this->hasDinamicExternalFields ) {
            // выполнить перестроение
            ftDBTable::repairTable( $this->getModel() );
        }

        return $this;
        
    }

    
    /**
     * Создает/модифицирует таблицу в БД
     * @return \ftEntity
     */
    public function build() {
        ftDBTable::repairTable( $this->oModel );
        return $this;
    }

    /**
     * Записать/выбрать параметр
     * @param $sParamName
     * @param null $mValue
     * @return \ftEntity|null|string
     */
    public function parameter( $sParamName, $mValue=null ) {
        
        // 2 параметра - сохранение
        if ( func_num_args() == 2 ){
            
            // перебрать все выбранные поля 
            foreach ( $this->aSelectedFields as $sFieldName ) {
                
                // взять поле
                $oField = $this->oModel->getFiled($sFieldName);
                if ( !$oField ) continue;

                // добавить параметр
                $oField->setParameter($sParamName, $mValue);

            }
    
            return $this;
            
        }
        
        // иначе 1 параметр - выборка
        else {

            // если есть выбранне поля
            if ( count($this->aSelectedFields) ){
            
                // взять первое поле
                $oField = $this->oModel->getFiled($this->aSelectedFields[0]);
                if ( !$oField ) return null;

                // вернуть значение параметра
                return $oField->getParameter( $sParamName );
                
            }
            
            // если нет выбранных полей
            else return null;
            
        }

    }

    /**
     * Добавление набора процессоров набору полей
     * @param $sProcType
     * @param $mProcList
     * @return bool
     */
    protected function addProcessor( $sProcType ,$mProcList ){
        
        // выбранные поля
        $aFieldList = $this->aSelectedFields;
        
        // набор процессоров
        $aProcList = ftFnc::toArray( $mProcList );
        
        // установка параметров для полей
        foreach ( $aFieldList as $sFieldName ) {
            
            // взять поле
            $oField = $this->oModel->getFiled($sFieldName);
            if ( !$oField ) continue;

            // добавдение процессоров
            foreach ( $aProcList as $new_processor )
                call_user_func(array($oField,'add'.ucfirst($sProcType)), $new_processor);

        }
        
        return true;
    
    }

    /**
     * Удаление процессоров полей
     * @param $sProcType
     * @param $mProcList
     * @return bool
     */
    protected function delProcessor( $sProcType ,$mProcList ){
        
        $aFieldList = $this->aSelectedFields;
        
        $aProcList = ftFnc::toArray( $mProcList );
        
        // установка параметров для полей
        foreach ( $aFieldList as $sFieldName ) {
            
            // взять поле
            $oField = $this->oModel->getFiled($sFieldName);
            if ( !$oField ) continue;

            // добавдение процессоров
            foreach ( $aProcList as $sPName )
                call_user_func(array($oField,'del'.ucfirst($sProcType), $sPName));

        }
        
        return true;
    
    }

    /**
     * добавление модификатора
     * @param $processor_name
     * @return \ftEntity
     */
    public function addModificator( $processor_name ){
        $this->addProcessor( 'modificator', $processor_name );
        return $this;
    }

    /**
     * добавление виджета
     * @param $processor_name
     * @return \ftEntity
     */
    public function addWidget( $processor_name ){
        $this->addProcessor( 'widget', $processor_name );
        return $this;
    }

    /**
     * добавление набора стандартных процессоров полей
     * @return \ftEntity
     */
    public function addDefaultProcessorSet(){
        $this
            ->addDefaultEditors()
            ->addDefaultValidatorSet()
            ->addDefaultModificatorSet()
            ->addDefaultWidgetSet()
            ->unselect()
        ;
        return $this;
    }

    /**
     * Добавляет стандартный набор редакторов
     * @return \ftEntity
     */
    public function addDefaultEditors() {

        // перебрать все поля
        foreach ( $this->oModel->getFileds() as $oField ) {

            // с устанавленным редактором не трогаем
            if ( $oField->getEditorName() )
                continue;

            // перебираем по типу данных
            switch ( $oField->getDatatype() ) {
                default:
                case 'varchar':
                    $oField->setEditorName('string');
                    break;
                case 'text':
                    $oField->setEditorName('text');
                    break;
                case 'text':
                    $oField->setEditorName('text');
                    break;
                case 'int':
                    if ( $oField->getSize() === 1 ) {
                        $oField->setEditorName('check');
                    } elseif ( $oField->getName() == 'id' ) {
                        $oField->setEditorName('hide');
                    } elseif ( $oField->getName() == '_weight' ) {
                        $oField->setEditorName('weight');
                    } else {
                        $oField->setEditorName('string');
                    }
                    break;
                case 'datetime':
                case 'date':
                case 'time':
                    $oField->setEditorName('datetime');
                    break;
                case '':
                    $oField->setFictitious(true);
                    break;
            }

        }

        return $this;
    }

    /**
     * добавление набора стандартных виджетов
     * @return \ftEntity
     */
    function addDefaultWidgetSet(){
        
        $this

            // добалвнеие обработчиков текста к текстовым полям 
            ->selectFieldsByType( 'text,varchar' )
                ->addWidget('text')
            
            // обработка текстовых полей, содержащих html код
            ->selectFieldsByEditor( 'wyswyg' )
                ->delWidget('text')
                ->addWidget('html')
        
            ->selectFieldsByEditor( 'gallery' )
                ->addWidget('gallery')
        
            // обработчики даты и времени
            ->selectFieldsByType( 'date,datetime,time' )
                ->addWidget('datetime')
                ->setEditor('datetime')

            ->selectFields('_add_date,_upd_date')
                ->parameter('hide_on_add','1')
            
            // галочки
            ->selectFieldsByType( 'int(1)' )
                ->setEditor('check')
                ->addModificator( 'bool_as_int' )

        ;

        $this->unselect();
        
        return $this;
                                  
    }


    /**
     * добавление набора стандартных валидаторов
     * @return \ftEntity
     */
    function addDefaultValidatorSet(){
        
        $this->unselect();
        
        // добавление валидатора для полей с уникальным индексом
        foreach ( $this->oModel->getIndexes() as $oIndex ) {
            
            // если индекс уникальный
            if ( $oIndex->isUnique() ) {
                
                // набор полей
                $aFields = $oIndex->getFileds();
                
                // поле для отображения ошибки (может быть задано)
                $field_name = $this
                    ->selectField($aFields[0])
                    ->parameter('unique_main_field')
                ;
                // иначе берем первое поле
                if ( !$field_name or !in_array($field_name, $aFields) )
                    $field_name = $aFields[0];
                
                // добавление валидатора для поля
                $this
                    ->selectField($field_name)
                    ->addValidator('unique')
                    ->parameter('unique_fields',implode(',',$aFields))
                ;
                
            }
            
        }

        // версия данных
        $this->selectFieldsByEditor( 'data_version' )
            ->addValidator('data_version')
        ;

        $this->unselect();

        return $this;
    
    }

    /**
     * добавление набора стандартных модификаторов
     * @return \ftEntity
     */
    function addDefaultModificatorSet(){
        
        $this

            // автозаполнение поля
            ->selectField('_add_date')
                ->addModificator('add_date')
            
            // автозаполнение поля даты обновления
            ->selectField('_upd_date')
                ->addModificator('upd_date')

            // пользователь, привязанный к записи
            ->selectFieldsByEditor( 'user' )
                ->addModificator('user')
            
            // дробные числа
            ->selectFieldsByType( 'float,double' )
                ->addModificator('float')
        
            // версия данных
            ->selectFieldsByEditor( 'data_version' )
                ->addModificator('data_version')
                ->parameter( 'add_as_hidden',1 )
        ;

        $this->unselect();
        
        return $this;
    
    }


    /**
     * удаление виджетов
     * @param $mProc
     * @return \ftEntity
     */
    public function delWidget( $mProc ){
        $this->delProcessor( 'widget', $mProc );
        return $this;
    }

    /**
     * добавление валидатора
     * @param $mProc
     * @return \ftEntity
     */
    public function addValidator( $mProc ){
        $this->addProcessor( 'validator', $mProc );
        return $this;
    }


    /**
     * удаление валидатора
     * @param $mProc
     * @return \ftEntity
     */
    public function delValidator( $mProc ){
        $this->delProcessor( 'validator', $mProc );
        return $this;
    }


    /**
     * мультиязычные поля
     * @param bool $full
     * @return \ftEntity
     */
    public function multilang( $full=false ) {
        
        if ( !ftFnc::hasLanguages() )
            return $this;

        // установка параметров для полей
        foreach ( $this->aSelectedFields as $sFieldName ) {
            
            // взять поле
            $oField = $this->oModel->getFiled($sFieldName);
            if ( !$oField ) continue;

            // установка флага мультиязычности
            $oField->setMultilang(1+(int)(bool)$full);

            // установка соответствующего редактора
            $sEditor = $oField->getEditorName();
            if ( strpos($sEditor,'_lang')===false )
                $oField->setEditorName( $sEditor.'_lang' );

        }
        
        return $this;
    
    }

    /**
     * Уничтожить все записи о мультиязычночти
     * Применяется при генерации мультиязычной сущности-расширения
     * @param bool $all
     * @return \ftEntity
     */
    public function dropMultilang( $all=false ) {
        if ( $all === true ) {
            foreach ( $this->oModel->getFileds() as $oField )
                $oField->setMultilang(0);
        }
        return $this;
    }


    /**
     * Добавляет индекс для выбранных полей
     * @param string $sType
     * @param string $sIndexName
     * @throws ftModelException
     * @return \ftEntity
     */
    public function addIndex( $sType='index', $sIndexName='' ) {
        
        // выбранные поля
        $aFields = $this->aSelectedFields;
        
        try {
        
            if ( !count($aFields) )
                throw new ftModelException('Не мегу задать индекс для сущности `'.$this->getEntityName().'`- не выбрано ни одного поля');
            
            // добавить индекс
            $this->oModel->addIndexByAlias( $sIndexName, $aFields, $sType );

        } catch ( ftException $e ) {
            ftFnc::error( $e );
        }
        
        return $this;
        
    }

    /**
     * удалить таблицу
     * @param bool $lang_only
     * @return bool
     */
    public function deleteTable( $lang_only=false ) {
        return ftDBTable::deleteTable( $this->oModel, $lang_only );
    }

    /**
     * переименовать таблицу
     * @param $sNewName
     * @return bool
     */
    public function renameTable( $sNewName ) {
        return ftDBTable::renameTable( $this->oModel, $sNewName );
    }

}
