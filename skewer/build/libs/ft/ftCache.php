<?php
/**
 * Класс для работы с кэшем ft
 * ver 2.00
 */

class ftCache {

    /** имя контейнера кэша модедей */
    const contName = 'ft_cache';

    /**
     * Проверяет существует ли сущность
     * @static
     * @param string $sEntityName имя сущности
     * @return bool
     */
    static public function exists( $sEntityName ) {
        return (isset($_SESSION[self::contName]) and isset($_SESSION[self::contName][(string)$sEntityName]));
    }

    /**
     * Отдает объект-описание сущности
     * @static
     * @param string $sEntityName имя сущности
     * @return ftModel|null
     */
    static public function get( $sEntityName ) {
        try {
            $sEntityName = (string)$sEntityName;
            if ( !self::exists($sEntityName) )
                return null;
            return new ftModel( $_SESSION[self::contName][$sEntityName] );
        } catch ( ftException $e ) {
            ftFnc::error($e);
            return null;
        }
    }

    /**
     * Записать в кэш описание модели
     * @static
     * @param string $sEntityName
     * @param array $aModel
     */
    static public function set( $sEntityName, $aModel ) {
        $_SESSION[self::contName][$sEntityName] = $aModel;
    }

}
