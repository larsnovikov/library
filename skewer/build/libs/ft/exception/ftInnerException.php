<?php
/**
 * Исключение системного ft уровня
 * Если возникли проблемы в выполненнии внутри ft
 */

class ftInnerException extends ftException {

    /**
     * Отдает набор путей для исключения из трассировки
     * @return array
     */
    protected function getSkipList() {
        return array();
    }

}
