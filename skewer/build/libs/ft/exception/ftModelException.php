<?php
/**
 *
 *
 * @class: ftModelException
 *
 * @Author: User, $Author: sapozhkov $
 * @version: $Revision: 6 $
 * @date: $Date: 2012-05-18 10:52:53 +0400 (Пт., 18 мая 2012) $
 *
 */

class ftModelException extends ftException {

    /**
     * Отдает набор путей для исключения из трассировки
     * @return array
     */
    protected function getSkipList() {
        return array(
            COREPATH,
            BUILDPATH.'libs/ft/',
            BUILDPATH.'classes/ft.php'
        );
    }


}
