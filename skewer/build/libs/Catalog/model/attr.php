<?php
/**
 * Наборы атрибутов для полей каталога
 */

ft::entity(Catalog::tablePrefix.'attr')
    ->clear()

    ->setTablePrefix('')

    ->addSubEntity( 'field_id', Catalog::tablePrefix.'field' )
    ->addSubEntity( 'tpl_id', Catalog::tablePrefix.'attr_tpl' )
    ->addField('value','varchar(255)','Значение')

    ->addDefaultProcessorSet()

    ->save()
    //->build()
;
