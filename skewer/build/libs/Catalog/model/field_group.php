<?php
/**
 * Модель групп полей
 */
ft::entity(Catalog::tablePrefix.'field_group')
    ->clear()

    ->setTablePrefix('')

    ->addField('title','varchar(255)','Название атрибута')
        ->addValidator('set')

    ->addField('position','int','Положение')
        ->setEditor('position')

    ->addSubEntity('entity_id', Catalog::tablePrefix.'entity', '--')

    ->addDefaultProcessorSet()

    ->save()
    //->build()
;
