<?php
/**
 * Формирование модели поля для каталога
 */

ft::entity(Catalog::tablePrefix.'field')
    ->clear()

    ->setTablePrefix('')

    ->addField('entity_id','int','id сущности')
        ->setEditor('hide')

    ->addField('name','varchar(64)','Системное имя')
        ->addValidator('system_name')
    ->addField('title','varchar(255)','Название')

    ->addField('type','varchar(32)','Тип данных')
    ->addField('size','int','Размер в базе')

    ->addField('editor','varchar(32)','Элемент управления')

    ->addField('widget','varchar(255)','Визуализатор')
    ->addField('modificator','varchar(255)','Преобразователь')
    ->addField('validator','varchar(255)','Валидатор')

    ->addField('group', 'int', 'Группа')
    ->addField('position','int','Положение')
        ->setEditor('position')

    // обязательные поля
    ->selectFields('name,title')
        ->addValidator('set')

    ->addDefaultProcessorSet()

    ->save()
    //->build()
;
