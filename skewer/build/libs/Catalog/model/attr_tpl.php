<?php
/**
 * Модель шаблонов атрибутов полей
 */

ft::entity(Catalog::tablePrefix.'attr_tpl')
    ->clear()

    ->setTablePrefix('')

    ->addField('section_filter','varchar(255)','Фильтр по разделам')
    ->addField('card_filter','varchar(255)','Фильтр по карточке')
    ->addField('name_filter','varchar(255)','Фильтр по имени')
    ->addField('title','varchar(255)','Название атрибута')
        ->addValidator('set')
    ->addField('default','varchar(255)','Значение по умолчанию')

    ->addDefaultProcessorSet()

    ->save()
    //->build()
;
