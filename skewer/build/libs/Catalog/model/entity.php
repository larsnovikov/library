<?php
/**
 * Формирование модели для каталога
 */

ft::entity(Catalog::tablePrefix.'entity')
    ->clear()

    ->setTablePrefix('')

    ->addField('name','varchar(64)','Системное имя')
        ->addValidator('system_name')
    ->addField('title','varchar(255)','Название')

    ->addField('simple','int(1)','Простой справочник')
    ->addField('in_base','int(1)','Сохранить в базе')

    ->addField('desc','text','Описание')
    ->addField('cache','text','Кэш')
        ->setEditor('hide')

    ->addField('parent', 'int', 'Родительская сущность')
    ->addField('module','varchar(64)','Модуль')

    // обязательные поля
    ->selectFields('name,title,module')
        ->addValidator('set')

    ->selectField('name')
        ->addIndex('unique')

    ->addColumnSet('editor','name,title')

    ->addDefaultProcessorSet()

    ->save()
    //->build()
;
