<?php
/**
 * Каталог
 * Класс для работы со списками сущностей
 */

class cEntities {

    /**
     * Отддает имя модуля для сласса
     * @static
     * @return string
     */
    protected static function moduleName() {
        return 'catalog';
    }

    /**
     * Отдает имя таблицы для хранения набора сущностей
     * @static
     * @return string
     */
    public static function tableName() {
        return 'c_entity';
    }

    /**
     * Отдает объект для работы с сущностью
     * @static
     * @param int $iId id сущности
     * @return cEntity|null
     */
    public static function getById( $iId ) {
        // todo написать
    }

    /**
     * Отдает объект для работы с сущностью
     * @static
     * @param string $sName имя сущности
     * @return cEntity|null
     */
    public static function getByName( $sName ) {
        // todo написать
    }

    /**
     * Отдает набор сущностей в виде массива
     * @static
     * @return array
     */
    public static function getList(){
        // todo написать
    }

    /**
     * Отдает набор базовых сущностей в виде массива
     * @static
     * @return array
     */
    public static function getBaseList() {
        // todo написать
        // работает на основе getList с заданными фильтрами
    }

    /**
     * Отдает набор сущностей-справочников (с установленным флагом simple) в виде массива
     * @static
     * @return array
     */
    public static function getSimpleList() {
        // todo написать
        // работает на основе getList с заданными фильтрами
    }

    /**
     * Удаляет сущность по id
     * @static
     * @param int $iId
     * @return array
     */
    public static function delete( $iId ) {
        // todo написать
    }

}
