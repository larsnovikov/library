<?php
/**
 * Класс для автоматической сборки админских интерфейсов на ExtJS
 * Класс для построения форм
 *
 * @class: ExtForm
 * @project Skewer
 * @package Build
 *
 * @Author: sapozhkov
 * @version: $Revision: 1466 $
 * @date: $Date: 2012-12-20 19:13:34 +0400 (Чт., 20 дек. 2012) $
 *
 */

class ExtForm extends ExtModelPrototype {

    /** @var bool флаг использования спец директории для изображений модуля */
    protected $bUseSpecSectionForImages = false;

    /** @var int id спец директории для загружаемых изображений */
    protected $iSpecSectionForImages = 0;

    /**
     * Устанавливает флаг использования спец директории для изображений модуля
     */
    public function useSpecSectionForImages( $iId=0 ) {
        $this->bUseSpecSectionForImages = true;
        $this->iSpecSectionForImages = (int)$iId;
    }

    /**
     * Общие Функции
     */

    /**
     * Возвращает имя компонента
     * @return string
     */
    public function getComponentName() {
        return 'Form';
    }

    /**
     * Запрос дополнительных полей для инициализации полей по ft модели
     * @param ftModelField $oField
     * @return array
     */
    protected function getAddParamsForFtField( ftModelField $oField ) {
        return array(
            'disabled' => false,
        );
    }

    /**
     * Добавляет к текущей модели запись
     * @param \ExtFieldPrototype $oItem новая запись для модели
     * @return bool
     */
    public function addField( ExtFieldPrototype $oItem ) {

        // проверка корректности описания
        if ( !$oItem->getName() or !$oItem->hasDescVal('view') ) {
            $this->error( 'Model create. Wrong input.', $oItem->getDesc() );
            return false;
        }

        return parent::addField( $oItem );

    }

    /**
     * Преобразует объект поля в пригодный для ExtJS массив
     * @param ExtFieldPrototype $oItem
     * todo bug повторный вызов ломает выходной массив
     * @return array
     */
    public static function getFieldDesc( ExtFieldPrototype $oItem ) {

        // замена инициализационного массива перекрытым для обхекта
        $oItem->setAddDesc( $oItem->getFormFieldDesc() );

        // типы для хранилища
        switch ( $oItem->getView() ) {
            default:
            case 'string':
                $oItem->setDescVal('type','str');
                break;
            case 'int':
                $oItem->setDescVal('type','num');
                break;
            case 'check':
            case 'file':
            case 'hide':
            case 'text':
            case 'wyswyg':
            case 'html':
            case 'float':
            case 'boolean':
            case 'select':
            case 'date':
            case 'time':
            case 'show':
            case 'pass':
            case 'specific':
                $oItem->setDescVal('type',$oItem->getView());
                break;
        }

        // убрать ненужное поле
        $oItem->delDescVal('view');

        // значение поля - обязательное ( может быть false / 0 / null / ... )
        if ( !$oItem->hasValue() )
            $oItem->setValue('');

        // название - обязательное
        if ( !$oItem->getTitle() )
            $oItem->setTitle( $oItem->getName() );

        return $oItem->getDesc();

    }

    /**
     * Устанавливает значения для набор элементов
     * @param $aValues - набор пар имя поля - значение
     */
    public function setValues($aValues) {

        // обойти весь пришедший массив
        foreach ( $aValues as $sFieldName => $mValue ) {

            // для найденных элементов установить значения
            if ( $this->hasField($sFieldName) )
                $this->aFields[$sFieldName]->setValue($mValue);

        }

    }

    /**
     * Устанавливает значения по умолчанию
     */
    public function setDefaultValues(){

        foreach ( $this->getFields() as $oItem ) {
            $oItem->setValue( $oItem->getDefaultVal() );
        }

    }

    /**
     * Протокол Передачи Данных
     */

    /**
     * Собирает интерфейсный массив для выдачи в JS
     * @return array
     */
    public function getInterfaceArray() {

        // собираем массив описаний
        $aItems = array();
        foreach ( $this->getFields() as $oItem )
            $aItems[] = $this->getFieldDesc( $oItem );

        // выходной массив
        $aOut = array(
            'items' => $aItems
        );

        // вывод данных
        return $aOut;

    }

    /**
     * Возввращает массив инициализации специфического поля
     * По сути расширяет массив $aInitParams дополнительными параметрам, которые будут
     *      приняты js кодом и обработаны
     * @param string $sLibName - имя спец класса
     * @param array $aInitParams - параметры для передачи
     * @return array
     */
    public function getSpecificItemInitArray( $sLibName, $aInitParams=array() ){

        // добавить инициализацию библиотеки
        $this->addLibClass( $sLibName );

        // метка спец обработчика
        $aInitParams['view'] = 'specific';

        // имя для библиотеки наследования
        $aInitParams['extendLibName'] = $sLibName;

        return $aInitParams;

    }

    /**
     * Возвращает инициализационный массив для создания элемента типа select в ExtJS
     * Пример
     *  $aData =
     *      0
     *          value = 12
     *          title = декабрь
     *      1
     *          value = 2
     *          title = февраль
     *  $sValName = value
     *  $sTitleName = title
     * @static
     * @param array $aData - набор данных
     * @param string $sValName - имя переменной для сохранения из массива данных
     * @param string $sTitleName - имя переменной для отображения из массива данных
     * @return array
     */
    public static function getDesc4Select( $aData, $sValName, $sTitleName ){

        return array(
            'view' => 'select',
            'valueField' => $sValName,
            'displayField' => $sTitleName,
            'store' => array(
                'fields' => array( $sValName, $sTitleName ),
                'data' => array_values($aData)
            )
        );

    }

    /**
     * Возвращает инициализационный массив для создания элемента типа select в ExtJS
     * На вход принимается одномерный массив пар значение => заголовок
     * @static
     * @param $aData
     * @return array
     */
    public static function getDesc4SelectFromArray( $aData ){

        // сборка массива данных
        $aItems = array();
        foreach ( $aData as $sKey => $mItem ) {
            $aItems[] = array(
                'v' => $sKey,
                't' => $mItem
            );
        }

        // отдать массив с данными
        return static::getDesc4Select( $aItems, 'v', 't' );

    }

    /**
     * Отдает описание для создания прописанного в js библиотеке поля
     * @static
     * @param string $sLibName
     * @param array $aAddData
     * @return array
     */
    public static function getDesc4CustomField( $sLibName, $aAddData=array() ) {

        return array_merge(array(
            'customField' => $sLibName
        ),$aAddData);

    }

    /**
     * Задает инициализационный  массив для атопостроителя интерфейсов
     * @param \AdminModulePrototype $oModule - ссылка на вызвавший объект
     */
    public function setInterfaceData( AdminModulePrototype $oModule ) {

        // если есть спец флаг
        if ( $this->bUseSpecSectionForImages ) {
            // увязывание файлов для wyswyg в спец директорию
            foreach ( $this->aFields as $oField ) {
                if ( $oField->getView() === 'wyswyg' ) {
                    $aAddConfig = $oField->getDescVal( 'addConfig', array() );
                    $aAddConfig['filebrowserBrowseUrl'] = $this->getFileBrowserUrl( $oModule );
                    $oField->setDescVal( 'addConfig', $aAddConfig );
                }
            }
        }

        // выполняем родительскую часть модуля
        parent::setInterfaceData( $oModule );

    }

    /**
     * Отдает ссылку на
     * @param AdminModulePrototype $oModule
     * @return string
     */
    protected function getFileBrowserUrl( AdminModulePrototype $oModule ) {
        $sPattern = '/admin/?mode=fileBrowser&%s=%s&type=file&returnTo=ckeditor';
        if ( $this->iSpecSectionForImages )
            return sprintf( $sPattern, 'section', $this->iSpecSectionForImages );
        else
            return sprintf( $sPattern, 'module', get_class( $oModule ) );
    }


}
