<?php
/**
 * Класс прототип для модулей автопостроителя с определением модели
 */
abstract class ExtModelPrototype extends ExtPrototype {

    /** @var ExtFieldPrototype[] Модель данных */
    protected $aFields = array();

    /**
     * Очищает текущую модель данных
     */
    public function clearFields() {
        $this->aFields = array();
    }

    /**
     * Возвращает текущую модель данных
     * @param bool $bWithNames - с ключами - именами полей
     * @return ExtFieldPrototype[]
     */
    public function getFields( $bWithNames=false ) {
        return $bWithNames ? $this->aFields : array_values($this->aFields);
    }

    /**
     * Проверяет наличие поля в модели
     * @param $sName
     * @return bool
     */
    protected function hasField( $sName ) {
        return isset( $this->aFields[ $sName ] );
    }

    /**
     * Отдает объект поля из модели или null, если нету
     * @param $sName
     * @return ExtFieldPrototype|null
     */
    protected function getField( $sName ) {
        if ( $this->hasField( $sName ) )
            return $this->aFields[ $sName ];
        else
            return null;
    }

    /**
     * Добавляет к текущей модели запись
     * @param \ExtFieldPrototype $oItem новая запись для модели
     * @return bool
     */
    public function addField( ExtFieldPrototype $oItem ) {

        // добавление в массив модели
        $this->aFields[ $oItem->getName() ] = $oItem;

        return true;

    }

    /**
     * Устанавливает модель данных для списка
     * @param $aItems - новое описание модели
     */
    public function setFields( $aItems ) {

        // очищаем текеущую модель
        $this->clearFields();

        // перебираем пришедшие данные
        foreach ( $aItems as $aItemRow ) {

            // добавить поле
            $this->addField( Ext::makeFieldObject( $aItemRow ) );

            // если есть кастомные поля
            if ( is_array($aItemRow) ) {  /** @todo сделать обработку и для объектов */
                if ( isset($aItemRow['customField']) )
                    $this->addLibClass( $aItemRow['customField'] );
            }

        }

    }

    /**
     * Задает набор полей по FT модели
     * @param \ftModel $oModel описание модели
     * @param string $sColSet набор колонок для вывода
     * @return void
     */
    public function setFieldsByFtModel( ftModel $oModel, $sColSet='editor' ) {

        $aOutModel = array();

        // набор колонок для отображения
        $aFields = $oModel->getColumnSet($sColSet);

        // префикс параметров
        $sPrefix = $this->getParamPrefix();

        /** @var $sPCont имя оконтейнера для параметров */
        $sPCont = $this->getParamCont();

        // переобразуем объект в массив полей
        foreach ( $oModel->getFileds() as $oField ) {

            // если задан набор проверить наличие поля в нем
            if ( $aFields and !in_array($oField->getName(),$aFields) )
                continue;

            $aParams = array_merge( array(
                'name' => $oField->getName(),
                'title' => $oField->getTitle(),
                'view' => $oField->getEditorName() ? $oField->getEditorName() : $oField->getDatatype(),
            ), $this->getAddParamsForFtField( $oField ) );

            // добавление параметров по шаблону
            foreach ($oField->getParameterList() as $sParamName => $mParamVal ) {
                if ( strpos($sParamName,$sPrefix) === 0 ) {
                    if ( $sPCont )
                        $aParams[$sPCont][ substr($sParamName,strlen($sPrefix)) ] = $mParamVal;
                    else
                        $aParams[ substr($sParamName,strlen($sPrefix)) ] = $mParamVal;
                }
            }

            $aOutModel[$oField->getName()] = $aParams;

        }

        $this->setFields( $aOutModel );

    }

    /**
     * Отдает имя контейнера для параметров
     * @return string
     */
    protected function getParamCont() {
        return '';
    }

    /**
     * Отдает префикс параметра для сборки модели
     * @return string
     */
    protected function getParamPrefix() {
        $sVal = $this->getComponentName();
        return $sVal ? strtolower($sVal).'_' : '';
    }

    /**
     * Запрос дополнительных полей для инициализации полей по ft модели
     * @param ftModelField $oField
     * @return array
     */
    protected function getAddParamsForFtField( /** @noinspection PhpUnusedParameterInspection */ftModelField $oField ) {
        return array();
    }

}
