<?php
/**
 *  Класс для автоматической сборки админских интерфейсов на ExtJS
 *
 * @class: ExtList
 * @project Skewer
 * @package Build
 *
 * @Author: User, $Author: acat $
 * @version: $Revision: 1173 $
 * @date: $Date: 2012-11-12 16:37:54 +0400 (Пн., 12 нояб. 2012) $
 *
 */

class ExtList extends ExtModelPrototype {

    /* @var string имя состояния загрузки данных */
    protected $actionNameLoad = 'init';

    /* @var string команда для интерфейса */
    protected $sCmd = '';

    /**
     * Общие Функции
     */

    /**
     * Возвращает имя компонента
     * @return string
     */
    public function getComponentName() {
        return 'List';
    }

    /**
     * Отдает модель для хранилища
     * @return array
     */
    protected function getModelForStore() {
        $aOut = array();
        foreach ( $this->getFields() as $oItem ) {
            $aOut[] = array(
                'name' => $oItem->getName(),
                'type' => $oItem->getDescVal('storeType')
            );
        }
        return $aOut;
    }

    /**
     * Отдает модель для набора колонок
     * @return array
     */
    protected function getModelForColumns() {

        // выходной массив
        $aOut = array();

        // перебрать все строки модели
        foreach ( $this->getFields() as $oItem ) {

            // собрать строку
            $aOutRow = array_merge( array(
                'text' => $oItem->getTitle(),
                'jsView' => $oItem->getView(),
                'dataIndex' => $oItem->getName()
            ), $oItem->getListFieldDesc());

            if ( $oItem->hasDescVal('columnType') ) {
                $aOutRow['xtype'] = $oItem->getDescVal('columnType');
                $aOutRow['editor'] = $oItem->getDescVal('editor');
            }

            // сортировка разрешена, есть прямое разрешение или нет постраничного
            $aOutRow['sortable'] = $this->sortingIsAllowed() || !$this->getOnPage();

            // меню колонок отключено
            $aOutRow['menuDisabled'] = true;

            // расширение массива, если есть специальный контейнер
            if ( $oItem->hasDescVal('listColumns') )
                $aOutRow = array_merge( $aOutRow, $oItem->getDescVal('listColumns') );

            // добавить в вывод
            $aOut[] = $aOutRow;

        }

        return $aOut;
    }

    /**
     * Преобразует объект поля в пригодный для ExtJS массив
     * @param ExtFieldPrototype $oItem
     * @return array
     */
    protected function getFieldDesc( ExtFieldPrototype $oItem ) {

        // замена инициализационного массива перекрытым для обхекта
        $oItem->setDesc( $oItem->getListFieldDesc() );

        // типы для хранилища
        switch ( $oItem->getView() ) {
            default:
                $oItem->setDescVal('storeType','auto');
                break;
            case 'str':
            case 'text':
            case 'html':
            case 'auto':
            case 'string':
            case 'int':
            case 'float':
            case 'boolean':
            case 'date':
                $oItem->setDescVal('storeType','string');
                break;
            case 'hide':
                $oItem->setDescVal('storeType','hidden');
                break;
//            /** todo доделать chechbox для автопостроителя форм
//             * нужен css файл с картинками и обработчик нажатия
//             */
//            case 'check':
//                $aModelRow['storeType'] = 'bool';
//                $aModelRow['columnType'] = 'checkcolumn';
//                $aModelRow['editor'] = array('xtype'=>'checkbox','cls'=>'x-grid-checkheader-editor');
//                break;
        }

        // название - обязательное
        if ( !$oItem->getTitle() )
            $oItem->setTitle( $oItem->getName() );

        return $oItem->getDesc();

    }

    /**
     * Добавляет к текущей модели запись
     * @param \ExtFieldPrototype $oItem новая запись для модели
     * @return bool
     */
    public function addField( ExtFieldPrototype $oItem ) {

        // проверка корректности описания
        if ( !$oItem->getName() or !$oItem->hasDescVal('view') ) {
            $this->error( 'Model create. Wrong input.', $oItem->getDesc() );
            return false;
        }

        return parent::addField( $oItem );

    }

    /**
     * Запрос дополнительных полей для инициализации полей по ft модели
     * @param ftModelField $oField
     * @return array
     */
    protected function getAddParamsForFtField( ftModelField $oField ) {
        return array(
            'type' => ExtFT::getLetterType( $oField ),
            'view' => $this->getView( $oField ),
        );
    }


    /**
     * Отдает тип отображения
     * @param ftModelField $oField
     * @return string
     */
    protected function getView( ftModelField $oField ) {

        if ( $oField->getEditorName() == 'hide' )
            return 'hide';

        switch ( $oField->getDatatype() ) {
            case 'varchar':
            default:
                return 'str';
        }

    }

    /**
     * Задает набор полей, которые можно редактировать в списке,
     * а также имя состояния для сохранения
     * Вызывается после добавления полей модели в набор
     * @param string[] $aFieldList набор полей
     * @param string $sCmd имя состояния
     */
    public function setEditableFields( $aFieldList, $sCmd='save' ) {

        // добавить поля в список
        foreach ( $aFieldList as $sFieldName ) {
            if ( $this->hasField($sFieldName) ) {
                $this->getField( $sFieldName )->setAddListDesc( array(
                    'listSaveCmd' => $sCmd
                ) );
            }
        }

    }

    /**
     * Работа С Данными
     */

    /**
     * @var array - набор данных для выдачи
     */
    protected $aValues = array();

    /**
     * Задает набор данных для выдачи
     * @param $aValueList - данные
     * @return array
     */
    public function setValues( $aValueList ) {

        // контейнер для сборки финального массива
        $aOutList = array();

        if ( !$aValueList )
            return $aOutList;

        // перебираем пришедщие строки
        foreach ( $aValueList as $aValue ) {

            // контейнер для поля
            $aOutVal = array();

            // перебираем значения, а не модель поскольку они могут дополнительно применяться
            foreach ( $aValue as $sName => $mVal ) {

                // запросить объект поля
                $oField = $this->getField($sName);

                // если поле
                if ( $oField ) {

                    // пропустить через него значение
                    $oField->setValue( $mVal );
                    $aOutVal[$sName] = $oField->getText();

                } else {

                    // нет - просто присвоить
                    $aOutVal[$sName] = $mVal;

                }

            }

            // занести полученный результат в выходной массив
            if ( $aOutVal )
                $aOutList[] = $aOutVal;

        }

        return $this->aValues = $aOutList;

    }

    /**
     * Отдает набор данных
     * @return array
     */
    public function getValues() {
        return $this->aValues;
    }

    /**
     * Массив для сортировки
     * @var array
     */
    protected $aSorters = array();

    /**
     * Сбрасывает массив для первичной сортировки
     */
    public function clearSorters() {
        $this->aSorters = array();
    }

    /**
     * Добавить поле сортировки
     * Первичная сортировка при отображении
     * @param $sFieldName
     * @param string $sDirection
     */
    public function addSorter( $sFieldName, $sDirection='ASC' ) {
        $this->aSorters[] = array(
            'property' => $sFieldName,
            'direction' => $sDirection
        );
    }

    /**
     * Возвращает массив полей для сортировки
     * @return array
     */
    public function getSorters() {
        return $this->aSorters;
    }

    /** @var bool Флаг допустимости сортировки по полям */
    protected $aAllowSorting = false;

//    /**
//     * Устанавливает разрешение для сортировки
//     * @param bool $bVal
//     */
//    public function setAllowSorting( $bVal=true ) {
//        $this->aAllowSorting = (bool)$bVal;
//    }

    /**
     * Отдает флаг разрешения сортировки по полям
     * @return bool
     */
    public function sortingIsAllowed() {
        return $this->aAllowSorting;
    }

    /**
     * Поле для группировки
     * @var string
     */
    protected $sGroupField = '';

    /**
     * Возвращает поле для группировки
     * @return string
     */
    public function getGroupField(){
        return $this->sGroupField;
    }

    /**
     * Задает поле для группировки
     * @param string $sGroupField
     */
    public function setGroupField($sGroupField){
        $this->sGroupField = $sGroupField;
    }

    /**
     * Работа С Постраничным Просмотром
     */

    /**
     * @var int - Всего записей в франилище
     */
    protected $iTotalCnt = 0;

    /**
     * Устанавливает общее число записей в хранилище
     * @param $iValue - значение
     */
    public function setTotal( $iValue ) {
        $this->iTotalCnt = $iValue;
    }

    /**
     * Возвращает общее число записей в хранилище
     * @return int
     */
    public function getTotal() {
        return $this->iTotalCnt;
    }

    /**
     * @var int - число записей на страницу
     */
    protected $iOnPage = 0;

    /**
     * Устанавливает общее число записей на страницу
     * @param $iValue - значение
     */
    public function setOnPage( $iValue ) {
        $this->iOnPage = $iValue;
    }

    /**
     * Возвращает общее число записей на страницу
     * @return int
     */
    public function getOnPage() {
        return $this->iOnPage;
    }

    /**
     * устанавливает состояние, которое будет вызвано при переходе по страницам
     * по умолчанию будет вызвано состояние "init"
     * @param string $sActionName
     */
    public function setPageLoadActionName( $sActionName ) {
        $this->actionNameLoad = (string)$sActionName;
    }

    /**
     * Отдает имя состояния для загрузки страницы
     * @return string
     */
    public function getPageLoadActionName() {
        return $this->actionNameLoad;
    }

    /**
     * @var int - номер страницы
     */
    protected $iPage = 0;

    /**
     * Устанавливает номер страницы
     * Счет начинается с 0
     * @param $iValue - значение
     */
    public function setPageNum( $iValue ) {
        $this->iPage = $iValue;
    }

    /**
     * Возвращает номер страницы
     * @return int
     */
    public function getPageNum() {
        return $this->iPage;
    }



    /**
     * Работа С Кнопками В Строках
     */

    protected $aRowButtons = array();

    /**
     * Добавляет кнопку к строке
     * @param $aBtn - описание кнопки
     * @return bool
     */
    public function addRowBtn( $aBtn ) {
        $this->aRowButtons[] = $aBtn;
        return true;
    }

    /**
     * Добавляет кастомную js кнопку
     * @param $sName
     */
    public function addRowCustomBtn( $sName ) {

        $this->addLibClass($sName);
        $this->addRowBtn( array(
            'customBtnName' => $sName
        ) );

    }

    /**
     * Добавить кнопку "Редактировать"
     * @param string $sAction
     * @param string $sState
     * @return bool
     */
    public function addRowBtnUpdate( $sAction='show', $sState='edit_form' ){
        return $this->addRowBtn(array(
            'tooltip' =>'_upd',
            'iconCls' => 'icon-edit',
            'action' => $sAction,
            'state' => $sState
        ));
    }

    /**
     * Добавить кнопку "Удалить"
     * @param string $sAction
     * @param string $sState
     * @return bool
     */
    public function addRowBtnDelete( $sAction='delete', $sState='delete' ){
        return $this->addRowBtn(array(
            'tooltip' =>'_del',
            'iconCls' => 'icon-delete',
            'action' => $sAction,
            'state' => $sState
        ));
    }

    /**
     * Запрашивает набор кнопок для строк
     * @return array
     */
    public function getRowButtons() {
        return $this->aRowButtons;
    }

    /**
     * Работа с фильтрами
     */

    /**
     * @var array - набор служебных данных
     */
    protected $aFilters = array();

    /**
     * @var bool- флаг: задан ли хоть один фильтр
     */
    protected $bFilterIsSet = false;

    /**
     * Добавление фильтра - выпадающего списка
     * @param $sName - системное имя фильтра
     * @param $aValues - массив пар ключ - значение для выпадающего списка
     * @param bool $mValue - текущее значение фильтра
     * @param string $sTitle - название фильтра
     * @return bool
     */
    public function addFilterSelect( $sName, $aValues, $mValue=false, $sTitle='' ) {

        // пустой селектор добавлять смысла нет
        if ( !$aValues )
            return false;

        // имена библиотек
        $sLibName = 'ListFilterSelect';
        $sLibFullName = $this->getJSLibPrefix().$sLibName;

        // добавление библиотеки в список загрузок
        $this->addComponent($sLibName);

        // значение отключенной фильтрации
        $aData = array( array(
            'data'=>false,
            'text'=>'Все',
            'group' => $sName,
            'checked' => $mValue===false
        ));

        // сборка допустимых значений
        foreach ( $aValues as $mKey => $sVal ) {
            $aData[] = array(
                'data' => $mKey,
                'text' => $sVal,
                'group' => $sName,
                'checked' => $mKey===$mValue
            );
        }

        // установка флага заданность значений в фильтрах
        if (!$this->bFilterIsSet and $mValue!==false)
            $this->bFilterIsSet = true;

        // добавление записи в список фильтров
        $this->aFilters[] = array(
            'libName' => $sLibFullName,
            'fieldName' => $sName,
            'title' => $sTitle,
            'text' => $sTitle,
            'menu' => array(
                'data' => $sName,
                'items' => $aData
            )
        );

        return true;

    }

    /**
     * Добавление кнопки в панель фильтров
     * @param string $sAction имя метода в php
     * @param string $sTitle надпись на кнопке
     * @param string $sConfirm текст подтверждения, если требуется
     * @param array $aParams дополнительные параметры
     * @return bool
     */
    public function addFilterButton( $sAction, $sTitle, $sConfirm='', $aParams=array() ){

        // имена библиотек
        $sLibName = 'ListFilterButton';
        $sLibFullName = $this->getJSLibPrefix().$sLibName;

        // добавление библиотеки в список загрузок
        $this->addComponent($sLibName);

        // имя метода кладем в список
        $aParams['cmd'] = $sAction;

        // добавление кнопки в панель управления
        $this->aFilters[] = array(
            'libName' => $sLibFullName,
            'text' => $sTitle,
            'textConfirm' => $sConfirm,
            'addParams' => $aParams
        );

        return true;

    }

    /**
     * Добавление фильтра - текстового поля
     * @param string $sName имя фильтра
     * @param string $sValue значение по умолчанию
     * @param string $sTitle название фильтра
     * @return bool
     */
    public function addFilterText( $sName, $sValue='', $sTitle='' ) {

        // установка флага "заданность значений в фильтрах"
        if (!$this->bFilterIsSet and $sValue!=='')
            $this->bFilterIsSet = true;

        // имена библиотек
        $sLibName = 'ListFilterText';
        $sLibFullName = $this->getJSLibPrefix().$sLibName;

        // добавление библиотеки в список загрузок
        $this->addComponent($sLibName);

        // добавление кнопки в панель управления
        $this->aFilters[] = array(
            'libName' => $sLibFullName,
            'emptyText' => $sTitle,
            'fieldValue' => $sValue,
            'fieldName' => $sName
        );

        return true;

    }

    /**
     * Добавление фильтра по дате
     * @param string $sName имя фильтра
     * @param string $sValue значение
     * @param string $sTitle название
     * @param array $aParams дополнительные параметры
     * @return bool
     */
    public function addFilterDate( $sName, $sValue, $sTitle='', $aParams=array() ){

        // установка флага "заданность значений в фильтрах"
        if (!$this->bFilterIsSet and $sValue!=='')
            $this->bFilterIsSet = true;

        // имена библиотек
        $sLibName = 'ListFilterDate';
        $sLibFullName = $this->getJSLibPrefix().$sLibName;

        // добавление библиотеки в список загрузок
        $this->addComponent($sLibName);

        // добавление кнопки в панель управления
        $this->aFilters[] = array(
            'libName' => $sLibFullName,
            'title' => $sTitle,
            'fieldValue' => $sValue,
            'fieldName' => $sName
        )+$aParams;

        return true;

    }

    /**
     * Возвращает набор фильтров для состояния
     * @return array
     */
    protected function getFilters() {
        return $this->aFilters;
    }

    /**
     * Варианты Интерфейса
     */

    /**
     * Отдает только данные для страницы
     */
    public function actionLoadPage(){

        // отключить перезагрузку компонента
        $this->setDoNotReload();

        $this->sCmd = 'loadPage';

    }

    /*
     * Drag'n'Drop
     */

    /**
     * Имя состояния при сортировке
     * @var string
     */
    protected $sDDAction = '';

    /**
     * Активирует сортировку
     * @param string $sAction имя метода
     */
    public function enableDragAndDrop( $sAction ) {
        $this->setDragAndDropAction($sAction);
    }

    /**
     * Деактивирует сортировку
     */
    public function disableDragAndDrop() {
        $this->setDragAndDropAction('');
    }

    /**
     * Задает метод для сортировки
     * Если не задан - сортировка не активируется в интерфейсе
     * @param string $sAction
     */
    public function setDragAndDropAction( $sAction ) {
        $this->sDDAction = $sAction;
    }

    /**
     * Отдает статус активности сортировки
     * @return bool
     */
    protected function getDragAndDropActionName() {
        return $this->sDDAction;
    }

    /**
     * Протокол Передачи Данных
     */

    /**
     * Собирает интерфейсный массив для выдачи в JS
     * @return array
     */
    public function getInterfaceArray() {

        // выходной массив
        $aOut = array(
            'storeModel' => $this->getModelForStore(),
            'columnsModel' => $this->getModelForColumns(),
            'rowButtons' => $this->getRowButtons(),
            'pageNum' => $this->getPageNum(),
            'itemsOnPage' => $this->getOnPage(),
            'itemsTotal' => $this->getTotal(),
            'actionNameLoad' => $this->getPageLoadActionName(),
            'sorters' => $this->getSorters(),
            'groupField' => $this->getGroupField(),
            'barElements' => $this->getFilters(),
            'items' => $this->getValues(),
            'cmd' => $this->sCmd,
            'ddAction' => $this->getDragAndDropActionName()
        );

        // если не надо перегружать страницу
        if ( $this->getDoNotReload() ) {
            // удалим пустые контейнеры
            foreach ( $aOut as $sKey => $mValue ) {
                if ( empty($mValue) ){
                    unset( $aOut[$sKey] );
                }
            }
        }

        // вывод данных
        return $aOut;

    }

    /**
     * Отдает имя контейнера для параметров
     * @return string
     */
    protected function getParamCont() {
        return 'listColumns';
    }

    /**
     * Задает инициализационный  массив для атопостроителя интерфейсов
     * @param \AdminModulePrototype $oModule - ссылка на вызвавший объект
     */
    public function setInterfaceData( AdminModulePrototype $oModule ) {

        // проверка возможности просмотра постраничного
        if ( !$oModule->actionExists( $this->getPageLoadActionName() ) )
            $this->addError('Задан недоступный для вызова метод постраничного просмотра');

        parent::setInterfaceData( $oModule );

    }

}
