<?php
/**
 * Класс для работы с полями автопостроителя
 *
 * @class: ExtFieldPrototype
 * @author: sapozhkov
 */

class ExtFieldByArray extends ExtFieldPrototype {

    /**
     * Конструктор для первичной инициализации в момент создания
     * @param array $aBaseDesc
     * @param array $aAddDesc
     */
    public function __construct( array $aBaseDesc=null, array $aAddDesc=null ) {

        // если есть базовое описание
        if ( !is_null($aBaseDesc) ) {

            // инициализировать базовое описание
            $this->setBaseDesc( $aBaseDesc );

            // если есть дополнительные параметры
            if ( !is_null($aAddDesc) ) {

                // инициализировать дополнительные параметры
                $this->setAddDesc( $aAddDesc );

            }

        }

    }

}
