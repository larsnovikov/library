<?php
/**
 * Created by JetBrains PhpStorm.
 * User: User
 * Date: 01.11.12
 * Time: 17:36
 * To change this template use File | Settings | File Templates.
 */
class ExtFieldLink extends ExtFieldPrototype {

    public function __construct() {
        $this->setView('show');
    }

    /**
     * Текст для ссылки
     * и ссылка, если она еще не была задана
     * @param mixed $sText
     * @param $sHref
     * @return mixed|void
     */
    public function setLink( $sText, $sHref ) {
        parent::setValue(sprintf('<a href="%s" target="_blank">%s</a>',$sHref,$sText));
    }

}
