<?php
/**
 * Родительский класс для создания списковых элементов в автопостроителе
 *
 * @class: ExtFieldSelectPrototype
 *
 * @Author: sapozhkov
 *
 */

abstract class ExtFieldSelectPrototype extends ExtFieldPrototype {

    /**
     * Возвращает список доступных значений в формате value => title
     * @abstract
     * @return array
     */
    abstract protected function getList();

    /**
     * Отдает описание элемента для формы
     * @return array
     */
    final function getFormFieldDesc() {

        // формирование дополнительных данных
        return ExtForm::getDesc4SelectFromArray( $this->getList() );

    }

}
