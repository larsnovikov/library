<?php
/**
 * Набор общих методов для завязки ft и ext автопостроителя
 */

class ExtFT {

    /**
     * Отдает букву типа для запроса по полю
     * @static
     * @param $oField
     * @return string ( s / i )
     */
    public static function getLetterType( ftModelField $oField ){
        switch ( $oField->getDatatype() ) {
            case 'tinyint':
            case 'int': return 'i';
            default: return 's';
        }
    }

}
