<?php
/**
 * Класс валидатор для сохраняемых данных
 *
 * @class: ExtValidator
 * @author: sapozhkov
 */

class ExtValidator {

    /**
     * Работа С Набором Элементов
     */

    /** @var ExtFieldPrototype[] Набор элементов */
    protected $aFields = array();

    /**
     * Очищает набор элементов
     */
    public function clearFields(){
        $this->aFields = array();
    }

    /**
     * Проверяет наличие заданного поля
     * @param $sName
     * @return bool
     */
    public function hasField( $sName ) {
        return isset($this->aFields[$sName]);
    }

    /**
     * Возвращает текущую модель данных
     * @param bool $bWithNames - с ключами - именами полей
     * @return ExtFieldPrototype[]
     */
    public function getFields( $bWithNames=false ) {
        return $bWithNames ? $this->aFields : array_values($this->aFields);
    }

    /**
     * Вывод ошибки
     * @param string $sText
     * @param array $mData
     * @return bool
     */
    protected function error( $sText, $mData=array() ) {
        echo 'Error in ExtValidator: '.$sText;
        if ( $mData ) var_dump($mData);
        return true;
    }

    /**
     * Добавляет к текущей модели запись
     * @param ExtFieldPrototype $oItem новая запись для модели
     * @return bool
     */
    public function addField( ExtFieldPrototype $oItem ) {

        // проверка корректности описания
        if ( !$oItem->getName() or !$oItem->hasDescVal('view') ) {
            $this->error( 'Model create. Wrong input.', $oItem->getDesc() );
            return false;
        }

        // добавление в массив модели
        $this->aFields[ $oItem->getName() ] = $oItem;

        return true;

    }

    /** @var array набор найденных ошибок */
    protected $aMessages = array();

    /**
     * Отдает набор сохраненных сообщений
     * @return array
     */
    public function getMessages(){
        return $this->aMessages;
    }

    /**
     * Валидация заданных значений
     * @return bool
     */
    public function validate() {

        $aMessages = array();
        $bValid = true;

        // перебрать все поля модели
        foreach ( $this->getFields() as $oItem ) {

            // если поле не валидно
            if ( !$oItem->isValid() ) {

                // добавить сообщение в очередь
                $aMessages[] = $oItem->getInvalidText();

                // установить флаг ошибки валидации
                $bValid = false;

            }

        }

        // сохранение ошибок
        $this->aMessages = $aMessages;

        return $bValid;
    }

    /**
     * Устанавливает модель данных для списка
     * @param $aItems - новое описание модели
     */
    public function setFields( $aItems ) {

        // очищаем текеущую модель
        $this->clearFields();

        // перебираем пришедшие данные
        foreach ( $aItems as $aItemRow ) {

            // добавить поле
            $this->addField( Ext::makeFieldObject( $aItemRow ) );

        }

    }

    /**
     * Нетронутый набор сохраняемых данных
     * @var array
     */
    protected $aValues = array();

    /**
     * Устанавливает значения для набор элементов
     * @param $aValues - набор пар имя поля - значение
     */
    public function setValues($aValues) {

        // сохранение набора данных
        $this->aValues = $aValues;

        // обойти весь пришедший массив
        foreach ( $aValues as $sFieldName => $mValue ) {

            // для найденных элементов установить значения
            if ( $this->hasField($sFieldName) )
                $this->aFields[$sFieldName]->setValue($mValue);

        }

    }

    /**
     * Преобразует сохраняемые данные объектами полей
     * @return array
     */
    public function getSaveValues() {

        // выходной массив
        $aOut = array();

        // перебираем сохраненный набор данных
        foreach ( $this->aValues as $sName => $mVal ) {

            // если инициализировано обрабатывающее поле
            if ( $this->hasField($sName) ) {
                // взять заначение из него
                $aOut[$sName] = $this->aFields[$sName]->getSaveValue();
            } else {
                // иначе просто пробросить значение
                $aOut[$sName] = $mVal;
            }

        }

        return $aOut;

    }

}
