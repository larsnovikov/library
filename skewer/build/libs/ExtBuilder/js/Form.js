/**
 * Автопостроитель интерфейсов
 * Класс для работы с формой
 */
Ext.define('Ext.Builder.Form', {
    extend: 'Ext.form.Panel',

    cls: 'sk-tab-form',

    height: '100%',
    width: '100%',
    flex: 1,
    autoScroll: true,
    border: 0,
    padding: 5,

    fieldDefaults: {
        labelAlign: 'left',
        margin: '2 5 2 0',
        labelWidth: 190,
        anchor: '100%'
    },

    items: [],

    initComponent: function() {

        // сформировать набор полей
        this.items = Ext.create('Ext.sk.FieldTypes').createFields( this.ifaceData.items || [], this.layerName );

        this.callParent();

        // при загрузке вкладок вызвать перехватчик
        processManager.addEventListener( 'tabs_load', this.Builder.path, this.blockPostSending, this );

        // при уничтожении снять прослушку события загрузки вкладок
        this.on('destroy',function(self){
            processManager.removeEventListener( 'tabs_load', self.Builder.path );
        });

    },

    /**
     * Выполнение после инициализации
     */
    execute: function(){

        var itemList = this.items.items;
        var key, item;

        // перебрать все поля
        for ( key in itemList ) {
            item = itemList[key];
            // выполнить спец обработку, если есть
            if ( typeof item.execute === 'function' )
                item.execute( arguments, key );
        }


    },

    /**
     * Получение данных при действии
     */
    getData: function( button ){

        // выяснить не отключена ли блокировка
        if ( !button['unsetFormDirtyBlocker'] ) {

            this.blockPostSending();

        }

        return { data: this.getValues() };

    },

    blockPostSending: function() {

        if ( Ext.isIE7 )
            return;

        var form = this;

        // если были внесены изменения
        if ( form.getForm().isDirty() ) {

            // создать перехват
            var key = processManager.interceptPostData();

            // запросить действие

            sk.confirmWindow(buildLang.editorCloseConfirmHeader, buildLang.editorCloseConfirm, function(res){
                if ( res === 'yes' ) {
                    // отправить данные
                    processManager.postInterceptedData( key );
                } else {
                    // сбросить посылку
                    processManager.terminateInterceptedData( key );
                }
            }, this );

        }

    }

});
