/**
 * Автопостроитель интерфейсов
 * Класс для работы со списками
 */

Ext.define('Ext.Builder.List', {

    extend: 'Ext.grid.Panel',

    requires: [
        'Ext.ux.CheckColumn'
    ],

    cls: 'sk-tab-list',

    width: '100%',
    height: '100%',
    border: 0,
    flex: 1,

    plugins: [],
    ifaceData: {},
    pagerLoaded: false,
    listeners: {
        // для операций редактирования в списковом режиме
        edit: function( editor, e ) {
            if ( !e.record.dirty ) return;
            var self = e.column;
            processManager.getMainContainer(self).setLoading(true);
            processManager.sendDataFromMainContainer( self, {
                cmd: self['listSaveCmd'],
                data: e.record.data
            } );
        }
    },

    initComponent: function() {

        var data = this.ifaceData;

        // инициализация сортировки переносом (drag and drop)
        this.initDD( data );

        // группировка записей
        this.initGrouping(data);

        // хранилище
        this.initStore(data);

        // инициализация постраничного
        this.initPager(data);

        // набор колонок
        this.initColumns(data);

        // создать объект
        this.callParent();

        // добавление полей фильтров
        this.resetFilters( data['barElements'] );

        // добавить данные для отображения
        this.getStore().loadData( data['items'] || [] );

    },

    /**
     * инициализация сортировки переносом (drag and drop)
     * @param data
     */
    initDD: function ( data ) {

        if ( data['ddAction'] ) {
            this.ddAction = data['ddAction'];
            this.viewConfig = {
                plugins: {
                    ptype: 'gridviewdragdrop',
                    ddGroup: 'firstGridDDGroup'
                },
                listeners: {
                    drop:  this.ddEvent
                }
            }

        }

    },

    ddEvent: function(node, data, dropRec, dropPosition) {
        var self = this;
        processManager.sendDataFromMainContainer( self, {
            cmd: this.up()['ddAction'],
            data: data.records[0].data,
            dropData: dropRec.data,
            position: dropPosition
        } );
    },

    /**
     * Инициализация группировки
     * @param data пришедшая посылка
     */
    initGrouping: function(data){

        if ( data['groupField'] ) {
            this.features = [{
                id: data['groupField'],
                ftype: 'groupingsummary',
                groupHeaderTpl: '{name}',
                hideGroupedHeader: true,
                remoteRoot: 'summaryData'
            }];
        }
    },

    /**
     * Инициализация хранилища
     * @param data пришедшая посылка
     */
    initStore: function(data){

        var onPage = data['itemsOnPage'] || 0,
            pageNum = (data['pageNum'] || 0)+1,
            totalCnt = data['itemsTotal'] || 0;

        this.store = Ext.create('Ext.data.Store',{
            pageSize: onPage,
            currentPage: pageNum,
            totalCnt: totalCnt,
            clearOnPageLoad: false,
            getCount: function(){ return onPage; },
            getTotalCount: function(){ return this.totalCnt; },
            fields: data['storeModel'] || [],
            sorters: data['sorters'] || [],
            groupField: data['groupField'] || '',
            data: []
        });

    },

    /**
     * Инициализация постраничного
     * @param data пришедшая посылка
     */
    initPager: function( data ) {

        var onPage = data['itemsOnPage'] || 0;

        // постраничный
        if ( onPage ) {
            this.dockedItems= [{
                xtype: 'pagingtoolbar',
                store: this.store,
                dock: 'bottom',
                actionName: data['actionNameLoad'],
                displayInfo: true,
                firstChange: true,
                listeners: {
                    change: this.onPageChange
                }
            }];
        }

    },

    /**
     * Инициализация колонок
     * @param data пришедшая посылка
     */
    initColumns: function(data){

        var colId, column,
            xTypes = Ext.create('Ext.sk.FieldTypes').getTypesAsObject(),
            xType, rName
        ;

        this.columns = data['columnsModel'] || [];

        // расширение описания колонок
        var bEditing = false;
        for ( colId in this.columns ) {

            // пришедшее на обработку описание
            column = this.columns[colId];

            // интерфейсное описание поля
            xType = (column['jsView'] && xTypes[column['jsView']]) ? xTypes[column['jsView']] : null;

            if( column['jsView'] == 'addImg' )
                xType = 'addImg';

            if ( xType ) {

                // добавление колонки картинок
                if( xType == 'addImg' ) {
                    this.columns[colId] = this.initImageBlock(data);
                    continue;
                }

                // если поле редактируемое
                if ( column['listSaveCmd'] && xType['listEditableSettings'] ) {
                    bEditing = true;
                    Ext.merge( column, xType['listEditableSettings'] );
                    if ( column['beforeInit'] )
                        column['beforeInit']();
                    this.columns[colId] = column;
                }

                // проверка наличия расширяющего описания
                else if ( xType['listSettings'] )
                    this.columns[colId] = Ext.merge( column, xTypes[column['jsView']]['listSettings'] );

            }

            // проверка наличия спец обработчиков
            rName = column['specRenderer'];
            if ( rName && xType['rendererList'] && xType['rendererList'][rName] )
                this.columns[colId]['renderer'] = xType['rendererList'][rName];


            if ( column['sortBy'] ) {
                column.getSortParam = function() {
                    return this['sortBy'];
                };
            }

        }

        if ( bEditing ) {
            var cellEditing = Ext.create('Ext.grid.plugin.CellEditing', {
                clicksToEdit: 1
            });
            this.plugins.push( cellEditing );

        }

        // добавление кнопок в строках
        this.initRowButtons(data);

    },

    addCellImage: function(val, cell, record) {

        var data = record.data;

        return '<img height="100px" src="'+data.preview_img+'">';

    },

    initImageBlock: function(data){

        var rowImage = {
            xtype: 'actioncolumn',
            flex: true,
            sortable: false,
            menuDisabled: true,
            renderer: this.addCellImage,
            items: []
        };

        //this.columns.push(rowImage);
        return rowImage;
    },

    initRowButtons: function(data){

        if ( data['rowButtons'] && data['rowButtons'].length) {
            // созадать контейнер для кнопок
            var rowButtons = {
                xtype: 'actioncolumn',
                width: 16*data['rowButtons'].length+4,
                sortable: false,
                menuDisabled: true,
                items: []
            };

            // добавить все кнопки
            for (var rowBtnDefKey in data['rowButtons']) {
                var rowBtn = data['rowButtons'][rowBtnDefKey];
                rowBtn.cls = 'sk-tab-list-row-btn-'+rowBtn.action;

                if ( rowBtn['customBtnName'] ) {

                    rowBtn = Ext.create( 'Ext.'+this.layerName+'.'+rowBtn['customBtnName'] );

                } else {

                    // надпись - попробовать найи в словаре
                    if ( rowBtn.tooltip && rowBtn.tooltip.search('_')!==-1 ){
                        var textAlias = rowBtn.tooltip.substr(1);
                        if ( buildLang[ textAlias ] )
                            rowBtn.tooltip = buildLang[ textAlias ];
                    }

                    // обработчик нажатия
                    rowBtn.handler = this.buttonHandler;

                }

                // двойной клик
                this.listeners.itemdblclick = this.onDblClick;

                rowButtons.items.push( rowBtn );
            }
            this.columns.push( rowButtons );
        }

    },

    execute: function( data, cmd ){

        switch ( cmd ) {

            // загрузка данных на страницу
            case 'loadPage':

                var store = this.getStore();

                // загрузка данных
                store.loadData( data['items'] || [] );

                // обновить общее число
                store.totalCnt = data['itemsTotal'] || 0;

                break;

        }

        if ( data['pageNum'] ) {
            this.updatePager();
        }

        this.Builder.setLoading(false);

    },

    /**
     * Обновление панели постраничного просмотра
     */
    updatePager: function(){

        // есть панель постраничного
        var pager = this.down('pagingtoolbar');
        if ( pager ) {
            var pageData,
                currPage,
                pageCount,
                afterText;

            if (!pager.rendered) {
                return;
            }

            pageData = pager.getPageData();
            currPage = pageData.currentPage;
            pageCount = pageData.pageCount;
            afterText = Ext.String.format(pager.afterPageText, isNaN(pageCount) ? 1 : pageCount);

            pager.child('#afterTextItem').setText(afterText);
            pager.child('#inputItem').setValue(currPage);
            pager.child('#first').setDisabled(currPage === 1);
            pager.child('#prev').setDisabled(currPage === 1);
            pager.child('#next').setDisabled(currPage === pageCount);
            pager.child('#last').setDisabled(currPage === pageCount);
            pager.child('#refresh').enable();
            pager.updateInfo();
        }

    },

    /**
     * Обработчик кнопок
     */
    buttonHandler: function( grid, rowIndex, colIndex, item ){

        var state = item.state || '';
        var action = item.action || '';
        var container = grid.up('panel');
        var rootCont = processManager.getMainContainer(grid);
        var rec = grid.getStore().getAt(rowIndex);

        if ( action ) {

            // данные к отправке
            var dataPack = {};
            Ext.merge(
                dataPack,
                rootCont.serviceData,
                {
                    cmd: action,
                    data: rec.data
                }
            );

            // функция отправки данных
            function postData(){
                processManager.setData(rootCont.path,dataPack);
                rootCont.setLoading(true);
                if ( item.doNotUseTimeout )
                    processManager.doNotUseTimeout();
                processManager.postData();
            }

            switch (state) {

                // обработка операции удаления
                case 'delete':

                    // удалить
                    var row_text = rec.get('title');

                    if ( !row_text )
                        row_text = rec.get('name');

                    if ( !row_text ) {
                        var titleField = '';
                        for ( var colId in container.columns ) {
                            var column = container.columns[colId];
                            if ( column && !column.hidden && column.dataIndex ) {
                                titleField = column.dataIndex;
                                break;
                            }
                        }
                        if ( titleField )
                            row_text = rec.get(titleField);
                    }
                    /** @todo Доделать кнопки */
                    // подтверждение удаления
                    var $oMsg = Ext.MessageBox;
                    var cfg = {
                        title: buildLang.delRowHeader,
                        icon: 'ext-mb-question',
                        msg: buildLang.delRow+'"'+row_text+'"?',
                        buttonText: {
                            ok: 'Ок', yes: 'Да', no: 'Нет', cancel: 'Отмена'
                        },
                        buttons: $oMsg.YESNO,
                        callback: function(res){

                            if ( res !== 'yes' ) return;

                            // отправить данные
                            postData();

                        },
                        scope: $oMsg

                    };

                    $oMsg.confirm(cfg);

                    break;

                /* Спрашиваем разрешение на выполнения действия (текст в self.actionText) */
                case 'allow_do':

                    Ext.MessageBox.confirm(buildLang.allowDoHeader,item.actionText, function(res){
                        if ( res !== 'yes' ) return false;
                        postData();
                        return true;
                    } );

                    break;

                default:
                    // отправить данные
                    postData();
                    break;

            }

        }

        return true;

    },

    /**
     * Обновить набр записей по фильтрам
     */
    commitFilterValues: function( self, pageData ){

        // задать значение по умолчанию
        if ( !pageData )
            pageData = {};

        var values = {},
            item,
            grid = self.up('panel'),
            rootCont = grid.up('panel').up('panel'),
            toolbar  = grid.down('toolbar');

        // если есть панель фильтров
        if ( toolbar ) {

            // набор элементов фильтров
            var items = toolbar.items.items;

            // собрать все фильтры воедино
            for ( var itemKey in items ) {

                // ссылка на элемент
                item = items[itemKey];

                // если есть метод запроса одиночного значения
                if ( typeof(item.getFilterName)==='function' )
                    values[ item.getFilterName() ] = item.getFilterValue();

                // если есть метод группрвого запроса
                if ( typeof(item.getGroupFilter)==='function' ) {
                    item.getGroupFilter(function( name, val ){
                        values[name] = val;
                    });
                }

            }

        }

        // задать комманду обновления
        values.cmd = grid.ifaceData['actionNameLoad'];

        // установить страницу
        values.page = pageData['currentPage']-1 || 0;

        // установить индикатор загрузки
        rootCont.setLoading(true);

        // данные к отправке
        var dataPack = {};
        Ext.merge(dataPack, rootCont.serviceData, values);

        processManager.setData(rootCont.path,dataPack);
        processManager.postData();
        rootCont.setLoading(true);

    },

    /**
     * При изменении номера страницы просмотра
     */
    onPageChange: function( self, pageData ){

        var tab = self.up('panel');

        // pager hack - иначе срабатывет загрузка при инициализации
        if ( !tab.pagerLoaded ) {
            tab.pagerLoaded = true;
            return false;
        }

        // контейнеры
        var grid = self.up('panel');

        // вызвать перегрузку элементов
        grid.commitFilterValues( self, pageData );

        return true;

    },

    /**
     * Событие по двойному клику на строке
     * @param self
     * @param rec
     * @param item
     * @param index
     */
    onDblClick: function( self, rec, item, index ) {

        // перебрать колонки
        var columns = self.up('panel').columns || [];
        for ( var columnId in columns ) {

            var column = columns[columnId];

            // найти колонку с кнопками
            if ( column.xtype == 'actioncolumn' ) {

                var buttons = column.items || [];

                // найти кнопку редактирования
                for ( var btnId in buttons ) {

                    var button = buttons[btnId];

                    // найти кнопку редактирования
                    if ( button.state == 'edit_form' ) {

                        button.handler( self, index, 0, button );

                    }

                }

            }

        }

    },

    /**
     * Получение данных при действии
     */
    getData: function(){

        var selection = this.getView().getSelectionModel().getSelection();
        var data = {};
        if ( selection.length ) {
            var row = selection.shift();
            data = row.data;
        }

        return { data: data };

    },

    /**
     * Переустановить набор фильтров
     * @param newItems
     */
    resetFilters: function( newItems ) {

        var grid = this,
            itemId, item;

        if ( !newItems.length )
            return;

        // перебрать все пришедшие поля
        for (itemId in newItems) {

            item = newItems[itemId];

            // добавление метода выполнения поиска
            item.doSearch = function(){
                grid.doSearch();
            };

            // если задан модуль - провести инициализацию сразу
            if ( item['libName'] )
                newItems[itemId] = Ext.create(item['libName'],item);

        }

        this.addDocked( {
            xtype: 'toolbar',
            dock: 'top',
            items: newItems
        } );

    },

    doSearch: function() {

        // постраничный
        var grid = this,
            pager = grid.down('pagingtoolbar');

        // есть панель постраничного
        if ( pager ) {
            // перейти к первой странице с новыми фильтрами
            pager.moveFirst();
        } else {
            // нет - просто отослать запрос на обновление
            grid.commitFilterValues(this);
        }

    }

});
