/**
 * Класс для автопостроения интерфейсов
 */
Ext.define('Ext.Builder.Builder',{

    extend: 'Ext.panel.Panel',
    region: 'center',

    ifaceData: {},
    serviceData: {},
    dirName: '/skewer_build/libs/ExtBuilder',

    nowComponent: null,
    nowComponentName: '',

    title: '-',
    actionText: '', // текст, выводимый в режиме allowDo кнопок
    path: '',
    width: '100%',
    closable: false,
    autoScroll: true,
    layout: 'fit',
    items: [],
    initOnActivate: true,

    // флаг выполнения метода execute
    executed: false,

    /**
     * Инициализация
     */
    initComponent: function(){

        var me = this;
        var data = me.ifaceData;

        // уникальный класс для контейнера
        me.cls = 'sk-tab-'+me.className;
        // уникальный класс для вкладки-заголовка
        me.tabConfig = {
            cls: 'sk-tabHeader-'+me.className
        };

        // набор инициализационных данных
        me.serviceData = data['serviceData'] || {};

        // заголовок вкладки
        me.title = data['componentTitle'] || me.path;

        // событие при активации вкладки
        me.on( 'activate', me.onActivate );

        // генерация объекта
        me.callParent();

    },

    /**
     * Выполнение пришедших команд
     */
    execute: function( data, cmd ) {

        var me = this;
        me.executed = true;

        if ( !data ) {
            sk.error('ExtBuilder. Wrong input.');
            return false;
        }

        if ( data['error'] ) {
            sk.error(data['error']+' ('+me.className+')');
        }

        // дополнительные библиотеки: заданы - прописать пути загрузки
        var extModuleName;
        if ( typeof data['subLibs'] == 'object' )
            for ( var subLibName in data['subLibs'] ) {
                if ( !data['subLibs'][subLibName] ) continue;
                extModuleName = 'Ext.Builder.'+data['subLibs'][subLibName];
                Ext.Loader.setPath(extModuleName, this.dirName+'/js/'+data['subLibs'][subLibName]+'.js');
            }

        // переменные для работы
        var component = me.nowComponent;

        // сервисные данные
        if ( data['serviceData'] )
            me.serviceData = data['serviceData'];

        var componentName;
        // если родной компонент построителя
        if ( data['extComponent'] ) {
            componentName = data['extComponent'];
            extModuleName = 'Ext.Builder.'+componentName;
        }
        // иначе инициализировать компонент слоя, если есть имя модуля
        else if ( data['componentName'] ) {
            extModuleName = 'Ext.'+this.layerName+'.'+data['componentName'];
            componentName = extModuleName;
        }

        else if ( data['skipInit'] ) {
            me.initOnActivate = false;
            return false;
        }

        // иначе, если стоит флаг загрузки при активации
        else if ( data['initTabFlag'] ) {

            // иначе ответ пришел без определения нового интерфейса

            // если вкладка выбрана
            if ( !me.isVisible() ) {
                // поставить галку обновления при активации
                this.initOnActivate = true;

                if ( me.cont ) {
                    me.removeAll();
                    me.cont.destroy();
                    me.cont = null;
                }


            }
            me.setLoading(false);
            return false;
        } else {
            me.setLoading(false);
            component.execute( data, data['cmd'] || '' );
            return false;
        }

        if ( !me.cont ) {

            me.cont = Ext.create('ExtBuilderContainer',{
                title: this.title,
                path: this.path
            });
            me.add( me.cont );

        }

        var cont = me.cont;

        // заголовок вкладки
        cont.setTitle( data['panelTitle'] || this.path );

        var doReload = !data['doNotReload'];

        // проверить необходимость переинициализации
        if ( me.nowComponentName != componentName || doReload ) {

            // удалить
            cont.removeAll();

            // создать основной компонент
            var props = {
                ifaceData: data,
                border: 0,
                layerName: me.layerName,
                serviceData: data['serviceData'],
                Builder: this
            };

            if ( data['columnsModel'] )
                props.columns = data['columnsModel'];

            // дополнительные параметры
            Ext.merge( props, data['init'] );

            component = Ext.create(extModuleName, props);
            me.nowComponent = component;
            me.nowComponentName = componentName;

            // добавление кнопок
            cont.resetDocked( data['dockedItems'] );

            if ( data['addText'] ) {

                cont.add( {
                    xtype: 'panel',
                    border: 0,
                    width: '100%',
                    items: {
                        border: 0,
                        padding: 5,
                        html: data['addText']
                    }
                } );

                // вставить разделитель, если он не запрещен
                if ( !component.dropPanelDelimiter ) {
                    cont.add({
                        height: 1,
                        width: '100%'
                    });
                }

            }

            cont.add( component );

        }

        // проверка наличия компонента
        if ( !component ) {
            sk.error('ExtBuilder. No component.');
            return false;
        }

        // сообщения
        var item, itemId;
        if ( data['pageMessages'] ) {
            for ( itemId in data['pageMessages'] ) {
                item = data['pageMessages'][itemId];
                sk.message( item[0], item[1] );
            }
        }

        // сообщения об ошибках
        if ( data['pageErrors'] ) {
            for ( itemId in data['pageErrors'] ) {
                item = data['pageErrors'][itemId];
                sk.error( item );
            }
        }

        // выполнить
        data.path = me.path;
        component.execute( data, data['cmd'] || '' );
        me.setLoading(false);

        return true;

    },

    /**
     * Вызывается после установки процесса
     */
    afterProcessSet: function() {
        var me = this;
        if ( me.nowComponent && me.nowComponent['afterProcessSet'] )
            me.nowComponent['afterProcessSet']( arguments );
    },

    onActivate: function() {
        var me = this;

        // если не надо инициализировать при активации - выйти
        if ( !me.initOnActivate )
            return false;

        // вкладка для открытия
        var tabToSelect = processManager.getEventValue('tab_to_select');

        // имя текущей вкладки
        var thisTabName = processManager.getModuleAlias(me.path);

        // если нужно открыть вкладку и не текущую - выйти
        if ( tabToSelect && tabToSelect!==thisTabName )
            return false;

        // убрать флаг инициализации при активации
        me.initOnActivate = false;

        // переинициализировать модуль
        me.reInitModule();

        return true;
    },

    /**
     * Вызвать инициализацию первичного интерфейса модуля
     */
    reInitModule: function() {
        var me = this;
        me.setLoading(true);
        processManager.setData(me.path,Ext.merge({
            cmd: 'init'
        },me.serviceData));
        me.setLoading(true);
        processManager.postData();
    }

});

Ext.define('ExtBuilderContainer',{

    extend: 'Ext.panel.Panel',
    layout: {
        type: 'vbox',
        align: 'center'
    },
    border: true,
    path: '',

    /**
     * Переустановить набор кнопок для состояния
     * @param newItems
     */
    resetDocked: function( newItems ) {

        var dItem;
        var me = this;
        var dItems = me.getDockedItems('toolbar');

        for ( dItem in dItems ) {
            me.removeDocked( dItems[dItem] );
        }

        // добавление элементов
        newItems = newItems || [];
        for ( var sDock in newItems ) {

            var itemsToAdd = [];
            for ( dItem in newItems[sDock] ) {
                var item = newItems[sDock][dItem];
                item.cls = 'sk-tab-btn sk-tab-btn-'+item.action;
                item.handler = me.dockedItemHandler;

                // надпись - попробовать найи в словаре
                if ( item.text && item.text.search('_')!==-1 ){
                    var textAlias = item.text.substr(1);
                    if ( buildLang[ textAlias ] )
                        item.text = buildLang[ textAlias ];
                }

                if ( item['userFile'] ) {
                    var builder = processManager.getMainContainer( this );
                    item = Ext.create( 'Ext.'+builder.layerName+'.'+item['userFile'], {
                        initData: item
                    } );
                }

                // модифицированное значение вернуть назад
                if ( item === '-&gt;' )
                    item = '->';

                // добавить элемент к набору
                itemsToAdd.push( item );
            }

            // добавление набора кнопок для состояния
            me.addDocked( {
                xtype: 'toolbar',
                dock: sDock,
                items: itemsToAdd
            } );

        }

        me.doLayout();

    },

    /**
     * Обработчик для управляющих элементов
     */
    dockedItemHandler: function(){

        var self = this,
            state = self.state || '',
            action = self.action || '',
            confirm = self['confirmText'] || '',
            container = self.up('panel').up('panel'),
            serviceData = container.serviceData || {},
            addParams = self.addParams||{},
            dataPack = {}
        ;

        if ( action ) {

            // команда
            dataPack.cmd = action;

            // данные от компонента
            var component = container.nowComponent,
                componentData = {};
            if ( component ) {
                if ( component.getData )
                    componentData = component.getData( this );
            }

            // функция отправки данных
            function postData(){
                Ext.merge( dataPack, serviceData, addParams, componentData );
                processManager.setData(container.path, dataPack);
                container.setLoading(true);
                if ( self.doNotUseTimeout )
                    processManager.doNotUseTimeout();
                processManager.postData();
            }


            switch (state) {
                /* Спрашиваем разрешение на удаление элемента */
                case 'delete':

                    var row_text = '';
                    if ( componentData.data ) {
                        row_text = componentData.data['title'] || componentData.data['name'] || '';
                    }
                    if ( row_text ) {
                        row_text = '"'+row_text+'"';
                    } else {
                        row_text = buildLang.delRowNoName;
                    }
                    sk.confirmWindow(buildLang.delRowHeader, buildLang.delRow+row_text+'?', function(res){
                        if ( res !== 'yes' ) return;
                        postData();
                    }, this );

                    break;
                /* Спрашиваем разрешение на выполнения действия (текст в self.actionText) */
                case 'allow_do':

                    Ext.MessageBox.confirm(buildLang.allowDoHeader,self.actionText, function(res){
                        if ( res !== 'yes' ) return;
                        postData();
                    } );

                    break;

                default:

                    if ( confirm ) {
                        sk.confirmWindow(buildLang.allowDoHeader,confirm, function(res){
                            if ( res !== 'yes' ) return;
                            postData();
                        }, this );
                    } else {
                        postData();
                    }

                    break;

            }// state switch

        }

        // иначе попробовать вызвать состояние компонента
        else if ( state ) {

            // данные
            var data = {
                path: container.path,
                serviceData: serviceData,
                addParams: Ext.merge(dataPack, serviceData, addParams)
            };

            // выполнить состояние компонента
            container.nowComponent.execute( data, state );

        }


    }

});
