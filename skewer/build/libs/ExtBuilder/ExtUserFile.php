<?php
/**
 * Класс для автоматической сборки админских интерфейсов на ExtJS
 *
 * Эта библиотека служит для построения интерфейса из пользовательских файлов
 * Собственного JS файла ни имеет
 *
 * @class: ExtUser
 *
 * @Author: User, $Author: sapozhkov $
 * @version: $Revision: 337 $
 * @date: $Date: 2012-06-20 21:00:40 +0400 (Ср., 20 июня 2012) $
 *
 */

class ExtUserFile extends ExtPrototype {

    /**
     * Конструктор объекта
     * @param $sLibName - имя основного пользовательского JS файла
     */
    public function __construct( $sLibName ) {
        $this->setLibName( $sLibName );
    }

    /**
     * @var string - Имя основной библиотеки
     */
    protected $sLibName = '';

    /**
     * Возвращает имя основной библиотеки
     * @return string
     */
    public function getLibName() {
        return $this->sLibName;
    }

    /**
     * Устанавливает имя основной библиотеки
     * @param string $sLibName
     */
    public function setLibName($sLibName) {
        $this->sLibName = $sLibName;
    }

    /**
     * Возвращает имя компонента
     * @return string
     */
    public function getComponentName() {
        return '';
    }

    /**
     * Отдает интерфейсный массив для атопостроителя интерфейсов
     * @return array
     */
    public function getInterfaceArray() {

        // выходной массив
        $aOut = array(
            'componentName' => $this->getLibName()
        );

        // вывод данных
        return $aOut;
    }

    /**
     * Задает инициализационный  массив для атопостроителя интерфейсов
     * @param \AdminModulePrototype $oModule - ссылка на вызвавший объект
     */
    public function setInterfaceData( AdminModulePrototype $oModule ) {

        $oModule->addLibClass( $this->getLibName() );

        parent::setInterfaceData( $oModule );

    }

}
