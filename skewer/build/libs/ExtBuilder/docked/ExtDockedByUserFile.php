<?php
/**
 * Создает кнопку по пользовательском js файлу
 */
class ExtDockedByUserFile extends ExtDockedPrototype {

    /** @var string имя файла для подгрузки */
    protected $sFileName = '';

    /**
     * @param string $sTitle подпись
     * @param string $sFileName имя файла
     * @return ExtDocked
     */
    public static function create( $sTitle, $sFileName ) {
        $oDocked = new ExtDockedByUserFile();
        $oDocked->setTitle( $sTitle );
        $oDocked->setFileName( $sFileName );
        return $oDocked;
    }

    /**
     * Отдает имя файла для загрузки
     * @return string
     */
    protected function getFileName() {
        return $this->sFileName;
    }

    /**
     * Задает имя файла для загрузки
     * @param string $sFileName
     */
    protected function setFileName( $sFileName ) {
        $this->sFileName = $sFileName;
    }

    /**
     * Отдает инициализационный массив
     * @param ExtPrototype $oExtInterface
     * @return array
     */
    public function getInitArray( ExtPrototype $oExtInterface ) {
        $oExtInterface->addLibClass( $this->getFileName() );
        return array_merge(
            parent::getInitArray(),
            array(
                'userFile' => $this->getFileName()
            )
        );
    }

}
