<?php
/**
 * Класс для работы с общими кнопками интерфейса (добавить / удалить / ...)
 */
class ExtDocked extends ExtDockedPrototype {

    /*
     * Набор иконок
     */
    const iconAdd = 'icon-add';
    const iconDel = 'icon-delete';
    const iconEdit = 'icon-edit';
    const iconSave = 'icon-save';
    const iconNext = 'icon-next';

    /**
     *
     * @param string $sTitle подпись
     * @return \ExtDocked
     */
    public static function create( $sTitle ) {
        $oDocked = new ExtDocked();
        $oDocked->setTitle( $sTitle );
        return $oDocked;
    }

}
