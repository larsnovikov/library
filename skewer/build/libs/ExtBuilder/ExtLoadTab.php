<?php
/**
 * Объект для создания новой пусто вкладки
 */
class ExtLoadTab extends ExtPrototype {

    /**
     * Возвращает имя компонента
     * @return string
     */
    function getComponentName() {
        return '';
    }

    /**
     * Отдает интерфейсный массив для атопостроителя интерфейсов
     * @return array
     */
    function getInterfaceArray() {
        return array();
    }

}
