﻿/*
 Copyright (c) 2003-2011, CKSource - Frederico Knabben. All rights reserved.
 For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function (config) {
    config.language = 'ru';
    config.uiColor = '#E9EEF5';
    config.dialog_backgroundCoverColor = '#CCCCCC';
    config.toolbar = [
        ['Source'],
        ['Maximize'],
        ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord'],
        ['Undo', 'Redo', '-', 'Find', 'Replace', '-', 'SelectAll', 'RemoveFormat'],
        ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent'],
        ['Subscript', 'Superscript'],
        ['TextColor', 'BGColor'],
        '/',
        ['Bold', 'Italic', 'Underline', 'Strike'],

        ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
        ['Link', 'Unlink', 'Anchor'],
        ['Image', 'Flash', 'Table', 'HorizontalRule', 'SpecialChar'],
        ['Format', 'FontSize']

    ];

};

CKEDITOR.config['filebrowserBrowseUrl']	= buildConfig.files_path+'?mode=fileBrowser&type=file&returnTo=ckeditor';
