/**
 * CKEditor надстройка для ExtJS
 */
Ext.form.CKEditor = function(config){

    this.config = config;

    Ext.form.CKEditor.superclass.constructor.call(this, config);
    this.on('destroy', function (ct) {
        ct.destroyInstance();
    });

};

Ext.define('Ext.form.CKEditor', {

    extend:'Ext.form.TextArea',

    alias:'widget.ckeditor',

    grow: true,

    onRender:function (ct, position) {

        var me = this;

        if (!this.el) {

            //noinspection JSUnusedGlobalSymbols
            this.defaultAutoCreate = {
                tag:'textarea',
                autocomplete:'off'
            };

        }

        Ext.form.TextArea.superclass.onRender.call(this, ct, position);
        if (!this.config.CKConfig) this.config.CKConfig = {};
        var config = {};
        var defConfig = {
            resize_enabled: me.grow,
            on:{
                // maximize the editor on startup
                'instanceReady':function (evt) {
                    var editor = Ext.get('cke_'+evt.editor.name);
                    var fieldDom = editor.dom.parentElement.parentElement;
                    var field = Ext.ComponentManager.get(fieldDom.id);
                    evt.editor.resize((evt.editor.element.$.style.width ? evt.editor.element.$.style : '100%'), field.height);
                    evt.editor.is_instance_ready = true;
                },
                resize: function( evt ) {
                    var editor = this.editor ? this.editor : this.editor = Ext.get('cke_'+evt.editor.name);
                    var field = this.field ? this.field : this.field = Ext.ComponentManager.get(editor.dom.parentElement.parentElement.id);
                    field.setHeight( editor.getHeight() );
                    field.autoSize();
                }
            }
        };
        Ext.merge(
            config,
            this.config.CKConfig,
            defConfig,
            this['addConfig'] ? this['addConfig'] : {}
        );
        // если задана дополнительная конфикурация
        this.elementId = this.getInputId();
        CKEDITOR.replace(this.elementId, config);

    },

    onResize:function (width, height) {
        Ext.form.TextArea.superclass.onResize.call(this, width, height);
        if (CKEDITOR.instances[this.elementId].is_instance_ready) {
            CKEDITOR.instances[this.elementId].resize(width, height);
        }
    },

    setValue:function (value) {
        if (!value) value = ' ';
        Ext.form.TextArea.superclass.setValue.apply(this, arguments);
        if (CKEDITOR.instances[this.elementId]) CKEDITOR.instances[this.elementId].setData(value);
    },

    getValue:function () {
        if ( Ext.isIE7 )
            return '';
        if (CKEDITOR.instances[this.elementId]) CKEDITOR.instances[this.elementId].updateElement();
        return Ext.form.TextArea.superclass.getValue.call(this);
    },


    getRawValue:function () {
        if (CKEDITOR.instances[this.elementId]) CKEDITOR.instances[this.elementId].updateElement();
        return Ext.form.TextArea.superclass.getRawValue.call(this);
    },

    destroyInstance:function () {
        if (CKEDITOR.instances[this.elementId]) {
            delete CKEDITOR.instances[this.elementId];
        }
    }

});
