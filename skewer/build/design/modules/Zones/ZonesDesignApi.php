<?php
/**
 * Библиотека методов работы с редактором зон
 */
class ZonesDesignApi {

    /** имя группы с хранилищем зон */
    const layoutGroupName = '.layout';

    /** имя параметра с названием шаблона */
    const layoutTitleName = '.title';

    /** имя параметра со значением для сортировки шаблонов */
    const layoutOrderName = '.order';

    /** имя параметра для хранения доступных меток отображения */
    const layoutParamName = 'layout';

    /** имя параметра для хранения модуля в группе */
    const moduleParamName = 'object';

    /** значение веса - не связанный */
    const weightNone = 0;

    /** значение веса - родительский */
    const weightParent = 1;

    /** значение веса - текуущий */
    const weightCurrent = 2;

    /**
     * Отдает id родительского раздела шаблонов
     * @return int
     */
    protected static function getTplRootSection() {
        return (int)skConfig::get('section.templates');
    }

    /**
     * Отдает id родительского раздела шаблонов
     * @return int
     */
    protected static function getRootSection() {
        return (int)(int)skConfig::get('section.sectionRoot');
    }

    /**
     * Отдает набор доступных шаблонов
     * @static
     */
    public static function getTplList( $sShowUrl = '/' ) {

        // запросить все параметры со спец метками
        $aParamList = Parameters::getListByName( self::layoutTitleName, self::layoutGroupName );

        // набор значений для сортировки шаблонов
        $aOrderParams = Parameters::getListByName( self::layoutOrderName, self::layoutGroupName );
        $aOrderList = array();
        foreach ( $aOrderParams as $aOrderItem )
            $aOrderList[(int)$aOrderItem[Parameters::PARENT]] = (int)$aOrderItem[Parameters::VALUE];

        // контейнер для шаблонов
        $aTplList = array();

        // вычислить id текущей страницы
        $iShowSectionId = self::getSectionIdByPath( $sShowUrl );

        // получить иерархию разделов наследования в виде набора разделов
        $aParentList = Parameters::getParentList( $iShowSectionId );

        // флаг "основной шаблон найден"
        $bFoundMainTpl = false;

        // перебрать все параметры
        foreach ( $aParamList as $aParam ) {

            // id раздела
            $iSestionId = (int)$aParam[Parameters::PARENT];

            // вес
            if ( $iSestionId===$iShowSectionId ) {
                $iWeight = self::weightCurrent;
                $bFoundMainTpl = true;
            }elseif ( in_array($iSestionId,$aParentList) ) {
                $iWeight = self::weightParent;
            } else {
                $iWeight = self::weightNone;
            }

            // добавить в выходной массив
            $aTplList[$iSestionId] = array(
                'id' => $iSestionId,
                'title' => $aParam[Parameters::VALUE],
                'weight' => $iWeight,
                'order' => isset($aOrderList[$iSestionId]) ? $aOrderList[$iSestionId] : 1000000+$aParam[Parameters::ID]
            );

        }

        // если не найден основной шаблон - взять первый родительский
        if ( !$bFoundMainTpl ) {
            foreach ( $aParentList as $iParentId ) {
                if ( isset( $aTplList[$iParentId] ) ) {
                    $aTplList[$iParentId]['weight'] = self::weightCurrent;
                    break;
                }
            }
        }

        // исключть корневой раздел
        $iRootSectionId = self::getRootSection();
        if ( isset($aTplList[$iRootSectionId]) )
            unset($aTplList[$iRootSectionId]);

        // сортировка
        uasort( $aTplList, array(__CLASS__, 'tplSort') );

        return array_values( $aTplList );

    }

    /**
     *
     * @param array $a
     * @param array $b
     * @return mixed
     */
    protected static function tplSort($a,$b) {
        return $a['order'] - $b['order'];
    }

    /**
     * Отдает набор зон для заданного шаблона
     * @static
     * @param $iTplId
     * @return array
     */
    public static function getZoneList( $iTplId ) {

        $iTplId = (int)$iTplId;

        // запрос набора параметров
        $aParamList = Parameters::getAll( $iTplId, array('fields'=>array(
            'id',
            'parent',
            'group',
            'name',
            'value',
            'title',
            'access_level'
        )) );

        $aOut = array();

        // если есть контейнер с
        if ( isset($aParamList[self::layoutGroupName]) ) {

            // перебрать все запись
            foreach ( $aParamList[self::layoutGroupName] as $aParam ) {

                // не включать служебные параметры
                if ( in_array($aParam['name'], array(self::layoutTitleName, self::layoutOrderName) ) )
                    continue;

                // занести в выходной массив с разделением на собственные и наследованные
                $aOut[] = array(
                    'id' => $aParam['id'],
                    'title' => $aParam['title'],
                    'own' => (int)$aParam['parent']===$iTplId
                );

            }

        }

        return $aOut;

    }

    /**
     * Удаление зоны
     * @static
     * @param int $iZoneId
     * @param int $iTplId
     * @throws Exception
     * @return int
     */
    public static function deleteZone( $iZoneId, $iTplId ) {

        if ( (int)$iTplId === self::getRootSection() )
            throw new Exception('Удаление зон из основных настроек запрещено');

        return Parameters::delById( $iZoneId, $iTplId );

    }

    /**
     * Отдает
     * @static
     * @param int $iZoneId
     * @param int $iTplId
     * @return array
     * @throws Exception
     */
    public static function getLabelList( $iZoneId, $iTplId ) {

        // получить запись с зоной
        $aParam = self::getZoneRow( $iZoneId );

        // флаг собственных параметров
        $bOwn = (int)$aParam['parent'] === $iTplId;

        // набор меток
        $aLabelList = explode(',',$aParam['value']);

        // набор связей метка-модуль
        $aLabelToModuleName = self::getLabelToModuleNameArray( $iTplId );

        // собрать набор меток
        $aOut = array();
        foreach ( $aLabelList as $sLabelName ) {
            if ( !$sLabelName )
                continue;

            // имя модуля
            $sModule = isset($aLabelToModuleName[$sLabelName]) ? $aLabelToModuleName[$sLabelName] : $sLabelName;
            $sModule = preg_replace( '/Module$/', '', $sModule );

            $aOut[] = array(
                'name' => $sLabelName,
                'title' => FrameDesignApi::getModuleTitleByName( $sModule ),
                'own' => $bOwn
            );
        }

        // отдать набор меток
        return $aOut;

    }

    /**
     * Список возможных для добавления элементов
     * @static
     * @param int $iZoneId
     * @param int $iTplId
     * @return array
     */
    public static function getAddLabelList( $iZoneId, $iTplId ) {

        $aOut = array();

        // имя зоны
        $sZoneName = self::getZoneName( $iZoneId );

        // список подключенных меток
        $aSetLabelList = self::getAllLabelsForTpl( $iTplId );

        // набор связей метка-модуль
        $aLabelToModuleName = self::getLabelToModuleNameArray( $iTplId );

        // перебираем все параметры, сгруппированные по меткам
        $aParamList = Parameters::getAll( $iTplId );
        foreach ( $aParamList as $sLabelName => $aLabelParams ) {

            // пропустить корневую
            if ( $sLabelName==='.' )
                continue;

            // ищем группы с заданным параметром положения
            if ( !isset($aLabelParams[self::layoutParamName]) or !$aLabelParams[self::layoutParamName]['value'] )
                continue;

            // доступные для отображения зоны
            $aAllowedForGroup = explode(',',$aLabelParams[self::layoutParamName]['value']);

            // если есть в списке терущая группа
            if ( in_array($sZoneName, $aAllowedForGroup) ) {

                // имя модуля
                $sModule = isset($aLabelToModuleName[$sLabelName]) ? $aLabelToModuleName[$sLabelName] : $sLabelName;

                $aOut[] = array(
                    'name' => $sLabelName,
                    'title' => FrameDesignApi::getModuleTitleByName( $sModule ),
                    'own' => !in_array( $sLabelName, $aSetLabelList )
                );


                $aAllowedLabels[] = $sLabelName;
            }

        }

        return $aOut;

    }


    /**
     * Проверяет принадлежность зоны к шаблону.
     * создает новую запись если чужая и возвращает id
     * @static
     * @param $iZoneId
     * @param $iTplId
     * @throws Exception
     * @return int
     */
    public static function getZoneForTpl( $iZoneId, $iTplId ) {

        // получить запись с зоной
        $aParam = self::getZoneRow( $iZoneId );

        // приведение типов
        $iTplId = (int)$iTplId;

        // если параметр принадлежит текущему шаблону - выйти
        if ( (int)$aParam['parent'] === $iTplId )
            return $iZoneId;

        // перекрытие параметров
        $aParam['id'] = 0;
        $aParam['parent'] = $iTplId;

        // сохранение записи
        return Parameters::saveParameter( $aParam );

    }

    /**
     * Отдает id зоны по имени и номеруц шаблона
     * @static
     * @param string $sZoneName
     * @param int $iTplId
     * @return int
     */
    public static function getZoneIdByName( $sZoneName, $iTplId ) {

        // запросить параметр рекурсивно по шаблонам
        $aParam = Parameters::getByName( $iTplId, self::layoutGroupName, $sZoneName, true );

        return $aParam ? $aParam['id'] : 0;

    }

    /**
     * Сортирует набор меток для зоны
     * @static
     * @param string[] $aLabels
     * @param int $iZoneId
     * @throws Exception
     * @return int
     */
    public static function saveLabels( $aLabels, $iZoneId ) {

        // получить запись с зоной
        $aParam = self::getZoneRow( $iZoneId );

        // убрать повторяющиеся
        $aLabels = array_unique($aLabels);

        // сборка параметра для сохранения
        $aParam['value'] = implode(',',$aLabels);

        // сохранение записи
        return Parameters::saveParameter( $aParam );

    }

    /**
     * Удаляет метку по имени
     * @static
     * @param int $sLabelName
     * @param int $iZoneId
     * @throws Exception
     * @return bool
     */
    public static function deleteLabel( $sLabelName, $iZoneId ) {

        // получить запись с зоной
        $aParam = self::getZoneRow( $iZoneId );

        // разобрать значение на подстроки
        $aLabels = explode(',',$aParam['value']);

        // если параметр есть
        if ( in_array( $sLabelName, $aLabels ) ) {

            // удалить параметр
            unset( $aLabels[array_search( $sLabelName, $aLabels )] );

            // сборка параметра для сохранения
            $aParam['value'] = implode(',',$aLabels);

            // сохранение записи
            return (bool)Parameters::saveParameter( $aParam );

        } else {
            return false;
        }

    }

    /**
     * Отдает запись с зоной или выбрасывает исключение
     * @static
     * @param $iZoneId
     * @return array
     * @throws Exception
     */
    protected static function getZoneRow( $iZoneId ) {
        // запросить параметр
        $aParam = Parameters::getById( $iZoneId );
        if ( !$aParam )
            throw new Exception('Зона не обнаружена');
        return $aParam;
    }

    /**
     * Отдает имя зоны или выбрасывает исключение
     * @static
     * @param $iZoneId
     * @return string
     * @throws Exception
     */
    protected static function getZoneName( $iZoneId ) {
        // запросить параметр
        $aParam = Parameters::getById( $iZoneId );
        if ( !$aParam )
            throw new Exception('Зона не обнаружена');
        return $aParam['name'];
    }

    /**
     * Отдает массив связей метка-модуль для заданного раздела
     * @static
     * @param int $iTplId
     * @return array
     */
    private static function getLabelToModuleNameArray( $iTplId ) {

        $aOut = array();

        // взять все параметры
        $aParamList = Parameters::getAll( $iTplId );

        // перебрать их
        foreach ( $aParamList as $sLabelName => $aLabelParams ) {

            // пропустить корневую
            if ( $sLabelName==='.' )
                continue;

            // ищем группы с заданным параметром модуля
            if ( isset($aLabelParams[self::moduleParamName]) and $aLabelParams[self::moduleParamName]['value'] ){
                $aOut[$sLabelName] = $aLabelParams[self::moduleParamName]['value'];
            }

            // ищем группы с заданным заголовком
            if ( isset($aLabelParams[self::layoutTitleName]) and $aLabelParams[self::layoutTitleName]['value'] ){
                $aOut[$sLabelName] = $aLabelParams[self::layoutTitleName]['value'];
            }

        }

        return $aOut;

    }

    /**
     * Отдает все подключенные метки для шаблона
     * @static
     * @param $iTplId
     * @return array
     */
    private static function getAllLabelsForTpl( $iTplId ) {

        // запросить все параметры
        $aAllParamList = Parameters::getAll( $iTplId );

        // проверить наличие обязательного параметра
        if ( !isset($aAllParamList[self::layoutGroupName]) )
            return array();

        // параметры служебной группы
        $aLayoutParams = $aAllParamList[self::layoutGroupName];

        // выходной массив
        $aOut = array();

        // собрать все метки
        foreach ( $aLayoutParams as $sParamName => $aParam ) {

            // служебный параметр пропускаем
            if ( $sParamName === self::layoutTitleName )
                continue;

            // значение
            $sValue = $aParam['value'];
            if ( !$sValue )
                continue;

            $aOut = array_merge($aOut,explode(',',$sValue));

        }

        // отдать набор уникальных значений
        return array_unique($aOut);

    }

    /**
     * Отдает id раздела по пути
     * @static
     * @param $sShowUrl
     * @return int
     */
    public static function getSectionIdByPath( $sShowUrl ) {

        // отрезать хост, если есть
        $sHost = 'http://'.skConfig::get('url.root');
        if ( strpos( $sShowUrl, $sHost ) === 0 )
            $sShowUrl = substr( $sShowUrl, strlen($sHost)-1 );

        if ( $sShowUrl==='/' )
            return skConfig::get('section.main');

        // вычислить id текущей страницы
        $oTree = new Tree();
        $iSectionId = $oTree->getIdByPath( $sShowUrl );

        return (int)$iSectionId;

    }

    /**
     * Отдает id шаблона для заданного url
     * @static
     * @param string $sShowUrl
     * @return int
     */
    public static function getTplIdByPath( $sShowUrl ) {

        $iShowId = self::getSectionIdByPath( $sShowUrl );

        // получить иерархию разделов наследования в виде набора разделов
        $aParentList = Parameters::getParentList( $iShowId );

        // добавить к списку текущий раздел
        array_unshift($aParentList,$iShowId);

        // найти первый раздел с определенной зоной layout
        while ( $iSectionId = array_shift($aParentList) ) {
            if ( Parameters::getByName( $iSectionId, self::layoutGroupName, self::layoutTitleName, false ) )
                return $iSectionId;
        }

        return 0;

    }

}
