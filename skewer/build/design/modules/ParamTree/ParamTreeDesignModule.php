<?php
/**
 * 
 * @class ParamTreeDesignModule
 *
 * @author ArmiT, $Author: acat $
 * @version $Revision: 974 $
 * @date $Date: 2012-10-05 12:54:23 +0400 (Пт., 05 окт. 2012) $
 *
 */ 
class ParamTreeDesignModule extends AdminModulePrototype {

    /**
     * @var string режим отображения (по-умолчанию или pda версия)
     */
    protected $sViewMode = Design::versionDefault;

    /**
     * Состаяние. Выбор дерева групп
     * @return bool
     */

    protected function actionInit() {

        $oDesignManager = new DesignManager();

        // устновитьь состояние
        $this->setCmd('init');

        // отдать набор групп
        $this->setData('items', $oDesignManager->getGroupsTree(0,0,array('id','title','visible'), $this->sViewMode) );

        return true;

    }

    /**
     * Проверка состояния
     */
    protected function actionCheckVersion(){

        // текущая версия в клиентской части
        $nowVersion = $this->getStr('ver',Design::versionDefault);

        // url открытой страницы
        $nowUrl = $this->getStr('url');

        // новая версия из url
        $newVersion = Design::getVersionByUrl($nowUrl);

        $this->sViewMode = $newVersion;

        if ( $nowVersion !== $newVersion ) {

            // отдать набор групп
            $this->actionInit();

            // переустновить состояние
            $this->setCmd('loadItems');

            // отдать новую версию
            $this->setData('newVersion', $newVersion );

        }

    }

}
