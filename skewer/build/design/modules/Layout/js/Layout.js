/**
 * Корневой слой вывода интерфейса
 */

Ext.define('Ext.design.Layout',{
    extend: 'Ext.Viewport',
    id: 'view-port',
    layout: 'border',
    items: [{
        region: 'center',
        html: 'viewport'
    }],

    // после инициализации объекта
    afterRenderInterface:function(){

        if ( pageHistory )
            pageHistory.afterRender();

    }

});
