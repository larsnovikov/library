<?php
/**
 * Модуль вывода основного интерфейсного контейнера
 * @class LayoutDesignModule
 *
 * @author ArmiT, $Author: sapozhkov $
 * @version $Revision: 461 $
 * @date $Date: 2012-07-24 13:26:48 +0400 (Вт., 24 июля 2012) $
 *
 */ 
class LayoutDesignModule extends AdminModulePrototype {

    public function execute() {

        $this->addChildProcess(new skContext('head','HeaderDesignModule',ctModule,array()));
        $this->addChildProcess(new skContext('tabs','TabsDesignModule',ctModule,array()));

        return psComplete;

    }

}
