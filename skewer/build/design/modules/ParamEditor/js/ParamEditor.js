/**
 * Работа с параметрами дизайнерского режима
 */
Ext.define('Ext.design.ParamEditor', {
    title: designLang.paramsPanelHeader,
    extend: 'Ext.design.ParamEditorGrid',
    margins: '5 5 5 0',
    region: 'center',

    width: 300,
    nameColumnWidth: 200,

    propertyNames: {},
    source: {},
    groupId: 0,

    // инициализация
    initComponent: function() {

        this.callParent();

        /**
         * установить обработчики
         */

        // выбор группы параметров
        processManager.addEventListener('group_select',this.path,'cmsOnGroupSelect');
        processManager.addEventListener('tree_loaded',this.path,'clearElements');
        processManager.addEventListener('reload_show_frame',this.path,this.reloadShowFrame);
        processManager.addEventListener('reload_param_editor',this.path,this.reloadItems);
        processManager.addEventListener('save_css_params',this.path,this.saveCssParams);

        // изменение строки
        this.on('propertychange',this.onFieldUpdate,this);

    },

    // выполение обработки данных
    execute: function( data, cmd ){

        switch ( cmd ) {

            // загрузка подчиненных элементов
            case 'loadItems':

                this.groupId = data.groupId;

                // контейнер для элементов
                var items = {};

                // набор названий
                this.propertyNames = {};

                // набор редакторов
                this.customEditors = {};

                // если есть строки
                if ( data.items ) {

                    // перебрать все
                    for ( var key in data.items ) {

                        // строка с данными
                        var row = data.items[key];

                        // имя параметра
                        var name = row.id;

                        // добавить в набор
                        items[name] = row['value'];

                        // задать имя
                        this.propertyNames[name] = row.title;

                        // устанавливает нужный редактор, если необходимо
                        this.setEditor( row );

                    }

                }

                // заменить набор элементов
                this.setSource( items );

                break;

        }

        this.setLoading( false );

    },

    saveCssParams: function( items ) {
        var me = this;
        processManager.setData(me.path,{
            cmd: 'saveCssParams',
            data: items
        });
        processManager.postData();
    },

    reloadShowFrame: function() {
        frameApi.reloadDisplayFrame();
    },

    reloadItems: function() {
        this.cmsOnGroupSelect( this.groupId );
    },

    /**
     * Вызывается при нажатии кнопки возврата значения по умолчанию
     * @param rec
     */
    onRevert: function( rec ) {

        var me = this;

        Ext.MessageBox.confirm(designLang.paramsRevertParamTitle, designLang.paramsRevertParam,function(res){

            if ( res !== 'yes' ) return false;

            processManager.setData(me.path,{
                cmd: 'revertParam',
                groupId: me.groupId,
                id: rec.name
            });
            processManager.postData();

            return true;

        });

    },

    /**
     * устанавливает нужный редактор, если необходимо
     * @param row - object
     */
    setEditor: function( row ) {

        var name = row['id'];

        // перебор типов
        switch ( row['type'] ) {

            // файл
            case 'url':
                this.customEditors[name] = Ext.create('Ext.sk.field.FileSelector',{
                    selectMode: 'designFileBrowser'
                });
                break;

            // цвет
            case 'color':
                this.customEditors[name] = Ext.create('Ext.sk.field.ColorSelector');
                break;

            // Толщина шрифта
            case 'font-weight':
                this.customEditors[name] = this.createComboBox('normal, bold, bolder, lighter');
                break;

            // Название шрифта
            case 'family':
                this.customEditors[name] = this.createComboBox('Tahoma, Arial, Verdana, Times New Roman', true);
                break;

            // Стиль шрифта
            case 'font-style':
                this.customEditors[name] = this.createComboBox('normal, italic, oblique');
                break;

            // Повторение фонового изображения
            case 'repeat':
                this.customEditors[name] = this.createComboBox('repeat, no-repeat, repeat-x, repeat-y');
                break;

            // Горизонтальное выравнивание
            case 'h-position':
                this.customEditors[name] = this.createComboBox('left, right, center',true);
                break;

            // Вертикальное выравнивание
            case 'v-position':
                this.customEditors[name] = this.createComboBox('top, center, bottom',true);
                break;

            // Горизонтальное выравнивание
            case 'h-position-abs':
                this.customEditors[name] = this.createComboBox('left, right',true);
                break;

            // Вертикальное выравнивание
            case 'v-position-abs':
                this.customEditors[name] = this.createComboBox('top, bottom',true);
                break;

            // Вертикальное выравнивание
            case 'text-transform':
                this.customEditors[name] = this.createComboBox('none, capitalize, lowercase, uppercase, inherit',true);
                break;

            // Горизонтальное выравнивание
            case 'text-align':
                this.customEditors[name] = this.createComboBox('left, right, center, justify');
                break;

            // Вертикальное выравнивание
            case 'vectical':
            case 'vectical-align':
                this.customEditors[name] = this.createComboBox('baseline, sub, super, top, middle, bottom, text-top, text-bottom');
                break;

            // стиль бордюра
            case 'border-style':
                this.customEditors[name] = this.createComboBox('none, hidden, dotted, dashed, solid, double, groove, ridge, inset, outset, inherit');
                break;

            // подчёркивание
            case 'text-decoration':
                this.customEditors[name] = this.createComboBox('none, underline, overline, line-through, blink');
                break;

            // подчёркивание
            case 'switch':
                this.customEditors[name] = this.createComboBox('block, none');
                break;

            // Привязка фона
            case 'bg-attachment':
                this.customEditors[name] = this.createComboBox('scroll, fixed');
                break;

        }

    },

    /**
     * Добавляет выпадающий список как редактор
     * @param data
     * @param [editable]
     * @param [displayField]
     * @param [valueField]
     */
    createComboBox: function( data, editable, valueField, displayField ) {

        // значения по умолчанию
        if ( !editable ) editable = false;
        if ( !valueField ) valueField = 'value';
        if ( !displayField ) displayField = valueField;

        if ( typeof data === 'string' ) {
            var dataItems = [];
            var splitData = data.split(/,[\s]*/);
            for ( var item in splitData ) {
                dataItems.push({value: splitData[item]})
            }
            data = dataItems;
        }

        // набор элементов
        var fields = valueField===displayField ? [valueField] : [valueField, displayField];

        return Ext.create('Ext.form.field.ComboBox',{
            allowBlank: false,
            editable: editable,
            displayField: displayField,
            valueField: valueField,
            store:  {
                fields: fields,
                data: data
            }
        });

    },

    /**
     * При выборе группы (событие) в дереве добавить данные к посылке
     * @param groupId
     */
    cmsOnGroupSelect: function( groupId ){

        this.setLoading( true );

        // добавить данные в посылку
        processManager.setData(this.path,{
            cmd: 'loadItems',
            groupId: groupId
        })

    },

    /**
     * Обработчик изменения значения поля
     */
    onFieldUpdate: function(  source, recordName, value ){

        // собрать посылку
        processManager.setData(this.path,{
            cmd: 'updParam',
            value: value,
            id: recordName
        });

        // отослать её
        processManager.postData();

    },

    /**
     * Очистить набор элементов
     */
    clearElements: function(){

        // контейнер для элементов
        var items = {};

        // набор названий
        this.propertyNames = {};

        // набор редакторов
        this.customEditors = {};

        // заменить набор элементов
        this.setSource( items );

    }

});
