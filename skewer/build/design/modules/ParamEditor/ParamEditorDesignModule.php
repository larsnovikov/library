<?php
/**
 * Класс для редактирования набора параметров дизайнерского режима
 * @class ParamEditorDesignModule
 *
 * @author sapozhkov, $Author: acat $
 * @version $Revision: 1205 $
 * @date $Date: 2012-11-15 17:27:51 +0400 (Чт., 15 нояб. 2012) $
 *
 */
class ParamEditorDesignModule extends AdminModulePrototype {

    /**
     * Состаяние. Выбор корневого набора разделов
     * @return bool
     */
    protected function actionInit() {

        // команда инициализации
        $this->setCmd('init');

        $this->addLibClass('ParamEditorGrid');
        $this->addLibClass('ParamEditorGridColumns');

    }


    /**
     * Состаяние. Выбор корневого набора разделов
     * @param null $iGroupId
     * @return bool
     */
    protected function actionLoadItems( $iGroupId=null ) {

        // id группы
        if ( is_null($iGroupId) )
            $iGroupId = $this->getInt( 'groupId' );

        // команда отображения списка
        $this->setCmd('loadItems');

        // запросить данные для вывода
        $this->setData('items', DesignManager::getParamsByGroup( $iGroupId ) );
        $this->setData('groupId',$iGroupId);

    }

    /**
     * Обновление параметра
     */
    protected function actionUpdParam() {

        // входной набор эдементов
        $iId = $this->getStr('id');
        $sValue = $this->getStr('value');

        // сохранить
        $bRes = DesignManager::saveCSSParamValue( $iId, $sValue );
        if ( $bRes ) {
            $this->addMessage('Значение сохранено');
            $this->actionLoadItems( DesignManager::getGroupByParam($iId) );
            $this->fireJSEvent('reload_show_frame');
        } else {
            $this->addError('Значение не сохранено');
        }


    }

    /**
     * Сбрасывает значение параметра на стандартное
     */
    protected function actionRevertParam() {

        // входной набор эдементов
        $iId = $this->getInt('id');

        // откатить
        if ( DesignManager::revertSCCParam( $iId ) )
            $this->actionLoadItems();

        $this->fireJSEvent('reload_show_frame');

    }


    /**
     * Сохраняет значение отступов при перетаскивании мышкой блоков в шапке
     */
    protected function actionSaveCssParams() {

        $aData = $this->get('data');
        $sLayer = 'default';
        $aParamH = DesignManager::getParam( $aData['paramPath'].'.h_value', $sLayer );
        if($aParamH){
            $aParamH['value'] = ($aData['hValue']*1)."px";
            DesignManager::saveCSSParam($aParamH);
        }

        $aParamV = DesignManager::getParam( $aData['paramPath'].'.v_value', $sLayer );
        if($aParamV){
            $aParamV['value'] = ($aData['vValue']*1)."px";
            DesignManager::saveCSSParam($aParamV);
        }

        $this->fireJSEvent('reload_param_editor');

    }

}
