<?php
/**
 * Модуль работы с редактором пользовательского css кода
 */
class CSSEditorDesignModule extends AdminTabModulePrototype {

    /** @var stringимя вкладки */
    protected $sTabName = 'CSS Редактор';

    /** @var string режим отображения (по-умолчанию или pda версия) */
    protected $sViewMode = Design::versionDefault;

    /**
     * Пользовательская функция инициализации модуля
     */
    protected function onCreate() {
        $this->addJSListener( 'urlChange', 'checkVersion' );
    }

    protected function actionCheckVersion() {

        // получить тип отображения
        list( $sUrl ) = $this->get('params');
        if ( !$sUrl ) throw new Exception('Url не задан');
        $sType = Design::getVersionByUrl( $sUrl );

        // если не совпадает с текущим
        if ( $sType !== $this->sViewMode ) {
            $this->sViewMode = $sType;
            $this->actionInit();
        }

    }

    /**
     * Метод, выполняемый в начале обработки
     */
    protected function preExecute() {

        // тип отображения сайта
        $this->sViewMode = Design::getValidVersion( $this->getStr('type', $this->sViewMode) );

        // протестировать наличие необходимх файлов
        $this->testFileAccess( $this->sViewMode );

    }

    /**
     * Состаяние. Выбор корневого набора разделов
     * @return bool
     */
    protected function actionInit() {

        // объект для построения списка
        $oForm = new ExtForm();

        $aItems = array();

        /* Файл */
        $aItems['css_text'] = array(
            'name' => 'css_text',
            'title' => 'Использовать локальные настройки',
            'view' => 'text',
            'value' => $this->getValFromFile(),
            'hideLabel' => true,
            'height' => '100%',
            'margin' => '0 1 0 0',
            //'disabled' => true,
        );

        $oForm->setInitParam( 'autoScroll', false );

        $oForm->addBntSave();
        $oForm->addBntCancel();

        $oForm->setFields($aItems);
        $oForm->setValues(array());

        $this->setExtInterface($oForm);

    }

    /**
     * Состаяние. Выбор корневого набора разделов
     * @throws Exception
     * @return bool
     */
    protected function actionSave() {

        // значение для сохранения
        $sText = (string)$this->getInDataVal( 'css_text' );

        // имя файла
        $sFilePath = Design::getAddCssFilePath($this->sViewMode);

        // если данные есть
        if ( $sText ) {

            // создать
            if (!$handle = fopen($sFilePath, 'w+'))
                throw new Exception("Не могу открыть файл для записи.");

            // записать
            if (fwrite($handle, $sText) === false)
                throw new Exception("Не могу записать в файл.");

            // сохранить
            fclose($handle);

            // изменение прав доступа
            chmod( $sFilePath, 0777 );

        } else {

            // удалить файл
            if ( file_exists($sFilePath) )
                unlink($sFilePath);

        }

        // перегрузить фрейм отображения
        $this->fireJSEvent('reload_display_form');

        // открыть заново формуы
        $this->actionInit();

    }

    /**
     * Установка служебных данных
     * @param ExtPrototype $oExtIface
     */
    protected function setServiceData( ExtPrototype $oExtIface ) {
        $sTitle = sprintf('%s: %s сайта', $this->sTabName, Design::getVersionTitle( $this->sViewMode ));
        $oExtIface->setPanelTitle($sTitle);
    }

    /**
     * Проверяет наличие файлов и доступность их на запись
     */
    protected function testFileAccess() {

        CSSEditorDesignApi::testFileAccess();

    }

    /**
     * Отдает данные из нужного файла
     * @throws Exception
     * @return string
     */
    protected function getValFromFile() {
        return (string)file_get_contents( Design::getAddCssFilePath($this->sViewMode) );
    }

    /**
     * Отдает имя css файла для нужного слоя отображения
     * @return string
     */
    protected function getFilePath() {
        return sprintf('%s%sadd_%s.css',ROOTPATH, Design::addDir, $this->sViewMode);
    }

}
