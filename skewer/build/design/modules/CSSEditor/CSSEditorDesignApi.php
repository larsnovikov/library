<?php
/**
 * API для работы с редактором пользовательских css файлов
 */
class CSSEditorDesignApi {

    /**
     * Проверяет наличие необходимых файлов и папок для заданного слоя
     * @throws Exception
     */
    public static function testFileAccess() {

        // директория хранения
        $sDir = Design::getAddCssDirPath();

        // проверить наличие директории
        if ( !is_dir($sDir) ) {

            // попробовать создать
            if ( !mkdir( $sDir, 0777, true ) )
                throw new Exception('Директория хранения дополнительных css файлов ('.Design::addDir.') отсутствует и не может быть создана!');

            // выставлние прав. параметр mkdir иногда не срабатывает
            chmod($sDir,0777);

        }

        // проверка доступности на запись дополнительных файлов
        foreach ( Design::getVersionList() as $sVersion ) {
            $sFileName = Design::getAddCssFilePath($sVersion);
            if ( file_exists($sFileName) and !is_writable($sFileName) ) {
                throw new Exception('Дополнительный css файл для слоя "'.$sVersion.'" не доступен для записи');
            }
        }

    }

}
