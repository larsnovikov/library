/**
 * Шапка дизайнерской панели
 */
Ext.define('Ext.design.Header',{
    extend: 'Ext.panel.Panel',
    region: 'north',
    baseCls: 'b-header-panel',
    border: 0,
    padding: '0 0 3 0',

    dockedItems: [{
        xtype: 'toolbar',
        dock: 'bottom',
        ui: 'footer',
        defaults: {
            xtype: 'button',
            minWidth: 100
        },
        items: [
            {
                text: designLang.headFullVer,
                handler: function() {
                    frameApi.setDisplayUrl('/');
                }
            },{
                text: designLang.headPdaVer,
                handler: function() {
                    frameApi.setDisplayUrl('/pda/');
                }
            },{
                text: designLang.headToAdminText,
                handler: function() {
                    window.open('/admin/');
                }
            },{
                xtype: 'component', flex: 1
            },{
                text: designLang.headRenewText,
                handler: function() {
                    frameApi.reloadDisplayFrame();
                }
            },{
                text: designLang.headExit,
                handler: function() {
                    var me = this.up('panel');
                    Ext.MessageBox.confirm(designLang.headExit, designLang.headExitConfirm, function(res){

                        if ( res !== 'yes' ) return;

                        processManager.setData(me.path, {
                            cmd: 'logout'
                        });
                        processManager.postData();

                    });
                }
            }
        ]
    }],

    execute: function( data, cmd ) {

        switch ( cmd ) {

            case 'logout':

                if ( data.success ) {
                    frameApi.reloadAll();
                } else {
                    sk.error(designLang.headExitError);
                }

                break;

        }

    }

});
