<?php
/**
 * Модуль верхней панели дизайнерского режима
 */
class HeaderDesignModule extends AdminModulePrototype {

    /**
     * Первичная инициализация
     * @return int
     */
    public function actionInit() {
    }

    /**
     * Выход из системы
     * @return int
     */
    protected function actionLogout(){

        // задать состояние
        $this->setCmd('logout');

        skLogger::addNoticeReport("Выход пользователя из системы администрирования",skLogger::buildDescription(array('ID пользователя'=>CurrentAdmin::getId(), 'Логин'=>$_SESSION['auth']['admin']['userData']['login'])),skLogger::logUsers,$this->getModuleName());

        // попытка авторизации
        $bLogOut = CurrentAdmin::logout();

        // результат авторизации
        $this->setData('success',$bLogOut);

        // отдать результат работы метода
        return psComplete;

    }

}
