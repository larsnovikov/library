<?php
/**
 * Модуль для вывода панели с редакторм параметров
 */
class ParamPanelDesignModule extends AdminModulePrototype {

    public function execute() {

        $this->addChildProcess(new skContext('tree','ParamTreeDesignModule',ctModule,array()));
        $this->addChildProcess(new skContext('params','ParamEditorDesignModule',ctModule,array()));

        return psComplete;
    }

}
