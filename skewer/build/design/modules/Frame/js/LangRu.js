/**
 * Словарь для русской языковой версии дизайнерского режима
 */
Ext.define('Ext.design.LangRu',{

    /*
     * Общие
     */

    headRenewText: 'Обновить',
    headToAdminText: 'CMS',
    headFullVer: 'Сайт',
    headPdaVer: 'PDA',
    headExit: 'Выйти',
    headExitError: 'Ошибка при выходе',
    headExitConfirm: 'Вы верены, что хотите покинуть административный интерфейс?',

    /*
     * Набор параметров
     */
    paramsTabTitle: 'CSS параметры',
    paramsPanelHeader: 'Параметры',
    paramsNameTitle: 'Название',
    paramsValueTitle: 'Значение',
    paramsRevertParamTitle: 'Сброс параметра',
    paramsRevertParam: 'Сбросить значение параметра?',
    paramsErrorOnSave: 'Ошибка при сохрранении',
    paramsGroupNotFound: 'Группа параметров не найдена',

    /*
     * Зоны
     */
    zonesTabTitle: 'Модули',
    zoneTemplatesPanelTitle: 'Шаблоны',
    zoneSelectorPanelTitle: 'Области',
    zoneLabelsPanelTitle: 'Модули',
    zoneAddLabelHeader: 'Доступные модули',
    zoneDelLabelHeader: 'Удаление модуля',
    zoneDelLabelText: 'Удалить модуль',
    zoneDelZoneHeader: 'Удаление области',
    zoneDelZoneText: 'Удалить область',
    zoneLabelDragText: 'Перетащите для изменения порядка',
    zoneAddLabelDragText: 'Перетащите для добавления модуля'

});
