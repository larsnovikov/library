Ext.Loader.setConfig({enabled: true});

// конфигурация слоя
var layerName = 'design';
var extPrefix = 'Ext.'+layerName+'.';
var rootPath = '/skewer_build/'+layerName+'/modules/Frame/js';

// инициализация базовых инструментов
var sk, frameApi;
Ext.Loader.setPath('Ext.sk', '/skewer_build/libs/pmExtJS/js');
Ext.Loader.setPath('Ext.cms', '/skewer_build/cms/modules/Frame/js');

// установка путей для слоя
Ext.Loader.setPath('Ext.'+layerName, rootPath);

buildConfig = Ext.create(extPrefix+'Config');
buildLang = Ext.create('Ext.cms.LangRu');
designLang = Ext.create(extPrefix+'LangRu');

// функция вызова клобального события
var firePanelEvent = function() {};

Ext.onReady(function() {

    // инициализация основного набора параменных
    sk = Ext.create('Ext.sk.Init');
    frameApi = Ext.create(extPrefix+'FrameApi');
    frameApi.init();
    frameApi.setDisplayUrl('/');
    processManager.setProcess('init',sk);

    // инициализация CKEditor'а
    Ext.Loader.require('/skewer_build/libs/CKEditor/ckInit');

    // инициализация браузера файлов и поля типа "файл"
    processManager.setProcess('fileSelector',Ext.create('Ext.sk.FileSelector'));

    firePanelEvent = function( eventName ) {
        processManager.fireEvent.apply( processManager, arguments );
    };

});

// удержание сессии в живых
setInterval(function(){
    Ext.Ajax.request({
        url: '/skewer_build/cms/modules/Frame/keepalive.php',
        method: 'GET',
        params: {ping: 1},
        success: function (result, request) {}
    });

}, 420000); // каждые 7 минут
