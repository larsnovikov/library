/**
 * Основные конфигурационные константы CMS
 */
Ext.define('Ext.design.Config', {

    // версия движка
    cmsVersion: '3.0',

    // название слоя
    layerName: layerName,
    rootPath: rootPath,

    // имя основного файла для запросов
    request_script: '/'+layerName+'/index.php',

    request_dir: '/'+layerName+'/',

    // путь для вызова файлового браузера
    files_path: '/admin/',

    // время ожидания ответа ajax запроса
    request_timeout: 30000

});
