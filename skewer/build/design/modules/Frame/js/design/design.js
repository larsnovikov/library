/**
 * Дизайнерский режим
 * основной модуль
 */

$(function(){

    // если дизайнерский режим активен
    if ( !designObj.init() ) {

        // убрать обводку
        $('.g-ramaborder').removeClass('g-ramaborder');

        // убрать дополнительные элементы
        $('.b-desbtn').remove();

        return;

    }

    /**
     * Добавление функции, вызаваемой перед обновлением страницы
     */
    window.preReload = function() {
        $('<div></div>')
            .addClass('page_cover')
            .appendTo($('body'))
        ;
    };

    var version = $('body').attr('sklayer') || 'default';

    //noinspection JSUnresolvedFunction
    $.getJSON('/design/','mode=menu&sectionId='+designObj.sectionId+'&version='+version,function(data){

        // добавление выпадающих меню в html
        designObj.addMenuItemsToBody( data );

    });

    // Обработка позиции перетаскиваемых мышью блоков в шапке

    $( ".js-designDrag-left" ).draggable();
    $( ".js-designDrag-left" ).bind('dragstart',function( event ){

        return event.ctrlKey;

    }).bind('dragstop',function( event , ui){

            designObj.sendCSSParams( {
                'hValue':$(this).css('left'),
                'vValue':$(this).css('top'),
                'hPosition':'left',
                'paramPath': $(this).attr('skTag')
            } );

        });

    $( ".js-designDrag-right" ).draggable();
    $( ".js-designDrag-right" ).bind('dragstart',function( event ){

        return event.ctrlKey;

    }).bind('dragstop',function( event , ui){

            var rightPos = $('.container__content').width() - ui.position.left - $(this).width();
            $(this).css('left','auto');
            $(this).css('right',rightPos);
            designObj.sendCSSParams( {
                'hValue':rightPos,
                'vValue':$(this).css('top'),
                'hPosition':'right',
                'paramPath': $(this).attr('skTag')

            } );

        });

});
