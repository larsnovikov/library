<?php
/**
 * 
 * @class FrameDesignModule
 *
 * @author ArmiT, $Author: acat $
 * @version $Revision: 974 $
 * @date $Date: 2012-10-05 12:54:23 +0400 (Пт., 05 окт. 2012) $
 *
 */ 
class FrameDesignModule extends skModule {
    public function init(){

        $this->setParser(parserTwig);

    }// func

    public function execute() {

        // установка директории для шаблонов
        $this->setData('moduleDir',$this->getModuleWebDir());

        // проверка прав доступа к дизайнерскому режиму
        if ( !CurrentAdmin::allowDesignAccess() ) {
            $this->setTemplate('denied.twig');
            return psComplete;
        }

        switch ( (string)$this->getStr('mode') ) {

            // страница загрузки
            case 'loading':
                $this->setTemplate('loading.twig');
                break;

            // панель редактора
            case 'editor':

                $oProcessSession = new skProcessSession();
                $sTicket = $oProcessSession->createSession();

                $this->setData('sessionId',$sTicket);

                $this->setTemplate('editor.twig');

                break;

            // запрос пунктов контекстного меню
            case 'menu':

                // сборка меню
                $aMenu = array();
                $sVersion = $this->getStr('version','default');
                $iSectionId = $this->getInt('sectionId');
                $aGroupList = DesignManager::getGroupList( $sVersion, array('id','name','title') );
                foreach ( $aGroupList as $aGroup ) {
                    $aMenu[$aGroup['name']] = array(
                        'id' => $aGroup['id'],
                        'title' => $aGroup['title'],
                    );
                }

                echo json_encode(array(
                    'menu'=>$aMenu,
                    'modules' => $this->getModuleTitltes( $iSectionId )
                ));

                break;

            // окно с 2 панелями: отображения и редактора
            default:

                $this->setTemplate('index.twig');

                // глобальных флаг активации дизайнерского режима
                Design::setModeGlobalFlag();

                break;

        }

        return psComplete;
    }// func

    /**
     * Отдает массив названий админских модулей для раздела
     * В качестве ключей идут метки
     * @param $iSectionId
     * @return array
     */
    protected function getModuleTitltes( $iSectionId ) {

        $aOut = array();

        $aParamList = Parameters::getAllAsList($iSectionId,array('fields'=>'name,value'));

        foreach ( $aParamList as $aParam )
            if ( $aParam['name']==='object' and $aParam['value'] )
                $aOut[ $aParam['group'] ] = FrameDesignApi::getModuleTitleByName( $aParam['value'] );

        return $aOut;

    }

    public function shutdown(){

    }// func
}// class
