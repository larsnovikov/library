<?php
/**
 * Библиотека классов для дизайнерского режима
 */
class FrameDesignApi {

    /**
     * Отдает название модуля по имени
     * @static
     * @param string $sModule
     * @return string
     */
    public static function getModuleTitleByName( $sModule ) {
        $sModule = preg_replace( '/Module$/', '', $sModule );
        $sConfigPath = 'buildConfig.Page.modules.' . $sModule . '.title';
        return skConfig::isExists( $sConfigPath )? (string)skConfig::get($sConfigPath): $sModule;
    }

}
