/**
 * Шапка cms
 */

Ext.define('Ext.design.Path',{
    extend: 'Ext.panel.Panel',
    region: 'north',
    baseCls: 'b-header-panel',
    sectionId: 0,
    isLoaded: false,
    isValid: false,
    dataLoaded: false,
    autoScroll: true,
    border: 0,
    html: '',
    layout: 'fit'
});
