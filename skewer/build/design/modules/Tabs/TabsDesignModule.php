<?php
/**
 * Модуль для вывода нескольких интерфейсов в дизайнерском режиме
 */
class TabsDesignModule extends AdminModulePrototype {

    /**
     * Первичная инициализация
     * @return int
     */
    public function actionInit() {

        $this->addChildProcess(new skContext('param_panel','ParamPanelDesignModule',ctModule,array()));
        $this->addChildProcess(new skContext('css_panel','CSSEditorDesignModule',ctModule,array()));
        $this->addChildProcess(new skContext('zone_panel','ZonesDesignModule',ctModule,array()));

        return psComplete;
    }

    /**
     * Добавляет модуль
     */
    protected function actionAddModule() {

        $sLabelName = $this->getStr( 'labelName' );
        $iSectionId = $this->getInt( 'sectionId', skConfig::get('section.main') );

        // запросить параметр в метке с админкой
        $aParam = Parameters::getByName( $iSectionId, $sLabelName, 'objectAdm' );

        // если параметр есть
        if ( $aParam ) {

            // добавление модуля
            $this->addModule( $sLabelName, $aParam['value'], $iSectionId );

            return;

        }

        // запросить параметр в обычного объекта
        $aParam = Parameters::getByName( $iSectionId, $sLabelName, 'object' );
        if ( $aParam ) {
            // попробовать скомпоновать админское имя
            $sModuleName = preg_replace( '/Module$/', 'AdmModule', $aParam['value'] );

            if ( $this->hasAdmModuleInSection( $sModuleName, $iSectionId ) ) {
                $this->addModule( $sModuleName, $sModuleName, $iSectionId );
                return;
            }

        }

        $this->addError('Модуль управления не найден');

    }

    /**
     * Добавляет модуль как подчиненный
     * @param $sLabelName
     * @param $sModuleName
     * @param $iSectionId
     */
    protected function addModule( $sLabelName, $sModuleName, $iSectionId ) {

        $sObjName = 'module_' . $sLabelName;

        if ( !class_exists($sModuleName) ) {
            $this->addError('Модуль не найден');
            return;
        }

        // создать
        $this->addChildProcess(new skContext($sObjName,$sModuleName,ctModule,array()));

        // добавить параметр "id раздела"
        $process = $this->getChildProcess($sObjName);
        $process->addRequest('sectionId', $iSectionId);

        // инструкции для интерфейсной части
        $this->setCmd('load_module');
        $this->setData('tabPath',$process->getLabelPath());

    }

    /**
     * Удаляет набор модулей
     */
    protected function actionDelModule() {

        // набор путей для удаления
        $aList = $this->get('items');
        if ( !is_array($aList) )
            throw new Exception('Неверный формат посылки');

        $sCurrentPath = $this->oContext->oProcess->getLabelPath();

        // перебираем все
        foreach ( $aList as $sPath ) {

            // процесс должен быть подчиненным
            if ( strpos( $sPath, $sCurrentPath.'.' )!==0 )
                continue;

            $sLabel = substr( $sPath, strlen($sCurrentPath)+1 );

            $this->removeChildProcess( $sLabel );

        }

        // инструкции для интерфейсной части
        $this->setCmd('close_module');
        $this->setData('list',$aList);

    }

    /**
     * Определяет подключен ли модуль в разделе
     * @param string $sModuleName
     * @param int $iSectionId
     * @return bool
     */
    private function hasAdmModuleInSection( $sModuleName, $iSectionId ) {

        $aParamList = Parameters::getAllAsList($iSectionId,array('fields'=>'name,value'));

        foreach ( $aParamList as $aParam )
            if ( $aParam['name']==='objectAdm' and $aParam['value']===$sModuleName )
                return true;

        return false;

    }

    /**
     * Добавление редактора
     */
    protected function actionAddEditor() {

        $sEditorId = $this->getStr( 'editorId' );
        $iSectionId = $this->getInt( 'sectionId', skConfig::get('section.main') );

        $sObjName = 'editor_'.str_replace(array('/','.'),'_',$sEditorId);

        // создать
        $this->addChildProcess(new skContext($sObjName,'Editor4OneAdmModule',ctModule,array()));

        // добавить параметр "id раздела"
        $process = $this->getChildProcess($sObjName);
        $process->addRequest('sectionId', $iSectionId);
        $process->addRequest('editorId',  $sEditorId);

        // инструкции для интерфейсной части
        $this->setCmd('load_module');
        $this->setData('tabPath',$process->getLabelPath());

    }

}
